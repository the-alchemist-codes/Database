import os
from PIL import Image
from lib.shared import respath
from lib.AssetManagement.Download import get_assetlist
from ReExtractAssets import processAssets

def main():
	inp = input('Select Version:\nGL - Global Version\nJP - Japanese Version\nSelection:\t').upper()
	if inp == 'GL':
		pathPost = 'GL'
		region = 'global'
	elif inp == 'JP':
		pathPost = 'JP'
		region = 'japan'
	elif inp == 'TW':
		pathPost = 'TW'
		region = 'taiwan'
	else:
		input('wrong input')
		main()
		return
	
	print('Fetching Assetlist')
	assets = get_assetlist(region, 'aatc')['assets']
	origin = os.path.join(respath, 'RAW_Assets_%s' % pathPost)
	dest = os.path.join(respath, 'Assets%s' % pathPost)
	SearchInvalidImages(assets, origin, dest)

def SearchInvalidImages(assets, origin, dest):
	reextract = []
	for path, subdirs, files in os.walk(dest):
		for name in files:
			if name[-4:] == '.png':
				fp = os.path.join(path, name)
				try:
					Image.open(fp)
				except OSError:
					os.remove(fp)
					print(fp)
					apath = assets[fp[len(dest)+1:-4].replace('\\','/').lstrip('/')]
					try:
						reextract.append(assets[apath])
					except KeyError:
						try:
							reextract.append(assets[apath.rsplit('/',1)[0]])						
						except KeyError:
							pass
	processAssets(origin, dest,reextract, 1)


if __name__ == '__main__':
	main()