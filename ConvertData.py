import json
import os

import lib.ParamFunctions as ParamFunctions
from lib import DATAGL, DATAJP, DifParam, GenericSkillDescription, TRANSLATION, dmg_formula, respath, rootpath, exportpath
from lib.localization import apply_loc, compendium

LOAD_QUEST_STORY = True
LOAD_QUEST_MAP = True
Map_Path_GL = os.path.join(respath, *['AssetsGL', 'LocalMaps'])
Map_Path_JP = os.path.join(respath, *['AssetsJP', 'LocalMaps'])
Loc_Path_GL = os.path.join(respath, *['AssetsGL', 'loc', 'english'])
Loc_Path_JP = os.path.join(respath, *['AssetsJP', 'loc', 'japanese'])


def main():
	export = convertRaws()
	apply_loc(export, DATAGL, DATAJP)
	export = Fixes(export)
	os.makedirs(exportpath, exist_ok = True)
	# bot stuff
	ExportData(export, exportpath)


def convertRaws(save = True):
	mastersfiles = ['MasterParam', 'QuestParam', 'QuestDropParam']
	masters = [data[f'Data/{mf}'] for mf in mastersfiles for data in [DATAGL, DATAJP]]
	export = {}
	
	for master in masters:
		for main in master:
			# get right method for convertion
			mName = main[0].upper() + main[1:]
			try:
				method = getattr(ParamFunctions, ParamFunctions._Assignments.methods[mName], False)
			except:
				print('Not Found: ' + main)
				continue
			# method= getattr(ParamFunctions, mName+'Param',  getattr(ParamFunctions, mName+'EffectParam',getattr(ParamFunctions, mName[:-1]+'Param',False)))
			# if mName not in convertList:
			#    convertList[mName]=method.__name__ if method else ''
			
			if not method:
				print('Not Found: ' + main)
				continue
			
			# convert the main
			# print(main)
			converted = {}
			if 'iname' in master[main][0]:  # most Params
				converted = {
						entry['iname']: {key: val for key, val in method(entry).items() if val or val == 0}
						for entry in master[main]
				}
			else:  # like Tobira
				converted = [
						method(entry)
						for entry in master[main]
				]
				if len(converted) == 1:
					converted = converted[0]
			
			# add to new main
			if main in export:
				if type(converted) == dict:
					if type(list(converted.items())[0][1]) == dict:
						for item in converted:
							if item in export[main]:
								export[main][item]['dif'] = DifParam(export[main][item], converted[item])
							else:
								export[main][item] = converted[item]
					else:
						export[main]['dif'] = DifParam(export[main], converted)
				elif type(converted) == list:
					export[main] = converted
			else:
				export[main] = converted
	
	return export


def Fixes(master):
	import zipfile
	zip = zipfile.ZipFile(os.path.join(exportpath,'extra_quests_data.zip'), mode = 'w', compression = zipfile.ZIP_DEFLATED)

	ABILITY = master['Ability']
	GEAR = master['Artifact']
	UNIT = master['Unit']
	SKILL = master['Skill']
	BUFF = master['Buff']
	ITEM = master['Item']
	JOB = master['Job']
	JOBSET = master['JobSet']
	GEAR = master['Artifact']
	WEAPON = master['Weapon']
	QUEST = master['quests']
	CARD = master['ConceptCard']
	CONDITION = master['Cond']
	VEDA = master['towerFloors']
	
	##job############################################################
	print('Fix - Skill')
	for key, skill in SKILL.items():
		if 'weapon' in skill:
			try:
				skill['formula'] = dmg_formula(WEAPON[skill['weapon']])
			except:
				skill['formula'] = 'missing'
		skill['expr2'] = GenericSkillDescription(skill, master)
	
	print('Fix - Job')
	if 1:
		# abilities and stats
		for key, job in JOB.items():
			job['units'] = {'unit': [], 'NPC': []}
			if 'fixed_ability' in job:
				# abilities
				abilities = {
						'main'    : job['fixed_ability'],
						'sub'     : '',
						'reaction': [],
						'passive' : []
				}
				if 'abilities' in job:
					for abil in job['abilities']:
						slot = master['Ability'][abil]['slot']
						if slot == 'Action':
							abilities['sub'] = abil
						elif slot == 'Reaction':
							abilities['reaction'] += [abil]
						elif slot == 'Support':
							abilities['passive'] += [abil]
				else:
					print('Missing abilities: %s' % key)
				job['abilities'] = abilities
				del job['fixed_ability']
			
			# stats
			stats = {}
			for i, rank in enumerate(job['ranks']):
				if i == 0:
					continue
				for item in rank['equips']:
					try:
						for buff in BUFF[SKILL[ITEM[item]['skill']]['target_buff_iname']]['buffs']:
							if buff['type'] not in stats:
								stats[buff['type']] = 0
							if i < len(job['ranks']) - 1:
								stats[buff['type']] += buff['value_ini']
							else:
								stats[buff['type']] += buff['value_max']
					except:
						pass
			
			job['stats'] = stats
			
			# weapon
			if 'artifact' in job:
				job['weapon'] = GEAR[job['artifact']]['tag']
			# formula
			job['formula'] = SKILL[job['atkskill'][0]]['formula'] if 'formula' in SKILL[job['atkskill'][0]] else '100% PATK'
			
			# evolutions
			if 'origin' in job and job['origin'] != job['iname'] and job['origin'] in JOB:
				origin = JOB[job['origin']]
				if 'evolutions' not in origin:
					origin['evolutions'] = []
				origin['evolutions'].append(job['iname'])
			# used by
			
			# stats modifiers
			for rank in job['ranks']:
				rank['status'] = {
						TRANSLATION[stat.title()]: value
						for stat, value in rank['status'].items()
				}
	# save
	
	##quests#########################################################
	print('Fix - Quests')
	if 1:
		def loadMap(name):
			mpath = os.path.join(Map_Path_GL, name)
			if os.path.exists(mpath):
				with open(mpath, 'rt', encoding = 'utf-8-sig') as f:
					return f.read()
			mpath = os.path.join(Map_Path_JP, name)
			if os.path.exists(mpath):
				with open(mpath, 'rt', encoding = 'utf-8-sig') as f:
					return f.read()
			return False
		
		# load in scene and set
		for key, quest in list(QUEST.items()) + list(VEDA.items()):
			quest['dropList'] = []
			if 'type' in quest and quest['type'] == 'Free':
				quest['type'] = 'Hard'
			# maps
			if LOAD_QUEST_MAP:
				if 'map' in quest:
					for Map in quest['map']:
						Scene = loadMap(Map['mapSceneName'])
						if Scene:
							Map['Scene'] = json.loads(Scene)
							if Map['mapSceneName'] not in zip.namelist():
								zip.writestr(Map['mapSceneName'], json.dumps(Map['Scene'], ensure_ascii = False).encode('utf8'))
						Set = loadMap(Map['mapSetName'])
						if Set:
							Map['Set'] = json.loads(Set)
							party = ['party', 'enemy', 'arena']
							for p in party:
								if p in Map['Set']:
									for index, value in enumerate(Map['Set'][p]):
										Map['Set'][p][index] = ParamFunctions.MapSetting(value)
							# seperate parties
							enemies = []
							allies = []
							treasures = []
							jewels = []
							walls = []
							traps = []
							if 'enemy' in Map['Set']:
								for unit in Map['Set']['enemy']:
									if unit['side'] == 'Ally':
										allies.append(unit)
									if unit['side'] == 'Enemy':
										if 'TREASURE' in unit['iname']:
											treasures.append(unit)
										elif '_GEM_' in unit['iname'] or unit['iname'][-4:] == '_GEM':
											jewels.append(unit)
										elif '_WALL_' in unit['iname']:
											walls.append(unit)
										else:
											enemies.append(unit)
							
							# tricks fix
							if 'tricks' in Map['Set']:
								traps = [ParamFunctions.TricksSetting(trick) for trick in Map['Set']['tricks']]
								del Map['Set']['tricks']
							Map['Set'].update({
									'enemy'   : enemies,
									'ally'    : allies,
									'treasure': treasures,
									'jewel'   : jewels,
									'wall'    : walls,
									'trap'	  : traps 
							})

							# patch arena match
							if 'arena' in Map['Set']:
								Map['Set']['enemy'] = Map['Set']['arena']
								del Map['Set']['arena']

							# save changes
							if Map['mapSetName'] not in zip.namelist():
								zip.writestr(Map['mapSetName'], json.dumps(Map['Set'], ensure_ascii = False).encode('utf8'))
								
			if LOAD_QUEST_STORY:
				def event_story(name):
					es_path = os.path.join(Loc_Path_GL, name)
					if not os.path.exists(es_path):
						es_path = os.path.join(Loc_Path_JP, name)
						if not os.path.exists(es_path):
							return name
					with open(os.path.join(es_path), "rt", encoding = 'utf-8-sig') as f:
						ret = []
						for line in f:
							try:
								line = line.rstrip('\n').rstrip(' ').split('\t')
								ret.append({
										'text'     : line[1],
										'voiceover': line[2] if len(line) == 3 else ''
								})
							except Exception as e:
								print(line, 'error', e)
						if name not in zip.namelist():
							zip.writestr(name, json.dumps(ret, ensure_ascii = False).encode('utf8'))
						return ret
				
				if 'event_start' in quest:
					quest['event_start_'] = event_story(quest['event_start'])
				if 'event_clear' in quest:
					quest['event_clear_'] = event_story(quest['event_clear'])
				if 'map' in quest:
					for map in quest['map']:
						if 'eventSceneName' in map:
							map['eventScene'] = event_story(map['eventSceneName'])
		
		# drop list
		for key, quest in master['simpleDropTable'].items():
			if key in QUEST:
				QUEST[key].update(quest)
			else:
				pass
		# print(key)
		
		del master['simpleDropTable']
		
		for key, item in master['simpleQuestDrops'].items():
			if key in ITEM:
				ITEM[key].update(item)
				for quest in item['questlist']:
					if quest in QUEST and key not in QUEST[quest]['dropList']:
						QUEST[quest]['dropList'].append(key)
		
		del master['simpleQuestDrops']
	##units##########################################################
	print('Fix - Unit')
	if 1:
		# jobset to job and add skins and seperate unit and NPC
		(eUnit, eEnemy) = ({}, {})
		for key, unit in UNIT.items():
			# unit or enemy
			if 'lore' in unit or ('piece' in unit and 'ai' in unit and unit['ai'] == 'AI_PLAYER' and 'EN' != key[6] + key[7] and 'role' not in unit) and unit['iname'][-6:] != '_TRIAL':
				eUnit[key] = unit
			else:
				eEnemy[key] = unit
			
			if 'jobs' not in unit:
				unit['jobs'] = [None]*9 # jobs - job +/e - job +
			# add skins from dif
			if 'skins' not in unit:
				unit['skins'] = []
			if 'dif' in unit and 'skins' in unit['dif']:
				unit['skins'] += unit['dif']['skins']
			
			# change js to job
			if 'jobsets' in unit and unit['jobsets']:
				unit['jobs'][:len(unit['jobsets'])] = [
						(master['JobSet'][js]['job'] if 'job' in master['JobSet'][js] else master['JobSet'][js]['dif']['job'] if 'dif' in master['JobSet'][js] else None) if js else None
						for js in unit['jobsets']
				]
				del unit['jobsets']
		
		# add job+
		for key, js in master['JobSet'].items():
			if 'jobchange' in js:
				target_unit = js['target_unit'] if 'target_unit' in js else js['dif']['target_unit'] if 'dif' in js and 'target_unit' in js['dif'] else False
				if not target_unit:
					continue
				unit = UNIT[target_unit]
				
				for index, job in enumerate(unit['jobs']):
					if job and job == js['lock_jobs']['iname']:
						unit['jobs'][index+3] = js['job'] if 'job' in js else js['dif']['job']
		
		# fix names
		print('\tFix Names')
		unit_names = {}
		for key, unit in eUnit.items():
			unit['name-jp'] = compendium[key] if key in compendium else ''
			
			if unit['name'] not in unit_names:
				unit_names[unit['name']] = unit
			else:
				ounit = unit_names[unit['name']]
				if len(unit['iname']) > len(ounit['iname']):
					if not unit.get('name-jp'):
						unit['name-jp'] = unit['name']
					print(unit['iname'], 'GL:', unit['name'], 'JP:', unit['name-jp'])
					unit['name'] = unit['name-jp']
				else:
					print(ounit['iname'], 'GL:', ounit['name'], 'JP:', ounit['name-jp'])
					ounit['name'] = ounit['name-jp']
		
		# add kaigan
		for kaigan in master['Tobira']:
			unit = UNIT[kaigan['mUnitIname']]
			if 'kaigan' not in unit:
				unit['kaigan'] = {}
			unit['kaigan'][kaigan['mCategory']] = kaigan
		
		# add nensou
		for key, card in CARD.items():
			if 'effects' not in card:
				continue
			try:
				unit = UNIT['UN_V2_' + key.rsplit('_', 2)[1]]
				if 'conceptcards' not in unit:
					unit['conceptcards'] = []
				unit['conceptcards'].append(key)
				card['unit'] = unit['iname']
			except:
				unit = None
			
			for effect in card['effects']:
				if 'cnds_iname' not in effect:
					continue
				
				if not unit:
					conds = master['ConceptCardConditions'][effect['cnds_iname']]
					if 'unit_group' not in conds:
						continue
					units = master['UnitGroup'][conds['unit_group']]['units']
					if len(units) == 1 and units[0] != 'UN_V2_L_UROB':
						unit = UNIT[units[0]]
						if 'conceptcards' not in unit:
							unit['conceptcards'] = []
						unit['conceptcards'].append(key)
						card['unit'] = unit['iname']
					else:
						continue
				# skin
				if 'skin' in effect:
					unit['skins'].append(effect['skin'])
		
		# add occurence
		for key, quest in master['quests'].items():
			if 'map' in quest:
				for map in quest['map']:
					if 'Set' in map and 'enemy' in map['Set']:
						for enemy in map['Set']['enemy']:
							if 'iname' not in enemy:
								continue
							if enemy['iname'] in UNIT:
								if 'occurrence' not in UNIT[enemy['iname']]:
									UNIT[enemy['iname']]['occurrence'] = []
								if key not in UNIT[enemy['iname']]['occurrence']:
									UNIT[enemy['iname']]['occurrence'].append(key)
							else:
								pass
			# Missing enemies
			# print(enemy['iname'])
		
		master['Enemy'] = eEnemy
		master['Unit'] = eUnit
	
	##### JOB 2 - adding units #####################################
	for key, unit in eUnit.items():
		if 'jobs' in unit:
			for job in unit['jobs']:
				try:
					job = JOB[job]
					job['units']['unit'].append(key)
				except:
					pass

	for key, unit in eEnemy.items():
		if 'jobs' in unit:
			for job in unit['jobs']:
				try:
					job = JOB[job]
					job['units']['NPC'].append(key)
				except:
					pass
	
	# add keywords
	
	def used_index(key):
		if key not in used:
			return key
		else:
			i = 2
			while True:
				keyword = '%s %d' % (key, i)
				if keyword not in used:
					return keyword
				else:
					i += 1
	
	if 1:
		# units
		for key, unit in eUnit.items():
			if 'keywords' in unit:
				if unit['name'] not in unit['keywords']:
					unit['keywords'].append(unit['name'])
			else:
				unit['keywords'] = [unit['name']]
			
			for keyword in unit['keywords']:
				if keyword.title() in unit_names and keyword.title() != unit['name']:
					unit['keywords'].remove(keyword)
		# jobs
		used = {}
		
		def checkJob(job, key, JP = False):
			try:
				if len(job['units']['unit']) == 1:
					junit = UNIT[job['units']['unit'][0]]
					job['keywords'].append('%s %s' % (job[key], junit['name']))
					if 'name-jp' in junit and junit['name'] != junit['name-jp']:
						job['keywords'].append('%s %s' % (job[key], junit['name-jp']))
				
				if job[key] in used:
					# resolve conflicts
					weak, ojob = used[job[key]]
					if (not JP and weak) or (JP == weak and len(job['units']['unit']) > len(ojob['units']['unit'])):
						# remove JP traces
						ojob['keywords'].remove(job[key])
					else:
						return
				
				job['keywords'].append(job[key])
				used[job[key]] = (JP, job)
			except:
				pass
		
		for key, job in JOB.items():
			# unit if unique
			job['keywords'] = []
			checkJob(job, 'name')
			if 'name-jp' in job:
				checkJob(job, 'name-jp', True)
		
		# gear
		used = {}
		for key, gear in GEAR.items():
			gear['keywords'] = []
			# gear name
			try:
				keyword = used_index(gear['name'])
			except:
				continue
			gear['keywords'].append(keyword)
			used[keyword] = gear
			# unit
			if 'abil_inames' in gear:
				for abil in gear['abil_inames']:
					if abil not in ABILITY:
						continue
					
					ability = ABILITY[abil]
					if 'condition_units' in ability and len(ability['condition_units']) == 1:
						gunit = UNIT[ability['condition_units'][0]]
						keyword = used_index(gunit['name'])
						gear['keywords'].append(keyword)
						used[keyword] = gear
						if 'name-jp' in gunit and gunit['name'] != gunit['name-jp']:
							keyword = used_index(gunit['name-jp'])
							gear['keywords'].append(keyword)
							used[keyword] = gear
		
		# nensou
		used = {}
		for key, card in CARD.items():
			if 'keywords' not in card:
				card['keywords'] = []
			card['keywords'].append(card['name'])
			if 'kanji' in card and card['kanji'] != card['name']:
				card['keywords'].append([card['kanji']])
			if 'unit' in card:
				cunit = UNIT[card['unit']]
				keyword = used_index(cunit['name'])
				card['keywords'].append(keyword)
				used[keyword] = card
				if 'name-jp' in cunit and cunit['name'] != cunit['name-jp']:
					keyword = used_index(cunit['name-jp'])
					card['keywords'].append(keyword)
					used[keyword] = card
	
	zip.close()
	return master


def ExportData(master, PATH_export):
	from lib.CreateAssetTable import createAssetTable

	# add file paths for bot
	master['path'] = createAssetTable(['.png', '.jpg', '.mp3'])

	#remove maps and stories
	for mkey in ['quests','towerFloors']:
		for key, quest in master[mkey].items():
			if 'map' in quest:
				for i, qmap in enumerate(quest['map']):
					for skey in ['Scene','Set','eventScene']:
						if skey in qmap:
							qmap.pop(skey)
			for skey in ['event_start_','event_clear_']:
				if skey in quest:
					quest.pop(skey)

	import pickle
	with open(os.path.join(PATH_export, 'Database.pickle'), 'wb') as f:
		pickle.dump(
				{
						key.title(): items
						for key, items in master.items()
				},
				f
		)
	
	for main, data in master.items():
		with open(os.path.join(PATH_export, f'{main}.json'), 'wb') as f:
			f.write(json.dumps(data, ensure_ascii=False, indent='\t').encode('utf8'))

if __name__ == '__main__':
	main()
