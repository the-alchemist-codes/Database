from ._variables import ENUM, SYS


def ObjectiveParam(json):
	this = {}  # ObjectiveParamjson)
	if 'iname' in json:
		this['iname'] = json['iname']
	this['objective'] = []
	if 'objective' in json:
		for objective in json['objective']:
			obj = {}
			if 'type' in objective:
				try:
					obj['Type'] = convertType(ENUM['EMissionType'][objective['type']], objective['val'] if 'val' in objective else 0)
				except:
					obj['Type'] = objective
			if 'val' in objective:
				obj['TypeParam'] = objective['val']
			if 'item' in objective:
				obj['item'] = objective['item']
			if 'num' in objective:
				obj['itemNum'] = objective['num']
			if 'item_type' in objective:
				obj['itemType'] = ENUM['RewardType'][objective['item_type']]
			if 'IsTakeoverProgress' in objective:
				obj['IsTakeoverProgress'] = objective['IsTakeoverProgress']
			this['objective'].append(obj)
	return this


def convertType(mtype, mTypeParam = 0):
	ret = ""
	if mtype == 'KillAllEnemy':
		ret = SYS['BONUS_KILLALL']
	elif mtype == 'NoDeath':
		ret = SYS['BONUS_NODEATH']
	elif mtype == 'LimitedTurn':
		ret = SYS['BONUS_LIMITEDTURN']
	elif mtype == 'ComboCount':
		ret = SYS['BONUS_COMBOCOUNT']
	elif mtype == 'MaxSkillCount':
		ret = SYS['BONUS_MAXSKILLCOUNT'] if mTypeParam else SYS['BONUS_NOSKILL']
	elif mtype == 'MaxItemCount':
		ret = SYS['BONUS_MAXITEMCOUNT'] if mTypeParam else SYS['BONUS_NOITEM']
	elif mtype == 'MaxPartySize':
		ret = SYS['BONUS_MAXPARTYSIZE']
	elif mtype == 'LimitedUnitElement':
		ret = SYS['BONUS_LIMITELEMENT']
	elif mtype == 'LimitedUnitID':
		ret = SYS['BONUS_LIMITUNIT']
	elif mtype == 'NoMercenary':
		ret = SYS['BONUS_NOMERCENARY']
	elif mtype == 'Killstreak':
		ret = SYS['BONUS_KILLSTREAK']
	elif mtype == 'TotalHealHPMax':
		ret = SYS['BONUS_TOTALHEALMAX'] if mTypeParam else SYS['BONUS_NOHEAL']
	elif mtype == 'TotalHealHPMin':
		ret = SYS['BONUS_TOTALHEALMIN']
	elif mtype == 'TotalDamagesTakenMax':
		ret = SYS['BONUS_TOTALDAMAGESTAKENMAX']
	elif mtype == 'TotalDamagesTakenMin':
		ret = SYS['BONUS_TOTALDAMAGESTAKENMIN']
	elif mtype == 'TotalDamagesMax':
		ret = SYS['BONUS_TOTALDAMAGESMAX']
	elif mtype == 'TotalDamagesMin':
		ret = SYS['BONUS_TOTALDAMAGESMIN']
	elif mtype == 'LimitedCT':
		ret = SYS['BONUS_CTMAX']
	elif mtype == 'LimitedContinue':
		ret = SYS['BONUS_CONTINUEMAX'] if mTypeParam else SYS['BONUS_NOCONTINUE']
	elif mtype == 'NoNpcDeath':
		ret = SYS['BONUS_NONPCDEATH']
	elif mtype == 'TargetKillstreak':
		ret = SYS['BONUS_TARGETKILLSTREAK']
	elif mtype == 'NoTargetDeath':
		ret = SYS['BONUS_NOTARGETDEATH']
	elif mtype == 'BreakObjClashMax':
		ret = SYS['BONUS_BREAKOBJCLASHMAX']
	elif mtype == 'BreakObjClashMin':
		ret = SYS['BONUS_BREAKOBJCLASHMIN']
	elif mtype == 'WithdrawUnit':
		ret = SYS['BONUS_WITHDRAWUNIT']
	elif mtype == 'UseMercenary':
		ret = SYS['BONUS_USEMERCENARY']
	elif mtype == 'LimitedUnitID_MainOnly':
		ret = SYS['BONUS_LIMITEDUNITID_MAINONLY']
	elif mtype == 'MissionAllCompleteAtOnce':
		ret = SYS['BONUS_MISSIONALLCOMPLETEATONCE']
	elif mtype == 'OnlyTargetArtifactType':
		ret = SYS['BONUS_ONLYTARGETARTIFACTTYPE']
	elif mtype == 'OnlyTargetArtifactType_MainOnly':
		ret = SYS['BONUS_ONLYTARGETARTIFACTTYPE_MAINONLY']
	elif mtype == 'OnlyTargetJobs':
		ret = SYS['BONUS_ONLYTARGETJOBS']
	elif mtype == 'OnlyTargetJobs_MainOnly':
		ret = SYS['BONUS_ONLYTARGETJOBS_MAINONLY']
	elif mtype == 'OnlyTargetUnitBirthplace':
		ret = SYS['BONUS_ONLYTARGETUNITBIRTHPLACE']
	elif mtype == 'OnlyTargetUnitBirthplace_MainOnly':
		ret = SYS['BONUS_ONLYTARGETUNITBIRTHPLACE_MAINONLY']
	elif mtype == 'OnlyTargetSex':
		ret = SYS['BONUS_ONLYTARGETSEX']
	elif mtype == 'OnlyTargetSex_MainOnly':
		ret = SYS['BONUS_ONLYTARGETSEX_MAINONLY']
	elif mtype == 'OnlyHeroUnit':
		ret = SYS['BONUS_ONLYHEROUNIT']
	elif mtype == 'OnlyHeroUnit_MainOnly':
		ret = SYS['BONUS_ONLYHEROUNIT_MAINONLY']
	elif mtype == 'Finisher':
		ret = SYS['BONUS_FINISHER']
	elif mtype == 'TotalGetTreasureCount':
		ret = SYS['BONUS_TOTALGETTREASURECOUNT']
	elif mtype == 'KillstreakByUsingTargetItem':
		ret = SYS['BONUS_KILLSTREAKBYUSINGTARGETITEM']
	elif mtype == 'KillstreakByUsingTargetSkill':
		ret = SYS['BONUS_KILLSTREAKBYUSINGTARGETSKILL']
	elif mtype == 'MaxPartySize_IgnoreFriend':
		ret = SYS['BONUS_MAXPARTYSIZE_IGNOREFRIEND']
	elif mtype == 'NoAutoMode':
		ret = SYS['BONUS_NOAUTOMODE']
	elif mtype == 'NoDeath_NoContinue':
		ret = SYS['BONUS_NODEATH_NOCONTINUE']
	elif mtype == 'OnlyTargetUnits':
		ret = SYS['BONUS_ONLYTARGETUNITS']
	elif mtype == 'OnlyTargetUnits_MainOnly':
		ret = SYS['BONUS_ONLYTARGETUNITS_MAINONLY']
	elif mtype == 'LimitedTurn_Leader':
		ret = SYS['BONUS_LIMITEDTURN_LEADER']
	elif mtype == 'NoDeathTargetNpcUnits':
		ret = SYS['BONUS_NODEATHTARGETNPCUNITS']
	elif mtype == 'UseTargetSkill':
		ret = SYS['BONUS_USETARGETSKILL']
	elif mtype == 'TotalKillstreakCount':
		ret = SYS['BONUS_TOTALKILLSTREAKCOUNT']
	elif mtype == 'TotalGetGemCount_Over':
		ret = SYS['BONUS_TOTALGETGEMCOUNT_OVER']
	elif mtype == 'TotalGetGemCount_Less':
		ret = SYS['BONUS_TOTALGETGEMCOUNT_LESS']
	elif mtype == 'TeamPartySizeMax_IncMercenary':
		ret = SYS['BONUS_TEAMPARTYSIZEMAX_INCMERCENARY']
	elif mtype == 'TeamPartySizeMax_NoMercenary':
		ret = SYS['BONUS_TEAMPARTYSIZEMAX_NOMERCENARY']
	elif mtype == 'ChallengeCountMax':
		ret = SYS['BONUS_CHALLENGECOUNT_MAX']
	elif mtype == 'DeathCountMax':
		return SYS['BONUS_DEATHCOUNTMAX']
	elif mtype == 'DamageOver':
		return SYS['BONUS_DAMAGE_OVER']
	elif mtype == 'SurviveUnit':
		return SYS['BONUS_SURVIVE_UNIT']
	elif mtype == 'KillTargetEnemy':
		return SYS['BONUS_KILL_TARGET_ENEMY']
	elif mtype == 'TakenDamageLessEqual':
		return SYS['BONUS_TAKEN_DAMAGE_LESS_EQUAL'] if mTypeParam else SYS['BONUS_NO_DAMAGE']
	elif mtype == 'TakenDamageGreatorEqual':
		return SYS['BONUS_TAKEN_DAMAGE_GREATOR_EQUAL']
	elif mtype == 'LimitedTurnLessEqualPartyOnly':
		return SYS['BONUS_LIMITED_TURN_LESS_EQUAL_PARTY_ONLY']
	elif mtype == 'LimitedTurnGreatorEqualPartyOnly':
		return SYS['BONUS_LIMITED_TURN_GREATOR_EQUAL_PARTY_ONLY']
	elif mtype == 'KillEnemy':
		return SYS['BONUS_KILL_ENEMY']
	elif mtype == 'BreakObj':
		return SYS['BONUS_BREAK_OBJ']
	elif mtype == 'LimitedTurnLessEqualPartyAndNPC':
		return SYS['BONUS_LIMITED_TURN_LESS_EQUAL_PARTY_AND_NPC']
	elif mtype == 'LimitedTurnGreatorEqualPartyAndNPC':
		return SYS['BONUS_LIMITED_TURN_GREATOR_EQUAL_PARTY_AND_NPC']
	if mTypeParam:
		entries = (mTypeParam + ',').split(',')[::-1][1:]
		for entry in entries:
			if len(entry) > 2 and entry in SYS:
				entry = SYS[entry]
		ret = ret.format(*entries)
	return ret if ret else mtype
