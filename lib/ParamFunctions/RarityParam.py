def RarityParam(json):
	this={}#RarityParamjson)
	if 'unitcap' in json:
		this['UnitLvCap'] = json['unitcap']
	if 'jobcap' in json:
		this['UnitJobLvCap'] = json['jobcap']
	if 'awakecap' in json:
		this['UnitAwakeLvCap'] = json['awakecap']
	if 'piece' in json:
		this['UnitUnlockPieceNum'] = json['piece']
	if 'ch_piece' in json:
		this['UnitChangePieceNum'] = json['ch_piece']
	if 'ch_piece_select' in json:
		this['UnitSelectChangePieceNum'] = json['ch_piece_select']
	if 'rareup_cost' in json:
		this['UnitRarityUpCost'] = json['rareup_cost']
	if 'gain_pp' in json:
		this['PieceToPoint'] = json['gain_pp']

	this['EquipEnhanceParam']={}
	if 'eq_enhcap' in json:
		this['EquipEnhanceParam']['rankcap']= json['eq_enhcap']+1
	if 'eq_costscale' in json:
		this['EquipEnhanceParam']['cost_scale'] = json['eq_costscale']

	if 'eq_item1' in json:
		this['EquipEnhanceParam']['ranks']=[
			{
				'need_point':json['eq_points'][index2],
				'return_item':[
					{
						'iname':	json['eq_item%s'%index1],
						'num':	  json['eq_num%s'%index1][index2]
					}
					for index1 in range(1,4)
				]
			}
			for index2 in range(this['EquipEnhanceParam']['rankcap'])
		]

	if 'af_lvcap' in json:
		this['ArtifactLvCap'] = json['af_lvcap']
	if 'af_upcost' in json:
		this['ArtifactCostRate'] = json['af_upcost']
	if 'af_unlock' in json:
		this['ArtifactCreatePieceNum'] = json['af_unlock']
	if 'af_gousei' in json:
		this['ArtifactGouseiPieceNum'] = json['af_gousei']
	if 'af_change' in json:
		this['ArtifactChangePieceNum'] = json['af_change']
	if 'af_unlock_cost' in json:
		this['ArtifactCreateCost'] = json['af_unlock_cost']
	if 'af_gousei_cost' in json:
		this['ArtifactRarityUpCost'] = json['af_gousei_cost']
	if 'af_change_cost' in json:
		this['ArtifactChangeCost'] = json['af_change_cost']

	this['GrowStatus']={}
	if 'hp' in json:
		this['GrowStatus']['hp'] = json['hp']
	if 'mp' in json:
		this['GrowStatus']['mp'] = json['mp']
	if 'atk' in json:
		this['GrowStatus']['atk'] = json['atk']
	if 'def' in json:
		this['GrowStatus']['def'] = json['def']
	if 'mag' in json:
		this['GrowStatus']['mag'] = json['mag']
	if 'mnd' in json:
		this['GrowStatus']['mnd'] = json['mnd']
	if 'dex' in json:
		this['GrowStatus']['dex'] = json['dex']
	if 'spd' in json:
		this['GrowStatus']['spd'] = json['spd']
	if 'cri' in json:
		this['GrowStatus']['cri'] = json['cri']
	if 'luk' in json:
		this['GrowStatus']['luk'] = json['luk']
	if 'drop' in json:
		this['DropSE'] = json['drop']
	if 'card_lvcap' in json:
		this['ConceptCardLvCap'] = json['card_lvcap']
	if 'card_awake_count' in json:
		this['ConceptCardAwakeCountMax'] = json['card_awake_count']
			#returntrue
	return this
