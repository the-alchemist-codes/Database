from .TowerRewardItem import TowerRewardItem
def TowerRoundRewardParam(json):
    this={}
    if 'iname' in json:
        this['iname'] = json['iname']
    if 'round' in json:
        this['round']=json['round']
    if 'rewards' in json:
        this['mTowerRewardItems']=[
            TowerRewardItem(reward)
            for reward in json['rewards']
        ]
    return this
