import os
import json
from lib.translation import TRANSLATION, SYS
from lib.shared import ENUM
from collections import defaultdict

# hotfixes
for param in ENUM:
	if type(ENUM[param]) == dict:
		ENUM[param] = defaultdict(lambda :None, {int(k): v for k, v in ENUM[param].items()})

RAWELEMENT = {
		0     : 'None',
		1     : 'Fire',
		2     : 'Water',
		3     : 'Wind',
		4     : 'Thunder',
		5     : 'Light',
		6     : 'Dark',
		10    : 'Water',
		100   : 'Wind',
		1000  : 'Thunder',
		10000 : 'Light',
		100000: 'Dark',
		111111: 'None',
}

RAWBIRTH = {  # ~EBirth
		"その他"      : "Foreign World",  # 0&100
		"エンヴィリア"   : "Envylia",  # 1
		"ラーストリス"   : "Wratharis",  # 2
		"スロウスシュタイン": "Slothstein",  # 3
		"ルストブルグ"   : "Lustburg",  # 4
		"グラトニー＝フォス": "Gluttony Foss",  # 5
		"グリードダイク"  : "Greed Dike",  # 6
		"サガ地方"     : "Saga Region",  # 7
		"ワダツミ"     : "Wadatsumi",  # 8
		"砂漠地帯"     : "Desert Zone",  # 9
		"ノーザンブライド" : "Northern Pride",  # 10
		"ロストブルー"   : "Lost Blue",  # 11
		"異界の巨人"    : "World of Titans",
		'異層塔'      : "Mystic Tower Veda",
		'神戸あいしす'   : 'Kobe Aisushi',  # placeholder? only one Doro enemy and 8 buffs/conds
		'神戸IJ'     : 'Kobe IJ',
		'ラットン田崎'   : 'Tasaki Kobe',
		'神戸芸人'     : 'Kobe Entertainer',
		'神戸師範'     : 'Comedian Sihan Kobe',
		'芸人'       : 'Actor',
		"ジョブ基本アビリティ" : "Job Basic Ability",
		"ジョブサポートアビリティ": "Job Support Ability",
		"ジョブ固有アビリティ": "Job Specific Ability",
		"分類なし" : "No Classification",

}

TAG_ARTIFACT = {
		"剣"         : "Sword",
		"杖"         : "Rod",
		"ローブ"       : "Robe",
		"鎧"         : "Armor",
		"具足"        : "Legguards",
		"籠手"        : "Gauntlet",
		"盾"         : "Shield",
		"アクセサリー"    : "Earring",
		"大剣"        : "Greatsword/Scythe",
		"短剣"        : "Dagger",
		"銃"         : "Gun",
		"ランス"       : "Lance",
		"ウェディングソード" : "Wedding Sword",
		"弓"         : "Bow",
		"グローブ"      : "Gloves",
		"サーベル"      : "Saber",
		"ライフル"      : "Sniper Rifle",
		"槍"         : "Spear",
		"カード"       : "Cards",
		"忍者刀"       : "Kunai",
		"チャクラム"     : "Chakram",
		"弦楽器"       : "Harp",
		"斧"         : "Axe",
		"双剣"        : "Twin Swords",
		"薬瓶"        : "Drug Vial",
		"獣"         : "Claw",
		"大麻"        : "Praying Staff",
		"鎚"         : "Hammer",
		"刀"         : "Katana",
		"ロケットランチャー" : "Rocket Launcher",
		"薙刀"        : "Halberd",
		"風水盤"       : "Feng Shui",
		"タクト"       : "Tact",
		"手袋"        : "Gloves",
		"グレネードランチャー": "Grenade Launcher",
		"長銃"        : "Rifle",  # JB_DGO
		"台本"        : "Script",  # JB_ACT_F
		"無手"        : "Paw",  # JB_DIS_CAT
		"大砲"        : "Cannon",  # JB_ART
		"銃槍"        : "Gunlance",  # JB_GUNL
		"装備なし"      : "N/A",  # JB_LUROB01
		"杖剣"        : "Swordstick",  # JB_HENC_WATER
		"カタール"      : "Qatar",  # [Default Gear] Assassin
		"ほうき"       : "Broom",  # [Default Gear] Shrine Maiden,
		"メイス"       : "Mace",  # [Default Gear] Exclusive: Salomo (Thief)
		'オーブ'		: "Orb",
		"爪"		: "Claw",
		'御祓棒'	: "Purification Rod",
  		"防具"		: "Armor",
  		'クナイ'     : "Kunai",
  		'フラスコ'   : "Flask",
  		'ハンマー'   : "Hammer",
  		'ブライダルソード'  : "Wedding Sword",
		"ガンランス"  : "Gunlance",
		"長刀"  : "Long Sword",
		'双鋭刃' : "Double Bladed Sword",
}

TAG_UNIT = {
		"人族"       : "Human",
		"男性"       : "Male",
		"女性"       : "Female",
		"モンスター"    : "Monster",
		"十戒衆"      : "Templar",
		"亜人種"      : "Demi",
		"メタル"      : "Metal",
		"異族"       : "Eldrith",
		"魔獣"       : "Beast",
		"植物"       : "Plant",
		"魔人"       : "Demon",
		"機械"       : "Machine",
		"昆虫"       : "Insect",
		"不定形"      : "Amorphous",
		"浮遊"       : "Floating",
		"FOE"      : "Enemy",
		"水棲"       : "Aquatic",
		"壊せるオブジェクト": "Breakable",
		"不死者"      : "Undead",
		"竜族"       : 'Dragon',
		"巨体"       : 'Giant',
		'雪'        : 'Snow',
		'正月'       : 'New Year',
		'神獣'       : 'Divine Beast',
		'<強欲>'     : 'Greed',
		'<嫉妬>'     : 'Envy',
		'<色欲>'     : 'Lust',
		'<憤怒>'     : 'Fury',
		'<暴食>'     : 'Gluttony',
		'<傲慢>'     : 'Pride',
		'<怠惰>'     : 'Sloth',
		'下位魔神'     : 'Low Devil',
		'バジュラ'     : 'Vajra',
		'ダークファントム' : "Dark Phantom",
		'ダーク' : 'Dark',
		'＜強欲＞'     : 'Greed',
		'＜嫉妬＞'     : 'Envy',
		'＜色欲＞'     : 'Lust',
		'＜憤怒＞'     : 'Fury',
		'＜暴食＞'     : 'Gluttony',
		'＜傲慢＞'     : 'Pride',
		'＜怠惰＞'     : 'Sloth',
		'魔動人形'     : 'Magical Doll',
		'オーク'       : 'Oak',
}
