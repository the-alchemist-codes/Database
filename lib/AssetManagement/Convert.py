import os
import threading

from PIL import Image

LOCK = threading.Lock()

LocalPath = os.path.realpath(os.path.dirname(__file__))


def getAvailableFileName(path, filename = "NONAME", extension = ""):
	# preventing duplicates for now
	global LOCK
	LOCK.acquire(True)
	if path == "":
		filename = "."
	if filename == "":
		filename = "NONAME"
	finalPath = os.path.join(path, "{}{}".format(filename, ".{}".format(extension) if (extension != "") else ""))
	LOCK.release()
	return finalPath