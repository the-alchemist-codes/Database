import os
import queue
import sys
import threading
import time
import zlib

from unitypack.unityfolder import ExportFile

from .Download import Download_Asset

IGNOR = ['StreamingAssets', 'Fonts']
errorLog = os.path.join(os.path.realpath(os.path.dirname(__file__)), "log.txt")
errorLog = open(errorLog, 'at')

# Queues
download_queue = queue.Queue()
process_queue = queue.Queue()
LOCK = threading.Lock()


class DownloadThread(threading.Thread):
	def __init__(self, queue, host, version, process = False, destFolder = ''):
		super(DownloadThread, self).__init__()
		self._stop_event = threading.Event()
		self.queue = queue
		self.daemon = True
		self.host = host
		self.version = version
		self.process = process
		self.destFolder = destFolder
		if destFolder:
			os.makedirs(destFolder, exist_ok = True)
	
	def stop(self):
		self._stop_event.set()
	
	def run(self):
		while True:
			if process_queue.qsize() > 50:
				time.sleep(2)
				continue
			try:
				url = self.queue.get()
				print('DQueue: %s,\t%s' % (download_queue.qsize(), url['path']))
				self.download_url(url)
			except queue.Empty:
				break
			except Exception as e:
				print("Download Error: %s" % e)
			# self.queue.put(url)
			
			self.queue.task_done()
			if self.queue.empty():
				break
		self.stop()
	
	def download_url(self, asset):
		# print ('%d Downloading: %s'%(self.ident,asset['path']))
		if self.process:
			data = Download_Asset(self.host, self.version, asset, 'aatc')
			process_queue.put((asset, data))
		else:
			data = Download_Asset(self.host, self.version, asset, 'aatc', False)
			fpath = os.path.join(self.destFolder, asset['id'])
			with open(fpath, 'wb') as fh:
				fh.write(data)
			if 'Compressed' in asset['flags']:
				data = zlib.decompress(data)
			process_queue.put((asset, data))


class ProcessThread(threading.Thread):
	def __init__(self, queue, destFolder):
		super(ProcessThread, self).__init__()
		self._stop_event = threading.Event()
		self.queue = queue
		self.daemon = True
		self.destFolder = destFolder
	
	def stop(self):
		self._stop_event.set()
	
	def run(self):
		while True:
			try:
				(asset, data) = self.queue.get(timeout = 1)
				try:
					if 'IsCombined' in asset['flags']:
						pass
					else:
						print('PQueue: %s,\t%s' % (process_queue.qsize(), asset['path']))
						ExportFile(data, os.path.join(self.destFolder, *asset['path'].split('/')))
				except Exception as e:
					print("Error Processing: %s\n%s" % (asset['path'], e))
				self.queue.task_done()
				
				if self.queue.empty() and download_queue.empty():
					break
			
			except queue.Empty:
				if download_queue.qsize == 0:
					break
			except Exception as e:
				print('Process Get Error: %s' % e)
		
		try:
			sys.exit(1)
		except:
			pass
