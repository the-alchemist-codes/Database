import os
import sys
import  multiprocessing
import time
import zlib

from unitypack.unityfolder import ExportFile
from .Download import Download_Asset

IGNOR = ['StreamingAssets', 'Fonts']
errorLog = os.path.join(os.path.realpath(os.path.dirname(__file__)), "log.txt")
errorLog = open(errorLog, 'at')

# Queues
download_queue = multiprocessing.JoinableQueue()
process_queue = multiprocessing.JoinableQueue()
LOCK = multiprocessing.Lock()

class DownloadNProcess(multiprocessing.Process):
	def __init__(self, queue, host, version,  rawFolder = '', destFolder = '', typ = 'aatc'):
		super(DownloadNProcess, self).__init__()
		self.queue = queue
		self.daemon = True
		self.host = host
		self.version = version
		self.rawFolder = rawFolder
		self.destFolder = destFolder
		self.typ = typ
		if destFolder:
			os.makedirs(destFolder, exist_ok = True)	

	def run(self):
		while not self.queue.empty():
			try:
				asset = self.queue.get()
				# Download
				data = Download_Asset(self.host, self.version, asset, self.typ, False)
				# Save raw
				fpath = os.path.join(self.rawFolder, asset['id'])
				with open(fpath, 'wb') as fh:
					fh.write(data)
				# Process
				if 'Compressed' in asset['flags']:
					data = zlib.decompress(data)
				if 'IsCombined' in asset['flags']:
					pass
				else:
					try:
						ExportFile(data, os.path.join(self.destFolder, *asset['path'].split('/')))
					except Exception as e:
						print("Error Processing: %s\n%s" % (asset['path'], e))
			except multiprocessing.queues.Empty:
				break
			except Exception as e:
				print("Download Error: %s" % e)
			
			self.queue.task_done()
			if self.queue.empty():
				break
		try:
			sys.exit(1)
		except:
			pass