from .shared import respath
import msgpack
import json
import zlib
import re
import os

from .compendium import compendium as init_compendium
compendium = init_compendium()
reJP = re.compile(r'([\u3041-\u3096\u30A0-\u30FF\u3400-\u4DB5\u4E00-\u9FCB\uF900-\uFA6A\u2E80-\u2FD5\uFF5F-\uFF9F\u3000-\u303F\u31F0-\u31FF\u3220-\u3243\u3280-\u337F\uFF01-\uFF5E]+)')

loc_files = [
    'LocalizedMasterParam',
    'LocalizedQuestParam',
    'sys',
    'unit',
    'help',
    'quest',
    'quest_info',
    'errorcode',
    'ConceptCard',
    'Secretshop',
    'external_artifact',
    'external_conceptcard',
    'external_item',
    'external_skill',
    'LocalizedUnitTag',
    'mail',
    'LocalizedNotification'
]

def apply_loc(master, GL, JP):
    print('Downloading external translation files')
    try:
        tproject = json.loads(open(os.path.join(respath,'translation_project_core.json'),'rb').read())
    except:
        tproject = {}

    print('Generating Kanji Translation Table')
    KANJI = generate_kanji_table(GL, JP, tproject)
    
    # data patch
    used_keys = {}
    allowed_keys = ['name','expr','spec','title','cond','nil']
    # apply normal patch
    locm = load_loc_master(GL, 'Loc/english/LocalizedMasterParam', False)
    locq = load_loc_master(GL, 'Loc/english/LocalizedQuestParam', False)
    for main, values in master.items():
        if not isinstance(values, dict):
            continue
        key0 = list(values.keys())[0]
        if not isinstance(values[key0], dict) or 'iname' not in values[key0]:
            continue
        for key,item in values.items():
            if item['iname'] in locm:
                item.update(locm[item['iname']])
            elif item['iname'] in locm:
                item.update(locm[item['iname']])
            else:
                for key, value in item.items():
                    if key in allowed_keys and isinstance(value, str) and len(value) > 2 and reJP.match(value) and value in KANJI:
                        item[key] = KANJI[value]
                        if key not in used_keys:
                            used_keys[key] = (main, item['iname'], key, value, KANJI[value])

    # patch unit names and or duplicates


reLoc = re.compile(r'(.+?)\t(.+?)(\t(.+?))?\r?\n')


def load_loc(am, name: str, SRPG=True) -> dict:
    data = am.load(name)
    for encoding in ['utf-8-sig', 'utf16']:
        try:
            data = data.decode(encoding)
            break
        except UnicodeDecodeError:
            pass

    return {
        match[1].lstrip('\x08'): match[2]
        for match in reLoc.finditer(data + '\n')  # appended newline as regex last line fix
    }


reLoc_Master = re.compile(r'(SRPG_.+?Param_)?(.+?)\t(.+?)\r?\n')


def load_loc_master(am, name: str, SRPG=True) -> dict:
    data = am.load(name)
    for encoding in ['utf-8-sig', 'utf16']:
        try:
            data = data.decode(encoding)
            break
        except UnicodeDecodeError:
            pass

    ret = {}
    for match in reLoc_Master.finditer(data + '\n'):
        iname, key = match[2].lstrip('\x08').rsplit('_', 1)
        value = match[3]
        if iname not in ret:
            ret[iname] = {}
        ret[iname][key.lower()] = value
    return ret


def load_master(am, name: str) -> dict:
    data = am.load(name)
    return json.loads(data)


def generate_kanji_table(GL, JP, tproject):
    table = {}
    # connection kanji <-> english is required
    # achieved via same iname
    # therefore following connections are possible
    # gl loc - jp loc
    # MasterParam <-> LocalizedMasterParam
    # global translation is treated with priority

    # MasterParam
    master = load_master(JP, 'Data/MasterParam')
    #  load localization
    loc = load_loc_master(GL, 'Loc/english/LocalizedMasterParam', False)
    for main, values in master.items():
        if isinstance(values, list) and isinstance(values[0], dict) and 'iname' in values[0]:
            for item in values:
                iname = item['iname']

                if iname in loc:
                    for key, english in loc[iname].items():
                        if english and key in item and item[key] not in table:
                            table[item[key]] = english

                if iname in compendium:
                    table[item['name']] = compendium[iname]

    # QuestParam
    master = load_master(JP, 'Data/QuestParam')
    # load localization
    loc = load_loc_master(GL, 'Loc/english/LocalizedQuestParam', False)
    for main, values in master.items():
        if isinstance(values, list) and isinstance(values[0], dict) and 'iname' in values[0]:
            for item in values:
                if item['iname'] in loc:
                    for key, english in loc[item['iname']].items():
                        if english and key in item and item[key] not in table:
                            table[item[key]] = english

    # loc files
    for f in loc_files:
        try:
            # load original
            jp = load_loc(JP, 'Loc/japanese/' + f)
            # load global
            gl = load_loc(GL, 'Loc/english/' + f)
            # generate table from global <-> japan
            for iname, kanji in jp.items():
                if kanji not in table:
                    if iname in gl:
                        table[kanji] = gl[iname]
            # load translation project
            if f in tproject:
                pj = {
                    item['system'] : item['english']
                    for item in parse_sheet(tproject[f])
                    if 'english' in item and item['english'].replace(' ','')
                }
                # add to table from project <-> japan
                for iname, kanji in jp.items():
                    if kanji not in table:
                        if iname in pj:
                            table[kanji] = pj[iname]
        except FileNotFoundError:
            pass
    return table

if __name__ == '__main__':
    main()
