import json
import os

from .shared import respath


def createAssetTable(whitelist = [], save = False):
	path = respath
	path_gl = os.path.join(path, 'AssetsGL')
	path_jp = os.path.join(path, 'AssetsJP')
	path_concept = os.path.join(path, 'ConceptArt')
	
	# [val for sublist in [[os.path.join(i[0], j) for j in i[2]] for i in os.walk('./')] for val in sublist]
	# files_gl=[val for sublist in [[os.path.join(i[0], j).split('resources')[-1].replace('\\','/') for j in i[2]] for i in os.walk(path_gl)] for val in sublist]
	# files_jp=[val for sublist in [[os.path.join(i[0], j).split('resources')[-1].replace('\\','/') for j in i[2]] for i in os.walk(path_jp)] for val in sublist]
	# find files which are missing in global
	def listFiles(directory):
		return [
				val for sublist in [
						[
								os.path.join(dirpath, filename).split('resources')[-1].replace('\\', '/')
								for filename in filenames
								if not whitelist or any([filt in filename for filt in whitelist])
						]
						for (dirpath, dirnames, filenames) in os.walk(directory)
						if '.git' not in dirpath
				]
				for val in sublist
		]
	
	# list files
	files_gl = listFiles(path_gl)
	files_jp = listFiles(path_jp)
	# find files which are missing in global
	# https://gitlab.com/K0lb3/TAC_Database/raw/master/resources/Assets/ArtiIcon/AF_ACCS_100CP_01.png
	# d:\\Projects\\TAC_Database\\resources\\Assets\\AbilityIcon\\AB_FATE_LAN_1.png
	files = {'ConceptArt': listFiles(path_concept)}
	for f in files_jp:
		ind = f.find('/', 2)
		files[f[ind:]] = f[:ind]
	for f in files_gl:
		ind = f.find('/', 2)
		files[f[ind:]] = f[:ind]
	
	if save:
		with open(os.path.join(path, 'AssetIndex.json'), 'wb') as f:
			f.write(json.dumps(files, ensure_ascii = False, ).encode('utf8'))
	return files


if __name__ == '__main__':
	createAssetTable(save = True)
