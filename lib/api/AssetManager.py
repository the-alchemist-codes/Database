import io
import struct
import urllib.request
import zlib
import os
import json
import msgpack
from .EncryptionHelper import *
import sys
from ._shared import DownloadWithBar

TEXT_FORMATS =  ['utf-8-sig', 'utf-16-bom', 'utf16', 'utf8','ascii', 'SHIFT_JIS']

class AssetManager():
	def __init__(self, path, enviroment, typ = 'aatc'):
		self.path = path
		self.host = enviroment.DLHost
		self.version = enviroment.Assets
		self.masterdigest = getattr(enviroment,'MasterDigest',None)
		self.questdigest = getattr(enviroment,'QuestDigest',None)
		self.typ = typ
		assetlist_bytes = self.download('ASSETLIST')
		self.assetlist = parse_assetlist(assetlist_bytes)['assets']
		self.EncryptionHelper = EncryptionHelper({})

		os.makedirs(self.path, exist_ok = True)
	
	def __getitem__(self, key):
		return Asset(key, self.assetlist[key], self)
	
	def download(self, name):
		url = '{host_dl}/assets/{version}/{typ}/{name}'.format(
				host_dl = self.host,
				version = self.version,
				typ = self.typ,
				name = name
		)
		print(url)
		#data = urllib.request.urlopen(url, timeout = 300).read()
		return DownloadWithBar(url)


class Asset(object):
	id = ""
	name = ""
	size = 0
	flags = []

	def __init__(self, key, asset: dict, assetmanager: AssetManager):
		self.__dict__.update(asset)
		self.name = key
		self.manager = assetmanager
		self.path = os.path.join(self.manager.path, self.id)
	
	def download(self):
		data = self.manager.download(self.id)
		return data
	
	def load(self, typ = None):
		if os.path.isfile(self.path) and self.size == os.path.getsize(self.path):
			data = open(self.path, 'rb').read()
		else:
			data = self.download()
			open(self.path, 'wb').write(data)
		if 'Compressed' in self.flags:
			data = zlib.decompress(data)
		return data
	
	def load_txt(self, encoding = None):
		data = self.load()
		if encoding:
			return data.decode(encoding)
		for encoding in TEXT_FORMATS:
			try:
				return data.decode(encoding)
			except:
				pass

	def load_json(self, encoding = None):
		data = self.load()
		return json.loads(data)
			
	def load_master(self, digest = None):
		if 'Master' in self.name:
			digest = self.manager.masterdigest
		elif 'Quest' in self.name:
			digest = self.manager.questdigest

		data = self.manager.EncryptionHelper.Decrypt(KeyType.APP, self.load(), digest, DecryptOptions.IsFile)
		try:
			return msgpack.unpackb(data, raw=False)
		except:
			return json.loads(data)
	
	
def parse_assetlist(fh):
	if type(fh) == bytes:
		fh = io.BytesIO(fh)
	
	def readInt32(fh):
		return struct.unpack("i", fh.read(4))[0]
	
	def readUInt32(fh):
		return struct.unpack("I", fh.read(4))[0]
	
	def readString(fh):
		res = ""
		control = "1"
		while control == "1":
			bit_str = bin(ord(fh.read(1)))[2:].zfill(8)
			control = bit_str[0]
			res += bit_str[1:]
		
		length = int(res, 2)
		return struct.unpack("{}s".format(length), fh.read(length))[0].decode("utf8")
	
	bundleFlags = ["Compressed", "RawData", "Required", "Scene", "Tutorial", "Multiplay", "StreamingAsset", "TutorialMovie", "Persistent", "DiffAsset", None, None, "IsLanguage", "IsCombined",
	               "IsFolder"]
	
	assetlist = {
			'revision': readInt32(fh),
			'length'  : readInt32(fh),
			'assets'  : {}
	}
	for i in range(assetlist['length']):
		cAsset = {
				"id"                             : "%08x" % readUInt32(fh),
				"size"                           : readInt32(fh),
				"compSize"                       : readInt32(fh),
				"path"                           : readString(fh),
				"pathHash"                       : readInt32(fh),
				"hash"                           : readUInt32(fh),
				"flags"                          : [
						bundleFlags[index]
						for index, bit in enumerate(bin(readInt32(fh))[2:][::-1])  # flags
						if bit == "1"
				],
				"dependencies"                   : [
						readInt32(fh)
						for j in range(readInt32(fh))  # array length
				],
				"additionalDependencies"         : [
						readInt32(fh)  # array length
						for j in range(readInt32(fh))
				],
				"additionalStreamingDependencies": [
						readInt32(fh)  # array length
						for j in range(readInt32(fh))
				],
		}
		assetlist['assets'][cAsset['path']] = cAsset
	return assetlist
