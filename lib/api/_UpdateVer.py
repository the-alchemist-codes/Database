import os, io
import http.client
import urllib.request
import zipfile
import unitypack
from _shared import respath

def UpdateJapanVer():
	QooApp_Download_APK('jp.co.gu3.alchemist', respath)
	fp = os.path.join(respath, 'jp.co.gu3.alchemist.apk')
	z = zipfile.ZipFile(fp)
	bundle = unitypack.load(z.open('assets/bin/Data/data.unity3d'))
	for asset in bundle.assets:
		if asset.name == 'resources.assets':
			ret = get_networkver(asset)
			if ret:
				print('Japan networkver:', ret)
				UpdateVersion(ret)
				break

def get_networkver(asset):
	for _id, obj in asset.objects.items():
		if obj.type == 'TextAsset':
			data = obj.read()
			if data.name == 'networkver':
				return data._obj['m_Script']
	return None

def UpdateVersion(ver):
	open(os.path.join(respath,'version.txt'),'wt').write(ver)


def QooApp_Download_APK(apk, download_folder = None):
	con = http.client.HTTPSConnection('api.qoo-app.com')
	con.connect()
	con.request("GET", f'/v6/apps/{apk}/download')
	res = con.getresponse()
	con.close()
	download_url = res.headers['Location']
	data =  urllib.request.urlopen(download_url,timeout=300).read()#Download_With_Bar(download_url)  #
	if not download_folder:
		download_folder = os.path.abspath('.')
	open(os.path.join(download_folder, f'{apk}.apk'), 'wb').write(data)

if __name__ == "__main__":
	UpdateJapanVer()