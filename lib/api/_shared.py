import os

from lib.shared import respath as resPath


def ParseSheet(sheet):
	return [
			{
					sheet[0][i].lower(): cell.lstrip('\n').rstrip('\n').lstrip(' ').rstrip(' ')
					for i, cell in enumerate(row)
					if cell and type(cell) == str
			}
			for row in sheet[1:]
	]


def DownloadWithBar(url):
	response = urllib.request.urlopen(url)
	length = response.getheader('content-length')

	if length:
		length = int(length)
		blocksize = max(4096, length // 100)
	else:
		blocksize = 1000000  # just made something up

	ret = b''
	downloaded = 0
	while True:
		data = response.read(blocksize)
		if not data:
			break
		downloaded += len(data)
		ret += data
		done = int(50*downloaded/length)
		sys.stdout.write(f"\r[{'█' * done}{'.' * (50-done)}]\t{downloaded//256*8}/{length//256*8}")
		sys.stdout.flush()
	print()
	return ret