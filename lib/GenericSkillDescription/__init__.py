from .Typ import Passive, Skill, Reaction


def GenericSkillDescription(skill, master):
	typ = skill['type'] if 'type' in skill else 'no_type'
	eff = skill['effect_type'] if 'effect_type' in skill else 'no_effect_type'
	
	if typ == 'Passive':
		return Passive(skill, master)
	elif typ == 'Reaction':
		return Reaction(skill, master)
	else:
		return Skill(skill, master)
