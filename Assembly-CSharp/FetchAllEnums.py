import os
import json
import re

reEnumName  = re.compile(r'\s*public enum (\w+?)(\s*:\s*\w+?)?\n?$')
reEnumEntry = re.compile(r'\s*(\w+?)( =\s*(.+?)L?)?[;,\n]',re.S|re.M)

def EnumToJson():
    # path to files
    path=os.path.dirname(os.path.realpath(__file__))
    mypath = os.path.join(path,'Japan','SRPG')
    files = os.listdir(mypath)

    # enum
    enum = {}

    for f in files:
        try:
            with open(os.path.join(mypath,f), "rt", encoding='utf8') as f:
                for line in f:
                    match = reEnumName.match(line)
                    if match:
                        ename = match[1]
                        enum[ename] = {}
                        f.readline()#{

                        i = 0
                        for line in f:
                            if '}' in line:
                                break
                            match = reEnumEntry.match(line)
                            if match:
                                # 1 - name, 3 number?
                                if match[3]:
                                    num = int(match[3]) if '0x' != match[3][:2] else int(match[3],16)
                                else:
                                    num = i
                                    i+=1
                                enum[ename][num]=match[1]
        except Exception as e:
            print(f)
            print(e)

    os.makedirs(path, exist_ok=True)
    name=os.path.join(path, 'Enums.json')
    with open(name, "wb") as f:
        f.write(json.dumps(enum, indent=4, ensure_ascii=False, sort_keys=True).encode('utf8'))

EnumToJson()