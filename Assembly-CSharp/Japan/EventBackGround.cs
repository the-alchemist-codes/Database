﻿// Decompiled with JetBrains decompiler
// Type: EventBackGround
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EventBackGround : MonoBehaviour
{
  public static List<EventBackGround> Instances = new List<EventBackGround>();
  public bool mClose;

  public EventBackGround()
  {
    base.\u002Ector();
  }

  public static EventBackGround Find()
  {
    foreach (EventBackGround instance in EventBackGround.Instances)
    {
      if (Object.op_Inequality((Object) instance, (Object) null))
        return instance;
    }
    return (EventBackGround) null;
  }

  public static void DiscardAll()
  {
    foreach (EventBackGround instance in EventBackGround.Instances)
    {
      if (!((Component) instance).get_gameObject().get_activeInHierarchy())
        Object.Destroy((Object) ((Component) instance).get_gameObject());
    }
    EventBackGround.Instances.Clear();
  }

  private void Awake()
  {
    EventBackGround.Instances.Add(this);
  }

  private void OnDestroy()
  {
    RawImage component = (RawImage) ((Component) this).GetComponent<RawImage>();
    if (Object.op_Inequality((Object) component, (Object) null))
    {
      component.set_texture((Texture) null);
      Object.Destroy((Object) component);
    }
    EventBackGround.Instances.Remove(this);
  }

  public void Open()
  {
    ((Component) this).get_gameObject().SetActive(true);
    this.mClose = false;
  }

  public void Close()
  {
    ((Component) this).get_gameObject().SetActive(false);
    this.mClose = true;
  }
}
