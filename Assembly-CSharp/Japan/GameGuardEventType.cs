﻿// Decompiled with JetBrains decompiler
// Type: GameGuardEventType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

public class GameGuardEventType
{
  public class S2Auth
  {
    public const int S2AUTH_RESULT_SUCCESS = 1;
    public const int S2AUTH_RESULT_RETRY = 2;
    public const int S2AUTH_RESULT_FAIL = 3;
  }

  public class Detect
  {
    public const int DETECT_RUNNING_BAD_APPLICATION = 21;
  }
}
