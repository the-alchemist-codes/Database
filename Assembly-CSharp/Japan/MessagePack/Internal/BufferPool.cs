﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Internal.BufferPool
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Internal
{
  internal sealed class BufferPool : ArrayPool<byte>
  {
    public static readonly BufferPool Default = new BufferPool((int) ushort.MaxValue);

    public BufferPool(int bufferLength)
      : base(bufferLength)
    {
    }
  }
}
