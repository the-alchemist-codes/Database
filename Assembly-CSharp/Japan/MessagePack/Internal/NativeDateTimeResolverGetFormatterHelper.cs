﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Internal.NativeDateTimeResolverGetFormatterHelper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack.Formatters;
using System;

namespace MessagePack.Internal
{
  internal static class NativeDateTimeResolverGetFormatterHelper
  {
    internal static object GetFormatter(Type t)
    {
      if (t == typeof (DateTime))
        return (object) NativeDateTimeFormatter.Instance;
      if (t == typeof (DateTime?))
        return (object) new StaticNullableFormatter<DateTime>((IMessagePackFormatter<DateTime>) NativeDateTimeFormatter.Instance);
      return t == typeof (DateTime[]) ? (object) NativeDateTimeArrayFormatter.Instance : (object) null;
    }
  }
}
