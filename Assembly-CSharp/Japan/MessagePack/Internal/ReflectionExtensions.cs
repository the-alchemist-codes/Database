﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Internal.ReflectionExtensions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace MessagePack.Internal
{
  internal static class ReflectionExtensions
  {
    public static bool IsNullable(this TypeInfo type)
    {
      return type.IsGenericType && type.GetGenericTypeDefinition() == typeof (Nullable<>);
    }

    public static bool IsPublic(this TypeInfo type)
    {
      return type.IsPublic;
    }

    public static bool IsAnonymous(this TypeInfo type)
    {
      if (type.GetCustomAttribute<CompilerGeneratedAttribute>(true) == null || !type.IsGenericType || !type.Name.Contains("AnonymousType") || !type.Name.StartsWith("<>") && !type.Name.StartsWith("VB$"))
        return false;
      int attributes = (int) type.Attributes;
      return true;
    }

    public static bool IsIndexer(this PropertyInfo propertyInfo)
    {
      return propertyInfo.GetIndexParameters().Length > 0;
    }
  }
}
