﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Internal.AnonymousDeserializeFunc`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Internal
{
  internal delegate T AnonymousDeserializeFunc<T>(
    object[] customFormatters,
    byte[] bytes,
    int offset,
    IFormatterResolver resolver,
    out int readSize);
}
