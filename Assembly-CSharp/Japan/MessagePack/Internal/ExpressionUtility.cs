﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Internal.ExpressionUtility
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using System.Linq.Expressions;
using System.Reflection;

namespace MessagePack.Internal
{
  public static class ExpressionUtility
  {
    private static MethodInfo GetMethodInfoCore(LambdaExpression expression)
    {
      if (expression == null)
        throw new ArgumentNullException(nameof (expression));
      return (expression.Body as MethodCallExpression).Method;
    }

    public static MethodInfo GetMethodInfo<T>(Expression<Func<T>> expression)
    {
      return ExpressionUtility.GetMethodInfoCore((LambdaExpression) expression);
    }

    public static MethodInfo GetMethodInfo(Expression<Action> expression)
    {
      return ExpressionUtility.GetMethodInfoCore((LambdaExpression) expression);
    }

    public static MethodInfo GetMethodInfo<T, TR>(Expression<Func<T, TR>> expression)
    {
      return ExpressionUtility.GetMethodInfoCore((LambdaExpression) expression);
    }

    public static MethodInfo GetMethodInfo<T>(Expression<Action<T>> expression)
    {
      return ExpressionUtility.GetMethodInfoCore((LambdaExpression) expression);
    }

    public static MethodInfo GetMethodInfo<T, TArg1, TR>(
      Expression<Func<T, TArg1, TR>> expression)
    {
      return ExpressionUtility.GetMethodInfoCore((LambdaExpression) expression);
    }

    private static MemberInfo GetMemberInfoCore<T>(Expression<T> source)
    {
      if (source == null)
        throw new ArgumentNullException(nameof (source));
      return (source.Body as MemberExpression).Member;
    }

    public static PropertyInfo GetPropertyInfo<T, TR>(Expression<Func<T, TR>> expression)
    {
      return ExpressionUtility.GetMemberInfoCore<Func<T, TR>>(expression) as PropertyInfo;
    }

    public static FieldInfo GetFieldInfo<T, TR>(Expression<Func<T, TR>> expression)
    {
      return ExpressionUtility.GetMemberInfoCore<Func<T, TR>>(expression) as FieldInfo;
    }
  }
}
