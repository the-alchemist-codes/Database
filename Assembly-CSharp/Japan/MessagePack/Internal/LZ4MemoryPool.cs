﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Internal.LZ4MemoryPool
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack.LZ4;
using System;

namespace MessagePack.Internal
{
  internal static class LZ4MemoryPool
  {
    [ThreadStatic]
    private static byte[] lz4buffer;

    public static byte[] GetBuffer()
    {
      if (LZ4MemoryPool.lz4buffer == null)
        LZ4MemoryPool.lz4buffer = new byte[LZ4Codec.MaximumOutputLength(65536)];
      return LZ4MemoryPool.lz4buffer;
    }
  }
}
