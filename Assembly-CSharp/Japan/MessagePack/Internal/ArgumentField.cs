﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Internal.ArgumentField
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using System.Reflection;
using System.Reflection.Emit;

namespace MessagePack.Internal
{
  internal struct ArgumentField
  {
    private readonly int i;
    private readonly bool @ref;
    private readonly ILGenerator il;

    public ArgumentField(ILGenerator il, int i, bool @ref = false)
    {
      this.il = il;
      this.i = i;
      this.@ref = @ref;
    }

    public ArgumentField(ILGenerator il, int i, Type type)
    {
      this.il = il;
      this.i = i;
      TypeInfo typeInfo = type.GetTypeInfo();
      this.@ref = !typeInfo.IsClass && !typeInfo.IsInterface && !typeInfo.IsAbstract;
    }

    public void EmitLoad()
    {
      if (this.@ref)
        this.il.EmitLdarga(this.i);
      else
        this.il.EmitLdarg(this.i);
    }

    public void EmitLdarg()
    {
      this.il.EmitLdarg(this.i);
    }

    public void EmitLdarga()
    {
      this.il.EmitLdarga(this.i);
    }

    public void EmitStore()
    {
      this.il.EmitStarg(this.i);
    }
  }
}
