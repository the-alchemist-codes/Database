﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Internal.AnonymousSerializeFunc`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Internal
{
  internal delegate int AnonymousSerializeFunc<T>(
    byte[][] stringByteKeysField,
    object[] customFormatters,
    ref byte[] bytes,
    int offset,
    T value,
    IFormatterResolver resolver);
}
