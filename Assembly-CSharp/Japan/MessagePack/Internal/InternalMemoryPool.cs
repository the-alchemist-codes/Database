﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Internal.InternalMemoryPool
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace MessagePack.Internal
{
  internal static class InternalMemoryPool
  {
    [ThreadStatic]
    private static byte[] buffer;

    public static byte[] GetBuffer()
    {
      if (InternalMemoryPool.buffer == null)
        InternalMemoryPool.buffer = new byte[65536];
      return InternalMemoryPool.buffer;
    }
  }
}
