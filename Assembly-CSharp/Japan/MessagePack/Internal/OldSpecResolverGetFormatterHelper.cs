﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Internal.OldSpecResolverGetFormatterHelper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack.Formatters;
using System;

namespace MessagePack.Internal
{
  internal static class OldSpecResolverGetFormatterHelper
  {
    internal static object GetFormatter(Type t)
    {
      if (t == typeof (string))
        return (object) OldSpecStringFormatter.Instance;
      if (t == typeof (string[]))
        return (object) new ArrayFormatter<string>();
      return t == typeof (byte[]) ? (object) OldSpecBinaryFormatter.Instance : (object) null;
    }
  }
}
