﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.CharFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Formatters
{
  public sealed class CharFormatter : IMessagePackFormatter<char>, IMessagePackFormatter
  {
    public static readonly CharFormatter Instance = new CharFormatter();

    private CharFormatter()
    {
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      char value,
      IFormatterResolver formatterResolver)
    {
      return MessagePackBinary.WriteChar(ref bytes, offset, value);
    }

    public char Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      return MessagePackBinary.ReadChar(bytes, offset, out readSize);
    }
  }
}
