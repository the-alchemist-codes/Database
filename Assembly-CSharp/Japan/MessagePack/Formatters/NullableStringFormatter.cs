﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.NullableStringFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Formatters
{
  public sealed class NullableStringFormatter : IMessagePackFormatter<string>, IMessagePackFormatter
  {
    public static readonly NullableStringFormatter Instance = new NullableStringFormatter();

    private NullableStringFormatter()
    {
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      string value,
      IFormatterResolver typeResolver)
    {
      return MessagePackBinary.WriteString(ref bytes, offset, value);
    }

    public string Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver typeResolver,
      out int readSize)
    {
      return MessagePackBinary.ReadString(bytes, offset, out readSize);
    }
  }
}
