﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.ByteArrayFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Formatters
{
  public sealed class ByteArrayFormatter : IMessagePackFormatter<byte[]>, IMessagePackFormatter
  {
    public static readonly ByteArrayFormatter Instance = new ByteArrayFormatter();

    private ByteArrayFormatter()
    {
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      byte[] value,
      IFormatterResolver formatterResolver)
    {
      return MessagePackBinary.WriteBytes(ref bytes, offset, value);
    }

    public byte[] Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      return MessagePackBinary.ReadBytes(bytes, offset, out readSize);
    }
  }
}
