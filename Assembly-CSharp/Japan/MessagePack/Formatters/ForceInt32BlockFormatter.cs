﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.ForceInt32BlockFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Formatters
{
  public sealed class ForceInt32BlockFormatter : IMessagePackFormatter<int>, IMessagePackFormatter
  {
    public static readonly ForceInt32BlockFormatter Instance = new ForceInt32BlockFormatter();

    private ForceInt32BlockFormatter()
    {
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      int value,
      IFormatterResolver formatterResolver)
    {
      return MessagePackBinary.WriteInt32ForceInt32Block(ref bytes, offset, value);
    }

    public int Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      return MessagePackBinary.ReadInt32(bytes, offset, out readSize);
    }
  }
}
