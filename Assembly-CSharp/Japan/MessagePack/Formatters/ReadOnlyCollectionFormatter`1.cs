﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.ReadOnlyCollectionFormatter`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace MessagePack.Formatters
{
  public sealed class ReadOnlyCollectionFormatter<T> : CollectionFormatterBase<T, T[], ReadOnlyCollection<T>>
  {
    protected override void Add(T[] collection, int index, T value)
    {
      collection[index] = value;
    }

    protected override ReadOnlyCollection<T> Complete(T[] intermediateCollection)
    {
      return new ReadOnlyCollection<T>((IList<T>) intermediateCollection);
    }

    protected override T[] Create(int count)
    {
      return new T[count];
    }
  }
}
