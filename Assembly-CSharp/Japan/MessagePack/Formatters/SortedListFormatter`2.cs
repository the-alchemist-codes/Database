﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.SortedListFormatter`2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace MessagePack.Formatters
{
  public sealed class SortedListFormatter<TKey, TValue> : DictionaryFormatterBase<TKey, TValue, SortedList<TKey, TValue>, SortedList<TKey, TValue>>
  {
    protected override void Add(
      SortedList<TKey, TValue> collection,
      int index,
      TKey key,
      TValue value)
    {
      collection.Add(key, value);
    }

    protected override SortedList<TKey, TValue> Complete(
      SortedList<TKey, TValue> intermediateCollection)
    {
      return intermediateCollection;
    }

    protected override SortedList<TKey, TValue> Create(int count)
    {
      return new SortedList<TKey, TValue>(count);
    }
  }
}
