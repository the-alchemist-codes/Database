﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.ForceInt64BlockFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Formatters
{
  public sealed class ForceInt64BlockFormatter : IMessagePackFormatter<long>, IMessagePackFormatter
  {
    public static readonly ForceInt64BlockFormatter Instance = new ForceInt64BlockFormatter();

    private ForceInt64BlockFormatter()
    {
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      long value,
      IFormatterResolver formatterResolver)
    {
      return MessagePackBinary.WriteInt64ForceInt64Block(ref bytes, offset, value);
    }

    public long Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      return MessagePackBinary.ReadInt64(bytes, offset, out readSize);
    }
  }
}
