﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.VersionFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace MessagePack.Formatters
{
  public sealed class VersionFormatter : IMessagePackFormatter<Version>, IMessagePackFormatter
  {
    public static readonly IMessagePackFormatter<Version> Instance = (IMessagePackFormatter<Version>) new VersionFormatter();

    private VersionFormatter()
    {
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      Version value,
      IFormatterResolver formatterResolver)
    {
      return value == (Version) null ? MessagePackBinary.WriteNil(ref bytes, offset) : MessagePackBinary.WriteString(ref bytes, offset, value.ToString());
    }

    public Version Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      if (!MessagePackBinary.IsNil(bytes, offset))
        return new Version(MessagePackBinary.ReadString(bytes, offset, out readSize));
      readSize = 1;
      return (Version) null;
    }
  }
}
