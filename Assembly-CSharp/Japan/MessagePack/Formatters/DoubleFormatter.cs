﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.DoubleFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Formatters
{
  public sealed class DoubleFormatter : IMessagePackFormatter<double>, IMessagePackFormatter
  {
    public static readonly DoubleFormatter Instance = new DoubleFormatter();

    private DoubleFormatter()
    {
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      double value,
      IFormatterResolver formatterResolver)
    {
      return MessagePackBinary.WriteDouble(ref bytes, offset, value);
    }

    public double Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      return MessagePackBinary.ReadDouble(bytes, offset, out readSize);
    }
  }
}
