﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.SortedDictionaryFormatter`2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace MessagePack.Formatters
{
  public sealed class SortedDictionaryFormatter<TKey, TValue> : DictionaryFormatterBase<TKey, TValue, SortedDictionary<TKey, TValue>, SortedDictionary<TKey, TValue>>
  {
    protected override void Add(
      SortedDictionary<TKey, TValue> collection,
      int index,
      TKey key,
      TValue value)
    {
      collection.Add(key, value);
    }

    protected override SortedDictionary<TKey, TValue> Complete(
      SortedDictionary<TKey, TValue> intermediateCollection)
    {
      return intermediateCollection;
    }

    protected override SortedDictionary<TKey, TValue> Create(int count)
    {
      return new SortedDictionary<TKey, TValue>();
    }
  }
}
