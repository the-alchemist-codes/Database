﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.NullableSByteFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Formatters
{
  public sealed class NullableSByteFormatter : IMessagePackFormatter<sbyte?>, IMessagePackFormatter
  {
    public static readonly NullableSByteFormatter Instance = new NullableSByteFormatter();

    private NullableSByteFormatter()
    {
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      sbyte? value,
      IFormatterResolver formatterResolver)
    {
      return !value.HasValue ? MessagePackBinary.WriteNil(ref bytes, offset) : MessagePackBinary.WriteSByte(ref bytes, offset, value.Value);
    }

    public sbyte? Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      if (!MessagePackBinary.IsNil(bytes, offset))
        return new sbyte?(MessagePackBinary.ReadSByte(bytes, offset, out readSize));
      readSize = 1;
      return new sbyte?();
    }
  }
}
