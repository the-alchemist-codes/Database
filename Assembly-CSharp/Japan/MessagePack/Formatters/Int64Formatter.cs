﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.Int64Formatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Formatters
{
  public sealed class Int64Formatter : IMessagePackFormatter<long>, IMessagePackFormatter
  {
    public static readonly Int64Formatter Instance = new Int64Formatter();

    private Int64Formatter()
    {
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      long value,
      IFormatterResolver formatterResolver)
    {
      return MessagePackBinary.WriteInt64(ref bytes, offset, value);
    }

    public long Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      return MessagePackBinary.ReadInt64(bytes, offset, out readSize);
    }
  }
}
