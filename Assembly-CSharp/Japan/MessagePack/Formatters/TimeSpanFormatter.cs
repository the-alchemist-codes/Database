﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.TimeSpanFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace MessagePack.Formatters
{
  public sealed class TimeSpanFormatter : IMessagePackFormatter<TimeSpan>, IMessagePackFormatter
  {
    public static readonly IMessagePackFormatter<TimeSpan> Instance = (IMessagePackFormatter<TimeSpan>) new TimeSpanFormatter();

    private TimeSpanFormatter()
    {
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      TimeSpan value,
      IFormatterResolver formatterResolver)
    {
      return MessagePackBinary.WriteInt64(ref bytes, offset, value.Ticks);
    }

    public TimeSpan Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      return new TimeSpan(MessagePackBinary.ReadInt64(bytes, offset, out readSize));
    }
  }
}
