﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.InterfaceEnumerableFormatter`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace MessagePack.Formatters
{
  public sealed class InterfaceEnumerableFormatter<T> : CollectionFormatterBase<T, T[], IEnumerable<T>>
  {
    protected override void Add(T[] collection, int index, T value)
    {
      collection[index] = value;
    }

    protected override T[] Create(int count)
    {
      return new T[count];
    }

    protected override IEnumerable<T> Complete(T[] intermediateCollection)
    {
      return (IEnumerable<T>) intermediateCollection;
    }
  }
}
