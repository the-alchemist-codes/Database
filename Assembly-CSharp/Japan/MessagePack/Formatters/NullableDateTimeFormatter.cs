﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.NullableDateTimeFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace MessagePack.Formatters
{
  public sealed class NullableDateTimeFormatter : IMessagePackFormatter<DateTime?>, IMessagePackFormatter
  {
    public static readonly NullableDateTimeFormatter Instance = new NullableDateTimeFormatter();

    private NullableDateTimeFormatter()
    {
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      DateTime? value,
      IFormatterResolver formatterResolver)
    {
      return !value.HasValue ? MessagePackBinary.WriteNil(ref bytes, offset) : MessagePackBinary.WriteDateTime(ref bytes, offset, value.Value);
    }

    public DateTime? Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      if (!MessagePackBinary.IsNil(bytes, offset))
        return new DateTime?(MessagePackBinary.ReadDateTime(bytes, offset, out readSize));
      readSize = 1;
      return new DateTime?();
    }
  }
}
