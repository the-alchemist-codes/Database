﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.InterfaceLookupFormatter`2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;
using System.Linq;

namespace MessagePack.Formatters
{
  public sealed class InterfaceLookupFormatter<TKey, TElement> : CollectionFormatterBase<IGrouping<TKey, TElement>, Dictionary<TKey, IGrouping<TKey, TElement>>, ILookup<TKey, TElement>>
  {
    protected override void Add(
      Dictionary<TKey, IGrouping<TKey, TElement>> collection,
      int index,
      IGrouping<TKey, TElement> value)
    {
      collection.Add(value.Key, value);
    }

    protected override ILookup<TKey, TElement> Complete(
      Dictionary<TKey, IGrouping<TKey, TElement>> intermediateCollection)
    {
      return (ILookup<TKey, TElement>) new Lookup<TKey, TElement>(intermediateCollection);
    }

    protected override Dictionary<TKey, IGrouping<TKey, TElement>> Create(
      int count)
    {
      return new Dictionary<TKey, IGrouping<TKey, TElement>>(count);
    }
  }
}
