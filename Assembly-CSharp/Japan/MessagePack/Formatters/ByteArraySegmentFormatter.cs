﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.ByteArraySegmentFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace MessagePack.Formatters
{
  public sealed class ByteArraySegmentFormatter : IMessagePackFormatter<ArraySegment<byte>>, IMessagePackFormatter
  {
    public static readonly ByteArraySegmentFormatter Instance = new ByteArraySegmentFormatter();

    private ByteArraySegmentFormatter()
    {
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      ArraySegment<byte> value,
      IFormatterResolver formatterResolver)
    {
      return value.Array == null ? MessagePackBinary.WriteNil(ref bytes, offset) : MessagePackBinary.WriteBytes(ref bytes, offset, value.Array, value.Offset, value.Count);
    }

    public ArraySegment<byte> Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      if (MessagePackBinary.IsNil(bytes, offset))
      {
        readSize = 1;
        return new ArraySegment<byte>();
      }
      byte[] array = MessagePackBinary.ReadBytes(bytes, offset, out readSize);
      return new ArraySegment<byte>(array, 0, array.Length);
    }
  }
}
