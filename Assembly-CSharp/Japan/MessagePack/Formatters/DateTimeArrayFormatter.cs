﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.DateTimeArrayFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace MessagePack.Formatters
{
  public sealed class DateTimeArrayFormatter : IMessagePackFormatter<DateTime[]>, IMessagePackFormatter
  {
    public static readonly DateTimeArrayFormatter Instance = new DateTimeArrayFormatter();

    private DateTimeArrayFormatter()
    {
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      DateTime[] value,
      IFormatterResolver formatterResolver)
    {
      if (value == null)
        return MessagePackBinary.WriteNil(ref bytes, offset);
      int num = offset;
      offset += MessagePackBinary.WriteArrayHeader(ref bytes, offset, value.Length);
      for (int index = 0; index < value.Length; ++index)
        offset += MessagePackBinary.WriteDateTime(ref bytes, offset, value[index]);
      return offset - num;
    }

    public DateTime[] Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      if (MessagePackBinary.IsNil(bytes, offset))
      {
        readSize = 1;
        return (DateTime[]) null;
      }
      int num = offset;
      int length = MessagePackBinary.ReadArrayHeader(bytes, offset, out readSize);
      offset += readSize;
      DateTime[] dateTimeArray = new DateTime[length];
      for (int index = 0; index < dateTimeArray.Length; ++index)
      {
        dateTimeArray[index] = MessagePackBinary.ReadDateTime(bytes, offset, out readSize);
        offset += readSize;
      }
      readSize = offset - num;
      return dateTimeArray;
    }
  }
}
