﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.QeueueFormatter`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace MessagePack.Formatters
{
  public sealed class QeueueFormatter<T> : CollectionFormatterBase<T, Queue<T>, Queue<T>.Enumerator, Queue<T>>
  {
    protected override int? GetCount(Queue<T> sequence)
    {
      return new int?(sequence.Count);
    }

    protected override void Add(Queue<T> collection, int index, T value)
    {
      collection.Enqueue(value);
    }

    protected override Queue<T> Create(int count)
    {
      return new Queue<T>(count);
    }

    protected override Queue<T>.Enumerator GetSourceEnumerator(Queue<T> source)
    {
      return source.GetEnumerator();
    }

    protected override Queue<T> Complete(Queue<T> intermediateCollection)
    {
      return intermediateCollection;
    }
  }
}
