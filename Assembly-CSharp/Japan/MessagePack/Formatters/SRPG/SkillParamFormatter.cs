﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.SRPG.SkillParamFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack.Internal;
using SRPG;
using System;
using System.Collections.Generic;

namespace MessagePack.Formatters.SRPG
{
  public sealed class SkillParamFormatter : IMessagePackFormatter<SkillParam>, IMessagePackFormatter
  {
    private readonly AutomataDictionary ____keyMapping;
    private readonly byte[][] ____stringByteKeys;

    public SkillParamFormatter()
    {
      this.____keyMapping = new AutomataDictionary()
      {
        {
          "ProtectSkill",
          0
        },
        {
          "IsCritical",
          1
        },
        {
          "IsJewelAbsorb",
          2
        },
        {
          "IsTargetGridNoUnit",
          3
        },
        {
          "IsTargetValidGrid",
          4
        },
        {
          "IsSkillCountNoLimit",
          5
        },
        {
          "IsTargetTeleport",
          6
        },
        {
          "IsForcedTargetingSkillEffect",
          7
        },
        {
          "iname",
          8
        },
        {
          "name",
          9
        },
        {
          "expr",
          10
        },
        {
          "motion",
          11
        },
        {
          "effect",
          12
        },
        {
          "defend_effect",
          13
        },
        {
          "weapon",
          14
        },
        {
          "tokkou",
          15
        },
        {
          "tk_rate",
          16
        },
        {
          "type",
          17
        },
        {
          "timing",
          18
        },
        {
          "condition",
          19
        },
        {
          "lvcap",
          20
        },
        {
          "cost",
          21
        },
        {
          "count",
          22
        },
        {
          "rate",
          23
        },
        {
          "back_defrate",
          24
        },
        {
          "side_defrate",
          25
        },
        {
          "ignore_defense_rate",
          26
        },
        {
          "line_type",
          27
        },
        {
          "select_range",
          28
        },
        {
          "range_min",
          29
        },
        {
          "range_max",
          30
        },
        {
          "select_scope",
          31
        },
        {
          "scope",
          32
        },
        {
          "effect_height",
          33
        },
        {
          "hp_cost_rate",
          34
        },
        {
          "hp_cost",
          35
        },
        {
          "random_hit_rate",
          36
        },
        {
          "cast_type",
          37
        },
        {
          "cast_speed",
          38
        },
        {
          "target",
          39
        },
        {
          "effect_type",
          40
        },
        {
          "effect_rate",
          41
        },
        {
          "effect_value",
          42
        },
        {
          "effect_range",
          43
        },
        {
          "effect_calc",
          44
        },
        {
          "effect_hprate",
          45
        },
        {
          "effect_mprate",
          46
        },
        {
          "effect_dead_rate",
          47
        },
        {
          "effect_lvrate",
          48
        },
        {
          "absorb_damage_rate",
          49
        },
        {
          "element_type",
          50
        },
        {
          "attack_type",
          51
        },
        {
          "attack_detail",
          52
        },
        {
          "reaction_damage_type",
          53
        },
        {
          "reaction_det_lists",
          54
        },
        {
          "control_damage_rate",
          55
        },
        {
          "control_damage_value",
          56
        },
        {
          "control_damage_calc",
          57
        },
        {
          "control_ct_rate",
          58
        },
        {
          "control_ct_value",
          59
        },
        {
          "control_ct_calc",
          60
        },
        {
          "target_buff_iname",
          61
        },
        {
          "target_cond_iname",
          62
        },
        {
          "self_buff_iname",
          63
        },
        {
          "self_cond_iname",
          64
        },
        {
          "shield_type",
          65
        },
        {
          "shield_damage_type",
          66
        },
        {
          "shield_turn",
          67
        },
        {
          "shield_value",
          68
        },
        {
          "job",
          69
        },
        {
          "ComboNum",
          70
        },
        {
          "ComboDamageRate",
          71
        },
        {
          "JewelDamageType",
          72
        },
        {
          "JewelDamageValue",
          73
        },
        {
          "DuplicateCount",
          74
        },
        {
          "SceneName",
          75
        },
        {
          "SceneNameBigUnit",
          76
        },
        {
          "CollaboMainId",
          77
        },
        {
          "CollaboHeight",
          78
        },
        {
          "KnockBackRate",
          79
        },
        {
          "KnockBackVal",
          80
        },
        {
          "KnockBackDir",
          81
        },
        {
          "KnockBackDs",
          82
        },
        {
          "DamageDispType",
          83
        },
        {
          "TeleportType",
          84
        },
        {
          "TeleportTarget",
          85
        },
        {
          "TeleportHeight",
          86
        },
        {
          "TeleportIsMove",
          87
        },
        {
          "ReplaceTargetIdLists",
          88
        },
        {
          "ReplaceChangeIdLists",
          89
        },
        {
          "AbilityReplaceTargetIdLists",
          90
        },
        {
          "AbilityReplaceChangeIdLists",
          91
        },
        {
          "CollaboVoiceId",
          92
        },
        {
          "CollaboVoicePlayDelayFrame",
          93
        },
        {
          "ReplacedTargetId",
          94
        },
        {
          "TrickId",
          95
        },
        {
          "TrickSetType",
          96
        },
        {
          "BreakObjId",
          97
        },
        {
          "MapEffectDesc",
          98
        },
        {
          "WeatherRate",
          99
        },
        {
          "WeatherId",
          100
        },
        {
          "ElementSpcAtkRate",
          101
        },
        {
          "MaxDamageValue",
          102
        },
        {
          "CutInConceptCardId",
          103
        },
        {
          "JudgeHpVal",
          104
        },
        {
          "JudgeHpCalc",
          105
        },
        {
          "AcFromAbilId",
          106
        },
        {
          "AcToAbilId",
          107
        },
        {
          "AcTurn",
          108
        },
        {
          "EffectHitTargetNumRate",
          109
        },
        {
          "AbsorbAndGive",
          110
        },
        {
          "TargetEx",
          111
        },
        {
          "JumpSpcAtkRate",
          112
        },
        {
          "TeleportSkillPos",
          113
        },
        {
          "DynamicTransformUnitId",
          114
        },
        {
          "SkillMotionId",
          115
        },
        {
          "DependStateSpcEffId",
          116
        },
        {
          "DependStateSpcEffSelfId",
          117
        },
        {
          "ForcedTargetingTurn",
          118
        },
        {
          "ProtectSkillId",
          119
        }
      };
      this.____stringByteKeys = new byte[120][]
      {
        MessagePackBinary.GetEncodedStringBytes("ProtectSkill"),
        MessagePackBinary.GetEncodedStringBytes("IsCritical"),
        MessagePackBinary.GetEncodedStringBytes("IsJewelAbsorb"),
        MessagePackBinary.GetEncodedStringBytes("IsTargetGridNoUnit"),
        MessagePackBinary.GetEncodedStringBytes("IsTargetValidGrid"),
        MessagePackBinary.GetEncodedStringBytes("IsSkillCountNoLimit"),
        MessagePackBinary.GetEncodedStringBytes("IsTargetTeleport"),
        MessagePackBinary.GetEncodedStringBytes("IsForcedTargetingSkillEffect"),
        MessagePackBinary.GetEncodedStringBytes("iname"),
        MessagePackBinary.GetEncodedStringBytes("name"),
        MessagePackBinary.GetEncodedStringBytes("expr"),
        MessagePackBinary.GetEncodedStringBytes("motion"),
        MessagePackBinary.GetEncodedStringBytes("effect"),
        MessagePackBinary.GetEncodedStringBytes("defend_effect"),
        MessagePackBinary.GetEncodedStringBytes("weapon"),
        MessagePackBinary.GetEncodedStringBytes("tokkou"),
        MessagePackBinary.GetEncodedStringBytes("tk_rate"),
        MessagePackBinary.GetEncodedStringBytes("type"),
        MessagePackBinary.GetEncodedStringBytes("timing"),
        MessagePackBinary.GetEncodedStringBytes("condition"),
        MessagePackBinary.GetEncodedStringBytes("lvcap"),
        MessagePackBinary.GetEncodedStringBytes("cost"),
        MessagePackBinary.GetEncodedStringBytes("count"),
        MessagePackBinary.GetEncodedStringBytes("rate"),
        MessagePackBinary.GetEncodedStringBytes("back_defrate"),
        MessagePackBinary.GetEncodedStringBytes("side_defrate"),
        MessagePackBinary.GetEncodedStringBytes("ignore_defense_rate"),
        MessagePackBinary.GetEncodedStringBytes("line_type"),
        MessagePackBinary.GetEncodedStringBytes("select_range"),
        MessagePackBinary.GetEncodedStringBytes("range_min"),
        MessagePackBinary.GetEncodedStringBytes("range_max"),
        MessagePackBinary.GetEncodedStringBytes("select_scope"),
        MessagePackBinary.GetEncodedStringBytes("scope"),
        MessagePackBinary.GetEncodedStringBytes("effect_height"),
        MessagePackBinary.GetEncodedStringBytes("hp_cost_rate"),
        MessagePackBinary.GetEncodedStringBytes("hp_cost"),
        MessagePackBinary.GetEncodedStringBytes("random_hit_rate"),
        MessagePackBinary.GetEncodedStringBytes("cast_type"),
        MessagePackBinary.GetEncodedStringBytes("cast_speed"),
        MessagePackBinary.GetEncodedStringBytes("target"),
        MessagePackBinary.GetEncodedStringBytes("effect_type"),
        MessagePackBinary.GetEncodedStringBytes("effect_rate"),
        MessagePackBinary.GetEncodedStringBytes("effect_value"),
        MessagePackBinary.GetEncodedStringBytes("effect_range"),
        MessagePackBinary.GetEncodedStringBytes("effect_calc"),
        MessagePackBinary.GetEncodedStringBytes("effect_hprate"),
        MessagePackBinary.GetEncodedStringBytes("effect_mprate"),
        MessagePackBinary.GetEncodedStringBytes("effect_dead_rate"),
        MessagePackBinary.GetEncodedStringBytes("effect_lvrate"),
        MessagePackBinary.GetEncodedStringBytes("absorb_damage_rate"),
        MessagePackBinary.GetEncodedStringBytes("element_type"),
        MessagePackBinary.GetEncodedStringBytes("attack_type"),
        MessagePackBinary.GetEncodedStringBytes("attack_detail"),
        MessagePackBinary.GetEncodedStringBytes("reaction_damage_type"),
        MessagePackBinary.GetEncodedStringBytes("reaction_det_lists"),
        MessagePackBinary.GetEncodedStringBytes("control_damage_rate"),
        MessagePackBinary.GetEncodedStringBytes("control_damage_value"),
        MessagePackBinary.GetEncodedStringBytes("control_damage_calc"),
        MessagePackBinary.GetEncodedStringBytes("control_ct_rate"),
        MessagePackBinary.GetEncodedStringBytes("control_ct_value"),
        MessagePackBinary.GetEncodedStringBytes("control_ct_calc"),
        MessagePackBinary.GetEncodedStringBytes("target_buff_iname"),
        MessagePackBinary.GetEncodedStringBytes("target_cond_iname"),
        MessagePackBinary.GetEncodedStringBytes("self_buff_iname"),
        MessagePackBinary.GetEncodedStringBytes("self_cond_iname"),
        MessagePackBinary.GetEncodedStringBytes("shield_type"),
        MessagePackBinary.GetEncodedStringBytes("shield_damage_type"),
        MessagePackBinary.GetEncodedStringBytes("shield_turn"),
        MessagePackBinary.GetEncodedStringBytes("shield_value"),
        MessagePackBinary.GetEncodedStringBytes("job"),
        MessagePackBinary.GetEncodedStringBytes("ComboNum"),
        MessagePackBinary.GetEncodedStringBytes("ComboDamageRate"),
        MessagePackBinary.GetEncodedStringBytes("JewelDamageType"),
        MessagePackBinary.GetEncodedStringBytes("JewelDamageValue"),
        MessagePackBinary.GetEncodedStringBytes("DuplicateCount"),
        MessagePackBinary.GetEncodedStringBytes("SceneName"),
        MessagePackBinary.GetEncodedStringBytes("SceneNameBigUnit"),
        MessagePackBinary.GetEncodedStringBytes("CollaboMainId"),
        MessagePackBinary.GetEncodedStringBytes("CollaboHeight"),
        MessagePackBinary.GetEncodedStringBytes("KnockBackRate"),
        MessagePackBinary.GetEncodedStringBytes("KnockBackVal"),
        MessagePackBinary.GetEncodedStringBytes("KnockBackDir"),
        MessagePackBinary.GetEncodedStringBytes("KnockBackDs"),
        MessagePackBinary.GetEncodedStringBytes("DamageDispType"),
        MessagePackBinary.GetEncodedStringBytes("TeleportType"),
        MessagePackBinary.GetEncodedStringBytes("TeleportTarget"),
        MessagePackBinary.GetEncodedStringBytes("TeleportHeight"),
        MessagePackBinary.GetEncodedStringBytes("TeleportIsMove"),
        MessagePackBinary.GetEncodedStringBytes("ReplaceTargetIdLists"),
        MessagePackBinary.GetEncodedStringBytes("ReplaceChangeIdLists"),
        MessagePackBinary.GetEncodedStringBytes("AbilityReplaceTargetIdLists"),
        MessagePackBinary.GetEncodedStringBytes("AbilityReplaceChangeIdLists"),
        MessagePackBinary.GetEncodedStringBytes("CollaboVoiceId"),
        MessagePackBinary.GetEncodedStringBytes("CollaboVoicePlayDelayFrame"),
        MessagePackBinary.GetEncodedStringBytes("ReplacedTargetId"),
        MessagePackBinary.GetEncodedStringBytes("TrickId"),
        MessagePackBinary.GetEncodedStringBytes("TrickSetType"),
        MessagePackBinary.GetEncodedStringBytes("BreakObjId"),
        MessagePackBinary.GetEncodedStringBytes("MapEffectDesc"),
        MessagePackBinary.GetEncodedStringBytes("WeatherRate"),
        MessagePackBinary.GetEncodedStringBytes("WeatherId"),
        MessagePackBinary.GetEncodedStringBytes("ElementSpcAtkRate"),
        MessagePackBinary.GetEncodedStringBytes("MaxDamageValue"),
        MessagePackBinary.GetEncodedStringBytes("CutInConceptCardId"),
        MessagePackBinary.GetEncodedStringBytes("JudgeHpVal"),
        MessagePackBinary.GetEncodedStringBytes("JudgeHpCalc"),
        MessagePackBinary.GetEncodedStringBytes("AcFromAbilId"),
        MessagePackBinary.GetEncodedStringBytes("AcToAbilId"),
        MessagePackBinary.GetEncodedStringBytes("AcTurn"),
        MessagePackBinary.GetEncodedStringBytes("EffectHitTargetNumRate"),
        MessagePackBinary.GetEncodedStringBytes("AbsorbAndGive"),
        MessagePackBinary.GetEncodedStringBytes("TargetEx"),
        MessagePackBinary.GetEncodedStringBytes("JumpSpcAtkRate"),
        MessagePackBinary.GetEncodedStringBytes("TeleportSkillPos"),
        MessagePackBinary.GetEncodedStringBytes("DynamicTransformUnitId"),
        MessagePackBinary.GetEncodedStringBytes("SkillMotionId"),
        MessagePackBinary.GetEncodedStringBytes("DependStateSpcEffId"),
        MessagePackBinary.GetEncodedStringBytes("DependStateSpcEffSelfId"),
        MessagePackBinary.GetEncodedStringBytes("ForcedTargetingTurn"),
        MessagePackBinary.GetEncodedStringBytes("ProtectSkillId")
      };
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      SkillParam value,
      IFormatterResolver formatterResolver)
    {
      if (value == null)
        return MessagePackBinary.WriteNil(ref bytes, offset);
      int num = offset;
      offset += MessagePackBinary.WriteMapHeader(ref bytes, offset, 120);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[0]);
      offset += formatterResolver.GetFormatterWithVerify<ProtectSkillParam>().Serialize(ref bytes, offset, value.ProtectSkill, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[1]);
      offset += MessagePackBinary.WriteBoolean(ref bytes, offset, value.IsCritical);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[2]);
      offset += MessagePackBinary.WriteBoolean(ref bytes, offset, value.IsJewelAbsorb);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[3]);
      offset += MessagePackBinary.WriteBoolean(ref bytes, offset, value.IsTargetGridNoUnit);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[4]);
      offset += MessagePackBinary.WriteBoolean(ref bytes, offset, value.IsTargetValidGrid);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[5]);
      offset += MessagePackBinary.WriteBoolean(ref bytes, offset, value.IsSkillCountNoLimit);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[6]);
      offset += MessagePackBinary.WriteBoolean(ref bytes, offset, value.IsTargetTeleport);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[7]);
      offset += MessagePackBinary.WriteBoolean(ref bytes, offset, value.IsForcedTargetingSkillEffect);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[8]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.iname, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[9]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.name, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[10]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.expr, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[11]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.motion, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[12]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.effect, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[13]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.defend_effect, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[14]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.weapon, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[15]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.tokkou, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[16]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.tk_rate);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[17]);
      offset += formatterResolver.GetFormatterWithVerify<ESkillType>().Serialize(ref bytes, offset, value.type, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[18]);
      offset += formatterResolver.GetFormatterWithVerify<ESkillTiming>().Serialize(ref bytes, offset, value.timing, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[19]);
      offset += formatterResolver.GetFormatterWithVerify<ESkillCondition>().Serialize(ref bytes, offset, value.condition, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[20]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.lvcap);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[21]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.cost);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[22]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.count);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[23]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.rate);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[24]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.back_defrate);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[25]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.side_defrate);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[26]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.ignore_defense_rate);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[27]);
      offset += formatterResolver.GetFormatterWithVerify<ELineType>().Serialize(ref bytes, offset, value.line_type, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[28]);
      offset += formatterResolver.GetFormatterWithVerify<ESelectType>().Serialize(ref bytes, offset, value.select_range, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[29]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.range_min);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[30]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.range_max);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[31]);
      offset += formatterResolver.GetFormatterWithVerify<ESelectType>().Serialize(ref bytes, offset, value.select_scope, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[32]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.scope);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[33]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.effect_height);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[34]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.hp_cost_rate);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[35]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.hp_cost);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[36]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.random_hit_rate);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[37]);
      offset += formatterResolver.GetFormatterWithVerify<ECastTypes>().Serialize(ref bytes, offset, value.cast_type, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[38]);
      offset += formatterResolver.GetFormatterWithVerify<SkillRankUpValueShort>().Serialize(ref bytes, offset, value.cast_speed, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[39]);
      offset += formatterResolver.GetFormatterWithVerify<ESkillTarget>().Serialize(ref bytes, offset, value.target, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[40]);
      offset += formatterResolver.GetFormatterWithVerify<SkillEffectTypes>().Serialize(ref bytes, offset, value.effect_type, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[41]);
      offset += formatterResolver.GetFormatterWithVerify<SkillRankUpValueShort>().Serialize(ref bytes, offset, value.effect_rate, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[42]);
      offset += formatterResolver.GetFormatterWithVerify<SkillRankUpValue>().Serialize(ref bytes, offset, value.effect_value, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[43]);
      offset += formatterResolver.GetFormatterWithVerify<SkillRankUpValueShort>().Serialize(ref bytes, offset, value.effect_range, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[44]);
      offset += formatterResolver.GetFormatterWithVerify<SkillParamCalcTypes>().Serialize(ref bytes, offset, value.effect_calc, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[45]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.effect_hprate);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[46]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.effect_mprate);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[47]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.effect_dead_rate);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[48]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.effect_lvrate);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[49]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.absorb_damage_rate);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[50]);
      offset += formatterResolver.GetFormatterWithVerify<EElement>().Serialize(ref bytes, offset, value.element_type, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[51]);
      offset += formatterResolver.GetFormatterWithVerify<AttackTypes>().Serialize(ref bytes, offset, value.attack_type, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[52]);
      offset += formatterResolver.GetFormatterWithVerify<AttackDetailTypes>().Serialize(ref bytes, offset, value.attack_detail, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[53]);
      offset += formatterResolver.GetFormatterWithVerify<DamageTypes>().Serialize(ref bytes, offset, value.reaction_damage_type, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[54]);
      offset += formatterResolver.GetFormatterWithVerify<List<AttackDetailTypes>>().Serialize(ref bytes, offset, value.reaction_det_lists, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[55]);
      offset += formatterResolver.GetFormatterWithVerify<SkillRankUpValueShort>().Serialize(ref bytes, offset, value.control_damage_rate, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[56]);
      offset += formatterResolver.GetFormatterWithVerify<SkillRankUpValueShort>().Serialize(ref bytes, offset, value.control_damage_value, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[57]);
      offset += formatterResolver.GetFormatterWithVerify<SkillParamCalcTypes>().Serialize(ref bytes, offset, value.control_damage_calc, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[58]);
      offset += formatterResolver.GetFormatterWithVerify<SkillRankUpValueShort>().Serialize(ref bytes, offset, value.control_ct_rate, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[59]);
      offset += formatterResolver.GetFormatterWithVerify<SkillRankUpValueShort>().Serialize(ref bytes, offset, value.control_ct_value, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[60]);
      offset += formatterResolver.GetFormatterWithVerify<SkillParamCalcTypes>().Serialize(ref bytes, offset, value.control_ct_calc, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[61]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.target_buff_iname, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[62]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.target_cond_iname, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[63]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.self_buff_iname, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[64]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.self_cond_iname, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[65]);
      offset += formatterResolver.GetFormatterWithVerify<ShieldTypes>().Serialize(ref bytes, offset, value.shield_type, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[66]);
      offset += formatterResolver.GetFormatterWithVerify<DamageTypes>().Serialize(ref bytes, offset, value.shield_damage_type, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[67]);
      offset += formatterResolver.GetFormatterWithVerify<SkillRankUpValueShort>().Serialize(ref bytes, offset, value.shield_turn, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[68]);
      offset += formatterResolver.GetFormatterWithVerify<SkillRankUpValue>().Serialize(ref bytes, offset, value.shield_value, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[69]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.job, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[70]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.ComboNum);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[71]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.ComboDamageRate);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[72]);
      offset += formatterResolver.GetFormatterWithVerify<JewelDamageTypes>().Serialize(ref bytes, offset, value.JewelDamageType, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[73]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.JewelDamageValue);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[74]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.DuplicateCount);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[75]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.SceneName, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[76]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.SceneNameBigUnit, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[77]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.CollaboMainId, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[78]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.CollaboHeight);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[79]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.KnockBackRate);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[80]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.KnockBackVal);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[81]);
      offset += formatterResolver.GetFormatterWithVerify<eKnockBackDir>().Serialize(ref bytes, offset, value.KnockBackDir, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[82]);
      offset += formatterResolver.GetFormatterWithVerify<eKnockBackDs>().Serialize(ref bytes, offset, value.KnockBackDs, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[83]);
      offset += formatterResolver.GetFormatterWithVerify<eDamageDispType>().Serialize(ref bytes, offset, value.DamageDispType, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[84]);
      offset += formatterResolver.GetFormatterWithVerify<eTeleportType>().Serialize(ref bytes, offset, value.TeleportType, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[85]);
      offset += formatterResolver.GetFormatterWithVerify<ESkillTarget>().Serialize(ref bytes, offset, value.TeleportTarget, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[86]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.TeleportHeight);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[87]);
      offset += MessagePackBinary.WriteBoolean(ref bytes, offset, value.TeleportIsMove);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[88]);
      offset += formatterResolver.GetFormatterWithVerify<List<string>>().Serialize(ref bytes, offset, value.ReplaceTargetIdLists, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[89]);
      offset += formatterResolver.GetFormatterWithVerify<List<string>>().Serialize(ref bytes, offset, value.ReplaceChangeIdLists, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[90]);
      offset += formatterResolver.GetFormatterWithVerify<List<string>>().Serialize(ref bytes, offset, value.AbilityReplaceTargetIdLists, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[91]);
      offset += formatterResolver.GetFormatterWithVerify<List<string>>().Serialize(ref bytes, offset, value.AbilityReplaceChangeIdLists, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[92]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.CollaboVoiceId, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[93]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.CollaboVoicePlayDelayFrame);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[94]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.ReplacedTargetId, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[95]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.TrickId, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[96]);
      offset += formatterResolver.GetFormatterWithVerify<eTrickSetType>().Serialize(ref bytes, offset, value.TrickSetType, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[97]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.BreakObjId, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[98]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.MapEffectDesc, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[99]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.WeatherRate);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[100]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.WeatherId, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[101]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.ElementSpcAtkRate);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[102]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.MaxDamageValue);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[103]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.CutInConceptCardId, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[104]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.JudgeHpVal);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[105]);
      offset += formatterResolver.GetFormatterWithVerify<SkillParamCalcTypes>().Serialize(ref bytes, offset, value.JudgeHpCalc, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[106]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.AcFromAbilId, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[107]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.AcToAbilId, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[108]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.AcTurn);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[109]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.EffectHitTargetNumRate);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[110]);
      offset += formatterResolver.GetFormatterWithVerify<eAbsorbAndGive>().Serialize(ref bytes, offset, value.AbsorbAndGive, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[111]);
      offset += formatterResolver.GetFormatterWithVerify<eSkillTargetEx>().Serialize(ref bytes, offset, value.TargetEx, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[112]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.JumpSpcAtkRate);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[113]);
      offset += formatterResolver.GetFormatterWithVerify<eTeleportSkillPos>().Serialize(ref bytes, offset, value.TeleportSkillPos, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[114]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.DynamicTransformUnitId, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[115]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.SkillMotionId, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[116]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.DependStateSpcEffId, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[117]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.DependStateSpcEffSelfId, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[118]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.ForcedTargetingTurn);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[119]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.ProtectSkillId, formatterResolver);
      return offset - num;
    }

    public SkillParam Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      if (MessagePackBinary.IsNil(bytes, offset))
      {
        readSize = 1;
        return (SkillParam) null;
      }
      int num1 = offset;
      int num2 = MessagePackBinary.ReadMapHeader(bytes, offset, out readSize);
      offset += readSize;
      ProtectSkillParam protectSkillParam = (ProtectSkillParam) null;
      bool flag1 = false;
      bool flag2 = false;
      bool flag3 = false;
      bool flag4 = false;
      bool flag5 = false;
      bool flag6 = false;
      bool flag7 = false;
      string str1 = (string) null;
      string str2 = (string) null;
      string str3 = (string) null;
      string str4 = (string) null;
      string str5 = (string) null;
      string str6 = (string) null;
      string str7 = (string) null;
      string str8 = (string) null;
      int num3 = 0;
      ESkillType eskillType = ESkillType.Attack;
      ESkillTiming eskillTiming = ESkillTiming.Used;
      ESkillCondition eskillCondition = ESkillCondition.None;
      int num4 = 0;
      int num5 = 0;
      int num6 = 0;
      int num7 = 0;
      int num8 = 0;
      int num9 = 0;
      int num10 = 0;
      ELineType elineType = ELineType.None;
      ESelectType eselectType1 = ESelectType.Cross;
      int num11 = 0;
      int num12 = 0;
      ESelectType eselectType2 = ESelectType.Cross;
      int num13 = 0;
      int num14 = 0;
      int num15 = 0;
      int num16 = 0;
      int num17 = 0;
      ECastTypes ecastTypes = ECastTypes.Chant;
      SkillRankUpValueShort rankUpValueShort1 = (SkillRankUpValueShort) null;
      ESkillTarget eskillTarget1 = ESkillTarget.Self;
      SkillEffectTypes skillEffectTypes = SkillEffectTypes.None;
      SkillRankUpValueShort rankUpValueShort2 = (SkillRankUpValueShort) null;
      SkillRankUpValue skillRankUpValue1 = (SkillRankUpValue) null;
      SkillRankUpValueShort rankUpValueShort3 = (SkillRankUpValueShort) null;
      SkillParamCalcTypes skillParamCalcTypes1 = SkillParamCalcTypes.Add;
      int num18 = 0;
      int num19 = 0;
      int num20 = 0;
      int num21 = 0;
      int num22 = 0;
      EElement eelement = EElement.None;
      AttackTypes attackTypes = AttackTypes.None;
      AttackDetailTypes attackDetailTypes = AttackDetailTypes.None;
      DamageTypes damageTypes1 = DamageTypes.None;
      List<AttackDetailTypes> attackDetailTypesList = (List<AttackDetailTypes>) null;
      SkillRankUpValueShort rankUpValueShort4 = (SkillRankUpValueShort) null;
      SkillRankUpValueShort rankUpValueShort5 = (SkillRankUpValueShort) null;
      SkillParamCalcTypes skillParamCalcTypes2 = SkillParamCalcTypes.Add;
      SkillRankUpValueShort rankUpValueShort6 = (SkillRankUpValueShort) null;
      SkillRankUpValueShort rankUpValueShort7 = (SkillRankUpValueShort) null;
      SkillParamCalcTypes skillParamCalcTypes3 = SkillParamCalcTypes.Add;
      string str9 = (string) null;
      string str10 = (string) null;
      string str11 = (string) null;
      string str12 = (string) null;
      ShieldTypes shieldTypes = ShieldTypes.None;
      DamageTypes damageTypes2 = DamageTypes.None;
      SkillRankUpValueShort rankUpValueShort8 = (SkillRankUpValueShort) null;
      SkillRankUpValue skillRankUpValue2 = (SkillRankUpValue) null;
      string str13 = (string) null;
      int num23 = 0;
      int num24 = 0;
      JewelDamageTypes jewelDamageTypes = JewelDamageTypes.None;
      int num25 = 0;
      int num26 = 0;
      string str14 = (string) null;
      string str15 = (string) null;
      string str16 = (string) null;
      int num27 = 0;
      int num28 = 0;
      int num29 = 0;
      eKnockBackDir eKnockBackDir = eKnockBackDir.Back;
      eKnockBackDs eKnockBackDs = eKnockBackDs.Target;
      eDamageDispType eDamageDispType = eDamageDispType.Standard;
      eTeleportType eTeleportType = eTeleportType.None;
      ESkillTarget eskillTarget2 = ESkillTarget.Self;
      int num30 = 0;
      bool flag8 = false;
      List<string> stringList1 = (List<string>) null;
      List<string> stringList2 = (List<string>) null;
      List<string> stringList3 = (List<string>) null;
      List<string> stringList4 = (List<string>) null;
      string str17 = (string) null;
      int num31 = 0;
      string str18 = (string) null;
      string str19 = (string) null;
      eTrickSetType eTrickSetType = eTrickSetType.GridNoUnit;
      string str20 = (string) null;
      string str21 = (string) null;
      int num32 = 0;
      string str22 = (string) null;
      int num33 = 0;
      int num34 = 0;
      string str23 = (string) null;
      int num35 = 0;
      SkillParamCalcTypes skillParamCalcTypes4 = SkillParamCalcTypes.Add;
      string str24 = (string) null;
      string str25 = (string) null;
      int num36 = 0;
      int num37 = 0;
      eAbsorbAndGive eAbsorbAndGive = eAbsorbAndGive.None;
      eSkillTargetEx eSkillTargetEx = eSkillTargetEx.None;
      int num38 = 0;
      eTeleportSkillPos teleportSkillPos = eTeleportSkillPos.None;
      string str26 = (string) null;
      string str27 = (string) null;
      string str28 = (string) null;
      string str29 = (string) null;
      int num39 = 0;
      string str30 = (string) null;
      for (int index = 0; index < num2; ++index)
      {
        ArraySegment<byte> key = MessagePackBinary.ReadStringSegment(bytes, offset, out readSize);
        offset += readSize;
        int num40;
        if (!this.____keyMapping.TryGetValueSafe(key, out num40))
        {
          readSize = MessagePackBinary.ReadNextBlock(bytes, offset);
        }
        else
        {
          switch (num40)
          {
            case 0:
              protectSkillParam = formatterResolver.GetFormatterWithVerify<ProtectSkillParam>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 1:
              flag1 = MessagePackBinary.ReadBoolean(bytes, offset, out readSize);
              break;
            case 2:
              flag2 = MessagePackBinary.ReadBoolean(bytes, offset, out readSize);
              break;
            case 3:
              flag3 = MessagePackBinary.ReadBoolean(bytes, offset, out readSize);
              break;
            case 4:
              flag4 = MessagePackBinary.ReadBoolean(bytes, offset, out readSize);
              break;
            case 5:
              flag5 = MessagePackBinary.ReadBoolean(bytes, offset, out readSize);
              break;
            case 6:
              flag6 = MessagePackBinary.ReadBoolean(bytes, offset, out readSize);
              break;
            case 7:
              flag7 = MessagePackBinary.ReadBoolean(bytes, offset, out readSize);
              break;
            case 8:
              str1 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 9:
              str2 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 10:
              str3 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 11:
              str4 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 12:
              str5 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 13:
              str6 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 14:
              str7 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 15:
              str8 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 16:
              num3 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 17:
              eskillType = formatterResolver.GetFormatterWithVerify<ESkillType>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 18:
              eskillTiming = formatterResolver.GetFormatterWithVerify<ESkillTiming>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 19:
              eskillCondition = formatterResolver.GetFormatterWithVerify<ESkillCondition>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 20:
              num4 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 21:
              num5 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 22:
              num6 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 23:
              num7 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 24:
              num8 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 25:
              num9 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 26:
              num10 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 27:
              elineType = formatterResolver.GetFormatterWithVerify<ELineType>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 28:
              eselectType1 = formatterResolver.GetFormatterWithVerify<ESelectType>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 29:
              num11 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 30:
              num12 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 31:
              eselectType2 = formatterResolver.GetFormatterWithVerify<ESelectType>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 32:
              num13 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 33:
              num14 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 34:
              num15 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 35:
              num16 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 36:
              num17 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 37:
              ecastTypes = formatterResolver.GetFormatterWithVerify<ECastTypes>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 38:
              rankUpValueShort1 = formatterResolver.GetFormatterWithVerify<SkillRankUpValueShort>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 39:
              eskillTarget1 = formatterResolver.GetFormatterWithVerify<ESkillTarget>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 40:
              skillEffectTypes = formatterResolver.GetFormatterWithVerify<SkillEffectTypes>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 41:
              rankUpValueShort2 = formatterResolver.GetFormatterWithVerify<SkillRankUpValueShort>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 42:
              skillRankUpValue1 = formatterResolver.GetFormatterWithVerify<SkillRankUpValue>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 43:
              rankUpValueShort3 = formatterResolver.GetFormatterWithVerify<SkillRankUpValueShort>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 44:
              skillParamCalcTypes1 = formatterResolver.GetFormatterWithVerify<SkillParamCalcTypes>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 45:
              num18 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 46:
              num19 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 47:
              num20 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 48:
              num21 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 49:
              num22 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 50:
              eelement = formatterResolver.GetFormatterWithVerify<EElement>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 51:
              attackTypes = formatterResolver.GetFormatterWithVerify<AttackTypes>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 52:
              attackDetailTypes = formatterResolver.GetFormatterWithVerify<AttackDetailTypes>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 53:
              damageTypes1 = formatterResolver.GetFormatterWithVerify<DamageTypes>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 54:
              attackDetailTypesList = formatterResolver.GetFormatterWithVerify<List<AttackDetailTypes>>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 55:
              rankUpValueShort4 = formatterResolver.GetFormatterWithVerify<SkillRankUpValueShort>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 56:
              rankUpValueShort5 = formatterResolver.GetFormatterWithVerify<SkillRankUpValueShort>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 57:
              skillParamCalcTypes2 = formatterResolver.GetFormatterWithVerify<SkillParamCalcTypes>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 58:
              rankUpValueShort6 = formatterResolver.GetFormatterWithVerify<SkillRankUpValueShort>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 59:
              rankUpValueShort7 = formatterResolver.GetFormatterWithVerify<SkillRankUpValueShort>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 60:
              skillParamCalcTypes3 = formatterResolver.GetFormatterWithVerify<SkillParamCalcTypes>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 61:
              str9 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 62:
              str10 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 63:
              str11 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 64:
              str12 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 65:
              shieldTypes = formatterResolver.GetFormatterWithVerify<ShieldTypes>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 66:
              damageTypes2 = formatterResolver.GetFormatterWithVerify<DamageTypes>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 67:
              rankUpValueShort8 = formatterResolver.GetFormatterWithVerify<SkillRankUpValueShort>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 68:
              skillRankUpValue2 = formatterResolver.GetFormatterWithVerify<SkillRankUpValue>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 69:
              str13 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 70:
              num23 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 71:
              num24 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 72:
              jewelDamageTypes = formatterResolver.GetFormatterWithVerify<JewelDamageTypes>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 73:
              num25 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 74:
              num26 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 75:
              str14 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 76:
              str15 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 77:
              str16 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 78:
              num27 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 79:
              num28 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 80:
              num29 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 81:
              eKnockBackDir = formatterResolver.GetFormatterWithVerify<eKnockBackDir>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 82:
              eKnockBackDs = formatterResolver.GetFormatterWithVerify<eKnockBackDs>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 83:
              eDamageDispType = formatterResolver.GetFormatterWithVerify<eDamageDispType>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 84:
              eTeleportType = formatterResolver.GetFormatterWithVerify<eTeleportType>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 85:
              eskillTarget2 = formatterResolver.GetFormatterWithVerify<ESkillTarget>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 86:
              num30 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 87:
              flag8 = MessagePackBinary.ReadBoolean(bytes, offset, out readSize);
              break;
            case 88:
              stringList1 = formatterResolver.GetFormatterWithVerify<List<string>>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 89:
              stringList2 = formatterResolver.GetFormatterWithVerify<List<string>>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 90:
              stringList3 = formatterResolver.GetFormatterWithVerify<List<string>>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 91:
              stringList4 = formatterResolver.GetFormatterWithVerify<List<string>>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 92:
              str17 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 93:
              num31 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 94:
              str18 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 95:
              str19 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 96:
              eTrickSetType = formatterResolver.GetFormatterWithVerify<eTrickSetType>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 97:
              str20 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 98:
              str21 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 99:
              num32 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 100:
              str22 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 101:
              num33 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 102:
              num34 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 103:
              str23 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 104:
              num35 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 105:
              skillParamCalcTypes4 = formatterResolver.GetFormatterWithVerify<SkillParamCalcTypes>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 106:
              str24 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 107:
              str25 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 108:
              num36 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 109:
              num37 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 110:
              eAbsorbAndGive = formatterResolver.GetFormatterWithVerify<eAbsorbAndGive>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 111:
              eSkillTargetEx = formatterResolver.GetFormatterWithVerify<eSkillTargetEx>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 112:
              num38 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 113:
              teleportSkillPos = formatterResolver.GetFormatterWithVerify<eTeleportSkillPos>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 114:
              str26 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 115:
              str27 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 116:
              str28 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 117:
              str29 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 118:
              num39 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 119:
              str30 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            default:
              readSize = MessagePackBinary.ReadNextBlock(bytes, offset);
              break;
          }
        }
        offset += readSize;
      }
      readSize = offset - num1;
      return new SkillParam()
      {
        iname = str1,
        name = str2,
        expr = str3,
        motion = str4,
        effect = str5,
        defend_effect = str6,
        weapon = str7,
        tokkou = str8,
        tk_rate = num3,
        type = eskillType,
        timing = eskillTiming,
        condition = eskillCondition,
        lvcap = num4,
        cost = num5,
        count = num6,
        rate = num7,
        back_defrate = num8,
        side_defrate = num9,
        ignore_defense_rate = num10,
        line_type = elineType,
        select_range = eselectType1,
        range_min = num11,
        range_max = num12,
        select_scope = eselectType2,
        scope = num13,
        effect_height = num14,
        hp_cost_rate = num15,
        hp_cost = num16,
        random_hit_rate = num17,
        cast_type = ecastTypes,
        cast_speed = rankUpValueShort1,
        target = eskillTarget1,
        effect_type = skillEffectTypes,
        effect_rate = rankUpValueShort2,
        effect_value = skillRankUpValue1,
        effect_range = rankUpValueShort3,
        effect_calc = skillParamCalcTypes1,
        effect_hprate = num18,
        effect_mprate = num19,
        effect_dead_rate = num20,
        effect_lvrate = num21,
        absorb_damage_rate = num22,
        element_type = eelement,
        attack_type = attackTypes,
        attack_detail = attackDetailTypes,
        reaction_damage_type = damageTypes1,
        reaction_det_lists = attackDetailTypesList,
        control_damage_rate = rankUpValueShort4,
        control_damage_value = rankUpValueShort5,
        control_damage_calc = skillParamCalcTypes2,
        control_ct_rate = rankUpValueShort6,
        control_ct_value = rankUpValueShort7,
        control_ct_calc = skillParamCalcTypes3,
        target_buff_iname = str9,
        target_cond_iname = str10,
        self_buff_iname = str11,
        self_cond_iname = str12,
        shield_type = shieldTypes,
        shield_damage_type = damageTypes2,
        shield_turn = rankUpValueShort8,
        shield_value = skillRankUpValue2,
        job = str13,
        ComboNum = num23,
        ComboDamageRate = num24,
        JewelDamageType = jewelDamageTypes,
        JewelDamageValue = num25,
        DuplicateCount = num26,
        SceneName = str14,
        SceneNameBigUnit = str15,
        CollaboMainId = str16,
        CollaboHeight = num27,
        KnockBackRate = num28,
        KnockBackVal = num29,
        KnockBackDir = eKnockBackDir,
        KnockBackDs = eKnockBackDs,
        DamageDispType = eDamageDispType,
        TeleportType = eTeleportType,
        TeleportTarget = eskillTarget2,
        TeleportHeight = num30,
        TeleportIsMove = flag8,
        ReplaceTargetIdLists = stringList1,
        ReplaceChangeIdLists = stringList2,
        AbilityReplaceTargetIdLists = stringList3,
        AbilityReplaceChangeIdLists = stringList4,
        CollaboVoiceId = str17,
        CollaboVoicePlayDelayFrame = num31,
        ReplacedTargetId = str18,
        TrickId = str19,
        TrickSetType = eTrickSetType,
        BreakObjId = str20,
        MapEffectDesc = str21,
        WeatherRate = num32,
        WeatherId = str22,
        ElementSpcAtkRate = num33,
        MaxDamageValue = num34,
        CutInConceptCardId = str23,
        JudgeHpVal = num35,
        JudgeHpCalc = skillParamCalcTypes4,
        AcFromAbilId = str24,
        AcToAbilId = str25,
        AcTurn = num36,
        EffectHitTargetNumRate = num37,
        AbsorbAndGive = eAbsorbAndGive,
        TargetEx = eSkillTargetEx,
        JumpSpcAtkRate = num38,
        TeleportSkillPos = teleportSkillPos,
        DynamicTransformUnitId = str26,
        SkillMotionId = str27,
        DependStateSpcEffId = str28,
        DependStateSpcEffSelfId = str29,
        ForcedTargetingTurn = num39,
        ProtectSkillId = str30
      };
    }
  }
}
