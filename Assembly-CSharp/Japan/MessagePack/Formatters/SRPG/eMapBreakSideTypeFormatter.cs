﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.SRPG.eMapBreakSideTypeFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using SRPG;

namespace MessagePack.Formatters.SRPG
{
  public sealed class eMapBreakSideTypeFormatter : IMessagePackFormatter<eMapBreakSideType>, IMessagePackFormatter
  {
    public int Serialize(
      ref byte[] bytes,
      int offset,
      eMapBreakSideType value,
      IFormatterResolver formatterResolver)
    {
      return MessagePackBinary.WriteInt32(ref bytes, offset, (int) value);
    }

    public eMapBreakSideType Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      return (eMapBreakSideType) MessagePackBinary.ReadInt32(bytes, offset, out readSize);
    }
  }
}
