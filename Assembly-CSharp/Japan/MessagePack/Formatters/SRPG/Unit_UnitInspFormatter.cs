﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.SRPG.Unit_UnitInspFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using SRPG;

namespace MessagePack.Formatters.SRPG
{
  public sealed class Unit_UnitInspFormatter : IMessagePackFormatter<Unit.UnitInsp>, IMessagePackFormatter
  {
    public int Serialize(
      ref byte[] bytes,
      int offset,
      Unit.UnitInsp value,
      IFormatterResolver formatterResolver)
    {
      if (value == null)
        return MessagePackBinary.WriteNil(ref bytes, offset);
      int num = offset;
      offset += MessagePackBinary.WriteFixedArrayHeaderUnsafe(ref bytes, offset, 4);
      offset += formatterResolver.GetFormatterWithVerify<ArtifactData>().Serialize(ref bytes, offset, value.mArtifact, formatterResolver);
      offset += formatterResolver.GetFormatterWithVerify<OInt>().Serialize(ref bytes, offset, value.mSlotNo, formatterResolver);
      offset += formatterResolver.GetFormatterWithVerify<OBool>().Serialize(ref bytes, offset, value.mValid, formatterResolver);
      offset += formatterResolver.GetFormatterWithVerify<OInt>().Serialize(ref bytes, offset, value.mCheckCtr, formatterResolver);
      return offset - num;
    }

    public Unit.UnitInsp Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      if (MessagePackBinary.IsNil(bytes, offset))
      {
        readSize = 1;
        return (Unit.UnitInsp) null;
      }
      int num1 = offset;
      int num2 = MessagePackBinary.ReadArrayHeader(bytes, offset, out readSize);
      offset += readSize;
      ArtifactData artifact = (ArtifactData) null;
      OInt oint1 = new OInt();
      OBool obool = new OBool();
      OInt oint2 = new OInt();
      for (int index = 0; index < num2; ++index)
      {
        switch (index)
        {
          case 0:
            artifact = formatterResolver.GetFormatterWithVerify<ArtifactData>().Deserialize(bytes, offset, formatterResolver, out readSize);
            break;
          case 1:
            oint1 = formatterResolver.GetFormatterWithVerify<OInt>().Deserialize(bytes, offset, formatterResolver, out readSize);
            break;
          case 2:
            obool = formatterResolver.GetFormatterWithVerify<OBool>().Deserialize(bytes, offset, formatterResolver, out readSize);
            break;
          case 3:
            oint2 = formatterResolver.GetFormatterWithVerify<OInt>().Deserialize(bytes, offset, formatterResolver, out readSize);
            break;
          default:
            readSize = MessagePackBinary.ReadNextBlock(bytes, offset);
            break;
        }
        offset += readSize;
      }
      readSize = offset - num1;
      return new Unit.UnitInsp(artifact, (int) oint1, (bool) obool, (int) oint2)
      {
        mArtifact = artifact,
        mSlotNo = oint1,
        mValid = obool,
        mCheckCtr = oint2
      };
    }
  }
}
