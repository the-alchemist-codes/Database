﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.SRPG.JSON_GvGPeriodParamFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack.Internal;
using SRPG;
using System;

namespace MessagePack.Formatters.SRPG
{
  public sealed class JSON_GvGPeriodParamFormatter : IMessagePackFormatter<JSON_GvGPeriodParam>, IMessagePackFormatter
  {
    private readonly AutomataDictionary ____keyMapping;
    private readonly byte[][] ____stringByteKeys;

    public JSON_GvGPeriodParamFormatter()
    {
      this.____keyMapping = new AutomataDictionary()
      {
        {
          "id",
          0
        },
        {
          "begin_at",
          1
        },
        {
          "end_at",
          2
        },
        {
          "exit_at",
          3
        },
        {
          "declaration_start_time",
          4
        },
        {
          "declaration_end_time",
          5
        },
        {
          "battle_start_time",
          6
        },
        {
          "battle_end_time",
          7
        },
        {
          "declared_cool_minutes",
          8
        },
        {
          "battle_cool_seconds",
          9
        },
        {
          "declare_num",
          10
        },
        {
          "map_idx",
          11
        },
        {
          "matching_count_min",
          12
        },
        {
          "matching_count_max",
          13
        },
        {
          "first_occupy_node_num",
          14
        },
        {
          "defense_unit_min",
          15
        },
        {
          "url",
          16
        },
        {
          "rule_cycle",
          17
        },
        {
          "url_title",
          18
        }
      };
      this.____stringByteKeys = new byte[19][]
      {
        MessagePackBinary.GetEncodedStringBytes("id"),
        MessagePackBinary.GetEncodedStringBytes("begin_at"),
        MessagePackBinary.GetEncodedStringBytes("end_at"),
        MessagePackBinary.GetEncodedStringBytes("exit_at"),
        MessagePackBinary.GetEncodedStringBytes("declaration_start_time"),
        MessagePackBinary.GetEncodedStringBytes("declaration_end_time"),
        MessagePackBinary.GetEncodedStringBytes("battle_start_time"),
        MessagePackBinary.GetEncodedStringBytes("battle_end_time"),
        MessagePackBinary.GetEncodedStringBytes("declared_cool_minutes"),
        MessagePackBinary.GetEncodedStringBytes("battle_cool_seconds"),
        MessagePackBinary.GetEncodedStringBytes("declare_num"),
        MessagePackBinary.GetEncodedStringBytes("map_idx"),
        MessagePackBinary.GetEncodedStringBytes("matching_count_min"),
        MessagePackBinary.GetEncodedStringBytes("matching_count_max"),
        MessagePackBinary.GetEncodedStringBytes("first_occupy_node_num"),
        MessagePackBinary.GetEncodedStringBytes("defense_unit_min"),
        MessagePackBinary.GetEncodedStringBytes("url"),
        MessagePackBinary.GetEncodedStringBytes("rule_cycle"),
        MessagePackBinary.GetEncodedStringBytes("url_title")
      };
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      JSON_GvGPeriodParam value,
      IFormatterResolver formatterResolver)
    {
      if (value == null)
        return MessagePackBinary.WriteNil(ref bytes, offset);
      int num = offset;
      offset += MessagePackBinary.WriteMapHeader(ref bytes, offset, 19);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[0]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.id);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[1]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.begin_at, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[2]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.end_at, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[3]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.exit_at, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[4]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.declaration_start_time, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[5]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.declaration_end_time, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[6]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.battle_start_time, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[7]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.battle_end_time, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[8]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.declared_cool_minutes);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[9]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.battle_cool_seconds);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[10]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.declare_num);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[11]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.map_idx);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[12]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.matching_count_min);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[13]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.matching_count_max);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[14]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.first_occupy_node_num);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[15]);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, value.defense_unit_min);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[16]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.url, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[17]);
      offset += formatterResolver.GetFormatterWithVerify<string[]>().Serialize(ref bytes, offset, value.rule_cycle, formatterResolver);
      offset += MessagePackBinary.WriteRaw(ref bytes, offset, this.____stringByteKeys[18]);
      offset += formatterResolver.GetFormatterWithVerify<string>().Serialize(ref bytes, offset, value.url_title, formatterResolver);
      return offset - num;
    }

    public JSON_GvGPeriodParam Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      if (MessagePackBinary.IsNil(bytes, offset))
      {
        readSize = 1;
        return (JSON_GvGPeriodParam) null;
      }
      int num1 = offset;
      int num2 = MessagePackBinary.ReadMapHeader(bytes, offset, out readSize);
      offset += readSize;
      int num3 = 0;
      string str1 = (string) null;
      string str2 = (string) null;
      string str3 = (string) null;
      string str4 = (string) null;
      string str5 = (string) null;
      string str6 = (string) null;
      string str7 = (string) null;
      int num4 = 0;
      int num5 = 0;
      int num6 = 0;
      int num7 = 0;
      int num8 = 0;
      int num9 = 0;
      int num10 = 0;
      int num11 = 0;
      string str8 = (string) null;
      string[] strArray = (string[]) null;
      string str9 = (string) null;
      for (int index = 0; index < num2; ++index)
      {
        ArraySegment<byte> key = MessagePackBinary.ReadStringSegment(bytes, offset, out readSize);
        offset += readSize;
        int num12;
        if (!this.____keyMapping.TryGetValueSafe(key, out num12))
        {
          readSize = MessagePackBinary.ReadNextBlock(bytes, offset);
        }
        else
        {
          switch (num12)
          {
            case 0:
              num3 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 1:
              str1 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 2:
              str2 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 3:
              str3 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 4:
              str4 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 5:
              str5 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 6:
              str6 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 7:
              str7 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 8:
              num4 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 9:
              num5 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 10:
              num6 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 11:
              num7 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 12:
              num8 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 13:
              num9 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 14:
              num10 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 15:
              num11 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
              break;
            case 16:
              str8 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 17:
              strArray = formatterResolver.GetFormatterWithVerify<string[]>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            case 18:
              str9 = formatterResolver.GetFormatterWithVerify<string>().Deserialize(bytes, offset, formatterResolver, out readSize);
              break;
            default:
              readSize = MessagePackBinary.ReadNextBlock(bytes, offset);
              break;
          }
        }
        offset += readSize;
      }
      readSize = offset - num1;
      return new JSON_GvGPeriodParam()
      {
        id = num3,
        begin_at = str1,
        end_at = str2,
        exit_at = str3,
        declaration_start_time = str4,
        declaration_end_time = str5,
        battle_start_time = str6,
        battle_end_time = str7,
        declared_cool_minutes = num4,
        battle_cool_seconds = num5,
        declare_num = num6,
        map_idx = num7,
        matching_count_min = num8,
        matching_count_max = num9,
        first_occupy_node_num = num10,
        defense_unit_min = num11,
        url = str8,
        rule_cycle = strArray,
        url_title = str9
      };
    }
  }
}
