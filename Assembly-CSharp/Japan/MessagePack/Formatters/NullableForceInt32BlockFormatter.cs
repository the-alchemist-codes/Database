﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.NullableForceInt32BlockFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Formatters
{
  public sealed class NullableForceInt32BlockFormatter : IMessagePackFormatter<int?>, IMessagePackFormatter
  {
    public static readonly NullableForceInt32BlockFormatter Instance = new NullableForceInt32BlockFormatter();

    private NullableForceInt32BlockFormatter()
    {
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      int? value,
      IFormatterResolver formatterResolver)
    {
      return !value.HasValue ? MessagePackBinary.WriteNil(ref bytes, offset) : MessagePackBinary.WriteInt32ForceInt32Block(ref bytes, offset, value.Value);
    }

    public int? Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      if (!MessagePackBinary.IsNil(bytes, offset))
        return new int?(MessagePackBinary.ReadInt32(bytes, offset, out readSize));
      readSize = 1;
      return new int?();
    }
  }
}
