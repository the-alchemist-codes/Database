﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.Lookup`2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace MessagePack.Formatters
{
  internal class Lookup<TKey, TElement> : ILookup<TKey, TElement>, IEnumerable, IEnumerable<IGrouping<TKey, TElement>>
  {
    private readonly Dictionary<TKey, IGrouping<TKey, TElement>> groupings;

    public Lookup(
      Dictionary<TKey, IGrouping<TKey, TElement>> groupings)
    {
      this.groupings = groupings;
    }

    public IEnumerable<TElement> this[TKey key]
    {
      get
      {
        return (IEnumerable<TElement>) this.groupings[key];
      }
    }

    public int Count
    {
      get
      {
        return this.groupings.Count;
      }
    }

    public bool Contains(TKey key)
    {
      return this.groupings.ContainsKey(key);
    }

    public IEnumerator<IGrouping<TKey, TElement>> GetEnumerator()
    {
      return (IEnumerator<IGrouping<TKey, TElement>>) this.groupings.Values.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
      return (IEnumerator) this.groupings.Values.GetEnumerator();
    }
  }
}
