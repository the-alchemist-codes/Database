﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.EnumAsStringFormatter`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using System.Collections.Generic;

namespace MessagePack.Formatters
{
  public sealed class EnumAsStringFormatter<T> : IMessagePackFormatter<T>, IMessagePackFormatter
  {
    private readonly Dictionary<string, T> nameValueMapping;
    private readonly Dictionary<T, string> valueNameMapping;

    public EnumAsStringFormatter()
    {
      string[] names = Enum.GetNames(typeof (T));
      Array values = Enum.GetValues(typeof (T));
      this.nameValueMapping = new Dictionary<string, T>(names.Length);
      this.valueNameMapping = new Dictionary<T, string>(names.Length);
      for (int index = 0; index < names.Length; ++index)
      {
        this.nameValueMapping[names[index]] = (T) values.GetValue(index);
        this.valueNameMapping[(T) values.GetValue(index)] = names[index];
      }
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      T value,
      IFormatterResolver formatterResolver)
    {
      string str;
      if (!this.valueNameMapping.TryGetValue(value, out str))
        str = value.ToString();
      return MessagePackBinary.WriteString(ref bytes, offset, str);
    }

    public T Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      string key = MessagePackBinary.ReadString(bytes, offset, out readSize);
      T obj;
      if (!this.nameValueMapping.TryGetValue(key, out obj))
        obj = (T) Enum.Parse(typeof (T), key);
      return obj;
    }
  }
}
