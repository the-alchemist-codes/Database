﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.BooleanFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Formatters
{
  public sealed class BooleanFormatter : IMessagePackFormatter<bool>, IMessagePackFormatter
  {
    public static readonly BooleanFormatter Instance = new BooleanFormatter();

    private BooleanFormatter()
    {
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      bool value,
      IFormatterResolver formatterResolver)
    {
      return MessagePackBinary.WriteBoolean(ref bytes, offset, value);
    }

    public bool Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      return MessagePackBinary.ReadBoolean(bytes, offset, out readSize);
    }
  }
}
