﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.UInt32Formatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Formatters
{
  public sealed class UInt32Formatter : IMessagePackFormatter<uint>, IMessagePackFormatter
  {
    public static readonly UInt32Formatter Instance = new UInt32Formatter();

    private UInt32Formatter()
    {
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      uint value,
      IFormatterResolver formatterResolver)
    {
      return MessagePackBinary.WriteUInt32(ref bytes, offset, value);
    }

    public uint Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      return MessagePackBinary.ReadUInt32(bytes, offset, out readSize);
    }
  }
}
