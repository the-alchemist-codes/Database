﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Formatters.NullableDoubleFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Formatters
{
  public sealed class NullableDoubleFormatter : IMessagePackFormatter<double?>, IMessagePackFormatter
  {
    public static readonly NullableDoubleFormatter Instance = new NullableDoubleFormatter();

    private NullableDoubleFormatter()
    {
    }

    public int Serialize(
      ref byte[] bytes,
      int offset,
      double? value,
      IFormatterResolver formatterResolver)
    {
      return !value.HasValue ? MessagePackBinary.WriteNil(ref bytes, offset) : MessagePackBinary.WriteDouble(ref bytes, offset, value.Value);
    }

    public double? Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      if (!MessagePackBinary.IsNil(bytes, offset))
        return new double?(MessagePackBinary.ReadDouble(bytes, offset, out readSize));
      readSize = 1;
      return new double?();
    }
  }
}
