﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.MessagePackFormatterAttribute
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace MessagePack
{
  [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Interface, AllowMultiple = false, Inherited = true)]
  public class MessagePackFormatterAttribute : Attribute
  {
    public MessagePackFormatterAttribute(Type formatterType)
    {
      this.FormatterType = formatterType;
    }

    public MessagePackFormatterAttribute(Type formatterType, params object[] arguments)
    {
      this.FormatterType = formatterType;
      this.Arguments = arguments;
    }

    public Type FormatterType { get; private set; }

    public object[] Arguments { get; private set; }
  }
}
