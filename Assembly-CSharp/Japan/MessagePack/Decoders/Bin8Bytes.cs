﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Decoders.Bin8Bytes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace MessagePack.Decoders
{
  internal sealed class Bin8Bytes : IBytesDecoder
  {
    internal static readonly IBytesDecoder Instance = (IBytesDecoder) new Bin8Bytes();

    private Bin8Bytes()
    {
    }

    public byte[] Read(byte[] bytes, int offset, out int readSize)
    {
      byte num = bytes[offset + 1];
      byte[] numArray = new byte[(int) num];
      Buffer.BlockCopy((Array) bytes, offset + 2, (Array) numArray, 0, (int) num);
      readSize = (int) num + 2;
      return numArray;
    }
  }
}
