﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Decoders.Float64Double
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Decoders
{
  internal sealed class Float64Double : IDoubleDecoder
  {
    internal static readonly IDoubleDecoder Instance = (IDoubleDecoder) new Float64Double();

    private Float64Double()
    {
    }

    public double Read(byte[] bytes, int offset, out int readSize)
    {
      readSize = 9;
      return new Float64Bits(bytes, offset + 1).Value;
    }
  }
}
