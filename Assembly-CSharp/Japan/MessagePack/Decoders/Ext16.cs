﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Decoders.Ext16
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace MessagePack.Decoders
{
  internal sealed class Ext16 : IExtDecoder
  {
    internal static readonly IExtDecoder Instance = (IExtDecoder) new Ext16();

    private Ext16()
    {
    }

    public ExtensionResult Read(byte[] bytes, int offset, out int readSize)
    {
      int count = (int) (ushort) ((uint) bytes[offset + 1] << 8) | (int) bytes[offset + 2];
      sbyte typeCode = (sbyte) bytes[offset + 3];
      byte[] data = new byte[count];
      readSize = count + 4;
      Buffer.BlockCopy((Array) bytes, offset + 4, (Array) data, 0, count);
      return new ExtensionResult(typeCode, data);
    }
  }
}
