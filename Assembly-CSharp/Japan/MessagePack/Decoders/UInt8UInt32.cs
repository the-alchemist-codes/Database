﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Decoders.UInt8UInt32
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Decoders
{
  internal sealed class UInt8UInt32 : IUInt32Decoder
  {
    internal static readonly IUInt32Decoder Instance = (IUInt32Decoder) new UInt8UInt32();

    private UInt8UInt32()
    {
    }

    public uint Read(byte[] bytes, int offset, out int readSize)
    {
      readSize = 2;
      return (uint) bytes[offset + 1];
    }
  }
}
