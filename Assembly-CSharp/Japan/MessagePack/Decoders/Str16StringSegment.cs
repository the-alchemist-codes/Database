﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Decoders.Str16StringSegment
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace MessagePack.Decoders
{
  internal sealed class Str16StringSegment : IStringSegmentDecoder
  {
    internal static readonly IStringSegmentDecoder Instance = (IStringSegmentDecoder) new Str16StringSegment();

    private Str16StringSegment()
    {
    }

    public ArraySegment<byte> Read(byte[] bytes, int offset, out int readSize)
    {
      int count = ((int) bytes[offset + 1] << 8) + (int) bytes[offset + 2];
      readSize = count + 3;
      return new ArraySegment<byte>(bytes, offset + 3, count);
    }
  }
}
