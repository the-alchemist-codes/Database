﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Decoders.True
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Decoders
{
  internal sealed class True : IBooleanDecoder
  {
    internal static IBooleanDecoder Instance = (IBooleanDecoder) new True();

    private True()
    {
    }

    public bool Read()
    {
      return true;
    }
  }
}
