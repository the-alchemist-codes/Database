﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Decoders.FixInt64
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Decoders
{
  internal sealed class FixInt64 : IInt64Decoder
  {
    internal static readonly IInt64Decoder Instance = (IInt64Decoder) new FixInt64();

    private FixInt64()
    {
    }

    public long Read(byte[] bytes, int offset, out int readSize)
    {
      readSize = 1;
      return (long) bytes[offset];
    }
  }
}
