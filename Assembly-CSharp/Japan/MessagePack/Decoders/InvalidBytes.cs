﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Decoders.InvalidBytes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace MessagePack.Decoders
{
  internal sealed class InvalidBytes : IBytesDecoder
  {
    internal static readonly IBytesDecoder Instance = (IBytesDecoder) new InvalidBytes();

    private InvalidBytes()
    {
    }

    public byte[] Read(byte[] bytes, int offset, out int readSize)
    {
      throw new InvalidOperationException(string.Format("code is invalid. code:{0} format:{1}", (object) bytes[offset], (object) MessagePackCode.ToFormatName(bytes[offset])));
    }
  }
}
