﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Decoders.FixDouble
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Decoders
{
  internal sealed class FixDouble : IDoubleDecoder
  {
    internal static readonly IDoubleDecoder Instance = (IDoubleDecoder) new FixDouble();

    private FixDouble()
    {
    }

    public double Read(byte[] bytes, int offset, out int readSize)
    {
      return (double) FixByte.Instance.Read(bytes, offset, out readSize);
    }
  }
}
