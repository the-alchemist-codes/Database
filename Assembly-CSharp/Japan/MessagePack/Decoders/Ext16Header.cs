﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Decoders.Ext16Header
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Decoders
{
  internal sealed class Ext16Header : IExtHeaderDecoder
  {
    internal static readonly IExtHeaderDecoder Instance = (IExtHeaderDecoder) new Ext16Header();

    private Ext16Header()
    {
    }

    public ExtensionHeader Read(byte[] bytes, int offset, out int readSize)
    {
      uint length = (uint) (ushort) ((uint) bytes[offset + 1] << 8) | (uint) bytes[offset + 2];
      sbyte typeCode = (sbyte) bytes[offset + 3];
      readSize = 4;
      return new ExtensionHeader(typeCode, length);
    }
  }
}
