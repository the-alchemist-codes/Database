﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Decoders.Int8SByte
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Decoders
{
  internal sealed class Int8SByte : ISByteDecoder
  {
    internal static readonly ISByteDecoder Instance = (ISByteDecoder) new Int8SByte();

    private Int8SByte()
    {
    }

    public sbyte Read(byte[] bytes, int offset, out int readSize)
    {
      readSize = 2;
      return (sbyte) bytes[offset + 1];
    }
  }
}
