﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Decoders.UInt32UInt32
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack.Decoders
{
  internal sealed class UInt32UInt32 : IUInt32Decoder
  {
    internal static readonly IUInt32Decoder Instance = (IUInt32Decoder) new UInt32UInt32();

    private UInt32UInt32()
    {
    }

    public uint Read(byte[] bytes, int offset, out int readSize)
    {
      readSize = 5;
      return (uint) ((int) bytes[offset + 1] << 24 | (int) bytes[offset + 2] << 16 | (int) bytes[offset + 3] << 8) | (uint) bytes[offset + 4];
    }
  }
}
