﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.MessagePackObjectAttribute
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace MessagePack
{
  [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = false, Inherited = true)]
  public class MessagePackObjectAttribute : Attribute
  {
    public MessagePackObjectAttribute(bool keyAsPropertyName = false)
    {
      this.KeyAsPropertyName = keyAsPropertyName;
    }

    public bool KeyAsPropertyName { get; private set; }
  }
}
