﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Unity.BoundsFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack.Formatters;
using System;
using UnityEngine;

namespace MessagePack.Unity
{
  public sealed class BoundsFormatter : IMessagePackFormatter<Bounds>, IMessagePackFormatter
  {
    public int Serialize(
      ref byte[] bytes,
      int offset,
      Bounds value,
      IFormatterResolver formatterResolver)
    {
      int num = offset;
      offset += MessagePackBinary.WriteFixedArrayHeaderUnsafe(ref bytes, offset, 2);
      offset += formatterResolver.GetFormatterWithVerify<Vector3>().Serialize(ref bytes, offset, ((Bounds) ref value).get_center(), formatterResolver);
      offset += formatterResolver.GetFormatterWithVerify<Vector3>().Serialize(ref bytes, offset, ((Bounds) ref value).get_size(), formatterResolver);
      return offset - num;
    }

    public Bounds Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      if (MessagePackBinary.IsNil(bytes, offset))
        throw new InvalidOperationException("typecode is null, struct not supported");
      int num1 = offset;
      int num2 = MessagePackBinary.ReadArrayHeader(bytes, offset, out readSize);
      offset += readSize;
      Vector3 vector3_1 = (Vector3) null;
      Vector3 vector3_2 = (Vector3) null;
      for (int index = 0; index < num2; ++index)
      {
        switch (index)
        {
          case 0:
            vector3_1 = formatterResolver.GetFormatterWithVerify<Vector3>().Deserialize(bytes, offset, formatterResolver, out readSize);
            break;
          case 1:
            vector3_2 = formatterResolver.GetFormatterWithVerify<Vector3>().Deserialize(bytes, offset, formatterResolver, out readSize);
            break;
          default:
            readSize = MessagePackBinary.ReadNextBlock(bytes, offset);
            break;
        }
        offset += readSize;
      }
      readSize = offset - num1;
      Bounds bounds;
      ((Bounds) ref bounds).\u002Ector(vector3_1, vector3_2);
      return bounds;
    }
  }
}
