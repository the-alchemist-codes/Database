﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Unity.Color32Formatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack.Formatters;
using System;
using UnityEngine;

namespace MessagePack.Unity
{
  public sealed class Color32Formatter : IMessagePackFormatter<Color32>, IMessagePackFormatter
  {
    public int Serialize(
      ref byte[] bytes,
      int offset,
      Color32 value,
      IFormatterResolver formatterResolver)
    {
      int num = offset;
      offset += MessagePackBinary.WriteFixedArrayHeaderUnsafe(ref bytes, offset, 4);
      offset += MessagePackBinary.WriteByte(ref bytes, offset, (byte) value.r);
      offset += MessagePackBinary.WriteByte(ref bytes, offset, (byte) value.g);
      offset += MessagePackBinary.WriteByte(ref bytes, offset, (byte) value.b);
      offset += MessagePackBinary.WriteByte(ref bytes, offset, (byte) value.a);
      return offset - num;
    }

    public Color32 Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      if (MessagePackBinary.IsNil(bytes, offset))
        throw new InvalidOperationException("typecode is null, struct not supported");
      int num1 = offset;
      int num2 = MessagePackBinary.ReadArrayHeader(bytes, offset, out readSize);
      offset += readSize;
      byte num3 = 0;
      byte num4 = 0;
      byte num5 = 0;
      byte num6 = 0;
      for (int index = 0; index < num2; ++index)
      {
        switch (index)
        {
          case 0:
            num3 = MessagePackBinary.ReadByte(bytes, offset, out readSize);
            break;
          case 1:
            num4 = MessagePackBinary.ReadByte(bytes, offset, out readSize);
            break;
          case 2:
            num5 = MessagePackBinary.ReadByte(bytes, offset, out readSize);
            break;
          case 3:
            num6 = MessagePackBinary.ReadByte(bytes, offset, out readSize);
            break;
          default:
            readSize = MessagePackBinary.ReadNextBlock(bytes, offset);
            break;
        }
        offset += readSize;
      }
      readSize = offset - num1;
      Color32 color32;
      ((Color32) ref color32).\u002Ector(num3, num4, num5, num6);
      color32.r = (__Null) (int) num3;
      color32.g = (__Null) (int) num4;
      color32.b = (__Null) (int) num5;
      color32.a = (__Null) (int) num6;
      return color32;
    }
  }
}
