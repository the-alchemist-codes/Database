﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Unity.LayerMaskFormatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack.Formatters;
using System;
using UnityEngine;

namespace MessagePack.Unity
{
  public sealed class LayerMaskFormatter : IMessagePackFormatter<LayerMask>, IMessagePackFormatter
  {
    public int Serialize(
      ref byte[] bytes,
      int offset,
      LayerMask value,
      IFormatterResolver formatterResolver)
    {
      int num = offset;
      offset += MessagePackBinary.WriteFixedArrayHeaderUnsafe(ref bytes, offset, 1);
      offset += MessagePackBinary.WriteInt32(ref bytes, offset, ((LayerMask) ref value).get_value());
      return offset - num;
    }

    public LayerMask Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      if (MessagePackBinary.IsNil(bytes, offset))
        throw new InvalidOperationException("typecode is null, struct not supported");
      int num1 = offset;
      int num2 = MessagePackBinary.ReadArrayHeader(bytes, offset, out readSize);
      offset += readSize;
      int num3 = 0;
      for (int index = 0; index < num2; ++index)
      {
        if (index == 0)
          num3 = MessagePackBinary.ReadInt32(bytes, offset, out readSize);
        else
          readSize = MessagePackBinary.ReadNextBlock(bytes, offset);
        offset += readSize;
      }
      readSize = offset - num1;
      LayerMask layerMask = (LayerMask) null;
      ((LayerMask) ref layerMask).set_value(num3);
      return layerMask;
    }
  }
}
