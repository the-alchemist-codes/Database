﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Unity.Matrix4x4Formatter
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack.Formatters;
using System;
using UnityEngine;

namespace MessagePack.Unity
{
  public sealed class Matrix4x4Formatter : IMessagePackFormatter<Matrix4x4>, IMessagePackFormatter
  {
    public int Serialize(
      ref byte[] bytes,
      int offset,
      Matrix4x4 value,
      IFormatterResolver formatterResolver)
    {
      int num = offset;
      offset += MessagePackBinary.WriteArrayHeader(ref bytes, offset, 16);
      offset += MessagePackBinary.WriteSingle(ref bytes, offset, (float) value.m00);
      offset += MessagePackBinary.WriteSingle(ref bytes, offset, (float) value.m10);
      offset += MessagePackBinary.WriteSingle(ref bytes, offset, (float) value.m20);
      offset += MessagePackBinary.WriteSingle(ref bytes, offset, (float) value.m30);
      offset += MessagePackBinary.WriteSingle(ref bytes, offset, (float) value.m01);
      offset += MessagePackBinary.WriteSingle(ref bytes, offset, (float) value.m11);
      offset += MessagePackBinary.WriteSingle(ref bytes, offset, (float) value.m21);
      offset += MessagePackBinary.WriteSingle(ref bytes, offset, (float) value.m31);
      offset += MessagePackBinary.WriteSingle(ref bytes, offset, (float) value.m02);
      offset += MessagePackBinary.WriteSingle(ref bytes, offset, (float) value.m12);
      offset += MessagePackBinary.WriteSingle(ref bytes, offset, (float) value.m22);
      offset += MessagePackBinary.WriteSingle(ref bytes, offset, (float) value.m32);
      offset += MessagePackBinary.WriteSingle(ref bytes, offset, (float) value.m03);
      offset += MessagePackBinary.WriteSingle(ref bytes, offset, (float) value.m13);
      offset += MessagePackBinary.WriteSingle(ref bytes, offset, (float) value.m23);
      offset += MessagePackBinary.WriteSingle(ref bytes, offset, (float) value.m33);
      return offset - num;
    }

    public Matrix4x4 Deserialize(
      byte[] bytes,
      int offset,
      IFormatterResolver formatterResolver,
      out int readSize)
    {
      if (MessagePackBinary.IsNil(bytes, offset))
        throw new InvalidOperationException("typecode is null, struct not supported");
      int num1 = offset;
      int num2 = MessagePackBinary.ReadArrayHeader(bytes, offset, out readSize);
      offset += readSize;
      float num3 = 0.0f;
      float num4 = 0.0f;
      float num5 = 0.0f;
      float num6 = 0.0f;
      float num7 = 0.0f;
      float num8 = 0.0f;
      float num9 = 0.0f;
      float num10 = 0.0f;
      float num11 = 0.0f;
      float num12 = 0.0f;
      float num13 = 0.0f;
      float num14 = 0.0f;
      float num15 = 0.0f;
      float num16 = 0.0f;
      float num17 = 0.0f;
      float num18 = 0.0f;
      for (int index = 0; index < num2; ++index)
      {
        switch (index)
        {
          case 0:
            num3 = MessagePackBinary.ReadSingle(bytes, offset, out readSize);
            break;
          case 1:
            num4 = MessagePackBinary.ReadSingle(bytes, offset, out readSize);
            break;
          case 2:
            num5 = MessagePackBinary.ReadSingle(bytes, offset, out readSize);
            break;
          case 3:
            num6 = MessagePackBinary.ReadSingle(bytes, offset, out readSize);
            break;
          case 4:
            num7 = MessagePackBinary.ReadSingle(bytes, offset, out readSize);
            break;
          case 5:
            num8 = MessagePackBinary.ReadSingle(bytes, offset, out readSize);
            break;
          case 6:
            num9 = MessagePackBinary.ReadSingle(bytes, offset, out readSize);
            break;
          case 7:
            num10 = MessagePackBinary.ReadSingle(bytes, offset, out readSize);
            break;
          case 8:
            num11 = MessagePackBinary.ReadSingle(bytes, offset, out readSize);
            break;
          case 9:
            num12 = MessagePackBinary.ReadSingle(bytes, offset, out readSize);
            break;
          case 10:
            num13 = MessagePackBinary.ReadSingle(bytes, offset, out readSize);
            break;
          case 11:
            num14 = MessagePackBinary.ReadSingle(bytes, offset, out readSize);
            break;
          case 12:
            num15 = MessagePackBinary.ReadSingle(bytes, offset, out readSize);
            break;
          case 13:
            num16 = MessagePackBinary.ReadSingle(bytes, offset, out readSize);
            break;
          case 14:
            num17 = MessagePackBinary.ReadSingle(bytes, offset, out readSize);
            break;
          case 15:
            num18 = MessagePackBinary.ReadSingle(bytes, offset, out readSize);
            break;
          default:
            readSize = MessagePackBinary.ReadNextBlock(bytes, offset);
            break;
        }
        offset += readSize;
      }
      readSize = offset - num1;
      Matrix4x4 matrix4x4 = (Matrix4x4) null;
      matrix4x4.m00 = (__Null) (double) num3;
      matrix4x4.m10 = (__Null) (double) num4;
      matrix4x4.m20 = (__Null) (double) num5;
      matrix4x4.m30 = (__Null) (double) num6;
      matrix4x4.m01 = (__Null) (double) num7;
      matrix4x4.m11 = (__Null) (double) num8;
      matrix4x4.m21 = (__Null) (double) num9;
      matrix4x4.m31 = (__Null) (double) num10;
      matrix4x4.m02 = (__Null) (double) num11;
      matrix4x4.m12 = (__Null) (double) num12;
      matrix4x4.m22 = (__Null) (double) num13;
      matrix4x4.m32 = (__Null) (double) num14;
      matrix4x4.m03 = (__Null) (double) num15;
      matrix4x4.m13 = (__Null) (double) num16;
      matrix4x4.m23 = (__Null) (double) num17;
      matrix4x4.m33 = (__Null) (double) num18;
      return matrix4x4;
    }
  }
}
