﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Float32Bits
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using System.Runtime.InteropServices;

namespace MessagePack
{
  [StructLayout(LayoutKind.Explicit)]
  internal struct Float32Bits
  {
    [FieldOffset(0)]
    public readonly float Value;
    [FieldOffset(0)]
    public readonly byte Byte0;
    [FieldOffset(1)]
    public readonly byte Byte1;
    [FieldOffset(2)]
    public readonly byte Byte2;
    [FieldOffset(3)]
    public readonly byte Byte3;

    public Float32Bits(float value)
    {
      this = new Float32Bits();
      this.Value = value;
    }

    public Float32Bits(byte[] bigEndianBytes, int offset)
    {
      this = new Float32Bits();
      if (BitConverter.IsLittleEndian)
      {
        this.Byte0 = bigEndianBytes[offset + 3];
        this.Byte1 = bigEndianBytes[offset + 2];
        this.Byte2 = bigEndianBytes[offset + 1];
        this.Byte3 = bigEndianBytes[offset];
      }
      else
      {
        this.Byte0 = bigEndianBytes[offset];
        this.Byte1 = bigEndianBytes[offset + 1];
        this.Byte2 = bigEndianBytes[offset + 2];
        this.Byte3 = bigEndianBytes[offset + 3];
      }
    }
  }
}
