﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.TinyJsonToken
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace MessagePack
{
  internal enum TinyJsonToken
  {
    None,
    StartObject,
    EndObject,
    StartArray,
    EndArray,
    Number,
    String,
    True,
    False,
    Null,
  }
}
