﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Resolvers.OldSpecResolver
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack.Formatters;
using MessagePack.Internal;

namespace MessagePack.Resolvers
{
  public sealed class OldSpecResolver : IFormatterResolver
  {
    public static readonly IFormatterResolver Instance = (IFormatterResolver) new OldSpecResolver();

    private OldSpecResolver()
    {
    }

    public IMessagePackFormatter<T> GetFormatter<T>()
    {
      return OldSpecResolver.FormatterCache<T>.formatter;
    }

    private static class FormatterCache<T>
    {
      public static readonly IMessagePackFormatter<T> formatter = (IMessagePackFormatter<T>) OldSpecResolverGetFormatterHelper.GetFormatter(typeof (T));
    }
  }
}
