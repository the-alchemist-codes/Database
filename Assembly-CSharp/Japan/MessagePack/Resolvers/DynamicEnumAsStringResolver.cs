﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Resolvers.DynamicEnumAsStringResolver
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack.Formatters;
using System;
using System.Reflection;

namespace MessagePack.Resolvers
{
  public sealed class DynamicEnumAsStringResolver : IFormatterResolver
  {
    public static readonly IFormatterResolver Instance = (IFormatterResolver) new DynamicEnumAsStringResolver();

    private DynamicEnumAsStringResolver()
    {
    }

    public IMessagePackFormatter<T> GetFormatter<T>()
    {
      return DynamicEnumAsStringResolver.FormatterCache<T>.formatter;
    }

    private static class FormatterCache<T>
    {
      public static readonly IMessagePackFormatter<T> formatter;

      static FormatterCache()
      {
        TypeInfo typeInfo1 = typeof (T).GetTypeInfo();
        if (typeInfo1.IsNullable())
        {
          TypeInfo typeInfo2 = typeInfo1.GenericTypeArguments[0].GetTypeInfo();
          if (!typeInfo2.IsEnum)
            return;
          object formatterDynamic = DynamicEnumAsStringResolver.Instance.GetFormatterDynamic(typeInfo2.AsType());
          if (formatterDynamic == null)
            return;
          DynamicEnumAsStringResolver.FormatterCache<T>.formatter = (IMessagePackFormatter<T>) Activator.CreateInstance(typeof (StaticNullableFormatter<>).MakeGenericType(typeInfo2.AsType()), formatterDynamic);
        }
        else
        {
          if (!typeInfo1.IsEnum)
            return;
          DynamicEnumAsStringResolver.FormatterCache<T>.formatter = (IMessagePackFormatter<T>) new EnumAsStringFormatter<T>();
        }
      }
    }
  }
}
