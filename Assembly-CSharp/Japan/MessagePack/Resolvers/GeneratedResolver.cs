﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Resolvers.GeneratedResolver
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack.Formatters;

namespace MessagePack.Resolvers
{
  public class GeneratedResolver : IFormatterResolver
  {
    public static readonly IFormatterResolver Instance = (IFormatterResolver) new GeneratedResolver();

    private GeneratedResolver()
    {
    }

    public IMessagePackFormatter<T> GetFormatter<T>()
    {
      return GeneratedResolver.FormatterCache<T>.formatter;
    }

    private static class FormatterCache<T>
    {
      public static readonly IMessagePackFormatter<T> formatter;

      static FormatterCache()
      {
        object formatter = GeneratedResolverGetFormatterHelper.GetFormatter(typeof (T));
        if (formatter == null)
          return;
        GeneratedResolver.FormatterCache<T>.formatter = (IMessagePackFormatter<T>) formatter;
      }
    }
  }
}
