﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Resolvers.GeneratedResolverGetFormatterHelper
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack.Formatters;
using MessagePack.Formatters.SRPG;
using SRPG;
using System.Collections.Generic;

namespace MessagePack.Resolvers
{
  internal static class GeneratedResolverGetFormatterHelper
  {
    private static readonly Dictionary<System.Type, int> lookup = new Dictionary<System.Type, int>(1176)
    {
      {
        typeof (List<int>),
        0
      },
      {
        typeof (EquipSkillSetting[]),
        1
      },
      {
        typeof (EquipAbilitySetting[]),
        2
      },
      {
        typeof (List<AIAction>),
        3
      },
      {
        typeof (AIPatrolPoint[]),
        4
      },
      {
        typeof (List<OString>),
        5
      },
      {
        typeof (List<UnitEntryTrigger>),
        6
      },
      {
        typeof (List<MultiPlayResumeBuff.ResistStatus>),
        7
      },
      {
        typeof (MultiPlayResumeAbilChg.Data[]),
        8
      },
      {
        typeof (MultiPlayResumeBuff[]),
        9
      },
      {
        typeof (MultiPlayResumeShield[]),
        10
      },
      {
        typeof (MultiPlayResumeMhmDmg[]),
        11
      },
      {
        typeof (MultiPlayResumeFtgt[]),
        12
      },
      {
        typeof (MultiPlayResumeAbilChg[]),
        13
      },
      {
        typeof (MultiPlayResumeAddedAbil[]),
        14
      },
      {
        typeof (List<MultiPlayResumeProtect>),
        15
      },
      {
        typeof (MultiPlayResumeUnitData[]),
        16
      },
      {
        typeof (MultiPlayGimmickEventParam[]),
        17
      },
      {
        typeof (MultiPlayTrickParam[]),
        18
      },
      {
        typeof (List<AttackDetailTypes>),
        19
      },
      {
        typeof (List<string>),
        20
      },
      {
        typeof (List<QuestClearUnlockUnitDataParam>),
        21
      },
      {
        typeof (OShort[]),
        22
      },
      {
        typeof (List<TokkouValue>),
        23
      },
      {
        typeof (RecipeItem[]),
        24
      },
      {
        typeof (ReturnItem[]),
        25
      },
      {
        typeof (RarityEquipEnhanceParam.RankParam[]),
        26
      },
      {
        typeof (EquipData[]),
        27
      },
      {
        typeof (List<AbilityData>),
        28
      },
      {
        typeof (ConceptCardEffectsParam[]),
        29
      },
      {
        typeof (List<ConceptLimitUpItemParam>),
        30
      },
      {
        typeof (BuffEffectParam.Buff[]),
        31
      },
      {
        typeof (List<BuffEffect.BuffTarget>),
        32
      },
      {
        typeof (List<ConceptCardEquipEffect>),
        33
      },
      {
        typeof (ConceptCardData[]),
        34
      },
      {
        typeof (List<RuneBuffDataEvoState>),
        35
      },
      {
        typeof (RuneData[]),
        36
      },
      {
        typeof (BuffEffect.BuffValues[]),
        37
      },
      {
        typeof (OString[]),
        38
      },
      {
        typeof (JobRankParam[]),
        39
      },
      {
        typeof (LearningSkill[]),
        40
      },
      {
        typeof (List<InspSkillTriggerParam.TriggerData>),
        41
      },
      {
        typeof (List<InspSkillTriggerParam>),
        42
      },
      {
        typeof (List<InspSkillParam>),
        43
      },
      {
        typeof (List<InspirationSkillData>),
        44
      },
      {
        typeof (ArtifactData[]),
        45
      },
      {
        typeof (TobiraLearnAbilityParam[]),
        46
      },
      {
        typeof (List<TobiraData>),
        47
      },
      {
        typeof (JobData[]),
        48
      },
      {
        typeof (QuestClearUnlockUnitDataParam[]),
        49
      },
      {
        typeof (List<SkillDeriveParam>),
        50
      },
      {
        typeof (List<AbilityDeriveParam>),
        51
      },
      {
        typeof (SkillAbilityDeriveTriggerParam[]),
        52
      },
      {
        typeof (EUnitCondition[]),
        53
      },
      {
        typeof (List<SkillData>),
        54
      },
      {
        typeof (List<Unit>),
        55
      },
      {
        typeof (List<BuffAttachment.ResistStatusBuff>),
        56
      },
      {
        typeof (List<BuffAttachment>),
        57
      },
      {
        typeof (List<CondAttachment>),
        58
      },
      {
        typeof (SkillCategory[]),
        59
      },
      {
        typeof (ParamTypes[]),
        60
      },
      {
        typeof (List<Unit.DropItem>),
        61
      },
      {
        typeof (List<Unit.UnitShield>),
        62
      },
      {
        typeof (List<Unit.UnitProtect>),
        63
      },
      {
        typeof (List<Unit.UnitMhmDamage>),
        64
      },
      {
        typeof (List<Unit.UnitInsp>),
        65
      },
      {
        typeof (List<Unit.UnitForcedTargeting>),
        66
      },
      {
        typeof (List<Unit.AbilityChange.Data>),
        67
      },
      {
        typeof (List<Unit.AbilityChange>),
        68
      },
      {
        typeof (Json_InspirationSkill[]),
        69
      },
      {
        typeof (JSON_GuildRaidMailListItem[]),
        70
      },
      {
        typeof (Json_CollaboSkill[]),
        71
      },
      {
        typeof (Json_Equip[]),
        72
      },
      {
        typeof (Json_Ability[]),
        73
      },
      {
        typeof (Json_Artifact[]),
        74
      },
      {
        typeof (Json_InspirationSkillExt[]),
        75
      },
      {
        typeof (Json_Job[]),
        76
      },
      {
        typeof (Json_UnitJob[]),
        77
      },
      {
        typeof (JSON_ConceptCard[]),
        78
      },
      {
        typeof (Json_Tobira[]),
        79
      },
      {
        typeof (Json_RuneBuffData[]),
        80
      },
      {
        typeof (Json_RuneData[]),
        81
      },
      {
        typeof (Json_Unit[]),
        82
      },
      {
        typeof (JSON_GuildRaidRankingMemberBoss[]),
        83
      },
      {
        typeof (Json_Item[]),
        84
      },
      {
        typeof (Json_Gift[]),
        85
      },
      {
        typeof (Json_Mail[]),
        86
      },
      {
        typeof (Json_Party[]),
        87
      },
      {
        typeof (Json_Friend[]),
        88
      },
      {
        typeof (Json_Skin[]),
        89
      },
      {
        typeof (Json_LoginBonus[]),
        90
      },
      {
        typeof (Json_PremiumLoginBonusItem[]),
        91
      },
      {
        typeof (Json_PremiumLoginBonus[]),
        92
      },
      {
        typeof (Json_LoginBonusTable[]),
        93
      },
      {
        typeof (Json_MultiFuids[]),
        94
      },
      {
        typeof (Json_VersusCount[]),
        95
      },
      {
        typeof (Json_ExpireItem[]),
        96
      },
      {
        typeof (JSON_TrophyProgress[]),
        97
      },
      {
        typeof (JSON_UnitOverWriteData[]),
        98
      },
      {
        typeof (JSON_PartyOverWrite[]),
        99
      },
      {
        typeof (Json_TrophyConceptCard[]),
        100
      },
      {
        typeof (JSON_SupportRanking[]),
        101
      },
      {
        typeof (JSON_SupportUnitRanking[]),
        102
      },
      {
        typeof (BattleCore.Json_BtlDrop[]),
        103
      },
      {
        typeof (BattleCore.Json_BtlDrop[][]),
        104
      },
      {
        typeof (Json_BtlRewardConceptCard[]),
        105
      },
      {
        typeof (JSON_QuestProgress[]),
        106
      },
      {
        typeof (ReqDrawCard.CardInfo[]),
        107
      },
      {
        typeof (ReqDrawCard.CardInfo.Card[]),
        108
      },
      {
        typeof (JSON_FixParam[]),
        109
      },
      {
        typeof (JSON_UnitParam[]),
        110
      },
      {
        typeof (JSON_UnitJobOverwriteParam[]),
        111
      },
      {
        typeof (JSON_SkillParam[]),
        112
      },
      {
        typeof (JSON_BuffEffectParam[]),
        113
      },
      {
        typeof (JSON_CondEffectParam[]),
        114
      },
      {
        typeof (JSON_AbilityParam[]),
        115
      },
      {
        typeof (JSON_ItemParam[]),
        116
      },
      {
        typeof (JSON_ArtifactParam[]),
        117
      },
      {
        typeof (JSON_WeaponParam[]),
        118
      },
      {
        typeof (JSON_RecipeParam[]),
        119
      },
      {
        typeof (JSON_JobRankParam[]),
        120
      },
      {
        typeof (JSON_JobParam[]),
        121
      },
      {
        typeof (JSON_JobSetParam[]),
        122
      },
      {
        typeof (JSON_EvaluationParam[]),
        123
      },
      {
        typeof (JSON_AIParam[]),
        124
      },
      {
        typeof (JSON_GeoParam[]),
        125
      },
      {
        typeof (JSON_RarityParam[]),
        126
      },
      {
        typeof (JSON_ShopParam[]),
        (int) sbyte.MaxValue
      },
      {
        typeof (JSON_PlayerParam[]),
        128
      },
      {
        typeof (JSON_GrowCurve[]),
        129
      },
      {
        typeof (JSON_GrowParam[]),
        130
      },
      {
        typeof (JSON_LocalNotificationParam[]),
        131
      },
      {
        typeof (JSON_TrophyCategoryParam[]),
        132
      },
      {
        typeof (JSON_ChallengeCategoryParam[]),
        133
      },
      {
        typeof (JSON_TrophyParam[]),
        134
      },
      {
        typeof (JSON_UnlockParam[]),
        135
      },
      {
        typeof (JSON_VipParam[]),
        136
      },
      {
        typeof (JSON_ArenaWinResult[]),
        137
      },
      {
        typeof (JSON_ArenaResult[]),
        138
      },
      {
        typeof (JSON_StreamingMovie[]),
        139
      },
      {
        typeof (JSON_BannerParam[]),
        140
      },
      {
        typeof (JSON_QuestClearUnlockUnitDataParam[]),
        141
      },
      {
        typeof (JSON_AwardParam[]),
        142
      },
      {
        typeof (JSON_LoginInfoParam[]),
        143
      },
      {
        typeof (JSON_CollaboSkillParam[]),
        144
      },
      {
        typeof (JSON_TrickParam[]),
        145
      },
      {
        typeof (JSON_BreakObjParam[]),
        146
      },
      {
        typeof (JSON_VersusMatchingParam[]),
        147
      },
      {
        typeof (JSON_VersusMatchCondParam[]),
        148
      },
      {
        typeof (JSON_TowerScoreThreshold[]),
        149
      },
      {
        typeof (JSON_TowerScore[]),
        150
      },
      {
        typeof (JSON_FriendPresentItemParam[]),
        151
      },
      {
        typeof (JSON_WeatherParam[]),
        152
      },
      {
        typeof (JSON_UnitUnlockTimeParam[]),
        153
      },
      {
        typeof (JSON_TobiraLearnAbilityParam[]),
        154
      },
      {
        typeof (JSON_TobiraParam[]),
        155
      },
      {
        typeof (JSON_TobiraCategoriesParam[]),
        156
      },
      {
        typeof (JSON_TobiraConditionParam[]),
        157
      },
      {
        typeof (JSON_TobiraCondsParam[]),
        158
      },
      {
        typeof (JSON_TobiraCondsUnitParam.JobCond[]),
        159
      },
      {
        typeof (JSON_TobiraCondsUnitParam[]),
        160
      },
      {
        typeof (JSON_TobiraRecipeMaterialParam[]),
        161
      },
      {
        typeof (JSON_TobiraRecipeParam[]),
        162
      },
      {
        typeof (JSON_ConceptCardEquipParam[]),
        163
      },
      {
        typeof (JSON_ConceptCardParam[]),
        164
      },
      {
        typeof (JSON_ConceptCardConditionsParam[]),
        165
      },
      {
        typeof (JSON_ConceptCardTrustRewardItemParam[]),
        166
      },
      {
        typeof (JSON_ConceptCardTrustRewardParam[]),
        167
      },
      {
        typeof (JSON_ConceptCardLsBuffCoefParam[]),
        168
      },
      {
        typeof (JSON_ConceptCardGroup[]),
        169
      },
      {
        typeof (JSON_ConceptLimitUpItem[]),
        170
      },
      {
        typeof (JSON_UnitGroupParam[]),
        171
      },
      {
        typeof (JSON_JobGroupParam[]),
        172
      },
      {
        typeof (JSON_StatusCoefficientParam[]),
        173
      },
      {
        typeof (JSON_CustomTargetParam[]),
        174
      },
      {
        typeof (JSON_SkillAbilityDeriveParam[]),
        175
      },
      {
        typeof (JSON_RaidPeriodParam[]),
        176
      },
      {
        typeof (JSON_RaidPeriodTimeScheduleParam[]),
        177
      },
      {
        typeof (JSON_RaidPeriodTimeParam[]),
        178
      },
      {
        typeof (JSON_RaidAreaParam[]),
        179
      },
      {
        typeof (JSON_RaidBossParam[]),
        180
      },
      {
        typeof (JSON_RaidBattleRewardWeightParam[]),
        181
      },
      {
        typeof (JSON_RaidBattleRewardParam[]),
        182
      },
      {
        typeof (JSON_RaidBeatRewardDataParam[]),
        183
      },
      {
        typeof (JSON_RaidBeatRewardParam[]),
        184
      },
      {
        typeof (JSON_RaidDamageRatioRewardRatioParam[]),
        185
      },
      {
        typeof (JSON_RaidDamageRatioRewardParam[]),
        186
      },
      {
        typeof (JSON_RaidDamageAmountRewardAmountParam[]),
        187
      },
      {
        typeof (JSON_RaidDamageAmountRewardParam[]),
        188
      },
      {
        typeof (JSON_RaidAreaClearRewardDataParam[]),
        189
      },
      {
        typeof (JSON_RaidAreaClearRewardParam[]),
        190
      },
      {
        typeof (JSON_RaidCompleteRewardDataParam[]),
        191
      },
      {
        typeof (JSON_RaidCompleteRewardParam[]),
        192
      },
      {
        typeof (JSON_RaidReward[]),
        193
      },
      {
        typeof (JSON_RaidRewardParam[]),
        194
      },
      {
        typeof (JSON_TipsParam[]),
        195
      },
      {
        typeof (JSON_GuildEmblemParam[]),
        196
      },
      {
        typeof (JSON_GuildFacilityEffectParam[]),
        197
      },
      {
        typeof (JSON_GuildFacilityParam[]),
        198
      },
      {
        typeof (JSON_GuildFacilityLvParam[]),
        199
      },
      {
        typeof (JSON_ConvertUnitPieceExcludeParam[]),
        200
      },
      {
        typeof (JSON_PremiumParam[]),
        201
      },
      {
        typeof (JSON_BuyCoinShopParam[]),
        202
      },
      {
        typeof (JSON_BuyCoinProductParam[]),
        203
      },
      {
        typeof (JSON_BuyCoinRewardItemParam[]),
        204
      },
      {
        typeof (JSON_BuyCoinRewardParam[]),
        205
      },
      {
        typeof (JSON_BuyCoinProductConvertParam[]),
        206
      },
      {
        typeof (JSON_DynamicTransformUnitParam[]),
        207
      },
      {
        typeof (JSON_RecommendedArtifactParam[]),
        208
      },
      {
        typeof (JSON_SkillMotionDataParam[]),
        209
      },
      {
        typeof (JSON_SkillMotionParam[]),
        210
      },
      {
        typeof (JSON_DependStateSpcEffParam[]),
        211
      },
      {
        typeof (JSON_InspSkillDerivation[]),
        212
      },
      {
        typeof (JSON_InspSkillParam[]),
        213
      },
      {
        typeof (JSON_InspSkillTriggerParam.JSON_TriggerData[]),
        214
      },
      {
        typeof (JSON_InspSkillTriggerParam[]),
        215
      },
      {
        typeof (JSON_InspSkillCostParam[]),
        216
      },
      {
        typeof (JSON_InspSkillLvUpCostParam.JSON_CostData[]),
        217
      },
      {
        typeof (JSON_InspSkillLvUpCostParam[]),
        218
      },
      {
        typeof (JSON_HighlightResource[]),
        219
      },
      {
        typeof (JSON_HighlightParam[]),
        220
      },
      {
        typeof (JSON_HighlightGiftData[]),
        221
      },
      {
        typeof (JSON_HighlightGift[]),
        222
      },
      {
        typeof (JSON_GenesisParam[]),
        223
      },
      {
        typeof (JSON_CoinBuyUseBonusParam[]),
        224
      },
      {
        typeof (JSON_CoinBuyUseBonusContentParam[]),
        225
      },
      {
        typeof (JSON_CoinBuyUseBonusRewardSetParam[]),
        226
      },
      {
        typeof (JSON_CoinBuyUseBonusItemParam[]),
        227
      },
      {
        typeof (JSON_CoinBuyUseBonusRewardParam[]),
        228
      },
      {
        typeof (JSON_UnitRentalNotificationDataParam[]),
        229
      },
      {
        typeof (JSON_UnitRentalNotificationParam[]),
        230
      },
      {
        typeof (JSON_UnitRentalParam.QuestInfo[]),
        231
      },
      {
        typeof (JSON_UnitRentalParam[]),
        232
      },
      {
        typeof (JSON_DrawCardRewardParam.Data[]),
        233
      },
      {
        typeof (JSON_DrawCardRewardParam[]),
        234
      },
      {
        typeof (JSON_DrawCardParam.DrawInfo[]),
        235
      },
      {
        typeof (JSON_DrawCardParam[]),
        236
      },
      {
        typeof (JSON_TrophyStarMissionRewardParam.Data[]),
        237
      },
      {
        typeof (JSON_TrophyStarMissionRewardParam[]),
        238
      },
      {
        typeof (JSON_TrophyStarMissionParam.StarSetParam[]),
        239
      },
      {
        typeof (JSON_TrophyStarMissionParam[]),
        240
      },
      {
        typeof (JSON_UnitPieceShopParam[]),
        241
      },
      {
        typeof (JSON_UnitPieceShopGroupCost[]),
        242
      },
      {
        typeof (JSON_UnitPieceShopGroupParam[]),
        243
      },
      {
        typeof (JSON_TwitterMessageDetailParam[]),
        244
      },
      {
        typeof (JSON_TwitterMessageParam[]),
        245
      },
      {
        typeof (JSON_FilterConceptCardConditionParam[]),
        246
      },
      {
        typeof (JSON_FilterConceptCardParam[]),
        247
      },
      {
        typeof (JSON_FilterRuneConditionParam[]),
        248
      },
      {
        typeof (JSON_FilterRuneParam[]),
        249
      },
      {
        typeof (JSON_FilterUnitConditionParam[]),
        250
      },
      {
        typeof (JSON_FilterUnitParam[]),
        251
      },
      {
        typeof (JSON_FilterArtifactParam.Condition[]),
        252
      },
      {
        typeof (JSON_FilterArtifactParam[]),
        253
      },
      {
        typeof (JSON_SortRuneConditionParam[]),
        254
      },
      {
        typeof (JSON_SortRuneParam[]),
        (int) byte.MaxValue
      },
      {
        typeof (JSON_RuneParam[]),
        256
      },
      {
        typeof (JSON_RuneLottery[]),
        257
      },
      {
        typeof (JSON_RuneLotteryBaseState[]),
        258
      },
      {
        typeof (JSON_RuneLotteryEvoState[]),
        259
      },
      {
        typeof (JSON_RuneDisassembly[]),
        260
      },
      {
        typeof (JSON_RuneMaterial[]),
        261
      },
      {
        typeof (JSON_RuneCost[]),
        262
      },
      {
        typeof (JSON_RuneSetEffState[]),
        263
      },
      {
        typeof (JSON_RuneSetEff[]),
        264
      },
      {
        typeof (JSON_JukeBoxParam[]),
        265
      },
      {
        typeof (JSON_JukeBoxSectionParam[]),
        266
      },
      {
        typeof (JSON_UnitSameGroupParam[]),
        267
      },
      {
        typeof (JSON_AutoRepeatQuestBoxParam[]),
        268
      },
      {
        typeof (JSON_GuildAttendRewardDetail[]),
        269
      },
      {
        typeof (JSON_GuildAttendParam[]),
        270
      },
      {
        typeof (JSON_GuildAttendReward[]),
        271
      },
      {
        typeof (JSON_GuildAttendRewardParam[]),
        272
      },
      {
        typeof (JSON_GuildRoleBonusDetail[]),
        273
      },
      {
        typeof (JSON_GuildRoleBonus[]),
        274
      },
      {
        typeof (JSON_GUildRoleBonusReward[]),
        275
      },
      {
        typeof (JSON_GuildRoleBonusRewardParam[]),
        276
      },
      {
        typeof (JSON_ResetCostInfoParam[]),
        277
      },
      {
        typeof (JSON_ResetCostParam[]),
        278
      },
      {
        typeof (JSON_ProtectSkillParam[]),
        279
      },
      {
        typeof (JSON_ReplacePeriod[]),
        280
      },
      {
        typeof (JSON_ReplaceSprite[]),
        281
      },
      {
        typeof (JSON_InitPlayer[]),
        282
      },
      {
        typeof (JSON_InitUnit[]),
        283
      },
      {
        typeof (JSON_InitItem[]),
        284
      },
      {
        typeof (JSON_SectionParam[]),
        285
      },
      {
        typeof (JSON_ArchiveItemsParam[]),
        286
      },
      {
        typeof (JSON_ArchiveParam[]),
        287
      },
      {
        typeof (JSON_ChapterParam[]),
        288
      },
      {
        typeof (JSON_MapParam[]),
        289
      },
      {
        typeof (JSON_QuestParam[]),
        290
      },
      {
        typeof (JSON_InnerObjective[]),
        291
      },
      {
        typeof (JSON_ObjectiveParam[]),
        292
      },
      {
        typeof (JSON_MagnificationParam[]),
        293
      },
      {
        typeof (JSON_QuestCondParam[]),
        294
      },
      {
        typeof (JSON_QuestPartyParam[]),
        295
      },
      {
        typeof (JSON_QuestCampaignParentParam[]),
        296
      },
      {
        typeof (JSON_QuestCampaignChildParam[]),
        297
      },
      {
        typeof (JSON_QuestCampaignTrust[]),
        298
      },
      {
        typeof (JSON_QuestCampaignInspSkill[]),
        299
      },
      {
        typeof (JSON_TowerFloorParam[]),
        300
      },
      {
        typeof (JSON_TowerRewardItem[]),
        301
      },
      {
        typeof (JSON_TowerRewardParam[]),
        302
      },
      {
        typeof (JSON_TowerRoundRewardItem[]),
        303
      },
      {
        typeof (JSON_TowerRoundRewardParam[]),
        304
      },
      {
        typeof (JSON_TowerParam[]),
        305
      },
      {
        typeof (JSON_VersusTowerParam[]),
        306
      },
      {
        typeof (JSON_VersusSchedule[]),
        307
      },
      {
        typeof (JSON_VersusCoin[]),
        308
      },
      {
        typeof (JSON_MultiTowerFloorParam[]),
        309
      },
      {
        typeof (JSON_MultiTowerRewardItem[]),
        310
      },
      {
        typeof (JSON_MultiTowerRewardParam[]),
        311
      },
      {
        typeof (JSON_MapEffectParam[]),
        312
      },
      {
        typeof (JSON_WeatherSetParam[]),
        313
      },
      {
        typeof (JSON_RankingQuestParam[]),
        314
      },
      {
        typeof (JSON_RankingQuestScheduleParam[]),
        315
      },
      {
        typeof (JSON_RankingQuestRewardParam[]),
        316
      },
      {
        typeof (JSON_VersusWinBonusReward[]),
        317
      },
      {
        typeof (JSON_VersusFirstWinBonus[]),
        318
      },
      {
        typeof (JSON_VersusStreakWinSchedule[]),
        319
      },
      {
        typeof (JSON_VersusStreakWinBonus[]),
        320
      },
      {
        typeof (JSON_VersusRule[]),
        321
      },
      {
        typeof (JSON_VersusCoinCampParam[]),
        322
      },
      {
        typeof (JSON_VersusEnableTimeScheduleParam[]),
        323
      },
      {
        typeof (JSON_VersusEnableTimeParam[]),
        324
      },
      {
        typeof (JSON_VersusRankParam[]),
        325
      },
      {
        typeof (JSON_VersusRankClassParam[]),
        326
      },
      {
        typeof (JSON_VersusRankRankingRewardParam[]),
        327
      },
      {
        typeof (JSON_VersusRankRewardRewardParam[]),
        328
      },
      {
        typeof (JSON_VersusRankRewardParam[]),
        329
      },
      {
        typeof (JSON_VersusRankMissionScheduleParam[]),
        330
      },
      {
        typeof (JSON_VersusRankMissionParam[]),
        331
      },
      {
        typeof (JSON_QuestLobbyNewsParam[]),
        332
      },
      {
        typeof (JSON_GuerrillaShopAdventQuestParam[]),
        333
      },
      {
        typeof (JSON_GuerrillaShopScheduleAdventParam[]),
        334
      },
      {
        typeof (JSON_GuerrillaShopScheduleParam[]),
        335
      },
      {
        typeof (JSON_VersusDraftDeckParam[]),
        336
      },
      {
        typeof (JSON_VersusDraftUnitParam[]),
        337
      },
      {
        typeof (JSON_GenesisStarRewardParam[]),
        338
      },
      {
        typeof (JSON_GenesisStarParam[]),
        339
      },
      {
        typeof (JSON_GenesisChapterModeInfoParam[]),
        340
      },
      {
        typeof (JSON_GenesisChapterParam[]),
        341
      },
      {
        typeof (JSON_GenesisRewardDataParam[]),
        342
      },
      {
        typeof (JSON_GenesisRewardParam[]),
        343
      },
      {
        typeof (JSON_GenesisLapBossParam.LapInfo[]),
        344
      },
      {
        typeof (JSON_GenesisLapBossParam[]),
        345
      },
      {
        typeof (JSON_AdvanceStarRewardParam[]),
        346
      },
      {
        typeof (JSON_AdvanceStarParam[]),
        347
      },
      {
        typeof (JSON_AdvanceEventModeInfoParam[]),
        348
      },
      {
        typeof (JSON_AdvanceEventParam[]),
        349
      },
      {
        typeof (JSON_AdvanceRewardDataParam[]),
        350
      },
      {
        typeof (JSON_AdvanceRewardParam[]),
        351
      },
      {
        typeof (JSON_AdvanceLapBossParam.LapInfo[]),
        352
      },
      {
        typeof (JSON_AdvanceLapBossParam[]),
        353
      },
      {
        typeof (JSON_GuildRaidBossParam[]),
        354
      },
      {
        typeof (JSON_GuildRaidCoolDaysParam[]),
        355
      },
      {
        typeof (JSON_GuildRaidScoreDataParam[]),
        356
      },
      {
        typeof (JSON_GuildRaidScoreParam[]),
        357
      },
      {
        typeof (JSON_GuildRaidPeriodTime[]),
        358
      },
      {
        typeof (JSON_GuildRaidPeriodParam[]),
        359
      },
      {
        typeof (JSON_GuildRaidReward[]),
        360
      },
      {
        typeof (JSON_GuildRaidRewardParam[]),
        361
      },
      {
        typeof (JSON_GuildRaidRewardDmgRankingRankParam[]),
        362
      },
      {
        typeof (JSON_GuildRaidRewardDmgRankingParam[]),
        363
      },
      {
        typeof (JSON_GuildRaidRewardDmgRatio[]),
        364
      },
      {
        typeof (JSON_GuildRaidRewardDmgRatioParam[]),
        365
      },
      {
        typeof (JSON_GuildRaidRewardRound[]),
        366
      },
      {
        typeof (JSON_GuildRaidRewardRoundParam[]),
        367
      },
      {
        typeof (JSON_GuildRaidRewardRankingDataParam[]),
        368
      },
      {
        typeof (JSON_GuildRaidRewardRankingParam[]),
        369
      },
      {
        typeof (JSON_GvGPeriodParam[]),
        370
      },
      {
        typeof (JSON_GvGNodeParam[]),
        371
      },
      {
        typeof (JSON_GvGNPCPartyDetailParam[]),
        372
      },
      {
        typeof (JSON_GvGNPCPartyParam[]),
        373
      },
      {
        typeof (JSON_GvGNPCUnitParam[]),
        374
      },
      {
        typeof (JSON_GvGRewardRankingDetailParam[]),
        375
      },
      {
        typeof (JSON_GvGRewardRankingParam[]),
        376
      },
      {
        typeof (JSON_GvGRewardDetailParam[]),
        377
      },
      {
        typeof (JSON_GvGRewardParam[]),
        378
      },
      {
        typeof (JSON_GvGRuleParam[]),
        379
      },
      {
        typeof (JSON_SupportHistory[]),
        380
      },
      {
        typeof (Json_UnitPieceShopItem[]),
        381
      },
      {
        typeof (Json_MyPhotonPlayerBinaryParam.UnitDataElem[]),
        382
      },
      {
        typeof (Json_MyPhotonPlayerBinaryParam[]),
        383
      },
      {
        typeof (JukeBoxWindow.ResPlayList[]),
        384
      },
      {
        typeof (JSON_GuildFacilityData[]),
        385
      },
      {
        typeof (JSON_GuildRaidBattleLog[]),
        386
      },
      {
        typeof (JSON_GuildRaidChallengingPlayer[]),
        387
      },
      {
        typeof (JSON_GuildRaidRanking[]),
        388
      },
      {
        typeof (JSON_GuildRaidRankingDamage[]),
        389
      },
      {
        typeof (JSON_GuildRaidRankingMember[]),
        390
      },
      {
        typeof (JSON_GuildRaidReport[]),
        391
      },
      {
        typeof (JSON_GvGNodeData[]),
        392
      },
      {
        typeof (JSON_ViewGuild[]),
        393
      },
      {
        typeof (JSON_GvGPartyUnit[]),
        394
      },
      {
        typeof (JSON_GvGPartyNPC[]),
        395
      },
      {
        typeof (JSON_GvGParty[]),
        396
      },
      {
        typeof (JSON_GvGBeatRanking[]),
        397
      },
      {
        typeof (JSON_GvGRankingData[]),
        398
      },
      {
        typeof (Json_ChatChannelMasterParam[]),
        399
      },
      {
        typeof (Json_RuneEnforceGaugeData[]),
        400
      },
      {
        typeof (ReqRuneDisassembly.Response.Rewards[]),
        401
      },
      {
        typeof (ReqTrophyStarMissionGetReward.Response.JSON_StarMissionConceptCard[]),
        402
      },
      {
        typeof (JSON_ProductParam[]),
        403
      },
      {
        typeof (JSON_ProductBuyCoinParam[]),
        404
      },
      {
        typeof (RuneSetEffState[]),
        405
      },
      {
        typeof (RuneCost[]),
        406
      },
      {
        typeof (List<RuneDisassembly.Materials>),
        407
      },
      {
        typeof (List<ReplacePeriod>),
        408
      },
      {
        typeof (EBattleRewardType),
        409
      },
      {
        typeof (eAIActionNoExecAct),
        410
      },
      {
        typeof (eAIActionNextTurnAct),
        411
      },
      {
        typeof (eMapUnitCtCalcType),
        412
      },
      {
        typeof (AIActionType),
        413
      },
      {
        typeof (EEventTrigger),
        414
      },
      {
        typeof (EEventType),
        415
      },
      {
        typeof (EEventGimmick),
        416
      },
      {
        typeof (ProtectTypes),
        417
      },
      {
        typeof (DamageTypes),
        418
      },
      {
        typeof (ESkillType),
        419
      },
      {
        typeof (ESkillTiming),
        420
      },
      {
        typeof (ESkillCondition),
        421
      },
      {
        typeof (ELineType),
        422
      },
      {
        typeof (ESelectType),
        423
      },
      {
        typeof (ECastTypes),
        424
      },
      {
        typeof (ESkillTarget),
        425
      },
      {
        typeof (SkillEffectTypes),
        426
      },
      {
        typeof (SkillParamCalcTypes),
        427
      },
      {
        typeof (EElement),
        428
      },
      {
        typeof (AttackTypes),
        429
      },
      {
        typeof (AttackDetailTypes),
        430
      },
      {
        typeof (ShieldTypes),
        431
      },
      {
        typeof (JewelDamageTypes),
        432
      },
      {
        typeof (eKnockBackDir),
        433
      },
      {
        typeof (eKnockBackDs),
        434
      },
      {
        typeof (eDamageDispType),
        435
      },
      {
        typeof (eTeleportType),
        436
      },
      {
        typeof (eTrickSetType),
        437
      },
      {
        typeof (eAbsorbAndGive),
        438
      },
      {
        typeof (eSkillTargetEx),
        439
      },
      {
        typeof (eTeleportSkillPos),
        440
      },
      {
        typeof (QuestClearUnlockUnitDataParam.EUnlockTypes),
        441
      },
      {
        typeof (ESex),
        442
      },
      {
        typeof (EUnitType),
        443
      },
      {
        typeof (JobTypes),
        444
      },
      {
        typeof (RoleTypes),
        445
      },
      {
        typeof (EItemType),
        446
      },
      {
        typeof (EItemTabType),
        447
      },
      {
        typeof (GalleryVisibilityType),
        448
      },
      {
        typeof (eCardType),
        449
      },
      {
        typeof (EffectCheckTargets),
        450
      },
      {
        typeof (EffectCheckTimings),
        451
      },
      {
        typeof (EAppType),
        452
      },
      {
        typeof (EEffRange),
        453
      },
      {
        typeof (BuffFlags),
        454
      },
      {
        typeof (ParamTypes),
        455
      },
      {
        typeof (BuffTypes),
        456
      },
      {
        typeof (ArtifactTypes),
        457
      },
      {
        typeof (BuffMethodTypes),
        458
      },
      {
        typeof (eMovType),
        459
      },
      {
        typeof (EAbilityType),
        460
      },
      {
        typeof (EAbilitySlot),
        461
      },
      {
        typeof (EUseConditionsType),
        462
      },
      {
        typeof (EAbilityTypeDetail),
        463
      },
      {
        typeof (eInspSkillTriggerType),
        464
      },
      {
        typeof (TobiraParam.Category),
        465
      },
      {
        typeof (TobiraLearnAbilityParam.AddType),
        466
      },
      {
        typeof (UnitData.TemporaryFlags),
        467
      },
      {
        typeof (UnitBadgeTypes),
        468
      },
      {
        typeof (ESkillAbilityDeriveConds),
        469
      },
      {
        typeof (ConditionEffectTypes),
        470
      },
      {
        typeof (EUnitCondition),
        471
      },
      {
        typeof (SkillEffectTargets),
        472
      },
      {
        typeof (StatusTypes),
        473
      },
      {
        typeof (ParamPriorities),
        474
      },
      {
        typeof (SkillCategory),
        475
      },
      {
        typeof (EUnitSide),
        476
      },
      {
        typeof (Unit.eTypeMhmDamage),
        477
      },
      {
        typeof (EUnitDirection),
        478
      },
      {
        typeof (eMapBreakClashType),
        479
      },
      {
        typeof (eMapBreakAIType),
        480
      },
      {
        typeof (eMapBreakSideType),
        481
      },
      {
        typeof (eMapBreakRayType),
        482
      },
      {
        typeof (FriendStates),
        483
      },
      {
        typeof (JukeBoxParam.eUnlockType),
        484
      },
      {
        typeof (BattleCore.Json_BtlDrop),
        485
      },
      {
        typeof (UnitEntryTrigger),
        486
      },
      {
        typeof (OInt),
        487
      },
      {
        typeof (OBool),
        488
      },
      {
        typeof (GeoParam),
        489
      },
      {
        typeof (Grid),
        490
      },
      {
        typeof (OString),
        491
      },
      {
        typeof (SkillLockCondition),
        492
      },
      {
        typeof (EquipSkillSetting),
        493
      },
      {
        typeof (EquipAbilitySetting),
        494
      },
      {
        typeof (AIAction),
        495
      },
      {
        typeof (AIActionTable),
        496
      },
      {
        typeof (AIPatrolPoint),
        497
      },
      {
        typeof (AIPatrolTable),
        498
      },
      {
        typeof (MapBreakObj),
        499
      },
      {
        typeof (OIntVector2),
        500
      },
      {
        typeof (EventTrigger),
        501
      },
      {
        typeof (NPCSetting),
        502
      },
      {
        typeof (MultiPlayResumeBuff.ResistStatus),
        503
      },
      {
        typeof (MultiPlayResumeBuff),
        504
      },
      {
        typeof (MultiPlayResumeShield),
        505
      },
      {
        typeof (MultiPlayResumeMhmDmg),
        506
      },
      {
        typeof (MultiPlayResumeFtgt),
        507
      },
      {
        typeof (MultiPlayResumeAbilChg.Data),
        508
      },
      {
        typeof (MultiPlayResumeAbilChg),
        509
      },
      {
        typeof (MultiPlayResumeAddedAbil),
        510
      },
      {
        typeof (MultiPlayResumeProtect),
        511
      },
      {
        typeof (MultiPlayResumeUnitData),
        512
      },
      {
        typeof (MultiPlayGimmickEventParam),
        513
      },
      {
        typeof (MultiPlayTrickParam),
        514
      },
      {
        typeof (MultiPlayResumeParam.WeatherInfo),
        515
      },
      {
        typeof (MultiPlayResumeParam),
        516
      },
      {
        typeof (SkillRankUpValue),
        517
      },
      {
        typeof (ProtectSkillParam),
        518
      },
      {
        typeof (SkillRankUpValueShort),
        519
      },
      {
        typeof (SkillParam),
        520
      },
      {
        typeof (QuestClearUnlockUnitDataParam),
        521
      },
      {
        typeof (FlagManager),
        522
      },
      {
        typeof (OShort),
        523
      },
      {
        typeof (StatusParam),
        524
      },
      {
        typeof (EnchantParam),
        525
      },
      {
        typeof (UnitParam.Status),
        526
      },
      {
        typeof (UnitParam.NoJobStatus),
        527
      },
      {
        typeof (UnitParam),
        528
      },
      {
        typeof (ElementParam),
        529
      },
      {
        typeof (BattleBonusParam),
        530
      },
      {
        typeof (TokkouValue),
        531
      },
      {
        typeof (TokkouParam),
        532
      },
      {
        typeof (BaseStatus),
        533
      },
      {
        typeof (RecipeItem),
        534
      },
      {
        typeof (RecipeParam),
        535
      },
      {
        typeof (ItemParam),
        536
      },
      {
        typeof (ReturnItem),
        537
      },
      {
        typeof (RarityEquipEnhanceParam.RankParam),
        538
      },
      {
        typeof (RarityEquipEnhanceParam),
        539
      },
      {
        typeof (RarityParam),
        540
      },
      {
        typeof (EquipData),
        541
      },
      {
        typeof (OLong),
        542
      },
      {
        typeof (ConceptCardEffectsParam),
        543
      },
      {
        typeof (ConceptLimitUpItemParam),
        544
      },
      {
        typeof (ConceptCardParam),
        545
      },
      {
        typeof (BuffEffectParam.Buff),
        546
      },
      {
        typeof (BuffEffectParam),
        547
      },
      {
        typeof (BuffEffect.BuffTarget),
        548
      },
      {
        typeof (BuffEffect),
        549
      },
      {
        typeof (ConceptCardEquipEffect),
        550
      },
      {
        typeof (ConceptCardData),
        551
      },
      {
        typeof (RuneLotteryBaseState),
        552
      },
      {
        typeof (RuneBuffDataBaseState),
        553
      },
      {
        typeof (RuneLotteryEvoState),
        554
      },
      {
        typeof (RuneBuffDataEvoState),
        555
      },
      {
        typeof (RuneStateData),
        556
      },
      {
        typeof (RuneData),
        557
      },
      {
        typeof (ArtifactParam),
        558
      },
      {
        typeof (BuffEffect.BuffValues),
        559
      },
      {
        typeof (JobRankParam),
        560
      },
      {
        typeof (JobParam),
        561
      },
      {
        typeof (ItemData),
        562
      },
      {
        typeof (LearningSkill),
        563
      },
      {
        typeof (AbilityParam),
        564
      },
      {
        typeof (InspSkillTriggerParam.TriggerData),
        565
      },
      {
        typeof (InspSkillTriggerParam),
        566
      },
      {
        typeof (InspSkillParam),
        567
      },
      {
        typeof (InspirationSkillData),
        568
      },
      {
        typeof (ArtifactData),
        569
      },
      {
        typeof (JobData),
        570
      },
      {
        typeof (TobiraLearnAbilityParam),
        571
      },
      {
        typeof (TobiraParam),
        572
      },
      {
        typeof (TobiraData),
        573
      },
      {
        typeof (UnitData),
        574
      },
      {
        typeof (SkillDeriveParam),
        575
      },
      {
        typeof (SkillAbilityDeriveTriggerParam),
        576
      },
      {
        typeof (SkillAbilityDeriveParam),
        577
      },
      {
        typeof (AbilityDeriveParam),
        578
      },
      {
        typeof (AbilityData),
        579
      },
      {
        typeof (CondEffectParam),
        580
      },
      {
        typeof (CondEffect),
        581
      },
      {
        typeof (SkillData),
        582
      },
      {
        typeof (BuffAttachment.ResistStatusBuff),
        583
      },
      {
        typeof (BuffAttachment),
        584
      },
      {
        typeof (CondAttachment),
        585
      },
      {
        typeof (AIParam),
        586
      },
      {
        typeof (Unit.DropItem),
        587
      },
      {
        typeof (Unit.UnitDrop),
        588
      },
      {
        typeof (Unit.UnitSteal),
        589
      },
      {
        typeof (Unit.UnitShield),
        590
      },
      {
        typeof (Unit.UnitProtect),
        591
      },
      {
        typeof (Unit.UnitMhmDamage),
        592
      },
      {
        typeof (Unit.UnitInsp),
        593
      },
      {
        typeof (Unit.UnitForcedTargeting),
        594
      },
      {
        typeof (Unit.AbilityChange.Data),
        595
      },
      {
        typeof (Unit.AbilityChange),
        596
      },
      {
        typeof (DynamicTransformUnitParam),
        597
      },
      {
        typeof (BuffBit),
        598
      },
      {
        typeof (Unit),
        599
      },
      {
        typeof (MultiPlayResumeSkillData),
        600
      },
      {
        typeof (Json_BtlRewardConceptCard),
        601
      },
      {
        typeof (SceneBattle.MultiPlayRecvData),
        602
      },
      {
        typeof (Json_InspirationSkill),
        603
      },
      {
        typeof (Json_Artifact),
        604
      },
      {
        typeof (JSON_ConceptCard),
        605
      },
      {
        typeof (JSON_PlayerGuild),
        606
      },
      {
        typeof (JSON_ViewGuild),
        607
      },
      {
        typeof (JSON_GuildFacilityData),
        608
      },
      {
        typeof (JSON_GuildRaidPrev),
        609
      },
      {
        typeof (JSON_GuildRaidCurrent),
        610
      },
      {
        typeof (JSON_GuildRaidBattlePoint),
        611
      },
      {
        typeof (JSON_GuildRaidBossInfo),
        612
      },
      {
        typeof (JSON_GuildRaidData),
        613
      },
      {
        typeof (JSON_GuildRaidChallengingPlayer),
        614
      },
      {
        typeof (JSON_GuildRaidKnockDownInfo),
        615
      },
      {
        typeof (JSON_GuildRaidMailListItem),
        616
      },
      {
        typeof (JSON_GuildRaidMailOption),
        617
      },
      {
        typeof (JSON_GuildRaidMail),
        618
      },
      {
        typeof (Json_MasterAbility),
        619
      },
      {
        typeof (Json_CollaboSkill),
        620
      },
      {
        typeof (Json_CollaboAbility),
        621
      },
      {
        typeof (Json_Equip),
        622
      },
      {
        typeof (Json_Ability),
        623
      },
      {
        typeof (Json_InspirationSkillExt),
        624
      },
      {
        typeof (Json_JobSelectable),
        625
      },
      {
        typeof (Json_Job),
        626
      },
      {
        typeof (Json_UnitJob),
        627
      },
      {
        typeof (Json_UnitSelectable),
        628
      },
      {
        typeof (Json_Tobira),
        629
      },
      {
        typeof (Json_RuneBuffData),
        630
      },
      {
        typeof (Json_RuneStateData),
        631
      },
      {
        typeof (Json_RuneData),
        632
      },
      {
        typeof (Json_Unit),
        633
      },
      {
        typeof (JSON_GuildRaidDeck),
        634
      },
      {
        typeof (JSON_GuildRaidBattleLog),
        635
      },
      {
        typeof (JSON_GuildRaidReport),
        636
      },
      {
        typeof (JSON_GuildRaidRankingGuild),
        637
      },
      {
        typeof (JSON_GuildRaidRanking),
        638
      },
      {
        typeof (JSON_GuildRaidRankingMemberBoss),
        639
      },
      {
        typeof (JSON_GuildRaidRankingMember),
        640
      },
      {
        typeof (JSON_GuildRaidRankingDamage),
        641
      },
      {
        typeof (JSON_GuildRaidGuildData),
        642
      },
      {
        typeof (JSON_GuildRaidRankingRewardData),
        643
      },
      {
        typeof (JSON_GuildRaidGuildRanking),
        644
      },
      {
        typeof (OByte),
        645
      },
      {
        typeof (OSbyte),
        646
      },
      {
        typeof (OUInt),
        647
      },
      {
        typeof (Json_Coin),
        648
      },
      {
        typeof (Json_Hikkoshi),
        649
      },
      {
        typeof (Json_Stamina),
        650
      },
      {
        typeof (Json_Cave),
        651
      },
      {
        typeof (Json_AbilityUp),
        652
      },
      {
        typeof (Json_Arena),
        653
      },
      {
        typeof (Json_Tour),
        654
      },
      {
        typeof (Json_Vip),
        655
      },
      {
        typeof (Json_Premium),
        656
      },
      {
        typeof (Json_FreeGacha),
        657
      },
      {
        typeof (Json_PaidGacha),
        658
      },
      {
        typeof (Json_Friends),
        659
      },
      {
        typeof (Json_MultiOption),
        660
      },
      {
        typeof (Json_GuerrillaShopPeriod),
        661
      },
      {
        typeof (Json_PlayerData),
        662
      },
      {
        typeof (Json_Item),
        663
      },
      {
        typeof (Json_GiftConceptCard),
        664
      },
      {
        typeof (Json_Gift),
        665
      },
      {
        typeof (Json_Mail),
        666
      },
      {
        typeof (Json_Party),
        667
      },
      {
        typeof (Json_Friend),
        668
      },
      {
        typeof (Json_Skin),
        669
      },
      {
        typeof (Json_LoginBonusVip),
        670
      },
      {
        typeof (Json_LoginBonus),
        671
      },
      {
        typeof (Json_PremiumLoginBonusItem),
        672
      },
      {
        typeof (Json_PremiumLoginBonus),
        673
      },
      {
        typeof (Json_LoginBonusTable),
        674
      },
      {
        typeof (Json_Notify),
        675
      },
      {
        typeof (Json_MultiFuids),
        676
      },
      {
        typeof (Json_VersusCount),
        677
      },
      {
        typeof (Json_Versus),
        678
      },
      {
        typeof (Json_ExpireItem),
        679
      },
      {
        typeof (JSON_TrophyProgress),
        680
      },
      {
        typeof (JSON_UnitOverWriteData),
        681
      },
      {
        typeof (JSON_PartyOverWrite),
        682
      },
      {
        typeof (JSON_StoryExChallengeCount),
        683
      },
      {
        typeof (Json_PlayerDataAll),
        684
      },
      {
        typeof (Json_TrophyPlayerData),
        685
      },
      {
        typeof (Json_TrophyConceptCard),
        686
      },
      {
        typeof (Json_TrophyConceptCards),
        687
      },
      {
        typeof (ReqTrophyStarMission.StarMission.Info),
        688
      },
      {
        typeof (ReqTrophyStarMission.StarMission),
        689
      },
      {
        typeof (Json_TrophyPlayerDataAll),
        690
      },
      {
        typeof (JSON_SupportHistory),
        691
      },
      {
        typeof (JSON_SupportMyInfo),
        692
      },
      {
        typeof (JSON_SupportRankingGuild),
        693
      },
      {
        typeof (JSON_SupportRanking),
        694
      },
      {
        typeof (JSON_SupportRankingUser),
        695
      },
      {
        typeof (JSON_SupportUnitRanking),
        696
      },
      {
        typeof (JSON_SupportRankingUnit),
        697
      },
      {
        typeof (Json_RuneEnforceGaugeData),
        698
      },
      {
        typeof (Json_UnitPieceShopItem),
        699
      },
      {
        typeof (ReqAutoRepeatQuestBox.Response),
        700
      },
      {
        typeof (FlowNode_ReqAutoRepeatQuestBox.MP_ReqAutoRepeatQuestBoxResponse),
        701
      },
      {
        typeof (ReqAutoRepeatQuestBoxAdd.Response),
        702
      },
      {
        typeof (FlowNode_ReqAutoRepeatQuestBoxAdd.MP_ReqAutoRepeatQuestBoxAddResponse),
        703
      },
      {
        typeof (Json_AutoRepeatQuestData),
        704
      },
      {
        typeof (ReqAutoRepeatQuestProgress.Response),
        705
      },
      {
        typeof (FlowNode_ReqAutoRepeatQuestProgress.MP_AutoRepeatQuestProgressResponse),
        706
      },
      {
        typeof (JSON_QuestCount),
        707
      },
      {
        typeof (JSON_QuestProgress),
        708
      },
      {
        typeof (ReqAutoRepeatQuestEnd.Response),
        709
      },
      {
        typeof (FlowNode_ReqAutoRepeatQuestResult.MP_AutoRepeatQuestEndResponse),
        710
      },
      {
        typeof (ReqAutoRepeatQuestStart.Response),
        711
      },
      {
        typeof (FlowNode_ReqAutoRepeatQuestStart.MP_AutoRepeatQuestStartResponse),
        712
      },
      {
        typeof (ReqDrawCard.CardInfo.Card),
        713
      },
      {
        typeof (ReqDrawCard.CardInfo),
        714
      },
      {
        typeof (ReqDrawCard.Response.Status),
        715
      },
      {
        typeof (ReqDrawCard.Response),
        716
      },
      {
        typeof (FlowNode_ReqDrawCard.MP_Response),
        717
      },
      {
        typeof (ReqDrawCardExec.Response),
        718
      },
      {
        typeof (FlowNode_ReqDrawCardExec.MP_Response),
        719
      },
      {
        typeof (ReqAllEquipExpAdd.Response),
        720
      },
      {
        typeof (FlowNode_AllEnhanceEquip.MP_Response),
        721
      },
      {
        typeof (FlowNode_Login.MP_PlayerDataAll),
        722
      },
      {
        typeof (FlowNode_PlayNew.MP_PlayNew),
        723
      },
      {
        typeof (ReqArtifactSet_OverWrite.Response),
        724
      },
      {
        typeof (FlowNode_ReqArtifactsSet.MP_ArtifactSet_OverWriteResponse),
        725
      },
      {
        typeof (FlowNode_ReqBingoProgress.JSON_BingoResponse),
        726
      },
      {
        typeof (FlowNode_ReqBingoProgress.MP_BingoResponse),
        727
      },
      {
        typeof (ReqSetConceptCard_OverWrite.Response),
        728
      },
      {
        typeof (FlowNode_ReqConceptCardSet.MP_SetConceptCard_OverWriteResponse),
        729
      },
      {
        typeof (ReqSetConceptLeaderSkill_OverWrite.Response),
        730
      },
      {
        typeof (FlowNode_ReqConceptLeaderSkillSet.MP_SetConceptLeaderSkill_OverWriteResponse),
        731
      },
      {
        typeof (JSON_FixParam),
        732
      },
      {
        typeof (JSON_UnitParam),
        733
      },
      {
        typeof (JSON_UnitJobOverwriteParam),
        734
      },
      {
        typeof (JSON_SkillParam),
        735
      },
      {
        typeof (JSON_BuffEffectParam),
        736
      },
      {
        typeof (JSON_CondEffectParam),
        737
      },
      {
        typeof (JSON_AbilityParam),
        738
      },
      {
        typeof (JSON_ItemParam),
        739
      },
      {
        typeof (JSON_ArtifactParam),
        740
      },
      {
        typeof (JSON_WeaponParam),
        741
      },
      {
        typeof (JSON_RecipeParam),
        742
      },
      {
        typeof (JSON_JobRankParam),
        743
      },
      {
        typeof (JSON_JobParam),
        744
      },
      {
        typeof (JSON_JobSetParam),
        745
      },
      {
        typeof (JSON_EvaluationParam),
        746
      },
      {
        typeof (JSON_AIParam),
        747
      },
      {
        typeof (JSON_GeoParam),
        748
      },
      {
        typeof (JSON_RarityParam),
        749
      },
      {
        typeof (JSON_ShopParam),
        750
      },
      {
        typeof (JSON_PlayerParam),
        751
      },
      {
        typeof (JSON_GrowCurve),
        752
      },
      {
        typeof (JSON_GrowParam),
        753
      },
      {
        typeof (JSON_LocalNotificationParam),
        754
      },
      {
        typeof (JSON_TrophyCategoryParam),
        755
      },
      {
        typeof (JSON_ChallengeCategoryParam),
        756
      },
      {
        typeof (JSON_TrophyParam),
        757
      },
      {
        typeof (JSON_UnlockParam),
        758
      },
      {
        typeof (JSON_VipParam),
        759
      },
      {
        typeof (JSON_ArenaWinResult),
        760
      },
      {
        typeof (JSON_ArenaResult),
        761
      },
      {
        typeof (JSON_StreamingMovie),
        762
      },
      {
        typeof (JSON_BannerParam),
        763
      },
      {
        typeof (JSON_QuestClearUnlockUnitDataParam),
        764
      },
      {
        typeof (JSON_AwardParam),
        765
      },
      {
        typeof (JSON_LoginInfoParam),
        766
      },
      {
        typeof (JSON_CollaboSkillParam),
        767
      },
      {
        typeof (JSON_TrickParam),
        768
      },
      {
        typeof (JSON_BreakObjParam),
        769
      },
      {
        typeof (JSON_VersusMatchingParam),
        770
      },
      {
        typeof (JSON_VersusMatchCondParam),
        771
      },
      {
        typeof (JSON_TowerScoreThreshold),
        772
      },
      {
        typeof (JSON_TowerScore),
        773
      },
      {
        typeof (JSON_FriendPresentItemParam),
        774
      },
      {
        typeof (JSON_WeatherParam),
        775
      },
      {
        typeof (JSON_UnitUnlockTimeParam),
        776
      },
      {
        typeof (JSON_TobiraLearnAbilityParam),
        777
      },
      {
        typeof (JSON_TobiraParam),
        778
      },
      {
        typeof (JSON_TobiraCategoriesParam),
        779
      },
      {
        typeof (JSON_TobiraConditionParam),
        780
      },
      {
        typeof (JSON_TobiraCondsParam),
        781
      },
      {
        typeof (JSON_TobiraCondsUnitParam.JobCond),
        782
      },
      {
        typeof (JSON_TobiraCondsUnitParam),
        783
      },
      {
        typeof (JSON_TobiraRecipeMaterialParam),
        784
      },
      {
        typeof (JSON_TobiraRecipeParam),
        785
      },
      {
        typeof (JSON_ConceptCardEquipParam),
        786
      },
      {
        typeof (JSON_ConceptCardParam),
        787
      },
      {
        typeof (JSON_ConceptCardConditionsParam),
        788
      },
      {
        typeof (JSON_ConceptCardTrustRewardItemParam),
        789
      },
      {
        typeof (JSON_ConceptCardTrustRewardParam),
        790
      },
      {
        typeof (JSON_ConceptCardLsBuffCoefParam),
        791
      },
      {
        typeof (JSON_ConceptCardGroup),
        792
      },
      {
        typeof (JSON_ConceptLimitUpItem),
        793
      },
      {
        typeof (JSON_UnitGroupParam),
        794
      },
      {
        typeof (JSON_JobGroupParam),
        795
      },
      {
        typeof (JSON_StatusCoefficientParam),
        796
      },
      {
        typeof (JSON_CustomTargetParam),
        797
      },
      {
        typeof (JSON_SkillAbilityDeriveParam),
        798
      },
      {
        typeof (JSON_RaidPeriodParam),
        799
      },
      {
        typeof (JSON_RaidPeriodTimeScheduleParam),
        800
      },
      {
        typeof (JSON_RaidPeriodTimeParam),
        801
      },
      {
        typeof (JSON_RaidAreaParam),
        802
      },
      {
        typeof (JSON_RaidBossParam),
        803
      },
      {
        typeof (JSON_RaidBattleRewardWeightParam),
        804
      },
      {
        typeof (JSON_RaidBattleRewardParam),
        805
      },
      {
        typeof (JSON_RaidBeatRewardDataParam),
        806
      },
      {
        typeof (JSON_RaidBeatRewardParam),
        807
      },
      {
        typeof (JSON_RaidDamageRatioRewardRatioParam),
        808
      },
      {
        typeof (JSON_RaidDamageRatioRewardParam),
        809
      },
      {
        typeof (JSON_RaidDamageAmountRewardAmountParam),
        810
      },
      {
        typeof (JSON_RaidDamageAmountRewardParam),
        811
      },
      {
        typeof (JSON_RaidAreaClearRewardDataParam),
        812
      },
      {
        typeof (JSON_RaidAreaClearRewardParam),
        813
      },
      {
        typeof (JSON_RaidCompleteRewardDataParam),
        814
      },
      {
        typeof (JSON_RaidCompleteRewardParam),
        815
      },
      {
        typeof (JSON_RaidReward),
        816
      },
      {
        typeof (JSON_RaidRewardParam),
        817
      },
      {
        typeof (JSON_TipsParam),
        818
      },
      {
        typeof (JSON_GuildEmblemParam),
        819
      },
      {
        typeof (JSON_GuildFacilityEffectParam),
        820
      },
      {
        typeof (JSON_GuildFacilityParam),
        821
      },
      {
        typeof (JSON_GuildFacilityLvParam),
        822
      },
      {
        typeof (JSON_ConvertUnitPieceExcludeParam),
        823
      },
      {
        typeof (JSON_PremiumParam),
        824
      },
      {
        typeof (JSON_BuyCoinShopParam),
        825
      },
      {
        typeof (JSON_BuyCoinProductParam),
        826
      },
      {
        typeof (JSON_BuyCoinRewardItemParam),
        827
      },
      {
        typeof (JSON_BuyCoinRewardParam),
        828
      },
      {
        typeof (JSON_BuyCoinProductConvertParam),
        829
      },
      {
        typeof (JSON_DynamicTransformUnitParam),
        830
      },
      {
        typeof (JSON_RecommendedArtifactParam),
        831
      },
      {
        typeof (JSON_SkillMotionDataParam),
        832
      },
      {
        typeof (JSON_SkillMotionParam),
        833
      },
      {
        typeof (JSON_DependStateSpcEffParam),
        834
      },
      {
        typeof (JSON_InspSkillDerivation),
        835
      },
      {
        typeof (JSON_InspSkillParam),
        836
      },
      {
        typeof (JSON_InspSkillTriggerParam.JSON_TriggerData),
        837
      },
      {
        typeof (JSON_InspSkillTriggerParam),
        838
      },
      {
        typeof (JSON_InspSkillCostParam),
        839
      },
      {
        typeof (JSON_InspSkillLvUpCostParam.JSON_CostData),
        840
      },
      {
        typeof (JSON_InspSkillLvUpCostParam),
        841
      },
      {
        typeof (JSON_HighlightResource),
        842
      },
      {
        typeof (JSON_HighlightParam),
        843
      },
      {
        typeof (JSON_HighlightGiftData),
        844
      },
      {
        typeof (JSON_HighlightGift),
        845
      },
      {
        typeof (JSON_GenesisParam),
        846
      },
      {
        typeof (JSON_CoinBuyUseBonusParam),
        847
      },
      {
        typeof (JSON_CoinBuyUseBonusContentParam),
        848
      },
      {
        typeof (JSON_CoinBuyUseBonusRewardSetParam),
        849
      },
      {
        typeof (JSON_CoinBuyUseBonusItemParam),
        850
      },
      {
        typeof (JSON_CoinBuyUseBonusRewardParam),
        851
      },
      {
        typeof (JSON_UnitRentalNotificationDataParam),
        852
      },
      {
        typeof (JSON_UnitRentalNotificationParam),
        853
      },
      {
        typeof (JSON_UnitRentalParam.QuestInfo),
        854
      },
      {
        typeof (JSON_UnitRentalParam),
        855
      },
      {
        typeof (JSON_DrawCardRewardParam.Data),
        856
      },
      {
        typeof (JSON_DrawCardRewardParam),
        857
      },
      {
        typeof (JSON_DrawCardParam.DrawInfo),
        858
      },
      {
        typeof (JSON_DrawCardParam),
        859
      },
      {
        typeof (JSON_TrophyStarMissionRewardParam.Data),
        860
      },
      {
        typeof (JSON_TrophyStarMissionRewardParam),
        861
      },
      {
        typeof (JSON_TrophyStarMissionParam.StarSetParam),
        862
      },
      {
        typeof (JSON_TrophyStarMissionParam),
        863
      },
      {
        typeof (JSON_UnitPieceShopParam),
        864
      },
      {
        typeof (JSON_UnitPieceShopGroupCost),
        865
      },
      {
        typeof (JSON_UnitPieceShopGroupParam),
        866
      },
      {
        typeof (JSON_TwitterMessageDetailParam),
        867
      },
      {
        typeof (JSON_TwitterMessageParam),
        868
      },
      {
        typeof (JSON_FilterConceptCardConditionParam),
        869
      },
      {
        typeof (JSON_FilterConceptCardParam),
        870
      },
      {
        typeof (JSON_FilterRuneConditionParam),
        871
      },
      {
        typeof (JSON_FilterRuneParam),
        872
      },
      {
        typeof (JSON_FilterUnitConditionParam),
        873
      },
      {
        typeof (JSON_FilterUnitParam),
        874
      },
      {
        typeof (JSON_FilterArtifactParam.Condition),
        875
      },
      {
        typeof (JSON_FilterArtifactParam),
        876
      },
      {
        typeof (JSON_SortRuneConditionParam),
        877
      },
      {
        typeof (JSON_SortRuneParam),
        878
      },
      {
        typeof (JSON_RuneParam),
        879
      },
      {
        typeof (JSON_RuneLottery),
        880
      },
      {
        typeof (JSON_RuneLotteryBaseState),
        881
      },
      {
        typeof (JSON_RuneLotteryEvoState),
        882
      },
      {
        typeof (JSON_RuneDisassembly),
        883
      },
      {
        typeof (JSON_RuneMaterial),
        884
      },
      {
        typeof (JSON_RuneCost),
        885
      },
      {
        typeof (JSON_RuneSetEffState),
        886
      },
      {
        typeof (JSON_RuneSetEff),
        887
      },
      {
        typeof (JSON_JukeBoxParam),
        888
      },
      {
        typeof (JSON_JukeBoxSectionParam),
        889
      },
      {
        typeof (JSON_UnitSameGroupParam),
        890
      },
      {
        typeof (JSON_AutoRepeatQuestBoxParam),
        891
      },
      {
        typeof (JSON_GuildAttendRewardDetail),
        892
      },
      {
        typeof (JSON_GuildAttendParam),
        893
      },
      {
        typeof (JSON_GuildAttendReward),
        894
      },
      {
        typeof (JSON_GuildAttendRewardParam),
        895
      },
      {
        typeof (JSON_GuildRoleBonusDetail),
        896
      },
      {
        typeof (JSON_GuildRoleBonus),
        897
      },
      {
        typeof (JSON_GUildRoleBonusReward),
        898
      },
      {
        typeof (JSON_GuildRoleBonusRewardParam),
        899
      },
      {
        typeof (JSON_ResetCostInfoParam),
        900
      },
      {
        typeof (JSON_ResetCostParam),
        901
      },
      {
        typeof (JSON_ProtectSkillParam),
        902
      },
      {
        typeof (JSON_ReplacePeriod),
        903
      },
      {
        typeof (JSON_ReplaceSprite),
        904
      },
      {
        typeof (JSON_InitPlayer),
        905
      },
      {
        typeof (JSON_InitUnit),
        906
      },
      {
        typeof (JSON_InitItem),
        907
      },
      {
        typeof (JSON_MasterParam),
        908
      },
      {
        typeof (FlowNode_ReqMasterParam.MP_MasterParam),
        909
      },
      {
        typeof (ReqOverWriteParty.Response),
        910
      },
      {
        typeof (FlowNode_ReqOverWriteParty.MP_OverWritePartyResponse),
        911
      },
      {
        typeof (JSON_SectionParam),
        912
      },
      {
        typeof (JSON_ArchiveItemsParam),
        913
      },
      {
        typeof (JSON_ArchiveParam),
        914
      },
      {
        typeof (JSON_ChapterParam),
        915
      },
      {
        typeof (JSON_MapParam),
        916
      },
      {
        typeof (JSON_QuestParam),
        917
      },
      {
        typeof (JSON_InnerObjective),
        918
      },
      {
        typeof (JSON_ObjectiveParam),
        919
      },
      {
        typeof (JSON_MagnificationParam),
        920
      },
      {
        typeof (JSON_QuestCondParam),
        921
      },
      {
        typeof (JSON_QuestPartyParam),
        922
      },
      {
        typeof (JSON_QuestCampaignParentParam),
        923
      },
      {
        typeof (JSON_QuestCampaignChildParam),
        924
      },
      {
        typeof (JSON_QuestCampaignTrust),
        925
      },
      {
        typeof (JSON_QuestCampaignInspSkill),
        926
      },
      {
        typeof (JSON_TowerFloorParam),
        927
      },
      {
        typeof (JSON_TowerRewardItem),
        928
      },
      {
        typeof (JSON_TowerRewardParam),
        929
      },
      {
        typeof (JSON_TowerRoundRewardItem),
        930
      },
      {
        typeof (JSON_TowerRoundRewardParam),
        931
      },
      {
        typeof (JSON_TowerParam),
        932
      },
      {
        typeof (JSON_VersusTowerParam),
        933
      },
      {
        typeof (JSON_VersusSchedule),
        934
      },
      {
        typeof (JSON_VersusCoin),
        935
      },
      {
        typeof (JSON_MultiTowerFloorParam),
        936
      },
      {
        typeof (JSON_MultiTowerRewardItem),
        937
      },
      {
        typeof (JSON_MultiTowerRewardParam),
        938
      },
      {
        typeof (JSON_MapEffectParam),
        939
      },
      {
        typeof (JSON_WeatherSetParam),
        940
      },
      {
        typeof (JSON_RankingQuestParam),
        941
      },
      {
        typeof (JSON_RankingQuestScheduleParam),
        942
      },
      {
        typeof (JSON_RankingQuestRewardParam),
        943
      },
      {
        typeof (JSON_VersusWinBonusReward),
        944
      },
      {
        typeof (JSON_VersusFirstWinBonus),
        945
      },
      {
        typeof (JSON_VersusStreakWinSchedule),
        946
      },
      {
        typeof (JSON_VersusStreakWinBonus),
        947
      },
      {
        typeof (JSON_VersusRule),
        948
      },
      {
        typeof (JSON_VersusCoinCampParam),
        949
      },
      {
        typeof (JSON_VersusEnableTimeScheduleParam),
        950
      },
      {
        typeof (JSON_VersusEnableTimeParam),
        951
      },
      {
        typeof (JSON_VersusRankParam),
        952
      },
      {
        typeof (JSON_VersusRankClassParam),
        953
      },
      {
        typeof (JSON_VersusRankRankingRewardParam),
        954
      },
      {
        typeof (JSON_VersusRankRewardRewardParam),
        955
      },
      {
        typeof (JSON_VersusRankRewardParam),
        956
      },
      {
        typeof (JSON_VersusRankMissionScheduleParam),
        957
      },
      {
        typeof (JSON_VersusRankMissionParam),
        958
      },
      {
        typeof (JSON_QuestLobbyNewsParam),
        959
      },
      {
        typeof (JSON_GuerrillaShopAdventQuestParam),
        960
      },
      {
        typeof (JSON_GuerrillaShopScheduleAdventParam),
        961
      },
      {
        typeof (JSON_GuerrillaShopScheduleParam),
        962
      },
      {
        typeof (JSON_VersusDraftDeckParam),
        963
      },
      {
        typeof (JSON_VersusDraftUnitParam),
        964
      },
      {
        typeof (JSON_GenesisStarRewardParam),
        965
      },
      {
        typeof (JSON_GenesisStarParam),
        966
      },
      {
        typeof (JSON_GenesisChapterModeInfoParam),
        967
      },
      {
        typeof (JSON_GenesisChapterParam),
        968
      },
      {
        typeof (JSON_GenesisRewardDataParam),
        969
      },
      {
        typeof (JSON_GenesisRewardParam),
        970
      },
      {
        typeof (JSON_GenesisLapBossParam.LapInfo),
        971
      },
      {
        typeof (JSON_GenesisLapBossParam),
        972
      },
      {
        typeof (JSON_AdvanceStarRewardParam),
        973
      },
      {
        typeof (JSON_AdvanceStarParam),
        974
      },
      {
        typeof (JSON_AdvanceEventModeInfoParam),
        975
      },
      {
        typeof (JSON_AdvanceEventParam),
        976
      },
      {
        typeof (JSON_AdvanceRewardDataParam),
        977
      },
      {
        typeof (JSON_AdvanceRewardParam),
        978
      },
      {
        typeof (JSON_AdvanceLapBossParam.LapInfo),
        979
      },
      {
        typeof (JSON_AdvanceLapBossParam),
        980
      },
      {
        typeof (JSON_GuildRaidBossParam),
        981
      },
      {
        typeof (JSON_GuildRaidCoolDaysParam),
        982
      },
      {
        typeof (JSON_GuildRaidScoreDataParam),
        983
      },
      {
        typeof (JSON_GuildRaidScoreParam),
        984
      },
      {
        typeof (JSON_GuildRaidPeriodTime),
        985
      },
      {
        typeof (JSON_GuildRaidPeriodParam),
        986
      },
      {
        typeof (JSON_GuildRaidReward),
        987
      },
      {
        typeof (JSON_GuildRaidRewardParam),
        988
      },
      {
        typeof (JSON_GuildRaidRewardDmgRankingRankParam),
        989
      },
      {
        typeof (JSON_GuildRaidRewardDmgRankingParam),
        990
      },
      {
        typeof (JSON_GuildRaidRewardDmgRatio),
        991
      },
      {
        typeof (JSON_GuildRaidRewardDmgRatioParam),
        992
      },
      {
        typeof (JSON_GuildRaidRewardRound),
        993
      },
      {
        typeof (JSON_GuildRaidRewardRoundParam),
        994
      },
      {
        typeof (JSON_GuildRaidRewardRankingDataParam),
        995
      },
      {
        typeof (JSON_GuildRaidRewardRankingParam),
        996
      },
      {
        typeof (JSON_GvGPeriodParam),
        997
      },
      {
        typeof (JSON_GvGNodeParam),
        998
      },
      {
        typeof (JSON_GvGNPCPartyDetailParam),
        999
      },
      {
        typeof (JSON_GvGNPCPartyParam),
        1000
      },
      {
        typeof (JSON_GvGNPCUnitParam),
        1001
      },
      {
        typeof (JSON_GvGRewardRankingDetailParam),
        1002
      },
      {
        typeof (JSON_GvGRewardRankingParam),
        1003
      },
      {
        typeof (JSON_GvGRewardDetailParam),
        1004
      },
      {
        typeof (JSON_GvGRewardParam),
        1005
      },
      {
        typeof (JSON_GvGRuleParam),
        1006
      },
      {
        typeof (Json_QuestList),
        1007
      },
      {
        typeof (FlowNode_ReqQuestParam.MP_QuestParam),
        1008
      },
      {
        typeof (ReqSetSupportRanking.Response),
        1009
      },
      {
        typeof (FlowNode_ReqSupportRanking.MP_Response),
        1010
      },
      {
        typeof (ReqSetSupportUsed.Response),
        1011
      },
      {
        typeof (FlowNode_ReqSupportUsed.MP_Response),
        1012
      },
      {
        typeof (ReqUnitPieceShopItemList.Response),
        1013
      },
      {
        typeof (FlowNode_RequestUnitPieceShopItems.MP_Response),
        1014
      },
      {
        typeof (FlowNode_ReqUpdateBingo.MP_UpdateBingoResponse),
        1015
      },
      {
        typeof (FlowNode_ReqUpdateTrophy.MP_TrophyPlayerDataAllResponse),
        1016
      },
      {
        typeof (ReqJobAbility_OverWrite.Response),
        1017
      },
      {
        typeof (FlowNode_SetAbility.MP_JobAbilityt_OverWriteResponse),
        1018
      },
      {
        typeof (Json_MyPhotonPlayerBinaryParam.UnitDataElem),
        1019
      },
      {
        typeof (Json_MyPhotonPlayerBinaryParam),
        1020
      },
      {
        typeof (FlowNode_StartMultiPlay.RecvData),
        1021
      },
      {
        typeof (ReqUnitPieceShopBuypaid.Response),
        1022
      },
      {
        typeof (FlowNode_UnitPieceBuyItem.MP_Response),
        1023
      },
      {
        typeof (JukeBoxWindow.ResPlayList),
        1024
      },
      {
        typeof (ReqJukeBox.Response),
        1025
      },
      {
        typeof (FlowNode_ReqJukeBox.MP_Response),
        1026
      },
      {
        typeof (ReqJukeBoxPlaylistAdd.Response),
        1027
      },
      {
        typeof (FlowNode_ReqJukeBoxMylistAdd.MP_Add_Response),
        1028
      },
      {
        typeof (ReqJukeBoxPlaylistDel.Response),
        1029
      },
      {
        typeof (FlowNode_ReqJukeBoxMylistDel.MP_Del_Response),
        1030
      },
      {
        typeof (ReqGuildAttend.Response),
        1031
      },
      {
        typeof (FlowNode_ReqGuildAttend.MP_Response),
        1032
      },
      {
        typeof (ReqGuildRoleBonus.Response),
        1033
      },
      {
        typeof (FlowNode_ReqGuildRoleBonus.MP_Response),
        1034
      },
      {
        typeof (ReqGuildRaid.Response),
        1035
      },
      {
        typeof (FlowNode_ReqGuildRaid.MP_Response),
        1036
      },
      {
        typeof (ReqGuildRaidBtlLog.Response),
        1037
      },
      {
        typeof (FlowNode_ReqGuildRaidBtlLog.MP_Response),
        1038
      },
      {
        typeof (ReqGuildRaidInfo.Response),
        1039
      },
      {
        typeof (FlowNode_ReqGuildRaidInfo.MP_Response),
        1040
      },
      {
        typeof (ReqGuildRaidMail.Response),
        1041
      },
      {
        typeof (FlowNode_ReqGuildRaidMail.MP_Response),
        1042
      },
      {
        typeof (ReqGuildRaidMailRead.Response),
        1043
      },
      {
        typeof (FlowNode_ReqGuildRaidMailRead.MP_Response),
        1044
      },
      {
        typeof (ReqGuildRaidRanking.Response),
        1045
      },
      {
        typeof (FlowNode_ReqGuildRaidRanking.MP_Response),
        1046
      },
      {
        typeof (ReqGuildRaidRankingDamageRound.Response),
        1047
      },
      {
        typeof (FlowNode_ReqGuildRaidRankingDamageRound.MP_Response),
        1048
      },
      {
        typeof (ReqGuildRaidRankingDamageSummary.Response),
        1049
      },
      {
        typeof (FlowNode_ReqGuildRaidRankingDamageSummary.MP_Response),
        1050
      },
      {
        typeof (ReqGuildRaidRankingPort.Response),
        1051
      },
      {
        typeof (FlowNode_ReqGuildRaidRankingPort.MP_Response),
        1052
      },
      {
        typeof (ReqGuildRaidRankingPortBoss.Response),
        1053
      },
      {
        typeof (FlowNode_ReqGuildRaidRankingPortBoss.MP_Response),
        1054
      },
      {
        typeof (FlowNode_ReqGuildRaidReportDetail.MP_Response),
        1055
      },
      {
        typeof (ReqGuildRaidReportSelf.Response),
        1056
      },
      {
        typeof (FlowNode_ReqGuildRaidReportSelf.MP_Response),
        1057
      },
      {
        typeof (ReqGuildRaidRankingReward.Response),
        1058
      },
      {
        typeof (FlowNode_ReqGuildRaidReward.MP_Response),
        1059
      },
      {
        typeof (JSON_GvGNodeData),
        1060
      },
      {
        typeof (JSON_GvGResult),
        1061
      },
      {
        typeof (ReqGvG.Response),
        1062
      },
      {
        typeof (FlowNode_ReqGvG.MP_Response),
        1063
      },
      {
        typeof (JSON_GvGPartyUnit),
        1064
      },
      {
        typeof (ReqGvGBattle.Response),
        1065
      },
      {
        typeof (FlowNode_ReqGvGBattle.MP_Response),
        1066
      },
      {
        typeof (ReqGvGBattleCapture.Response),
        1067
      },
      {
        typeof (FlowNode_ReqGvGBattleCapture.MP_Response),
        1068
      },
      {
        typeof (JSON_GvGPartyNPC),
        1069
      },
      {
        typeof (JSON_GvGParty),
        1070
      },
      {
        typeof (ReqGvGBattleEnemy.Response),
        1071
      },
      {
        typeof (FlowNode_ReqGvGBattleEnemy.MP_Response),
        1072
      },
      {
        typeof (JSON_GvGBeatRanking),
        1073
      },
      {
        typeof (ReqGvGBeatRanking.Response),
        1074
      },
      {
        typeof (FlowNode_ReqGvGBeatRanking.MP_Response),
        1075
      },
      {
        typeof (ReqGvGNodeDeclare.Response),
        1076
      },
      {
        typeof (FlowNode_ReqGvGNodeDeclare.MP_Response),
        1077
      },
      {
        typeof (ReqGvGNodeDefenseEntry.Response),
        1078
      },
      {
        typeof (FlowNode_ReqGvGNodeDefenseEntry.MP_Response),
        1079
      },
      {
        typeof (ReqGvGNodeDetail.Response),
        1080
      },
      {
        typeof (FlowNode_ReqGvGNodeDetail.MP_Response),
        1081
      },
      {
        typeof (ReqGvGNodeOffenseEntry.Response),
        1082
      },
      {
        typeof (FlowNode_ReqGvGNodeOffenseEntry.MP_Response),
        1083
      },
      {
        typeof (JSON_GvGRankingData),
        1084
      },
      {
        typeof (ReqGvGRankingGroup.Response),
        1085
      },
      {
        typeof (FlowNode_ReqGvGRankingGroup.MP_Response),
        1086
      },
      {
        typeof (ReqGvGReward.Response),
        1087
      },
      {
        typeof (FlowNode_ReqGvGReward.MP_Response),
        1088
      },
      {
        typeof (Json_ChatChannelMasterParam.Fields),
        1089
      },
      {
        typeof (Json_ChatChannelMasterParam),
        1090
      },
      {
        typeof (LoginNewsInfo.JSON_PubInfo),
        1091
      },
      {
        typeof (FlowNode_ReqLoginPack.JSON_ReqLoginPackResponse),
        1092
      },
      {
        typeof (FlowNode_ReqLoginPack.MP_ReqLoginPackResponse),
        1093
      },
      {
        typeof (Json_Notify_Monthly),
        1094
      },
      {
        typeof (ReqMonthlyRecover.Response),
        1095
      },
      {
        typeof (FlowNode_ReqRecoverMonthly.MP_Response),
        1096
      },
      {
        typeof (ReqStoryExChallengeCountReset.Response),
        1097
      },
      {
        typeof (FlowNode_ReqStoryExTotalChallengeCountReset.MP_ReqStoryExChallengeCountResetResponse),
        1098
      },
      {
        typeof (ReqGetRune.Response),
        1099
      },
      {
        typeof (FlowNode_ReqRune.MP_Response),
        1100
      },
      {
        typeof (ReqRuneDisassembly.Response.Rewards),
        1101
      },
      {
        typeof (ReqRuneDisassembly.Response),
        1102
      },
      {
        typeof (FlowNode_ReqRuneDisassembly.MP_Response),
        1103
      },
      {
        typeof (ReqRuneEnhance.Response),
        1104
      },
      {
        typeof (FlowNode_ReqRuneEnhance.MP_Response),
        1105
      },
      {
        typeof (ReqRuneEquip.Response),
        1106
      },
      {
        typeof (FlowNode_ReqRuneEquip.MP_Response),
        1107
      },
      {
        typeof (ReqRuneEvo.Response),
        1108
      },
      {
        typeof (FlowNode_ReqRuneEvo.MP_Response),
        1109
      },
      {
        typeof (ReqRuneFavorite.Response),
        1110
      },
      {
        typeof (FlowNode_ReqRuneFavorite.MP_Response),
        1111
      },
      {
        typeof (ReqRuneParamEnhEvo.Response),
        1112
      },
      {
        typeof (FlowNode_ReqRuneParamEnhEvo.MP_Response),
        1113
      },
      {
        typeof (ReqRuneResetParamBase.Response),
        1114
      },
      {
        typeof (FlowNode_ReqRuneResetParamBase.MP_Response),
        1115
      },
      {
        typeof (ReqRuneResetStatusEvo.Response),
        1116
      },
      {
        typeof (FlowNode_ReqRuneResetStatusEvo.MP_Response),
        1117
      },
      {
        typeof (ReqRuneStorageAdd.Response),
        1118
      },
      {
        typeof (FlowNode_ReqRuneStorageAdd.MP_Response),
        1119
      },
      {
        typeof (ReqTrophyStarMission.Response),
        1120
      },
      {
        typeof (FlowNode_ReqTrophyStarMission.MP_Response),
        1121
      },
      {
        typeof (ReqTrophyStarMissionGetReward.Response.JSON_StarMissionConceptCard),
        1122
      },
      {
        typeof (ReqTrophyStarMissionGetReward.Response),
        1123
      },
      {
        typeof (FlowNode_ReqTrophyStarMissionGetReward.MP_Response),
        1124
      },
      {
        typeof (ReqUnitRentalAdd.Response),
        1125
      },
      {
        typeof (FlowNode_ReqUnitRentalAdd.MP_Response),
        1126
      },
      {
        typeof (ReqUnitRentalExec.Response),
        1127
      },
      {
        typeof (FlowNode_ReqUnitRentalExec.MP_Response),
        1128
      },
      {
        typeof (ReqUnitRentalLeave.Response),
        1129
      },
      {
        typeof (FlowNode_ReqUnitRentalLeave.MP_Response),
        1130
      },
      {
        typeof (EmbeddedTutorialMasterParams.JSON_EmbededMasterParam),
        1131
      },
      {
        typeof (EmbeddedTutorialMasterParams.JSON_EmbededQuestParam),
        1132
      },
      {
        typeof (JukeBoxParam),
        1133
      },
      {
        typeof (JukeBoxSectionParam),
        1134
      },
      {
        typeof (GuildEmblemParam),
        1135
      },
      {
        typeof (JSON_ProductSaleInfo),
        1136
      },
      {
        typeof (JSON_ProductParam),
        1137
      },
      {
        typeof (JSON_ProductBuyCoinParam),
        1138
      },
      {
        typeof (JSON_ProductParamResponse),
        1139
      },
      {
        typeof (RaidPeriodTimeScheduleParam),
        1140
      },
      {
        typeof (RuneSetEffState),
        1141
      },
      {
        typeof (RuneSetEff),
        1142
      },
      {
        typeof (RuneSlotIndex),
        1143
      },
      {
        typeof (RuneParam),
        1144
      },
      {
        typeof (JSON_RuneLotteryState),
        1145
      },
      {
        typeof (RuneLotteryState),
        1146
      },
      {
        typeof (RuneCost),
        1147
      },
      {
        typeof (RuneDisassembly.Materials),
        1148
      },
      {
        typeof (RuneDisassembly),
        1149
      },
      {
        typeof (RuneMaterial),
        1150
      },
      {
        typeof (JSON_TrophyResponse),
        1151
      },
      {
        typeof (MP_TrophyResponse),
        1152
      },
      {
        typeof (AbilitySlots.MP_JobAbilityt_OverWriteResponse),
        1153
      },
      {
        typeof (ArtifactSlots.MP_ArtifactSet_OverWriteResponse),
        1154
      },
      {
        typeof (JSON_GvGBattleEndParam),
        1155
      },
      {
        typeof (ReplacePeriod),
        1156
      },
      {
        typeof (ReplaceSprite),
        1157
      },
      {
        typeof (VersusDraftList.VersusDraftMessageData),
        1158
      },
      {
        typeof (ReqSetConceptCardList.Response),
        1159
      },
      {
        typeof (PartyWindow2.MP_Response_SetConceptCardList),
        1160
      },
      {
        typeof (ReqUnitJob_OverWrite.Response),
        1161
      },
      {
        typeof (UnitJobDropdown.MP_UnitJob_OverWriteResponse),
        1162
      },
      {
        typeof (WebAPI.JSON_BaseResponse),
        1163
      },
      {
        typeof (ReqGuildAttend.RequestParam),
        1164
      },
      {
        typeof (ReqGuildRoleBonus.RequestParam),
        1165
      },
      {
        typeof (ReqGvGBattleExec.Response),
        1166
      },
      {
        typeof (ReqGetRune.RequestParam),
        1167
      },
      {
        typeof (ReqRuneEquip.RequestParam),
        1168
      },
      {
        typeof (ReqRuneEnhance.RequestParam),
        1169
      },
      {
        typeof (ReqRuneEvo.RequestParam),
        1170
      },
      {
        typeof (ReqRuneDisassembly.RequestParam),
        1171
      },
      {
        typeof (ReqRuneResetParamBase.RequestParam),
        1172
      },
      {
        typeof (ReqRuneResetStatusEvo.RequestParam),
        1173
      },
      {
        typeof (ReqRuneParamEnhEvo.RequestParam),
        1174
      },
      {
        typeof (ReqRuneFavorite.RequestParam),
        1175
      }
    };

    internal static object GetFormatter(System.Type t)
    {
      int num;
      if (!GeneratedResolverGetFormatterHelper.lookup.TryGetValue(t, out num))
        return (object) null;
      switch (num)
      {
        case 0:
          return (object) new ListFormatter<int>();
        case 1:
          return (object) new ArrayFormatter<EquipSkillSetting>();
        case 2:
          return (object) new ArrayFormatter<EquipAbilitySetting>();
        case 3:
          return (object) new ListFormatter<AIAction>();
        case 4:
          return (object) new ArrayFormatter<AIPatrolPoint>();
        case 5:
          return (object) new ListFormatter<OString>();
        case 6:
          return (object) new ListFormatter<UnitEntryTrigger>();
        case 7:
          return (object) new ListFormatter<MultiPlayResumeBuff.ResistStatus>();
        case 8:
          return (object) new ArrayFormatter<MultiPlayResumeAbilChg.Data>();
        case 9:
          return (object) new ArrayFormatter<MultiPlayResumeBuff>();
        case 10:
          return (object) new ArrayFormatter<MultiPlayResumeShield>();
        case 11:
          return (object) new ArrayFormatter<MultiPlayResumeMhmDmg>();
        case 12:
          return (object) new ArrayFormatter<MultiPlayResumeFtgt>();
        case 13:
          return (object) new ArrayFormatter<MultiPlayResumeAbilChg>();
        case 14:
          return (object) new ArrayFormatter<MultiPlayResumeAddedAbil>();
        case 15:
          return (object) new ListFormatter<MultiPlayResumeProtect>();
        case 16:
          return (object) new ArrayFormatter<MultiPlayResumeUnitData>();
        case 17:
          return (object) new ArrayFormatter<MultiPlayGimmickEventParam>();
        case 18:
          return (object) new ArrayFormatter<MultiPlayTrickParam>();
        case 19:
          return (object) new ListFormatter<AttackDetailTypes>();
        case 20:
          return (object) new ListFormatter<string>();
        case 21:
          return (object) new ListFormatter<QuestClearUnlockUnitDataParam>();
        case 22:
          return (object) new ArrayFormatter<OShort>();
        case 23:
          return (object) new ListFormatter<TokkouValue>();
        case 24:
          return (object) new ArrayFormatter<RecipeItem>();
        case 25:
          return (object) new ArrayFormatter<ReturnItem>();
        case 26:
          return (object) new ArrayFormatter<RarityEquipEnhanceParam.RankParam>();
        case 27:
          return (object) new ArrayFormatter<EquipData>();
        case 28:
          return (object) new ListFormatter<AbilityData>();
        case 29:
          return (object) new ArrayFormatter<ConceptCardEffectsParam>();
        case 30:
          return (object) new ListFormatter<ConceptLimitUpItemParam>();
        case 31:
          return (object) new ArrayFormatter<BuffEffectParam.Buff>();
        case 32:
          return (object) new ListFormatter<BuffEffect.BuffTarget>();
        case 33:
          return (object) new ListFormatter<ConceptCardEquipEffect>();
        case 34:
          return (object) new ArrayFormatter<ConceptCardData>();
        case 35:
          return (object) new ListFormatter<RuneBuffDataEvoState>();
        case 36:
          return (object) new ArrayFormatter<RuneData>();
        case 37:
          return (object) new ArrayFormatter<BuffEffect.BuffValues>();
        case 38:
          return (object) new ArrayFormatter<OString>();
        case 39:
          return (object) new ArrayFormatter<JobRankParam>();
        case 40:
          return (object) new ArrayFormatter<LearningSkill>();
        case 41:
          return (object) new ListFormatter<InspSkillTriggerParam.TriggerData>();
        case 42:
          return (object) new ListFormatter<InspSkillTriggerParam>();
        case 43:
          return (object) new ListFormatter<InspSkillParam>();
        case 44:
          return (object) new ListFormatter<InspirationSkillData>();
        case 45:
          return (object) new ArrayFormatter<ArtifactData>();
        case 46:
          return (object) new ArrayFormatter<TobiraLearnAbilityParam>();
        case 47:
          return (object) new ListFormatter<TobiraData>();
        case 48:
          return (object) new ArrayFormatter<JobData>();
        case 49:
          return (object) new ArrayFormatter<QuestClearUnlockUnitDataParam>();
        case 50:
          return (object) new ListFormatter<SkillDeriveParam>();
        case 51:
          return (object) new ListFormatter<AbilityDeriveParam>();
        case 52:
          return (object) new ArrayFormatter<SkillAbilityDeriveTriggerParam>();
        case 53:
          return (object) new ArrayFormatter<EUnitCondition>();
        case 54:
          return (object) new ListFormatter<SkillData>();
        case 55:
          return (object) new ListFormatter<Unit>();
        case 56:
          return (object) new ListFormatter<BuffAttachment.ResistStatusBuff>();
        case 57:
          return (object) new ListFormatter<BuffAttachment>();
        case 58:
          return (object) new ListFormatter<CondAttachment>();
        case 59:
          return (object) new ArrayFormatter<SkillCategory>();
        case 60:
          return (object) new ArrayFormatter<ParamTypes>();
        case 61:
          return (object) new ListFormatter<Unit.DropItem>();
        case 62:
          return (object) new ListFormatter<Unit.UnitShield>();
        case 63:
          return (object) new ListFormatter<Unit.UnitProtect>();
        case 64:
          return (object) new ListFormatter<Unit.UnitMhmDamage>();
        case 65:
          return (object) new ListFormatter<Unit.UnitInsp>();
        case 66:
          return (object) new ListFormatter<Unit.UnitForcedTargeting>();
        case 67:
          return (object) new ListFormatter<Unit.AbilityChange.Data>();
        case 68:
          return (object) new ListFormatter<Unit.AbilityChange>();
        case 69:
          return (object) new ArrayFormatter<Json_InspirationSkill>();
        case 70:
          return (object) new ArrayFormatter<JSON_GuildRaidMailListItem>();
        case 71:
          return (object) new ArrayFormatter<Json_CollaboSkill>();
        case 72:
          return (object) new ArrayFormatter<Json_Equip>();
        case 73:
          return (object) new ArrayFormatter<Json_Ability>();
        case 74:
          return (object) new ArrayFormatter<Json_Artifact>();
        case 75:
          return (object) new ArrayFormatter<Json_InspirationSkillExt>();
        case 76:
          return (object) new ArrayFormatter<Json_Job>();
        case 77:
          return (object) new ArrayFormatter<Json_UnitJob>();
        case 78:
          return (object) new ArrayFormatter<JSON_ConceptCard>();
        case 79:
          return (object) new ArrayFormatter<Json_Tobira>();
        case 80:
          return (object) new ArrayFormatter<Json_RuneBuffData>();
        case 81:
          return (object) new ArrayFormatter<Json_RuneData>();
        case 82:
          return (object) new ArrayFormatter<Json_Unit>();
        case 83:
          return (object) new ArrayFormatter<JSON_GuildRaidRankingMemberBoss>();
        case 84:
          return (object) new ArrayFormatter<Json_Item>();
        case 85:
          return (object) new ArrayFormatter<Json_Gift>();
        case 86:
          return (object) new ArrayFormatter<Json_Mail>();
        case 87:
          return (object) new ArrayFormatter<Json_Party>();
        case 88:
          return (object) new ArrayFormatter<Json_Friend>();
        case 89:
          return (object) new ArrayFormatter<Json_Skin>();
        case 90:
          return (object) new ArrayFormatter<Json_LoginBonus>();
        case 91:
          return (object) new ArrayFormatter<Json_PremiumLoginBonusItem>();
        case 92:
          return (object) new ArrayFormatter<Json_PremiumLoginBonus>();
        case 93:
          return (object) new ArrayFormatter<Json_LoginBonusTable>();
        case 94:
          return (object) new ArrayFormatter<Json_MultiFuids>();
        case 95:
          return (object) new ArrayFormatter<Json_VersusCount>();
        case 96:
          return (object) new ArrayFormatter<Json_ExpireItem>();
        case 97:
          return (object) new ArrayFormatter<JSON_TrophyProgress>();
        case 98:
          return (object) new ArrayFormatter<JSON_UnitOverWriteData>();
        case 99:
          return (object) new ArrayFormatter<JSON_PartyOverWrite>();
        case 100:
          return (object) new ArrayFormatter<Json_TrophyConceptCard>();
        case 101:
          return (object) new ArrayFormatter<JSON_SupportRanking>();
        case 102:
          return (object) new ArrayFormatter<JSON_SupportUnitRanking>();
        case 103:
          return (object) new ArrayFormatter<BattleCore.Json_BtlDrop>();
        case 104:
          return (object) new ArrayFormatter<BattleCore.Json_BtlDrop[]>();
        case 105:
          return (object) new ArrayFormatter<Json_BtlRewardConceptCard>();
        case 106:
          return (object) new ArrayFormatter<JSON_QuestProgress>();
        case 107:
          return (object) new ArrayFormatter<ReqDrawCard.CardInfo>();
        case 108:
          return (object) new ArrayFormatter<ReqDrawCard.CardInfo.Card>();
        case 109:
          return (object) new ArrayFormatter<JSON_FixParam>();
        case 110:
          return (object) new ArrayFormatter<JSON_UnitParam>();
        case 111:
          return (object) new ArrayFormatter<JSON_UnitJobOverwriteParam>();
        case 112:
          return (object) new ArrayFormatter<JSON_SkillParam>();
        case 113:
          return (object) new ArrayFormatter<JSON_BuffEffectParam>();
        case 114:
          return (object) new ArrayFormatter<JSON_CondEffectParam>();
        case 115:
          return (object) new ArrayFormatter<JSON_AbilityParam>();
        case 116:
          return (object) new ArrayFormatter<JSON_ItemParam>();
        case 117:
          return (object) new ArrayFormatter<JSON_ArtifactParam>();
        case 118:
          return (object) new ArrayFormatter<JSON_WeaponParam>();
        case 119:
          return (object) new ArrayFormatter<JSON_RecipeParam>();
        case 120:
          return (object) new ArrayFormatter<JSON_JobRankParam>();
        case 121:
          return (object) new ArrayFormatter<JSON_JobParam>();
        case 122:
          return (object) new ArrayFormatter<JSON_JobSetParam>();
        case 123:
          return (object) new ArrayFormatter<JSON_EvaluationParam>();
        case 124:
          return (object) new ArrayFormatter<JSON_AIParam>();
        case 125:
          return (object) new ArrayFormatter<JSON_GeoParam>();
        case 126:
          return (object) new ArrayFormatter<JSON_RarityParam>();
        case (int) sbyte.MaxValue:
          return (object) new ArrayFormatter<JSON_ShopParam>();
        case 128:
          return (object) new ArrayFormatter<JSON_PlayerParam>();
        case 129:
          return (object) new ArrayFormatter<JSON_GrowCurve>();
        case 130:
          return (object) new ArrayFormatter<JSON_GrowParam>();
        case 131:
          return (object) new ArrayFormatter<JSON_LocalNotificationParam>();
        case 132:
          return (object) new ArrayFormatter<JSON_TrophyCategoryParam>();
        case 133:
          return (object) new ArrayFormatter<JSON_ChallengeCategoryParam>();
        case 134:
          return (object) new ArrayFormatter<JSON_TrophyParam>();
        case 135:
          return (object) new ArrayFormatter<JSON_UnlockParam>();
        case 136:
          return (object) new ArrayFormatter<JSON_VipParam>();
        case 137:
          return (object) new ArrayFormatter<JSON_ArenaWinResult>();
        case 138:
          return (object) new ArrayFormatter<JSON_ArenaResult>();
        case 139:
          return (object) new ArrayFormatter<JSON_StreamingMovie>();
        case 140:
          return (object) new ArrayFormatter<JSON_BannerParam>();
        case 141:
          return (object) new ArrayFormatter<JSON_QuestClearUnlockUnitDataParam>();
        case 142:
          return (object) new ArrayFormatter<JSON_AwardParam>();
        case 143:
          return (object) new ArrayFormatter<JSON_LoginInfoParam>();
        case 144:
          return (object) new ArrayFormatter<JSON_CollaboSkillParam>();
        case 145:
          return (object) new ArrayFormatter<JSON_TrickParam>();
        case 146:
          return (object) new ArrayFormatter<JSON_BreakObjParam>();
        case 147:
          return (object) new ArrayFormatter<JSON_VersusMatchingParam>();
        case 148:
          return (object) new ArrayFormatter<JSON_VersusMatchCondParam>();
        case 149:
          return (object) new ArrayFormatter<JSON_TowerScoreThreshold>();
        case 150:
          return (object) new ArrayFormatter<JSON_TowerScore>();
        case 151:
          return (object) new ArrayFormatter<JSON_FriendPresentItemParam>();
        case 152:
          return (object) new ArrayFormatter<JSON_WeatherParam>();
        case 153:
          return (object) new ArrayFormatter<JSON_UnitUnlockTimeParam>();
        case 154:
          return (object) new ArrayFormatter<JSON_TobiraLearnAbilityParam>();
        case 155:
          return (object) new ArrayFormatter<JSON_TobiraParam>();
        case 156:
          return (object) new ArrayFormatter<JSON_TobiraCategoriesParam>();
        case 157:
          return (object) new ArrayFormatter<JSON_TobiraConditionParam>();
        case 158:
          return (object) new ArrayFormatter<JSON_TobiraCondsParam>();
        case 159:
          return (object) new ArrayFormatter<JSON_TobiraCondsUnitParam.JobCond>();
        case 160:
          return (object) new ArrayFormatter<JSON_TobiraCondsUnitParam>();
        case 161:
          return (object) new ArrayFormatter<JSON_TobiraRecipeMaterialParam>();
        case 162:
          return (object) new ArrayFormatter<JSON_TobiraRecipeParam>();
        case 163:
          return (object) new ArrayFormatter<JSON_ConceptCardEquipParam>();
        case 164:
          return (object) new ArrayFormatter<JSON_ConceptCardParam>();
        case 165:
          return (object) new ArrayFormatter<JSON_ConceptCardConditionsParam>();
        case 166:
          return (object) new ArrayFormatter<JSON_ConceptCardTrustRewardItemParam>();
        case 167:
          return (object) new ArrayFormatter<JSON_ConceptCardTrustRewardParam>();
        case 168:
          return (object) new ArrayFormatter<JSON_ConceptCardLsBuffCoefParam>();
        case 169:
          return (object) new ArrayFormatter<JSON_ConceptCardGroup>();
        case 170:
          return (object) new ArrayFormatter<JSON_ConceptLimitUpItem>();
        case 171:
          return (object) new ArrayFormatter<JSON_UnitGroupParam>();
        case 172:
          return (object) new ArrayFormatter<JSON_JobGroupParam>();
        case 173:
          return (object) new ArrayFormatter<JSON_StatusCoefficientParam>();
        case 174:
          return (object) new ArrayFormatter<JSON_CustomTargetParam>();
        case 175:
          return (object) new ArrayFormatter<JSON_SkillAbilityDeriveParam>();
        case 176:
          return (object) new ArrayFormatter<JSON_RaidPeriodParam>();
        case 177:
          return (object) new ArrayFormatter<JSON_RaidPeriodTimeScheduleParam>();
        case 178:
          return (object) new ArrayFormatter<JSON_RaidPeriodTimeParam>();
        case 179:
          return (object) new ArrayFormatter<JSON_RaidAreaParam>();
        case 180:
          return (object) new ArrayFormatter<JSON_RaidBossParam>();
        case 181:
          return (object) new ArrayFormatter<JSON_RaidBattleRewardWeightParam>();
        case 182:
          return (object) new ArrayFormatter<JSON_RaidBattleRewardParam>();
        case 183:
          return (object) new ArrayFormatter<JSON_RaidBeatRewardDataParam>();
        case 184:
          return (object) new ArrayFormatter<JSON_RaidBeatRewardParam>();
        case 185:
          return (object) new ArrayFormatter<JSON_RaidDamageRatioRewardRatioParam>();
        case 186:
          return (object) new ArrayFormatter<JSON_RaidDamageRatioRewardParam>();
        case 187:
          return (object) new ArrayFormatter<JSON_RaidDamageAmountRewardAmountParam>();
        case 188:
          return (object) new ArrayFormatter<JSON_RaidDamageAmountRewardParam>();
        case 189:
          return (object) new ArrayFormatter<JSON_RaidAreaClearRewardDataParam>();
        case 190:
          return (object) new ArrayFormatter<JSON_RaidAreaClearRewardParam>();
        case 191:
          return (object) new ArrayFormatter<JSON_RaidCompleteRewardDataParam>();
        case 192:
          return (object) new ArrayFormatter<JSON_RaidCompleteRewardParam>();
        case 193:
          return (object) new ArrayFormatter<JSON_RaidReward>();
        case 194:
          return (object) new ArrayFormatter<JSON_RaidRewardParam>();
        case 195:
          return (object) new ArrayFormatter<JSON_TipsParam>();
        case 196:
          return (object) new ArrayFormatter<JSON_GuildEmblemParam>();
        case 197:
          return (object) new ArrayFormatter<JSON_GuildFacilityEffectParam>();
        case 198:
          return (object) new ArrayFormatter<JSON_GuildFacilityParam>();
        case 199:
          return (object) new ArrayFormatter<JSON_GuildFacilityLvParam>();
        case 200:
          return (object) new ArrayFormatter<JSON_ConvertUnitPieceExcludeParam>();
        case 201:
          return (object) new ArrayFormatter<JSON_PremiumParam>();
        case 202:
          return (object) new ArrayFormatter<JSON_BuyCoinShopParam>();
        case 203:
          return (object) new ArrayFormatter<JSON_BuyCoinProductParam>();
        case 204:
          return (object) new ArrayFormatter<JSON_BuyCoinRewardItemParam>();
        case 205:
          return (object) new ArrayFormatter<JSON_BuyCoinRewardParam>();
        case 206:
          return (object) new ArrayFormatter<JSON_BuyCoinProductConvertParam>();
        case 207:
          return (object) new ArrayFormatter<JSON_DynamicTransformUnitParam>();
        case 208:
          return (object) new ArrayFormatter<JSON_RecommendedArtifactParam>();
        case 209:
          return (object) new ArrayFormatter<JSON_SkillMotionDataParam>();
        case 210:
          return (object) new ArrayFormatter<JSON_SkillMotionParam>();
        case 211:
          return (object) new ArrayFormatter<JSON_DependStateSpcEffParam>();
        case 212:
          return (object) new ArrayFormatter<JSON_InspSkillDerivation>();
        case 213:
          return (object) new ArrayFormatter<JSON_InspSkillParam>();
        case 214:
          return (object) new ArrayFormatter<JSON_InspSkillTriggerParam.JSON_TriggerData>();
        case 215:
          return (object) new ArrayFormatter<JSON_InspSkillTriggerParam>();
        case 216:
          return (object) new ArrayFormatter<JSON_InspSkillCostParam>();
        case 217:
          return (object) new ArrayFormatter<JSON_InspSkillLvUpCostParam.JSON_CostData>();
        case 218:
          return (object) new ArrayFormatter<JSON_InspSkillLvUpCostParam>();
        case 219:
          return (object) new ArrayFormatter<JSON_HighlightResource>();
        case 220:
          return (object) new ArrayFormatter<JSON_HighlightParam>();
        case 221:
          return (object) new ArrayFormatter<JSON_HighlightGiftData>();
        case 222:
          return (object) new ArrayFormatter<JSON_HighlightGift>();
        case 223:
          return (object) new ArrayFormatter<JSON_GenesisParam>();
        case 224:
          return (object) new ArrayFormatter<JSON_CoinBuyUseBonusParam>();
        case 225:
          return (object) new ArrayFormatter<JSON_CoinBuyUseBonusContentParam>();
        case 226:
          return (object) new ArrayFormatter<JSON_CoinBuyUseBonusRewardSetParam>();
        case 227:
          return (object) new ArrayFormatter<JSON_CoinBuyUseBonusItemParam>();
        case 228:
          return (object) new ArrayFormatter<JSON_CoinBuyUseBonusRewardParam>();
        case 229:
          return (object) new ArrayFormatter<JSON_UnitRentalNotificationDataParam>();
        case 230:
          return (object) new ArrayFormatter<JSON_UnitRentalNotificationParam>();
        case 231:
          return (object) new ArrayFormatter<JSON_UnitRentalParam.QuestInfo>();
        case 232:
          return (object) new ArrayFormatter<JSON_UnitRentalParam>();
        case 233:
          return (object) new ArrayFormatter<JSON_DrawCardRewardParam.Data>();
        case 234:
          return (object) new ArrayFormatter<JSON_DrawCardRewardParam>();
        case 235:
          return (object) new ArrayFormatter<JSON_DrawCardParam.DrawInfo>();
        case 236:
          return (object) new ArrayFormatter<JSON_DrawCardParam>();
        case 237:
          return (object) new ArrayFormatter<JSON_TrophyStarMissionRewardParam.Data>();
        case 238:
          return (object) new ArrayFormatter<JSON_TrophyStarMissionRewardParam>();
        case 239:
          return (object) new ArrayFormatter<JSON_TrophyStarMissionParam.StarSetParam>();
        case 240:
          return (object) new ArrayFormatter<JSON_TrophyStarMissionParam>();
        case 241:
          return (object) new ArrayFormatter<JSON_UnitPieceShopParam>();
        case 242:
          return (object) new ArrayFormatter<JSON_UnitPieceShopGroupCost>();
        case 243:
          return (object) new ArrayFormatter<JSON_UnitPieceShopGroupParam>();
        case 244:
          return (object) new ArrayFormatter<JSON_TwitterMessageDetailParam>();
        case 245:
          return (object) new ArrayFormatter<JSON_TwitterMessageParam>();
        case 246:
          return (object) new ArrayFormatter<JSON_FilterConceptCardConditionParam>();
        case 247:
          return (object) new ArrayFormatter<JSON_FilterConceptCardParam>();
        case 248:
          return (object) new ArrayFormatter<JSON_FilterRuneConditionParam>();
        case 249:
          return (object) new ArrayFormatter<JSON_FilterRuneParam>();
        case 250:
          return (object) new ArrayFormatter<JSON_FilterUnitConditionParam>();
        case 251:
          return (object) new ArrayFormatter<JSON_FilterUnitParam>();
        case 252:
          return (object) new ArrayFormatter<JSON_FilterArtifactParam.Condition>();
        case 253:
          return (object) new ArrayFormatter<JSON_FilterArtifactParam>();
        case 254:
          return (object) new ArrayFormatter<JSON_SortRuneConditionParam>();
        case (int) byte.MaxValue:
          return (object) new ArrayFormatter<JSON_SortRuneParam>();
        case 256:
          return (object) new ArrayFormatter<JSON_RuneParam>();
        case 257:
          return (object) new ArrayFormatter<JSON_RuneLottery>();
        case 258:
          return (object) new ArrayFormatter<JSON_RuneLotteryBaseState>();
        case 259:
          return (object) new ArrayFormatter<JSON_RuneLotteryEvoState>();
        case 260:
          return (object) new ArrayFormatter<JSON_RuneDisassembly>();
        case 261:
          return (object) new ArrayFormatter<JSON_RuneMaterial>();
        case 262:
          return (object) new ArrayFormatter<JSON_RuneCost>();
        case 263:
          return (object) new ArrayFormatter<JSON_RuneSetEffState>();
        case 264:
          return (object) new ArrayFormatter<JSON_RuneSetEff>();
        case 265:
          return (object) new ArrayFormatter<JSON_JukeBoxParam>();
        case 266:
          return (object) new ArrayFormatter<JSON_JukeBoxSectionParam>();
        case 267:
          return (object) new ArrayFormatter<JSON_UnitSameGroupParam>();
        case 268:
          return (object) new ArrayFormatter<JSON_AutoRepeatQuestBoxParam>();
        case 269:
          return (object) new ArrayFormatter<JSON_GuildAttendRewardDetail>();
        case 270:
          return (object) new ArrayFormatter<JSON_GuildAttendParam>();
        case 271:
          return (object) new ArrayFormatter<JSON_GuildAttendReward>();
        case 272:
          return (object) new ArrayFormatter<JSON_GuildAttendRewardParam>();
        case 273:
          return (object) new ArrayFormatter<JSON_GuildRoleBonusDetail>();
        case 274:
          return (object) new ArrayFormatter<JSON_GuildRoleBonus>();
        case 275:
          return (object) new ArrayFormatter<JSON_GUildRoleBonusReward>();
        case 276:
          return (object) new ArrayFormatter<JSON_GuildRoleBonusRewardParam>();
        case 277:
          return (object) new ArrayFormatter<JSON_ResetCostInfoParam>();
        case 278:
          return (object) new ArrayFormatter<JSON_ResetCostParam>();
        case 279:
          return (object) new ArrayFormatter<JSON_ProtectSkillParam>();
        case 280:
          return (object) new ArrayFormatter<JSON_ReplacePeriod>();
        case 281:
          return (object) new ArrayFormatter<JSON_ReplaceSprite>();
        case 282:
          return (object) new ArrayFormatter<JSON_InitPlayer>();
        case 283:
          return (object) new ArrayFormatter<JSON_InitUnit>();
        case 284:
          return (object) new ArrayFormatter<JSON_InitItem>();
        case 285:
          return (object) new ArrayFormatter<JSON_SectionParam>();
        case 286:
          return (object) new ArrayFormatter<JSON_ArchiveItemsParam>();
        case 287:
          return (object) new ArrayFormatter<JSON_ArchiveParam>();
        case 288:
          return (object) new ArrayFormatter<JSON_ChapterParam>();
        case 289:
          return (object) new ArrayFormatter<JSON_MapParam>();
        case 290:
          return (object) new ArrayFormatter<JSON_QuestParam>();
        case 291:
          return (object) new ArrayFormatter<JSON_InnerObjective>();
        case 292:
          return (object) new ArrayFormatter<JSON_ObjectiveParam>();
        case 293:
          return (object) new ArrayFormatter<JSON_MagnificationParam>();
        case 294:
          return (object) new ArrayFormatter<JSON_QuestCondParam>();
        case 295:
          return (object) new ArrayFormatter<JSON_QuestPartyParam>();
        case 296:
          return (object) new ArrayFormatter<JSON_QuestCampaignParentParam>();
        case 297:
          return (object) new ArrayFormatter<JSON_QuestCampaignChildParam>();
        case 298:
          return (object) new ArrayFormatter<JSON_QuestCampaignTrust>();
        case 299:
          return (object) new ArrayFormatter<JSON_QuestCampaignInspSkill>();
        case 300:
          return (object) new ArrayFormatter<JSON_TowerFloorParam>();
        case 301:
          return (object) new ArrayFormatter<JSON_TowerRewardItem>();
        case 302:
          return (object) new ArrayFormatter<JSON_TowerRewardParam>();
        case 303:
          return (object) new ArrayFormatter<JSON_TowerRoundRewardItem>();
        case 304:
          return (object) new ArrayFormatter<JSON_TowerRoundRewardParam>();
        case 305:
          return (object) new ArrayFormatter<JSON_TowerParam>();
        case 306:
          return (object) new ArrayFormatter<JSON_VersusTowerParam>();
        case 307:
          return (object) new ArrayFormatter<JSON_VersusSchedule>();
        case 308:
          return (object) new ArrayFormatter<JSON_VersusCoin>();
        case 309:
          return (object) new ArrayFormatter<JSON_MultiTowerFloorParam>();
        case 310:
          return (object) new ArrayFormatter<JSON_MultiTowerRewardItem>();
        case 311:
          return (object) new ArrayFormatter<JSON_MultiTowerRewardParam>();
        case 312:
          return (object) new ArrayFormatter<JSON_MapEffectParam>();
        case 313:
          return (object) new ArrayFormatter<JSON_WeatherSetParam>();
        case 314:
          return (object) new ArrayFormatter<JSON_RankingQuestParam>();
        case 315:
          return (object) new ArrayFormatter<JSON_RankingQuestScheduleParam>();
        case 316:
          return (object) new ArrayFormatter<JSON_RankingQuestRewardParam>();
        case 317:
          return (object) new ArrayFormatter<JSON_VersusWinBonusReward>();
        case 318:
          return (object) new ArrayFormatter<JSON_VersusFirstWinBonus>();
        case 319:
          return (object) new ArrayFormatter<JSON_VersusStreakWinSchedule>();
        case 320:
          return (object) new ArrayFormatter<JSON_VersusStreakWinBonus>();
        case 321:
          return (object) new ArrayFormatter<JSON_VersusRule>();
        case 322:
          return (object) new ArrayFormatter<JSON_VersusCoinCampParam>();
        case 323:
          return (object) new ArrayFormatter<JSON_VersusEnableTimeScheduleParam>();
        case 324:
          return (object) new ArrayFormatter<JSON_VersusEnableTimeParam>();
        case 325:
          return (object) new ArrayFormatter<JSON_VersusRankParam>();
        case 326:
          return (object) new ArrayFormatter<JSON_VersusRankClassParam>();
        case 327:
          return (object) new ArrayFormatter<JSON_VersusRankRankingRewardParam>();
        case 328:
          return (object) new ArrayFormatter<JSON_VersusRankRewardRewardParam>();
        case 329:
          return (object) new ArrayFormatter<JSON_VersusRankRewardParam>();
        case 330:
          return (object) new ArrayFormatter<JSON_VersusRankMissionScheduleParam>();
        case 331:
          return (object) new ArrayFormatter<JSON_VersusRankMissionParam>();
        case 332:
          return (object) new ArrayFormatter<JSON_QuestLobbyNewsParam>();
        case 333:
          return (object) new ArrayFormatter<JSON_GuerrillaShopAdventQuestParam>();
        case 334:
          return (object) new ArrayFormatter<JSON_GuerrillaShopScheduleAdventParam>();
        case 335:
          return (object) new ArrayFormatter<JSON_GuerrillaShopScheduleParam>();
        case 336:
          return (object) new ArrayFormatter<JSON_VersusDraftDeckParam>();
        case 337:
          return (object) new ArrayFormatter<JSON_VersusDraftUnitParam>();
        case 338:
          return (object) new ArrayFormatter<JSON_GenesisStarRewardParam>();
        case 339:
          return (object) new ArrayFormatter<JSON_GenesisStarParam>();
        case 340:
          return (object) new ArrayFormatter<JSON_GenesisChapterModeInfoParam>();
        case 341:
          return (object) new ArrayFormatter<JSON_GenesisChapterParam>();
        case 342:
          return (object) new ArrayFormatter<JSON_GenesisRewardDataParam>();
        case 343:
          return (object) new ArrayFormatter<JSON_GenesisRewardParam>();
        case 344:
          return (object) new ArrayFormatter<JSON_GenesisLapBossParam.LapInfo>();
        case 345:
          return (object) new ArrayFormatter<JSON_GenesisLapBossParam>();
        case 346:
          return (object) new ArrayFormatter<JSON_AdvanceStarRewardParam>();
        case 347:
          return (object) new ArrayFormatter<JSON_AdvanceStarParam>();
        case 348:
          return (object) new ArrayFormatter<JSON_AdvanceEventModeInfoParam>();
        case 349:
          return (object) new ArrayFormatter<JSON_AdvanceEventParam>();
        case 350:
          return (object) new ArrayFormatter<JSON_AdvanceRewardDataParam>();
        case 351:
          return (object) new ArrayFormatter<JSON_AdvanceRewardParam>();
        case 352:
          return (object) new ArrayFormatter<JSON_AdvanceLapBossParam.LapInfo>();
        case 353:
          return (object) new ArrayFormatter<JSON_AdvanceLapBossParam>();
        case 354:
          return (object) new ArrayFormatter<JSON_GuildRaidBossParam>();
        case 355:
          return (object) new ArrayFormatter<JSON_GuildRaidCoolDaysParam>();
        case 356:
          return (object) new ArrayFormatter<JSON_GuildRaidScoreDataParam>();
        case 357:
          return (object) new ArrayFormatter<JSON_GuildRaidScoreParam>();
        case 358:
          return (object) new ArrayFormatter<JSON_GuildRaidPeriodTime>();
        case 359:
          return (object) new ArrayFormatter<JSON_GuildRaidPeriodParam>();
        case 360:
          return (object) new ArrayFormatter<JSON_GuildRaidReward>();
        case 361:
          return (object) new ArrayFormatter<JSON_GuildRaidRewardParam>();
        case 362:
          return (object) new ArrayFormatter<JSON_GuildRaidRewardDmgRankingRankParam>();
        case 363:
          return (object) new ArrayFormatter<JSON_GuildRaidRewardDmgRankingParam>();
        case 364:
          return (object) new ArrayFormatter<JSON_GuildRaidRewardDmgRatio>();
        case 365:
          return (object) new ArrayFormatter<JSON_GuildRaidRewardDmgRatioParam>();
        case 366:
          return (object) new ArrayFormatter<JSON_GuildRaidRewardRound>();
        case 367:
          return (object) new ArrayFormatter<JSON_GuildRaidRewardRoundParam>();
        case 368:
          return (object) new ArrayFormatter<JSON_GuildRaidRewardRankingDataParam>();
        case 369:
          return (object) new ArrayFormatter<JSON_GuildRaidRewardRankingParam>();
        case 370:
          return (object) new ArrayFormatter<JSON_GvGPeriodParam>();
        case 371:
          return (object) new ArrayFormatter<JSON_GvGNodeParam>();
        case 372:
          return (object) new ArrayFormatter<JSON_GvGNPCPartyDetailParam>();
        case 373:
          return (object) new ArrayFormatter<JSON_GvGNPCPartyParam>();
        case 374:
          return (object) new ArrayFormatter<JSON_GvGNPCUnitParam>();
        case 375:
          return (object) new ArrayFormatter<JSON_GvGRewardRankingDetailParam>();
        case 376:
          return (object) new ArrayFormatter<JSON_GvGRewardRankingParam>();
        case 377:
          return (object) new ArrayFormatter<JSON_GvGRewardDetailParam>();
        case 378:
          return (object) new ArrayFormatter<JSON_GvGRewardParam>();
        case 379:
          return (object) new ArrayFormatter<JSON_GvGRuleParam>();
        case 380:
          return (object) new ArrayFormatter<JSON_SupportHistory>();
        case 381:
          return (object) new ArrayFormatter<Json_UnitPieceShopItem>();
        case 382:
          return (object) new ArrayFormatter<Json_MyPhotonPlayerBinaryParam.UnitDataElem>();
        case 383:
          return (object) new ArrayFormatter<Json_MyPhotonPlayerBinaryParam>();
        case 384:
          return (object) new ArrayFormatter<JukeBoxWindow.ResPlayList>();
        case 385:
          return (object) new ArrayFormatter<JSON_GuildFacilityData>();
        case 386:
          return (object) new ArrayFormatter<JSON_GuildRaidBattleLog>();
        case 387:
          return (object) new ArrayFormatter<JSON_GuildRaidChallengingPlayer>();
        case 388:
          return (object) new ArrayFormatter<JSON_GuildRaidRanking>();
        case 389:
          return (object) new ArrayFormatter<JSON_GuildRaidRankingDamage>();
        case 390:
          return (object) new ArrayFormatter<JSON_GuildRaidRankingMember>();
        case 391:
          return (object) new ArrayFormatter<JSON_GuildRaidReport>();
        case 392:
          return (object) new ArrayFormatter<JSON_GvGNodeData>();
        case 393:
          return (object) new ArrayFormatter<JSON_ViewGuild>();
        case 394:
          return (object) new ArrayFormatter<JSON_GvGPartyUnit>();
        case 395:
          return (object) new ArrayFormatter<JSON_GvGPartyNPC>();
        case 396:
          return (object) new ArrayFormatter<JSON_GvGParty>();
        case 397:
          return (object) new ArrayFormatter<JSON_GvGBeatRanking>();
        case 398:
          return (object) new ArrayFormatter<JSON_GvGRankingData>();
        case 399:
          return (object) new ArrayFormatter<Json_ChatChannelMasterParam>();
        case 400:
          return (object) new ArrayFormatter<Json_RuneEnforceGaugeData>();
        case 401:
          return (object) new ArrayFormatter<ReqRuneDisassembly.Response.Rewards>();
        case 402:
          return (object) new ArrayFormatter<ReqTrophyStarMissionGetReward.Response.JSON_StarMissionConceptCard>();
        case 403:
          return (object) new ArrayFormatter<JSON_ProductParam>();
        case 404:
          return (object) new ArrayFormatter<JSON_ProductBuyCoinParam>();
        case 405:
          return (object) new ArrayFormatter<RuneSetEffState>();
        case 406:
          return (object) new ArrayFormatter<RuneCost>();
        case 407:
          return (object) new ListFormatter<RuneDisassembly.Materials>();
        case 408:
          return (object) new ListFormatter<ReplacePeriod>();
        case 409:
          return (object) new EBattleRewardTypeFormatter();
        case 410:
          return (object) new eAIActionNoExecActFormatter();
        case 411:
          return (object) new eAIActionNextTurnActFormatter();
        case 412:
          return (object) new eMapUnitCtCalcTypeFormatter();
        case 413:
          return (object) new AIActionTypeFormatter();
        case 414:
          return (object) new EEventTriggerFormatter();
        case 415:
          return (object) new EEventTypeFormatter();
        case 416:
          return (object) new EEventGimmickFormatter();
        case 417:
          return (object) new ProtectTypesFormatter();
        case 418:
          return (object) new DamageTypesFormatter();
        case 419:
          return (object) new ESkillTypeFormatter();
        case 420:
          return (object) new ESkillTimingFormatter();
        case 421:
          return (object) new ESkillConditionFormatter();
        case 422:
          return (object) new ELineTypeFormatter();
        case 423:
          return (object) new ESelectTypeFormatter();
        case 424:
          return (object) new ECastTypesFormatter();
        case 425:
          return (object) new ESkillTargetFormatter();
        case 426:
          return (object) new SkillEffectTypesFormatter();
        case 427:
          return (object) new SkillParamCalcTypesFormatter();
        case 428:
          return (object) new EElementFormatter();
        case 429:
          return (object) new AttackTypesFormatter();
        case 430:
          return (object) new AttackDetailTypesFormatter();
        case 431:
          return (object) new ShieldTypesFormatter();
        case 432:
          return (object) new JewelDamageTypesFormatter();
        case 433:
          return (object) new eKnockBackDirFormatter();
        case 434:
          return (object) new eKnockBackDsFormatter();
        case 435:
          return (object) new eDamageDispTypeFormatter();
        case 436:
          return (object) new eTeleportTypeFormatter();
        case 437:
          return (object) new eTrickSetTypeFormatter();
        case 438:
          return (object) new eAbsorbAndGiveFormatter();
        case 439:
          return (object) new eSkillTargetExFormatter();
        case 440:
          return (object) new eTeleportSkillPosFormatter();
        case 441:
          return (object) new EUnlockTypesFormatter();
        case 442:
          return (object) new ESexFormatter();
        case 443:
          return (object) new EUnitTypeFormatter();
        case 444:
          return (object) new JobTypesFormatter();
        case 445:
          return (object) new RoleTypesFormatter();
        case 446:
          return (object) new EItemTypeFormatter();
        case 447:
          return (object) new EItemTabTypeFormatter();
        case 448:
          return (object) new GalleryVisibilityTypeFormatter();
        case 449:
          return (object) new eCardTypeFormatter();
        case 450:
          return (object) new EffectCheckTargetsFormatter();
        case 451:
          return (object) new EffectCheckTimingsFormatter();
        case 452:
          return (object) new EAppTypeFormatter();
        case 453:
          return (object) new EEffRangeFormatter();
        case 454:
          return (object) new BuffFlagsFormatter();
        case 455:
          return (object) new ParamTypesFormatter();
        case 456:
          return (object) new BuffTypesFormatter();
        case 457:
          return (object) new ArtifactTypesFormatter();
        case 458:
          return (object) new BuffMethodTypesFormatter();
        case 459:
          return (object) new eMovTypeFormatter();
        case 460:
          return (object) new EAbilityTypeFormatter();
        case 461:
          return (object) new EAbilitySlotFormatter();
        case 462:
          return (object) new EUseConditionsTypeFormatter();
        case 463:
          return (object) new EAbilityTypeDetailFormatter();
        case 464:
          return (object) new eInspSkillTriggerTypeFormatter();
        case 465:
          return (object) new CategoryFormatter();
        case 466:
          return (object) new AddTypeFormatter();
        case 467:
          return (object) new TemporaryFlagsFormatter();
        case 468:
          return (object) new UnitBadgeTypesFormatter();
        case 469:
          return (object) new ESkillAbilityDeriveCondsFormatter();
        case 470:
          return (object) new ConditionEffectTypesFormatter();
        case 471:
          return (object) new EUnitConditionFormatter();
        case 472:
          return (object) new SkillEffectTargetsFormatter();
        case 473:
          return (object) new StatusTypesFormatter();
        case 474:
          return (object) new ParamPrioritiesFormatter();
        case 475:
          return (object) new SkillCategoryFormatter();
        case 476:
          return (object) new EUnitSideFormatter();
        case 477:
          return (object) new eTypeMhmDamageFormatter();
        case 478:
          return (object) new EUnitDirectionFormatter();
        case 479:
          return (object) new eMapBreakClashTypeFormatter();
        case 480:
          return (object) new eMapBreakAITypeFormatter();
        case 481:
          return (object) new eMapBreakSideTypeFormatter();
        case 482:
          return (object) new eMapBreakRayTypeFormatter();
        case 483:
          return (object) new FriendStatesFormatter();
        case 484:
          return (object) new eUnlockTypeFormatter();
        case 485:
          return (object) new BattleCore_Json_BtlDropFormatter();
        case 486:
          return (object) new UnitEntryTriggerFormatter();
        case 487:
          return (object) new OIntFormatter();
        case 488:
          return (object) new OBoolFormatter();
        case 489:
          return (object) new GeoParamFormatter();
        case 490:
          return (object) new GridFormatter();
        case 491:
          return (object) new OStringFormatter();
        case 492:
          return (object) new SkillLockConditionFormatter();
        case 493:
          return (object) new EquipSkillSettingFormatter();
        case 494:
          return (object) new EquipAbilitySettingFormatter();
        case 495:
          return (object) new AIActionFormatter();
        case 496:
          return (object) new AIActionTableFormatter();
        case 497:
          return (object) new AIPatrolPointFormatter();
        case 498:
          return (object) new AIPatrolTableFormatter();
        case 499:
          return (object) new MapBreakObjFormatter();
        case 500:
          return (object) new OIntVector2Formatter();
        case 501:
          return (object) new EventTriggerFormatter();
        case 502:
          return (object) new NPCSettingFormatter();
        case 503:
          return (object) new MultiPlayResumeBuff_ResistStatusFormatter();
        case 504:
          return (object) new MultiPlayResumeBuffFormatter();
        case 505:
          return (object) new MultiPlayResumeShieldFormatter();
        case 506:
          return (object) new MultiPlayResumeMhmDmgFormatter();
        case 507:
          return (object) new MultiPlayResumeFtgtFormatter();
        case 508:
          return (object) new MultiPlayResumeAbilChg_DataFormatter();
        case 509:
          return (object) new MultiPlayResumeAbilChgFormatter();
        case 510:
          return (object) new MultiPlayResumeAddedAbilFormatter();
        case 511:
          return (object) new MultiPlayResumeProtectFormatter();
        case 512:
          return (object) new MultiPlayResumeUnitDataFormatter();
        case 513:
          return (object) new MultiPlayGimmickEventParamFormatter();
        case 514:
          return (object) new MultiPlayTrickParamFormatter();
        case 515:
          return (object) new MultiPlayResumeParam_WeatherInfoFormatter();
        case 516:
          return (object) new MultiPlayResumeParamFormatter();
        case 517:
          return (object) new SkillRankUpValueFormatter();
        case 518:
          return (object) new ProtectSkillParamFormatter();
        case 519:
          return (object) new SkillRankUpValueShortFormatter();
        case 520:
          return (object) new SkillParamFormatter();
        case 521:
          return (object) new QuestClearUnlockUnitDataParamFormatter();
        case 522:
          return (object) new FlagManagerFormatter();
        case 523:
          return (object) new OShortFormatter();
        case 524:
          return (object) new StatusParamFormatter();
        case 525:
          return (object) new EnchantParamFormatter();
        case 526:
          return (object) new UnitParam_StatusFormatter();
        case 527:
          return (object) new UnitParam_NoJobStatusFormatter();
        case 528:
          return (object) new UnitParamFormatter();
        case 529:
          return (object) new ElementParamFormatter();
        case 530:
          return (object) new BattleBonusParamFormatter();
        case 531:
          return (object) new TokkouValueFormatter();
        case 532:
          return (object) new TokkouParamFormatter();
        case 533:
          return (object) new BaseStatusFormatter();
        case 534:
          return (object) new RecipeItemFormatter();
        case 535:
          return (object) new RecipeParamFormatter();
        case 536:
          return (object) new ItemParamFormatter();
        case 537:
          return (object) new ReturnItemFormatter();
        case 538:
          return (object) new RarityEquipEnhanceParam_RankParamFormatter();
        case 539:
          return (object) new RarityEquipEnhanceParamFormatter();
        case 540:
          return (object) new RarityParamFormatter();
        case 541:
          return (object) new EquipDataFormatter();
        case 542:
          return (object) new OLongFormatter();
        case 543:
          return (object) new ConceptCardEffectsParamFormatter();
        case 544:
          return (object) new ConceptLimitUpItemParamFormatter();
        case 545:
          return (object) new ConceptCardParamFormatter();
        case 546:
          return (object) new BuffEffectParam_BuffFormatter();
        case 547:
          return (object) new BuffEffectParamFormatter();
        case 548:
          return (object) new BuffEffect_BuffTargetFormatter();
        case 549:
          return (object) new BuffEffectFormatter();
        case 550:
          return (object) new ConceptCardEquipEffectFormatter();
        case 551:
          return (object) new ConceptCardDataFormatter();
        case 552:
          return (object) new RuneLotteryBaseStateFormatter();
        case 553:
          return (object) new RuneBuffDataBaseStateFormatter();
        case 554:
          return (object) new RuneLotteryEvoStateFormatter();
        case 555:
          return (object) new RuneBuffDataEvoStateFormatter();
        case 556:
          return (object) new RuneStateDataFormatter();
        case 557:
          return (object) new RuneDataFormatter();
        case 558:
          return (object) new ArtifactParamFormatter();
        case 559:
          return (object) new BuffEffect_BuffValuesFormatter();
        case 560:
          return (object) new JobRankParamFormatter();
        case 561:
          return (object) new JobParamFormatter();
        case 562:
          return (object) new ItemDataFormatter();
        case 563:
          return (object) new LearningSkillFormatter();
        case 564:
          return (object) new AbilityParamFormatter();
        case 565:
          return (object) new InspSkillTriggerParam_TriggerDataFormatter();
        case 566:
          return (object) new InspSkillTriggerParamFormatter();
        case 567:
          return (object) new InspSkillParamFormatter();
        case 568:
          return (object) new InspirationSkillDataFormatter();
        case 569:
          return (object) new ArtifactDataFormatter();
        case 570:
          return (object) new JobDataFormatter();
        case 571:
          return (object) new TobiraLearnAbilityParamFormatter();
        case 572:
          return (object) new TobiraParamFormatter();
        case 573:
          return (object) new TobiraDataFormatter();
        case 574:
          return (object) new UnitDataFormatter();
        case 575:
          return (object) new SkillDeriveParamFormatter();
        case 576:
          return (object) new SkillAbilityDeriveTriggerParamFormatter();
        case 577:
          return (object) new SkillAbilityDeriveParamFormatter();
        case 578:
          return (object) new AbilityDeriveParamFormatter();
        case 579:
          return (object) new AbilityDataFormatter();
        case 580:
          return (object) new CondEffectParamFormatter();
        case 581:
          return (object) new CondEffectFormatter();
        case 582:
          return (object) new SkillDataFormatter();
        case 583:
          return (object) new BuffAttachment_ResistStatusBuffFormatter();
        case 584:
          return (object) new BuffAttachmentFormatter();
        case 585:
          return (object) new CondAttachmentFormatter();
        case 586:
          return (object) new AIParamFormatter();
        case 587:
          return (object) new Unit_DropItemFormatter();
        case 588:
          return (object) new Unit_UnitDropFormatter();
        case 589:
          return (object) new Unit_UnitStealFormatter();
        case 590:
          return (object) new Unit_UnitShieldFormatter();
        case 591:
          return (object) new Unit_UnitProtectFormatter();
        case 592:
          return (object) new Unit_UnitMhmDamageFormatter();
        case 593:
          return (object) new Unit_UnitInspFormatter();
        case 594:
          return (object) new Unit_UnitForcedTargetingFormatter();
        case 595:
          return (object) new Unit_AbilityChange_DataFormatter();
        case 596:
          return (object) new Unit_AbilityChangeFormatter();
        case 597:
          return (object) new DynamicTransformUnitParamFormatter();
        case 598:
          return (object) new BuffBitFormatter();
        case 599:
          return (object) new UnitFormatter();
        case 600:
          return (object) new MultiPlayResumeSkillDataFormatter();
        case 601:
          return (object) new Json_BtlRewardConceptCardFormatter();
        case 602:
          return (object) new SceneBattle_MultiPlayRecvDataFormatter();
        case 603:
          return (object) new Json_InspirationSkillFormatter();
        case 604:
          return (object) new Json_ArtifactFormatter();
        case 605:
          return (object) new JSON_ConceptCardFormatter();
        case 606:
          return (object) new JSON_PlayerGuildFormatter();
        case 607:
          return (object) new JSON_ViewGuildFormatter();
        case 608:
          return (object) new JSON_GuildFacilityDataFormatter();
        case 609:
          return (object) new JSON_GuildRaidPrevFormatter();
        case 610:
          return (object) new JSON_GuildRaidCurrentFormatter();
        case 611:
          return (object) new JSON_GuildRaidBattlePointFormatter();
        case 612:
          return (object) new JSON_GuildRaidBossInfoFormatter();
        case 613:
          return (object) new JSON_GuildRaidDataFormatter();
        case 614:
          return (object) new JSON_GuildRaidChallengingPlayerFormatter();
        case 615:
          return (object) new JSON_GuildRaidKnockDownInfoFormatter();
        case 616:
          return (object) new JSON_GuildRaidMailListItemFormatter();
        case 617:
          return (object) new JSON_GuildRaidMailOptionFormatter();
        case 618:
          return (object) new JSON_GuildRaidMailFormatter();
        case 619:
          return (object) new Json_MasterAbilityFormatter();
        case 620:
          return (object) new Json_CollaboSkillFormatter();
        case 621:
          return (object) new Json_CollaboAbilityFormatter();
        case 622:
          return (object) new Json_EquipFormatter();
        case 623:
          return (object) new Json_AbilityFormatter();
        case 624:
          return (object) new Json_InspirationSkillExtFormatter();
        case 625:
          return (object) new Json_JobSelectableFormatter();
        case 626:
          return (object) new Json_JobFormatter();
        case 627:
          return (object) new Json_UnitJobFormatter();
        case 628:
          return (object) new Json_UnitSelectableFormatter();
        case 629:
          return (object) new Json_TobiraFormatter();
        case 630:
          return (object) new Json_RuneBuffDataFormatter();
        case 631:
          return (object) new Json_RuneStateDataFormatter();
        case 632:
          return (object) new Json_RuneDataFormatter();
        case 633:
          return (object) new Json_UnitFormatter();
        case 634:
          return (object) new JSON_GuildRaidDeckFormatter();
        case 635:
          return (object) new JSON_GuildRaidBattleLogFormatter();
        case 636:
          return (object) new JSON_GuildRaidReportFormatter();
        case 637:
          return (object) new JSON_GuildRaidRankingGuildFormatter();
        case 638:
          return (object) new JSON_GuildRaidRankingFormatter();
        case 639:
          return (object) new JSON_GuildRaidRankingMemberBossFormatter();
        case 640:
          return (object) new JSON_GuildRaidRankingMemberFormatter();
        case 641:
          return (object) new JSON_GuildRaidRankingDamageFormatter();
        case 642:
          return (object) new JSON_GuildRaidGuildDataFormatter();
        case 643:
          return (object) new JSON_GuildRaidRankingRewardDataFormatter();
        case 644:
          return (object) new JSON_GuildRaidGuildRankingFormatter();
        case 645:
          return (object) new OByteFormatter();
        case 646:
          return (object) new OSbyteFormatter();
        case 647:
          return (object) new OUIntFormatter();
        case 648:
          return (object) new Json_CoinFormatter();
        case 649:
          return (object) new Json_HikkoshiFormatter();
        case 650:
          return (object) new Json_StaminaFormatter();
        case 651:
          return (object) new Json_CaveFormatter();
        case 652:
          return (object) new Json_AbilityUpFormatter();
        case 653:
          return (object) new Json_ArenaFormatter();
        case 654:
          return (object) new Json_TourFormatter();
        case 655:
          return (object) new Json_VipFormatter();
        case 656:
          return (object) new Json_PremiumFormatter();
        case 657:
          return (object) new Json_FreeGachaFormatter();
        case 658:
          return (object) new Json_PaidGachaFormatter();
        case 659:
          return (object) new Json_FriendsFormatter();
        case 660:
          return (object) new Json_MultiOptionFormatter();
        case 661:
          return (object) new Json_GuerrillaShopPeriodFormatter();
        case 662:
          return (object) new Json_PlayerDataFormatter();
        case 663:
          return (object) new Json_ItemFormatter();
        case 664:
          return (object) new Json_GiftConceptCardFormatter();
        case 665:
          return (object) new Json_GiftFormatter();
        case 666:
          return (object) new Json_MailFormatter();
        case 667:
          return (object) new Json_PartyFormatter();
        case 668:
          return (object) new Json_FriendFormatter();
        case 669:
          return (object) new Json_SkinFormatter();
        case 670:
          return (object) new Json_LoginBonusVipFormatter();
        case 671:
          return (object) new Json_LoginBonusFormatter();
        case 672:
          return (object) new Json_PremiumLoginBonusItemFormatter();
        case 673:
          return (object) new Json_PremiumLoginBonusFormatter();
        case 674:
          return (object) new Json_LoginBonusTableFormatter();
        case 675:
          return (object) new Json_NotifyFormatter();
        case 676:
          return (object) new Json_MultiFuidsFormatter();
        case 677:
          return (object) new Json_VersusCountFormatter();
        case 678:
          return (object) new Json_VersusFormatter();
        case 679:
          return (object) new Json_ExpireItemFormatter();
        case 680:
          return (object) new JSON_TrophyProgressFormatter();
        case 681:
          return (object) new JSON_UnitOverWriteDataFormatter();
        case 682:
          return (object) new JSON_PartyOverWriteFormatter();
        case 683:
          return (object) new JSON_StoryExChallengeCountFormatter();
        case 684:
          return (object) new Json_PlayerDataAllFormatter();
        case 685:
          return (object) new Json_TrophyPlayerDataFormatter();
        case 686:
          return (object) new Json_TrophyConceptCardFormatter();
        case 687:
          return (object) new Json_TrophyConceptCardsFormatter();
        case 688:
          return (object) new ReqTrophyStarMission_StarMission_InfoFormatter();
        case 689:
          return (object) new ReqTrophyStarMission_StarMissionFormatter();
        case 690:
          return (object) new Json_TrophyPlayerDataAllFormatter();
        case 691:
          return (object) new JSON_SupportHistoryFormatter();
        case 692:
          return (object) new JSON_SupportMyInfoFormatter();
        case 693:
          return (object) new JSON_SupportRankingGuildFormatter();
        case 694:
          return (object) new JSON_SupportRankingFormatter();
        case 695:
          return (object) new JSON_SupportRankingUserFormatter();
        case 696:
          return (object) new JSON_SupportUnitRankingFormatter();
        case 697:
          return (object) new JSON_SupportRankingUnitFormatter();
        case 698:
          return (object) new Json_RuneEnforceGaugeDataFormatter();
        case 699:
          return (object) new Json_UnitPieceShopItemFormatter();
        case 700:
          return (object) new ReqAutoRepeatQuestBox_ResponseFormatter();
        case 701:
          return (object) new FlowNode_ReqAutoRepeatQuestBox_MP_ReqAutoRepeatQuestBoxResponseFormatter();
        case 702:
          return (object) new ReqAutoRepeatQuestBoxAdd_ResponseFormatter();
        case 703:
          return (object) new FlowNode_ReqAutoRepeatQuestBoxAdd_MP_ReqAutoRepeatQuestBoxAddResponseFormatter();
        case 704:
          return (object) new Json_AutoRepeatQuestDataFormatter();
        case 705:
          return (object) new ReqAutoRepeatQuestProgress_ResponseFormatter();
        case 706:
          return (object) new FlowNode_ReqAutoRepeatQuestProgress_MP_AutoRepeatQuestProgressResponseFormatter();
        case 707:
          return (object) new JSON_QuestCountFormatter();
        case 708:
          return (object) new JSON_QuestProgressFormatter();
        case 709:
          return (object) new ReqAutoRepeatQuestEnd_ResponseFormatter();
        case 710:
          return (object) new FlowNode_ReqAutoRepeatQuestResult_MP_AutoRepeatQuestEndResponseFormatter();
        case 711:
          return (object) new ReqAutoRepeatQuestStart_ResponseFormatter();
        case 712:
          return (object) new FlowNode_ReqAutoRepeatQuestStart_MP_AutoRepeatQuestStartResponseFormatter();
        case 713:
          return (object) new ReqDrawCard_CardInfo_CardFormatter();
        case 714:
          return (object) new ReqDrawCard_CardInfoFormatter();
        case 715:
          return (object) new ReqDrawCard_Response_StatusFormatter();
        case 716:
          return (object) new ReqDrawCard_ResponseFormatter();
        case 717:
          return (object) new FlowNode_ReqDrawCard_MP_ResponseFormatter();
        case 718:
          return (object) new ReqDrawCardExec_ResponseFormatter();
        case 719:
          return (object) new FlowNode_ReqDrawCardExec_MP_ResponseFormatter();
        case 720:
          return (object) new ReqAllEquipExpAdd_ResponseFormatter();
        case 721:
          return (object) new FlowNode_AllEnhanceEquip_MP_ResponseFormatter();
        case 722:
          return (object) new FlowNode_Login_MP_PlayerDataAllFormatter();
        case 723:
          return (object) new FlowNode_PlayNew_MP_PlayNewFormatter();
        case 724:
          return (object) new ReqArtifactSet_OverWrite_ResponseFormatter();
        case 725:
          return (object) new FlowNode_ReqArtifactsSet_MP_ArtifactSet_OverWriteResponseFormatter();
        case 726:
          return (object) new FlowNode_ReqBingoProgress_JSON_BingoResponseFormatter();
        case 727:
          return (object) new FlowNode_ReqBingoProgress_MP_BingoResponseFormatter();
        case 728:
          return (object) new ReqSetConceptCard_OverWrite_ResponseFormatter();
        case 729:
          return (object) new FlowNode_ReqConceptCardSet_MP_SetConceptCard_OverWriteResponseFormatter();
        case 730:
          return (object) new ReqSetConceptLeaderSkill_OverWrite_ResponseFormatter();
        case 731:
          return (object) new FlowNode_ReqConceptLeaderSkillSet_MP_SetConceptLeaderSkill_OverWriteResponseFormatter();
        case 732:
          return (object) new JSON_FixParamFormatter();
        case 733:
          return (object) new JSON_UnitParamFormatter();
        case 734:
          return (object) new JSON_UnitJobOverwriteParamFormatter();
        case 735:
          return (object) new JSON_SkillParamFormatter();
        case 736:
          return (object) new JSON_BuffEffectParamFormatter();
        case 737:
          return (object) new JSON_CondEffectParamFormatter();
        case 738:
          return (object) new JSON_AbilityParamFormatter();
        case 739:
          return (object) new JSON_ItemParamFormatter();
        case 740:
          return (object) new JSON_ArtifactParamFormatter();
        case 741:
          return (object) new JSON_WeaponParamFormatter();
        case 742:
          return (object) new JSON_RecipeParamFormatter();
        case 743:
          return (object) new JSON_JobRankParamFormatter();
        case 744:
          return (object) new JSON_JobParamFormatter();
        case 745:
          return (object) new JSON_JobSetParamFormatter();
        case 746:
          return (object) new JSON_EvaluationParamFormatter();
        case 747:
          return (object) new JSON_AIParamFormatter();
        case 748:
          return (object) new JSON_GeoParamFormatter();
        case 749:
          return (object) new JSON_RarityParamFormatter();
        case 750:
          return (object) new JSON_ShopParamFormatter();
        case 751:
          return (object) new JSON_PlayerParamFormatter();
        case 752:
          return (object) new JSON_GrowCurveFormatter();
        case 753:
          return (object) new JSON_GrowParamFormatter();
        case 754:
          return (object) new JSON_LocalNotificationParamFormatter();
        case 755:
          return (object) new JSON_TrophyCategoryParamFormatter();
        case 756:
          return (object) new JSON_ChallengeCategoryParamFormatter();
        case 757:
          return (object) new JSON_TrophyParamFormatter();
        case 758:
          return (object) new JSON_UnlockParamFormatter();
        case 759:
          return (object) new JSON_VipParamFormatter();
        case 760:
          return (object) new JSON_ArenaWinResultFormatter();
        case 761:
          return (object) new JSON_ArenaResultFormatter();
        case 762:
          return (object) new JSON_StreamingMovieFormatter();
        case 763:
          return (object) new JSON_BannerParamFormatter();
        case 764:
          return (object) new JSON_QuestClearUnlockUnitDataParamFormatter();
        case 765:
          return (object) new JSON_AwardParamFormatter();
        case 766:
          return (object) new JSON_LoginInfoParamFormatter();
        case 767:
          return (object) new JSON_CollaboSkillParamFormatter();
        case 768:
          return (object) new JSON_TrickParamFormatter();
        case 769:
          return (object) new JSON_BreakObjParamFormatter();
        case 770:
          return (object) new JSON_VersusMatchingParamFormatter();
        case 771:
          return (object) new JSON_VersusMatchCondParamFormatter();
        case 772:
          return (object) new JSON_TowerScoreThresholdFormatter();
        case 773:
          return (object) new JSON_TowerScoreFormatter();
        case 774:
          return (object) new JSON_FriendPresentItemParamFormatter();
        case 775:
          return (object) new JSON_WeatherParamFormatter();
        case 776:
          return (object) new JSON_UnitUnlockTimeParamFormatter();
        case 777:
          return (object) new JSON_TobiraLearnAbilityParamFormatter();
        case 778:
          return (object) new JSON_TobiraParamFormatter();
        case 779:
          return (object) new JSON_TobiraCategoriesParamFormatter();
        case 780:
          return (object) new JSON_TobiraConditionParamFormatter();
        case 781:
          return (object) new JSON_TobiraCondsParamFormatter();
        case 782:
          return (object) new JSON_TobiraCondsUnitParam_JobCondFormatter();
        case 783:
          return (object) new JSON_TobiraCondsUnitParamFormatter();
        case 784:
          return (object) new JSON_TobiraRecipeMaterialParamFormatter();
        case 785:
          return (object) new JSON_TobiraRecipeParamFormatter();
        case 786:
          return (object) new JSON_ConceptCardEquipParamFormatter();
        case 787:
          return (object) new JSON_ConceptCardParamFormatter();
        case 788:
          return (object) new JSON_ConceptCardConditionsParamFormatter();
        case 789:
          return (object) new JSON_ConceptCardTrustRewardItemParamFormatter();
        case 790:
          return (object) new JSON_ConceptCardTrustRewardParamFormatter();
        case 791:
          return (object) new JSON_ConceptCardLsBuffCoefParamFormatter();
        case 792:
          return (object) new JSON_ConceptCardGroupFormatter();
        case 793:
          return (object) new JSON_ConceptLimitUpItemFormatter();
        case 794:
          return (object) new JSON_UnitGroupParamFormatter();
        case 795:
          return (object) new JSON_JobGroupParamFormatter();
        case 796:
          return (object) new JSON_StatusCoefficientParamFormatter();
        case 797:
          return (object) new JSON_CustomTargetParamFormatter();
        case 798:
          return (object) new JSON_SkillAbilityDeriveParamFormatter();
        case 799:
          return (object) new JSON_RaidPeriodParamFormatter();
        case 800:
          return (object) new JSON_RaidPeriodTimeScheduleParamFormatter();
        case 801:
          return (object) new JSON_RaidPeriodTimeParamFormatter();
        case 802:
          return (object) new JSON_RaidAreaParamFormatter();
        case 803:
          return (object) new JSON_RaidBossParamFormatter();
        case 804:
          return (object) new JSON_RaidBattleRewardWeightParamFormatter();
        case 805:
          return (object) new JSON_RaidBattleRewardParamFormatter();
        case 806:
          return (object) new JSON_RaidBeatRewardDataParamFormatter();
        case 807:
          return (object) new JSON_RaidBeatRewardParamFormatter();
        case 808:
          return (object) new JSON_RaidDamageRatioRewardRatioParamFormatter();
        case 809:
          return (object) new JSON_RaidDamageRatioRewardParamFormatter();
        case 810:
          return (object) new JSON_RaidDamageAmountRewardAmountParamFormatter();
        case 811:
          return (object) new JSON_RaidDamageAmountRewardParamFormatter();
        case 812:
          return (object) new JSON_RaidAreaClearRewardDataParamFormatter();
        case 813:
          return (object) new JSON_RaidAreaClearRewardParamFormatter();
        case 814:
          return (object) new JSON_RaidCompleteRewardDataParamFormatter();
        case 815:
          return (object) new JSON_RaidCompleteRewardParamFormatter();
        case 816:
          return (object) new JSON_RaidRewardFormatter();
        case 817:
          return (object) new JSON_RaidRewardParamFormatter();
        case 818:
          return (object) new JSON_TipsParamFormatter();
        case 819:
          return (object) new JSON_GuildEmblemParamFormatter();
        case 820:
          return (object) new JSON_GuildFacilityEffectParamFormatter();
        case 821:
          return (object) new JSON_GuildFacilityParamFormatter();
        case 822:
          return (object) new JSON_GuildFacilityLvParamFormatter();
        case 823:
          return (object) new JSON_ConvertUnitPieceExcludeParamFormatter();
        case 824:
          return (object) new JSON_PremiumParamFormatter();
        case 825:
          return (object) new JSON_BuyCoinShopParamFormatter();
        case 826:
          return (object) new JSON_BuyCoinProductParamFormatter();
        case 827:
          return (object) new JSON_BuyCoinRewardItemParamFormatter();
        case 828:
          return (object) new JSON_BuyCoinRewardParamFormatter();
        case 829:
          return (object) new JSON_BuyCoinProductConvertParamFormatter();
        case 830:
          return (object) new JSON_DynamicTransformUnitParamFormatter();
        case 831:
          return (object) new JSON_RecommendedArtifactParamFormatter();
        case 832:
          return (object) new JSON_SkillMotionDataParamFormatter();
        case 833:
          return (object) new JSON_SkillMotionParamFormatter();
        case 834:
          return (object) new JSON_DependStateSpcEffParamFormatter();
        case 835:
          return (object) new JSON_InspSkillDerivationFormatter();
        case 836:
          return (object) new JSON_InspSkillParamFormatter();
        case 837:
          return (object) new JSON_InspSkillTriggerParam_JSON_TriggerDataFormatter();
        case 838:
          return (object) new JSON_InspSkillTriggerParamFormatter();
        case 839:
          return (object) new JSON_InspSkillCostParamFormatter();
        case 840:
          return (object) new JSON_InspSkillLvUpCostParam_JSON_CostDataFormatter();
        case 841:
          return (object) new JSON_InspSkillLvUpCostParamFormatter();
        case 842:
          return (object) new JSON_HighlightResourceFormatter();
        case 843:
          return (object) new JSON_HighlightParamFormatter();
        case 844:
          return (object) new JSON_HighlightGiftDataFormatter();
        case 845:
          return (object) new JSON_HighlightGiftFormatter();
        case 846:
          return (object) new JSON_GenesisParamFormatter();
        case 847:
          return (object) new JSON_CoinBuyUseBonusParamFormatter();
        case 848:
          return (object) new JSON_CoinBuyUseBonusContentParamFormatter();
        case 849:
          return (object) new JSON_CoinBuyUseBonusRewardSetParamFormatter();
        case 850:
          return (object) new JSON_CoinBuyUseBonusItemParamFormatter();
        case 851:
          return (object) new JSON_CoinBuyUseBonusRewardParamFormatter();
        case 852:
          return (object) new JSON_UnitRentalNotificationDataParamFormatter();
        case 853:
          return (object) new JSON_UnitRentalNotificationParamFormatter();
        case 854:
          return (object) new JSON_UnitRentalParam_QuestInfoFormatter();
        case 855:
          return (object) new JSON_UnitRentalParamFormatter();
        case 856:
          return (object) new JSON_DrawCardRewardParam_DataFormatter();
        case 857:
          return (object) new JSON_DrawCardRewardParamFormatter();
        case 858:
          return (object) new JSON_DrawCardParam_DrawInfoFormatter();
        case 859:
          return (object) new JSON_DrawCardParamFormatter();
        case 860:
          return (object) new JSON_TrophyStarMissionRewardParam_DataFormatter();
        case 861:
          return (object) new JSON_TrophyStarMissionRewardParamFormatter();
        case 862:
          return (object) new JSON_TrophyStarMissionParam_StarSetParamFormatter();
        case 863:
          return (object) new JSON_TrophyStarMissionParamFormatter();
        case 864:
          return (object) new JSON_UnitPieceShopParamFormatter();
        case 865:
          return (object) new JSON_UnitPieceShopGroupCostFormatter();
        case 866:
          return (object) new JSON_UnitPieceShopGroupParamFormatter();
        case 867:
          return (object) new JSON_TwitterMessageDetailParamFormatter();
        case 868:
          return (object) new JSON_TwitterMessageParamFormatter();
        case 869:
          return (object) new JSON_FilterConceptCardConditionParamFormatter();
        case 870:
          return (object) new JSON_FilterConceptCardParamFormatter();
        case 871:
          return (object) new JSON_FilterRuneConditionParamFormatter();
        case 872:
          return (object) new JSON_FilterRuneParamFormatter();
        case 873:
          return (object) new JSON_FilterUnitConditionParamFormatter();
        case 874:
          return (object) new JSON_FilterUnitParamFormatter();
        case 875:
          return (object) new JSON_FilterArtifactParam_ConditionFormatter();
        case 876:
          return (object) new JSON_FilterArtifactParamFormatter();
        case 877:
          return (object) new JSON_SortRuneConditionParamFormatter();
        case 878:
          return (object) new JSON_SortRuneParamFormatter();
        case 879:
          return (object) new JSON_RuneParamFormatter();
        case 880:
          return (object) new JSON_RuneLotteryFormatter();
        case 881:
          return (object) new JSON_RuneLotteryBaseStateFormatter();
        case 882:
          return (object) new JSON_RuneLotteryEvoStateFormatter();
        case 883:
          return (object) new JSON_RuneDisassemblyFormatter();
        case 884:
          return (object) new JSON_RuneMaterialFormatter();
        case 885:
          return (object) new JSON_RuneCostFormatter();
        case 886:
          return (object) new JSON_RuneSetEffStateFormatter();
        case 887:
          return (object) new JSON_RuneSetEffFormatter();
        case 888:
          return (object) new JSON_JukeBoxParamFormatter();
        case 889:
          return (object) new JSON_JukeBoxSectionParamFormatter();
        case 890:
          return (object) new JSON_UnitSameGroupParamFormatter();
        case 891:
          return (object) new JSON_AutoRepeatQuestBoxParamFormatter();
        case 892:
          return (object) new JSON_GuildAttendRewardDetailFormatter();
        case 893:
          return (object) new JSON_GuildAttendParamFormatter();
        case 894:
          return (object) new JSON_GuildAttendRewardFormatter();
        case 895:
          return (object) new JSON_GuildAttendRewardParamFormatter();
        case 896:
          return (object) new JSON_GuildRoleBonusDetailFormatter();
        case 897:
          return (object) new JSON_GuildRoleBonusFormatter();
        case 898:
          return (object) new JSON_GUildRoleBonusRewardFormatter();
        case 899:
          return (object) new JSON_GuildRoleBonusRewardParamFormatter();
        case 900:
          return (object) new JSON_ResetCostInfoParamFormatter();
        case 901:
          return (object) new JSON_ResetCostParamFormatter();
        case 902:
          return (object) new JSON_ProtectSkillParamFormatter();
        case 903:
          return (object) new JSON_ReplacePeriodFormatter();
        case 904:
          return (object) new JSON_ReplaceSpriteFormatter();
        case 905:
          return (object) new JSON_InitPlayerFormatter();
        case 906:
          return (object) new JSON_InitUnitFormatter();
        case 907:
          return (object) new JSON_InitItemFormatter();
        case 908:
          return (object) new JSON_MasterParamFormatter();
        case 909:
          return (object) new FlowNode_ReqMasterParam_MP_MasterParamFormatter();
        case 910:
          return (object) new ReqOverWriteParty_ResponseFormatter();
        case 911:
          return (object) new FlowNode_ReqOverWriteParty_MP_OverWritePartyResponseFormatter();
        case 912:
          return (object) new JSON_SectionParamFormatter();
        case 913:
          return (object) new JSON_ArchiveItemsParamFormatter();
        case 914:
          return (object) new JSON_ArchiveParamFormatter();
        case 915:
          return (object) new JSON_ChapterParamFormatter();
        case 916:
          return (object) new JSON_MapParamFormatter();
        case 917:
          return (object) new JSON_QuestParamFormatter();
        case 918:
          return (object) new JSON_InnerObjectiveFormatter();
        case 919:
          return (object) new JSON_ObjectiveParamFormatter();
        case 920:
          return (object) new JSON_MagnificationParamFormatter();
        case 921:
          return (object) new JSON_QuestCondParamFormatter();
        case 922:
          return (object) new JSON_QuestPartyParamFormatter();
        case 923:
          return (object) new JSON_QuestCampaignParentParamFormatter();
        case 924:
          return (object) new JSON_QuestCampaignChildParamFormatter();
        case 925:
          return (object) new JSON_QuestCampaignTrustFormatter();
        case 926:
          return (object) new JSON_QuestCampaignInspSkillFormatter();
        case 927:
          return (object) new JSON_TowerFloorParamFormatter();
        case 928:
          return (object) new JSON_TowerRewardItemFormatter();
        case 929:
          return (object) new JSON_TowerRewardParamFormatter();
        case 930:
          return (object) new JSON_TowerRoundRewardItemFormatter();
        case 931:
          return (object) new JSON_TowerRoundRewardParamFormatter();
        case 932:
          return (object) new JSON_TowerParamFormatter();
        case 933:
          return (object) new JSON_VersusTowerParamFormatter();
        case 934:
          return (object) new JSON_VersusScheduleFormatter();
        case 935:
          return (object) new JSON_VersusCoinFormatter();
        case 936:
          return (object) new JSON_MultiTowerFloorParamFormatter();
        case 937:
          return (object) new JSON_MultiTowerRewardItemFormatter();
        case 938:
          return (object) new JSON_MultiTowerRewardParamFormatter();
        case 939:
          return (object) new JSON_MapEffectParamFormatter();
        case 940:
          return (object) new JSON_WeatherSetParamFormatter();
        case 941:
          return (object) new JSON_RankingQuestParamFormatter();
        case 942:
          return (object) new JSON_RankingQuestScheduleParamFormatter();
        case 943:
          return (object) new JSON_RankingQuestRewardParamFormatter();
        case 944:
          return (object) new JSON_VersusWinBonusRewardFormatter();
        case 945:
          return (object) new JSON_VersusFirstWinBonusFormatter();
        case 946:
          return (object) new JSON_VersusStreakWinScheduleFormatter();
        case 947:
          return (object) new JSON_VersusStreakWinBonusFormatter();
        case 948:
          return (object) new JSON_VersusRuleFormatter();
        case 949:
          return (object) new JSON_VersusCoinCampParamFormatter();
        case 950:
          return (object) new JSON_VersusEnableTimeScheduleParamFormatter();
        case 951:
          return (object) new JSON_VersusEnableTimeParamFormatter();
        case 952:
          return (object) new JSON_VersusRankParamFormatter();
        case 953:
          return (object) new JSON_VersusRankClassParamFormatter();
        case 954:
          return (object) new JSON_VersusRankRankingRewardParamFormatter();
        case 955:
          return (object) new JSON_VersusRankRewardRewardParamFormatter();
        case 956:
          return (object) new JSON_VersusRankRewardParamFormatter();
        case 957:
          return (object) new JSON_VersusRankMissionScheduleParamFormatter();
        case 958:
          return (object) new JSON_VersusRankMissionParamFormatter();
        case 959:
          return (object) new JSON_QuestLobbyNewsParamFormatter();
        case 960:
          return (object) new JSON_GuerrillaShopAdventQuestParamFormatter();
        case 961:
          return (object) new JSON_GuerrillaShopScheduleAdventParamFormatter();
        case 962:
          return (object) new JSON_GuerrillaShopScheduleParamFormatter();
        case 963:
          return (object) new JSON_VersusDraftDeckParamFormatter();
        case 964:
          return (object) new JSON_VersusDraftUnitParamFormatter();
        case 965:
          return (object) new JSON_GenesisStarRewardParamFormatter();
        case 966:
          return (object) new JSON_GenesisStarParamFormatter();
        case 967:
          return (object) new JSON_GenesisChapterModeInfoParamFormatter();
        case 968:
          return (object) new JSON_GenesisChapterParamFormatter();
        case 969:
          return (object) new JSON_GenesisRewardDataParamFormatter();
        case 970:
          return (object) new JSON_GenesisRewardParamFormatter();
        case 971:
          return (object) new JSON_GenesisLapBossParam_LapInfoFormatter();
        case 972:
          return (object) new JSON_GenesisLapBossParamFormatter();
        case 973:
          return (object) new JSON_AdvanceStarRewardParamFormatter();
        case 974:
          return (object) new JSON_AdvanceStarParamFormatter();
        case 975:
          return (object) new JSON_AdvanceEventModeInfoParamFormatter();
        case 976:
          return (object) new JSON_AdvanceEventParamFormatter();
        case 977:
          return (object) new JSON_AdvanceRewardDataParamFormatter();
        case 978:
          return (object) new JSON_AdvanceRewardParamFormatter();
        case 979:
          return (object) new JSON_AdvanceLapBossParam_LapInfoFormatter();
        case 980:
          return (object) new JSON_AdvanceLapBossParamFormatter();
        case 981:
          return (object) new JSON_GuildRaidBossParamFormatter();
        case 982:
          return (object) new JSON_GuildRaidCoolDaysParamFormatter();
        case 983:
          return (object) new JSON_GuildRaidScoreDataParamFormatter();
        case 984:
          return (object) new JSON_GuildRaidScoreParamFormatter();
        case 985:
          return (object) new JSON_GuildRaidPeriodTimeFormatter();
        case 986:
          return (object) new JSON_GuildRaidPeriodParamFormatter();
        case 987:
          return (object) new JSON_GuildRaidRewardFormatter();
        case 988:
          return (object) new JSON_GuildRaidRewardParamFormatter();
        case 989:
          return (object) new JSON_GuildRaidRewardDmgRankingRankParamFormatter();
        case 990:
          return (object) new JSON_GuildRaidRewardDmgRankingParamFormatter();
        case 991:
          return (object) new JSON_GuildRaidRewardDmgRatioFormatter();
        case 992:
          return (object) new JSON_GuildRaidRewardDmgRatioParamFormatter();
        case 993:
          return (object) new JSON_GuildRaidRewardRoundFormatter();
        case 994:
          return (object) new JSON_GuildRaidRewardRoundParamFormatter();
        case 995:
          return (object) new JSON_GuildRaidRewardRankingDataParamFormatter();
        case 996:
          return (object) new JSON_GuildRaidRewardRankingParamFormatter();
        case 997:
          return (object) new JSON_GvGPeriodParamFormatter();
        case 998:
          return (object) new JSON_GvGNodeParamFormatter();
        case 999:
          return (object) new JSON_GvGNPCPartyDetailParamFormatter();
        case 1000:
          return (object) new JSON_GvGNPCPartyParamFormatter();
        case 1001:
          return (object) new JSON_GvGNPCUnitParamFormatter();
        case 1002:
          return (object) new JSON_GvGRewardRankingDetailParamFormatter();
        case 1003:
          return (object) new JSON_GvGRewardRankingParamFormatter();
        case 1004:
          return (object) new JSON_GvGRewardDetailParamFormatter();
        case 1005:
          return (object) new JSON_GvGRewardParamFormatter();
        case 1006:
          return (object) new JSON_GvGRuleParamFormatter();
        case 1007:
          return (object) new Json_QuestListFormatter();
        case 1008:
          return (object) new FlowNode_ReqQuestParam_MP_QuestParamFormatter();
        case 1009:
          return (object) new ReqSetSupportRanking_ResponseFormatter();
        case 1010:
          return (object) new FlowNode_ReqSupportRanking_MP_ResponseFormatter();
        case 1011:
          return (object) new ReqSetSupportUsed_ResponseFormatter();
        case 1012:
          return (object) new FlowNode_ReqSupportUsed_MP_ResponseFormatter();
        case 1013:
          return (object) new ReqUnitPieceShopItemList_ResponseFormatter();
        case 1014:
          return (object) new FlowNode_RequestUnitPieceShopItems_MP_ResponseFormatter();
        case 1015:
          return (object) new FlowNode_ReqUpdateBingo_MP_UpdateBingoResponseFormatter();
        case 1016:
          return (object) new FlowNode_ReqUpdateTrophy_MP_TrophyPlayerDataAllResponseFormatter();
        case 1017:
          return (object) new ReqJobAbility_OverWrite_ResponseFormatter();
        case 1018:
          return (object) new FlowNode_SetAbility_MP_JobAbilityt_OverWriteResponseFormatter();
        case 1019:
          return (object) new Json_MyPhotonPlayerBinaryParam_UnitDataElemFormatter();
        case 1020:
          return (object) new Json_MyPhotonPlayerBinaryParamFormatter();
        case 1021:
          return (object) new FlowNode_StartMultiPlay_RecvDataFormatter();
        case 1022:
          return (object) new ReqUnitPieceShopBuypaid_ResponseFormatter();
        case 1023:
          return (object) new FlowNode_UnitPieceBuyItem_MP_ResponseFormatter();
        case 1024:
          return (object) new JukeBoxWindow_ResPlayListFormatter();
        case 1025:
          return (object) new ReqJukeBox_ResponseFormatter();
        case 1026:
          return (object) new FlowNode_ReqJukeBox_MP_ResponseFormatter();
        case 1027:
          return (object) new ReqJukeBoxPlaylistAdd_ResponseFormatter();
        case 1028:
          return (object) new FlowNode_ReqJukeBoxMylistAdd_MP_Add_ResponseFormatter();
        case 1029:
          return (object) new ReqJukeBoxPlaylistDel_ResponseFormatter();
        case 1030:
          return (object) new FlowNode_ReqJukeBoxMylistDel_MP_Del_ResponseFormatter();
        case 1031:
          return (object) new ReqGuildAttend_ResponseFormatter();
        case 1032:
          return (object) new FlowNode_ReqGuildAttend_MP_ResponseFormatter();
        case 1033:
          return (object) new ReqGuildRoleBonus_ResponseFormatter();
        case 1034:
          return (object) new FlowNode_ReqGuildRoleBonus_MP_ResponseFormatter();
        case 1035:
          return (object) new ReqGuildRaid_ResponseFormatter();
        case 1036:
          return (object) new FlowNode_ReqGuildRaid_MP_ResponseFormatter();
        case 1037:
          return (object) new ReqGuildRaidBtlLog_ResponseFormatter();
        case 1038:
          return (object) new FlowNode_ReqGuildRaidBtlLog_MP_ResponseFormatter();
        case 1039:
          return (object) new ReqGuildRaidInfo_ResponseFormatter();
        case 1040:
          return (object) new FlowNode_ReqGuildRaidInfo_MP_ResponseFormatter();
        case 1041:
          return (object) new ReqGuildRaidMail_ResponseFormatter();
        case 1042:
          return (object) new FlowNode_ReqGuildRaidMail_MP_ResponseFormatter();
        case 1043:
          return (object) new ReqGuildRaidMailRead_ResponseFormatter();
        case 1044:
          return (object) new FlowNode_ReqGuildRaidMailRead_MP_ResponseFormatter();
        case 1045:
          return (object) new ReqGuildRaidRanking_ResponseFormatter();
        case 1046:
          return (object) new FlowNode_ReqGuildRaidRanking_MP_ResponseFormatter();
        case 1047:
          return (object) new ReqGuildRaidRankingDamageRound_ResponseFormatter();
        case 1048:
          return (object) new FlowNode_ReqGuildRaidRankingDamageRound_MP_ResponseFormatter();
        case 1049:
          return (object) new ReqGuildRaidRankingDamageSummary_ResponseFormatter();
        case 1050:
          return (object) new FlowNode_ReqGuildRaidRankingDamageSummary_MP_ResponseFormatter();
        case 1051:
          return (object) new ReqGuildRaidRankingPort_ResponseFormatter();
        case 1052:
          return (object) new FlowNode_ReqGuildRaidRankingPort_MP_ResponseFormatter();
        case 1053:
          return (object) new ReqGuildRaidRankingPortBoss_ResponseFormatter();
        case 1054:
          return (object) new FlowNode_ReqGuildRaidRankingPortBoss_MP_ResponseFormatter();
        case 1055:
          return (object) new FlowNode_ReqGuildRaidReportDetail_MP_ResponseFormatter();
        case 1056:
          return (object) new ReqGuildRaidReportSelf_ResponseFormatter();
        case 1057:
          return (object) new FlowNode_ReqGuildRaidReportSelf_MP_ResponseFormatter();
        case 1058:
          return (object) new ReqGuildRaidRankingReward_ResponseFormatter();
        case 1059:
          return (object) new FlowNode_ReqGuildRaidReward_MP_ResponseFormatter();
        case 1060:
          return (object) new JSON_GvGNodeDataFormatter();
        case 1061:
          return (object) new JSON_GvGResultFormatter();
        case 1062:
          return (object) new ReqGvG_ResponseFormatter();
        case 1063:
          return (object) new FlowNode_ReqGvG_MP_ResponseFormatter();
        case 1064:
          return (object) new JSON_GvGPartyUnitFormatter();
        case 1065:
          return (object) new ReqGvGBattle_ResponseFormatter();
        case 1066:
          return (object) new FlowNode_ReqGvGBattle_MP_ResponseFormatter();
        case 1067:
          return (object) new ReqGvGBattleCapture_ResponseFormatter();
        case 1068:
          return (object) new FlowNode_ReqGvGBattleCapture_MP_ResponseFormatter();
        case 1069:
          return (object) new JSON_GvGPartyNPCFormatter();
        case 1070:
          return (object) new JSON_GvGPartyFormatter();
        case 1071:
          return (object) new ReqGvGBattleEnemy_ResponseFormatter();
        case 1072:
          return (object) new FlowNode_ReqGvGBattleEnemy_MP_ResponseFormatter();
        case 1073:
          return (object) new JSON_GvGBeatRankingFormatter();
        case 1074:
          return (object) new ReqGvGBeatRanking_ResponseFormatter();
        case 1075:
          return (object) new FlowNode_ReqGvGBeatRanking_MP_ResponseFormatter();
        case 1076:
          return (object) new ReqGvGNodeDeclare_ResponseFormatter();
        case 1077:
          return (object) new FlowNode_ReqGvGNodeDeclare_MP_ResponseFormatter();
        case 1078:
          return (object) new ReqGvGNodeDefenseEntry_ResponseFormatter();
        case 1079:
          return (object) new FlowNode_ReqGvGNodeDefenseEntry_MP_ResponseFormatter();
        case 1080:
          return (object) new ReqGvGNodeDetail_ResponseFormatter();
        case 1081:
          return (object) new FlowNode_ReqGvGNodeDetail_MP_ResponseFormatter();
        case 1082:
          return (object) new ReqGvGNodeOffenseEntry_ResponseFormatter();
        case 1083:
          return (object) new FlowNode_ReqGvGNodeOffenseEntry_MP_ResponseFormatter();
        case 1084:
          return (object) new JSON_GvGRankingDataFormatter();
        case 1085:
          return (object) new ReqGvGRankingGroup_ResponseFormatter();
        case 1086:
          return (object) new FlowNode_ReqGvGRankingGroup_MP_ResponseFormatter();
        case 1087:
          return (object) new ReqGvGReward_ResponseFormatter();
        case 1088:
          return (object) new FlowNode_ReqGvGReward_MP_ResponseFormatter();
        case 1089:
          return (object) new Json_ChatChannelMasterParam_FieldsFormatter();
        case 1090:
          return (object) new Json_ChatChannelMasterParamFormatter();
        case 1091:
          return (object) new LoginNewsInfo_JSON_PubInfoFormatter();
        case 1092:
          return (object) new FlowNode_ReqLoginPack_JSON_ReqLoginPackResponseFormatter();
        case 1093:
          return (object) new FlowNode_ReqLoginPack_MP_ReqLoginPackResponseFormatter();
        case 1094:
          return (object) new Json_Notify_MonthlyFormatter();
        case 1095:
          return (object) new ReqMonthlyRecover_ResponseFormatter();
        case 1096:
          return (object) new FlowNode_ReqRecoverMonthly_MP_ResponseFormatter();
        case 1097:
          return (object) new ReqStoryExChallengeCountReset_ResponseFormatter();
        case 1098:
          return (object) new FlowNode_ReqStoryExTotalChallengeCountReset_MP_ReqStoryExChallengeCountResetResponseFormatter();
        case 1099:
          return (object) new ReqGetRune_ResponseFormatter();
        case 1100:
          return (object) new FlowNode_ReqRune_MP_ResponseFormatter();
        case 1101:
          return (object) new ReqRuneDisassembly_Response_RewardsFormatter();
        case 1102:
          return (object) new ReqRuneDisassembly_ResponseFormatter();
        case 1103:
          return (object) new FlowNode_ReqRuneDisassembly_MP_ResponseFormatter();
        case 1104:
          return (object) new ReqRuneEnhance_ResponseFormatter();
        case 1105:
          return (object) new FlowNode_ReqRuneEnhance_MP_ResponseFormatter();
        case 1106:
          return (object) new ReqRuneEquip_ResponseFormatter();
        case 1107:
          return (object) new FlowNode_ReqRuneEquip_MP_ResponseFormatter();
        case 1108:
          return (object) new ReqRuneEvo_ResponseFormatter();
        case 1109:
          return (object) new FlowNode_ReqRuneEvo_MP_ResponseFormatter();
        case 1110:
          return (object) new ReqRuneFavorite_ResponseFormatter();
        case 1111:
          return (object) new FlowNode_ReqRuneFavorite_MP_ResponseFormatter();
        case 1112:
          return (object) new ReqRuneParamEnhEvo_ResponseFormatter();
        case 1113:
          return (object) new FlowNode_ReqRuneParamEnhEvo_MP_ResponseFormatter();
        case 1114:
          return (object) new ReqRuneResetParamBase_ResponseFormatter();
        case 1115:
          return (object) new FlowNode_ReqRuneResetParamBase_MP_ResponseFormatter();
        case 1116:
          return (object) new ReqRuneResetStatusEvo_ResponseFormatter();
        case 1117:
          return (object) new FlowNode_ReqRuneResetStatusEvo_MP_ResponseFormatter();
        case 1118:
          return (object) new ReqRuneStorageAdd_ResponseFormatter();
        case 1119:
          return (object) new FlowNode_ReqRuneStorageAdd_MP_ResponseFormatter();
        case 1120:
          return (object) new ReqTrophyStarMission_ResponseFormatter();
        case 1121:
          return (object) new FlowNode_ReqTrophyStarMission_MP_ResponseFormatter();
        case 1122:
          return (object) new ReqTrophyStarMissionGetReward_Response_JSON_StarMissionConceptCardFormatter();
        case 1123:
          return (object) new ReqTrophyStarMissionGetReward_ResponseFormatter();
        case 1124:
          return (object) new FlowNode_ReqTrophyStarMissionGetReward_MP_ResponseFormatter();
        case 1125:
          return (object) new ReqUnitRentalAdd_ResponseFormatter();
        case 1126:
          return (object) new FlowNode_ReqUnitRentalAdd_MP_ResponseFormatter();
        case 1127:
          return (object) new ReqUnitRentalExec_ResponseFormatter();
        case 1128:
          return (object) new FlowNode_ReqUnitRentalExec_MP_ResponseFormatter();
        case 1129:
          return (object) new ReqUnitRentalLeave_ResponseFormatter();
        case 1130:
          return (object) new FlowNode_ReqUnitRentalLeave_MP_ResponseFormatter();
        case 1131:
          return (object) new EmbeddedTutorialMasterParams_JSON_EmbededMasterParamFormatter();
        case 1132:
          return (object) new EmbeddedTutorialMasterParams_JSON_EmbededQuestParamFormatter();
        case 1133:
          return (object) new JukeBoxParamFormatter();
        case 1134:
          return (object) new JukeBoxSectionParamFormatter();
        case 1135:
          return (object) new GuildEmblemParamFormatter();
        case 1136:
          return (object) new JSON_ProductSaleInfoFormatter();
        case 1137:
          return (object) new JSON_ProductParamFormatter();
        case 1138:
          return (object) new JSON_ProductBuyCoinParamFormatter();
        case 1139:
          return (object) new JSON_ProductParamResponseFormatter();
        case 1140:
          return (object) new RaidPeriodTimeScheduleParamFormatter();
        case 1141:
          return (object) new RuneSetEffStateFormatter();
        case 1142:
          return (object) new RuneSetEffFormatter();
        case 1143:
          return (object) new RuneSlotIndexFormatter();
        case 1144:
          return (object) new RuneParamFormatter();
        case 1145:
          return (object) new JSON_RuneLotteryStateFormatter();
        case 1146:
          return (object) new RuneLotteryStateFormatter();
        case 1147:
          return (object) new RuneCostFormatter();
        case 1148:
          return (object) new RuneDisassembly_MaterialsFormatter();
        case 1149:
          return (object) new RuneDisassemblyFormatter();
        case 1150:
          return (object) new RuneMaterialFormatter();
        case 1151:
          return (object) new JSON_TrophyResponseFormatter();
        case 1152:
          return (object) new MP_TrophyResponseFormatter();
        case 1153:
          return (object) new AbilitySlots_MP_JobAbilityt_OverWriteResponseFormatter();
        case 1154:
          return (object) new ArtifactSlots_MP_ArtifactSet_OverWriteResponseFormatter();
        case 1155:
          return (object) new JSON_GvGBattleEndParamFormatter();
        case 1156:
          return (object) new ReplacePeriodFormatter();
        case 1157:
          return (object) new ReplaceSpriteFormatter();
        case 1158:
          return (object) new VersusDraftList_VersusDraftMessageDataFormatter();
        case 1159:
          return (object) new ReqSetConceptCardList_ResponseFormatter();
        case 1160:
          return (object) new PartyWindow2_MP_Response_SetConceptCardListFormatter();
        case 1161:
          return (object) new ReqUnitJob_OverWrite_ResponseFormatter();
        case 1162:
          return (object) new UnitJobDropdown_MP_UnitJob_OverWriteResponseFormatter();
        case 1163:
          return (object) new WebAPI_JSON_BaseResponseFormatter();
        case 1164:
          return (object) new ReqGuildAttend_RequestParamFormatter();
        case 1165:
          return (object) new ReqGuildRoleBonus_RequestParamFormatter();
        case 1166:
          return (object) new ReqGvGBattleExec_ResponseFormatter();
        case 1167:
          return (object) new ReqGetRune_RequestParamFormatter();
        case 1168:
          return (object) new ReqRuneEquip_RequestParamFormatter();
        case 1169:
          return (object) new ReqRuneEnhance_RequestParamFormatter();
        case 1170:
          return (object) new ReqRuneEvo_RequestParamFormatter();
        case 1171:
          return (object) new ReqRuneDisassembly_RequestParamFormatter();
        case 1172:
          return (object) new ReqRuneResetParamBase_RequestParamFormatter();
        case 1173:
          return (object) new ReqRuneResetStatusEvo_RequestParamFormatter();
        case 1174:
          return (object) new ReqRuneParamEnhEvo_RequestParamFormatter();
        case 1175:
          return (object) new ReqRuneFavorite_RequestParamFormatter();
        default:
          return (object) null;
      }
    }
  }
}
