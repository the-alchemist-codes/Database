﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Resolvers.DynamicContractlessObjectResolver
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack.Formatters;
using MessagePack.Internal;
using System;
using System.Reflection;

namespace MessagePack.Resolvers
{
  public sealed class DynamicContractlessObjectResolver : IFormatterResolver
  {
    public static readonly DynamicContractlessObjectResolver Instance = new DynamicContractlessObjectResolver();
    private static readonly DynamicAssembly assembly = new DynamicAssembly("MessagePack.Resolvers.DynamicContractlessObjectResolver");
    private const string ModuleName = "MessagePack.Resolvers.DynamicContractlessObjectResolver";

    private DynamicContractlessObjectResolver()
    {
    }

    public IMessagePackFormatter<T> GetFormatter<T>()
    {
      return DynamicContractlessObjectResolver.FormatterCache<T>.formatter;
    }

    private static class FormatterCache<T>
    {
      public static readonly IMessagePackFormatter<T> formatter;

      static FormatterCache()
      {
        if (typeof (T) == typeof (object))
          return;
        TypeInfo typeInfo1 = typeof (T).GetTypeInfo();
        if (typeInfo1.IsInterface)
          return;
        if (typeInfo1.IsNullable())
        {
          TypeInfo typeInfo2 = typeInfo1.GenericTypeArguments[0].GetTypeInfo();
          object formatterDynamic = DynamicContractlessObjectResolver.Instance.GetFormatterDynamic(typeInfo2.AsType());
          if (formatterDynamic == null)
            return;
          DynamicContractlessObjectResolver.FormatterCache<T>.formatter = (IMessagePackFormatter<T>) Activator.CreateInstance(typeof (StaticNullableFormatter<>).MakeGenericType(typeInfo2.AsType()), formatterDynamic);
        }
        else if (typeInfo1.IsAnonymous())
        {
          DynamicContractlessObjectResolver.FormatterCache<T>.formatter = (IMessagePackFormatter<T>) DynamicObjectTypeBuilder.BuildFormatterToDynamicMethod(typeof (T), true, true, false);
        }
        else
        {
          TypeInfo typeInfo2 = DynamicObjectTypeBuilder.BuildType(DynamicContractlessObjectResolver.assembly, typeof (T), true, true);
          if (typeInfo2 == null)
            return;
          DynamicContractlessObjectResolver.FormatterCache<T>.formatter = (IMessagePackFormatter<T>) Activator.CreateInstance(typeInfo2.AsType());
        }
      }
    }
  }
}
