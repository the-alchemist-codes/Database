﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Resolvers.DynamicEnumResolver
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack.Formatters;
using MessagePack.Internal;
using System;
using System.Reflection;
using System.Reflection.Emit;
using System.Threading;

namespace MessagePack.Resolvers
{
  public sealed class DynamicEnumResolver : IFormatterResolver
  {
    public static readonly DynamicEnumResolver Instance = new DynamicEnumResolver();
    private static int nameSequence = 0;
    private static readonly DynamicAssembly assembly = new DynamicAssembly("MessagePack.Resolvers.DynamicEnumResolver");
    private const string ModuleName = "MessagePack.Resolvers.DynamicEnumResolver";

    private DynamicEnumResolver()
    {
    }

    public IMessagePackFormatter<T> GetFormatter<T>()
    {
      return DynamicEnumResolver.FormatterCache<T>.formatter;
    }

    private static TypeInfo BuildType(Type enumType)
    {
      Type underlyingType = Enum.GetUnderlyingType(enumType);
      Type type1 = typeof (IMessagePackFormatter<>).MakeGenericType(enumType);
      TypeBuilder type2 = DynamicEnumResolver.assembly.DefineType("MessagePack.Formatters." + enumType.FullName.Replace(".", "_") + "Formatter" + (object) Interlocked.Increment(ref DynamicEnumResolver.nameSequence), TypeAttributes.Public | TypeAttributes.Sealed, (Type) null, new Type[1]
      {
        type1
      });
      ILGenerator ilGenerator1 = type2.DefineMethod("Serialize", MethodAttributes.Public | MethodAttributes.Final | MethodAttributes.Virtual, typeof (int), new Type[4]
      {
        typeof (byte[]).MakeByRefType(),
        typeof (int),
        enumType,
        typeof (IFormatterResolver)
      }).GetILGenerator();
      ilGenerator1.Emit(OpCodes.Ldarg_1);
      ilGenerator1.Emit(OpCodes.Ldarg_2);
      ilGenerator1.Emit(OpCodes.Ldarg_3);
      ilGenerator1.Emit(OpCodes.Call, typeof (MessagePackBinary).GetRuntimeMethod("Write" + underlyingType.Name, new Type[3]
      {
        typeof (byte[]).MakeByRefType(),
        typeof (int),
        underlyingType
      }));
      ilGenerator1.Emit(OpCodes.Ret);
      ILGenerator ilGenerator2 = type2.DefineMethod("Deserialize", MethodAttributes.Public | MethodAttributes.Final | MethodAttributes.Virtual, enumType, new Type[4]
      {
        typeof (byte[]),
        typeof (int),
        typeof (IFormatterResolver),
        typeof (int).MakeByRefType()
      }).GetILGenerator();
      ilGenerator2.Emit(OpCodes.Ldarg_1);
      ilGenerator2.Emit(OpCodes.Ldarg_2);
      ilGenerator2.Emit(OpCodes.Ldarg_S, (byte) 4);
      ilGenerator2.Emit(OpCodes.Call, typeof (MessagePackBinary).GetRuntimeMethod("Read" + underlyingType.Name, new Type[3]
      {
        typeof (byte[]),
        typeof (int),
        typeof (int).MakeByRefType()
      }));
      ilGenerator2.Emit(OpCodes.Ret);
      return type2.CreateTypeInfo();
    }

    private static class FormatterCache<T>
    {
      public static readonly IMessagePackFormatter<T> formatter;

      static FormatterCache()
      {
        TypeInfo typeInfo1 = typeof (T).GetTypeInfo();
        if (typeInfo1.IsNullable())
        {
          TypeInfo typeInfo2 = typeInfo1.GenericTypeArguments[0].GetTypeInfo();
          if (!typeInfo2.IsEnum)
            return;
          object formatterDynamic = DynamicEnumResolver.Instance.GetFormatterDynamic(typeInfo2.AsType());
          if (formatterDynamic == null)
            return;
          DynamicEnumResolver.FormatterCache<T>.formatter = (IMessagePackFormatter<T>) Activator.CreateInstance(typeof (StaticNullableFormatter<>).MakeGenericType(typeInfo2.AsType()), formatterDynamic);
        }
        else
        {
          if (!typeInfo1.IsEnum)
            return;
          DynamicEnumResolver.FormatterCache<T>.formatter = (IMessagePackFormatter<T>) Activator.CreateInstance(DynamicEnumResolver.BuildType(typeof (T)).AsType());
        }
      }
    }
  }
}
