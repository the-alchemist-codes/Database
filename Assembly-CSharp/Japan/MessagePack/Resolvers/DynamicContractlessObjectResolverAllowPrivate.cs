﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.Resolvers.DynamicContractlessObjectResolverAllowPrivate
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack.Formatters;
using MessagePack.Internal;
using System;
using System.Reflection;

namespace MessagePack.Resolvers
{
  public sealed class DynamicContractlessObjectResolverAllowPrivate : IFormatterResolver
  {
    public static readonly DynamicContractlessObjectResolverAllowPrivate Instance = new DynamicContractlessObjectResolverAllowPrivate();

    public IMessagePackFormatter<T> GetFormatter<T>()
    {
      return DynamicContractlessObjectResolverAllowPrivate.FormatterCache<T>.formatter;
    }

    private static class FormatterCache<T>
    {
      public static readonly IMessagePackFormatter<T> formatter;

      static FormatterCache()
      {
        if (typeof (T) == typeof (object))
          return;
        TypeInfo typeInfo1 = typeof (T).GetTypeInfo();
        if (typeInfo1.IsInterface)
          return;
        if (typeInfo1.IsNullable())
        {
          TypeInfo typeInfo2 = typeInfo1.GenericTypeArguments[0].GetTypeInfo();
          object formatterDynamic = DynamicContractlessObjectResolverAllowPrivate.Instance.GetFormatterDynamic(typeInfo2.AsType());
          if (formatterDynamic == null)
            return;
          DynamicContractlessObjectResolverAllowPrivate.FormatterCache<T>.formatter = (IMessagePackFormatter<T>) Activator.CreateInstance(typeof (StaticNullableFormatter<>).MakeGenericType(typeInfo2.AsType()), formatterDynamic);
        }
        else if (typeInfo1.IsAnonymous())
          DynamicContractlessObjectResolverAllowPrivate.FormatterCache<T>.formatter = (IMessagePackFormatter<T>) DynamicObjectTypeBuilder.BuildFormatterToDynamicMethod(typeof (T), true, true, false);
        else
          DynamicContractlessObjectResolverAllowPrivate.FormatterCache<T>.formatter = (IMessagePackFormatter<T>) DynamicObjectTypeBuilder.BuildFormatterToDynamicMethod(typeof (T), true, true, true);
      }
    }
  }
}
