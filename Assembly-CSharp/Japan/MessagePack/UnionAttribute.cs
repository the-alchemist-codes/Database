﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.UnionAttribute
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace MessagePack
{
  [AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface, AllowMultiple = true, Inherited = false)]
  public class UnionAttribute : Attribute
  {
    public UnionAttribute(int key, Type subType)
    {
      this.Key = key;
      this.SubType = subType;
    }

    public int Key { get; private set; }

    public Type SubType { get; private set; }
  }
}
