﻿// Decompiled with JetBrains decompiler
// Type: MessagePack.KeyAttribute
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace MessagePack
{
  [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false, Inherited = true)]
  public class KeyAttribute : Attribute
  {
    public KeyAttribute(int x)
    {
      this.IntKey = new int?(x);
    }

    public KeyAttribute(string x)
    {
      this.StringKey = x;
    }

    public int? IntKey { get; private set; }

    public string StringKey { get; private set; }
  }
}
