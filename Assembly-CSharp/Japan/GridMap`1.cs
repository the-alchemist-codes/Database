﻿// Decompiled with JetBrains decompiler
// Type: GridMap`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

public class GridMap<T>
{
  private T[] _data;
  private int _w;
  private int _h;

  public GridMap(int wSize, int hSize)
  {
    this._data = new T[wSize * hSize];
    this._w = wSize;
    this._h = hSize;
  }

  protected GridMap()
  {
  }

  public int w
  {
    get
    {
      return this._w;
    }
  }

  public int h
  {
    get
    {
      return this._h;
    }
  }

  public T[] data
  {
    get
    {
      return this._data;
    }
  }

  public bool isValid(int x, int y)
  {
    return 0 <= x && 0 <= y && x < this._w && y < this._h;
  }

  public T get(int x, int y)
  {
    return this._data[x + y * this._w];
  }

  public T get(int x, int y, T defaultValue)
  {
    return !this.isValid(x, y) ? defaultValue : this._data[x + y * this._w];
  }

  public void set(int x, int y, T src)
  {
    this._data[x + y * this._w] = src;
  }

  public void set(int idx, T src)
  {
    if (this._data == null || idx < 0 || idx >= this._data.Length)
      return;
    this._data[idx] = src;
  }

  public void resize(int cx, int cy)
  {
    this._w = cx;
    this._h = cy;
    this._data = new T[cx * cy];
  }

  public void fill(T value)
  {
    for (int index = this._w * this._h - 1; index >= 0; --index)
      this._data[index] = value;
  }

  public GridMap<T> clone()
  {
    return new GridMap<T>()
    {
      _w = this._w,
      _h = this._h,
      _data = (T[]) this._data.Clone()
    };
  }
}
