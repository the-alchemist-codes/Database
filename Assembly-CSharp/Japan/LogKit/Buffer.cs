﻿// Decompiled with JetBrains decompiler
// Type: LogKit.Buffer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace LogKit
{
  public class Buffer : List<Log>
  {
    private readonly int mSize;

    public Buffer(int size)
      : base(size)
    {
      this.mSize = size;
    }

    public string mDeviceID { get; set; }

    public bool IsAcquired { get; private set; }

    public int AvailableSize
    {
      get
      {
        return this.mSize - this.Count;
      }
    }

    public void Acquire()
    {
      this.IsAcquired = true;
    }

    public void Release()
    {
      this.IsAcquired = false;
    }
  }
}
