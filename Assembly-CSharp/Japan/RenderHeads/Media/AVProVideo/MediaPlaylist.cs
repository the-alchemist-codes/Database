﻿// Decompiled with JetBrains decompiler
// Type: RenderHeads.Media.AVProVideo.MediaPlaylist
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using System.Collections.Generic;
using UnityEngine;

namespace RenderHeads.Media.AVProVideo
{
  [Serializable]
  public class MediaPlaylist
  {
    [SerializeField]
    private List<MediaPlaylist.MediaItem> _items = new List<MediaPlaylist.MediaItem>(8);

    public List<MediaPlaylist.MediaItem> Items
    {
      get
      {
        return this._items;
      }
    }

    public bool HasItemAt(int index)
    {
      return index >= 0 && index < this._items.Count;
    }

    [Serializable]
    public class MediaItem
    {
      [SerializeField]
      public MediaPlayer.FileLocation fileLocation = MediaPlayer.FileLocation.RelativeToStreamingAssetsFolder;
      [SerializeField]
      public float progressTimeSeconds = 0.5f;
      [SerializeField]
      public bool autoPlay = true;
      [SerializeField]
      public float overrideTransitionDuration = 1f;
      [SerializeField]
      public string filePath;
      [SerializeField]
      public bool loop;
      [SerializeField]
      public PlaylistMediaPlayer.StartMode startMode;
      [SerializeField]
      public PlaylistMediaPlayer.ProgressMode progressMode;
      [SerializeField]
      public StereoPacking stereoPacking;
      [SerializeField]
      public AlphaPacking alphaPacking;
      [SerializeField]
      public bool isOverrideTransition;
      [SerializeField]
      public PlaylistMediaPlayer.Transition overrideTransition;
      [SerializeField]
      public PlaylistMediaPlayer.Easing overrideTransitionEasing;
    }
  }
}
