﻿// Decompiled with JetBrains decompiler
// Type: RenderHeads.Media.AVProVideo.Subtitle
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace RenderHeads.Media.AVProVideo
{
  public class Subtitle
  {
    public int index;
    public string text;
    public int timeStartMs;
    public int timeEndMs;

    public bool IsBefore(float time)
    {
      return (double) time > (double) this.timeStartMs && (double) time > (double) this.timeEndMs;
    }

    public bool IsTime(float time)
    {
      return (double) time >= (double) this.timeStartMs && (double) time < (double) this.timeEndMs;
    }
  }
}
