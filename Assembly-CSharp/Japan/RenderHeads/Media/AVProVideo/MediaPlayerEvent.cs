﻿// Decompiled with JetBrains decompiler
// Type: RenderHeads.Media.AVProVideo.MediaPlayerEvent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using System.Collections.Generic;
using UnityEngine.Events;

namespace RenderHeads.Media.AVProVideo
{
  [Serializable]
  public class MediaPlayerEvent : UnityEvent<MediaPlayer, MediaPlayerEvent.EventType, ErrorCode>
  {
    private List<UnityAction<MediaPlayer, MediaPlayerEvent.EventType, ErrorCode>> _listeners;

    public MediaPlayerEvent()
    {
      base.\u002Ector();
    }

    public bool HasListeners()
    {
      return this._listeners.Count > 0 || ((UnityEventBase) this).GetPersistentEventCount() > 0;
    }

    public void AddListener(
      UnityAction<MediaPlayer, MediaPlayerEvent.EventType, ErrorCode> call)
    {
      if (this._listeners.Contains(call))
        return;
      this._listeners.Add(call);
      base.AddListener(call);
    }

    public void RemoveListener(
      UnityAction<MediaPlayer, MediaPlayerEvent.EventType, ErrorCode> call)
    {
      int index = this._listeners.IndexOf(call);
      if (index < 0)
        return;
      this._listeners.RemoveAt(index);
      base.RemoveListener(call);
    }

    public enum EventType
    {
      MetaDataReady,
      ReadyToPlay,
      Started,
      FirstFrameReady,
      FinishedPlaying,
      Closing,
      Error,
      SubtitleChange,
      Stalled,
      Unstalled,
      ResolutionChanged,
      StartedSeeking,
      FinishedSeeking,
      StartedBuffering,
      FinishedBuffering,
      PropertiesChanged,
      PlaylistItemChanged,
      PlaylistFinished,
    }
  }
}
