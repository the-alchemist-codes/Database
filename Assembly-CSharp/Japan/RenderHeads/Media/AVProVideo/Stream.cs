﻿// Decompiled with JetBrains decompiler
// Type: RenderHeads.Media.AVProVideo.Stream
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace RenderHeads.Media.AVProVideo
{
  public abstract class Stream
  {
    public abstract int Width { get; }

    public abstract int Height { get; }

    public abstract int Bandwidth { get; }

    public abstract string URL { get; }

    public abstract List<Stream.Chunk> GetAllChunks();

    public abstract List<Stream.Chunk> GetChunks();

    public abstract List<Stream> GetAllStreams();

    public abstract List<Stream> GetStreams();

    public struct Chunk
    {
      public string name;
    }
  }
}
