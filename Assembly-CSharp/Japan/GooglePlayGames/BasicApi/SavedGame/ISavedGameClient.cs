﻿// Decompiled with JetBrains decompiler
// Type: GooglePlayGames.BasicApi.SavedGame.ISavedGameClient
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using System.Collections.Generic;

namespace GooglePlayGames.BasicApi.SavedGame
{
  public interface ISavedGameClient
  {
    void OpenWithAutomaticConflictResolution(
      string filename,
      DataSource source,
      ConflictResolutionStrategy resolutionStrategy,
      Action<SavedGameRequestStatus, ISavedGameMetadata> callback);

    void OpenWithManualConflictResolution(
      string filename,
      DataSource source,
      bool prefetchDataOnConflict,
      ConflictCallback conflictCallback,
      Action<SavedGameRequestStatus, ISavedGameMetadata> completedCallback);

    void ReadBinaryData(
      ISavedGameMetadata metadata,
      Action<SavedGameRequestStatus, byte[]> completedCallback);

    void ShowSelectSavedGameUI(
      string uiTitle,
      uint maxDisplayedSavedGames,
      bool showCreateSaveUI,
      bool showDeleteSaveUI,
      Action<SelectUIStatus, ISavedGameMetadata> callback);

    void CommitUpdate(
      ISavedGameMetadata metadata,
      SavedGameMetadataUpdate updateForMetadata,
      byte[] updatedBinaryData,
      Action<SavedGameRequestStatus, ISavedGameMetadata> callback);

    void FetchAllSavedGames(
      DataSource source,
      Action<SavedGameRequestStatus, List<ISavedGameMetadata>> callback);

    void Delete(ISavedGameMetadata metadata);
  }
}
