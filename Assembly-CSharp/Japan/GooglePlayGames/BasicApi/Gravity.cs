﻿// Decompiled with JetBrains decompiler
// Type: GooglePlayGames.BasicApi.Gravity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace GooglePlayGames.BasicApi
{
  public enum Gravity
  {
    CENTER_HORIZONTAL = 1,
    LEFT = 3,
    RIGHT = 5,
    TOP = 48, // 0x00000030
    BOTTOM = 80, // 0x00000050
  }
}
