﻿// Decompiled with JetBrains decompiler
// Type: GooglePlayGames.BasicApi.VideoCaptureMode
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace GooglePlayGames.BasicApi
{
  public enum VideoCaptureMode
  {
    Unknown = -1, // 0xFFFFFFFF
    File = 0,
    Stream = 1,
  }
}
