﻿// Decompiled with JetBrains decompiler
// Type: GooglePlayGames.BasicApi.Video.IVideoClient
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace GooglePlayGames.BasicApi.Video
{
  public interface IVideoClient
  {
    void GetCaptureCapabilities(Action<ResponseStatus, VideoCapabilities> callback);

    void ShowCaptureOverlay();

    void GetCaptureState(Action<ResponseStatus, VideoCaptureState> callback);

    void IsCaptureAvailable(VideoCaptureMode captureMode, Action<ResponseStatus, bool> callback);

    bool IsCaptureSupported();

    void RegisterCaptureOverlayStateChangedListener(CaptureOverlayStateListener listener);

    void UnregisterCaptureOverlayStateChangedListener();
  }
}
