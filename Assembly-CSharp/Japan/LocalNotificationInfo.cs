﻿// Decompiled with JetBrains decompiler
// Type: LocalNotificationInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

public class LocalNotificationInfo
{
  public int id;
  public int push_flg;
  public string trophy_iname;
  public string push_word;

  public bool Deserialize(JSON_LocalNotificationInfo json)
  {
    if (json == null)
      return false;
    this.id = json.fields.id;
    this.trophy_iname = json.fields.trophy_iname;
    this.push_flg = json.fields.push_flg;
    this.push_word = json.fields.push_word;
    return true;
  }
}
