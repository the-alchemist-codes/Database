﻿// Decompiled with JetBrains decompiler
// Type: ExceptionMonitor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using SRPG;
using System;
using System.Collections;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.EventSystems;

[AddComponentMenu("")]
public class ExceptionMonitor : MonoBehaviour
{
  private static bool mExceptionStop;
  private static ExceptionMonitor mInstnace;
  private bool bMessageDraw;
  private Coroutine m_OutputRoutine;

  public ExceptionMonitor()
  {
    base.\u002Ector();
  }

  public static bool IsExceptionStop
  {
    get
    {
      return ExceptionMonitor.mExceptionStop;
    }
  }

  private static string CrashLogTextPath
  {
    get
    {
      return AppPath.crashLogPath + "/crash.txt";
    }
  }

  public static void Start()
  {
    if (!UnityEngine.Object.op_Equality((UnityEngine.Object) ExceptionMonitor.mInstnace, (UnityEngine.Object) null))
      return;
    ExceptionMonitor.mInstnace = (ExceptionMonitor) new GameObject(nameof (ExceptionMonitor)).AddComponent<ExceptionMonitor>();
  }

  private void Awake()
  {
    UnityEngine.Object.DontDestroyOnLoad((UnityEngine.Object) ((Component) this).get_gameObject());
    ((UnityEngine.Object) this).set_hideFlags((HideFlags) 61);
    // ISSUE: method pointer
    Application.add_logMessageReceived(new Application.LogCallback((object) this, __methodptr(HandleLog)));
  }

  private void HandleLog(string logString, string stackTrace, LogType type)
  {
    if (type != 4 || this.bMessageDraw)
      return;
    this.SendLogMessage(logString, stackTrace);
    ExceptionMonitor.mExceptionStop = true;
    if (logString.IndexOf("DllNotFoundException:") >= 0)
    {
      if (this.m_OutputRoutine != null)
        return;
      this.m_OutputRoutine = this.StartCoroutine(this.OutputCrashInfo(logString, stackTrace, new Action(this.MessageBoxDLL)));
    }
    else
    {
      if (this.m_OutputRoutine != null)
        return;
      this.m_OutputRoutine = this.StartCoroutine(this.OutputCrashInfo(logString, stackTrace, new Action(this.MessageBoxDefault)));
    }
  }

  private void SendLogMessage(string logString, string stackTrace)
  {
    FlowNode_SendLogMessage.SendLogGenerator dict = new FlowNode_SendLogMessage.SendLogGenerator();
    dict.AddCommon(true, false, false, true);
    dict.Add("err", logString);
    dict.Add("trace", stackTrace);
    FlowNode_SendLogMessage.SendLogMessage(dict, "Exception");
  }

  private void MessageBoxDefault()
  {
    this.bMessageDraw = true;
    ((Behaviour) EventSystem.get_current().get_currentInputModule()).set_enabled(true);
    EmbedSystemMessage.Create(LocalizedText.Get("embed.APP_EXCEPTION"), (EmbedSystemMessage.SystemMessageEvent) (yes =>
    {
      this.bMessageDraw = false;
      Application.Quit();
      ExceptionMonitor.mExceptionStop = false;
    }), false);
  }

  private void MessageBoxDLL()
  {
    this.bMessageDraw = true;
    EmbedSystemMessageEx embedSystemMessageEx = EmbedSystemMessageEx.Create(LocalizedText.Get("embed.APP_DLL_EXCEPTION"));
    embedSystemMessageEx.AddButton(LocalizedText.Get("embed.APP_DLL_SUPPORT"), false, (EmbedSystemMessageEx.SystemMessageEvent) (ok => Application.OpenURL("https://al.fg-games.co.jp/news/206/")), false);
    embedSystemMessageEx.AddButton(LocalizedText.Get("embed.APP_DLL_QUIT"), true, (EmbedSystemMessageEx.SystemMessageEvent) (ok =>
    {
      this.bMessageDraw = false;
      Application.Quit();
      ExceptionMonitor.mExceptionStop = false;
    }), false);
  }

  [DebuggerHidden]
  private IEnumerator OutputCrashInfo(
    string logString,
    string stackTrace,
    Action onComplete)
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ExceptionMonitor.\u003COutputCrashInfo\u003Ec__Iterator0()
    {
      logString = logString,
      stackTrace = stackTrace,
      onComplete = onComplete,
      \u0024this = this
    };
  }
}
