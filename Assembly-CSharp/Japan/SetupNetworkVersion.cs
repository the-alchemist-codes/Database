﻿// Decompiled with JetBrains decompiler
// Type: SetupNetworkVersion
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using SRPG;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetupNetworkVersion : MonoBehaviour
{
  [SerializeField]
  private GameObject uiRoot;
  [SerializeField]
  private Toggle toggle;
  [SerializeField]
  private SRPG_InputField versionInputField;
  [SerializeField]
  private Dropdown serverNameDropdown;
  [SerializeField]
  private List<string> serverList;

  public SetupNetworkVersion()
  {
    base.\u002Ector();
  }

  private void Start()
  {
    this.uiRoot.SetActive(false);
  }
}
