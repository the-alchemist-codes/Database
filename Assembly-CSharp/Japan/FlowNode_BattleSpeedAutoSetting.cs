﻿// Decompiled with JetBrains decompiler
// Type: FlowNode_BattleSpeedAutoSetting
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using SRPG;
using UnityEngine;
using UnityEngine.UI;

[FlowNode.NodeType("System/Battle/Speed/BattleSpeedAutoSetting", 32741)]
[FlowNode.Pin(1, "オプション画面での倍速切り替え", FlowNode.PinTypes.Input, 1)]
[FlowNode.Pin(2, "オートプレイON", FlowNode.PinTypes.Input, 2)]
[FlowNode.Pin(3, "オートプレイOFF", FlowNode.PinTypes.Input, 3)]
[FlowNode.Pin(4, "倍速機能ON", FlowNode.PinTypes.Input, 4)]
[FlowNode.Pin(5, "倍速機能OFF", FlowNode.PinTypes.Input, 5)]
[FlowNode.Pin(6, "オートプレイ操作可", FlowNode.PinTypes.Input, 6)]
[FlowNode.Pin(7, "オートプレイ操作不可", FlowNode.PinTypes.Input, 7)]
[FlowNode.Pin(8, "倍速機能 操作可", FlowNode.PinTypes.Input, 8)]
[FlowNode.Pin(9, "倍速機能 操作不可", FlowNode.PinTypes.Input, 9)]
public class FlowNode_BattleSpeedAutoSetting : FlowNode
{
  [SerializeField]
  private bool isShowButtonNotVip = true;
  private const int INPUT_CHANGE_BATTLE_SPEED = 1;
  private const int INPUT_AUTO_PLAY_ON = 2;
  private const int INPUT_AUTO_PLAY_OFF = 3;
  private const int INPUT_BATTLE_SPEED_ON = 4;
  private const int INPUT_BATTLE_SPEED_OFF = 5;
  private const int INPUT_AUTO_PLAY_INTERACTABLE_ON = 6;
  private const int INPUT_AUTO_PLAY_INTERACTABLE_OFF = 7;
  private const int INPUT_BATTLE_SPEED_INTERACTABLE_ON = 8;
  private const int INPUT_BATTLE_SPEED_INTERACTABLE_OFF = 9;
  [SerializeField]
  private Toggle battleSpeedToggle;
  [SerializeField]
  private GameObject battleSpeedLockObj;
  [SerializeField]
  private Toggle autoPlayToggle;
  public const string AUTO_TOGGLE_CHANGE_KEY = "AUTO_TOGGLE_CHANGE_KEY";

  protected override void Awake()
  {
    base.Awake();
    BattleSpeedController.BattleTimeConfig = GameUtility.Config_UseBattleSpeed.Value;
    if (!BattleSpeedController.IsShowSpeedButton() && Object.op_Implicit((Object) this.battleSpeedToggle))
      ((Component) this.battleSpeedToggle).get_gameObject().SetActive(false);
    else if (BattleSpeedController.isPremium)
    {
      if (Object.op_Implicit((Object) this.battleSpeedLockObj))
        this.battleSpeedLockObj.SetActive(false);
      ((Selectable) this.battleSpeedToggle).set_interactable(true);
      if (MonoSingleton<GameManager>.Instance.Player != null && MonoSingleton<GameManager>.Instance.Player.IsAutoRepeatQuestMeasuring)
      {
        BattleSpeedController.BattleTimeConfig = true;
        ((Selectable) this.battleSpeedToggle).set_interactable(false);
      }
      GameUtility.SetToggle(this.battleSpeedToggle, BattleSpeedController.BattleTimeConfig);
    }
    else
    {
      if (Object.op_Implicit((Object) this.battleSpeedLockObj))
        this.battleSpeedLockObj.SetActive(true);
      if (this.isShowButtonNotVip)
      {
        ((Selectable) this.battleSpeedToggle).set_interactable(false);
        GameUtility.SetToggle(this.battleSpeedToggle, false);
      }
      else
        ((Component) this.battleSpeedToggle).get_gameObject().SetActive(false);
    }
  }

  public override void OnActivate(int pinID)
  {
    switch (pinID)
    {
      case 1:
        if (BattleSpeedController.isAutoOnly && BattleSpeedController.BattleTimeConfig)
          this.autoPlayToggle.set_isOn(true);
        this.battleSpeedToggle.set_isOn(BattleSpeedController.BattleTimeConfig);
        break;
      case 3:
        if (!BattleSpeedController.isAutoOnly)
          break;
        BattleSpeedController.BattleTimeConfig = false;
        this.battleSpeedToggle.set_isOn(false);
        BattleSpeedController.ResetSpeed();
        break;
      case 4:
        if (!BattleSpeedController.isPremium)
          break;
        if (BattleSpeedController.isAutoOnly)
          this.autoPlayToggle.set_isOn(true);
        BattleSpeedController.BattleTimeConfig = true;
        BattleSpeedController.SetUp();
        break;
      case 5:
        BattleSpeedController.BattleTimeConfig = false;
        BattleSpeedController.ResetSpeed();
        break;
      case 6:
        this.ChangeToggleInteractable(this.autoPlayToggle, true);
        break;
      case 7:
        this.ChangeToggleInteractable(this.autoPlayToggle, false);
        break;
      case 8:
        this.ChangeToggleInteractable(this.battleSpeedToggle, true);
        break;
      case 9:
        this.ChangeToggleInteractable(this.battleSpeedToggle, false);
        break;
    }
  }

  protected override void OnDestroy()
  {
    if (!Object.op_Inequality((Object) MonoSingleton<TimeManager>.Instance, (Object) null))
      return;
    BattleSpeedController.EndBattle();
  }

  private void ChangeToggleInteractable(Toggle target, bool is_interactable)
  {
    if (Object.op_Equality((Object) target, (Object) null))
      return;
    ((Selectable) target).set_interactable(is_interactable);
  }
}
