﻿// Decompiled with JetBrains decompiler
// Type: NetworkCullingHandler
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof (PhotonView))]
public class NetworkCullingHandler : MonoBehaviour, IPunObservable
{
  private int orderIndex;
  private CullArea cullArea;
  private List<byte> previousActiveCells;
  private List<byte> activeCells;
  private PhotonView pView;
  private Vector3 lastPosition;
  private Vector3 currentPosition;

  public NetworkCullingHandler()
  {
    base.\u002Ector();
  }

  private void OnEnable()
  {
    if (Object.op_Equality((Object) this.pView, (Object) null))
    {
      this.pView = (PhotonView) ((Component) this).GetComponent<PhotonView>();
      if (!this.pView.isMine)
        return;
    }
    if (Object.op_Equality((Object) this.cullArea, (Object) null))
      this.cullArea = (CullArea) Object.FindObjectOfType<CullArea>();
    this.previousActiveCells = new List<byte>(0);
    this.activeCells = new List<byte>(0);
    this.currentPosition = this.lastPosition = ((Component) this).get_transform().get_position();
  }

  private void Start()
  {
    if (!this.pView.isMine || !PhotonNetwork.inRoom)
      return;
    if (this.cullArea.NumberOfSubdivisions == 0)
    {
      this.pView.group = this.cullArea.FIRST_GROUP_ID;
      PhotonNetwork.SetInterestGroups(this.cullArea.FIRST_GROUP_ID, true);
    }
    else
      this.pView.ObservedComponents.Add((Component) this);
  }

  private void Update()
  {
    if (!this.pView.isMine)
      return;
    this.lastPosition = this.currentPosition;
    this.currentPosition = ((Component) this).get_transform().get_position();
    if (!Vector3.op_Inequality(this.currentPosition, this.lastPosition) || !this.HaveActiveCellsChanged())
      return;
    this.UpdateInterestGroups();
  }

  private void OnGUI()
  {
    if (!this.pView.isMine)
      return;
    string str1 = "Inside cells:\n";
    string str2 = "Subscribed cells:\n";
    for (int index = 0; index < this.activeCells.Count; ++index)
    {
      if (index <= this.cullArea.NumberOfSubdivisions)
        str1 = str1 + (object) this.activeCells[index] + " | ";
      str2 = str2 + (object) this.activeCells[index] + " | ";
    }
    Rect rect1 = new Rect(20f, (float) Screen.get_height() - 120f, 200f, 40f);
    string str3 = "<color=white>PhotonView Group: " + (object) this.pView.group + "</color>";
    GUIStyle guiStyle1 = new GUIStyle();
    guiStyle1.set_alignment((TextAnchor) 0);
    guiStyle1.set_fontSize(16);
    GUIStyle guiStyle2 = guiStyle1;
    GUI.Label(rect1, str3, guiStyle2);
    Rect rect2 = new Rect(20f, (float) Screen.get_height() - 100f, 200f, 40f);
    string str4 = "<color=white>" + str1 + "</color>";
    GUIStyle guiStyle3 = new GUIStyle();
    guiStyle3.set_alignment((TextAnchor) 0);
    guiStyle3.set_fontSize(16);
    GUIStyle guiStyle4 = guiStyle3;
    GUI.Label(rect2, str4, guiStyle4);
    Rect rect3 = new Rect(20f, (float) Screen.get_height() - 60f, 200f, 40f);
    string str5 = "<color=white>" + str2 + "</color>";
    GUIStyle guiStyle5 = new GUIStyle();
    guiStyle5.set_alignment((TextAnchor) 0);
    guiStyle5.set_fontSize(16);
    GUIStyle guiStyle6 = guiStyle5;
    GUI.Label(rect3, str5, guiStyle6);
  }

  private bool HaveActiveCellsChanged()
  {
    if (this.cullArea.NumberOfSubdivisions == 0)
      return false;
    this.previousActiveCells = new List<byte>((IEnumerable<byte>) this.activeCells);
    this.activeCells = this.cullArea.GetActiveCells(((Component) this).get_transform().get_position());
    while (this.activeCells.Count <= this.cullArea.NumberOfSubdivisions)
      this.activeCells.Add(this.cullArea.FIRST_GROUP_ID);
    return this.activeCells.Count != this.previousActiveCells.Count || (int) this.activeCells[this.cullArea.NumberOfSubdivisions] != (int) this.previousActiveCells[this.cullArea.NumberOfSubdivisions];
  }

  private void UpdateInterestGroups()
  {
    List<byte> byteList = new List<byte>(0);
    foreach (byte previousActiveCell in this.previousActiveCells)
    {
      if (!this.activeCells.Contains(previousActiveCell))
        byteList.Add(previousActiveCell);
    }
    PhotonNetwork.SetInterestGroups(byteList.ToArray(), this.activeCells.ToArray());
  }

  public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
  {
    while (this.activeCells.Count <= this.cullArea.NumberOfSubdivisions)
      this.activeCells.Add(this.cullArea.FIRST_GROUP_ID);
    if (this.cullArea.NumberOfSubdivisions == 1)
    {
      this.orderIndex = ++this.orderIndex % this.cullArea.SUBDIVISION_FIRST_LEVEL_ORDER.Length;
      this.pView.group = this.activeCells[this.cullArea.SUBDIVISION_FIRST_LEVEL_ORDER[this.orderIndex]];
    }
    else if (this.cullArea.NumberOfSubdivisions == 2)
    {
      this.orderIndex = ++this.orderIndex % this.cullArea.SUBDIVISION_SECOND_LEVEL_ORDER.Length;
      this.pView.group = this.activeCells[this.cullArea.SUBDIVISION_SECOND_LEVEL_ORDER[this.orderIndex]];
    }
    else
    {
      if (this.cullArea.NumberOfSubdivisions != 3)
        return;
      this.orderIndex = ++this.orderIndex % this.cullArea.SUBDIVISION_THIRD_LEVEL_ORDER.Length;
      this.pView.group = this.activeCells[this.cullArea.SUBDIVISION_THIRD_LEVEL_ORDER[this.orderIndex]];
    }
  }
}
