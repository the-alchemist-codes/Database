﻿// Decompiled with JetBrains decompiler
// Type: com.adjust.sdk.JSONBinaryTag
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace com.adjust.sdk
{
  public enum JSONBinaryTag
  {
    Array = 1,
    Class = 2,
    Value = 3,
    IntValue = 4,
    DoubleValue = 5,
    BoolValue = 6,
    FloatValue = 7,
  }
}
