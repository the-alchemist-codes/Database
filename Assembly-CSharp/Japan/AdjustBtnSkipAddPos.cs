﻿// Decompiled with JetBrains decompiler
// Type: AdjustBtnSkipAddPos
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

public class AdjustBtnSkipAddPos : MonoBehaviour
{
  public AdjustBtnSkipAddPos()
  {
    base.\u002Ector();
  }

  private void Awake()
  {
    RectTransform component = (RectTransform) ((Component) this).GetComponent<RectTransform>();
    Vector2 anchoredPosition = component.get_anchoredPosition();
    int x = (int) SetCanvasBounds.GetAddFrame().x;
    ref Vector2 local = ref anchoredPosition;
    local.x = (__Null) (local.x - (double) x);
    component.set_anchoredPosition(anchoredPosition);
  }
}
