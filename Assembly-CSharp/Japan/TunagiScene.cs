﻿// Decompiled with JetBrains decompiler
// Type: TunagiScene
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;
using UnityEngine;

public class TunagiScene : MonoBehaviour
{
  private static List<TunagiScene> mScenes = new List<TunagiScene>();
  public static bool AutoDeactivateScene;
  public static TunagiScene.LoadCompleteEvent OnLoadComplete;
  public float ZMin;
  public float ZMax;

  public TunagiScene()
  {
    base.\u002Ector();
  }

  public static int SceneCount
  {
    get
    {
      return TunagiScene.mScenes.Count;
    }
  }

  public static void SetScenesActive(bool active)
  {
    for (int index = 0; index < TunagiScene.mScenes.Count; ++index)
      ((Component) TunagiScene.mScenes[index]).get_gameObject().SetActive(active);
  }

  private void Awake()
  {
    TunagiScene.mScenes.Add(this);
    if (TunagiScene.OnLoadComplete != null)
      TunagiScene.OnLoadComplete(this);
    if (!TunagiScene.AutoDeactivateScene)
      return;
    ((Component) this).get_gameObject().SetActive(false);
    TunagiScene.AutoDeactivateScene = false;
  }

  private void OnDestroy()
  {
    TunagiScene.mScenes.Remove(this);
  }

  public static void PopFirstScene()
  {
    if (TunagiScene.mScenes.Count <= 0)
      return;
    Object.DestroyImmediate((Object) ((Component) TunagiScene.mScenes[0]).get_gameObject());
  }

  public static void DestroyAllScenes()
  {
    while (TunagiScene.mScenes.Count > 0)
      Object.DestroyImmediate((Object) ((Component) TunagiScene.mScenes[TunagiScene.mScenes.Count - 1]).get_gameObject());
  }

  public static TunagiScene LastScene
  {
    get
    {
      return TunagiScene.mScenes.Count > 0 ? TunagiScene.mScenes[TunagiScene.mScenes.Count - 1] : (TunagiScene) null;
    }
  }

  public static TunagiScene FirstScene
  {
    get
    {
      return TunagiScene.mScenes.Count > 0 ? TunagiScene.mScenes[0] : (TunagiScene) null;
    }
  }

  public delegate void LoadCompleteEvent(TunagiScene scene);
}
