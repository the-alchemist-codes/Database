﻿// Decompiled with JetBrains decompiler
// Type: AssetBundleUnloader
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;
using UnityEngine;

public class AssetBundleUnloader : MonoBehaviour
{
  private static AssetBundleUnloader Inastance;
  private List<AsyncOperation> mAsyncOperationList;
  private bool mIsReservedUnload;
  private bool mIsForceUnload;
  private int mRestPassCount;
  private readonly int PASS_COUNT;
  private bool mIsForceUnloadNow;

  public AssetBundleUnloader()
  {
    base.\u002Ector();
  }

  public static bool IsForceUnloadNow
  {
    get
    {
      return AssetBundleUnloader.Inastance.mIsForceUnloadNow;
    }
  }

  private void Awake()
  {
    AssetBundleUnloader.Inastance = this;
    this.mRestPassCount = this.PASS_COUNT;
  }

  private void LateUpdate()
  {
    this.mRestPassCount = !this.CanUnload() ? this.PASS_COUNT : this.mRestPassCount - 1;
    if (this.mRestPassCount > 0)
      return;
    this.ExecUnload();
    this.mRestPassCount = this.PASS_COUNT;
  }

  public static void ResetPassCount()
  {
    AssetBundleUnloader.Inastance.mRestPassCount = AssetBundleUnloader.Inastance.PASS_COUNT;
  }

  public static void AddAsyncOperation(AsyncOperation ao)
  {
    AssetBundleUnloader.Inastance.mAsyncOperationList.Add(ao);
  }

  public static void ReserveUnload(bool is_force)
  {
    if (AssetBundleUnloader.Inastance.mIsForceUnloadNow)
      return;
    AssetBundleUnloader.Inastance.mIsReservedUnload = true;
    if (!AssetBundleUnloader.Inastance.mIsForceUnload)
      return;
    AssetBundleUnloader.Inastance.mIsForceUnload = is_force;
  }

  public static void ReserveUnloadForce()
  {
    AssetBundleUnloader.Inastance.mIsForceUnloadNow = true;
    AssetBundleUnloader.Inastance.mIsReservedUnload = true;
    AssetBundleUnloader.Inastance.mIsForceUnload = true;
  }

  private bool CanUnload()
  {
    if (!this.mIsReservedUnload)
      return false;
    for (int index = 0; index < this.mAsyncOperationList.Count; ++index)
    {
      if (this.mAsyncOperationList[index] != null && !this.mAsyncOperationList[index].get_isDone())
        return false;
    }
    return !AssetManager.IsLoading;
  }

  private void ExecUnload()
  {
    AssetManager.Instance.UnloadUnusedAssetBundles(true, this.mIsForceUnload);
    this.mAsyncOperationList.Clear();
    this.mIsForceUnload = true;
    this.mIsReservedUnload = false;
    AssetBundleUnloader.Inastance.mIsForceUnloadNow = false;
  }
}
