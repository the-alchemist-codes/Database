﻿// Decompiled with JetBrains decompiler
// Type: EventBackLogContent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Text.RegularExpressions;
using UnityEngine;

public class EventBackLogContent : MonoBehaviour
{
  private static Regex pattern = new Regex("<.*?>", RegexOptions.Singleline);
  [SerializeField]
  private UnityEngine.UI.Text m_LogName;
  [SerializeField]
  private UnityEngine.UI.Text m_LogText;

  public EventBackLogContent()
  {
    base.\u002Ector();
  }

  public void SetBackLogText(string name, string text)
  {
    this.m_LogName.set_text(this.ReplaceTag(name));
    this.m_LogText.set_text(this.ReplaceTag(text));
  }

  private string ReplaceTag(string text)
  {
    if (string.IsNullOrEmpty(text))
      return string.Empty;
    MatchCollection matchCollection = EventBackLogContent.pattern.Matches(text);
    if (matchCollection != null)
    {
      foreach (Match match in matchCollection)
        text = text.Replace(match.Value, string.Empty);
    }
    return text;
  }
}
