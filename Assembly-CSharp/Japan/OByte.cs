﻿// Decompiled with JetBrains decompiler
// Type: OByte
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using CodeStage.AntiCheat.ObscuredTypes;
using MessagePack;

[MessagePackObject(true)]
public struct OByte
{
  private ObscuredByte value;

  public OByte(byte value)
  {
    this.value = (ObscuredByte) value;
  }

  public static implicit operator OByte(byte value)
  {
    return new OByte(value);
  }

  public static implicit operator byte(OByte value)
  {
    return (byte) value.value;
  }

  public static OByte operator ++(OByte value)
  {
    ref OByte local = ref value;
    local.value = (ObscuredByte) (byte) ((uint) (byte) local.value + 1U);
    return value;
  }

  public static OByte operator --(OByte value)
  {
    ref OByte local = ref value;
    local.value = (ObscuredByte) (byte) ((uint) (byte) local.value - 1U);
    return value;
  }

  public override string ToString()
  {
    return this.value.ToString();
  }
}
