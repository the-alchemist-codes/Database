﻿// Decompiled with JetBrains decompiler
// Type: DestroyEventListener
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

public class DestroyEventListener : MonoBehaviour
{
  public DestroyEventListener.DestroyEvent Listeners;

  public DestroyEventListener()
  {
    base.\u002Ector();
  }

  private void OnApplicationQuit()
  {
    this.Listeners = (DestroyEventListener.DestroyEvent) (_param0 => {});
  }

  private void OnDestroy()
  {
    if (this.Listeners == null)
      return;
    this.Listeners(((Component) this).get_gameObject());
  }

  public delegate void DestroyEvent(GameObject go);
}
