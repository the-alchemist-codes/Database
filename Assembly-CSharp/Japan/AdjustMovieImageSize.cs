﻿// Decompiled with JetBrains decompiler
// Type: AdjustMovieImageSize
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.EventSystems;

public class AdjustMovieImageSize : UIBehaviour
{
  private RectTransform m_ParentTransform;
  private RectTransform m_Transform;
  private Coroutine m_AdjustCoroutine;

  public AdjustMovieImageSize()
  {
    base.\u002Ector();
  }

  protected virtual void Awake()
  {
    base.Awake();
    this.m_Transform = ((Component) this).get_transform() as RectTransform;
    this.m_ParentTransform = ((Component) this).get_transform().get_parent() as RectTransform;
  }

  protected virtual void Start()
  {
    base.Start();
    this.AsyncAdjustRecttransformSize();
  }

  protected virtual void OnRectTransformDimensionsChange()
  {
    this.AsyncAdjustRecttransformSize();
  }

  private void AsyncAdjustRecttransformSize()
  {
    if (!((Component) this).get_gameObject().get_activeInHierarchy())
      return;
    if (this.m_AdjustCoroutine != null)
      ((MonoBehaviour) this).StopCoroutine(this.m_AdjustCoroutine);
    this.m_AdjustCoroutine = ((MonoBehaviour) this).StartCoroutine(this.StartAdjustSize());
  }

  [DebuggerHidden]
  private IEnumerator StartAdjustSize()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new AdjustMovieImageSize.\u003CStartAdjustSize\u003Ec__Iterator0()
    {
      \u0024this = this
    };
  }

  private void InternalAdjustRecttransformSize(float canvasBoundsScale)
  {
    Rect rect = this.m_ParentTransform.get_rect();
    this.m_Transform.SetSizeWithCurrentAnchors((RectTransform.Axis) 1, (float) ((Rect) ref rect).get_size().y);
    float num = 1f / canvasBoundsScale;
    Vector2 vector2 = Vector2.op_Implicit(((Transform) this.m_Transform).get_localScale());
    vector2.x = (__Null) (double) num;
    vector2.y = (__Null) (double) num;
    ((Transform) this.m_Transform).set_localScale(Vector2.op_Implicit(vector2));
  }
}
