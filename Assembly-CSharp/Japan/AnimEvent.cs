﻿// Decompiled with JetBrains decompiler
// Type: AnimEvent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

public class AnimEvent : ScriptableObject
{
  public float Start;
  public float End;
  protected bool mIsEnd;

  public AnimEvent()
  {
    base.\u002Ector();
  }

  public virtual void OnStart(GameObject go)
  {
    this.mIsEnd = false;
  }

  public virtual void OnTick(GameObject go, float ratio)
  {
  }

  public virtual void OnEnd(GameObject go)
  {
    this.mIsEnd = true;
  }

  public bool IsEnd
  {
    get
    {
      return this.mIsEnd;
    }
  }
}
