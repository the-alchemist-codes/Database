﻿// Decompiled with JetBrains decompiler
// Type: JSON_QuestLobbyNewsParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

[MessagePackObject(true)]
[Serializable]
public class JSON_QuestLobbyNewsParam
{
  public int category;
  public string begin_at;
  public string end_at;
  public int show_type;
}
