﻿// Decompiled with JetBrains decompiler
// Type: RichBitmapText
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

public class RichBitmapText : BitmapText
{
  public Color32 TopColor = new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);
  public Color32 BottomColor = new Color32((byte) 0, (byte) 0, (byte) 0, byte.MaxValue);
  [Range(-1f, 1f)]
  public float Shear;

  private static Color32 Multiply(Color32 x, Color32 y)
  {
    x.r = (__Null) (int) (byte) (x.r * y.r / (int) byte.MaxValue);
    x.g = (__Null) (int) (byte) (x.g * y.g / (int) byte.MaxValue);
    x.b = (__Null) (int) (byte) (x.b * y.b / (int) byte.MaxValue);
    x.a = (__Null) (int) (byte) (x.a * y.a / (int) byte.MaxValue);
    return x;
  }

  protected virtual void OnPopulateMesh(VertexHelper toFill)
  {
    toFill.Clear();
    base.OnPopulateMesh(toFill);
    UIVertex simpleVert = (UIVertex) UIVertex.simpleVert;
    int currentVertCount = toFill.get_currentVertCount();
    int num1;
    for (int index = 0; index < currentVertCount; index = num1 + 1)
    {
      toFill.PopulateUIVertex(ref simpleVert, index);
      simpleVert.color = (__Null) RichBitmapText.Multiply((Color32) simpleVert.color, this.TopColor);
      toFill.SetUIVertex(simpleVert, index);
      int num2 = index + 1;
      toFill.PopulateUIVertex(ref simpleVert, num2);
      simpleVert.color = (__Null) RichBitmapText.Multiply((Color32) simpleVert.color, this.TopColor);
      toFill.SetUIVertex(simpleVert, num2);
      int num3 = num2 + 1;
      toFill.PopulateUIVertex(ref simpleVert, num3);
      simpleVert.color = (__Null) RichBitmapText.Multiply((Color32) simpleVert.color, this.BottomColor);
      toFill.SetUIVertex(simpleVert, num3);
      num1 = num3 + 1;
      toFill.PopulateUIVertex(ref simpleVert, num1);
      simpleVert.color = (__Null) RichBitmapText.Multiply((Color32) simpleVert.color, this.BottomColor);
      toFill.SetUIVertex(simpleVert, num1);
    }
    if ((double) this.Shear == 0.0)
      return;
    float num4 = this.Shear * (float) this.get_fontSize();
    int num5;
    for (int index = 0; index < currentVertCount; index = num5 + 1)
    {
      toFill.PopulateUIVertex(ref simpleVert, index);
      ref __Null local1 = ref simpleVert.position;
      // ISSUE: cast to a reference type
      // ISSUE: explicit reference operation
      // ISSUE: cast to a reference type
      // ISSUE: explicit reference operation
      (^(Vector3&) ref local1).x = (__Null) ((^(Vector3&) ref local1).x + (double) num4);
      toFill.SetUIVertex(simpleVert, index);
      int num2 = index + 1;
      toFill.PopulateUIVertex(ref simpleVert, num2);
      ref __Null local2 = ref simpleVert.position;
      // ISSUE: cast to a reference type
      // ISSUE: explicit reference operation
      // ISSUE: cast to a reference type
      // ISSUE: explicit reference operation
      (^(Vector3&) ref local2).x = (__Null) ((^(Vector3&) ref local2).x + (double) num4);
      toFill.SetUIVertex(simpleVert, num2);
      int num3 = num2 + 1;
      toFill.PopulateUIVertex(ref simpleVert, num3);
      ref __Null local3 = ref simpleVert.position;
      // ISSUE: cast to a reference type
      // ISSUE: explicit reference operation
      // ISSUE: cast to a reference type
      // ISSUE: explicit reference operation
      (^(Vector3&) ref local3).x = (__Null) ((^(Vector3&) ref local3).x - (double) num4);
      toFill.SetUIVertex(simpleVert, num3);
      num5 = num3 + 1;
      toFill.PopulateUIVertex(ref simpleVert, num5);
      ref __Null local4 = ref simpleVert.position;
      // ISSUE: cast to a reference type
      // ISSUE: explicit reference operation
      // ISSUE: cast to a reference type
      // ISSUE: explicit reference operation
      (^(Vector3&) ref local4).x = (__Null) ((^(Vector3&) ref local4).x - (double) num4);
      toFill.SetUIVertex(simpleVert, num5);
    }
  }
}
