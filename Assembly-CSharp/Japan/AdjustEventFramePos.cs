﻿// Decompiled with JetBrains decompiler
// Type: AdjustEventFramePos
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

public class AdjustEventFramePos : MonoBehaviour
{
  public AdjustEventFramePos()
  {
    base.\u002Ector();
  }

  private void Awake()
  {
    RectTransform component = (RectTransform) ((Component) this).GetComponent<RectTransform>();
    Rect safeArea = SetCanvasBounds.GetSafeArea(false);
    float num1 = ((Rect) ref safeArea).get_width() / (float) Screen.get_width();
    int num2 = 0;
    if ((double) num1 < 1.0)
      num2 = (int) ((Rect) ref safeArea).get_x();
    int x = (int) SetCanvasBounds.GetAddFrame().x;
    int num3 = num2 + x;
    component.set_sizeDelta(new Vector2((float) num3, (float) Screen.get_height()));
  }
}
