﻿// Decompiled with JetBrains decompiler
// Type: RaiseEventOptions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

public class RaiseEventOptions
{
  public static readonly RaiseEventOptions Default = new RaiseEventOptions();
  public EventCaching CachingOption;
  public byte InterestGroup;
  public int[] TargetActors;
  public ReceiverGroup Receivers;
  public byte SequenceChannel;
  public bool ForwardToWebhook;
  public bool Encrypt;

  public void Reset()
  {
    this.CachingOption = RaiseEventOptions.Default.CachingOption;
    this.InterestGroup = RaiseEventOptions.Default.InterestGroup;
    this.TargetActors = RaiseEventOptions.Default.TargetActors;
    this.Receivers = RaiseEventOptions.Default.Receivers;
    this.SequenceChannel = RaiseEventOptions.Default.SequenceChannel;
    this.ForwardToWebhook = RaiseEventOptions.Default.ForwardToWebhook;
    this.Encrypt = RaiseEventOptions.Default.Encrypt;
  }
}
