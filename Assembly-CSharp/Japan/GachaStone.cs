﻿// Decompiled with JetBrains decompiler
// Type: GachaStone
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

public class GachaStone : MonoBehaviour
{
  public Camera TargetCamera;

  public GachaStone()
  {
    base.\u002Ector();
  }

  public string DROP_ID { get; set; }

  private void Start()
  {
    if (!Object.op_Equality((Object) this.TargetCamera, (Object) null))
      return;
    this.TargetCamera = Camera.get_main();
  }

  private void Update()
  {
    ((Component) this).get_transform().LookAt(((Component) this.TargetCamera).get_transform());
  }
}
