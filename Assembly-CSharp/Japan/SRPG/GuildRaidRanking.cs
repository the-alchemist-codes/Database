﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildRaidRanking
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class GuildRaidRanking
  {
    public int Rank { get; private set; }

    public long Score { get; private set; }

    public GuildRaidRankingGuild Guild { get; private set; }

    public bool Deserialize(JSON_GuildRaidRanking json)
    {
      this.Rank = json.rank;
      this.Score = json.score;
      if (json.guild == null)
        return false;
      this.Guild = new GuildRaidRankingGuild();
      this.Guild.Deserialize(json.guild);
      return true;
    }
  }
}
