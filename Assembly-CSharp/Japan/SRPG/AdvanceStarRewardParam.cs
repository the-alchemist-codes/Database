﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AdvanceStarRewardParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class AdvanceStarRewardParam
  {
    public int NeedStarNum;
    public string RewardId;
    public bool IsReward;

    public void Deserialize(JSON_AdvanceStarRewardParam json)
    {
      if (json == null)
        return;
      this.NeedStarNum = json.need;
      this.RewardId = json.reward_id;
    }
  }
}
