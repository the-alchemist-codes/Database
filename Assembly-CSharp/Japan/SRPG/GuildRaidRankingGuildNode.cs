﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildRaidRankingGuildNode
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class GuildRaidRankingGuildNode : ContentNode
  {
    [SerializeField]
    public GameObject bindObject;
    [SerializeField]
    public GameObject EmptyObject;

    public void Setup(GuildRaidRanking guildRaidRanking)
    {
      if (Object.op_Equality((Object) this.bindObject, (Object) null) || guildRaidRanking == null)
        return;
      DataSource.Bind<GuildRaidRanking>(this.bindObject, guildRaidRanking, false);
      this.bindObject.get_gameObject().SetActive(true);
      GameParameter.UpdateAll(this.bindObject.get_gameObject());
    }

    public void Empty(bool is_enmpty)
    {
      if (Object.op_Equality((Object) this.EmptyObject, (Object) null) || Object.op_Equality((Object) this.bindObject, (Object) null))
        return;
      this.bindObject.get_gameObject().SetActive(!is_enmpty);
      this.EmptyObject.SetActive(is_enmpty);
    }
  }
}
