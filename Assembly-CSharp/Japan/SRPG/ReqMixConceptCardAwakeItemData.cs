﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqMixConceptCardAwakeItemData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;
using System.Text;

namespace SRPG
{
  public class ReqMixConceptCardAwakeItemData : WebAPI
  {
    public ReqMixConceptCardAwakeItemData(
      long base_id,
      Dictionary<string, int> materials,
      Network.ResponseCallback response,
      string trophyProgs = null,
      string bingoProgs = null)
    {
      this.name = "unit/concept/plus";
      StringBuilder stringBuilder = WebAPI.GetStringBuilder();
      stringBuilder.Append("\"base_id\":");
      stringBuilder.Append(base_id);
      stringBuilder.Append(",");
      stringBuilder.Append("\"mats\":[");
      int num = 0;
      foreach (KeyValuePair<string, int> material in materials)
      {
        stringBuilder.Append("{");
        stringBuilder.Append("\"iname\":");
        stringBuilder.Append("\"");
        stringBuilder.Append(material.Key);
        stringBuilder.Append("\"");
        stringBuilder.Append(",");
        stringBuilder.Append("\"num\":");
        stringBuilder.Append(material.Value);
        if (num < materials.Count - 1)
          stringBuilder.Append("},");
        else
          stringBuilder.Append("}");
        ++num;
      }
      stringBuilder.Append("]");
      if (!string.IsNullOrEmpty(trophyProgs))
      {
        stringBuilder.Append(",");
        stringBuilder.Append(trophyProgs);
      }
      if (!string.IsNullOrEmpty(bingoProgs))
      {
        stringBuilder.Append(",");
        stringBuilder.Append(bingoProgs);
      }
      this.body = WebAPI.GetRequestString(stringBuilder.ToString());
      this.callback = response;
    }
  }
}
