﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ConditionsResult_TobiraNoConditions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class ConditionsResult_TobiraNoConditions : ConditionsResult
  {
    public ConditionsResult_TobiraNoConditions()
    {
      this.mIsClear = true;
    }

    public override string text
    {
      get
      {
        return LocalizedText.Get("sys.TOBIRA_CONDITIONS_NOTHING");
      }
    }

    public override string errorText
    {
      get
      {
        return LocalizedText.Get("sys.ITEM_NOT_ENOUGH");
      }
    }
  }
}
