﻿// Decompiled with JetBrains decompiler
// Type: SRPG.UnitTobiraParamLevel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class UnitTobiraParamLevel : MonoBehaviour
  {
    [SerializeField]
    private GameObject OnGO;
    [SerializeField]
    private GameObject OffGO;
    [SerializeField]
    private int OwnLevel;

    public UnitTobiraParamLevel()
    {
      base.\u002Ector();
    }

    public void Refresh(int targetLevel)
    {
      if (Object.op_Equality((Object) this.OnGO, (Object) null) || Object.op_Equality((Object) this.OffGO, (Object) null))
        return;
      bool flag = targetLevel >= this.OwnLevel;
      this.OnGO.SetActive(flag);
      this.OffGO.SetActive(!flag);
    }
  }
}
