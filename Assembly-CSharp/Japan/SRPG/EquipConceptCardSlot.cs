﻿// Decompiled with JetBrains decompiler
// Type: SRPG.EquipConceptCardSlot
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class EquipConceptCardSlot : GenericSlot
  {
    [SerializeField]
    private SRPG_Button Lock;
    private int m_SlotIndex;
    private ConceptCardData m_ConceptCardData;

    public int slotIndex
    {
      get
      {
        return this.m_SlotIndex;
      }
      set
      {
        this.m_SlotIndex = value;
      }
    }

    public ConceptCardData conceptCardData
    {
      get
      {
        return this.m_ConceptCardData;
      }
      set
      {
        this.m_ConceptCardData = value;
      }
    }

    private void Awake()
    {
      if (Object.op_Inequality((Object) this.SelectButton, (Object) null))
        this.SelectButton.AddListener(new SRPG_Button.ButtonClickEvent(this.OnButtonClick));
      if (!Object.op_Inequality((Object) this.Lock, (Object) null))
        return;
      this.Lock.AddListener(new SRPG_Button.ButtonClickEvent(this.OnLockClick));
    }

    private void OnButtonClick(SRPG_Button button)
    {
      if (this.OnSelect == null || !((Selectable) button).get_interactable())
        return;
      this.OnSelect((GenericSlot) this, ((Selectable) button).get_interactable());
    }

    private void OnLockClick(SRPG_Button button)
    {
      if (this.OnSelect == null)
        return;
      this.OnSelect((GenericSlot) this, false);
    }
  }
}
