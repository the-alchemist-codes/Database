﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RuneParamEnhEvoEffectWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  [FlowNode.Pin(100, "ゲージアニメ開始", FlowNode.PinTypes.Input, 100)]
  [FlowNode.Pin(900, "閉じるボタン押下", FlowNode.PinTypes.Input, 900)]
  [FlowNode.Pin(1000, "自身を閉じる", FlowNode.PinTypes.Output, 1000)]
  public class RuneParamEnhEvoEffectWindow : MonoBehaviour, IFlowInterface
  {
    private const int INPUT_START_GAUGE_ANIM = 100;
    private const int INPUT_CLOSE = 900;
    private const int OUTPUT_CLOSE_WINDOW = 1000;
    [SerializeField]
    private RuneIcon mRuneIcon;
    [SerializeField]
    private RuneDrawEnhanceEffect mRuneDrawEnhanceEffect;
    [SerializeField]
    private RuneDrawEvoStateOneSetting mRuneDrawEvoStateOneSetting;
    private BindRuneData mRuneDataBefore;
    private BindRuneData mRuneDataCurr;
    private RuneManager mRuneManager;
    private bool mIsEnhanceSuccess;
    private int mEvoSlot;

    public RuneParamEnhEvoEffectWindow()
    {
      base.\u002Ector();
    }

    public void Awake()
    {
    }

    private void OnDestroy()
    {
    }

    public void Activated(int pinID)
    {
      switch (pinID)
      {
        case 100:
          if (!Object.op_Implicit((Object) this.mRuneDrawEvoStateOneSetting))
            break;
          this.mRuneDrawEvoStateOneSetting.StartGaugeAnim();
          break;
        case 900:
          if (Object.op_Inequality((Object) this.mRuneManager, (Object) null) && this.mRuneDataCurr != null && this.mRuneDataCurr.Rune != null)
            this.mRuneManager.SelectedResetStatus(this.mRuneDataCurr, true, this.mRuneDataCurr.Rune.GetEvoIndex(this.mEvoSlot));
          FlowNode_GameObject.ActivateOutputLinks((Component) this, 1000);
          break;
      }
    }

    public void Setup(
      RuneManager manager,
      BindRuneData before_rune_data,
      BindRuneData after_rune_data)
    {
      this.mRuneManager = manager;
      this.mRuneDataBefore = before_rune_data;
      this.mRuneDataCurr = after_rune_data;
      this.mIsEnhanceSuccess = FlowNode_ReqRuneParamEnhEvo.IsResultSuccess;
      this.mEvoSlot = FlowNode_ReqRuneParamEnhEvo.SelectedEvoSlot;
      if (Object.op_Implicit((Object) this.mRuneIcon))
        this.mRuneIcon.Setup(this.mRuneDataCurr, false);
      if (Object.op_Implicit((Object) this.mRuneDrawEnhanceEffect))
        this.mRuneDrawEnhanceEffect.SetDrawParam(this.mIsEnhanceSuccess);
      this.SetGaugeStatus();
      this.Refresh();
    }

    public void Refresh()
    {
      if (Object.op_Implicit((Object) this.mRuneIcon))
        this.mRuneIcon.Refresh();
      if (Object.op_Implicit((Object) this.mRuneDrawEnhanceEffect))
        this.mRuneDrawEnhanceEffect.Refresh();
      if (!Object.op_Implicit((Object) this.mRuneDrawEvoStateOneSetting))
        return;
      this.mRuneDrawEvoStateOneSetting.Refresh();
    }

    private void SetGaugeStatus()
    {
      if (this.mRuneDataCurr == null || Object.op_Equality((Object) this.mRuneDrawEvoStateOneSetting, (Object) null))
        return;
      RuneData rune1 = this.mRuneDataCurr.Rune;
      if (rune1 == null)
        return;
      RuneData rune2 = this.mRuneDataBefore.Rune;
      if (rune2 == null)
        return;
      int index = this.mEvoSlot - 1;
      if (0 > index || index >= rune1.state.evo_state.Count)
        return;
      BaseStatus addStatus = (BaseStatus) null;
      BaseStatus scaleStatus = (BaseStatus) null;
      if (index < rune1.GetLengthFromEvoParam())
      {
        rune1.CreateBaseStatusFromEvoParam(index, ref addStatus, ref scaleStatus, true);
        if (addStatus == null)
          addStatus = new BaseStatus();
        if (scaleStatus == null)
          scaleStatus = new BaseStatus();
        float percentage = rune1.PowerPercentageFromEvoParam(index);
        float start_percentage = rune2.PowerPercentageFromEvoParam(index);
        this.mRuneDrawEvoStateOneSetting.SetStatus(addStatus, scaleStatus, percentage, true, start_percentage);
      }
      else
        this.mRuneDrawEvoStateOneSetting.SetStatus((BaseStatus) null, (BaseStatus) null, 0.0f, false, 0.0f);
    }
  }
}
