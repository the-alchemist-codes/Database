﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_GuildFacilityEffectParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_GuildFacilityEffectParam
  {
    public int lv;
    public string[] buff;
    public int mem_cnt;
    public int sub_mas;
    public int shop_cnt;
  }
}
