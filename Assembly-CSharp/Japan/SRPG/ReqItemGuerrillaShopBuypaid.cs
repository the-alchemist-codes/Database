﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqItemGuerrillaShopBuypaid
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class ReqItemGuerrillaShopBuypaid : WebAPI
  {
    public ReqItemGuerrillaShopBuypaid(int id, int buynum, Network.ResponseCallback response)
    {
      this.name = "shop/guerrilla/buy";
      ReqItemGuerrillaShopBuypaid guerrillaShopBuypaid1 = this;
      guerrillaShopBuypaid1.body = guerrillaShopBuypaid1.body + "\"id\":" + id.ToString() + ",";
      ReqItemGuerrillaShopBuypaid guerrillaShopBuypaid2 = this;
      guerrillaShopBuypaid2.body = guerrillaShopBuypaid2.body + "\"buynum\":" + buynum.ToString();
      this.body = WebAPI.GetRequestString(this.body);
      this.callback = response;
    }
  }
}
