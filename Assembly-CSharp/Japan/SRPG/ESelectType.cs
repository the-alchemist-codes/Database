﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ESelectType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public enum ESelectType : byte
  {
    Cross,
    Diamond,
    Square,
    Laser,
    All,
    Wall,
    WallPlus,
    Bishop,
    Victory,
    LaserSpread,
    LaserWide,
    Horse,
    LaserTwin,
    LaserTriple,
    SquareOutline,
  }
}
