﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqGvG
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using Gsc.Network.Encoding;
using MessagePack;
using System;

namespace SRPG
{
  public class ReqGvG : WebAPI
  {
    public ReqGvG(
      int gid,
      int gvg_group_id,
      SRPG.Network.ResponseCallback response,
      EncodingTypes.ESerializeCompressMethod serializeCompressMethod)
    {
      this.name = "gvg";
      this.body = WebAPI.GetRequestString<ReqGvG.RequestParam>(new ReqGvG.RequestParam()
      {
        gid = gid,
        gvg_group_id = gvg_group_id
      });
      this.callback = response;
      this.serializeCompressMethod = serializeCompressMethod;
    }

    [Serializable]
    public class RequestParam
    {
      public int gid;
      public int gvg_group_id;
    }

    [MessagePackObject(true)]
    [Serializable]
    public class Response
    {
      public JSON_GvGNodeData[] nodes;
      public int[] matching_order;
      public JSON_ViewGuild[] guilds;
      public JSON_ViewGuild my_guild;
      public long[] used_units;
      public int declare_num;
      public int refresh_wait_sec;
      public int auto_refresh_wait_sec;
      public JSON_GvGResult result_daily;
      public JSON_GvGResult result;
    }
  }
}
