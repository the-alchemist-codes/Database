﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ChallengeCountResetCompleteWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace SRPG
{
  public class ChallengeCountResetCompleteWindow : MonoBehaviour
  {
    [SerializeField]
    private Text ChallengeCountResetNum;
    [SerializeField]
    private Text ChallengeCountResetMax;
    [SerializeField]
    private Button DecideButton;
    public ChallengeCountResetCompleteWindow.DecideButtonEvent DecideEvent;

    public ChallengeCountResetCompleteWindow()
    {
      base.\u002Ector();
    }

    private void Awake()
    {
      if (!Object.op_Inequality((Object) this.DecideButton, (Object) null))
        return;
      // ISSUE: method pointer
      ((UnityEvent) this.DecideButton.get_onClick()).AddListener(new UnityAction((object) this, __methodptr(OnClickEvent)));
    }

    private void OnClickEvent()
    {
      if (this.DecideEvent == null)
        return;
      this.DecideEvent(((Component) this).get_gameObject());
    }

    public void Setup(
      int reset_num,
      int reset_max,
      ChallengeCountResetCompleteWindow.DecideButtonEvent okEvnet)
    {
      if (Object.op_Inequality((Object) this.ChallengeCountResetNum, (Object) null))
        this.ChallengeCountResetNum.set_text(reset_num.ToString());
      if (Object.op_Inequality((Object) this.ChallengeCountResetMax, (Object) null))
        this.ChallengeCountResetMax.set_text(reset_max.ToString());
      this.DecideEvent = okEvnet;
    }

    public delegate void DecideButtonEvent(GameObject dialog);
  }
}
