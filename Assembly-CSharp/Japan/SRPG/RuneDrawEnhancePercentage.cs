﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RuneDrawEnhancePercentage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class RuneDrawEnhancePercentage : MonoBehaviour
  {
    [SerializeField]
    private Text mGaugeText;
    [SerializeField]
    private Image mGauge;
    [SerializeField]
    private ImageArray mGaugeButtonImage;
    [SerializeField]
    private Text mPercentage;
    [SerializeField]
    private float mAnimTime;
    [SerializeField]
    private GameObject mGaugeStartSE;
    [SerializeField]
    private GameObject mGaugeFullSE;
    private BindRuneData mRuneData;
    private bool mIsUseEnhanceGauge;
    private bool mIsGaugeAnim;
    private float mStartGauge;
    private const int mCanUseGaugePercentage = 100;

    public RuneDrawEnhancePercentage()
    {
      base.\u002Ector();
    }

    public void Awake()
    {
    }

    public void OnEnable()
    {
      this.StartGaugeAnim();
    }

    public void SetDrawParam(BindRuneData rune_data, bool is_use_enhance_gauge)
    {
      this.mRuneData = rune_data;
      this.mIsUseEnhanceGauge = is_use_enhance_gauge;
      this.Refresh();
    }

    public void SetupGaugeAnim(float _start_gauge)
    {
      if (this.mIsGaugeAnim)
        return;
      this.mStartGauge = _start_gauge;
      if ((double) this.mStartGauge >= (double) this.CalcPlayerGauge(this.mRuneData))
        return;
      this.mIsGaugeAnim = true;
      if (Object.op_Implicit((Object) this.mGaugeStartSE))
        this.mGaugeStartSE.SetActive(false);
      if (!Object.op_Implicit((Object) this.mGaugeFullSE))
        return;
      this.mGaugeFullSE.SetActive(false);
    }

    public void Refresh()
    {
      if (Object.op_Inequality((Object) this.mGaugeButtonImage, (Object) null))
        this.mGaugeButtonImage.ImageIndex = !this.mIsUseEnhanceGauge ? 0 : 1;
      int num1 = this.CalcSuccessPercentage(this.mRuneData);
      int num2 = this.CalcPlayerGauge(this.mRuneData);
      int num3 = !this.mIsUseEnhanceGauge ? num1 : Mathf.Min(100, num1 + num2);
      if (Object.op_Implicit((Object) this.mPercentage))
        this.mPercentage.set_text(num3.ToString());
      if (Object.op_Implicit((Object) this.mGauge))
        this.mGauge.set_fillAmount((float) num2 / 100f);
      if (!Object.op_Implicit((Object) this.mGaugeText))
        return;
      this.mGaugeText.set_text(num2.ToString());
    }

    public bool IsCanUseGauge(BindRuneData rune)
    {
      int num = this.CalcSuccessPercentage(rune);
      return 100 > num && 100 <= this.CalcPlayerGauge(rune) + num;
    }

    public int CalcPlayerGauge(BindRuneData rune)
    {
      int rarity = rune.Rarity;
      List<RuneEnforceGaugeData> runeEnforceGauge = MonoSingleton<GameManager>.Instance.Player.GetRuneEnforceGauge();
      for (int index = 0; index < runeEnforceGauge.Count; ++index)
      {
        if ((int) runeEnforceGauge[index].rare == rarity)
          return (int) runeEnforceGauge[index].val;
      }
      return 0;
    }

    private int CalcSuccessPercentage(BindRuneData bind_rune)
    {
      if (bind_rune == null)
        return 0;
      RuneData rune = bind_rune.Rune;
      if (rune == null)
        return 0;
      RuneMaterial runeMaterial = rune.RuneMaterial;
      if (runeMaterial == null)
        return 0;
      short[] enhanceProbability = runeMaterial.enhance_probability;
      int index = (int) rune.enforce;
      if (0 >= enhanceProbability.Length)
        return 0;
      if (index >= enhanceProbability.Length)
        index = enhanceProbability.Length - 1;
      return (int) enhanceProbability[index];
    }

    public void StartGaugeAnim()
    {
      if (!this.mIsGaugeAnim)
        return;
      this.StartCoroutine(this.GaugeAnim());
    }

    [DebuggerHidden]
    private IEnumerator GaugeAnim()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new RuneDrawEnhancePercentage.\u003CGaugeAnim\u003Ec__Iterator0()
      {
        \u0024this = this
      };
    }
  }
}
