﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RestorePoints
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public enum RestorePoints
  {
    Home,
    QuestList,
    MP,
    UnitKyouka,
    UnitUnlock,
    Arena,
    UnitCharacterQuest,
    ReplayQuestList,
    UnitKakera,
    Tower,
    EventQuestList,
    CharacterQuestList,
    Versus,
    UnitEvolution,
    MultiTower,
    Ordeal,
    UnlockTobira,
    EnhanceTobira,
    RankMatch,
    Shop,
    BeginnerTop,
    Raid,
    GenesisStage,
    GenesisBoss,
    AdvanceStage,
    AdvanceBoss,
    UnitRentalQuest,
    DrawCard,
    GuildRaid,
    JukeBox,
    GVG,
    UnitRune,
  }
}
