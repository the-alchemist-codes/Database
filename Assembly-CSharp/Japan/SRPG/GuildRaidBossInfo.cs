﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildRaidBossInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;

namespace SRPG
{
  public class GuildRaidBossInfo
  {
    public int BossId { get; private set; }

    public int CurrentHP { get; private set; }

    public int MaxHP { get; private set; }

    public bool Deserialize(JSON_GuildRaidBossInfo json)
    {
      if (json == null)
        return false;
      this.BossId = json.boss_id;
      this.CurrentHP = json.current_hp;
      GuildRaidBossParam guildRaidBossParam = MonoSingleton<GameManager>.Instance.GetGuildRaidBossParam(this.BossId);
      if (guildRaidBossParam == null)
        return false;
      int round = 1;
      if (Object.op_Inequality((Object) GuildRaidManager.Instance, (Object) null))
        round = GuildRaidManager.Instance.CurrentRound;
      this.MaxHP = GuildRaidBossParam.CalcMaxHP(guildRaidBossParam, round);
      return true;
    }
  }
}
