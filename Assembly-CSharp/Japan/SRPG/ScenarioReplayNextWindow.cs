﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ScenarioReplayNextWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;

namespace SRPG
{
  public class ScenarioReplayNextWindow : MonoBehaviour
  {
    public ScenarioReplayNextWindow()
    {
      base.\u002Ector();
    }

    private void Start()
    {
      if (string.IsNullOrEmpty((string) GlobalVars.ReplaySelectedNextQuestID))
        return;
      DataSource.Bind<QuestParam>(((Component) this).get_gameObject(), MonoSingleton<GameManager>.Instance.FindQuest((string) GlobalVars.ReplaySelectedNextQuestID), false);
      GameParameter.UpdateAll(((Component) this).get_gameObject());
    }
  }
}
