﻿// Decompiled with JetBrains decompiler
// Type: SRPG.MultiPlayTrickParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class MultiPlayTrickParam
  {
    public string tid;
    public bool val;
    public int cun;
    public int rnk;
    public int rcp;
    public int grx;
    public int gry;
    public int rac;
    public int ccl;
    public string tag;
  }
}
