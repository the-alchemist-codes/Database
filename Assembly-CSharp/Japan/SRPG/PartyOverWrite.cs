﻿// Decompiled with JetBrains decompiler
// Type: SRPG.PartyOverWrite
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class PartyOverWrite
  {
    private eOverWritePartyType mPatryType;
    private UnitOverWriteData[] mOverWriteDatas;

    public eOverWritePartyType PatryType
    {
      get
      {
        return this.mPatryType;
      }
    }

    public UnitOverWriteData[] Units
    {
      get
      {
        return this.mOverWriteDatas;
      }
    }

    public void Deserialize(JSON_PartyOverWrite json)
    {
      this.mPatryType = UnitOverWriteUtility.String2OverWritePartyType(json.ptype);
      this.mOverWriteDatas = (UnitOverWriteData[]) null;
      if (json == null || json.unit_ow == null)
        return;
      this.mOverWriteDatas = new UnitOverWriteData[json.unit_ow.Length];
      for (int index = 0; index < json.unit_ow.Length; ++index)
      {
        UnitOverWriteData unitOverWriteData = new UnitOverWriteData();
        unitOverWriteData.Deserialize(json.unit_ow[index]);
        this.mOverWriteDatas[index] = unitOverWriteData;
      }
    }
  }
}
