﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqCoinBuyUseBonusReward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  public class ReqCoinBuyUseBonusReward : WebAPI
  {
    public ReqCoinBuyUseBonusReward(
      string reward_iname,
      int count,
      Network.ResponseCallback response)
    {
      this.name = "coin_bonuse/reward";
      this.body = WebAPI.GetRequestString<ReqCoinBuyUseBonusReward.RequestParam>(new ReqCoinBuyUseBonusReward.RequestParam()
      {
        iname = reward_iname,
        num = count
      });
      this.callback = response;
    }

    [Serializable]
    public class RequestParam
    {
      public string iname;
      public int num;
    }

    [Serializable]
    public class Response
    {
      public JSON_PlayerCoinBuyUseBonusRewardState[] bonus_rewards;
    }
  }
}
