﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_SummonCoinConvertWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  [FlowNode.NodeType("UI/SummonCoinConvertWindow")]
  public class FlowNode_SummonCoinConvertWindow : FlowNode_GUI
  {
    [SerializeField]
    private GachaCoinChangeWindow.CoinType coinType;

    protected override void OnCreatePinActive()
    {
      base.OnCreatePinActive();
      if (!Object.op_Inequality((Object) this.Instance, (Object) null))
        return;
      GachaCoinChangeWindow componentInChildren = (GachaCoinChangeWindow) this.Instance.GetComponentInChildren<GachaCoinChangeWindow>(true);
      if (!Object.op_Inequality((Object) componentInChildren, (Object) null))
        return;
      componentInChildren.Refresh(this.coinType);
    }
  }
}
