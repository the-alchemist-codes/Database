﻿// Decompiled with JetBrains decompiler
// Type: SRPG.UnitEnhancePanel
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using UnityEngine;

namespace SRPG
{
  public class UnitEnhancePanel : MonoBehaviour
  {
    public UnitEquipmentSlotEvents[] EquipmentSlots;
    public SRPG_Button JobRankUpButton;
    public SRPG_Button JobUnlockButton;
    public SRPG_Button EquipButton;
    public SRPG_Button AllEquipButton;
    public GameObject JobRankCapCaution;
    public SRPG_Button JobRankupAllIn;
    [Space(10f)]
    public GenericSlot ArtifactSlot;
    [Space(10f)]
    public GenericSlot ArtifactSlot2;
    [Space(10f)]
    public GenericSlot ArtifactSlot3;
    [Space(10f)]
    public RectTransform ExpItemList;
    public ListItemEvents ExpItemTemplate;
    public SRPG_Button UnitLevelupButton;
    [Space(10f)]
    public UnitAbilityList AbilityList;
    [Space(10f)]
    public UnitAbilityList AbilitySlots;
    [Space(10f)]
    public UnitEnhancePanel.ConceptCardSlotObjects[] mConceptCardSlots;

    public UnitEnhancePanel()
    {
      base.\u002Ector();
    }

    private void Awake()
    {
      Canvas component = (Canvas) ((Component) this).GetComponent<Canvas>();
      if (!UnityEngine.Object.op_Inequality((UnityEngine.Object) component, (UnityEngine.Object) null))
        return;
      ((Behaviour) component).set_enabled(false);
    }

    private void Start()
    {
      if (!UnityEngine.Object.op_Inequality((UnityEngine.Object) this.ExpItemTemplate, (UnityEngine.Object) null))
        return;
      ((Component) this.ExpItemTemplate).get_gameObject().SetActive(false);
    }

    public void SetConceptCardOnSelect(GenericSlot.SelectEvent ev)
    {
      if (this.mConceptCardSlots == null)
        return;
      for (int index = 0; index < this.mConceptCardSlots.Length; ++index)
      {
        if (!UnityEngine.Object.op_Equality((UnityEngine.Object) this.mConceptCardSlots[index].m_ConceptCardSlot, (UnityEngine.Object) null))
        {
          this.mConceptCardSlots[index].m_ConceptCardSlot.slotIndex = index;
          this.mConceptCardSlots[index].m_ConceptCardSlot.OnSelect = ev;
        }
      }
    }

    [Serializable]
    public struct ConceptCardSlotObjects
    {
      [LabelPropertyName("Slot")]
      public EquipConceptCardSlot m_ConceptCardSlot;
      [LabelPropertyName("Icon")]
      public ConceptCardIcon m_EquipConceptCardIcon;
    }
  }
}
