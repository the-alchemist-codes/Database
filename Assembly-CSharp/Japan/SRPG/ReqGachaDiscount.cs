﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqGachaDiscount
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  public class ReqGachaDiscount : WebAPI
  {
    public ReqGachaDiscount(
      string gacha_id,
      string gacha_group_id,
      string discount_item_iname,
      Network.ResponseCallback response)
    {
      this.name = "gacha/discount/apply";
      this.body = WebAPI.GetRequestString<ReqGachaDiscount.RequestParam>(new ReqGachaDiscount.RequestParam()
      {
        gachaid = gacha_id,
        groupid = gacha_group_id,
        discount_iname = discount_item_iname
      });
      this.callback = response;
    }

    [Serializable]
    public class RequestParam
    {
      public string gachaid;
      public string groupid;
      public string discount_iname;
    }

    [Serializable]
    public class Response
    {
      public Json_Item[] items;
      public Json_GachaParam[] gachas;
    }
  }
}
