﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildRoleBonusRewardInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class GuildRoleBonusRewardInfo : MonoBehaviour
  {
    [SerializeField]
    [Header("報酬リストアイテムの親")]
    private Transform RewardListRoot;
    [SerializeField]
    [Header("報酬リストアイテムのテンプレート")]
    private GameObject RewardListItemTemplate;
    [SerializeField]
    [Header("現在の施設本部Lvテキスト")]
    private Text GuildCurrentFacilityLvText;

    public GuildRoleBonusRewardInfo()
    {
      base.\u002Ector();
    }

    private void Awake()
    {
      GameUtility.SetGameObjectActive(this.RewardListItemTemplate, false);
    }

    private void Start()
    {
      GuildRoleBonusParam[] guildRoleBonusParams = MonoSingleton<GameManager>.Instance.MasterParam.GuildRoleBonusParams;
      if (guildRoleBonusParams == null)
        return;
      GuildMemberData.eRole roleId = MonoSingleton<GameManager>.Instance.Player.PlayerGuild.RoleId;
      GuildRoleBonusParam guildRoleBonusParam1 = (GuildRoleBonusParam) null;
      long serverTime = Network.GetServerTime();
      for (int index = 0; index < guildRoleBonusParams.Length; ++index)
      {
        GuildRoleBonusParam guildRoleBonusParam2 = guildRoleBonusParams[index];
        if (guildRoleBonusParam2 != null && guildRoleBonusParam2.role == roleId && (guildRoleBonusParam2.start_at <= serverTime && serverTime < guildRoleBonusParam2.end_at))
        {
          guildRoleBonusParam1 = guildRoleBonusParam2;
          break;
        }
      }
      this.Refresh(guildRoleBonusParam1);
    }

    private void Refresh(GuildRoleBonusParam param)
    {
      if (param == null || param.rewards == null || (Object.op_Equality((Object) this.RewardListRoot, (Object) null) || Object.op_Equality((Object) this.RewardListItemTemplate, (Object) null)))
        return;
      GuildFacilityData[] facilities = MonoSingleton<GameManager>.Instance.Player.Guild.Facilities;
      int num = 1;
      if (facilities == null)
        return;
      for (int index = 0; index < facilities.Length; ++index)
      {
        GuildFacilityData guildFacilityData = facilities[index];
        if (guildFacilityData != null && guildFacilityData.Param.Type == GuildFacilityParam.eFacilityType.BASE_CAMP)
        {
          num = guildFacilityData.Level;
          break;
        }
      }
      if (Object.op_Inequality((Object) this.GuildCurrentFacilityLvText, (Object) null))
        this.GuildCurrentFacilityLvText.set_text(num.ToString());
      if (param.rewards == null)
        return;
      for (int index = 0; index < param.rewards.Length; ++index)
      {
        GuildRoleBonusDetail reward = param.rewards[index];
        if (reward != null)
        {
          GuildRoleBonusRewardParam bonusRewardParam = MonoSingleton<GameManager>.Instance.MasterParam.GetGuildRoleBonusRewardParam(reward.reward_id);
          if (bonusRewardParam != null)
          {
            GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) this.RewardListItemTemplate);
            if (Object.op_Inequality((Object) gameObject, (Object) null))
            {
              gameObject.get_transform().SetParent(this.RewardListRoot, false);
              GuildRoleBonusRewardInfoListItem component = (GuildRoleBonusRewardInfoListItem) gameObject.GetComponent<GuildRoleBonusRewardInfoListItem>();
              if (Object.op_Inequality((Object) component, (Object) null))
                component.Setup(reward.lv, bonusRewardParam);
            }
            GameUtility.SetGameObjectActive(gameObject, true);
          }
        }
      }
    }
  }
}
