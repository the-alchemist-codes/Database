﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ArenaRewardListItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class ArenaRewardListItem : ListItemEvents
  {
    public GameObject RankImage;
    public GameObject RankText;
    public GameObject RankObjectSingle;
    public Text RankTextSingle;
    public GameObject RankObjectMulti;
    public Text RankTextMultiFrom;
    public Text RankTextMultiTo;
    public GameObject RankObjectMultiTo;
    public GameObject RewardCoin;
    public GameObject RewardArenaCoin;
    public GameObject RewardGold;
    public GameObject RewardItem;

    public void Initialize(ArenaRewardParam param, bool end)
    {
      DataSource.Bind<ArenaRewardParam>(this.RankImage, param, false);
      if (param.Coin > 0 && Object.op_Inequality((Object) this.RewardCoin, (Object) null))
        this.RewardCoin.SetActive(this.SetAmount(this.RewardCoin, param.Coin));
      if (param.Gold > 0 && Object.op_Inequality((Object) this.RewardGold, (Object) null))
        this.RewardGold.SetActive(this.SetAmount(this.RewardGold, param.Gold));
      if (param.ArenaCoin > 0 && Object.op_Inequality((Object) this.RewardArenaCoin, (Object) null))
        this.RewardArenaCoin.SetActive(this.SetAmount(this.RewardArenaCoin, param.ArenaCoin));
      if (param.Items != null && param.Items.Count > 0)
      {
        GameManager instance = MonoSingleton<GameManager>.Instance;
        foreach (ArenaRewardParam.RewardItem rewardItem in param.Items)
        {
          ItemParam itemParam = instance.GetItemParam(rewardItem.iname);
          if (itemParam != null)
          {
            GameObject go = (GameObject) Object.Instantiate<GameObject>((M0) this.RewardItem);
            DataSource.Bind<ItemParam>(go, itemParam, false);
            go.get_transform().SetParent(this.RewardItem.get_transform().get_parent(), false);
            go.SetActive(this.SetAmount(go, rewardItem.num));
          }
        }
      }
      this.SetRankingText(param, end);
      ((Component) this).get_gameObject().SetActive(true);
      GameParameter.UpdateAll(((Component) this).get_gameObject());
    }

    private bool SetAmount(GameObject go, int num)
    {
      Transform transform = go.get_transform().Find("amount/Text_amount");
      if (Object.op_Equality((Object) transform, (Object) null))
        return false;
      Text component = (Text) ((Component) transform).GetComponent<Text>();
      if (Object.op_Equality((Object) component, (Object) null))
        return false;
      component.set_text(num.ToString());
      return true;
    }

    private void SetRankingText(ArenaRewardParam param, bool end)
    {
      if (param.Rank <= 3)
      {
        this.RankText.SetActive(false);
      }
      else
      {
        this.RankText.SetActive(true);
        if (param.Rank != param.FromRank)
        {
          this.RankObjectSingle.SetActive(false);
          this.RankObjectMulti.SetActive(true);
          this.RankTextMultiFrom.set_text(param.FromRank.ToString());
          this.RankTextMultiTo.set_text(param.Rank.ToString());
          if (!end)
            return;
          this.RankObjectMultiTo.SetActive(false);
        }
        else
        {
          this.RankObjectSingle.SetActive(true);
          this.RankObjectMulti.SetActive(false);
          this.RankTextSingle.set_text(param.Rank.ToString());
        }
      }
    }
  }
}
