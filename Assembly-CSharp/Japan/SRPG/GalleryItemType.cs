﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GalleryItemType
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class GalleryItemType
  {
    public const string ABILITY = "ability";
    public const string ARTIFACT = "artifact";
    public const string AWARD = "award";
    public const string COLLABOABIL = "collaboabil";
    public const string CONCEPTCARD = "concept_card";
    public const string ITEM = "item";
    public const string EQUIP = "equip";
    public const string JOB = "job";
    public const string UNIT = "unit";
    public const string UNITABIL = "unitabil";
    public const string UNITDOORABIL = "unitdoorabil";
  }
}
