﻿// Decompiled with JetBrains decompiler
// Type: SRPG.UnitSelectListItemData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class UnitSelectListItemData
  {
    public string iname;
    public UnitParam param;
    public int convert_piece_num;

    public void Deserialize(Json_UnitSelectItem json)
    {
      this.iname = json.iname;
      this.convert_piece_num = json.convert_piece_num;
    }
  }
}
