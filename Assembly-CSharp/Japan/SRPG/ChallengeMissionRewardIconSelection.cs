﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ChallengeMissionRewardIconSelection
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class ChallengeMissionRewardIconSelection : MonoBehaviour
  {
    [SerializeField]
    private GameObject IconLarge;
    [SerializeField]
    private GameObject IconSmall;

    public ChallengeMissionRewardIconSelection()
    {
      base.\u002Ector();
    }

    public void SetLarge(bool isLarge)
    {
      if (Object.op_Inequality((Object) this.IconLarge, (Object) null))
        this.IconLarge.SetActive(isLarge);
      if (!Object.op_Inequality((Object) this.IconSmall, (Object) null))
        return;
      this.IconSmall.SetActive(!isLarge);
    }
  }
}
