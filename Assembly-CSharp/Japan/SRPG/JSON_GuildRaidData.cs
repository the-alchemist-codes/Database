﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_GuildRaidData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_GuildRaidData
  {
    public JSON_GuildRaidPrev prev;
    public JSON_GuildRaidCurrent current;
    public JSON_GuildRaidBattlePoint bp;
    public JSON_GuildRaidBossInfo bossinfo;
    public int can_open_area_id;
    public int refresh_wait_sec;
  }
}
