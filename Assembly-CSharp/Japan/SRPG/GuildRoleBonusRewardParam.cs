﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildRoleBonusRewardParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace SRPG
{
  public class GuildRoleBonusRewardParam
  {
    private string mID;
    private GuildRoleBonusReward[] mRewards;

    public string id
    {
      get
      {
        return this.mID;
      }
    }

    public GuildRoleBonusReward[] rewards
    {
      get
      {
        return this.mRewards;
      }
    }

    public static bool Deserialize(
      ref List<GuildRoleBonusRewardParam> param,
      JSON_GuildRoleBonusRewardParam[] json)
    {
      if (json == null)
        return false;
      param = new List<GuildRoleBonusRewardParam>(json.Length);
      for (int index = 0; index < json.Length; ++index)
      {
        GuildRoleBonusRewardParam bonusRewardParam = new GuildRoleBonusRewardParam();
        bonusRewardParam.Deserialize(json[index]);
        param.Add(bonusRewardParam);
      }
      return true;
    }

    public void Deserialize(JSON_GuildRoleBonusRewardParam json)
    {
      if (json == null || json.rewards == null)
        return;
      this.mID = json.id;
      this.mRewards = new GuildRoleBonusReward[json.rewards.Length];
      for (int index = 0; index < json.rewards.Length; ++index)
      {
        GuildRoleBonusReward guildRoleBonusReward = new GuildRoleBonusReward();
        guildRoleBonusReward.Deserialize(json.rewards[index]);
        this.mRewards[index] = guildRoleBonusReward;
      }
    }
  }
}
