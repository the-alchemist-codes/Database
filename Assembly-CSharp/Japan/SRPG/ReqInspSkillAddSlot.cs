﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqInspSkillAddSlot
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  public class ReqInspSkillAddSlot : WebAPI
  {
    public ReqInspSkillAddSlot(long artifact_iid, Network.ResponseCallback response)
    {
      this.name = "unit/job/artifact/inspirationskill/add_slot";
      this.body = WebAPI.GetRequestString<ReqInspSkillAddSlot.RequestParam>(new ReqInspSkillAddSlot.RequestParam()
      {
        artifact_iid = artifact_iid
      });
      this.callback = response;
    }

    [Serializable]
    public class RequestParam
    {
      public long artifact_iid;
    }

    [Serializable]
    public class Response
    {
      public Json_Artifact artifact;
      public Json_PlayerData player;
    }
  }
}
