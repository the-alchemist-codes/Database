﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqGuildRaidRanking
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using Gsc.Network.Encoding;
using MessagePack;
using System;

namespace SRPG
{
  public class ReqGuildRaidRanking : WebAPI
  {
    public ReqGuildRaidRanking(
      int gid,
      int page,
      GuildRaidManager.GuildRaidRankingType type,
      SRPG.Network.ResponseCallback response,
      EncodingTypes.ESerializeCompressMethod serializeCompressMethod)
    {
      if (type == GuildRaidManager.GuildRaidRankingType.Current)
        this.name = "guildraid/ranking";
      else
        this.name = "guildraid/ranking/history";
      this.body = WebAPI.GetRequestString<ReqGuildRaidRanking.RequestParam>(new ReqGuildRaidRanking.RequestParam()
      {
        gid = gid,
        page = page
      });
      this.callback = response;
      this.serializeCompressMethod = serializeCompressMethod;
    }

    [Serializable]
    public class RequestParam
    {
      public int gid;
      public int page;
    }

    [MessagePackObject(true)]
    [Serializable]
    public class Response
    {
      public JSON_GuildRaidRanking[] ranking;
      public JSON_GuildRaidRanking my_info;
      public int totalPage;
    }
  }
}
