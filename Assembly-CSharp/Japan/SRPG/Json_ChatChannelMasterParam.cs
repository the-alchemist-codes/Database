﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_ChatChannelMasterParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;

namespace SRPG
{
  [MessagePackObject(true)]
  public class Json_ChatChannelMasterParam
  {
    public int pk;
    public Json_ChatChannelMasterParam.Fields fields;

    [MessagePackObject(true)]
    public class Fields
    {
      public int id;
      public byte category_id;
      public string name;
    }
  }
}
