﻿// Decompiled with JetBrains decompiler
// Type: SRPG.PulldownItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class PulldownItem : MonoBehaviour
  {
    public Text Text;
    public Graphic Graphic;
    public int Value;
    public Image Overray;

    public PulldownItem()
    {
      base.\u002Ector();
    }

    public virtual void OnStatusChanged(bool enabled)
    {
    }
  }
}
