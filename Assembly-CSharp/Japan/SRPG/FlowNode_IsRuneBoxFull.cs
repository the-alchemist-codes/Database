﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_IsRuneBoxFull
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;

namespace SRPG
{
  [FlowNode.NodeType("Rune/IsRuneBoxFull", 32741)]
  [FlowNode.Pin(10, "ルーンのBoxが満タンか？", FlowNode.PinTypes.Input, 10)]
  [FlowNode.Pin(100, "true", FlowNode.PinTypes.Output, 100)]
  [FlowNode.Pin(110, "false", FlowNode.PinTypes.Output, 110)]
  public class FlowNode_IsRuneBoxFull : FlowNode
  {
    private const int PIN_INPUT_CHECK = 10;
    private const int PIN_OUTPUT_TRUE = 100;
    private const int PIN_OUTPUT_FALSE = 110;

    public override void OnActivate(int pinID)
    {
      if (pinID != 10)
        return;
      if (MonoSingleton<GameManager>.Instance.Player.IsRuneStorageFull)
        UIUtility.SystemMessage(string.Empty, LocalizedText.Get("sys.RUNE_STORAGE_OVER_TEXT"), (UIUtility.DialogResultEvent) (go => this.ActivateOutputLinks(100)), (GameObject) null, false, -1);
      else
        this.ActivateOutputLinks(110);
    }
  }
}
