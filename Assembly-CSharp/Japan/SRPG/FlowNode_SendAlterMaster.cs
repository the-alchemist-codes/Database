﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_SendAlterMaster
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;

namespace SRPG
{
  [FlowNode.NodeType("Master/SendAlterCheck", 32741)]
  [FlowNode.Pin(0, "Request", FlowNode.PinTypes.Input, 0)]
  [FlowNode.Pin(10, "Success", FlowNode.PinTypes.Output, 10)]
  [FlowNode.Pin(11, "Failed", FlowNode.PinTypes.Output, 11)]
  public class FlowNode_SendAlterMaster : FlowNode_Network
  {
    public override void OnActivate(int pinID)
    {
      if (pinID != 0)
        return;
      if (!AssetManager.UseDLC)
      {
        this.Success();
      }
      else
      {
        string digestHash = MonoSingleton<GameManager>.GetInstanceDirect().DigestHash;
        string alterCheckHash = MonoSingleton<GameManager>.GetInstanceDirect().AlterCheckHash;
        if (!string.IsNullOrEmpty(digestHash) && !string.IsNullOrEmpty(alterCheckHash))
        {
          this.ExecRequest((WebAPI) new ReqSendAlterData(new Network.ResponseCallback(((FlowNode_Network) this).ResponseCallback)));
          ((Behaviour) this).set_enabled(true);
        }
        else
          this.Success();
      }
    }

    private void Success()
    {
      MonoSingleton<GameManager>.GetInstanceDirect().AlterCheckHash = (string) null;
      MonoSingleton<GameManager>.GetInstanceDirect().DigestHash = (string) null;
      MonoSingleton<GameManager>.GetInstanceDirect().PrevCheckHash = (string) null;
      ((Behaviour) this).set_enabled(false);
      this.ActivateOutputLinks(10);
    }

    public override void OnSuccess(WWWResult www)
    {
      if (Network.IsError)
      {
        int errCode = (int) Network.ErrCode;
      }
      Network.RemoveAPI();
      PlayerPrefsUtility.SetString(PlayerPrefsUtility.ALTER_PREV_CHECK_HASH, MonoSingleton<GameManager>.Instance.AlterCheckHash, false);
      this.Success();
    }
  }
}
