﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_VersusEndEnd
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class Json_VersusEndEnd : Json_PlayerDataAll
  {
    public JSON_QuestProgress[] quests;
    public int wincnt;
    public int win_bonus;
    public int key;
    public int rankup;
    public int floor;
    public int arravied;
  }
}
