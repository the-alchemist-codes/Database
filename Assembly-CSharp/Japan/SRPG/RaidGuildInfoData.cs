﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RaidGuildInfoData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class RaidGuildInfoData
  {
    private int mRank;
    private int mScore;

    public int Rank
    {
      get
      {
        return this.mRank;
      }
    }

    public int Score
    {
      get
      {
        return this.mScore;
      }
    }

    public bool Deserialize(Json_RaidGuildInfoData json)
    {
      this.mRank = json.rank;
      this.mScore = json.score;
      return true;
    }
  }
}
