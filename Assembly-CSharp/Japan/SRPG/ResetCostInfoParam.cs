﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ResetCostInfoParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;
using UnityEngine;

namespace SRPG
{
  public class ResetCostInfoParam
  {
    private List<int> num = new List<int>();
    private eResetCostType type;
    private string item;

    public eResetCostType Type
    {
      get
      {
        return this.type;
      }
    }

    public string Item
    {
      get
      {
        return this.item;
      }
    }

    public List<int> Num
    {
      get
      {
        return this.num;
      }
    }

    public void Deserialize(JSON_ResetCostInfoParam json)
    {
      if (json == null)
        return;
      this.type = (eResetCostType) json.type;
      this.item = json.item_iname;
      this.num.Clear();
      if (json.costs == null)
        return;
      for (int index = 0; index < json.costs.Length; ++index)
        this.num.Add(json.costs[index]);
    }

    public int GetNeedNum(int current_reset_count)
    {
      if (this.num.Count <= 0)
      {
        DebugUtility.LogError("MasterParam.ResetCostの消費数データの入力が不十分な可能性があります");
        return 0;
      }
      int num = this.num.Count - 1;
      return this.num[Mathf.Min(current_reset_count, num)];
    }
  }
}
