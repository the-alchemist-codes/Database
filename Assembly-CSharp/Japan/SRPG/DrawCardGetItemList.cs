﻿// Decompiled with JetBrains decompiler
// Type: SRPG.DrawCardGetItemList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;
using UnityEngine;

namespace SRPG
{
  [FlowNode.Pin(1, "Initialize", FlowNode.PinTypes.Input, 1)]
  public class DrawCardGetItemList : MonoBehaviour, IFlowInterface
  {
    private const int INPUT_PIN_INIT = 1;
    [SerializeField]
    private GameObject mItemIconTemplate;

    public DrawCardGetItemList()
    {
      base.\u002Ector();
    }

    public void Activated(int pinID)
    {
      if (pinID != 1)
        return;
      this.Initialize();
    }

    private void Initialize()
    {
      if (Object.op_Equality((Object) this.mItemIconTemplate, (Object) null))
        return;
      this.mItemIconTemplate.SetActive(false);
      List<DrawCardParam.CardData> rewardDrawCardList = DrawCardParam.RewardDrawCardList;
      if (rewardDrawCardList == null || rewardDrawCardList.Count <= 0)
        return;
      Transform parent = this.mItemIconTemplate.get_transform().get_parent();
      foreach (DrawCardParam.CardData data in rewardDrawCardList)
      {
        GameObject root = (GameObject) Object.Instantiate<GameObject>((M0) this.mItemIconTemplate, parent);
        root.SetActive(true);
        DataSource.Bind<DrawCardParam.CardData>(root, data, false);
        GameParameter.UpdateAll(root);
      }
    }
  }
}
