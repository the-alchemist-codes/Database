﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildRaidRankingGuildParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class GuildRaidRankingGuildParam : ContentSource.Param
  {
    public GuildRaidRanking mGuildRaidRanking;
    public bool IsEmpty;
    private GuildRaidRankingGuildNode mNode;

    public override void OnEnable(ContentNode node)
    {
      base.OnEnable(node);
      this.mNode = node as GuildRaidRankingGuildNode;
      this.Refresh();
    }

    public override void OnDisable(ContentNode node)
    {
      this.mNode = (GuildRaidRankingGuildNode) null;
      base.OnDisable(node);
    }

    public void Refresh()
    {
      if (Object.op_Equality((Object) this.mNode, (Object) null))
        return;
      this.mNode.Empty(this.IsEmpty);
      this.mNode.Setup(this.mGuildRaidRanking);
    }
  }
}
