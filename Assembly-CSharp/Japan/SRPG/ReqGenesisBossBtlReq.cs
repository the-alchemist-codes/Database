﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqGenesisBossBtlReq
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  public class ReqGenesisBossBtlReq : WebAPI
  {
    public ReqGenesisBossBtlReq(
      string area_id,
      string qid,
      QuestDifficulties difficulty,
      Network.ResponseCallback response)
    {
      this.name = "genesis/raidboss/btl/req";
      this.body = WebAPI.GetRequestString<ReqGenesisBossBtlReq.RequestParam>(new ReqGenesisBossBtlReq.RequestParam()
      {
        area_id = area_id,
        qid = qid,
        difficulty = (int) difficulty
      });
      this.callback = response;
    }

    [Serializable]
    public class RequestParam
    {
      public string area_id;
      public string qid;
      public int difficulty;
    }
  }
}
