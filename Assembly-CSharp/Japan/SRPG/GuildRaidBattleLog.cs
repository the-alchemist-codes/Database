﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildRaidBattleLog
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  public class GuildRaidBattleLog
  {
    public string Message { get; private set; }

    public int ReportId { get; private set; }

    public DateTime PostedAt { get; private set; }

    public bool Deserialize(JSON_GuildRaidBattleLog json)
    {
      this.Message = json.message;
      this.ReportId = json.report_id;
      this.PostedAt = TimeManager.FromUnixTime((long) json.posted_at);
      return true;
    }
  }
}
