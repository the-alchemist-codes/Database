﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqVersusFreematchStatus
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class ReqVersusFreematchStatus : WebAPI
  {
    public ReqVersusFreematchStatus(Network.ResponseCallback response)
    {
      this.name = "vs/freematch/status";
      this.body = string.Empty;
      this.body = WebAPI.GetRequestString(this.body);
      this.callback = response;
    }

    public class EnableTimeSchedule
    {
      public long expired;
      public long next;
    }

    public class Response
    {
      public ReqVersusFreematchStatus.EnableTimeSchedule enabletime;
    }
  }
}
