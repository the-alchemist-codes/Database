﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildAttendReward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class GuildAttendReward
  {
    private RaidRewardType mType;
    private string mItemIname;
    private int mNum;

    public RaidRewardType type
    {
      get
      {
        return this.mType;
      }
    }

    public string item_iname
    {
      get
      {
        return this.mItemIname;
      }
    }

    public int num
    {
      get
      {
        return this.mNum;
      }
    }

    public bool Deserialize(JSON_GuildAttendReward json)
    {
      if (json == null)
        return false;
      this.mType = (RaidRewardType) json.item_type;
      this.mItemIname = json.item_iname;
      this.mNum = json.item_num;
      return true;
    }
  }
}
