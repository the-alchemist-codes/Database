﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ConceptCardDetailImage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class ConceptCardDetailImage : MonoBehaviour
  {
    [SerializeField]
    private RawImage_Transparent Image;
    [SerializeField]
    private GameObject Message;
    [SerializeField]
    private Text MessageText;
    [SerializeField]
    private GameObject OverlayImageTemplate;

    public ConceptCardDetailImage()
    {
      base.\u002Ector();
    }

    private void Awake()
    {
      if (Object.op_Inequality((Object) this.OverlayImageTemplate, (Object) null))
        this.OverlayImageTemplate.get_gameObject().SetActive(false);
      if (!Object.op_Inequality((Object) this.Message, (Object) null))
        return;
      this.Message.get_gameObject().SetActive(false);
    }

    private void Start()
    {
      ConceptCardData conceptCardData;
      if (Object.op_Inequality((Object) ConceptCardManager.Instance, (Object) null))
      {
        ConceptCardManager instance = ConceptCardManager.Instance;
        conceptCardData = instance.SelectedConceptCardMaterialData == null ? instance.SelectedConceptCardData : instance.SelectedConceptCardMaterialData;
      }
      else
      {
        if (!(FlowNode_ButtonEvent.currentValue is SerializeValueList currentValue))
          return;
        conceptCardData = currentValue.GetObject<ConceptCardData>("conceptcard_data");
      }
      if (!Object.op_Inequality((Object) this.Image, (Object) null) || conceptCardData == null)
        return;
      ConceptCardUnitImageSettings.ComposeUnitConceptCardImage(conceptCardData.Param, (RawImage) this.Image, this.OverlayImageTemplate, this.Message, this.MessageText);
    }
  }
}
