﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AdvanceResult
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  [FlowNode.Pin(1, "報酬表示", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(11, "シェア画面表示へ", FlowNode.PinTypes.Input, 11)]
  [FlowNode.Pin(100, "報酬表示した", FlowNode.PinTypes.Output, 100)]
  [FlowNode.Pin(101, "シェア画面表示した", FlowNode.PinTypes.Output, 101)]
  [FlowNode.Pin(102, "強制終了", FlowNode.PinTypes.Output, 102)]
  public class AdvanceResult : MonoBehaviour, IFlowInterface
  {
    [SerializeField]
    private GameObject Window;
    [SerializeField]
    private GameObject GoReward;
    [SerializeField]
    private GameObject GoShare;
    [Space(10f)]
    [SerializeField]
    private GameObject GoSingle;
    [SerializeField]
    private GameObject GoMultiple;
    [Space(5f)]
    [SerializeField]
    private Text TextRewardName;
    [SerializeField]
    private Text TextRewardNum;
    [SerializeField]
    private GameObject TextRewardConn;
    [SerializeField]
    private AdvanceRewardIcon RewardIconTemplate;
    [SerializeField]
    private Transform TrRewardIconParent;
    [Space(5f)]
    [SerializeField]
    private AdvanceResultItem RewardItemListTemplate;
    [SerializeField]
    private Transform TrRewardItemListParent;
    [SerializeField]
    private AdvanceResultItem RewardIconListTemplate;
    [SerializeField]
    private Transform TrRewardIconListParent;
    [Space(10f)]
    [SerializeField]
    private GameObject GoShareParent;
    private const int PIN_IN_REWARD = 1;
    private const int PIN_IN_SHARE = 11;
    private const int PIN_OUT_REWARD = 100;
    private const int PIN_OUT_SHARE = 101;
    private const int PIN_OUT_EXIT = 102;

    public AdvanceResult()
    {
      base.\u002Ector();
    }

    private void Awake()
    {
      if (!Object.op_Implicit((Object) this.Window))
        return;
      this.Window.SetActive(false);
    }

    private void RewardStart()
    {
      GiftData giftData = (GiftData) null;
      BattleCore battleCore = (BattleCore) null;
      if (Object.op_Implicit((Object) SceneBattle.Instance))
        battleCore = SceneBattle.Instance.Battle;
      if (battleCore != null)
      {
        BattleCore.Record questRecord = battleCore.GetQuestRecord();
        if (questRecord != null && questRecord.mAdvanceBossResultRewards != null && questRecord.mAdvanceBossResultRewards.Length != 0)
        {
          int length = questRecord.mAdvanceBossResultRewards.Length;
          if (Object.op_Implicit((Object) this.GoSingle))
            this.GoSingle.SetActive(length == 1);
          if (Object.op_Implicit((Object) this.GoMultiple))
            this.GoMultiple.SetActive(length != 1);
          if (Object.op_Implicit((Object) this.GoSingle))
            this.GoSingle.SetActive(length == 1);
          if (Object.op_Implicit((Object) this.GoMultiple))
            this.GoMultiple.SetActive(length != 1);
          if (length == 1)
          {
            giftData = new GiftData();
            giftData.Deserialize(questRecord.mAdvanceBossResultRewards[0]);
            string name;
            int amount;
            giftData.GetRewardNameAndAmount(out name, out amount);
            if (Object.op_Implicit((Object) this.TextRewardName))
              this.TextRewardName.set_text(name);
            if (Object.op_Implicit((Object) this.TextRewardNum))
            {
              if (giftData.CheckGiftTypeIncluded(GiftTypes.Gold))
                this.TextRewardNum.set_text(string.Format("{0:#,0}", (object) amount));
              else
                this.TextRewardNum.set_text(amount.ToString());
            }
            if (Object.op_Implicit((Object) this.TextRewardConn))
              this.TextRewardConn.SetActive(true);
            if (Object.op_Implicit((Object) this.RewardIconTemplate) && Object.op_Implicit((Object) this.TrRewardIconParent))
              ((AdvanceRewardIcon) Object.Instantiate<AdvanceRewardIcon>((M0) this.RewardIconTemplate, this.TrRewardIconParent)).Initialize(giftData);
          }
          else
          {
            if (Object.op_Implicit((Object) this.RewardItemListTemplate) && Object.op_Implicit((Object) this.TrRewardItemListParent))
            {
              ((Component) this.RewardItemListTemplate).get_gameObject().SetActive(false);
              GameUtility.DestroyChildGameObjects(((Component) this.TrRewardItemListParent).get_gameObject(), new List<GameObject>((IEnumerable<GameObject>) new GameObject[1]
              {
                ((Component) this.RewardItemListTemplate).get_gameObject()
              }));
            }
            if (Object.op_Implicit((Object) this.RewardIconListTemplate) && Object.op_Implicit((Object) this.TrRewardIconListParent))
            {
              ((Component) this.RewardIconListTemplate).get_gameObject().SetActive(false);
              GameUtility.DestroyChildGameObjects(((Component) this.TrRewardIconListParent).get_gameObject(), new List<GameObject>((IEnumerable<GameObject>) new GameObject[1]
              {
                ((Component) this.RewardIconListTemplate).get_gameObject()
              }));
            }
            for (int index = 0; index < length; ++index)
            {
              giftData = new GiftData();
              giftData.Deserialize(questRecord.mAdvanceBossResultRewards[index]);
              if (Object.op_Implicit((Object) this.RewardItemListTemplate) && Object.op_Implicit((Object) this.TrRewardItemListParent))
              {
                AdvanceResultItem advanceResultItem = (AdvanceResultItem) Object.Instantiate<AdvanceResultItem>((M0) this.RewardItemListTemplate, this.TrRewardItemListParent, false);
                if (Object.op_Implicit((Object) advanceResultItem))
                {
                  advanceResultItem.SetItem(index, giftData);
                  ((Component) advanceResultItem).get_gameObject().SetActive(true);
                }
              }
              if (Object.op_Implicit((Object) this.RewardIconListTemplate) && Object.op_Implicit((Object) this.TrRewardIconListParent))
              {
                AdvanceResultItem advanceResultItem = (AdvanceResultItem) Object.Instantiate<AdvanceResultItem>((M0) this.RewardIconListTemplate, this.TrRewardIconListParent, false);
                if (Object.op_Implicit((Object) advanceResultItem))
                {
                  advanceResultItem.SetItem(index, giftData);
                  ((Component) advanceResultItem).get_gameObject().SetActive(true);
                }
              }
            }
          }
        }
      }
      if (giftData == null)
      {
        this.Exit();
      }
      else
      {
        GameUtility.SetGameObjectActive(this.GoReward, true);
        GameUtility.SetGameObjectActive(this.GoShare, false);
        ((Component) this).get_gameObject().SetActive(true);
        if (!Object.op_Implicit((Object) this.Window))
          return;
        this.Window.SetActive(true);
        GameParameter.UpdateAll(this.Window);
      }
    }

    private void ShareStart()
    {
      if (Object.op_Implicit((Object) this.GoReward) && Object.op_Implicit((Object) this.GoShare) && Object.op_Implicit((Object) this.GoShareParent))
      {
        GameManager instance1 = MonoSingleton<GameManager>.Instance;
        SceneBattle instance2 = SceneBattle.Instance;
        if (Object.op_Implicit((Object) instance1) && Object.op_Implicit((Object) instance2) && instance2.CurrentQuest != null)
        {
          QuestParam currentQuest = instance2.CurrentQuest;
          AdvanceEventParam eventParamFromAreaId = instance1.GetAdvanceEventParamFromAreaId(currentQuest.ChapterID);
          if (eventParamFromAreaId != null)
          {
            AdvanceEventModeInfoParam modeInfo = eventParamFromAreaId.GetModeInfo(currentQuest.difficulty);
            if (modeInfo != null && modeInfo.IsLapBoss)
            {
              DataSource.Bind<AdvanceBossInfo.AdvanceBossData>(this.GoShareParent, new AdvanceBossInfo.AdvanceBossData()
              {
                unit = modeInfo.BossUnitParam
              }, false);
              GameParameter.UpdateAll(this.GoShareParent);
              this.GoReward.SetActive(false);
              this.GoShare.SetActive(true);
              FlowNode_GameObject.ActivateOutputLinks((Component) this, 101);
              return;
            }
          }
        }
      }
      this.Exit();
    }

    private void PrepareAsset()
    {
      GameManager instance1 = MonoSingleton<GameManager>.Instance;
      SceneBattle instance2 = SceneBattle.Instance;
      if (Object.op_Implicit((Object) instance1) && Object.op_Implicit((Object) instance2) && instance2.CurrentQuest != null)
      {
        QuestParam currentQuest = instance2.CurrentQuest;
        AdvanceEventParam eventParamFromAreaId = instance1.GetAdvanceEventParamFromAreaId(currentQuest.ChapterID);
        if (eventParamFromAreaId != null)
        {
          AdvanceEventModeInfoParam modeInfo = eventParamFromAreaId.GetModeInfo(currentQuest.difficulty);
          if (modeInfo != null && modeInfo.IsLapBoss)
            AssetManager.PrepareAssets(AssetPath.UnitImage(modeInfo.BossUnitParam, (string) null));
        }
      }
      this.StartCoroutine(this.DownloadUnitImage());
    }

    [DebuggerHidden]
    private IEnumerator DownloadUnitImage()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new AdvanceResult.\u003CDownloadUnitImage\u003Ec__Iterator0()
      {
        \u0024this = this
      };
    }

    private void Exit()
    {
      FlowNode_GameObject.ActivateOutputLinks((Component) this, 102);
    }

    public void Activated(int pinID)
    {
      if (pinID != 1)
      {
        if (pinID != 11)
          return;
        this.ShareStart();
      }
      else
        this.PrepareAsset();
    }
  }
}
