﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ChangeListItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class ChangeListItem : MonoBehaviour
  {
    [FourCC]
    public int ID;
    public Text Label;
    public Text ValOld;
    public Text ValNew;
    public Text Diff;

    public ChangeListItem()
    {
      base.\u002Ector();
    }
  }
}
