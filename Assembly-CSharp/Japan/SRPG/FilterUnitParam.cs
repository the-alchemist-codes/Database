﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FilterUnitParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class FilterUnitParam
  {
    public string iname;
    public string tab_name;
    public string name;
    public FilterUnitConditionParam[] conditions;
    private eFilterUnitTypes filter_type;

    public bool IsEnableFilterType(eFilterUnitTypes type)
    {
      return this.filter_type == type;
    }

    public void Deserialize(JSON_FilterUnitParam json)
    {
      this.iname = json.iname;
      this.tab_name = json.tab_name;
      this.name = json.name;
      this.filter_type = (eFilterUnitTypes) json.filter_type;
      if (json.cnds == null)
        return;
      this.conditions = new FilterUnitConditionParam[json.cnds.Length];
      for (int index = 0; index < json.cnds.Length; ++index)
      {
        FilterUnitConditionParam unitConditionParam = new FilterUnitConditionParam(this);
        unitConditionParam.Deserialize(json.cnds[index]);
        this.conditions[index] = unitConditionParam;
      }
    }

    public static void Deserialize(ref FilterUnitParam[] param, JSON_FilterUnitParam[] json)
    {
      if (json == null)
        return;
      param = new FilterUnitParam[json.Length];
      for (int index = 0; index < json.Length; ++index)
      {
        FilterUnitParam filterUnitParam = new FilterUnitParam();
        filterUnitParam.Deserialize(json[index]);
        param[index] = filterUnitParam;
      }
    }
  }
}
