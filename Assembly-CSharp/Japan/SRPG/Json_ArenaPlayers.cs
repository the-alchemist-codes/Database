﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_ArenaPlayers
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class Json_ArenaPlayers
  {
    public Json_ArenaPlayer[] coloenemies;
    public int rank_myself;
    public int best_myself;
    public long btl_at;
    public string quest_iname;
    public int seed;
    public int maxActionNum;
    public long end_at;
  }
}
