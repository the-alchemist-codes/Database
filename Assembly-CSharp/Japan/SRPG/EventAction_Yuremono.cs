﻿// Decompiled with JetBrains decompiler
// Type: SRPG.EventAction_Yuremono
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  [EventActionInfo("New/アクター/揺れもの切替", "アクターの揺れもの状態を切り替えます。", 6702148, 11158596)]
  public class EventAction_Yuremono : EventAction
  {
    [StringIsActorList]
    public string ActorID;
    public bool EnableYuremono;

    public override void OnActivate()
    {
      TacticsUnitController byUniqueName = TacticsUnitController.FindByUniqueName(this.ActorID);
      if (Object.op_Inequality((Object) byUniqueName, (Object) null))
      {
        foreach (Behaviour componentsInChild in (YuremonoInstance[]) ((Component) byUniqueName).get_gameObject().GetComponentsInChildren<YuremonoInstance>())
          componentsInChild.set_enabled(this.EnableYuremono);
      }
      this.ActivateNext();
    }
  }
}
