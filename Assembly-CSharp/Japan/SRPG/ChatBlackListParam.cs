﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ChatBlackListParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class ChatBlackListParam
  {
    public string name;
    public string uid;
    public long blocked_at;
    public long lastlogin;
    public string icon;
    public string skin_iname;
    public string job_iname;
    public int exp;
    public Json_Unit unit;
  }
}
