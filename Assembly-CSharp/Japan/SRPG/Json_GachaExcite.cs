﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_GachaExcite
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class Json_GachaExcite
  {
    public int pk;
    public Json_GachaExcite.Fields fields;

    public class Fields
    {
      public int id;
      public int rarity;
      public int weight;
      public string color1;
      public string color2;
      public string color3;
      public string color4;
      public string color5;
    }
  }
}
