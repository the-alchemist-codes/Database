﻿// Decompiled with JetBrains decompiler
// Type: SRPG.DropItemNode
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class DropItemNode : ContentNode
  {
    [SerializeField]
    private DropItemIcon mDropItemIcon;

    public DropItemIcon DropItemIcon
    {
      get
      {
        return this.mDropItemIcon;
      }
    }

    public override void Initialize(ContentController controller)
    {
      base.Initialize(controller);
    }

    public override void Release()
    {
      base.Release();
    }
  }
}
