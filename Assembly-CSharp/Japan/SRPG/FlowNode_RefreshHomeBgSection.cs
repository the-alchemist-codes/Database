﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_RefreshHomeBgSection
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  [FlowNode.NodeType("Home/RefreshHomeBgSection", 32741)]
  [FlowNode.Pin(1, "In", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(100, "Out", FlowNode.PinTypes.Output, 100)]
  public class FlowNode_RefreshHomeBgSection : FlowNode
  {
    private const int PIN_IN_INPUT = 1;
    private const int PIN_OUT_OUTPUT = 100;

    public override void OnActivate(int pinID)
    {
      if (pinID != 1)
        return;
      this.Refresh_HomeBgSection();
      this.ActivateOutputLinks(100);
    }

    private void Refresh_HomeBgSection()
    {
      if (!string.IsNullOrEmpty((string) GlobalVars.SelectedSection))
        HomeBgSection.SetSection_SectionId((string) GlobalVars.SelectedSection);
      else if (!string.IsNullOrEmpty((string) GlobalVars.SelectedChapter))
      {
        HomeBgSection.SetSection_ChapterId((string) GlobalVars.SelectedChapter);
      }
      else
      {
        if (string.IsNullOrEmpty(GlobalVars.SelectedQuestID))
          return;
        HomeBgSection.SetSection_QuestId(GlobalVars.SelectedQuestID);
      }
    }
  }
}
