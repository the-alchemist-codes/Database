﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ExpireItemParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  [Serializable]
  public class ExpireItemParam
  {
    public string iname;
    public int num;

    public void Deserialize(Json_ExpireItem json)
    {
      this.iname = json.iname;
      this.num = json.num;
    }
  }
}
