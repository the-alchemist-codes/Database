﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildRaidRankingGuild
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class GuildRaidRankingGuild : ViewGuildData
  {
    public int MemberCount { get; private set; }

    public int MemberMax { get; private set; }

    public bool Deserialize(JSON_GuildRaidRankingGuild json)
    {
      this.Deserialize((JSON_ViewGuild) json);
      this.MemberCount = json.member_count;
      this.MemberMax = json.member_max;
      return true;
    }
  }
}
