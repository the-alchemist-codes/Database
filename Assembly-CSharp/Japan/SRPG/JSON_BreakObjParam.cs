﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_BreakObjParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_BreakObjParam
  {
    public string iname;
    public string name;
    public string expr;
    public string unit_id;
    public int clash_type;
    public int ai_type;
    public int side_type;
    public int ray_type;
    public int is_ui;
    public string rest_hps;
    public int clock;
    public int appear_dir;
  }
}
