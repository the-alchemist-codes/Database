﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GvGNPCPartyParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace SRPG
{
  public class GvGNPCPartyParam : GvGMasterParam<JSON_GvGNPCPartyParam>
  {
    public int Id { get; private set; }

    public List<GvGNPCPartyDetailParam> Party { get; private set; }

    public override bool Deserialize(JSON_GvGNPCPartyParam json)
    {
      if (json == null)
        return false;
      this.Id = json.id;
      this.Party = new List<GvGNPCPartyDetailParam>();
      if (json.party != null)
      {
        for (int index = 0; index < json.party.Length; ++index)
        {
          GvGNPCPartyDetailParam partyDetailParam = new GvGNPCPartyDetailParam();
          if (partyDetailParam.Deserialize(json.party[index]))
            this.Party.Add(partyDetailParam);
        }
      }
      return true;
    }
  }
}
