﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqRankMatchMission
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  public class ReqRankMatchMission : WebAPI
  {
    public ReqRankMatchMission(Network.ResponseCallback response)
    {
      this.name = "vs/rankmatch/mission";
      this.body = WebAPI.GetRequestString(string.Empty);
      this.callback = response;
    }

    [Serializable]
    public class MissionProgress
    {
      public string iname;
      public int prog;
      public string rewarded_at;
    }

    [Serializable]
    public class Response
    {
      public ReqRankMatchMission.MissionProgress[] missionprogs;
    }
  }
}
