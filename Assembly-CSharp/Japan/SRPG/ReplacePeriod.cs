﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReplacePeriod
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;

namespace SRPG
{
  [MessagePackObject(true)]
  public class ReplacePeriod
  {
    public string mSpriteName;
    public string mBeginAt;
    public string mEndAt;

    public void Deserialize(JSON_ReplacePeriod json)
    {
      this.mSpriteName = json.sprite_name;
      this.mBeginAt = json.begin_at;
      this.mEndAt = json.end_at;
    }
  }
}
