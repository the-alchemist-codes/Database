﻿// Decompiled with JetBrains decompiler
// Type: SRPG.CoinBuyUseBonusContentParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  public class CoinBuyUseBonusContentParam
  {
    private string name;
    private int num;
    private string reward_iname;
    private string gift_msg;
    private CoinBuyUseBonusRewardParam mRewardParam;

    public string Name
    {
      get
      {
        return this.name;
      }
    }

    public int Num
    {
      get
      {
        return this.num;
      }
    }

    public string RewardIname
    {
      get
      {
        return this.reward_iname;
      }
    }

    public string GiftMsg
    {
      get
      {
        return this.gift_msg;
      }
    }

    public CoinBuyUseBonusRewardParam RewardParam
    {
      get
      {
        return this.mRewardParam;
      }
    }

    public void Deserialize(
      JSON_CoinBuyUseBonusContentParam json,
      CoinBuyUseBonusRewardParam[] reward_params)
    {
      this.name = json.name;
      this.num = json.num;
      this.reward_iname = json.reward_iname;
      this.gift_msg = json.gift_msg;
      this.mRewardParam = Array.Find<CoinBuyUseBonusRewardParam>(reward_params, (Predicate<CoinBuyUseBonusRewardParam>) (param => param.Iname == this.reward_iname));
    }
  }
}
