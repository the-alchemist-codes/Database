﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_GuildEmblemParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_GuildEmblemParam
  {
    public string iname;
    public string name;
    public int cnds_type;
    public int cnds_val;
    public string image;
    public string start_at;
    public string end_at;
  }
}
