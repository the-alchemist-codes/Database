﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RuneIcon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class RuneIcon : MonoBehaviour
  {
    [SerializeField]
    private Button mButton;
    [SerializeField]
    private Image mCheckIcon;
    [SerializeField]
    private ImageSpriteSheet mSetType;
    [SerializeField]
    private ImageArray mEvoImage;
    [SerializeField]
    private ImageArray mEnhanceImage;
    [SerializeField]
    private GameObject mOwnerIconParent;
    [SerializeField]
    private Image mOwnerIcon;
    [SerializeField]
    private Image mSelectObject;
    [SerializeField]
    private Text mStatusCount;
    [SerializeField]
    private bool mIsReplaseRune;
    private BindRuneData mRune;

    public RuneIcon()
    {
      base.\u002Ector();
    }

    public BindRuneData Rune
    {
      get
      {
        return this.mRune;
      }
    }

    public void Setup(BindRuneData rune, bool is_owner_disable = false)
    {
      this.mRune = rune;
      this.Refresh();
    }

    public void Refresh()
    {
      DataSource.Bind<BindRuneData>(((Component) this).get_gameObject(), this.mRune, false);
      GameParameter.UpdateAll(((Component) this).get_gameObject());
      this.RefreshCheckIcon();
      this.RefreshSetType();
      this.RefreshEvoImage();
      this.RefreshEnhanceImage();
      this.RefreshOwnerIcon();
      this.RefreshSelectFrame();
      this.RefreshBaseStatusCount();
    }

    private void RefreshSetType()
    {
      if (this.mRune == null || Object.op_Equality((Object) this.mSetType, (Object) null))
        return;
      RuneData rune = this.mRune.Rune;
      if (rune == null)
        return;
      RuneParam runeParam = rune.RuneParam;
      if (runeParam == null)
        return;
      this.mSetType.SetSprite(runeParam.SetEffTypeIconIndex.ToString());
    }

    private void RefreshEvoImage()
    {
      if (this.mRune == null || Object.op_Equality((Object) this.mEvoImage, (Object) null))
        return;
      RuneData rune = this.mRune.Rune;
      if (rune == null)
        return;
      if (rune.EvoNum <= 0)
      {
        ((Behaviour) this.mEvoImage).set_enabled(false);
        this.mEvoImage.ImageIndex = 0;
      }
      else
      {
        ((Behaviour) this.mEvoImage).set_enabled(true);
        this.mEvoImage.ImageIndex = rune.EvoNum - 1;
      }
    }

    private void RefreshEnhanceImage()
    {
      if (this.mRune == null)
        return;
      RuneData rune = this.mRune.Rune;
      if (rune == null || !Object.op_Inequality((Object) this.mEnhanceImage, (Object) null))
        return;
      ((Behaviour) this.mEnhanceImage).set_enabled(rune.EnhanceNum > 0);
      this.mEnhanceImage.ImageIndex = rune.EnhanceNum;
    }

    private void RefreshOwnerIcon()
    {
      if (this.mRune == null || Object.op_Equality((Object) this.mOwnerIcon, (Object) null))
        return;
      RuneData rune = this.mRune.Rune;
      if (rune == null)
        return;
      UnitData owner = rune.GetOwner();
      if (owner != null)
      {
        if (Object.op_Inequality((Object) this.mOwnerIconParent, (Object) null))
          this.mOwnerIconParent.SetActive(true);
        this.mOwnerIcon.set_sprite(AssetManager.Load<SpriteSheet>("ItemIcon/small").GetSprite(MonoSingleton<GameManager>.Instance.GetItemParam(owner.UnitParam.piece).icon));
        if (this.mIsReplaseRune)
        {
          if (!Object.op_Inequality((Object) this.mButton, (Object) null))
            return;
          ((Selectable) this.mButton).set_interactable(true);
        }
        else
        {
          if (!Object.op_Inequality((Object) this.mButton, (Object) null))
            return;
          ((Selectable) this.mButton).set_interactable(!this.mRune.is_owner_disable);
        }
      }
      else
      {
        if (Object.op_Inequality((Object) this.mOwnerIconParent, (Object) null))
          this.mOwnerIconParent.SetActive(false);
        this.mOwnerIcon.set_sprite((Sprite) null);
        if (!Object.op_Inequality((Object) this.mButton, (Object) null))
          return;
        ((Selectable) this.mButton).set_interactable(true);
      }
    }

    private void RefreshCheckIcon()
    {
      if (this.mRune == null || !Object.op_Inequality((Object) this.mCheckIcon, (Object) null))
        return;
      ((Behaviour) this.mCheckIcon).set_enabled(this.mRune.is_check);
    }

    public void RefreshSelectFrame()
    {
      if (this.mRune == null || !Object.op_Inequality((Object) this.mSelectObject, (Object) null))
        return;
      ((Behaviour) this.mSelectObject).set_enabled(this.mRune.is_selected);
    }

    public UnitData GetOwner()
    {
      if (this.mRune == null)
        return (UnitData) null;
      return this.mRune.Rune?.GetOwner();
    }

    public void RefreshEnableParam(bool enable)
    {
      Button component = (Button) ((Component) ((Component) this).get_transform()).get_gameObject().GetComponent<Button>();
      if (!Object.op_Inequality((Object) component, (Object) null))
        return;
      ((Selectable) component).set_interactable(enable);
    }

    public void RefreshBaseStatusCount()
    {
      if (this.mRune == null || !Object.op_Implicit((Object) this.mStatusCount))
        return;
      this.mStatusCount.set_text(this.mRune.Rune.DisplayRuneBaseParam());
    }
  }
}
