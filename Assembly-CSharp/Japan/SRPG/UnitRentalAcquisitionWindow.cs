﻿// Decompiled with JetBrains decompiler
// Type: SRPG.UnitRentalAcquisitionWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class UnitRentalAcquisitionWindow : MonoBehaviour
  {
    public UnitRentalAcquisitionWindow()
    {
      base.\u002Ector();
    }

    private void Start()
    {
      UnitData getUnitData = UnitRentalParam.GetUnitData;
      if (getUnitData == null)
        return;
      DataSource.Bind<UnitData>(((Component) this).get_gameObject(), getUnitData, false);
      GameParameter.UpdateAll(((Component) this).get_gameObject());
    }
  }
}
