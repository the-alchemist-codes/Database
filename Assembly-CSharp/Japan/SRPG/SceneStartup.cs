﻿// Decompiled with JetBrains decompiler
// Type: SRPG.SceneStartup
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using MessagePack.Resolvers;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

namespace SRPG
{
  [AddComponentMenu("Scripts/SRPG/Scene/Startup")]
  public class SceneStartup : Scene
  {
    public bool AutoStart = true;
    private static bool mResolutionChanged;
    private const string Key_ClearCache = "CLEARCACHE";

    private new void Awake()
    {
      base.Awake();
      MonoSingleton<UrlScheme>.Instance.Ensure();
      MonoSingleton<PaymentManager>.Instance.Ensure();
      MonoSingleton<NetworkError>.Instance.Ensure();
      MonoSingleton<WatchManager>.Instance.Ensure();
      MonoSingleton<PermissionManager>.Instance.Ensure();
      TextAsset textAsset = (TextAsset) Resources.Load<TextAsset>("appserveraddr");
      if (Object.op_Inequality((Object) textAsset, (Object) null))
        Network.SetDefaultHostConfigured(textAsset.get_text());
      SceneStartup.SetupMessagePackResolvers();
    }

    [DebuggerHidden]
    private IEnumerator Start()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new SceneStartup.\u003CStart\u003Ec__Iterator0()
      {
        \u0024this = this
      };
    }

    public static void SetupMessagePackResolvers()
    {
      CompositeResolver.RegisterAndSetAsDefault(GeneratedResolver.Instance, BuiltinResolver.Instance, AttributeFormatterResolver.Instance, PrimitiveObjectResolver.Instance);
    }
  }
}
