﻿// Decompiled with JetBrains decompiler
// Type: SRPG.VersusDraftMapWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;

namespace SRPG
{
  public class VersusDraftMapWindow : MonoBehaviour
  {
    public VersusDraftMapWindow()
    {
      base.\u002Ector();
    }

    private void Start()
    {
      GameManager instance = MonoSingleton<GameManager>.Instance;
      QuestParam quest = instance.FindQuest(instance.VSDraftQuestId);
      if (quest == null)
        return;
      DataSource.Bind<QuestParam>(((Component) this).get_gameObject(), quest, false);
    }
  }
}
