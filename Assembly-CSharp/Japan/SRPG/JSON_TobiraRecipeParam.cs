﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_TobiraRecipeParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_TobiraRecipeParam
  {
    public string recipe_iname;
    public int tobira_lv;
    public int cost;
    public int unit_piece_num;
    public int piece_elem_num;
    public int unlock_elem_num;
    public int unlock_birth_num;
    public JSON_TobiraRecipeMaterialParam[] mats;
  }
}
