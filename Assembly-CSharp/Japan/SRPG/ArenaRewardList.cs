﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ArenaRewardList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace SRPG
{
  public class ArenaRewardList : SRPG_ListBase
  {
    [SerializeField]
    private ArenaRewardListItem ListItem;

    protected override void Start()
    {
      this.Refresh();
      base.Start();
    }

    private void Refresh()
    {
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) this.ListItem, (UnityEngine.Object) null))
        return;
      ((Component) this.ListItem).get_gameObject().SetActive(false);
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) this.ListItem.RewardCoin, (UnityEngine.Object) null))
        return;
      this.ListItem.RewardCoin.SetActive(false);
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) this.ListItem.RewardArenaCoin, (UnityEngine.Object) null))
        return;
      this.ListItem.RewardArenaCoin.SetActive(false);
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) this.ListItem.RewardGold, (UnityEngine.Object) null))
        return;
      this.ListItem.RewardGold.SetActive(false);
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) this.ListItem.RewardItem, (UnityEngine.Object) null))
        return;
      this.ListItem.RewardItem.SetActive(false);
      List<ArenaRewardParam> arp = MonoSingleton<GameManager>.Instance.MasterParam.GetArenaRewardParams();
      arp.ForEach((Action<ArenaRewardParam>) (reward =>
      {
        ArenaRewardListItem arenaRewardListItem = (ArenaRewardListItem) UnityEngine.Object.Instantiate<ArenaRewardListItem>((M0) this.ListItem);
        ((Component) arenaRewardListItem).get_transform().SetParent(((Component) this.ListItem).get_transform().get_parent(), false);
        arenaRewardListItem.Initialize(reward, arp.IndexOf(reward) >= arp.Count - 1);
        this.AddItem((ListItemEvents) arenaRewardListItem);
      }));
    }
  }
}
