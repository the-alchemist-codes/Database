﻿// Decompiled with JetBrains decompiler
// Type: SRPG.HomeUnitController
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

namespace SRPG
{
  public class HomeUnitController : UnitController
  {
    public Color DirectLitColor = Color.get_white();
    public Color IndirectLitColor = Color.get_clear();
    private const string ID_IDLE = "idle";
    private const string ID_ACTION = "action";

    public static SectionParam GetHomeWorld()
    {
      GameManager instance = MonoSingleton<GameManager>.Instance;
      string iname = (string) GlobalVars.HomeBgSection;
      if (string.IsNullOrEmpty(iname))
      {
        QuestParam lastStoryQuest = instance.Player.FindLastStoryQuest();
        if (lastStoryQuest != null && lastStoryQuest.Chapter != null)
        {
          iname = lastStoryQuest.Chapter.section;
          GlobalVars.HomeBgSection.Set(iname);
        }
      }
      return instance.FindWorld(iname);
    }

    protected override void Start()
    {
      CriticalSection.Enter(CriticalSections.SceneChange);
      this.LoadEquipments = true;
      base.Start();
    }

    protected override void OnDestroy()
    {
      base.OnDestroy();
    }

    protected override void OnEnable()
    {
      base.OnEnable();
    }

    protected override void OnDisable()
    {
      base.OnDisable();
    }

    protected override void PostSetup()
    {
      base.PostSetup();
      this.LoadUnitAnimationAsync("idle", "unit_info_idle0", true, false);
      this.LoadUnitAnimationAsync("action", "unit_info_act0", true, false);
      this.StartCoroutine(this.LoadAsync());
    }

    [DebuggerHidden]
    private IEnumerator LoadAsync()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new HomeUnitController.\u003CLoadAsync\u003Ec__Iterator0()
      {
        \u0024this = this
      };
    }
  }
}
