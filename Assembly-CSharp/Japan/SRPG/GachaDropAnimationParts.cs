﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GachaDropAnimationParts
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class GachaDropAnimationParts : MonoBehaviour
  {
    [SerializeField]
    private GameObject[] DropObjs;

    public GachaDropAnimationParts()
    {
      base.\u002Ector();
    }

    private void ResetObj()
    {
      if (this.DropObjs == null)
        return;
      for (int index = 0; index < this.DropObjs.Length; ++index)
      {
        if (Object.op_Inequality((Object) this.DropObjs[index], (Object) null))
          this.DropObjs[index].SetActive(false);
      }
    }

    public void Setup(GameObject drop_object)
    {
      this.ResetObj();
      if (Object.op_Equality((Object) drop_object, (Object) null) || this.DropObjs == null || (this.DropObjs.Length <= 0 || !Object.op_Inequality((Object) this.DropObjs[0], (Object) null)))
        return;
      this.DropObjs[0].SetActive(true);
      drop_object.get_transform().SetParent(this.DropObjs[0].get_transform(), false);
    }

    public void Setup(GameObject[] drop_objects, GachaDropData[] drops)
    {
      this.ResetObj();
      if (drop_objects == null || drop_objects.Length <= 0 || (drops == null || drops.Length <= 0) || (this.DropObjs == null || this.DropObjs.Length <= 0))
        return;
      int num = drop_objects.Length <= this.DropObjs.Length ? drop_objects.Length : this.DropObjs.Length;
      for (int index = 0; index < num; ++index)
      {
        GameObject dropObj = this.DropObjs[index];
        if (Object.op_Inequality((Object) dropObj, (Object) null))
        {
          dropObj.SetActive(true);
          drop_objects[index].get_transform().SetParent(dropObj.get_transform(), false);
          GachaAnimationParts component = (GachaAnimationParts) dropObj.GetComponent<GachaAnimationParts>();
          if (Object.op_Inequality((Object) component, (Object) null))
            component.Setup(drops[index].FirstExcite);
        }
      }
    }

    public void Setup(GachaDropData[] drops)
    {
      this.ResetObj();
      if (drops == null || drops.Length <= 0)
      {
        DebugUtility.LogError("排出データが存在しません.");
      }
      else
      {
        if (this.DropObjs == null || this.DropObjs.Length <= 0)
          return;
        int num = drops.Length <= this.DropObjs.Length ? drops.Length : this.DropObjs.Length;
        for (int index = 0; index < num; ++index)
        {
          GameObject dropObj = this.DropObjs[index];
          if (Object.op_Inequality((Object) dropObj, (Object) null))
          {
            dropObj.SetActive(true);
            GachaAnimationParts componentInChildren = (GachaAnimationParts) dropObj.GetComponentInChildren<GachaAnimationParts>();
            if (Object.op_Inequality((Object) componentInChildren, (Object) null))
              componentInChildren.Setup(drops[index].excites[0]);
          }
        }
      }
    }
  }
}
