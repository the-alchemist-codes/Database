﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqRankMatchReward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  public class ReqRankMatchReward : WebAPI
  {
    public ReqRankMatchReward(Network.ResponseCallback response)
    {
      this.name = "vs/rankmatch/reward";
      this.body = WebAPI.GetRequestString(string.Empty);
      this.callback = response;
    }

    [Serializable]
    public class RwardResponse
    {
      public string ranking;
      public string type;
    }

    [Serializable]
    public class Response
    {
      public int schedule_id;
      public int score;
      public int rank;
      public int type;
      public ReqRankMatchReward.RwardResponse reward;
    }
  }
}
