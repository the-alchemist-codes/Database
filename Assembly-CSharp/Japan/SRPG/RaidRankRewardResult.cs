﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RaidRankRewardResult
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class RaidRankRewardResult
  {
    private int mStatus;
    private int mPeriodId;
    private int mRank;
    private int mScore;
    private string mReward;
    private int mResqueRank;
    private int mResqueScore;
    private string mRescueReward;
    private ViewGuildData mGuild;
    private int mGuildRank;
    private int mGuildScore;
    private string mGuildReward;
    private string mGuildMemberReward;

    public int Status
    {
      get
      {
        return this.mStatus;
      }
    }

    public int PeriodId
    {
      get
      {
        return this.mPeriodId;
      }
    }

    public int Score
    {
      get
      {
        return this.mScore;
      }
    }

    public int Rank
    {
      get
      {
        return this.mRank;
      }
    }

    public string Reward
    {
      get
      {
        return this.mReward;
      }
    }

    public int ResqueScore
    {
      get
      {
        return this.mResqueScore;
      }
    }

    public int ResqueRank
    {
      get
      {
        return this.mResqueRank;
      }
    }

    public string RescueReward
    {
      get
      {
        return this.mRescueReward;
      }
    }

    public ViewGuildData Guild
    {
      get
      {
        return this.mGuild;
      }
    }

    public int GuildScore
    {
      get
      {
        return this.mGuildScore;
      }
    }

    public int GuildRank
    {
      get
      {
        return this.mGuildRank;
      }
    }

    public string GuildReward
    {
      get
      {
        return this.mGuildReward;
      }
    }

    public string GuildMemberReward
    {
      get
      {
        return this.mGuildMemberReward;
      }
    }

    public void Deserialize(ReqRaidRankingReward.Response res)
    {
      this.mStatus = res.status;
      this.mPeriodId = res.period_id;
      if (res.beat != null && res.beat.my_info != null)
      {
        this.mRank = res.beat.my_info.rank;
        this.mScore = res.beat.my_info.score;
        this.mReward = res.beat.reward_id;
      }
      if (res.rescue != null && res.rescue.my_info != null)
      {
        this.mResqueRank = res.rescue.my_info.rank;
        this.mResqueScore = res.rescue.my_info.score;
        this.mRescueReward = res.rescue.reward_id;
      }
      if (res.my_guild_info == null || res.my_guild_info.beat == null)
        return;
      this.mGuildRank = res.my_guild_info.beat.rank;
      this.mGuildScore = res.my_guild_info.beat.score;
      this.mGuildReward = res.my_guild_info.guild_reward_id;
      this.mGuildMemberReward = res.my_guild_info.reward_id;
      if (res.my_guild_info.guild == null)
        return;
      this.mGuild = new ViewGuildData();
      this.mGuild.Deserialize(res.my_guild_info.guild);
    }
  }
}
