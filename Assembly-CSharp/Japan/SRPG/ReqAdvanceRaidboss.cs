﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqAdvanceRaidboss
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  public class ReqAdvanceRaidboss : WebAPI
  {
    public ReqAdvanceRaidboss(
      string area_id,
      QuestDifficulties difficulty,
      Network.ResponseCallback response)
    {
      this.name = "advance/raidboss";
      this.body = WebAPI.GetRequestString<ReqAdvanceRaidboss.RequestParam>(new ReqAdvanceRaidboss.RequestParam()
      {
        area_id = area_id,
        difficulty = (int) difficulty
      });
      this.callback = response;
    }

    [Serializable]
    public class RequestParam
    {
      public string area_id;
      public int difficulty;
    }

    [Serializable]
    public class Response
    {
      public int hp;
      public int round;
    }
  }
}
