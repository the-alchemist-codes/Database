﻿// Decompiled with JetBrains decompiler
// Type: SRPG.WeaponFormulaTypes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public enum WeaponFormulaTypes
  {
    None,
    Atk,
    Mag,
    AtkSpd,
    MagSpd,
    AtkDex,
    MagDex,
    AtkLuk,
    MagLuk,
    AtkMag,
    SpAtk,
    SpMag,
    AtkSpdDex,
    MagSpdDex,
    AtkDexLuk,
    MagDexLuk,
    Luk,
    Dex,
    Spd,
    Cri,
    Def,
    Mnd,
    AtkRndLuk,
    MagRndLuk,
    AtkEAt,
    MagEMg,
    AtkDefEDf,
    MagMndEMd,
    LukELk,
    MHp,
    EAt,
    EMg,
    EDx,
    ESp2,
    CtUpEAt,
    CtUpEMg,
    CtLoEAt,
    CtLoEMg,
  }
}
