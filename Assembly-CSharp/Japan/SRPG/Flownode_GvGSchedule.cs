﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Flownode_GvGSchedule
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  [FlowNode.NodeType("GvG/GvGOpen", 32741)]
  [FlowNode.Pin(1, "Start Check", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(101, "GvG Open", FlowNode.PinTypes.Output, 101)]
  [FlowNode.Pin(102, "GvG Close", FlowNode.PinTypes.Output, 102)]
  public class Flownode_GvGSchedule : FlowNode
  {
    private const int PIN_INPUT_CHECK = 1;
    private const int PIN_OUTPUT_OPEN = 101;
    private const int PIN_OUTPUT_CLOSE = 102;

    public override void OnActivate(int pinID)
    {
      if (pinID != 1)
        return;
      if (GvGPeriodParam.GetGvGPeriod() != null)
        this.ActivateOutputLinks(101);
      else
        this.ActivateOutputLinks(102);
    }
  }
}
