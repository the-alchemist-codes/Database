﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ArtifactSetList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System.Collections.Generic;
using UnityEngine;

namespace SRPG
{
  [FlowNode.Pin(100, "アビリティ詳細が開く", FlowNode.PinTypes.Output, 0)]
  public class ArtifactSetList : MonoBehaviour, IFlowInterface
  {
    private const int OUTPUT_ABILITY_DETAIL_OPEN = 100;
    [HeaderBar("▼セット効果のリストの親")]
    [SerializeField]
    private RectTransform m_SetListRoot;
    [HeaderBar("▼セット効果のアイテムのテンプレート")]
    [SerializeField]
    private GameObject m_SetListItemTemplate;
    private ArtifactParam m_ArtifactParam;
    private static ArtifactParam s_SelectedArtifactParam;

    public ArtifactSetList()
    {
      base.\u002Ector();
    }

    public static void SetSelectedArtifactParam(ArtifactParam artifactParam)
    {
      ArtifactSetList.s_SelectedArtifactParam = artifactParam;
    }

    private void Start()
    {
      GameUtility.SetGameObjectActive(this.m_SetListItemTemplate, false);
      this.m_ArtifactParam = ArtifactSetList.s_SelectedArtifactParam;
      this.CreateListItem(MonoSingleton<GameManager>.Instance.MasterParam.FindAllSkillAbilityDeriveDataWithArtifact(this.m_ArtifactParam.iname));
    }

    private void CreateListItem(
      List<SkillAbilityDeriveData> skillAbilityDeriveData)
    {
      foreach (SkillAbilityDeriveData skillAbilityDeriveData1 in skillAbilityDeriveData)
      {
        GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) this.m_SetListItemTemplate);
        ((SkillAbilityDeriveListItem) gameObject.GetComponentInChildren<SkillAbilityDeriveListItem>()).Setup(skillAbilityDeriveData1);
        gameObject.SetActive(true);
        gameObject.get_transform().SetParent((Transform) this.m_SetListRoot, false);
        DataSource.Bind<SkillAbilityDeriveParam>(gameObject, skillAbilityDeriveData1.m_SkillAbilityDeriveParam, false);
      }
    }

    public void Activated(int pinID)
    {
    }

    public void OnAbilityDetailOpen(GameObject go)
    {
      FlowNode_GameObject.ActivateOutputLinks((Component) this, 100);
    }
  }
}
