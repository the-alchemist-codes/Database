﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JobParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using MessagePack;
using System;
using System.Collections;
using System.Collections.Generic;

namespace SRPG
{
  [MessagePackObject(true)]
  public class JobParam
  {
    public static readonly int MAX_JOB_RANK = 11;
    public string[] atkskill = new string[7];
    public StatusParam status = new StatusParam();
    public OInt avoid = (OInt) 0;
    public OInt inimp = (OInt) 0;
    public JobRankParam[] ranks = new JobRankParam[JobParam.MAX_JOB_RANK + 1];
    private BitArray mFlags = new BitArray(2);
    public string iname;
    public string name;
    public string expr;
    public string model;
    public string ac2d;
    public string modelp;
    public string pet;
    public string buki;
    public string origin;
    public JobTypes type;
    public RoleTypes role;
    public OInt mov;
    public OInt jmp;
    public string wepmdl;
    public string artifact;
    public string ai;
    public string master;
    public string fixed_ability;
    public string MapEffectAbility;
    public bool IsMapEffectRevReso;
    public string DescCharacteristic;
    public string DescOther;
    public string unit_image;
    private ArtifactParam m_DefaultArtifact;
    public eMovType MovType;

    public ArtifactParam DefaultArtifact
    {
      get
      {
        return this.m_DefaultArtifact;
      }
    }

    public bool IsRiding
    {
      get
      {
        return this.mFlags.Get(0);
      }
    }

    public bool IsFlyingPass
    {
      get
      {
        return this.mFlags.Get(1);
      }
    }

    public bool Deserialize(JSON_JobParam json, MasterParam master_param)
    {
      if (json == null)
        return false;
      this.iname = json.iname;
      this.name = json.name;
      this.expr = json.expr;
      this.model = json.mdl;
      this.ac2d = json.ac2d;
      this.modelp = json.mdlp;
      this.pet = json.pet;
      this.buki = json.buki;
      this.origin = json.origin;
      this.type = (JobTypes) json.type;
      this.role = (RoleTypes) json.role;
      this.wepmdl = json.wepmdl;
      this.mov = (OInt) json.jmov;
      this.jmp = (OInt) json.jjmp;
      this.atkskill[0] = string.IsNullOrEmpty(json.atkskl) ? string.Empty : json.atkskl;
      this.atkskill[1] = string.IsNullOrEmpty(json.atkfi) ? string.Empty : json.atkfi;
      this.atkskill[2] = string.IsNullOrEmpty(json.atkwa) ? string.Empty : json.atkwa;
      this.atkskill[3] = string.IsNullOrEmpty(json.atkwi) ? string.Empty : json.atkwi;
      this.atkskill[4] = string.IsNullOrEmpty(json.atkth) ? string.Empty : json.atkth;
      this.atkskill[5] = string.IsNullOrEmpty(json.atksh) ? string.Empty : json.atksh;
      this.atkskill[6] = string.IsNullOrEmpty(json.atkda) ? string.Empty : json.atkda;
      this.fixed_ability = json.fixabl;
      this.artifact = json.artifact;
      this.ai = json.ai;
      this.master = json.master;
      this.MapEffectAbility = json.me_abl;
      this.IsMapEffectRevReso = json.is_me_rr != 0;
      this.DescCharacteristic = json.desc_ch;
      this.DescOther = json.desc_ot;
      this.status.hp = (OInt) json.hp;
      this.status.mp = (OShort) json.mp;
      this.status.atk = (OShort) json.atk;
      this.status.def = (OShort) json.def;
      this.status.mag = (OShort) json.mag;
      this.status.mnd = (OShort) json.mnd;
      this.status.dex = (OShort) json.dex;
      this.status.spd = (OShort) json.spd;
      this.status.cri = (OShort) json.cri;
      this.status.luk = (OShort) json.luk;
      this.avoid = (OInt) json.avoid;
      this.inimp = (OInt) json.inimp;
      Array.Clear((Array) this.ranks, 0, this.ranks.Length);
      if (json.ranks != null)
      {
        for (int index = 0; index < json.ranks.Length; ++index)
        {
          this.ranks[index] = new JobRankParam();
          if (!this.ranks[index].Deserialize(json.ranks[index]))
            return false;
        }
      }
      if (master_param != null)
        this.CreateBuffList(master_param);
      this.unit_image = json.unit_image;
      this.MovType = (eMovType) json.mov_type;
      this.mFlags.Set(0, json.is_riding != 0);
      this.mFlags.Set(1, json.no_pass == 0);
      return true;
    }

    private void CreateBuffList(MasterParam master_param)
    {
      for (int index1 = 0; index1 < this.ranks.Length; ++index1)
      {
        if (this.ranks[index1] != null)
        {
          List<BuffEffect.BuffValues> list = new List<BuffEffect.BuffValues>();
          if (this.ranks[index1].equips != null || index1 != this.ranks.Length)
          {
            for (int index2 = 0; index2 < this.ranks[index1].equips.Length; ++index2)
            {
              if (!string.IsNullOrEmpty(this.ranks[index1].equips[index2]))
              {
                ItemParam itemParam = master_param.GetItemParam(this.ranks[index1].equips[index2]);
                if (itemParam != null && !string.IsNullOrEmpty(itemParam.skill))
                {
                  SkillData skillData = new SkillData();
                  skillData.Setup(itemParam.skill, 1, 1, master_param, (ConceptCardEffectDecreaseInfo) null);
                  skillData.BuffSkill(ESkillTiming.Passive, EElement.None, (BaseStatus) null, (BaseStatus) null, (BaseStatus) null, (BaseStatus) null, (BaseStatus) null, (BaseStatus) null, (RandXorshift) null, SkillEffectTargets.Target, false, list, false);
                }
              }
            }
            if (list.Count > 0)
            {
              this.ranks[index1].buff_list = new BuffEffect.BuffValues[list.Count];
              for (int index2 = 0; index2 < list.Count; ++index2)
                this.ranks[index1].buff_list[index2] = list[index2];
            }
          }
        }
      }
    }

    public int GetJobChangeCost(int lv)
    {
      return this.ranks == null || lv < 0 || lv >= this.ranks.Length ? 0 : this.ranks[lv].JobChangeCost;
    }

    public string[] GetJobChangeItems(int lv)
    {
      return this.ranks == null || lv < 0 || lv >= this.ranks.Length ? (string[]) null : this.ranks[lv].JobChangeItems;
    }

    public int[] GetJobChangeItemNums(int lv)
    {
      return this.ranks == null || lv < 0 || lv >= this.ranks.Length ? (int[]) null : this.ranks[lv].JobChangeItemNums;
    }

    public int GetRankupCost(int lv)
    {
      return this.ranks == null || lv < 0 || lv >= this.ranks.Length ? 0 : this.ranks[lv].cost;
    }

    public string[] GetRankupItems(int lv)
    {
      return this.ranks == null || lv < 0 || lv >= this.ranks.Length ? (string[]) null : this.ranks[lv].equips;
    }

    public string GetRankupItemID(int lv, int index)
    {
      string[] rankupItems = this.GetRankupItems(lv);
      return rankupItems != null && 0 <= index && index < rankupItems.Length ? rankupItems[index] : (string) null;
    }

    public OString[] GetLearningAbilitys(int lv)
    {
      return this.ranks == null || lv < 0 || lv >= this.ranks.Length ? (OString[]) null : this.ranks[lv].learnings;
    }

    public int GetJobRankAvoidRate(int lv)
    {
      return this.ranks == null || lv < 0 || lv >= this.ranks.Length ? 0 : (int) this.avoid;
    }

    public int GetJobRankInitJewelRate(int lv)
    {
      return this.ranks == null || lv < 0 || lv >= this.ranks.Length ? 0 : (int) this.inimp;
    }

    public StatusParam GetJobRankStatus(int lv)
    {
      return this.ranks == null || lv < 0 || lv >= this.ranks.Length ? (StatusParam) null : this.status;
    }

    public BaseStatus GetJobTransfarStatus(int lv, EElement element)
    {
      if (this.ranks == null || lv < 0 || lv >= this.ranks.Length)
        return (BaseStatus) null;
      BaseStatus status = new BaseStatus();
      for (int index = 0; index < lv; ++index)
      {
        if (this.ranks[index].buff_list != null)
        {
          foreach (BuffEffect.BuffValues buff in this.ranks[index].buff_list)
          {
            bool flag = false;
            switch (buff.param_type)
            {
              case ParamTypes.UnitDefenseFire:
                flag = element != EElement.Fire;
                break;
              case ParamTypes.UnitDefenseWater:
                flag = element != EElement.Water;
                break;
              case ParamTypes.UnitDefenseWind:
                flag = element != EElement.Wind;
                break;
              case ParamTypes.UnitDefenseThunder:
                flag = element != EElement.Thunder;
                break;
              case ParamTypes.UnitDefenseShine:
                flag = element != EElement.Shine;
                break;
              case ParamTypes.UnitDefenseDark:
                flag = element != EElement.Dark;
                break;
            }
            if (!flag)
              BuffEffect.SetBuffValues(buff.param_type, buff.method_type, ref status, buff.value, (string) null);
          }
        }
      }
      return status;
    }

    public static int GetJobRankCap(int unitRarity)
    {
      RarityParam rarityParam = MonoSingleton<GameManager>.GetInstanceDirect().MasterParam.GetRarityParam(unitRarity);
      return rarityParam != null ? (int) rarityParam.UnitJobLvCap : 1;
    }

    public int FindRankOfAbility(string abilityID)
    {
      if (this.ranks != null)
      {
        for (int index = 0; index < this.ranks.Length; ++index)
        {
          if (this.ranks[index].learnings != null && Array.FindIndex<OString>(this.ranks[index].learnings, (Predicate<OString>) (p => (string) p == abilityID)) != -1)
            return index;
        }
      }
      return -1;
    }

    public List<string> GetAllLearningAbilitys()
    {
      List<string> stringList = new List<string>();
      for (int lv = 1; lv < JobParam.MAX_JOB_RANK + 1; ++lv)
      {
        OString[] learningAbilitys = this.GetLearningAbilitys(lv);
        if (learningAbilitys != null)
        {
          for (int index = 0; index < learningAbilitys.Length; ++index)
            stringList.Add((string) learningAbilitys[index]);
        }
      }
      return stringList;
    }

    public static void UpdateCache(List<JobParam> jobParams)
    {
      GameManager instanceDirect = MonoSingleton<GameManager>.GetInstanceDirect();
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) instanceDirect, (UnityEngine.Object) null) || instanceDirect.MasterParam == null)
        return;
      for (int index = 0; index < jobParams.Count; ++index)
        jobParams[index].UpdateDefaultArtifactCache(instanceDirect.MasterParam);
    }

    private void UpdateDefaultArtifactCache(MasterParam master)
    {
      if (string.IsNullOrEmpty(this.artifact))
        return;
      this.m_DefaultArtifact = master.GetArtifactParam(this.artifact);
    }

    private enum eFlags
    {
      RIDING,
      FLYING_PASS,
      MAX,
    }
  }
}
