﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqRaidGuildStats
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  public class ReqRaidGuildStats : WebAPI
  {
    public ReqRaidGuildStats(int period_id, Network.ResponseCallback response)
    {
      this.name = "raidboss/guild/stats";
      this.body = WebAPI.GetRequestString<ReqRaidGuildStats.RequestParam>(new ReqRaidGuildStats.RequestParam()
      {
        period_id = period_id
      });
      this.callback = response;
    }

    [Serializable]
    public class RequestParam
    {
      public int period_id;
    }

    [Serializable]
    public class Response
    {
      public Json_RaidGuildInfo my_guild_info;
      public JSON_RaidGuildMember[] member;
    }
  }
}
