﻿// Decompiled with JetBrains decompiler
// Type: SRPG.DropItemSource
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class DropItemSource : ContentSource
  {
    public override void Initialize(ContentController controller)
    {
      base.Initialize(controller);
    }

    public override void Release()
    {
      base.Release();
    }

    public class DropItemParam : ContentSource.Param
    {
      private QuestResult.DropItemData mDropItem;

      public DropItemParam(QuestResult.DropItemData drop_item)
      {
        this.mDropItem = drop_item;
      }

      public override bool IsValid()
      {
        return this.mDropItem != null;
      }

      public override void OnEnable(ContentNode node)
      {
        base.OnEnable(node);
        DropItemNode dropItemNode = node as DropItemNode;
        DataSource.Bind<QuestResult.DropItemData>(((Component) dropItemNode.DropItemIcon).get_gameObject(), this.mDropItem, true);
        dropItemNode.DropItemIcon.UpdateValue();
      }
    }
  }
}
