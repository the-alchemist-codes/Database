﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RuneBuffData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class RuneBuffData
  {
    protected float PowerPercentage(ParamTypes param_types, short val, RuneLotteryState lottery)
    {
      if (lottery == null || param_types == ParamTypes.None)
        return 0.0f;
      short num1 = (short) ((int) val - (int) lottery.lot_min + 1);
      short num2 = (short) ((int) lottery.lot_max - (int) lottery.lot_min + 1);
      return num2 != (short) 0 ? (float) num1 / (float) num2 : 1f;
    }

    protected enum StateType
    {
      BaseState,
      EvoState,
    }
  }
}
