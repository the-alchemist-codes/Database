﻿// Decompiled with JetBrains decompiler
// Type: SRPG.PlayerCoinBuyUseBonusState
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class PlayerCoinBuyUseBonusState
  {
    public string iname;
    public int total;

    public void Deserialize(JSON_PlayerCoinBuyUseBonusState bonus_state)
    {
      if (bonus_state == null)
        return;
      this.iname = bonus_state.iname;
      this.total = bonus_state.total;
    }
  }
}
