﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ToggledPulldownItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class ToggledPulldownItem : PulldownItem
  {
    public GameObject imageOn;
    public GameObject imageOff;

    public override void OnStatusChanged(bool enabled)
    {
      if (Object.op_Inequality((Object) this.imageOn, (Object) null))
        this.imageOn.SetActive(enabled);
      if (!Object.op_Inequality((Object) this.imageOff, (Object) null))
        return;
      this.imageOff.SetActive(!enabled);
    }
  }
}
