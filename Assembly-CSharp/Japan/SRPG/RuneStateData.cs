﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RuneStateData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;
using System.Collections.Generic;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class RuneStateData
  {
    public RuneBuffDataBaseState base_state = new RuneBuffDataBaseState();
    public List<RuneBuffDataEvoState> evo_state = new List<RuneBuffDataEvoState>();

    public bool Deserialize(Json_RuneStateData json)
    {
      if (json == null)
        return false;
      this.base_state.Deserialize(json.basic);
      if (json.evo != null)
      {
        this.evo_state.Clear();
        foreach (Json_RuneBuffData json1 in json.evo)
        {
          RuneBuffDataEvoState buffDataEvoState = new RuneBuffDataEvoState();
          buffDataEvoState.Deserialize(json1);
          this.evo_state.Add(buffDataEvoState);
        }
        this.evo_state.Sort(new Comparison<RuneBuffDataEvoState>(RuneStateData.Compare));
      }
      return true;
    }

    private static int Compare(RuneBuffDataEvoState a, RuneBuffDataEvoState b)
    {
      return (int) a.slot - (int) b.slot;
    }

    public Json_RuneStateData Serialize()
    {
      Json_RuneStateData jsonRuneStateData = new Json_RuneStateData();
      jsonRuneStateData.basic = this.base_state.Serialize();
      if (this.evo_state != null && this.evo_state.Count > 0)
      {
        jsonRuneStateData.evo = new Json_RuneBuffData[this.evo_state.Count];
        for (int index = 0; index < this.evo_state.Count; ++index)
          jsonRuneStateData.evo[index] = this.evo_state[index].Serialize();
      }
      return jsonRuneStateData;
    }
  }
}
