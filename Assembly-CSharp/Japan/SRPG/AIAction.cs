﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AIAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;

namespace SRPG
{
  [MessagePackObject(true)]
  public class AIAction
  {
    public OString skill;
    public OInt type;
    public OInt turn;
    public OBool notBlock;
    public eAIActionNoExecAct noExecAct;
    public int nextActIdx;
    public eAIActionNextTurnAct nextTurnAct;
    public int turnActIdx;
    public SkillLockCondition cond;
  }
}
