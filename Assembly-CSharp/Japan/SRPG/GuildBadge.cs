﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildBadge
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;

namespace SRPG
{
  public class GuildBadge : MonoBehaviour
  {
    [SerializeField]
    private GameObject mBadge;

    public GuildBadge()
    {
      base.\u002Ector();
    }

    private void Start()
    {
      if (!Object.op_Inequality((Object) this.mBadge, (Object) null))
        return;
      this.mBadge.SetActive(false);
    }

    private void Update()
    {
      if (Object.op_Equality((Object) this.mBadge, (Object) null))
        return;
      if (MonoSingleton<GameManager>.Instance.Player.PlayerGuild == null || !MonoSingleton<GameManager>.Instance.Player.PlayerGuild.IsJoined)
        this.mBadge.SetActive(false);
      else if (!MonoSingleton<GameManager>.Instance.Player.PlayerGuild.IsGuildMaster && !MonoSingleton<GameManager>.Instance.Player.PlayerGuild.IsSubGuildMaster)
      {
        this.mBadge.SetActive(false);
      }
      else
      {
        bool flag = false;
        if (GuildManager.NotifyEntryRequestCount > 0)
          flag = true;
        if (!flag)
          flag = MonoSingleton<GameManager>.Instance.Player.HasGuildReward;
        this.mBadge.SetActive(flag);
      }
    }
  }
}
