﻿// Decompiled with JetBrains decompiler
// Type: SRPG.TobiraCategoriesParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class TobiraCategoriesParam
  {
    private TobiraParam.Category mCategory;
    private string mName;

    public TobiraParam.Category TobiraCategory
    {
      get
      {
        return this.mCategory;
      }
    }

    public string Name
    {
      get
      {
        return this.mName;
      }
    }

    public void Deserialize(JSON_TobiraCategoriesParam json)
    {
      if (json == null)
        return;
      this.mCategory = (TobiraParam.Category) json.category;
      this.mName = json.name;
    }
  }
}
