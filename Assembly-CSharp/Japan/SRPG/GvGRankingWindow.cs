﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GvGRankingWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;
using UnityEngine;

namespace SRPG
{
  [FlowNode.Pin(10, "ランキング初期化", FlowNode.PinTypes.Input, 10)]
  [FlowNode.Pin(11, "撃破ランキング初期化", FlowNode.PinTypes.Input, 11)]
  [FlowNode.Pin(100, "撃破ランキング切り替え", FlowNode.PinTypes.Output, 100)]
  public class GvGRankingWindow : MonoBehaviour, IFlowInterface
  {
    private const int PIN_INPUT_INIT = 10;
    private const int PIN_INPUT_BEATINIT = 11;
    private const int PIN_OUTPUT_BEATCHANGE = 100;
    private static GvGRankingWindow mInstance;
    [SerializeField]
    private GameObject mRankingRoot;
    [SerializeField]
    private GameObject mBeatRankingRoot;
    [SerializeField]
    private GvGRankingContent mRankingTemplate;
    [SerializeField]
    private GvGBeatRankingWindowContent mBeatRankingTemplate;
    [SerializeField]
    private GameObject mRankingTab;
    [SerializeField]
    private GameObject mBeatRankingTab;
    private GvGRankingData mGvGRanking;
    private GameObject SelectTab;
    private List<GameObject> beatList;
    private const int DEFAULT_PAGEINIT = 1;

    public GvGRankingWindow()
    {
      base.\u002Ector();
    }

    public static GvGRankingWindow Instance
    {
      get
      {
        return GvGRankingWindow.mInstance;
      }
    }

    public int mBeatRankingPage { get; private set; }

    private void Awake()
    {
      GvGRankingWindow.mInstance = this;
      GameUtility.SetGameObjectActive(this.mRankingRoot, false);
      GameUtility.SetGameObjectActive(this.mBeatRankingRoot, false);
      GameUtility.SetGameObjectActive((Component) this.mRankingTemplate, false);
      GameUtility.SetGameObjectActive((Component) this.mBeatRankingTemplate, false);
      this.mBeatRankingPage = 1;
    }

    private void OnDestroy()
    {
      GvGRankingWindow.mInstance = (GvGRankingWindow) null;
    }

    public void Activated(int pinID)
    {
      if (pinID != 10)
      {
        if (pinID != 11)
          return;
        this.CreateBeatRanking();
      }
      else
        this.Init();
    }

    private void Init()
    {
      this.CreateRanking();
      GameUtility.SetGameObjectActive(this.mRankingRoot, true);
    }

    public void OnSelectTab(GameObject go)
    {
      if (Object.op_Equality((Object) go, (Object) this.SelectTab))
        return;
      this.SelectTab = go;
      if (Object.op_Equality((Object) this.SelectTab, (Object) this.mRankingTab))
      {
        GameUtility.SetGameObjectActive(this.mRankingRoot, true);
        GameUtility.SetGameObjectActive(this.mBeatRankingRoot, false);
      }
      else
      {
        if (!Object.op_Equality((Object) this.SelectTab, (Object) this.mBeatRankingTab))
          return;
        FlowNode_GameObject.ActivateOutputLinks((Component) this, 100);
      }
    }

    private void CreateRanking()
    {
      if (this.mGvGRanking == null || Object.op_Equality((Object) this.mRankingTemplate, (Object) null))
        return;
      GameUtility.SetGameObjectActive((Component) this.mRankingTemplate, false);
      for (int index = 0; index < this.mGvGRanking.Guilds.Count; ++index)
      {
        if (this.mGvGRanking.Guilds[index] != null)
        {
          GvGRankingContent gvGrankingContent = (GvGRankingContent) Object.Instantiate<GvGRankingContent>((M0) this.mRankingTemplate, ((Component) this.mRankingTemplate).get_transform().get_parent(), false);
          gvGrankingContent.Setup(this.mGvGRanking.Guilds[index]);
          ((Component) gvGrankingContent).get_gameObject().SetActive(true);
        }
      }
      GameParameter.UpdateAll(((Component) ((Component) this.mRankingTemplate).get_transform().get_parent()).get_gameObject());
    }

    private void CreateBeatRanking()
    {
      GameUtility.SetGameObjectActive(this.mRankingRoot, false);
      GameUtility.SetGameObjectActive(this.mBeatRankingRoot, true);
      if (this.mGvGRanking == null || Object.op_Equality((Object) this.mBeatRankingTemplate, (Object) null))
        return;
      GameUtility.SetGameObjectActive((Component) this.mBeatRankingTemplate, false);
      if (this.beatList == null)
        this.beatList = new List<GameObject>();
      for (int index = 0; index < this.beatList.Count; ++index)
        Object.Destroy((Object) this.beatList[index]);
      this.beatList.Clear();
      for (int index = 0; index < this.mGvGRanking.Beats.Count; ++index)
      {
        if (this.mGvGRanking.Beats[index] != null)
        {
          GvGBeatRankingWindowContent rankingWindowContent = (GvGBeatRankingWindowContent) Object.Instantiate<GvGBeatRankingWindowContent>((M0) this.mBeatRankingTemplate, ((Component) this.mBeatRankingTemplate).get_transform().get_parent(), false);
          if (!Object.op_Equality((Object) rankingWindowContent, (Object) null))
          {
            this.beatList.Add(((Component) rankingWindowContent).get_gameObject());
            DataSource.Bind<GvGBeatRankingData>(((Component) rankingWindowContent).get_gameObject(), this.mGvGRanking.Beats[index], false);
            rankingWindowContent.Setup(this.mGvGRanking.Beats[index], this.mBeatRankingPage);
            ((Component) rankingWindowContent).get_gameObject().SetActive(true);
          }
        }
      }
      GameParameter.UpdateAll(((Component) ((Component) this.mRankingTemplate).get_transform().get_parent()).get_gameObject());
    }

    public void SetRankingData(JSON_GvGRankingData[] guilds)
    {
      if (this.mGvGRanking == null)
        this.mGvGRanking = new GvGRankingData();
      this.mGvGRanking.Deserialize(guilds);
    }

    public void SetBeatRankingData(JSON_GvGBeatRanking[] beats, int page)
    {
      if (this.mGvGRanking == null)
        this.mGvGRanking = new GvGRankingData();
      this.mGvGRanking.Deserialize(beats);
      if (this.mBeatRankingPage < page)
        return;
      this.mBeatRankingPage = 1;
    }
  }
}
