﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqUnitPieceShopItemList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using Gsc.Network.Encoding;
using MessagePack;
using System;

namespace SRPG
{
  public class ReqUnitPieceShopItemList : WebAPI
  {
    public ReqUnitPieceShopItemList(
      SRPG.Network.ResponseCallback response,
      EncodingTypes.ESerializeCompressMethod compress)
    {
      this.name = "shop/unitpiece";
      this.body = WebAPI.GetRequestString(string.Empty);
      this.callback = response;
      this.serializeCompressMethod = compress;
    }

    [MessagePackObject(true)]
    [Serializable]
    public class Response
    {
      public string shop_iname;
      public string cost_iname;
      public Json_UnitPieceShopItem[] shopitems;
    }
  }
}
