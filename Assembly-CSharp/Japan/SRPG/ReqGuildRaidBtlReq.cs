﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqGuildRaidBtlReq
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  public class ReqGuildRaidBtlReq : WebAPI
  {
    public ReqGuildRaidBtlReq(
      int gid,
      int round,
      int boss_id,
      GuildRaidBattleType battle_type,
      int unit_strength_total,
      Network.ResponseCallback response)
    {
      this.name = "guildraid/btl/req";
      this.body = WebAPI.GetRequestString<ReqGuildRaidBtlReq.RequestParam>(new ReqGuildRaidBtlReq.RequestParam()
      {
        gid = gid,
        round = round,
        boss_id = boss_id,
        battle_type = (int) battle_type,
        unit_strength_total = unit_strength_total
      });
      this.callback = response;
    }

    [Serializable]
    public class RequestParam
    {
      public int gid;
      public int round;
      public int boss_id;
      public int battle_type;
      public int unit_strength_total;
    }
  }
}
