﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RaidGuildInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class RaidGuildInfo
  {
    private RaidGuildInfoData mBeat;
    private RaidGuildInfoData mRescue;

    public RaidGuildInfoData Beat
    {
      get
      {
        return this.mBeat;
      }
    }

    public RaidGuildInfoData Rescue
    {
      get
      {
        return this.mRescue;
      }
    }

    public bool Deserialize(Json_RaidGuildInfo json)
    {
      this.mBeat = new RaidGuildInfoData();
      if (json.beat != null)
        this.mBeat.Deserialize(json.beat);
      this.mRescue = new RaidGuildInfoData();
      if (json.rescue != null)
        this.mRescue.Deserialize(json.rescue);
      return true;
    }
  }
}
