﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqLimitedShopList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class ReqLimitedShopList : WebAPI
  {
    public ReqLimitedShopList(
      Network.ResponseCallback response,
      ReqLimitedShopList.eShopType shop_type = ReqLimitedShopList.eShopType.Limited)
    {
      this.name = "shop/limited/shoplist";
      if (shop_type == ReqLimitedShopList.eShopType.Port)
        this.body = WebAPI.GetRequestString("\"is_port_shop\":1");
      else
        this.body = WebAPI.GetRequestString((string) null);
      this.callback = response;
    }

    public enum eShopType
    {
      Limited,
      Port,
    }
  }
}
