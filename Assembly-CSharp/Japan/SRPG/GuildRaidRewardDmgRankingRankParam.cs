﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildRaidRewardDmgRankingRankParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class GuildRaidRewardDmgRankingRankParam
  {
    public int RankStart { get; private set; }

    public int RankEnd { get; private set; }

    public string RewardRoundId { get; private set; }

    public bool Deserialize(JSON_GuildRaidRewardDmgRankingRankParam json)
    {
      this.RankStart = json.rank_start;
      this.RankEnd = json.rank_end;
      this.RewardRoundId = json.reward_round_id;
      return true;
    }
  }
}
