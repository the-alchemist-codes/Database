﻿// Decompiled with JetBrains decompiler
// Type: SRPG.SupportIcon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;

namespace SRPG
{
  public class SupportIcon : UnitIcon
  {
    [SerializeField]
    private GameObject ConceptCardLSEnableEffect;
    private const string TooltipPath = "UI/SupportTooltip";
    public bool UseSelection;

    public override void UpdateValue()
    {
      base.UpdateValue();
      bool flag1 = false;
      UnitData unitDataByUniqueId = MonoSingleton<GameManager>.Instance.Player.FindUnitDataByUniqueID((long) GlobalVars.SelectedLSChangeUnitUniqueID);
      SupportData supportData = this.GetSupportData();
      ConceptCardData conceptCardData1 = unitDataByUniqueId == null ? (ConceptCardData) null : unitDataByUniqueId.MainConceptCard;
      ConceptCardData conceptCardData2 = supportData == null || supportData.Unit == null ? (ConceptCardData) null : supportData.Unit.MainConceptCard;
      if (unitDataByUniqueId != null && conceptCardData1 != null && (unitDataByUniqueId.IsEquipConceptLeaderSkill() && conceptCardData1.Param.concept_card_groups != null) && (supportData != null && supportData.Unit != null && (supportData.IsFriend() && conceptCardData2 != null)))
      {
        bool flag2 = false;
        for (int index = 0; index < conceptCardData1.Param.concept_card_groups.Length; ++index)
          flag2 |= MonoSingleton<GameManager>.Instance.MasterParam.CheckConceptCardgroup(conceptCardData1.Param.concept_card_groups[index], conceptCardData2.Param);
        if (flag2)
          flag1 = true;
      }
      if (!Object.op_Inequality((Object) this.ConceptCardLSEnableEffect, (Object) null))
        return;
      this.ConceptCardLSEnableEffect.SetActive(flag1);
    }

    private SupportData GetSupportData()
    {
      return this.UseSelection ? (SupportData) GlobalVars.SelectedSupport : DataSource.FindDataOfClass<SupportData>(((Component) this).get_gameObject(), (SupportData) null);
    }

    protected override UnitData GetInstanceData()
    {
      SupportData supportData = this.GetSupportData();
      return supportData == null || supportData.Unit == null ? (UnitData) null : supportData.Unit;
    }

    protected override void ShowTooltip(Vector2 screen)
    {
      if (!this.Tooltip)
        return;
      SupportData supportData = this.GetSupportData();
      if (supportData == null || supportData.Unit == null)
        return;
      GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) AssetManager.Load<GameObject>("UI/SupportTooltip"));
      DataSource.Bind<UnitData>(gameObject, supportData.Unit, false);
      DataSource.Bind<SupportData>(gameObject, supportData, false);
    }
  }
}
