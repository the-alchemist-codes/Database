﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqConvertPiece
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Text;

namespace SRPG
{
  public class ReqConvertPiece : WebAPI
  {
    public ReqConvertPiece(
      string consume,
      string unit,
      int pieceNum,
      Network.ResponseCallback response)
    {
      this.name = "shop/piece/convert";
      StringBuilder stringBuilder = WebAPI.GetStringBuilder();
      stringBuilder.Append("\"convert_piece\":{");
      stringBuilder.Append("\"consume_piece_iname\":\"");
      stringBuilder.Append(consume);
      stringBuilder.Append("\",");
      stringBuilder.Append("\"convert_piece_iname\":\"");
      stringBuilder.Append(unit);
      stringBuilder.Append("\",");
      stringBuilder.Append("\"convert_piece_num\":");
      stringBuilder.Append(pieceNum);
      stringBuilder.Append("}");
      this.body = WebAPI.GetRequestString(stringBuilder.ToString());
      this.callback = response;
    }
  }
}
