﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_MultiPlayAutoEntryRoom
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  [FlowNode.NodeType("Multi/AutoEntryRoom", 32741)]
  [FlowNode.Pin(1, "開始", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(101, "自動入室に成功", FlowNode.PinTypes.Output, 101)]
  [FlowNode.Pin(102, "自動入室に失敗", FlowNode.PinTypes.Output, 102)]
  public class FlowNode_MultiPlayAutoEntryRoom : FlowNode
  {
    private const int PIN_INPUT_START = 1;
    private const int PIN_OUTPUT_SUCCESS = 101;
    private const int PIN_OUTPUT_FAILED = 102;

    public override void OnActivate(int pinID)
    {
      if (pinID != 1)
        return;
      this.AutoEntry();
    }

    private void AutoEntry()
    {
      MyPhoton instance = PunMonoSingleton<MyPhoton>.Instance;
      if (Object.op_Inequality((Object) instance, (Object) null) && instance.IsConnectedInRoom())
        this.ActivateOutputLinks(101);
      else
        this.ActivateOutputLinks(102);
    }
  }
}
