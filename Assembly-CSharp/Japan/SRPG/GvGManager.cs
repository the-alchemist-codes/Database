﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GvGManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

namespace SRPG
{
  [FlowNode.Pin(1, "Initialize", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(2, "Refresh", FlowNode.PinTypes.Input, 2)]
  [FlowNode.Pin(10, "Open Chat", FlowNode.PinTypes.Input, 10)]
  [FlowNode.Pin(100, "Initialized", FlowNode.PinTypes.Output, 100)]
  [FlowNode.Pin(101, "Selected Node", FlowNode.PinTypes.Output, 101)]
  [FlowNode.Pin(102, "Restore Battle", FlowNode.PinTypes.Output, 102)]
  [FlowNode.Pin(103, "Start Anime", FlowNode.PinTypes.Output, 103)]
  [FlowNode.Pin(111, "Occupied Node", FlowNode.PinTypes.Output, 111)]
  [FlowNode.Pin(112, "Select Refresh", FlowNode.PinTypes.Output, 112)]
  [FlowNode.Pin(901, "Error", FlowNode.PinTypes.Output, 901)]
  public class GvGManager : MonoBehaviour, IFlowInterface, IWebHelp
  {
    public const int PIN_INPUT_INIT = 1;
    public const int PIN_INPUT_REFRESH = 2;
    public const int PIN_INPUT_OPEN_CHAT = 10;
    public const int PIN_OUTPUT_INITALIZED = 100;
    public const int PIN_OUTPUT_SELECTED_NODE = 101;
    public const int PIN_OUTPUT_RESTORE_BATTLE = 102;
    public const int PIN_OUTPUT_STARTANIME = 103;
    public const int PIN_OUTPUT_OCCUPIED_NODE = 111;
    public const int PIN_OUTPUT_SELAUTOREFRESH = 112;
    public const int PIN_OUTPUT_ERROR = 901;
    [SerializeField]
    private GvGMapAssets GvGMapAssetsData;
    [SerializeField]
    private Transform BgParent;
    [SerializeField]
    private List<int> PartyCountImageBorder;
    [SerializeField]
    private bool IsHideChat;
    [SerializeField]
    private int OnePageDefensePartyCountMax;
    public const int PARTY_UNIT_NUM_MAX = 3;
    private static GvGManager mInstance;
    private DateTime mCanRefreshTime;
    private DateTime mCanAutoRefreshTime;
    private GvGManager.GvGAutoRefreshState AutoRefreshStatusOld;
    private GvGMap mGvGMap;
    private LoadRequest mResourceRequest;
    public static int GvGGroupId;
    private GvGRuleParam CacheCurrentRule;
    private int CacheRuleIndex;
    private int CacheRulePeriodId;
    public bool IsExecAutoRefresh;

    public GvGManager()
    {
      base.\u002Ector();
    }

    public int ONE_PAGE_DEFENSE_PARTY_COUNT_MAX
    {
      get
      {
        return this.OnePageDefensePartyCountMax;
      }
    }

    public static GvGManager Instance
    {
      get
      {
        return GvGManager.mInstance;
      }
    }

    public GvGPeriodParam GvGPeriod { get; private set; }

    public GvGManager.GvGAutoRefreshState AutoRefreshStatus { get; private set; }

    public RewardData ResultReward { get; private set; }

    public bool IsPrivilege { get; private set; }

    public int SelectNodeId { get; set; }

    public bool CanRefresh
    {
      get
      {
        return TimeManager.ServerTime.Ticks <= this.mCanRefreshTime.Ticks;
      }
    }

    public bool CanAutoRefresh
    {
      get
      {
        return TimeManager.ServerTime.Ticks >= this.mCanAutoRefreshTime.Ticks;
      }
    }

    public GvGManager.GvGStatus GvGStatusPhase
    {
      get
      {
        if (this.GvGPeriod == null)
          return GvGManager.GvGStatus.Finished;
        DateTime serverTime = TimeManager.ServerTime;
        if (serverTime < this.GvGPeriod.BeginAt || this.GvGPeriod.EndAt <= serverTime)
          return GvGManager.GvGStatus.Finished;
        DateTime result1;
        DateTime.TryParse(serverTime.ToString("yyyy/MM/dd ") + this.GvGPeriod.DeclarationStartTime, out result1);
        DateTime result2;
        DateTime.TryParse(serverTime.ToString("yyyy/MM/dd ") + this.GvGPeriod.DeclarationEndTime, out result2);
        if (result1 <= serverTime && serverTime < result2 || result2 < result1 && (result2 > serverTime || serverTime >= result1))
          return GvGManager.GvGStatus.Declaration;
        DateTime result3;
        DateTime.TryParse(serverTime.ToString("yyyy/MM/dd ") + this.GvGPeriod.BattleStartTime, out result3);
        DateTime result4;
        DateTime.TryParse(serverTime.ToString("yyyy/MM/dd ") + this.GvGPeriod.BattleEndTime, out result4);
        if (result3 <= serverTime && serverTime < result4 || result4 < result3 && (result4 > serverTime || serverTime >= result3))
          return GvGManager.GvGStatus.Offense;
        DateTime result5;
        DateTime.TryParse(serverTime.ToString("yyyy/MM/dd ") + this.GvGPeriod.DeclarationEndTime, out result5);
        DateTime result6;
        DateTime.TryParse(serverTime.ToString("yyyy/MM/dd ") + this.GvGPeriod.BattleStartTime, out result6);
        return result5 <= serverTime && serverTime < result6 || result4 < result3 && (result4 > serverTime || serverTime >= result3) ? GvGManager.GvGStatus.DeclarationCoolTime : GvGManager.GvGStatus.OffenseCoolTime;
      }
    }

    public GvGManager.GvGStatus GvGStatusPhaseSetPriod()
    {
      if (this.GvGPeriod != null)
        return this.GvGStatusPhase;
      this.GvGPeriod = GvGPeriodParam.GetGvGPeriod();
      GvGManager.GvGStatus gvGstatusPhase = this.GvGStatusPhase;
      this.GvGPeriod = (GvGPeriodParam) null;
      return gvGstatusPhase;
    }

    public GvGRuleParam CurrentRule
    {
      get
      {
        if (this.GvGPeriod == null || this.GvGPeriod.RuleCycle == null || (this.GvGPeriod.RuleCycle.Count <= 0 || MonoSingleton<GameManager>.Instance.mGvGRuleParam == null))
          return (GvGRuleParam) null;
        int index = 0;
        DateTime date1 = TimeManager.ServerTime.Date;
        DateTime date2 = this.GvGPeriod.BeginAt.Date;
        if (date1 >= date2)
          index = (date1 - date2).Days % this.GvGPeriod.RuleCycle.Count;
        if (this.GvGPeriod.RuleCycle.Count <= index)
          return (GvGRuleParam) null;
        if (this.CacheRuleIndex == index && this.CacheRulePeriodId == this.GvGPeriod.Id && this.CacheCurrentRule != null)
          return this.CacheCurrentRule;
        string rule_iname = this.GvGPeriod.RuleCycle[index];
        this.CacheRuleIndex = index;
        this.CacheRulePeriodId = this.GvGPeriod.Id;
        this.CacheCurrentRule = MonoSingleton<GameManager>.Instance.mGvGRuleParam.Find((Predicate<GvGRuleParam>) (rule => rule.Iname == rule_iname));
        return this.CacheCurrentRule;
      }
    }

    public int CurrentRuleUnitCount
    {
      get
      {
        GvGRuleParam currentRule = this.CurrentRule;
        if (currentRule == null)
          return 0;
        return this.GvGStatusPhase == GvGManager.GvGStatus.Offense ? currentRule.UnitCount : currentRule.DefUnitCount;
      }
    }

    public int RemainDeclareCount { get; set; }

    public List<GvGNodeData> NodeDataList { get; private set; }

    public List<int> MatchingOrder { get; private set; }

    public List<long> UsedUnitList { get; private set; }

    public List<ViewGuildData> OtherGuildList { get; private set; }

    public ViewGuildData MyGuild { get; private set; }

    public GvGResultData ResultDaily { get; private set; }

    public GvGResultData ResultSeason { get; private set; }

    public bool IsPeriod()
    {
      return GvGPeriodParam.GetGvGPeriod() != null;
    }

    public ViewGuildData FindViewGuild(int guildId)
    {
      return this.MyGuild == null || GvGManager.Instance.MyGuild.id != guildId ? this.OtherGuildList.Find((Predicate<ViewGuildData>) (g => g.id == guildId)) : this.MyGuild;
    }

    private void Awake()
    {
      GvGManager.mInstance = this;
      this.IsExecAutoRefresh = true;
    }

    private void OnDestroy()
    {
      GvGManager.mInstance = (GvGManager) null;
    }

    private void Update()
    {
      if (!this.CanAutoRefresh || this.AutoRefreshStatus != GvGManager.GvGAutoRefreshState.Top || (this.IsExecAutoRefresh || Network.IsConnecting))
        return;
      FlowNode_GameObject.ActivateOutputLinks((Component) this, 112);
      this.IsExecAutoRefresh = true;
    }

    public void Activated(int pinID)
    {
      switch (pinID)
      {
        case 1:
          this.Initialize();
          this.Refresh(false);
          FlowNode_GameObject.ActivateOutputLinks((Component) this, 100);
          this.IsExecAutoRefresh = false;
          break;
        case 2:
          this.Refresh(this.IsExecAutoRefresh);
          this.IsExecAutoRefresh = false;
          break;
        case 10:
          this.OpenChat();
          break;
      }
    }

    private void Initialize()
    {
      if (this.IsHideChat)
        this.HideChatButton(false);
      this.AutoRefreshStatus = GvGManager.GvGAutoRefreshState.Top;
      if (HomeWindow.GetRestorePoint() == RestorePoints.GVG)
        return;
      FlowNode_GameObject.ActivateOutputLinks((Component) this, 103);
    }

    private void Refresh(bool isAuto = false)
    {
      this.GvGPeriod = GvGPeriodParam.GetGvGPeriod();
      this.IsExecAutoRefresh = false;
      if (this.GvGPeriod == null)
      {
        this.GvGPeriod = GvGPeriodParam.GetGvGNearPeriod();
        if (this.GvGPeriod == null)
        {
          Debug.LogError((object) "GvgPeriod is nothing.");
          return;
        }
      }
      GameManager instance = MonoSingleton<GameManager>.Instance;
      if (instance.Player.PlayerGuild != null && (instance.Player.PlayerGuild.IsGuildMaster || instance.Player.PlayerGuild.IsSubGuildMaster))
        this.IsPrivilege = true;
      if (!isAuto)
        this.SelectNodeId = 0;
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) this.BgParent, (UnityEngine.Object) null))
        Debug.LogError((object) "MapParent is nothing.");
      else if (UnityEngine.Object.op_Equality((UnityEngine.Object) this.GvGMapAssetsData, (UnityEngine.Object) null) || this.GvGMapAssetsData.GvGMaps == null || this.GvGMapAssetsData.GvGMaps.Length < this.GvGPeriod.MapIndex + 1)
      {
        Debug.LogError((object) "GvGMapAssetsData is nothing.");
      }
      else
      {
        this.StartCoroutine(this.DownloadMap());
        GvGInfo component = (GvGInfo) ((Component) this).get_gameObject().GetComponent<GvGInfo>();
        if (!UnityEngine.Object.op_Inequality((UnityEngine.Object) component, (UnityEngine.Object) null))
          return;
        component.Refresh();
      }
    }

    public void OpenNodeInfo(int nodeId)
    {
      this.SelectNodeId = nodeId;
      GvGMap.Instance.NodeAutoForcus(nodeId);
      FlowNode_GameObject.ActivateOutputLinks((Component) this, 101);
    }

    public bool CanDeclareNode(GvGNodeData node)
    {
      return this.IsPrivilege && node.CanDeclare && (this.GvGStatusPhase == GvGManager.GvGStatus.Declaration && this.RemainDeclareCount > 0) && (node.NodeParam.AdjacentNode.FindIndex((Predicate<int>) (n => this.NodeDataList.Find((Predicate<GvGNodeData>) (nd => nd.NodeId == n && nd.GuildId == MonoSingleton<GameManager>.Instance.Player.PlayerGuild.Gid)) != null)) != -1 || node.NodeParam.Rank <= 1 && this.NodeDataList.Find((Predicate<GvGNodeData>) (nd => nd.GuildId == MonoSingleton<GameManager>.Instance.Player.PlayerGuild.Gid)) == null);
    }

    public bool CanAttackNode(GvGNodeData node)
    {
      return node.State == GvGNodeState.DeclareSelf && !node.IsAttackWait && this.GvGStatusPhase == GvGManager.GvGStatus.Offense;
    }

    public GvGManager.WindowBGImageIndex GetNodeImageIndex(GvGNodeData node)
    {
      if (node == null)
        return GvGManager.WindowBGImageIndex.Purple;
      switch (node.State)
      {
        case GvGNodeState.OccupySelf:
        case GvGNodeState.DeclaredEnemy:
          return GvGManager.WindowBGImageIndex.Blue;
        case GvGNodeState.DeclareSelf:
          return GvGManager.WindowBGImageIndex.Red;
        default:
          return GvGManager.WindowBGImageIndex.Purple;
      }
    }

    public GvGManager.WindowBGImageIndex GetNodeBattleImageIndex(
      GvGNodeData node,
      bool isAttack)
    {
      if (node == null)
        return GvGManager.WindowBGImageIndex.Purple;
      ViewGuildData viewGuild1 = this.FindViewGuild(node.DeclaredGuildId);
      ViewGuildData viewGuild2 = this.FindViewGuild(node.GuildId);
      if (viewGuild1 != null && viewGuild1.id == MonoSingleton<GameManager>.Instance.Player.PlayerGuild.Gid)
        return isAttack ? GvGManager.WindowBGImageIndex.Blue : GvGManager.WindowBGImageIndex.Red;
      if (viewGuild2 == null || viewGuild2.id != MonoSingleton<GameManager>.Instance.Player.PlayerGuild.Gid)
        return GvGManager.WindowBGImageIndex.Purple;
      return isAttack ? GvGManager.WindowBGImageIndex.Red : GvGManager.WindowBGImageIndex.Blue;
    }

    public int GetNodeBattleGuildImageIndex(GvGNodeData node, bool isAttack)
    {
      if (node == null)
      {
        Debug.LogError((object) "GetNodeBattleGuildImageIndex:Node Not Found:");
        return 0;
      }
      ViewGuildData viewGuild1 = this.FindViewGuild(node.DeclaredGuildId);
      ViewGuildData viewGuild2 = this.FindViewGuild(node.GuildId);
      if (viewGuild1 != null && viewGuild1.id == MonoSingleton<GameManager>.Instance.Player.PlayerGuild.Gid && isAttack || viewGuild2 != null && viewGuild2.id == MonoSingleton<GameManager>.Instance.Player.PlayerGuild.Gid && !isAttack)
        return 0;
      int gid = 0;
      if (viewGuild2 != null)
        gid = viewGuild2.id;
      if (isAttack && viewGuild1 != null)
        gid = viewGuild1.id;
      return this.GetMatchingOrderIndex(gid);
    }

    public int GetDefensePartyIconIndex(GvGNodeData node)
    {
      if (this.PartyCountImageBorder.Count == 0)
        return 0;
      for (int index = 0; index < this.PartyCountImageBorder.Count; ++index)
      {
        if (this.PartyCountImageBorder[index] > node.DefensePartyNum)
          return index;
      }
      return this.PartyCountImageBorder.Count - 1;
    }

    private void OpenChat()
    {
      if (!UnityEngine.Object.op_Inequality((UnityEngine.Object) MonoSingleton<ChatWindow>.Instance, (UnityEngine.Object) null))
        return;
      MonoSingleton<ChatWindow>.Instance.ChangeChatTypeTab(ChatWindow.eChatType.Guild);
      MonoSingleton<ChatWindow>.Instance.Open();
    }

    public void HideChatButton(bool flag = false)
    {
      if (!UnityEngine.Object.op_Inequality((UnityEngine.Object) MonoSingleton<ChatWindow>.Instance, (UnityEngine.Object) null))
        return;
      MonoSingleton<ChatWindow>.Instance.SetActiveOpenCloseButton(flag);
    }

    [DebuggerHidden]
    private IEnumerator DownloadMap()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new GvGManager.\u003CDownloadMap\u003Ec__Iterator0()
      {
        \u0024this = this
      };
    }

    public bool GetHelpURL(out string url, out string title)
    {
      url = string.Empty;
      title = string.Empty;
      GvGPeriodParam gvGperiod = GvGPeriodParam.GetGvGPeriod();
      if (gvGperiod == null)
        return false;
      url = gvGperiod.URL;
      title = gvGperiod.URL_TITLE;
      return true;
    }

    public int GetMatchingOrderIndex(int gid)
    {
      if (this.MatchingOrder == null || this.MatchingOrder.Count == 0)
        return 0;
      if (gid == 0)
        return this.MatchingOrder.Count - 1;
      int num = 0;
      for (int index = 0; index < this.MatchingOrder.Count; ++index)
      {
        if (this.MatchingOrder[index] != this.MyGuild.id)
        {
          if (this.MatchingOrder[index] == gid)
            return num + 1;
          ++num;
        }
      }
      Debug.LogError((object) ("Matching Not Find:" + gid.ToString()));
      return this.MatchingOrder.Count - 1;
    }

    public void SetAutoRefreshStatus(GvGManager.GvGAutoRefreshState state)
    {
      this.AutoRefreshStatusOld = this.AutoRefreshStatus;
      this.AutoRefreshStatus = state;
    }

    public void RevertAutoRefreshStatus()
    {
      this.AutoRefreshStatus = this.AutoRefreshStatusOld;
    }

    public void SetupRefreshWait(int sec)
    {
      this.mCanRefreshTime = TimeManager.ServerTime.AddSeconds((double) sec);
    }

    public void SetupAutoRefreshWait(int sec)
    {
      this.mCanAutoRefreshTime = TimeManager.ServerTime.AddSeconds((double) sec);
    }

    public bool SetupNodeDataList(JSON_GvGNodeData[] json)
    {
      if (json == null || json.Length <= 0)
        return false;
      if (this.NodeDataList == null)
        this.NodeDataList = new List<GvGNodeData>();
      for (int i = 0; i < json.Length; ++i)
      {
        GvGNodeData gvGnodeData = this.NodeDataList.Find((Predicate<GvGNodeData>) (n => n.NodeId == json[i].id)) ?? new GvGNodeData();
        if (!gvGnodeData.Deserialize(json[i]))
          return false;
        if (!this.NodeDataList.Contains(gvGnodeData))
          this.NodeDataList.Add(gvGnodeData);
      }
      return true;
    }

    public bool SetupMatchingOrder(int[] json)
    {
      if (json == null || json.Length <= 0)
        return false;
      if (this.MatchingOrder == null)
        this.MatchingOrder = new List<int>();
      for (int index = 0; index < json.Length; ++index)
        this.MatchingOrder.Add(json[index]);
      return true;
    }

    public void SetupUsedUnitList(long[] unit_iids)
    {
      if (unit_iids == null)
        return;
      this.UsedUnitList = new List<long>((IEnumerable<long>) unit_iids);
      ((GvGInfo) ((Component) this).get_gameObject().GetComponent<GvGInfo>()).UpdateGameParameter();
    }

    public bool SetupOtherGuildList(JSON_ViewGuild[] json)
    {
      if (json == null)
        return false;
      if (this.OtherGuildList == null)
        this.OtherGuildList = new List<ViewGuildData>();
      this.OtherGuildList.Clear();
      for (int index = 0; index < json.Length; ++index)
      {
        ViewGuildData viewGuildData = new ViewGuildData();
        viewGuildData.Deserialize(json[index]);
        this.OtherGuildList.Add(viewGuildData);
      }
      return true;
    }

    public bool SetupMyGuild(JSON_ViewGuild json)
    {
      if (json == null)
        return false;
      this.MyGuild = new ViewGuildData();
      this.MyGuild.Deserialize(json);
      return true;
    }

    public bool SetupResultDaily(JSON_GvGResult json)
    {
      this.ResultDaily = (GvGResultData) null;
      if (json == null)
        return false;
      this.ResultDaily = new GvGResultData();
      return this.ResultDaily.Deserialize(json);
    }

    public bool SetupResultSeason(JSON_GvGResult json)
    {
      this.ResultSeason = (GvGResultData) null;
      if (json == null)
        return false;
      this.ResultSeason = new GvGResultData();
      return this.ResultSeason.Deserialize(json);
    }

    public bool SetupResultReward(string[] json)
    {
      this.ResultReward = (RewardData) null;
      if (json == null || json.Length < 0)
        return false;
      RewardData rewardData = new RewardData();
      GameManager gm = MonoSingleton<GameManager>.Instance;
      for (int index = 0; index < json.Length; ++index)
        GvGRewardParam.GetReward(json[index])?.Rewards.ForEach((Action<GvGRewardDetailParam>) (reward =>
        {
          switch (reward.Type)
          {
            case RaidRewardType.Item:
              ItemParam ip = gm.GetItemParam(reward.IName);
              if (ip == null)
                break;
              ItemData itemData1 = rewardData.Items.Find((Predicate<ItemData>) (ri => ri.Param.iname == ip.iname));
              if (itemData1 != null)
              {
                itemData1.Gain(reward.Num);
                break;
              }
              ItemData itemData2 = new ItemData();
              if (!itemData2.Setup(0L, ip.iname, reward.Num))
                break;
              rewardData.Items.Add(itemData2);
              break;
            case RaidRewardType.Gold:
              rewardData.Gold += reward.Num;
              break;
            case RaidRewardType.Coin:
              rewardData.Coin += reward.Num;
              break;
            case RaidRewardType.Award:
              if (gm.MasterParam.GetAwardParam(reward.IName, true) == null)
                break;
              if (!rewardData.GiftRecieveItemDataDic.ContainsKey(reward.IName))
              {
                rewardData.GiftRecieveItemDataDic.Add(reward.IName, new GiftRecieveItemData()
                {
                  iname = reward.IName,
                  num = 1,
                  type = GiftTypes.Award
                });
                break;
              }
              ++rewardData.GiftRecieveItemDataDic[reward.IName].num;
              break;
            case RaidRewardType.Unit:
              if (gm.GetUnitParam(reward.IName) == null)
                break;
              if (!rewardData.GiftRecieveItemDataDic.ContainsKey(reward.IName))
              {
                rewardData.GiftRecieveItemDataDic.Add(reward.IName, new GiftRecieveItemData()
                {
                  iname = reward.IName,
                  num = 1,
                  type = GiftTypes.Unit
                });
                break;
              }
              ++rewardData.GiftRecieveItemDataDic[reward.IName].num;
              break;
            case RaidRewardType.ConceptCard:
              if (ConceptCardData.CreateConceptCardDataForDisplay(reward.IName) == null)
                break;
              if (!rewardData.GiftRecieveItemDataDic.ContainsKey(reward.IName))
              {
                rewardData.GiftRecieveItemDataDic.Add(reward.IName, new GiftRecieveItemData()
                {
                  iname = reward.IName,
                  num = reward.Num,
                  type = GiftTypes.ConceptCard
                });
                break;
              }
              rewardData.GiftRecieveItemDataDic[reward.IName].num += reward.Num;
              break;
            case RaidRewardType.Artifact:
              if (gm.MasterParam.GetArtifactParam(reward.IName) == null)
                break;
              if (!rewardData.GiftRecieveItemDataDic.ContainsKey(reward.IName))
              {
                rewardData.GiftRecieveItemDataDic.Add(reward.IName, new GiftRecieveItemData()
                {
                  iname = reward.IName,
                  num = reward.Num,
                  type = GiftTypes.Artifact
                });
                break;
              }
              rewardData.GiftRecieveItemDataDic[reward.IName].num += reward.Num;
              break;
          }
        }));
      GlobalVars.LastReward.Set(rewardData);
      this.ResultReward = rewardData;
      return true;
    }

    public enum GvGStatus
    {
      Declaration,
      Offense,
      DeclarationCoolTime,
      OffenseCoolTime,
      Finished,
    }

    public enum WindowBGImageIndex
    {
      Blue,
      Purple,
      Red,
    }

    public enum GvGAutoRefreshState
    {
      Top = 0,
      Battle = 10, // 0x0000000A
      Stop = 20, // 0x00000014
    }
  }
}
