﻿// Decompiled with JetBrains decompiler
// Type: SRPG.UnitKakeraConfirm
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  [FlowNode.Pin(1, "Decide", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(10, "Decided", FlowNode.PinTypes.Output, 10)]
  public class UnitKakeraConfirm : MonoBehaviour, IFlowInterface
  {
    [SerializeField]
    private GameObject ItemTemplate;
    private UnitKakeraConfirm.OnDecide mOnDecide;

    public UnitKakeraConfirm()
    {
      base.\u002Ector();
    }

    public void Setup(UnitKakeraConfirm.OnDecide onDecide, ItemData[] items)
    {
      if (Object.op_Inequality((Object) this.ItemTemplate, (Object) null) && items != null)
      {
        this.ItemTemplate.SetActive(false);
        for (int index = 0; index < items.Length; ++index)
        {
          if (items[index] != null)
          {
            GameObject root = (GameObject) Object.Instantiate<GameObject>((M0) this.ItemTemplate, this.ItemTemplate.get_transform().get_parent());
            DataSource.Bind<ItemData>(root, items[index], false);
            root.SetActive(true);
            GameParameter.UpdateAll(root);
          }
        }
      }
      this.mOnDecide = onDecide;
    }

    public void Activated(int pinID)
    {
      if (pinID != 1)
        return;
      if (this.mOnDecide != null)
        this.mOnDecide();
      FlowNode_GameObject.ActivateOutputLinks((Component) this, 10);
    }

    public delegate void OnDecide();
  }
}
