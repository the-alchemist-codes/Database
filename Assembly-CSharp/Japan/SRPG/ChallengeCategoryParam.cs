﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ChallengeCategoryParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System;

namespace SRPG
{
  public class ChallengeCategoryParam
  {
    public TimeParser begin_at = new TimeParser();
    public TimeParser end_at = new TimeParser();
    public TimeParser beginner_period = new TimeParser();
    public string iname;
    public int prio;

    public int Priority
    {
      get
      {
        return this.prio;
      }
    }

    public bool Deserialize(JSON_ChallengeCategoryParam json)
    {
      if (json == null)
        return false;
      this.iname = json.iname;
      this.begin_at.Set(json.begin_at, DateTime.MinValue);
      this.end_at.Set(json.end_at, DateTime.MaxValue);
      this.prio = json.prio;
      this.beginner_period.Set(json.beginner_period, DateTime.MinValue);
      return true;
    }

    public bool IsAvailablePeriod(DateTime now)
    {
      DateTime dateTimes1 = this.begin_at.DateTimes;
      DateTime dateTimes2 = this.end_at.DateTimes;
      return now >= dateTimes1 && dateTimes2 >= now && MonoSingleton<GameManager>.Instance.Player.NewGameAtDateTime >= this.beginner_period.DateTimes;
    }
  }
}
