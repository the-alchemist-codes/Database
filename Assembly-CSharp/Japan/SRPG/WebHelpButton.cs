﻿// Decompiled with JetBrains decompiler
// Type: SRPG.WebHelpButton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace SRPG
{
  public class WebHelpButton : SRPG_Button
  {
    public bool usegAuth = true;
    public GameObject Target;
    [StringIsResourcePath(typeof (GameObject))]
    public string PrefabPath;
    private IWebHelp mInterface;

    protected virtual void Start()
    {
      ((UIBehaviour) this).Start();
      if (Object.op_Inequality((Object) this.Target, (Object) null))
        this.mInterface = (IWebHelp) this.Target.GetComponentInChildren<IWebHelp>(true);
      // ISSUE: method pointer
      ((UnityEvent) this.get_onClick()).AddListener(new UnityAction((object) this, __methodptr(ShowWebHelp)));
    }

    protected virtual void OnEnable()
    {
      ((Selectable) this).OnEnable();
      this.Update();
    }

    private void Update()
    {
      if (this.mInterface == null)
        return;
      ((Selectable) this).set_interactable(this.mInterface.GetHelpURL(out string _, out string _));
    }

    private void ShowWebHelp()
    {
      string url;
      if (!((Selectable) this).IsInteractable() || !this.mInterface.GetHelpURL(out url, out string _))
        return;
      url = GameSettings.Instance.WebHelp_URLMode.ComposeURL(url);
      Application.OpenURL(url);
    }
  }
}
