﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqRaidRankingGuild
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  public class ReqRaidRankingGuild : WebAPI
  {
    public ReqRaidRankingGuild(Network.ResponseCallback response)
    {
      this.name = "raidboss/ranking/guild";
      this.body = WebAPI.GetRequestString(string.Empty);
      this.callback = response;
    }

    [Serializable]
    public class Response
    {
      public Json_RaidRankingGuildList beat;
      public Json_RaidRankingGuildList rescue;
    }
  }
}
