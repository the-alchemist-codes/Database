﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_MyPhoton
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  [FlowNode.NodeType("Multi/MyPhoton", 32741)]
  [FlowNode.Pin(0, "Reset", FlowNode.PinTypes.Input, 0)]
  [FlowNode.Pin(10, "Reset Complete", FlowNode.PinTypes.Output, 1)]
  public class FlowNode_MyPhoton : FlowNode
  {
    private const int PIN_IN_MYPHOTON_RESET = 0;
    private const int PIN_OUT_MYPHOTON_RESETED = 10;

    public override void OnActivate(int pinID)
    {
      MyPhoton instance = PunMonoSingleton<MyPhoton>.Instance;
      if (Object.op_Equality((Object) instance, (Object) null) || pinID != 0)
        return;
      instance.Reset();
      this.ActivateOutputLinks(10);
    }
  }
}
