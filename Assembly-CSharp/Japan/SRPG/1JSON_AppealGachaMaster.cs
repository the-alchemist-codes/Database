﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_AppealGachaMaster
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class JSON_AppealGachaMaster
  {
    public int pk;
    public JSON_AppealGachaMaster.Fields fields;

    public class Fields
    {
      public string appeal_id;
      public string start_at;
      public string end_at;
      public int flag_new;
    }
  }
}
