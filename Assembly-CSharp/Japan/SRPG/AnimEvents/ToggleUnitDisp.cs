﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AnimEvents.ToggleUnitDisp
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG.AnimEvents
{
  public class ToggleUnitDisp : AnimEvent
  {
    private UnitController mUnitController;
    private GeneratedCharacter mGeneratedCharacter;

    public override void OnStart(GameObject go)
    {
      base.OnStart(go);
      this.mGeneratedCharacter = (GeneratedCharacter) go.GetComponent<GeneratedCharacter>();
      if (Object.op_Inequality((Object) this.mGeneratedCharacter, (Object) null))
      {
        this.mGeneratedCharacter.SetVisible(false);
      }
      else
      {
        this.mUnitController = (UnitController) go.GetComponentInParent<UnitController>();
        if (!Object.op_Inequality((Object) this.mUnitController, (Object) null))
          return;
        this.mUnitController.SetVisible(false);
      }
    }

    public override void OnEnd(GameObject go)
    {
      base.OnEnd(go);
      if (Object.op_Inequality((Object) this.mGeneratedCharacter, (Object) null))
      {
        this.mGeneratedCharacter.SetVisible(true);
      }
      else
      {
        if (!Object.op_Inequality((Object) this.mUnitController, (Object) null))
          return;
        this.mUnitController.SetVisible(true);
      }
    }
  }
}
