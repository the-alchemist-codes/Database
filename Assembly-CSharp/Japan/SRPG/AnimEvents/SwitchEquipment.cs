﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AnimEvents.SwitchEquipment
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG.AnimEvents
{
  public class SwitchEquipment : AnimEvent
  {
    public SwitchEquipment.eSwitchTarget SwitchPrimaryHand;
    public SwitchEquipment.eSwitchTarget SwitchSecondaryHand;

    public override void OnStart(GameObject go)
    {
      GeneratedCharacter component = (GeneratedCharacter) go.GetComponent<GeneratedCharacter>();
      if (Object.op_Inequality((Object) component, (Object) null))
      {
        if (this.SwitchPrimaryHand != SwitchEquipment.eSwitchTarget.NO_CHANGE)
          component.SwitchEquipmentLists(GeneratedCharacter.EquipmentType.PRIMARY, (int) this.SwitchPrimaryHand);
        if (this.SwitchSecondaryHand == SwitchEquipment.eSwitchTarget.NO_CHANGE)
          return;
        component.SwitchEquipmentLists(GeneratedCharacter.EquipmentType.SECONDARY, (int) this.SwitchSecondaryHand);
      }
      else
      {
        UnitController componentInParent = (UnitController) go.GetComponentInParent<UnitController>();
        if (Object.op_Equality((Object) componentInParent, (Object) null))
          return;
        if (this.SwitchPrimaryHand != SwitchEquipment.eSwitchTarget.NO_CHANGE)
          componentInParent.SwitchEquipmentLists(UnitController.EquipmentType.PRIMARY, (int) this.SwitchPrimaryHand);
        if (this.SwitchSecondaryHand == SwitchEquipment.eSwitchTarget.NO_CHANGE)
          return;
        componentInParent.SwitchEquipmentLists(UnitController.EquipmentType.SECONDARY, (int) this.SwitchSecondaryHand);
      }
    }

    public enum eSwitchTarget
    {
      NO_CHANGE,
      Element_0,
      Element_1,
      Element_2,
      Element_3,
      Element_4,
      Element_5,
      Element_6,
      Element_7,
      Element_8,
      Element_9,
    }
  }
}
