﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AnimEvents.ParticleGenerator
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG.AnimEvents
{
  public class ParticleGenerator : AnimEventWithTarget
  {
    public GameObject Template;
    public bool Attach;
    public bool NotParticle;
    public bool Preload;
    private GameObject generateObject;

    public void PreLoad(GameObject go)
    {
      if (!this.Preload)
        return;
      this.Generate(go);
    }

    private void Generate(GameObject go)
    {
      this.generateObject = (GameObject) null;
      if (Object.op_Equality((Object) this.Template, (Object) null))
        return;
      this.generateObject = (GameObject) Object.Instantiate<GameObject>((M0) this.Template);
      this.generateObject.SetActive(false);
      if (!this.Attach || string.IsNullOrEmpty(this.BoneName))
        return;
      Transform transform = GameUtility.findChildRecursively(go.get_transform(), this.BoneName);
      if (this.BoneName == "CAMERA" && Object.op_Implicit((Object) Camera.get_main()))
        transform = ((Component) Camera.get_main()).get_transform();
      if (!Object.op_Inequality((Object) transform, (Object) null))
        return;
      this.generateObject.get_transform().SetParent(transform);
    }

    public override void OnStart(GameObject go)
    {
      if (!this.Preload)
        this.Generate(go);
      if (Object.op_Equality((Object) this.generateObject, (Object) null))
        return;
      this.generateObject.SetActive(this.Template.get_activeSelf());
      Vector3 spawnPos;
      Quaternion spawnRot;
      this.CalcPosition(go, this.Template, out spawnPos, out spawnRot);
      Transform transform = this.generateObject.get_transform();
      transform.set_position(spawnPos);
      transform.set_rotation(spawnRot);
      if (!this.NotParticle)
        GameUtility.RequireComponent<OneShotParticle>(this.generateObject);
      if (go.get_transform().get_lossyScale().x * transform.get_lossyScale().z < 0.0)
      {
        Vector3 localScale = transform.get_localScale();
        ref Vector3 local = ref localScale;
        local.z = (__Null) (local.z * -1.0);
        transform.set_localScale(localScale);
      }
      this.OnGenerate(this.generateObject);
      if ((double) this.End > (double) this.Start + 0.100000001490116)
      {
        DestructTimer destructTimer = GameUtility.RequireComponent<DestructTimer>(this.generateObject);
        if (Object.op_Implicit((Object) destructTimer))
          destructTimer.Timer = this.End - this.Start;
      }
      this.generateObject = (GameObject) null;
    }

    protected virtual void OnGenerate(GameObject go)
    {
    }
  }
}
