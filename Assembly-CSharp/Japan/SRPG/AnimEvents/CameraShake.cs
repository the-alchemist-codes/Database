﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AnimEvents.CameraShake
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG.AnimEvents
{
  public class CameraShake : AnimEvent
  {
    public float FrequencyX = 10f;
    public float FrequencyY = 10f;
    public float AmplitudeX = 1f;
    public float AmplitudeY = 1f;
    [Curve(0.0f, 0.0f, 1f, 1f, true)]
    public AnimationCurve ShakeCurve = AnimationCurve.Linear(0.0f, 1f, 1f, 0.0f);

    public Quaternion CalcOffset(float time, float randX, float randY)
    {
      float num1 = (float) (1.0 - ((double) this.Start >= (double) this.End ? 0.0 : ((double) time - (double) this.Start) / ((double) this.End - (double) this.Start)));
      float num2 = num1;
      if (this.ShakeCurve != null)
        num2 = this.ShakeCurve.Evaluate(1f - num1);
      return Quaternion.op_Multiply(Quaternion.AngleAxis(Mathf.Sin((float) (((double) time + (double) randX) * (double) this.FrequencyX * 3.14159274101257)) * this.AmplitudeX * num2, Vector3.get_up()), Quaternion.AngleAxis(Mathf.Sin((float) (((double) time + (double) randY) * (double) this.FrequencyY * 3.14159274101257)) * this.AmplitudeY * num2, Vector3.get_forward()));
    }
  }
}
