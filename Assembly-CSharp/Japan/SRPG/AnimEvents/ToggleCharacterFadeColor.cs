﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AnimEvents.ToggleCharacterFadeColor
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG.AnimEvents
{
  public class ToggleCharacterFadeColor : AnimEvent
  {
    [SerializeField]
    private Color FadeColor = Color.get_white();
    private Color StartColor = Color.get_white();
    private UnitController mUnitController;
    private GeneratedCharacter mGeneratedCharacter;

    public override void OnStart(GameObject go)
    {
      this.mGeneratedCharacter = (GeneratedCharacter) go.GetComponent<GeneratedCharacter>();
      if (Object.op_Inequality((Object) this.mGeneratedCharacter, (Object) null))
      {
        this.StartColor = this.mGeneratedCharacter.GetColor();
      }
      else
      {
        this.mUnitController = (UnitController) go.GetComponentInParent<UnitController>();
        if (!Object.op_Equality((Object) this.mUnitController, (Object) null))
          return;
        this.StartColor = this.mUnitController.GetColor();
      }
    }

    public override void OnEnd(GameObject go)
    {
      if (Object.op_Inequality((Object) this.mGeneratedCharacter, (Object) null))
      {
        this.mGeneratedCharacter.SetColor(this.FadeColor);
      }
      else
      {
        if (!Object.op_Inequality((Object) this.mUnitController, (Object) null))
          return;
        this.mUnitController.SetColor(this.FadeColor);
      }
    }

    public override void OnTick(GameObject go, float ratio)
    {
      this.SetRenderMode(ratio);
    }

    private void SetRenderMode(float ratio)
    {
      Color color = Color.Lerp(this.StartColor, this.FadeColor, ratio);
      if (Object.op_Inequality((Object) this.mGeneratedCharacter, (Object) null))
      {
        this.mGeneratedCharacter.SetColor(color);
      }
      else
      {
        if (!Object.op_Inequality((Object) this.mUnitController, (Object) null))
          return;
        this.mUnitController.SetColor(color);
      }
    }
  }
}
