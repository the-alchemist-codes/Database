﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AnimEvents.ResetEquipment
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG.AnimEvents
{
  public class ResetEquipment : AnimEvent
  {
    public bool IsNoResetPrimaryHand;
    public bool IsNoResetSecondaryHand;

    public override void OnStart(GameObject go)
    {
      GeneratedCharacter component = (GeneratedCharacter) go.GetComponent<GeneratedCharacter>();
      if (Object.op_Inequality((Object) component, (Object) null))
      {
        if (!this.IsNoResetPrimaryHand)
          component.ResetEquipmentLists(GeneratedCharacter.EquipmentType.PRIMARY);
        if (this.IsNoResetSecondaryHand)
          return;
        component.ResetEquipmentLists(GeneratedCharacter.EquipmentType.SECONDARY);
      }
      else
      {
        UnitController componentInParent = (UnitController) go.GetComponentInParent<UnitController>();
        if (Object.op_Equality((Object) componentInParent, (Object) null))
          return;
        if (!this.IsNoResetPrimaryHand)
          componentInParent.ResetEquipmentLists(UnitController.EquipmentType.PRIMARY);
        if (this.IsNoResetSecondaryHand)
          return;
        componentInParent.ResetEquipmentLists(UnitController.EquipmentType.SECONDARY);
      }
    }
  }
}
