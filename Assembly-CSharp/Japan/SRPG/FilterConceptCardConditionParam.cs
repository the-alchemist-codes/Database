﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FilterConceptCardConditionParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class FilterConceptCardConditionParam
  {
    public FilterConceptCardParam parent;
    public string cnds_iname;
    public string name;
    public int rarity;
    public EBirth birth;
    public string card_group;

    public FilterConceptCardConditionParam(FilterConceptCardParam _parent)
    {
      this.parent = _parent;
    }

    public void Deserialize(JSON_FilterConceptCardConditionParam json)
    {
      this.cnds_iname = json.cnds_iname;
      this.name = json.name;
      this.rarity = json.rarity;
      this.birth = (EBirth) json.birth;
      this.card_group = json.card_group;
    }

    public string PrefsKey
    {
      get
      {
        return FilterUtility.FilterPrefs.MakeKey(this.parent.iname, this.cnds_iname);
      }
    }
  }
}
