﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GvGInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class GvGInfo : MonoBehaviour
  {
    [SerializeField]
    private Text mPhaseText;
    [SerializeField]
    private Text mNextText;
    [SerializeField]
    private Button mUpdateButton;
    [SerializeField]
    private GameObject mRestUnitCountRoot;
    private DateTime mEndTime;
    private DateTime mElapsedTime;

    public GvGInfo()
    {
      base.\u002Ector();
    }

    private void Awake()
    {
      this.Refresh();
    }

    public void Refresh()
    {
      this.mElapsedTime = TimeManager.ServerTime;
      this.mEndTime = GvGInfo.SetNextPhaseTime(this.mPhaseText, this.mNextText);
      this.UpdateGameParameter();
    }

    public void UpdateGameParameter()
    {
      if (!UnityEngine.Object.op_Inequality((UnityEngine.Object) this.mRestUnitCountRoot, (UnityEngine.Object) null))
        return;
      this.mRestUnitCountRoot.SetActive(true);
      GameParameter.UpdateAll(this.mRestUnitCountRoot);
    }

    public static DateTime SetNextPhaseTime(Text phaseText, Text nextText)
    {
      DateTime dateTime = DateTime.MinValue;
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) phaseText, (UnityEngine.Object) null) && UnityEngine.Object.op_Inequality((UnityEngine.Object) nextText, (UnityEngine.Object) null))
      {
        GvGManager instance = GvGManager.Instance;
        if (UnityEngine.Object.op_Equality((UnityEngine.Object) instance, (UnityEngine.Object) null))
          return dateTime;
        DateTime serverTime = TimeManager.ServerTime;
        GameUtility.SetGameObjectActive(((Component) nextText).get_gameObject(), true);
        switch (instance.GvGStatusPhase)
        {
          case GvGManager.GvGStatus.Declaration:
            if (UnityEngine.Object.op_Inequality((UnityEngine.Object) phaseText, (UnityEngine.Object) null))
              phaseText.set_text(LocalizedText.Get("sys.GVG_PHASE_REMAIN_DECLARATON"));
            if (UnityEngine.Object.op_Inequality((UnityEngine.Object) nextText, (UnityEngine.Object) null))
              nextText.set_text(instance.GvGPeriod.DeclarationEndTime.ToString());
            DateTime result1;
            DateTime.TryParse(serverTime.ToString("yyyy/MM/dd ") + instance.GvGPeriod.DeclarationEndTime, out result1);
            if (result1 < serverTime)
              result1 = result1.AddDays(1.0);
            dateTime = result1;
            break;
          case GvGManager.GvGStatus.Offense:
            if (UnityEngine.Object.op_Inequality((UnityEngine.Object) phaseText, (UnityEngine.Object) null))
              phaseText.set_text(LocalizedText.Get("sys.GVG_PHASE_REMAIN_OFFENCE"));
            if (UnityEngine.Object.op_Inequality((UnityEngine.Object) nextText, (UnityEngine.Object) null))
              nextText.set_text(instance.GvGPeriod.BattleEndTime.ToString());
            DateTime result2;
            DateTime.TryParse(serverTime.ToString("yyyy/MM/dd ") + instance.GvGPeriod.BattleEndTime, out result2);
            if (result2 < serverTime)
              result2 = result2.AddDays(1.0);
            dateTime = result2;
            break;
          case GvGManager.GvGStatus.DeclarationCoolTime:
            if (UnityEngine.Object.op_Inequality((UnityEngine.Object) phaseText, (UnityEngine.Object) null))
              phaseText.set_text(LocalizedText.Get("sys.GVG_PHASE_START_OFFENCE"));
            if (UnityEngine.Object.op_Inequality((UnityEngine.Object) nextText, (UnityEngine.Object) null))
              nextText.set_text(instance.GvGPeriod.BattleStartTime.ToString());
            DateTime result3;
            DateTime.TryParse(serverTime.ToString("yyyy/MM/dd ") + instance.GvGPeriod.BattleStartTime, out result3);
            if (result3 < serverTime)
              result3 = result3.AddDays(1.0);
            dateTime = result3;
            break;
          case GvGManager.GvGStatus.OffenseCoolTime:
            if (UnityEngine.Object.op_Inequality((UnityEngine.Object) phaseText, (UnityEngine.Object) null))
              phaseText.set_text(LocalizedText.Get("sys.GVG_PHASE_START_DECLARATON"));
            if (UnityEngine.Object.op_Inequality((UnityEngine.Object) nextText, (UnityEngine.Object) null))
              nextText.set_text(instance.GvGPeriod.DeclarationStartTime.ToString());
            DateTime result4;
            DateTime.TryParse(serverTime.ToString("yyyy/MM/dd ") + instance.GvGPeriod.DeclarationStartTime, out result4);
            if (result4 < serverTime)
              result4 = result4.AddDays(1.0);
            dateTime = result4;
            break;
          case GvGManager.GvGStatus.Finished:
            if (UnityEngine.Object.op_Inequality((UnityEngine.Object) phaseText, (UnityEngine.Object) null))
              phaseText.set_text(LocalizedText.Get("sys.GVG_PHASE_FINISH"));
            GameUtility.SetGameObjectActive(((Component) nextText).get_gameObject(), false);
            break;
        }
      }
      return dateTime;
    }

    private void Update()
    {
      this.mElapsedTime = this.mElapsedTime.AddSeconds((double) Time.get_unscaledDeltaTime());
      TimeSpan timeSpan = this.mEndTime - this.mElapsedTime;
      if (timeSpan <= TimeSpan.Zero)
        timeSpan = TimeSpan.Zero;
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.mNextText, (UnityEngine.Object) null))
        this.mNextText.set_text(string.Format(LocalizedText.Get("sys.GVG_PHASE_TIME"), (object) timeSpan.Hours, (object) timeSpan.Minutes, (object) timeSpan.Seconds));
      if (timeSpan <= TimeSpan.Zero)
        this.Refresh();
      GameUtility.SetButtonIntaractable(this.mUpdateButton, !GvGManager.Instance.CanRefresh);
    }

    public void Activated(int pinID)
    {
    }
  }
}
