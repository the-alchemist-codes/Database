﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_LoginBonusTable
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;

namespace SRPG
{
  [MessagePackObject(true)]
  public class Json_LoginBonusTable
  {
    public int count;
    public string type;
    public string prefab;
    public string[] bonus_units;
    public int lastday;
    public int[] login_days;
    public int remain_recover;
    public int max_recover;
    public int current_month;
    public Json_LoginBonus[] bonuses;
    public Json_PremiumLoginBonus[] premium_bonuses;

    public bool IsCanRecover
    {
      get
      {
        return this.remain_recover > 0 && this.login_days != null && this.login_days.Length > 0 && TimeManager.ServerTime.Day - this.login_days.Length > 0;
      }
    }
  }
}
