﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AdvanceStarRewardWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class AdvanceStarRewardWindow : MonoBehaviour
  {
    [SerializeField]
    private Text mBodyText;
    [SerializeField]
    private Transform mRewardIconParent;
    [SerializeField]
    private AdvanceRewardIcon mRewardIconTemplate;
    [SerializeField]
    private Button mReceiveButton;
    [SerializeField]
    private Text mButtonText;

    public AdvanceStarRewardWindow()
    {
      base.\u002Ector();
    }

    private void Start()
    {
      SerializeValueBehaviour component = (SerializeValueBehaviour) ((Component) this).GetComponent<SerializeValueBehaviour>();
      if (Object.op_Equality((Object) component, (Object) null))
        return;
      AdvanceStarRewardParam advanceStarRewardParam = component.list.GetObject<AdvanceStarRewardParam>("ADVANCE_STAR_REWARD", (AdvanceStarRewardParam) null);
      if (advanceStarRewardParam == null || Object.op_Equality((Object) this.mBodyText, (Object) null))
        return;
      if (!advanceStarRewardParam.IsReward)
        this.mBodyText.set_text(string.Format(LocalizedText.Get("sys.ADVANCE_QUEST_STARREWARD_TEXT"), (object) advanceStarRewardParam.NeedStarNum));
      else
        this.mBodyText.set_text(string.Format(LocalizedText.Get("sys.ADVANCE_QUEST_STARREWARD_RECEIVED")));
      if (Object.op_Equality((Object) this.mRewardIconTemplate, (Object) null))
        return;
      AdvanceRewardParam advanceRewardParam = MonoSingleton<GameManager>.Instance.GetAdvanceRewardParam(advanceStarRewardParam.RewardId);
      if (advanceRewardParam == null || advanceRewardParam.RewardList == null)
        return;
      for (int index = 0; index < advanceRewardParam.RewardList.Count; ++index)
      {
        AdvanceRewardIcon advanceRewardIcon = (AdvanceRewardIcon) Object.Instantiate<AdvanceRewardIcon>((M0) this.mRewardIconTemplate, this.mRewardIconParent);
        advanceRewardIcon.Initialize(advanceRewardParam.RewardList[index]);
        ((Component) advanceRewardIcon).get_gameObject().SetActive(true);
      }
      if (Object.op_Equality((Object) this.mReceiveButton, (Object) null))
        return;
      int num = component.list.GetInt("ADVANCE_STAR_POINT", 0);
      ((Selectable) this.mReceiveButton).set_interactable(advanceStarRewardParam.NeedStarNum <= num && !advanceStarRewardParam.IsReward);
      if (!Object.op_Inequality((Object) this.mButtonText, (Object) null))
        return;
      if (!advanceStarRewardParam.IsReward)
      {
        if (advanceStarRewardParam.NeedStarNum <= num)
          this.mButtonText.set_text(string.Format(LocalizedText.Get("sys.ADVANCE_QUEST_STARREWARD_BTN_OK")));
        else
          this.mButtonText.set_text(string.Format(LocalizedText.Get("sys.ADVANCE_QUEST_STARREWARD_BTN_UNACHIEVED")));
      }
      else
        this.mButtonText.set_text(string.Format(LocalizedText.Get("sys.ADVANCE_QUEST_STARREWARD_BTN_RECEIVED")));
    }
  }
}
