﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RecommendedArtifactViewParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class RecommendedArtifactViewParam
  {
    private bool m_IsRecommend;

    public RecommendedArtifactViewParam(bool isRecommend)
    {
      this.m_IsRecommend = isRecommend;
    }

    public bool IsRecommend
    {
      get
      {
        return this.m_IsRecommend;
      }
    }
  }
}
