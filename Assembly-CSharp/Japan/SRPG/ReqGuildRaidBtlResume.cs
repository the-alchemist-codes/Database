﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqGuildRaidBtlResume
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  public class ReqGuildRaidBtlResume : WebAPI
  {
    public ReqGuildRaidBtlResume(int gid, long btlid, Network.ResponseCallback response)
    {
      this.name = "guildraid/btl/resume";
      this.body = WebAPI.GetRequestString<ReqGuildRaidBtlResume.RequestParam>(new ReqGuildRaidBtlResume.RequestParam()
      {
        gid = gid,
        btlid = btlid
      });
      this.callback = response;
    }

    [Serializable]
    public class RequestParam
    {
      public int gid;
      public long btlid;
    }
  }
}
