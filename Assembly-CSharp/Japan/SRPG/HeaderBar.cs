﻿// Decompiled with JetBrains decompiler
// Type: SRPG.HeaderBar
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class HeaderBar : PropertyAttribute
  {
    public string Text;
    public Color BGColor;
    public Color FGColor;

    public HeaderBar(string text)
    {
      this.\u002Ector();
      this.Text = text;
      this.BGColor = new Color(0.0f, 0.2f, 0.5f);
      this.FGColor = Color.get_white();
    }

    public HeaderBar(string text, Color bg)
    {
      this.\u002Ector();
      this.Text = text;
      this.BGColor = bg;
      this.FGColor = Color.get_white();
    }

    public HeaderBar(string text, Color bg, Color fg)
    {
      this.\u002Ector();
      this.Text = text;
      this.BGColor = bg;
      this.FGColor = fg;
    }
  }
}
