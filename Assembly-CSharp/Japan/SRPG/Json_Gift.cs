﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_Gift
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;

namespace SRPG
{
  [MessagePackObject(true)]
  public class Json_Gift
  {
    public int rare = -1;
    public string iname;
    public int num;
    public int gold;
    public int coin;
    public int arenacoin;
    public int multicoin;
    public int kakeracoin;
    public Json_GiftConceptCard concept_card;
  }
}
