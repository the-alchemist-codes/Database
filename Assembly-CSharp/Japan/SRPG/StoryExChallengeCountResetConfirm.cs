﻿// Decompiled with JetBrains decompiler
// Type: SRPG.StoryExChallengeCountResetConfirm
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  [FlowNode.Pin(10, "初期化", FlowNode.PinTypes.Input, 10)]
  [FlowNode.Pin(100, "初期化完了", FlowNode.PinTypes.Output, 100)]
  public class StoryExChallengeCountResetConfirm : MonoBehaviour, IFlowInterface
  {
    private const int PIN_INPUT_INIT_START = 10;
    private const int PIN_OUTPUT_INIT_END = 100;
    [SerializeField]
    private eResetCostType mCostType;
    [SerializeField]
    private Text mConfirmText;
    [SerializeField]
    private Text mBeforeNum;
    [SerializeField]
    private Text mAfterNum;
    [SerializeField]
    private Text mRestResetCount;
    [SerializeField]
    private GameObject mItemIcon;

    public StoryExChallengeCountResetConfirm()
    {
      base.\u002Ector();
    }

    public void Activated(int pinID)
    {
      if (pinID != 10)
        return;
      this.Refresh();
    }

    private void Refresh()
    {
      switch (this.mCostType)
      {
        case eResetCostType.Coin:
          this.Refresh_Coin();
          break;
        case eResetCostType.Item:
          this.Refresh_Item();
          break;
      }
      FlowNode_GameObject.ActivateOutputLinks((Component) this, 100);
    }

    private void Refresh_Item()
    {
      QuestParam quest = MonoSingleton<GameManager>.Instance.FindQuest(GlobalVars.SelectedQuestID);
      if (quest == null)
        return;
      ResetCostParam resetCost = MonoSingleton<GameManager>.Instance.MasterParam.FindResetCost(quest.ResetCost);
      if (resetCost == null || !resetCost.IsEnableItemCost())
        return;
      ResetCostInfoParam resetCostInfo = resetCost.GetResetCostInfo(eResetCostType.Item);
      if (Object.op_Inequality((Object) this.mItemIcon, (Object) null))
      {
        ItemParam itemParam = MonoSingleton<GameManager>.Instance.GetItemParam(resetCostInfo.Item);
        if (itemParam != null)
        {
          GameUtility.SetGameObjectActive(this.mItemIcon, true);
          DataSource.Bind<ItemParam>(this.mItemIcon, itemParam, false);
          GameParameter.UpdateAll(this.mItemIcon);
        }
        else
          GameUtility.SetGameObjectActive(this.mItemIcon, false);
      }
      int needNum = resetCostInfo.GetNeedNum((int) quest.dailyReset);
      int itemAmount = MonoSingleton<GameManager>.Instance.Player.GetItemAmount(resetCostInfo.Item);
      if (Object.op_Inequality((Object) this.mConfirmText, (Object) null))
        this.mConfirmText.set_text(string.Format(LocalizedText.Get("sys.STORY_EX_CHALLENGE_COUNT_RESET_CONFIRM_ITEM"), (object) itemAmount, (object) needNum));
      if (Object.op_Inequality((Object) this.mBeforeNum, (Object) null))
        this.mBeforeNum.set_text(itemAmount.ToString());
      if (Object.op_Inequality((Object) this.mAfterNum, (Object) null))
        this.mAfterNum.set_text(Mathf.Max(0, itemAmount - needNum).ToString());
      if (!Object.op_Inequality((Object) this.mRestResetCount, (Object) null))
        return;
      this.mRestResetCount.set_text(quest.GetRestResetCount().ToString());
    }

    private void Refresh_Coin()
    {
      GameUtility.SetGameObjectActive(this.mItemIcon, false);
      QuestParam quest = MonoSingleton<GameManager>.Instance.FindQuest(GlobalVars.SelectedQuestID);
      if (quest == null)
        return;
      ResetCostParam resetCost = MonoSingleton<GameManager>.Instance.MasterParam.FindResetCost(quest.ResetCost);
      if (resetCost == null || !resetCost.IsEnableCoinCost())
        return;
      int needNum = resetCost.GetResetCostInfo(eResetCostType.Coin).GetNeedNum((int) quest.dailyReset);
      if (Object.op_Inequality((Object) this.mConfirmText, (Object) null))
        this.mConfirmText.set_text(string.Format(LocalizedText.Get("sys.STORY_EX_CHALLENGE_COUNT_RESET_CONFIRM_COIN"), (object) needNum));
      if (!Object.op_Inequality((Object) this.mRestResetCount, (Object) null))
        return;
      this.mRestResetCount.set_text(quest.GetRestResetCount().ToString());
    }
  }
}
