﻿// Decompiled with JetBrains decompiler
// Type: SRPG.UnitPreviewForRT
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class UnitPreviewForRT : MonoBehaviour
  {
    [Description("3Dユニット投影用テクスチャの描画先")]
    [SerializeField]
    private RawImage m_PreviewImage;
    [Description("3Dユニット撮影カメラ")]
    [SerializeField]
    private Camera m_Camera3D;
    [Description("3Dユニット")]
    [SerializeField]
    private UnitPreview m_UnitController;
    private RenderTexture m_UnitRenderTexture;

    public UnitPreviewForRT()
    {
      base.\u002Ector();
    }

    private RenderTexture CreateRenderTexture()
    {
      Rect rect1 = ((Graphic) this.m_PreviewImage).get_rectTransform().get_rect();
      int num1 = (int) Mathf.Floor(((Rect) ref rect1).get_width());
      Rect rect2 = ((Graphic) this.m_PreviewImage).get_rectTransform().get_rect();
      int num2 = (int) Mathf.Floor(((Rect) ref rect2).get_height());
      return RenderTexture.GetTemporary(num1, num2, 16, (RenderTextureFormat) 7);
    }

    private void Awake()
    {
      if (Object.op_Inequality((Object) this.m_Camera3D, (Object) null))
        ((Behaviour) this.m_Camera3D).set_enabled(false);
      if (!Object.op_Inequality((Object) this.m_PreviewImage, (Object) null))
        return;
      ((Component) this.m_PreviewImage).get_gameObject().SetActive(false);
    }

    private void Start()
    {
      this.Init();
    }

    private void Init()
    {
      this.StartCoroutine(this.Setup());
    }

    public void Refresh()
    {
      this.StartCoroutine(this.Setup());
    }

    private void OnDestroy()
    {
      if (!Object.op_Inequality((Object) this.m_UnitRenderTexture, (Object) null))
        return;
      RenderTexture.ReleaseTemporary(this.m_UnitRenderTexture);
      this.m_UnitRenderTexture = (RenderTexture) null;
    }

    public void SetUnitController(UnitPreview controller)
    {
      if (Object.op_Equality((Object) controller, (Object) null))
        DebugUtility.LogError("HomeUnitControllerが指定されていません.");
      else
        this.m_UnitController = controller;
    }

    [DebuggerHidden]
    private IEnumerator Setup()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new UnitPreviewForRT.\u003CSetup\u003Ec__Iterator0()
      {
        \u0024this = this
      };
    }
  }
}
