﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqMonthlyRecover
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using Gsc.Network.Encoding;
using MessagePack;
using System;

namespace SRPG
{
  public class ReqMonthlyRecover : WebAPI
  {
    public ReqMonthlyRecover(
      string select_iname,
      int select_dau,
      SRPG.Network.ResponseCallback response,
      EncodingTypes.ESerializeCompressMethod serializeCompressMethod)
    {
      this.name = "loginbonus/monthly/recover";
      this.body = WebAPI.GetRequestString<ReqMonthlyRecover.RequestParam>(new ReqMonthlyRecover.RequestParam()
      {
        iname = select_iname,
        day = select_dau
      });
      this.callback = response;
      this.serializeCompressMethod = serializeCompressMethod;
    }

    [Serializable]
    public class RequestParam
    {
      public string iname;
      public int day;
    }

    [MessagePackObject(true)]
    [Serializable]
    public class Response
    {
      public Json_Notify_Monthly notify;
    }
  }
}
