﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AutoRepeatQuestStart
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace SRPG
{
  [FlowNode.Pin(10, "初期化", FlowNode.PinTypes.Input, 10)]
  [FlowNode.Pin(100, "初期化完了", FlowNode.PinTypes.Output, 100)]
  [FlowNode.Pin(11, "出撃をタップ", FlowNode.PinTypes.Input, 11)]
  [FlowNode.Pin(101, "周回リクエスト", FlowNode.PinTypes.Output, 101)]
  [FlowNode.Pin(102, "AP回復", FlowNode.PinTypes.Output, 102)]
  [FlowNode.Pin(12, "表示更新", FlowNode.PinTypes.Input, 12)]
  public class AutoRepeatQuestStart : MonoBehaviour, IFlowInterface
  {
    private const int PIN_IN_INIT = 10;
    private const int PIN_OT_INIT = 100;
    private const int PIN_IN_CLICK_START = 11;
    private const int PIN_OT_REQUEST = 101;
    private const int PIN_OT_HEAL_AP = 102;
    private const int PIN_IN_REFRESH = 12;
    [SerializeField]
    private Text QuestClearTime;
    [SerializeField]
    private Text SelectAutoLapNum;
    [SerializeField]
    private Toggle UseNotification;
    [SerializeField]
    private Button AutoLapNumAdd;
    [SerializeField]
    private Button AutoLapNumSub;
    [SerializeField]
    private Toggle UseAPHealItem;
    [SerializeField]
    private ToggleGroup Group;
    [SerializeField]
    private Toggle UseSelectLapNum;
    [SerializeField]
    private Toggle UseSelectBoxFull;
    [SerializeField]
    private ToggleGroup PushGroup;
    [SerializeField]
    private Toggle PushOn;
    [SerializeField]
    private Toggle PushOff;
    [SerializeField]
    private Text mBoxSizeText;
    private int m_LapNum;
    private int m_MaxLapNum;
    private bool m_SelectBoxFull;
    private bool m_UseLocalNotification;

    public AutoRepeatQuestStart()
    {
      base.\u002Ector();
    }

    public int LapNum
    {
      get
      {
        return this.m_LapNum;
      }
    }

    public bool IsUseAPHealItem
    {
      get
      {
        return UnityEngine.Object.op_Inequality((UnityEngine.Object) this.UseAPHealItem, (UnityEngine.Object) null) && this.UseAPHealItem.get_isOn();
      }
    }

    public bool IsUseNotification
    {
      get
      {
        return this.m_UseLocalNotification;
      }
    }

    public bool IsSelectBoxFull
    {
      get
      {
        return this.m_SelectBoxFull;
      }
    }

    public void Activated(int pinID)
    {
      switch (pinID)
      {
        case 10:
          this.Init();
          break;
        case 11:
          this.OnClickStart();
          break;
        case 12:
          this.Refresh();
          break;
      }
    }

    private void Awake()
    {
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.AutoLapNumAdd, (UnityEngine.Object) null))
      {
        ButtonHoldObserver buttonHoldObserver = (ButtonHoldObserver) ((Component) this.AutoLapNumAdd).get_gameObject().AddComponent<ButtonHoldObserver>();
        buttonHoldObserver.OnHoldStart = (ButtonHoldObserver.DelegateOnHoldEvent) (() => {});
        buttonHoldObserver.OnHoldUpdate = (ButtonHoldObserver.DelegateOnHoldEvent) (() => this.OnLapNumAdd(this.AutoLapNumAdd, false));
        buttonHoldObserver.OnHoldEnd = (ButtonHoldObserver.DelegateOnHoldEvent) (() => this.OnLapNumAdd(this.AutoLapNumAdd, true));
      }
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.AutoLapNumSub, (UnityEngine.Object) null))
      {
        ButtonHoldObserver buttonHoldObserver = (ButtonHoldObserver) ((Component) this.AutoLapNumSub).get_gameObject().AddComponent<ButtonHoldObserver>();
        buttonHoldObserver.OnHoldStart = (ButtonHoldObserver.DelegateOnHoldEvent) (() => {});
        buttonHoldObserver.OnHoldUpdate = (ButtonHoldObserver.DelegateOnHoldEvent) (() => this.OnLapNumSub(this.AutoLapNumSub, false));
        buttonHoldObserver.OnHoldEnd = (ButtonHoldObserver.DelegateOnHoldEvent) (() => this.OnLapNumSub(this.AutoLapNumSub, true));
      }
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.UseSelectBoxFull, (UnityEngine.Object) null))
      {
        // ISSUE: method pointer
        ((UnityEvent<bool>) this.UseSelectBoxFull.onValueChanged).AddListener(new UnityAction<bool>((object) this, __methodptr(OnSelectToggle)));
      }
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.UseSelectLapNum, (UnityEngine.Object) null))
      {
        // ISSUE: method pointer
        ((UnityEvent<bool>) this.UseSelectLapNum.onValueChanged).AddListener(new UnityAction<bool>((object) this, __methodptr(OnSelectToggle)));
      }
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.PushOn, (UnityEngine.Object) null))
      {
        // ISSUE: method pointer
        ((UnityEvent<bool>) this.PushOn.onValueChanged).AddListener(new UnityAction<bool>((object) this, __methodptr(OnSelectTogglePushSetting)));
      }
      if (!UnityEngine.Object.op_Inequality((UnityEngine.Object) this.PushOff, (UnityEngine.Object) null))
        return;
      // ISSUE: method pointer
      ((UnityEvent<bool>) this.PushOff.onValueChanged).AddListener(new UnityAction<bool>((object) this, __methodptr(OnSelectTogglePushSetting)));
    }

    private void Init()
    {
      QuestParam quest = (QuestParam) null;
      if (!string.IsNullOrEmpty(GlobalVars.SelectedQuestID))
        quest = MonoSingleton<GameManager>.Instance.FindQuest(GlobalVars.SelectedQuestID);
      if (quest == null)
      {
        DebugUtility.LogError("QuestParamが設定されていません.");
      }
      else
      {
        int autoRepeatCountMax = MonoSingleton<GameManager>.Instance.MasterParam.FixParam.AutoRepeatCountMax;
        if (quest != null && autoRepeatCountMax > 0)
          this.m_MaxLapNum = Mathf.Min(this.m_MaxLapNum, QuestParam.GetRaidTicketCount_LimitMax(quest, autoRepeatCountMax));
        if (quest != null)
        {
          int num = quest.RequiredApWithPlayerLv(MonoSingleton<GameManager>.Instance.Player.Lv, true);
          this.m_LapNum = num > 0 ? Math.Max(1, Math.Min(this.m_MaxLapNum, MonoSingleton<GameManager>.Instance.Player.Stamina / num)) : this.m_MaxLapNum;
        }
        if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.QuestClearTime, (UnityEngine.Object) null))
        {
          int num1 = (double) MonoSingleton<GameManager>.Instance.GetQuestPlayTime() <= 0.0 || (int) MonoSingleton<GameManager>.Instance.GetQuestPlayTime() >= quest.best_clear_time ? quest.best_clear_time : (int) MonoSingleton<GameManager>.Instance.GetQuestPlayTime();
          int num2 = num1 / 60;
          this.QuestClearTime.set_text((num2 / 60).ToString("00") + ":" + (num2 % 60).ToString("00") + ":" + (num1 % 60).ToString("00"));
        }
        this.RefreshSelectLapNumUI(true);
        if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.UseSelectLapNum, (UnityEngine.Object) null))
          this.UseSelectLapNum.set_isOn(true);
        this.Refresh();
        FlowNode_GameObject.ActivateOutputLinks((Component) this, 100);
      }
    }

    private void Refresh()
    {
      if (!UnityEngine.Object.op_Inequality((UnityEngine.Object) this.mBoxSizeText, (UnityEngine.Object) null))
        return;
      this.mBoxSizeText.set_text(MonoSingleton<GameManager>.Instance.Player.AutoRepeatQuestBox.Size.ToString());
    }

    private void OnLapNumAdd(Button button, bool is_refresh_btn_interactable)
    {
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) button, (UnityEngine.Object) null) || !((Selectable) button).get_interactable())
        return;
      this.m_LapNum = Mathf.Min(this.m_MaxLapNum, this.m_LapNum + 1);
      this.RefreshSelectLapNumUI(is_refresh_btn_interactable);
    }

    private void OnLapNumSub(Button button, bool is_refresh_btn_interactable)
    {
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) button, (UnityEngine.Object) null) || !((Selectable) button).get_interactable())
        return;
      this.m_LapNum = Mathf.Max(1, this.m_LapNum - 1);
      this.RefreshSelectLapNumUI(is_refresh_btn_interactable);
    }

    private void RefreshSelectLapNumUI(bool is_refresh_btn_interactable = true)
    {
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.SelectAutoLapNum, (UnityEngine.Object) null))
        this.SelectAutoLapNum.set_text(this.m_LapNum.ToString());
      if (!is_refresh_btn_interactable)
        return;
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.AutoLapNumAdd, (UnityEngine.Object) null))
        ((Selectable) this.AutoLapNumAdd).set_interactable(this.m_LapNum < this.m_MaxLapNum);
      if (!UnityEngine.Object.op_Inequality((UnityEngine.Object) this.AutoLapNumSub, (UnityEngine.Object) null))
        return;
      ((Selectable) this.AutoLapNumSub).set_interactable(this.m_LapNum > 1);
    }

    private void OnSelectToggle(bool value)
    {
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) this.Group, (UnityEngine.Object) null) || UnityEngine.Object.op_Equality((UnityEngine.Object) this.UseSelectLapNum, (UnityEngine.Object) null) || UnityEngine.Object.op_Equality((UnityEngine.Object) this.UseSelectBoxFull, (UnityEngine.Object) null))
        return;
      List<Toggle> list = this.Group.ActiveToggles().ToList<Toggle>();
      if (list.Count != 1)
        return;
      Toggle toggle = list[0];
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) toggle, (UnityEngine.Object) this.UseSelectBoxFull))
      {
        ((Selectable) this.AutoLapNumAdd).set_interactable(false);
        ((Selectable) this.AutoLapNumSub).set_interactable(false);
        this.m_LapNum = 0;
        this.m_SelectBoxFull = true;
      }
      else
      {
        if (!UnityEngine.Object.op_Equality((UnityEngine.Object) toggle, (UnityEngine.Object) this.UseSelectLapNum))
          return;
        this.m_LapNum = int.Parse(this.SelectAutoLapNum.get_text());
        this.RefreshSelectLapNumUI(true);
        this.m_SelectBoxFull = false;
      }
    }

    private void OnSelectTogglePushSetting(bool value)
    {
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) this.PushGroup, (UnityEngine.Object) null) || UnityEngine.Object.op_Equality((UnityEngine.Object) this.PushOn, (UnityEngine.Object) null) || UnityEngine.Object.op_Equality((UnityEngine.Object) this.PushOff, (UnityEngine.Object) null))
        return;
      List<Toggle> list = this.PushGroup.ActiveToggles().ToList<Toggle>();
      if (list.Count != 1)
        return;
      Toggle toggle = list[0];
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) toggle, (UnityEngine.Object) this.PushOn))
      {
        this.m_UseLocalNotification = true;
      }
      else
      {
        if (!UnityEngine.Object.op_Equality((UnityEngine.Object) toggle, (UnityEngine.Object) this.PushOff))
          return;
        this.m_UseLocalNotification = false;
      }
    }

    private void OnClickStart()
    {
      QuestParam questParam = (QuestParam) null;
      if (!string.IsNullOrEmpty(GlobalVars.SelectedQuestID))
        questParam = MonoSingleton<GameManager>.Instance.FindQuest(GlobalVars.SelectedQuestID);
      if (questParam == null)
      {
        DebugUtility.LogError("QuestParamが設定されていません.");
      }
      else
      {
        int pinID = 101;
        if (questParam.RequiredApWithPlayerLv(MonoSingleton<GameManager>.Instance.Player.Lv, true) > MonoSingleton<GameManager>.Instance.Player.Stamina && (!this.IsUseAPHealItem || !MonoSingleton<GameManager>.Instance.Player.IsHaveHealAPItems()))
          pinID = 102;
        FlowNode_GameObject.ActivateOutputLinks((Component) this, pinID);
      }
    }
  }
}
