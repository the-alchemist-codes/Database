﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqGuildFacilityEnhance
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Text;

namespace SRPG
{
  public class ReqGuildFacilityEnhance : WebAPI
  {
    public ReqGuildFacilityEnhance(
      string facility_iname,
      EnhanceMaterial[] materials,
      long gold,
      Network.ResponseCallback response)
    {
      this.name = "guild/facility/invest";
      StringBuilder stringBuilder = WebAPI.GetStringBuilder();
      stringBuilder.Append("\"facility_iname\":\"");
      stringBuilder.Append(facility_iname);
      stringBuilder.Append("\"");
      stringBuilder.Append(",");
      stringBuilder.Append("\"mats\":[");
      for (int index = 0; index < materials.Length; ++index)
      {
        stringBuilder.Append("{");
        stringBuilder.Append("\"iname\":\"");
        stringBuilder.Append(materials[index].item.Param.iname);
        stringBuilder.Append("\"");
        stringBuilder.Append(",");
        stringBuilder.Append("\"num\":");
        stringBuilder.Append(materials[index].num);
        stringBuilder.Append("}");
        if (index < materials.Length - 1)
          stringBuilder.Append(",");
      }
      stringBuilder.Append("]");
      stringBuilder.Append(",");
      stringBuilder.Append("\"gold\":" + (object) gold);
      this.body = WebAPI.GetRequestString(stringBuilder.ToString());
      this.callback = response;
    }
  }
}
