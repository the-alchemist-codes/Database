﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ShopTimeOutItemInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  [Serializable]
  public class ShopTimeOutItemInfo
  {
    public string ShopId;
    public int ItemId;
    public long End;

    public ShopTimeOutItemInfo(string shopId, int itemId, long end)
    {
      this.ShopId = shopId;
      this.ItemId = itemId;
      this.End = end;
    }
  }
}
