﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GachaPickupParts
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class GachaPickupParts : MonoBehaviour
  {
    [Header("切り替え画像オブジェクト")]
    [SerializeField]
    private RawImage m_Image;

    public GachaPickupParts()
    {
      base.\u002Ector();
    }

    public RawImage Image
    {
      get
      {
        return this.m_Image;
      }
    }

    private void OnDestroy()
    {
      if (!Object.op_Inequality((Object) this.m_Image, (Object) null))
        return;
      this.m_Image.set_texture((Texture) null);
    }
  }
}
