﻿// Decompiled with JetBrains decompiler
// Type: SRPG.LoginBonusItemListDetailWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;

namespace SRPG
{
  public class LoginBonusItemListDetailWindow : MonoBehaviour
  {
    public LoginBonusItemListDetailWindow()
    {
      base.\u002Ector();
    }

    public void Refresh()
    {
      ItemData itemDataByItemId = MonoSingleton<GameManager>.Instance.Player.FindItemDataByItemID(GlobalVars.SelectedItemID, false);
      if (itemDataByItemId != null)
      {
        DataSource.Bind<ItemData>(((Component) this).get_gameObject(), itemDataByItemId, false);
      }
      else
      {
        ItemParam itemParam = MonoSingleton<GameManager>.Instance.MasterParam.GetItemParam(GlobalVars.SelectedItemID);
        if (itemParam == null)
          return;
        DataSource.Bind<ItemParam>(((Component) this).get_gameObject(), itemParam, false);
      }
      GameParameter.UpdateAll(((Component) this).get_gameObject());
      ((Behaviour) this).set_enabled(true);
    }
  }
}
