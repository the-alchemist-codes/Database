﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_RuneMaterial
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_RuneMaterial
  {
    public int rarity;
    public string[] enh_cost;
    public string[] evo_cost;
    public int disassembly_zeny;
    public JSON_RuneDisassembly[] disassembly;
    public int[] enhance_probability;
    public int[] magnification;
    public string[] reset_base_param_costs;
    public string[] reset_evo_status_costs;
    public string[] param_enh_evo_costs;
  }
}
