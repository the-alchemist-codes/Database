﻿// Decompiled with JetBrains decompiler
// Type: SRPG.TobiraLearnAbilityParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;

namespace SRPG
{
  [MessagePackObject(true)]
  public class TobiraLearnAbilityParam
  {
    private string mAbilityIname;
    private int mLevel;
    private TobiraLearnAbilityParam.AddType mAddType;
    private string mTargeJob;
    private string mAbilityOverwrite;

    public string AbilityIname
    {
      get
      {
        return this.mAbilityIname;
      }
    }

    public int Level
    {
      get
      {
        return this.mLevel;
      }
    }

    public TobiraLearnAbilityParam.AddType AbilityAddType
    {
      get
      {
        return this.mAddType;
      }
    }

    public string TargetJob
    {
      get
      {
        return this.mTargeJob;
      }
    }

    public string AbilityOverwrite
    {
      get
      {
        return this.mAbilityOverwrite;
      }
    }

    public void Deserialize(JSON_TobiraLearnAbilityParam json)
    {
      if (json == null)
        return;
      this.mAbilityIname = json.abil_iname;
      this.mLevel = json.learn_lv;
      this.mAddType = (TobiraLearnAbilityParam.AddType) json.add_type;
      this.mTargeJob = json.target_job;
      this.mAbilityOverwrite = json.abil_overwrite;
    }

    public enum AddType
    {
      Unknow,
      JobOverwrite,
      MasterAdd,
      MasterOverwrite,
    }
  }
}
