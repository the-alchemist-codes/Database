﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqUnitRentalLeave
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using Gsc.Network.Encoding;
using MessagePack;
using System;

namespace SRPG
{
  public class ReqUnitRentalLeave : WebAPI
  {
    public ReqUnitRentalLeave(
      string rental_iname,
      string unit_iname,
      SRPG.Network.ResponseCallback response,
      EncodingTypes.ESerializeCompressMethod serializeCompressMethod)
    {
      this.name = "unit/rental/leave";
      this.body = WebAPI.GetRequestString<ReqUnitRentalLeave.RequestParam>(new ReqUnitRentalLeave.RequestParam()
      {
        rental_iname = rental_iname,
        unit_iname = unit_iname
      });
      this.callback = response;
      this.serializeCompressMethod = serializeCompressMethod;
    }

    [Serializable]
    public class RequestParam
    {
      public string rental_iname;
      public string unit_iname;
    }

    [MessagePackObject(true)]
    [Serializable]
    public class Response
    {
      public string leave_unit_iname;
      public Json_Item[] return_items;
      public Json_Item[] items;
    }
  }
}
