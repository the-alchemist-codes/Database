﻿// Decompiled with JetBrains decompiler
// Type: SRPG.DrawCardParticleEff
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class DrawCardParticleEff : MonoBehaviour
  {
    private UIParticleSystem[] mParticle;

    public DrawCardParticleEff()
    {
      base.\u002Ector();
    }

    private void Awake()
    {
      this.mParticle = (UIParticleSystem[]) ((Component) this).get_gameObject().GetComponentsInChildren<UIParticleSystem>();
    }

    private void OnEnable()
    {
      if (this.mParticle == null)
        this.mParticle = (UIParticleSystem[]) ((Component) this).get_gameObject().GetComponentsInChildren<UIParticleSystem>();
      if (this.mParticle == null)
        return;
      foreach (UIParticleSystem uiParticleSystem in this.mParticle)
        uiParticleSystem.ResetParticleSystem();
    }
  }
}
