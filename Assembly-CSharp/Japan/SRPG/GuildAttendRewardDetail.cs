﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildAttendRewardDetail
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class GuildAttendRewardDetail
  {
    private int mMemberCount;
    private string mRewardID;

    public int member_cnt
    {
      get
      {
        return this.mMemberCount;
      }
    }

    public string reward_id
    {
      get
      {
        return this.mRewardID;
      }
    }

    public bool Deserialize(JSON_GuildAttendRewardDetail json)
    {
      if (json == null)
        return false;
      this.mMemberCount = json.member_cnt;
      this.mRewardID = json.reward_id;
      return true;
    }
  }
}
