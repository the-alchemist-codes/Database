﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildRaidRewardDmgRatio
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class GuildRaidRewardDmgRatio
  {
    public int DatageRatio { get; private set; }

    public string RewardRoundId { get; private set; }

    public bool Deserialize(JSON_GuildRaidRewardDmgRatio json)
    {
      if (json == null)
        return false;
      this.DatageRatio = json.damage_ratio;
      this.RewardRoundId = json.reward_round_id;
      return true;
    }
  }
}
