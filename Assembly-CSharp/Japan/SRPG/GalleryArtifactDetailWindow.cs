﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GalleryArtifactDetailWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class GalleryArtifactDetailWindow : MonoBehaviour
  {
    public GalleryArtifactDetailWindow()
    {
      base.\u002Ector();
    }

    private void Start()
    {
      if (!(FlowNode_ButtonEvent.currentValue is SerializeValueList currentValue))
        return;
      DataSource.Bind<ArtifactParam>(((Component) this).get_gameObject(), DataSource.FindDataOfClass<ArtifactParam>(currentValue.GetGameObject("_self"), (ArtifactParam) null), false);
      GameParameter.UpdateAll(((Component) this).get_gameObject());
    }
  }
}
