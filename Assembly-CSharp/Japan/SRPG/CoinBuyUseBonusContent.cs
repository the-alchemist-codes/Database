﻿// Decompiled with JetBrains decompiler
// Type: SRPG.CoinBuyUseBonusContent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class CoinBuyUseBonusContent : MonoBehaviour
  {
    [SerializeField]
    private GameObject mRewardIconParent;
    [SerializeField]
    private Button mReceiveButton;
    [SerializeField]
    private Text mTitleText;
    [SerializeField]
    private Text mCurrentValueText;
    [SerializeField]
    private Text mTargetValueText;
    [SerializeField]
    private ImageArray mBackgroundImageArray;
    [SerializeField]
    private GameObject mReceivedImage;
    private CoinBuyUseBonusParam mBonusParam;
    private CoinBuyUseBonusContentParam mContentParam;

    public CoinBuyUseBonusContent()
    {
      base.\u002Ector();
    }

    public void Refresh(CoinBuyUseBonusParam bonus_param, CoinBuyUseBonusContentParam content_param)
    {
      IEnumerator enumerator = this.mRewardIconParent.get_transform().GetEnumerator();
      try
      {
        while (enumerator.MoveNext())
        {
          Transform current = (Transform) enumerator.Current;
          ((Component) current).get_gameObject().SetActive(false);
          UnityEngine.Object.Destroy((UnityEngine.Object) ((Component) current).get_gameObject());
        }
      }
      finally
      {
        if (enumerator is IDisposable disposable)
          disposable.Dispose();
      }
      this.mBonusParam = bonus_param;
      this.mContentParam = content_param;
      bool flag = MonoSingleton<GameManager>.Instance.Player.IsReceivableCoinBuyUseBonus(this.mBonusParam.Iname, this.mContentParam.Num);
      ((Selectable) this.mReceiveButton).set_interactable(flag);
      this.mBackgroundImageArray.ImageIndex = !flag ? 0 : 1;
      this.mReceivedImage.SetActive(MonoSingleton<GameManager>.Instance.Player.IsReceivedCoinBuyUseBonus(this.mBonusParam.Iname, this.mContentParam.Num));
      this.mTitleText.set_text(content_param.Name);
      this.mCurrentValueText.set_text(MonoSingleton<GameManager>.Instance.Player.GetCoinBuyUseBonusProgress(this.mBonusParam.Iname).ToString());
      this.mTargetValueText.set_text(content_param.Num.ToString());
      for (int index = 0; index < content_param.RewardParam.Rewards.Length; ++index)
      {
        CoinBuyUseBonusItemParam reward = content_param.RewardParam.Rewards[index];
        switch (reward.Type)
        {
          case eCoinBuyUseBonusRewardType.Item:
            this.SetParam_Item(reward);
            break;
          case eCoinBuyUseBonusRewardType.Gold:
            this.SetParam_Gold(reward);
            break;
          case eCoinBuyUseBonusRewardType.Coin:
            this.SetParam_Coin(reward);
            break;
          case eCoinBuyUseBonusRewardType.Artifact:
            this.SetParam_Artifact(reward);
            break;
          case eCoinBuyUseBonusRewardType.Unit:
            this.SetParam_Unit(reward);
            break;
          case eCoinBuyUseBonusRewardType.ConceptCard:
            this.SetParam_ConceptCard(reward);
            break;
          case eCoinBuyUseBonusRewardType.Award:
            this.SetParam_Award(reward);
            break;
        }
      }
    }

    public void OnClickReceiveReward()
    {
      if (this.mBonusParam == null || this.mContentParam == null)
        return;
      CoinBuyUseBonusWindow.Instance.ReceiveReward(this.mBonusParam, this.mContentParam);
    }

    private void SetParam_Item(CoinBuyUseBonusItemParam reward)
    {
      GameObject gameObject = (GameObject) UnityEngine.Object.Instantiate<GameObject>((M0) CoinBuyUseBonusWindow.Instance.ItemIcon);
      gameObject.get_transform().SetParent(this.mRewardIconParent.get_transform(), false);
      gameObject.SetActive(true);
      ItemParam itemParam = MonoSingleton<GameManager>.Instance.MasterParam.GetItemParam(reward.Item);
      DataSource.Bind<ItemParam>(gameObject, itemParam, false);
      DataSource.Bind<int>(gameObject, reward.Num, false);
    }

    private void SetParam_Gold(CoinBuyUseBonusItemParam reward)
    {
      GameObject gameObject = (GameObject) UnityEngine.Object.Instantiate<GameObject>((M0) CoinBuyUseBonusWindow.Instance.GoldIcon);
      gameObject.get_transform().SetParent(this.mRewardIconParent.get_transform(), false);
      gameObject.SetActive(true);
      DataSource.Bind<int>(gameObject, reward.Num, false);
    }

    private void SetParam_Coin(CoinBuyUseBonusItemParam reward)
    {
      GameObject gameObject = (GameObject) UnityEngine.Object.Instantiate<GameObject>((M0) CoinBuyUseBonusWindow.Instance.CoinIcon);
      gameObject.get_transform().SetParent(this.mRewardIconParent.get_transform(), false);
      gameObject.SetActive(true);
      DataSource.Bind<int>(gameObject, reward.Num, false);
    }

    private void SetParam_Artifact(CoinBuyUseBonusItemParam reward)
    {
      GameObject gameObject = (GameObject) UnityEngine.Object.Instantiate<GameObject>((M0) CoinBuyUseBonusWindow.Instance.ArtifactIcon);
      gameObject.get_transform().SetParent(this.mRewardIconParent.get_transform(), false);
      gameObject.SetActive(true);
      ArtifactParam artifactParam = MonoSingleton<GameManager>.Instance.MasterParam.GetArtifactParam(reward.Item);
      DataSource.Bind<ArtifactParam>(gameObject, artifactParam, false);
      ArtifactIcon component = (ArtifactIcon) gameObject.GetComponent<ArtifactIcon>();
      if (!UnityEngine.Object.op_Inequality((UnityEngine.Object) component, (UnityEngine.Object) null))
        return;
      component.UpdateValue();
    }

    private void SetParam_Unit(CoinBuyUseBonusItemParam reward)
    {
      GameObject gameObject = (GameObject) UnityEngine.Object.Instantiate<GameObject>((M0) CoinBuyUseBonusWindow.Instance.UnitIcon);
      gameObject.get_transform().SetParent(this.mRewardIconParent.get_transform(), false);
      gameObject.SetActive(true);
      UnitParam unitParam = MonoSingleton<GameManager>.Instance.MasterParam.GetUnitParam(reward.Item);
      DataSource.Bind<UnitParam>(gameObject, unitParam, false);
    }

    private void SetParam_ConceptCard(CoinBuyUseBonusItemParam reward)
    {
      GameObject gameObject = (GameObject) UnityEngine.Object.Instantiate<GameObject>((M0) CoinBuyUseBonusWindow.Instance.ConceptCardIcon);
      gameObject.get_transform().SetParent(this.mRewardIconParent.get_transform(), false);
      gameObject.SetActive(true);
      ConceptCardData cardDataForDisplay = ConceptCardData.CreateConceptCardDataForDisplay(reward.Item);
      if (cardDataForDisplay == null)
        return;
      ConceptCardIcon component = (ConceptCardIcon) gameObject.GetComponent<ConceptCardIcon>();
      if (!UnityEngine.Object.op_Inequality((UnityEngine.Object) component, (UnityEngine.Object) null))
        return;
      component.Setup(cardDataForDisplay);
      DataSource.Bind<int>(gameObject, reward.Num, false);
    }

    private void SetParam_Award(CoinBuyUseBonusItemParam reward)
    {
      GameObject gameObject = (GameObject) UnityEngine.Object.Instantiate<GameObject>((M0) CoinBuyUseBonusWindow.Instance.AwardIcon);
      gameObject.get_transform().SetParent(this.mRewardIconParent.get_transform(), false);
      gameObject.SetActive(true);
      AwardParam awardParam = MonoSingleton<GameManager>.Instance.MasterParam.GetAwardParam(reward.Item, true);
      DataSource.Bind<AwardParam>(gameObject, awardParam, false);
    }
  }
}
