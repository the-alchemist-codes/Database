﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_SkillAbilityDeriveParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_SkillAbilityDeriveParam
  {
    public string iname;
    public int trig_type_1;
    public string trig_iname_1;
    public int trig_type_2;
    public string trig_iname_2;
    public int trig_type_3;
    public string trig_iname_3;
    public string[] base_abils;
    public string[] derive_abils;
    public string[] base_skills;
    public string[] derive_skills;
  }
}
