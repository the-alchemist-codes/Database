﻿// Decompiled with JetBrains decompiler
// Type: SRPG.PredefinedGlobalEvents
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public enum PredefinedGlobalEvents
  {
    REFRESH_COIN_STATUS,
    REFRESH_GOLD_STATUS,
    UNLOAD_MENU,
    ERROR_NETWORK,
    BACK_NETWORK,
    MAINTENANCE_NETWORK,
    REFRESH_PLAYER_STATUS,
    ERROR_APP_QUIT,
    VERSION_MISMATCH_NETWORK,
    SERVER_NOTIFY,
  }
}
