﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqJukeBox
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using Gsc.Network.Encoding;
using MessagePack;
using System;

namespace SRPG
{
  public class ReqJukeBox : WebAPI
  {
    public ReqJukeBox(
      SRPG.Network.ResponseCallback response,
      EncodingTypes.ESerializeCompressMethod type)
    {
      this.name = "gallery/jukebox";
      this.body = WebAPI.GetRequestString(string.Empty);
      this.callback = response;
      this.serializeCompressMethod = type;
    }

    [MessagePackObject(true)]
    [Serializable]
    public class Response
    {
      public string[] bgms;
      public JukeBoxWindow.ResPlayList[] playlists;
    }
  }
}
