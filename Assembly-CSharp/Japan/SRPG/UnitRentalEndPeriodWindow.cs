﻿// Decompiled with JetBrains decompiler
// Type: SRPG.UnitRentalEndPeriodWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;

namespace SRPG
{
  public class UnitRentalEndPeriodWindow : MonoBehaviour
  {
    public UnitRentalEndPeriodWindow()
    {
      base.\u002Ector();
    }

    private void Start()
    {
      GameManager instance = MonoSingleton<GameManager>.Instance;
      if (!Object.op_Implicit((Object) instance))
        return;
      UnitData rentalUnit = instance.Player.GetRentalUnit();
      if (rentalUnit == null)
        return;
      DataSource.Bind<UnitData>(((Component) this).get_gameObject(), rentalUnit, false);
      GameParameter.UpdateAll(((Component) this).get_gameObject());
    }
  }
}
