﻿// Decompiled with JetBrains decompiler
// Type: SRPG.SkillPowerUpResultContent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class SkillPowerUpResultContent : MonoBehaviour
  {
    [SerializeField]
    private SkillPowerUpResultContentStatus statusBase;
    [SerializeField]
    private Text skillNameText;
    private List<SkillPowerUpResultContentStatus> contentsList;

    public SkillPowerUpResultContent()
    {
      base.\u002Ector();
    }

    public void Start()
    {
      ((Component) this.statusBase).get_gameObject().SetActive(false);
    }

    public void SetData(
      SkillPowerUpResultContent.Param param,
      List<ParamTypes> dispTypeList,
      List<ParamTypes> dispTypeListMul)
    {
      foreach (Component contents in this.contentsList)
        UnityEngine.Object.Destroy((UnityEngine.Object) contents.get_gameObject());
      this.contentsList.Clear();
      this.skillNameText.set_text(param.skilName);
      foreach (SkillPowerUpResultContent.DispType dispType in param.GetAllBonusChangeType())
      {
        if (!dispType.isScale && dispTypeList.Contains(dispType.type))
        {
          GameObject gameObject = (GameObject) UnityEngine.Object.Instantiate<GameObject>((M0) ((Component) this.statusBase).get_gameObject());
          gameObject.get_transform().SetParent(((Component) this.statusBase).get_transform().get_parent(), false);
          SkillPowerUpResultContentStatus component = (SkillPowerUpResultContentStatus) gameObject.GetComponent<SkillPowerUpResultContentStatus>();
          component.SetData(param, dispType.type, false);
          gameObject.SetActive(true);
          this.contentsList.Add(component);
        }
        if (dispType.isScale && dispTypeListMul.Contains(dispType.type))
        {
          GameObject gameObject = (GameObject) UnityEngine.Object.Instantiate<GameObject>((M0) ((Component) this.statusBase).get_gameObject());
          gameObject.get_transform().SetParent(((Component) this.statusBase).get_transform().get_parent(), false);
          SkillPowerUpResultContentStatus component = (SkillPowerUpResultContentStatus) gameObject.GetComponent<SkillPowerUpResultContentStatus>();
          component.SetData(param, dispType.type, true);
          gameObject.SetActive(true);
          this.contentsList.Add(component);
        }
      }
    }

    public class DispType
    {
      public bool isScale;
      public ParamTypes type;

      public DispType(ParamTypes inType, bool inIsScale)
      {
        this.type = inType;
        this.isScale = inIsScale;
      }
    }

    public class Param
    {
      private List<ParamTypes> typeList = new List<ParamTypes>();
      private List<ParamTypes> typeListMul = new List<ParamTypes>();
      public BaseStatus prevParam;
      public BaseStatus prevParamMul;
      public BaseStatus currentParam;
      public BaseStatus currentParamMul;
      public BaseStatus prevParamBonus;
      public BaseStatus prevParamBonusMul;
      public BaseStatus currentParamBonus;
      public BaseStatus currentParamBonusMul;

      public Param(
        SkillData groupSkill,
        ConceptCardData currentCardData,
        int levelCap,
        int awakeCountCap,
        int prevLevel,
        int prevAwakeCount,
        bool includeMaxPowerUp)
      {
        this.skilName = groupSkill.Name;
        this.prevParam = new BaseStatus();
        this.prevParamMul = new BaseStatus();
        this.currentParam = new BaseStatus();
        this.currentParamMul = new BaseStatus();
        this.prevParamBonus = new BaseStatus();
        this.prevParamBonusMul = new BaseStatus();
        this.currentParamBonus = new BaseStatus();
        this.currentParamBonusMul = new BaseStatus();
        ConceptCardParam.GetSkillAllStatus(groupSkill.SkillID, levelCap, (int) currentCardData.Lv, ref this.currentParam, ref this.currentParamMul);
        ConceptCardParam.GetSkillAllStatus(groupSkill.SkillID, levelCap, prevLevel, ref this.prevParam, ref this.currentParamMul);
        SkillPowerUpResultContent.Param.GetBonusStatus(groupSkill, currentCardData, levelCap, awakeCountCap, (int) currentCardData.Lv, (int) currentCardData.AwakeCount, ref this.currentParamBonus, ref this.currentParamBonusMul, includeMaxPowerUp);
        SkillPowerUpResultContent.Param.GetBonusStatus(groupSkill, currentCardData, levelCap, awakeCountCap, prevLevel, prevAwakeCount, ref this.prevParamBonus, ref this.currentParamBonusMul, false);
        int length = Enum.GetValues(typeof (ParamTypes)).Length;
        for (int index1 = 0; index1 < length; ++index1)
        {
          switch (index1)
          {
            case 2:
              continue;
            case 153:
              for (int index2 = 0; index2 < this.currentParamBonus.tokkou.Count; ++index2)
              {
                int num1 = 0;
                int num2 = (int) this.currentParamBonus.tokkou[index2].value;
                if (this.prevParamBonus.tokkou.Count > index2)
                  num1 = (int) this.prevParamBonus.tokkou[index2].value;
                if (num1 != num2)
                {
                  this.typeList.Add(ParamTypes.Tokkou);
                  break;
                }
              }
              for (int index2 = 0; index2 < this.currentParamBonusMul.tokkou.Count; ++index2)
              {
                int num1 = 0;
                int num2 = (int) this.currentParamBonusMul.tokkou[index2].value;
                if (this.prevParamBonusMul.tokkou.Count > index2)
                  num1 = (int) this.prevParamBonusMul.tokkou[index2].value;
                if (num1 != num2)
                {
                  this.typeListMul.Add(ParamTypes.Tokkou);
                  break;
                }
              }
              continue;
            case 190:
              for (int index2 = 0; index2 < this.currentParamBonus.tokubou.Count; ++index2)
              {
                int num1 = 0;
                int num2 = (int) this.currentParamBonus.tokubou[index2].value;
                if (this.prevParamBonus.tokubou.Count > index2)
                  num1 = (int) this.prevParamBonus.tokubou[index2].value;
                if (num1 != num2)
                {
                  this.typeList.Add(ParamTypes.Tokubou);
                  break;
                }
              }
              for (int index2 = 0; index2 < this.currentParamBonusMul.tokubou.Count; ++index2)
              {
                int num1 = 0;
                int num2 = (int) this.currentParamBonusMul.tokubou[index2].value;
                if (this.prevParamBonusMul.tokubou.Count > index2)
                  num1 = (int) this.prevParamBonusMul.tokubou[index2].value;
                if (num1 != num2)
                {
                  this.typeListMul.Add(ParamTypes.Tokubou);
                  break;
                }
              }
              continue;
            default:
              ParamTypes index3 = (ParamTypes) index1;
              if (this.prevParamBonus[index3] != this.currentParamBonus[index3])
                this.typeList.Add(index3);
              if (this.prevParamBonusMul[index3] != this.currentParamBonusMul[index3])
              {
                this.typeListMul.Add(index3);
                continue;
              }
              continue;
          }
        }
      }

      public string skilName { get; private set; }

      public int GetBonusChangeTypeNum(bool scalingStatus)
      {
        return scalingStatus ? this.typeListMul.Count : this.typeList.Count;
      }

      public bool IsBonusParamChanged()
      {
        return this.typeListMul.Count > 0 || this.typeList.Count > 0;
      }

      [DebuggerHidden]
      public IEnumerable<SkillPowerUpResultContent.DispType> GetAllBonusChangeType()
      {
        // ISSUE: object of a compiler-generated type is created
        // ISSUE: variable of a compiler-generated type
        SkillPowerUpResultContent.Param.\u003CGetAllBonusChangeType\u003Ec__Iterator0 changeTypeCIterator0 = new SkillPowerUpResultContent.Param.\u003CGetAllBonusChangeType\u003Ec__Iterator0()
        {
          \u0024this = this
        };
        // ISSUE: reference to a compiler-generated field
        changeTypeCIterator0.\u0024PC = -2;
        return (IEnumerable<SkillPowerUpResultContent.DispType>) changeTypeCIterator0;
      }

      private static void GetBonusStatus(
        SkillData groupSkill,
        ConceptCardData currentCardData,
        int levelCap,
        int awakeCountCap,
        int level,
        int awakeCount,
        ref BaseStatus bonusAdd,
        ref BaseStatus bonusScale,
        bool includeMaxPowerUp)
      {
        if (currentCardData.EquipEffects == null)
          return;
        foreach (ConceptCardEquipEffect equipEffect in currentCardData.EquipEffects)
        {
          if (equipEffect.CardSkill != null && equipEffect.CardSkill.Name == groupSkill.Name)
          {
            equipEffect.GetAddCardSkillBuffStatusAwake(awakeCount, awakeCountCap, ref bonusAdd, ref bonusScale);
            if (!includeMaxPowerUp || level != levelCap || awakeCount != awakeCountCap)
              break;
            BaseStatus total_add = new BaseStatus();
            BaseStatus total_scale = new BaseStatus();
            equipEffect.GetAddCardSkillBuffStatusLvMax(level, levelCap, awakeCount, ref total_add, ref total_scale);
            bonusAdd.Add(total_add);
            bonusScale.Add(total_scale);
            break;
          }
        }
      }
    }
  }
}
