﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RuneDrawEvoState
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace SRPG
{
  public class RuneDrawEvoState : MonoBehaviour
  {
    [SerializeField]
    private GameObject mEvoStatusParentOn;
    [SerializeField]
    private GameObject mEvoStatusParentOff;
    [SerializeField]
    private RuneDrawEvoStateOneSetting mEvoStateOne;
    private BindRuneData mRuneData;
    private List<RuneDrawEvoStateOneSetting> mRuneEvoStatusList;
    private ColorBlock mKeepButtonColor;
    private bool mIsUseKeepButtonColor;

    public RuneDrawEvoState()
    {
      base.\u002Ector();
    }

    private event RuneDrawEvoState.OnSelectedEvent mOnSelectedEvent;

    public void SetSelectedCallBack(RuneDrawEvoState.OnSelectedEvent selected)
    {
      this.mOnSelectedEvent = selected;
    }

    public void OnClickEvoItem(int index)
    {
      this.mOnSelectedEvent(index);
    }

    public void Awake()
    {
    }

    public void SetDrawParam(BindRuneData rune_data)
    {
      this.mRuneData = rune_data;
      this.CloneEvoStatusList();
      this.Refresh();
    }

    public void ShowFrame(bool is_show, int index = 0)
    {
      this.RefreshFrame();
      for (int index1 = 0; index1 < this.mRuneEvoStatusList.Count; ++index1)
      {
        if (index1 == index)
          this.mRuneEvoStatusList[index].SetShowFrame(is_show);
        else
          this.mRuneEvoStatusList[index1].SetShowFrame(false);
      }
    }

    public void SetDisableColor(bool is_hilight, int index)
    {
      if (index >= this.mRuneEvoStatusList.Count)
        return;
      Button component = (Button) ((Component) this.mRuneEvoStatusList[index]).GetComponent<Button>();
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) component, (UnityEngine.Object) null))
        return;
      if (is_hilight)
      {
        this.mKeepButtonColor = ((Selectable) component).get_colors();
        this.mIsUseKeepButtonColor = true;
        ColorBlock colorBlock = (ColorBlock) null;
        ref ColorBlock local1 = ref colorBlock;
        ColorBlock colors1 = ((Selectable) component).get_colors();
        Color normalColor = ((ColorBlock) ref colors1).get_normalColor();
        ((ColorBlock) ref local1).set_normalColor(normalColor);
        ref ColorBlock local2 = ref colorBlock;
        ColorBlock colors2 = ((Selectable) component).get_colors();
        Color highlightedColor1 = ((ColorBlock) ref colors2).get_highlightedColor();
        ((ColorBlock) ref local2).set_highlightedColor(highlightedColor1);
        ref ColorBlock local3 = ref colorBlock;
        ColorBlock colors3 = ((Selectable) component).get_colors();
        Color pressedColor = ((ColorBlock) ref colors3).get_pressedColor();
        ((ColorBlock) ref local3).set_pressedColor(pressedColor);
        ref ColorBlock local4 = ref colorBlock;
        ColorBlock colors4 = ((Selectable) component).get_colors();
        Color highlightedColor2 = ((ColorBlock) ref colors4).get_highlightedColor();
        ((ColorBlock) ref local4).set_disabledColor(highlightedColor2);
        ref ColorBlock local5 = ref colorBlock;
        ColorBlock colors5 = ((Selectable) component).get_colors();
        double colorMultiplier = (double) ((ColorBlock) ref colors5).get_colorMultiplier();
        ((ColorBlock) ref local5).set_colorMultiplier((float) colorMultiplier);
        ref ColorBlock local6 = ref colorBlock;
        ColorBlock colors6 = ((Selectable) component).get_colors();
        double fadeDuration = (double) ((ColorBlock) ref colors6).get_fadeDuration();
        ((ColorBlock) ref local6).set_fadeDuration((float) fadeDuration);
        ((Selectable) component).set_colors(colorBlock);
      }
      else
      {
        if (!this.mIsUseKeepButtonColor)
          return;
        ((Selectable) component).set_colors(this.mKeepButtonColor);
      }
    }

    public void StartGaugeAnim(int index)
    {
      if (index >= this.mRuneEvoStatusList.Count)
        return;
      this.mRuneEvoStatusList[index].StartGaugeAnim();
    }

    public void Refresh()
    {
      this.RefreshEvoStatus();
      this.RefreshFrame();
    }

    public void RefreshFrame()
    {
      foreach (RuneDrawEvoStateOneSetting mRuneEvoStatus in this.mRuneEvoStatusList)
        mRuneEvoStatus.Refresh();
    }

    public void RefreshEvoStatus()
    {
      if (this.mRuneData == null)
        return;
      RuneData rune = this.mRuneData.Rune;
      if (rune == null)
        return;
      GameUtility.SetGameObjectActive(this.mEvoStatusParentOn, rune.state.evo_state.Count > 0);
      GameUtility.SetGameObjectActive(this.mEvoStatusParentOff, rune.state.evo_state.Count <= 0);
      this.DrawEvoStatusList();
    }

    public void CloneEvoStatusList()
    {
      if (this.mRuneData == null || UnityEngine.Object.op_Equality((UnityEngine.Object) this.mEvoStateOne, (UnityEngine.Object) null))
        return;
      RuneData rune = this.mRuneData.Rune;
      if (rune == null)
        return;
      ((Component) this.mEvoStateOne).get_gameObject().SetActive(false);
      this.mRuneEvoStatusList.ForEach((Action<RuneDrawEvoStateOneSetting>) (evo => UnityEngine.Object.Destroy((UnityEngine.Object) ((Component) evo).get_gameObject())));
      this.mRuneEvoStatusList.Clear();
      for (int index = 0; index < MonoSingleton<GameManager>.Instance.MasterParam.FixParam.RuneMaxEvoNum; ++index)
      {
        GameObject gameObject = (GameObject) UnityEngine.Object.Instantiate<GameObject>((M0) ((Component) this.mEvoStateOne).get_gameObject());
        gameObject.get_transform().SetParent(((Component) this.mEvoStateOne).get_transform().get_parent(), false);
        gameObject.SetActive(true);
        RuneDrawEvoStateOneSetting component = (RuneDrawEvoStateOneSetting) gameObject.GetComponent<RuneDrawEvoStateOneSetting>();
        if (UnityEngine.Object.op_Equality((UnityEngine.Object) component, (UnityEngine.Object) null))
          break;
        Button componentInChildren = (Button) gameObject.GetComponentInChildren<Button>();
        if (UnityEngine.Object.op_Inequality((UnityEngine.Object) componentInChildren, (UnityEngine.Object) null))
        {
          if (rune != null && index < rune.GetLengthFromEvoParam())
          {
            // ISSUE: object of a compiler-generated type is created
            // ISSUE: variable of a compiler-generated type
            RuneDrawEvoState.\u003CCloneEvoStatusList\u003Ec__AnonStorey0 listCAnonStorey0 = new RuneDrawEvoState.\u003CCloneEvoStatusList\u003Ec__AnonStorey0();
            // ISSUE: reference to a compiler-generated field
            listCAnonStorey0.\u0024this = this;
            // ISSUE: reference to a compiler-generated field
            listCAnonStorey0.number = index;
            ((Selectable) componentInChildren).set_interactable(true);
            // ISSUE: method pointer
            ((UnityEvent) componentInChildren.get_onClick()).AddListener(new UnityAction((object) listCAnonStorey0, __methodptr(\u003C\u003Em__0)));
          }
          else
            ((Selectable) componentInChildren).set_interactable(false);
        }
        component.Refresh();
        this.mRuneEvoStatusList.Add(component);
      }
    }

    public void DrawEvoStatusList()
    {
      if (this.mRuneData == null)
        return;
      RuneData rune = this.mRuneData.Rune;
      if (rune == null)
        return;
      for (int index = 0; index < this.mRuneEvoStatusList.Count; ++index)
      {
        BaseStatus addStatus = (BaseStatus) null;
        BaseStatus scaleStatus = (BaseStatus) null;
        if (index < rune.GetLengthFromEvoParam())
        {
          rune.CreateBaseStatusFromEvoParam(index, ref addStatus, ref scaleStatus, true);
          if (addStatus == null)
            addStatus = new BaseStatus();
          if (scaleStatus == null)
            scaleStatus = new BaseStatus();
          float percentage = rune.PowerPercentageFromEvoParam(index);
          this.mRuneEvoStatusList[index].SetStatus(addStatus, scaleStatus, percentage, false, 0.0f);
        }
        else
          this.mRuneEvoStatusList[index].SetStatus((BaseStatus) null, (BaseStatus) null, 0.0f, false, 0.0f);
      }
    }

    public delegate void OnSelectedEvent(int index);
  }
}
