﻿// Decompiled with JetBrains decompiler
// Type: SRPG.EBirth
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public enum EBirth
  {
    Unknown = 0,
    Envy = 1,
    Wrath = 2,
    Sloth = 3,
    Lust = 4,
    Gluttony = 5,
    Greed = 6,
    Saga = 7,
    Wadatsumi = 8,
    Desert = 9,
    Northan = 10, // 0x0000000A
    LostBlue = 11, // 0x0000000B
    Other = 100, // 0x00000064
  }
}
