﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildRoleBonusDetail
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class GuildRoleBonusDetail
  {
    private int mLevel;
    private string mRewardID;

    public int lv
    {
      get
      {
        return this.mLevel;
      }
    }

    public string reward_id
    {
      get
      {
        return this.mRewardID;
      }
    }

    public bool Deserialize(JSON_GuildRoleBonusDetail json)
    {
      if (json == null)
        return false;
      this.mLevel = json.lv;
      this.mRewardID = json.reward_id;
      return true;
    }
  }
}
