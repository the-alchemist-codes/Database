﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_ReqGvGReward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using Gsc.Network.Encoding;
using MessagePack;
using System;
using UnityEngine;

namespace SRPG
{
  [FlowNode.NodeType("GvG/Req/GvGReward", 32741)]
  [FlowNode.Pin(1, "Request", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(101, "Success", FlowNode.PinTypes.Output, 101)]
  [FlowNode.Pin(102, "Not Accept", FlowNode.PinTypes.Output, 102)]
  public class FlowNode_ReqGvGReward : FlowNode_Network
  {
    [SerializeField]
    private bool IsHomeGvGReward = true;
    [SerializeField]
    private bool IsResultGvGReward = true;
    protected const int PIN_IN_REQUEST = 1;
    protected const int PIN_OUT_SUCCESS = 101;
    protected const int PIN_OUT_NOTACCEPT = 102;

    public override void OnActivate(int pinID)
    {
      if (pinID != 1 || ((Behaviour) this).get_enabled())
        return;
      ((Behaviour) this).set_enabled(true);
      if (!MonoSingleton<GameManager>.Instance.Player.HasGvGReward && this.IsHomeGvGReward)
        return;
      GvGManager instance = GvGManager.Instance;
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) instance, (UnityEngine.Object) null) && instance.ResultDaily == null && (instance.ResultSeason == null && this.IsResultGvGReward))
        return;
      MonoSingleton<GameManager>.Instance.Player.HasGvGReward = false;
      if (MonoSingleton<GameManager>.Instance.Player.PlayerGuild == null)
        return;
      this.SerializeCompressMethod = EncodingTypes.ESerializeCompressMethod.TYPED_MESSAGE_PACK;
      this.ExecRequest((WebAPI) new ReqGvGReward(new SRPG.Network.ResponseCallback(((FlowNode_Network) this).ResponseCallback), this.SerializeCompressMethod));
    }

    private void Success()
    {
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) this, (UnityEngine.Object) null))
        return;
      ((Behaviour) this).set_enabled(false);
      this.ActivateOutputLinks(101);
    }

    public override void OnSuccess(WWWResult www)
    {
      ReqGvGReward.Response response = (ReqGvGReward.Response) null;
      bool flag = EncodingTypes.IsJsonSerializeCompressSelected(this.SerializeCompressMethod);
      if (!flag)
      {
        FlowNode_ReqGvGReward.MP_Response mpResponse = SerializerCompressorHelper.Decode<FlowNode_ReqGvGReward.MP_Response>(www.rawResult, true, EncodingTypes.GetCompressModeFromSerializeCompressMethod(this.SerializeCompressMethod), false, true);
        DebugUtility.Assert(mpResponse != null, "mp_res == null");
        SRPG.Network.EErrCode stat = (SRPG.Network.EErrCode) mpResponse.stat;
        string statMsg = mpResponse.stat_msg;
        if (stat != SRPG.Network.EErrCode.Success)
          SRPG.Network.SetServerMetaDataAsError(stat, statMsg);
        response = mpResponse.body;
      }
      if (SRPG.Network.IsError)
      {
        int errCode = (int) SRPG.Network.ErrCode;
        this.OnRetry();
      }
      else
      {
        if (flag)
        {
          WebAPI.JSON_BodyResponse<ReqGvGReward.Response> jsonObject = JSONParser.parseJSONObject<WebAPI.JSON_BodyResponse<ReqGvGReward.Response>>(www.text);
          DebugUtility.Assert(jsonObject != null, "res == null");
          if (jsonObject.body == null)
          {
            this.OnRetry();
            return;
          }
          response = jsonObject.body;
        }
        SRPG.Network.RemoveAPI();
        if (response.status != 1)
        {
          this.ActivateOutputLinks(102);
        }
        else
        {
          try
          {
            GvGManager.Instance.SetupResultReward(response.reward_ids);
          }
          catch (Exception ex)
          {
            DebugUtility.LogException(ex);
            this.OnFailed();
            return;
          }
          this.Success();
        }
      }
    }

    private enum GvGRewardStatus
    {
      NoneReward,
      HomeGift,
      NotReward,
      RewardTimeOut,
    }

    [MessagePackObject(true)]
    public class MP_Response : WebAPI.JSON_BaseResponse
    {
      public ReqGvGReward.Response body;
    }
  }
}
