﻿// Decompiled with JetBrains decompiler
// Type: SRPG.BaseObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public abstract class BaseObject
  {
    private bool mInitialized;
    private bool mPaused;

    public bool IsInitialized
    {
      set
      {
        this.mInitialized = value;
      }
      get
      {
        return this.mInitialized;
      }
    }

    public bool IsPaused
    {
      set
      {
        this.mPaused = value;
      }
      get
      {
        return this.mPaused;
      }
    }

    public virtual bool Load()
    {
      return true;
    }

    public virtual void Release()
    {
    }

    public virtual void Update()
    {
    }
  }
}
