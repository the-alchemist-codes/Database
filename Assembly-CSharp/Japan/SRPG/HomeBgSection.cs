﻿// Decompiled with JetBrains decompiler
// Type: SRPG.HomeBgSection
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;

namespace SRPG
{
  public class HomeBgSection
  {
    public static void SetSection_SectionId(string section_id)
    {
      if (string.IsNullOrEmpty(section_id) || !HomeBgSection.IsStorySection(section_id))
        return;
      GlobalVars.HomeBgSection.Set(section_id);
    }

    public static void SetSection_ChapterId(string chapter_id)
    {
      ChapterParam area = MonoSingleton<GameManager>.Instance.FindArea(chapter_id);
      if (area == null || !HomeBgSection.IsStorySection(area.section))
        return;
      GlobalVars.HomeBgSection.Set(area.section);
    }

    public static void SetSection_QuestId(string quest_id)
    {
      QuestParam quest = MonoSingleton<GameManager>.Instance.FindQuest(quest_id);
      if (quest == null || quest.Chapter == null || !HomeBgSection.IsStorySection(quest.Chapter.section))
        return;
      GlobalVars.HomeBgSection.Set(quest.Chapter.section);
    }

    private static bool IsStorySection(string section_iname)
    {
      SectionParam world = MonoSingleton<GameManager>.Instance.FindWorld(section_iname);
      return world != null && !string.IsNullOrEmpty(world.home);
    }
  }
}
