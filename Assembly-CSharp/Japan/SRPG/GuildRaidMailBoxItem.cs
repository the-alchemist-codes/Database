﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildRaidMailBoxItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class GuildRaidMailBoxItem : MonoBehaviour
  {
    [SerializeField]
    private SRPG_Button GetReceiveButton;
    [SerializeField]
    private GameObject RewardUnit;
    [SerializeField]
    private GameObject RewardItem;
    [SerializeField]
    private GameObject RewardCard;
    [SerializeField]
    private GameObject RewardArtifact;
    [SerializeField]
    private GameObject RewardAward;
    [SerializeField]
    private GameObject RewardGold;
    [SerializeField]
    private GameObject RewardCoin;

    public GuildRaidMailBoxItem()
    {
      base.\u002Ector();
    }

    public void Setup(GuildRaidMailListItem item, SRPG_Button.ButtonClickEvent callback)
    {
      GameUtility.SetGameObjectActive(this.RewardUnit, false);
      GameUtility.SetGameObjectActive(this.RewardItem, false);
      GameUtility.SetGameObjectActive(this.RewardCard, false);
      GameUtility.SetGameObjectActive(this.RewardArtifact, false);
      GameUtility.SetGameObjectActive(this.RewardAward, false);
      GameUtility.SetGameObjectActive(this.RewardGold, false);
      GameUtility.SetGameObjectActive(this.RewardCoin, false);
      DataSource.Bind<GuildRaidMailListItem>(((Component) this).get_gameObject(), item, false);
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.GetReceiveButton, (UnityEngine.Object) null))
        this.GetReceiveButton.AddListener(callback);
      GameManager gm = MonoSingleton<GameManager>.Instance;
      GuildRaidRewardParam guildRaidRewardParam = gm.GetGuildRaidRewardParam(item.RewardId);
      if (guildRaidRewardParam == null)
        return;
      GuildRaidBossParam guildRaidBossParam = gm.GetGuildRaidBossParam(item.BossId);
      if (guildRaidBossParam == null)
        return;
      DataSource.Bind<GuildRaidBossParam>(((Component) this).get_gameObject(), guildRaidBossParam, false);
      UnitParam unitParam = MonoSingleton<GameManager>.Instance.GetUnitParam(guildRaidBossParam.UnitIName);
      if (unitParam != null)
        DataSource.Bind<UnitParam>(((Component) this).get_gameObject(), unitParam, false);
      guildRaidRewardParam.Rewards.ForEach((Action<GuildRaidReward>) (reward =>
      {
        GameObject gameObject = (GameObject) null;
        bool flag = false;
        switch (reward.Type)
        {
          case RaidRewardType.Item:
            ItemParam itemParam = gm.GetItemParam(reward.IName);
            if (itemParam == null)
              return;
            gameObject = (GameObject) UnityEngine.Object.Instantiate<GameObject>((M0) this.RewardItem, this.RewardItem.get_transform().get_parent());
            DataSource.Bind<ItemParam>(gameObject, itemParam, false);
            flag = true;
            break;
          case RaidRewardType.Gold:
            gameObject = (GameObject) UnityEngine.Object.Instantiate<GameObject>((M0) this.RewardGold, this.RewardGold.get_transform().get_parent());
            flag = true;
            break;
          case RaidRewardType.Coin:
            gameObject = (GameObject) UnityEngine.Object.Instantiate<GameObject>((M0) this.RewardCoin, this.RewardCoin.get_transform().get_parent());
            flag = true;
            break;
          case RaidRewardType.Award:
            AwardParam awardParam = gm.GetAwardParam(reward.IName);
            if (awardParam == null)
              return;
            gameObject = (GameObject) UnityEngine.Object.Instantiate<GameObject>((M0) this.RewardAward, this.RewardAward.get_transform().get_parent());
            DataSource.Bind<AwardParam>(gameObject, awardParam, false);
            break;
          case RaidRewardType.Unit:
            return;
          case RaidRewardType.ConceptCard:
            ConceptCardData cardDataForDisplay = ConceptCardData.CreateConceptCardDataForDisplay(reward.IName);
            if (cardDataForDisplay != null)
            {
              gameObject = (GameObject) UnityEngine.Object.Instantiate<GameObject>((M0) this.RewardCard, this.RewardCard.get_transform().get_parent());
              ConceptCardIcon component = (ConceptCardIcon) gameObject.GetComponent<ConceptCardIcon>();
              if (UnityEngine.Object.op_Inequality((UnityEngine.Object) component, (UnityEngine.Object) null))
              {
                component.Setup(cardDataForDisplay);
                break;
              }
              break;
            }
            break;
          case RaidRewardType.Artifact:
            ArtifactParam artifactParam = gm.MasterParam.GetArtifactParam(reward.IName);
            if (artifactParam == null)
              return;
            gameObject = (GameObject) UnityEngine.Object.Instantiate<GameObject>((M0) this.RewardArtifact, this.RewardArtifact.get_transform().get_parent());
            DataSource.Bind<ArtifactParam>(gameObject, artifactParam, false);
            break;
          default:
            return;
        }
        if (flag)
        {
          Transform transform = gameObject.get_transform().Find("amount/Text_amount");
          if (UnityEngine.Object.op_Inequality((UnityEngine.Object) transform, (UnityEngine.Object) null))
          {
            Text component = (Text) ((Component) transform).GetComponent<Text>();
            if (UnityEngine.Object.op_Inequality((UnityEngine.Object) component, (UnityEngine.Object) null))
              component.set_text(reward.Num.ToString());
          }
        }
        gameObject.SetActive(true);
      }));
    }
  }
}
