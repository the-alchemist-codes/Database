﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildRaidRankingMemberBoss
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class GuildRaidRankingMemberBoss
  {
    public int BossId { get; private set; }

    public int Rank { get; private set; }

    public bool Deserialize(JSON_GuildRaidRankingMemberBoss json)
    {
      this.BossId = json.boss_id;
      this.Rank = json.rank;
      return true;
    }
  }
}
