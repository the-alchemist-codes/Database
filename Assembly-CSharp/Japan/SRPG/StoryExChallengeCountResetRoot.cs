﻿// Decompiled with JetBrains decompiler
// Type: SRPG.StoryExChallengeCountResetRoot
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;

namespace SRPG
{
  [FlowNode.Pin(10, "開始", FlowNode.PinTypes.Input, 10)]
  [FlowNode.Pin(100, "アイテムで挑戦回数リセット", FlowNode.PinTypes.Output, 100)]
  [FlowNode.Pin(200, "幻晶石で挑戦回数リセット", FlowNode.PinTypes.Output, 200)]
  [FlowNode.Pin(300, "エラー", FlowNode.PinTypes.Output, 300)]
  public class StoryExChallengeCountResetRoot : MonoBehaviour, IFlowInterface
  {
    private const int PIN_INPUT_START = 10;
    private const int PIN_OUTPUT_RESET_ITEM = 100;
    private const int PIN_OUTPUT_RESET_COIN = 200;
    private const int PIN_OUTPUT_ERROR = 300;

    public StoryExChallengeCountResetRoot()
    {
      base.\u002Ector();
    }

    public void Activated(int pinID)
    {
      if (pinID != 10)
        return;
      this.SendOutputPin();
    }

    private void SendOutputPin()
    {
      QuestParam quest = MonoSingleton<GameManager>.Instance.FindQuest(GlobalVars.SelectedQuestID);
      if (quest == null)
      {
        FlowNode_GameObject.ActivateOutputLinks((Component) this, 300);
      }
      else
      {
        ResetCostParam resetCost = MonoSingleton<GameManager>.Instance.MasterParam.FindResetCost(quest.ResetCost);
        if (resetCost == null)
        {
          FlowNode_GameObject.ActivateOutputLinks((Component) this, 300);
        }
        else
        {
          if (resetCost.IsEnableItemCost())
          {
            ResetCostInfoParam resetCostInfo = resetCost.GetResetCostInfo(eResetCostType.Item);
            int needNum = resetCostInfo.GetNeedNum((int) quest.dailyReset);
            if (MonoSingleton<GameManager>.Instance.Player.GetItemAmount(resetCostInfo.Item) >= needNum)
            {
              FlowNode_GameObject.ActivateOutputLinks((Component) this, 100);
              return;
            }
          }
          if (resetCost.IsEnableCoinCost())
            FlowNode_GameObject.ActivateOutputLinks((Component) this, 200);
          else
            FlowNode_GameObject.ActivateOutputLinks((Component) this, 300);
        }
      }
    }
  }
}
