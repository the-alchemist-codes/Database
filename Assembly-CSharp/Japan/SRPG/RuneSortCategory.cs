﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RuneSortCategory
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace SRPG
{
  public class RuneSortCategory : MonoBehaviour
  {
    [SerializeField]
    private Text mHeaderText;
    [SerializeField]
    private RuneSortToggleItem mToggleTemplate;
    private SortRuneParam mSortParam;
    private Dictionary<string, RuneSortToggleItem> mCreatedToggles;

    public RuneSortCategory()
    {
      base.\u002Ector();
    }

    public SortRuneParam SortParam
    {
      get
      {
        return this.mSortParam;
      }
    }

    public RuneSortToggleItem[] ToggleValues
    {
      get
      {
        return this.mCreatedToggles.Select<KeyValuePair<string, RuneSortToggleItem>, RuneSortToggleItem>((Func<KeyValuePair<string, RuneSortToggleItem>, RuneSortToggleItem>) (pair => pair.Value)).ToArray<RuneSortToggleItem>();
      }
    }

    public void Init(SortRuneParam sort_param)
    {
      this.mSortParam = sort_param;
      GameUtility.SetGameObjectActive((Component) this.mToggleTemplate, false);
      this.mHeaderText.set_text(sort_param.name);
      for (int index = 0; index < sort_param.conditions.Length; ++index)
      {
        if (!this.mCreatedToggles.ContainsKey(sort_param.conditions[index].cnds_iname))
        {
          RuneSortToggleItem runeSortToggleItem = (RuneSortToggleItem) UnityEngine.Object.Instantiate<RuneSortToggleItem>((M0) this.mToggleTemplate, ((Component) this).get_transform(), false);
          GameObject gameObject = ((Component) runeSortToggleItem).get_gameObject();
          // ISSUE: method pointer
          runeSortToggleItem.SetToggleValueChangeListner(new UnityAction<bool>((object) this, __methodptr(\u003CInit\u003Em__1)));
          gameObject.SetActive(true);
          this.mCreatedToggles.Add(sort_param.conditions[index].cnds_iname, runeSortToggleItem);
          runeSortToggleItem.SetName(sort_param.conditions[index].name);
          DataSource.Bind<SortRuneConditionParam>(gameObject, sort_param.conditions[index], false);
          if (UnityEngine.Object.op_Inequality((UnityEngine.Object) RuneSortWindow.Instance, (UnityEngine.Object) null))
          {
            bool flag = RuneSortWindow.Instance.CurrentSortPrefs.GetValue(sort_param.conditions[index].parent.iname, sort_param.conditions[index].cnds_iname, false);
            runeSortToggleItem.SetToggleSilient(flag);
          }
        }
      }
    }

    private void OnToggleValueChanged()
    {
      if (!UnityEngine.Object.op_Inequality((UnityEngine.Object) RuneFilterWindow.Instance, (UnityEngine.Object) null))
        return;
      RuneFilterWindow.Instance.UpdateTabState();
    }

    public bool IsExistActiveToggle()
    {
      foreach (RuneSortToggleItem runeSortToggleItem in this.mCreatedToggles.Values)
      {
        if (runeSortToggleItem.isOn)
          return true;
      }
      return false;
    }
  }
}
