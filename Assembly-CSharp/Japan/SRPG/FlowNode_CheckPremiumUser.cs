﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_CheckPremiumUser
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  [FlowNode.NodeType("System/Battle/Speed/BattleSpeedEditorOption", 32741)]
  [FlowNode.Pin(1, "Check", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(10, "Premium", FlowNode.PinTypes.Output, 2)]
  [FlowNode.Pin(20, "Not Premium", FlowNode.PinTypes.Output, 3)]
  public class FlowNode_CheckPremiumUser : FlowNode
  {
    public override void OnActivate(int pinID)
    {
      if (pinID == 1)
        this.ActivateOutputLinks(10);
      else
        this.ActivateOutputLinks(20);
    }
  }
}
