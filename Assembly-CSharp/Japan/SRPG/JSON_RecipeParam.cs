﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_RecipeParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_RecipeParam
  {
    public string iname;
    public int cost;
    public string mat1;
    public string mat2;
    public string mat3;
    public string mat4;
    public string mat5;
    public int num1;
    public int num2;
    public int num3;
    public int num4;
    public int num5;
  }
}
