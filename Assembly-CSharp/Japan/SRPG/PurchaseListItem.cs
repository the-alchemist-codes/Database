﻿// Decompiled with JetBrains decompiler
// Type: SRPG.PurchaseListItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class PurchaseListItem : MonoBehaviour
  {
    [SerializeField]
    private GameObject PurchaseBefore;
    [SerializeField]
    private GameObject Purchased;
    [SerializeField]
    private GameObject Purchasable;
    [SerializeField]
    private GameObject PurchaseIcon;
    [SerializeField]
    private RewardListItem PurchaseIconList;
    [SerializeField]
    private GameObject PurchaseBadge;
    [SerializeField]
    private ImageArray PurchaseBadgeImage;
    [SerializeField]
    private Text PurchaseBuyCountTitle;
    [SerializeField]
    private Text PurchaseBuyCountText;
    [SerializeField]
    private GameObject PurchaseBuyCountObject;
    [SerializeField]
    private GameObject PurchaseSoldOut;
    [SerializeField]
    private GameObject BonusItem;
    [SerializeField]
    private ImageArray CoinIcon;
    [SerializeField]
    private GameObject BuyCoinItemDetailWindow;
    private bool mIsVip;
    private bool mIsPremium;
    private bool mIsPurchased;
    private PaymentManager.Product mProduct;

    public PurchaseListItem()
    {
      base.\u002Ector();
    }

    public bool IsVip
    {
      get
      {
        return this.mIsVip;
      }
    }

    public bool IsPremium
    {
      get
      {
        return this.mIsPremium;
      }
    }

    public bool IsPurchased
    {
      get
      {
        return this.mIsPurchased;
      }
    }

    public int CoinNum
    {
      get
      {
        return this.mProduct != null ? this.mProduct.numFree + this.mProduct.numPaid : 0;
      }
    }

    public int CoinNumPaid
    {
      get
      {
        return this.mProduct != null ? this.mProduct.numPaid : 0;
      }
    }

    public PaymentManager.Product ProductID
    {
      get
      {
        return this.mProduct;
      }
    }

    public void Init(PaymentManager.Product product)
    {
      this.mProduct = product;
      string productId = product.productID;
      if (productId == (string) MonoSingleton<GameManager>.Instance.MasterParam.FixParam.VipCardProduct)
      {
        this.mIsVip = true;
        this.mIsPurchased = MonoSingleton<GameManager>.Instance.Player.CheckEnableVipCard();
        this.Purchasable.SetActive(false);
        this.PurchaseBefore.SetActive(false);
        this.Purchased.SetActive(false);
        if (this.mIsPurchased)
        {
          if ((MonoSingleton<GameManager>.Instance.Player.VipExpiredAt - TimeManager.FromUnixTime(Network.GetServerTime())).Days <= 3)
          {
            this.mIsPurchased = false;
            this.Purchasable.SetActive(true);
          }
          else
            this.Purchased.SetActive(true);
        }
        else
          this.PurchaseBefore.SetActive(true);
      }
      else if (productId == (string) MonoSingleton<GameManager>.Instance.MasterParam.FixParam.PremiumProduct)
      {
        this.mIsPremium = true;
        this.mIsPurchased = MonoSingleton<GameManager>.Instance.Player.CheckEnablePremiumMember();
        this.PurchaseBefore.SetActive(!this.mIsPurchased);
        this.Purchased.SetActive(this.mIsPurchased);
      }
      else
      {
        GameManager instance = MonoSingleton<GameManager>.Instance;
        GameObject gameObject = (GameObject) null;
        BuyCoinProductParam coinProductParam = instance.MasterParam.GetBuyCoinProductParam(product.ID);
        if (coinProductParam != null)
        {
          DataSource.Bind<BuyCoinProductParam>(((Component) this).get_gameObject(), coinProductParam, false);
          if (Object.op_Inequality((Object) this.PurchaseIcon, (Object) null) && Object.op_Inequality((Object) this.PurchaseIconList, (Object) null))
          {
            BuyCoinRewardParam buyCoinRewardParam = instance.MasterParam.GetBuyCoinRewardParam(coinProductParam.Reward);
            if (buyCoinRewardParam != null)
            {
              switch (buyCoinRewardParam.DrawType)
              {
                case BuyCoinManager.PremiumRewadType.Item:
                  ItemParam itemParam = instance.GetItemParam(buyCoinRewardParam.DrawIname);
                  if (itemParam != null)
                  {
                    gameObject = this.PurchaseIconList.RewardItem;
                    DataSource.Bind<ItemParam>(gameObject, itemParam, false);
                    break;
                  }
                  break;
                case BuyCoinManager.PremiumRewadType.Gold:
                  gameObject = this.PurchaseIconList.RewardGold;
                  break;
                case BuyCoinManager.PremiumRewadType.Coin:
                  gameObject = this.PurchaseIconList.RewardCoin;
                  break;
                case BuyCoinManager.PremiumRewadType.Artifact:
                  ArtifactParam artifactParam = instance.MasterParam.GetArtifactParam(buyCoinRewardParam.DrawIname);
                  if (artifactParam != null)
                  {
                    gameObject = this.PurchaseIconList.RewardArtifact;
                    DataSource.Bind<ArtifactParam>(gameObject, artifactParam, false);
                    break;
                  }
                  break;
                case BuyCoinManager.PremiumRewadType.ConceptCard:
                  ConceptCardData cardDataForDisplay = ConceptCardData.CreateConceptCardDataForDisplay(buyCoinRewardParam.DrawIname);
                  if (cardDataForDisplay != null)
                  {
                    gameObject = this.PurchaseIconList.RewardCard;
                    ConceptCardIcon component = (ConceptCardIcon) gameObject.GetComponent<ConceptCardIcon>();
                    if (Object.op_Inequality((Object) component, (Object) null))
                    {
                      component.Setup(cardDataForDisplay);
                      break;
                    }
                    break;
                  }
                  break;
                case BuyCoinManager.PremiumRewadType.Unit:
                  UnitParam unitParam = instance.GetUnitParam(buyCoinRewardParam.DrawIname);
                  if (unitParam != null)
                  {
                    gameObject = this.PurchaseIconList.RewardUnit;
                    DataSource.Bind<UnitParam>(gameObject, unitParam, false);
                    break;
                  }
                  break;
              }
              if (Object.op_Inequality((Object) gameObject, (Object) null))
              {
                gameObject.get_transform().SetParent(this.PurchaseIconList.RewardList, false);
                gameObject.SetActive(true);
              }
            }
          }
          if (Object.op_Inequality((Object) this.PurchaseBadge, (Object) null))
            this.PurchaseBadge.SetActive(false);
          if (coinProductParam.Badge > 0 && Object.op_Inequality((Object) this.PurchaseBadge, (Object) null))
          {
            this.PurchaseBadge.SetActive(true);
            this.SetBadgeId(coinProductParam.Badge - 1);
          }
          if (Object.op_Inequality((Object) this.PurchaseBuyCountObject, (Object) null))
            this.PurchaseBuyCountObject.SetActive(false);
          if (product.remainNum == 0)
          {
            if (Object.op_Inequality((Object) this.PurchaseSoldOut, (Object) null))
              this.PurchaseSoldOut.SetActive(true);
          }
          else if (product.remainNum > 0)
          {
            switch (coinProductParam.Type)
            {
              case BuyCoinManager.PremiumRestrictionType.AllBuy:
                if (Object.op_Inequality((Object) this.PurchaseBuyCountObject, (Object) null))
                  this.PurchaseBuyCountObject.SetActive(true);
                if (Object.op_Inequality((Object) this.PurchaseBuyCountTitle, (Object) null))
                  this.PurchaseBuyCountTitle.set_text(LocalizedText.Get("sys.BUYCOIN_LIMITED"));
                if (Object.op_Inequality((Object) this.PurchaseBuyCountText, (Object) null))
                  this.PurchaseBuyCountText.set_text(product.remainNum.ToString());
                if (Object.op_Inequality((Object) this.PurchaseBuyCountObject, (Object) null))
                {
                  ImageArray component = (ImageArray) this.PurchaseBuyCountObject.GetComponent<ImageArray>();
                  if (Object.op_Inequality((Object) component, (Object) null))
                  {
                    component.ImageIndex = 0;
                    break;
                  }
                  break;
                }
                break;
              case BuyCoinManager.PremiumRestrictionType.DayBuy:
                if (Object.op_Inequality((Object) this.PurchaseBuyCountObject, (Object) null))
                  this.PurchaseBuyCountObject.SetActive(true);
                if (Object.op_Inequality((Object) this.PurchaseBuyCountTitle, (Object) null))
                  this.PurchaseBuyCountTitle.set_text(LocalizedText.Get("sys.BUYCOIN_DAY"));
                if (Object.op_Inequality((Object) this.PurchaseBuyCountText, (Object) null))
                  this.PurchaseBuyCountText.set_text(product.remainNum.ToString());
                if (Object.op_Inequality((Object) this.PurchaseBuyCountObject, (Object) null))
                {
                  ImageArray component = (ImageArray) this.PurchaseBuyCountObject.GetComponent<ImageArray>();
                  if (Object.op_Inequality((Object) component, (Object) null))
                  {
                    component.ImageIndex = 1;
                    break;
                  }
                  break;
                }
                break;
            }
          }
        }
        if (!Object.op_Inequality((Object) this.BonusItem, (Object) null))
          return;
        this.BonusItem.SetActive(product.numFree > 0);
      }
    }

    public void SetSpriteId(int index)
    {
      if (Object.op_Equality((Object) this.CoinIcon, (Object) null))
        return;
      if (index < 0)
        index = 0;
      if (index >= this.CoinIcon.Images.Length)
        index = this.CoinIcon.Images.Length - 1;
      this.CoinIcon.ImageIndex = index;
    }

    public void SetBadgeId(int index)
    {
      if (Object.op_Equality((Object) this.PurchaseBadgeImage, (Object) null))
        return;
      if (index < 0)
        index = 0;
      if (index >= this.PurchaseBadgeImage.Images.Length)
        index = this.PurchaseBadgeImage.Images.Length - 1;
      this.PurchaseBadgeImage.ImageIndex = index;
    }

    public void OnButtonHelpClick()
    {
      BuyCoinProductParam coinProductParam = MonoSingleton<GameManager>.Instance.MasterParam.GetBuyCoinProductParam(this.mProduct.ID);
      if (coinProductParam == null || !Object.op_Inequality((Object) this.BuyCoinItemDetailWindow, (Object) null) || !BuyCoinManager.Instance.GetProductBuyConfirm(this.mProduct))
        return;
      GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) this.BuyCoinItemDetailWindow);
      if (Object.op_Equality((Object) gameObject, (Object) null))
        return;
      DataSource.Bind<BuyCoinProductParam>(gameObject, coinProductParam, false);
      DataSource.Bind<PaymentManager.Product>(gameObject, this.mProduct, false);
      gameObject.SetActive(true);
    }
  }
}
