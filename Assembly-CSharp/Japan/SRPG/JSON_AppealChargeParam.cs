﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_AppealChargeParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_AppealChargeParam
  {
    public JSON_AppealChargeParam.AppealParam fields;

    public class AppealParam
    {
      public string appeal_id = string.Empty;
      public string before_img_id = string.Empty;
      public string after_img_id = string.Empty;
      public string start_at = string.Empty;
      public string end_at = string.Empty;
    }
  }
}
