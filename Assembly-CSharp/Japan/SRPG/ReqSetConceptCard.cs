﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqSetConceptCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SRPG
{
  public class ReqSetConceptCard : WebAPI
  {
    private ReqSetConceptCard(long[] card_iids, long unit_iid, Network.ResponseCallback response)
    {
      this.name = "unit/concept/set";
      string[] array = ((IEnumerable<long>) card_iids).Select<long, string>((Func<long, string>) (iid => iid.ToString())).ToArray<string>();
      StringBuilder stringBuilder = WebAPI.GetStringBuilder();
      stringBuilder.Append("\"concept_iids\":[");
      stringBuilder.Append(string.Join(",", array));
      stringBuilder.Append("],");
      stringBuilder.Append("\"unit_iid\":");
      stringBuilder.Append(unit_iid);
      this.body = WebAPI.GetRequestString(stringBuilder.ToString());
      this.callback = response;
    }

    public static ReqSetConceptCard CreateSet(
      long[] card_iids,
      long unit_iid,
      Network.ResponseCallback response)
    {
      return new ReqSetConceptCard(card_iids, unit_iid, response);
    }
  }
}
