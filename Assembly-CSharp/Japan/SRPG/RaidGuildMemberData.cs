﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RaidGuildMemberData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class RaidGuildMemberData : GuildMemberData
  {
    private int mBeatScore;
    private int mRescueScore;
    private int mRound;

    public int BeatScore
    {
      get
      {
        return this.mBeatScore;
      }
    }

    public int RescueScore
    {
      get
      {
        return this.mRescueScore;
      }
    }

    public int Round
    {
      get
      {
        return this.mRound;
      }
    }

    public bool Deserialize(JSON_RaidGuildMember json)
    {
      json.units = json.unit;
      json.award_id = json.selected_award;
      this.Deserialize((JSON_GuildMember) json);
      this.mBeatScore = json.beat_score;
      this.mRescueScore = json.rescue_score;
      this.mRound = json.round;
      return true;
    }
  }
}
