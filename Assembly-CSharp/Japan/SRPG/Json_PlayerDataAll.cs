﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_PlayerDataAll
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;

namespace SRPG
{
  [MessagePackObject(true)]
  public class Json_PlayerDataAll
  {
    public Json_PlayerData player;
    public Json_Unit[] units;
    public Json_Item[] items;
    public Json_Mail[] mails;
    public Json_Party[] parties;
    public Json_Friend[] friends;
    public Json_Artifact[] artifacts;
    public JSON_ConceptCard[] concept_cards;
    public Json_Skin[] skins;
    public Json_Notify notify;
    public Json_MultiFuids[] fuids;
    public int status;
    public string cuid;
    public long tut;
    public int first_contact;
    public Json_Versus vs;
    public string[] tips;
    public JSON_PlayerGuild player_guild;
    public string fu_status;
    public Json_ExpireItem[] expire_items;
    public JSON_TrophyProgress[] trophyprogs;
    public JSON_TrophyProgress[] bingoprogs;
    public JSON_PartyOverWrite[] party_decks;
    public string[] bgms;
    public int rune_storage;
    public int rune_storage_used;
    public JSON_StoryExChallengeCount story_ex_challenge;
  }
}
