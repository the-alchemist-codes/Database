﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AbilityPowerUpResultContent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class AbilityPowerUpResultContent : MonoBehaviour
  {
    public AbilityPowerUpResultContent()
    {
      base.\u002Ector();
    }

    public void SetData(AbilityPowerUpResultContent.Param param)
    {
      DataSource.Bind<AbilityParam>(((Component) this).get_gameObject(), param.data, false);
      GameParameter.UpdateAll(((Component) this).get_gameObject());
    }

    public class Param
    {
      public AbilityParam data;
    }
  }
}
