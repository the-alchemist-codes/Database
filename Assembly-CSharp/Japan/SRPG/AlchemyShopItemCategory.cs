﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AlchemyShopItemCategory
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace SRPG
{
  public class AlchemyShopItemCategory : MonoBehaviour
  {
    [SerializeField]
    private Text TextTitle;
    [Space(5f)]
    [SerializeField]
    private GameObject GoActive;
    [Space(5f)]
    [SerializeField]
    private GameObject GoLocked;
    [SerializeField]
    private SRPG_Button BtnLock;
    [Space(5f)]
    [SerializeField]
    private SRPG_Button BtnSelect;
    private int mIndex;
    private JSON_ShopListArray.Shops mShop;

    public AlchemyShopItemCategory()
    {
      base.\u002Ector();
    }

    public int Index
    {
      get
      {
        return this.mIndex;
      }
    }

    public JSON_ShopListArray.Shops Shop
    {
      get
      {
        return this.mShop;
      }
    }

    public void SetItem(
      int index,
      JSON_ShopListArray.Shops shop,
      UnityAction action = null,
      UnityAction lock_action = null)
    {
      if (index < 0 || shop == null)
        return;
      this.mIndex = index;
      this.mShop = shop;
      if (Object.op_Implicit((Object) this.TextTitle) && shop.info != null)
        this.TextTitle.set_text(shop.info.title);
      if (Object.op_Implicit((Object) this.GoLocked))
      {
        this.GoLocked.SetActive(shop.info == null || shop.info.unlock == 0);
        if (lock_action != null && Object.op_Implicit((Object) this.BtnLock))
          ((UnityEvent) this.BtnLock.get_onClick()).AddListener(lock_action);
      }
      if (action == null || !Object.op_Implicit((Object) this.BtnSelect))
        return;
      ((UnityEvent) this.BtnSelect.get_onClick()).AddListener(action);
    }

    public void SetCurrent(bool is_active)
    {
      if (!Object.op_Implicit((Object) this.GoActive))
        return;
      this.GoActive.SetActive(is_active);
    }
  }
}
