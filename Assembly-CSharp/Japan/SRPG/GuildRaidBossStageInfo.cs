﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildRaidBossStageInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  [FlowNode.Pin(1, "Initialize", FlowNode.PinTypes.Input, 1)]
  public class GuildRaidBossStageInfo : MonoBehaviour, IFlowInterface
  {
    public const int PIN_INPUT_INIT = 1;
    [SerializeField]
    private RawImage_Transparent mGuildRaidImage;
    [SerializeField]
    private GameObject mClearIcon;
    [SerializeField]
    private GameObject mBeatReward;
    [SerializeField]
    private GameObject mBeatRewardItem;
    [SerializeField]
    private GameObject mLastAtkReward;
    [SerializeField]
    private GameObject mLastAtkRewardItem;
    [SerializeField]
    private Button mBeatReceiptButton;
    [SerializeField]
    private GameObject[] mRatioRewardPart;
    [SerializeField]
    private Button mRefreshButton;
    [SerializeField]
    private GameObject mChallengeList;
    [SerializeField]
    private GameObject mApLoopBpRemain;
    [SerializeField]
    private GameObject mApLoopBpEternal;
    private const int RATIO_MAX = 5;
    private const int RATIO_COUNT = 20;
    private List<GameObject> mListReward;
    private List<GameObject> mListChallenge;

    public GuildRaidBossStageInfo()
    {
      base.\u002Ector();
    }

    private void Awake()
    {
    }

    private void Update()
    {
      if (GuildRaidManager.Instance.IsRefreshConfirm() && GuildRaidManager.Instance.CurrentBossInfo.CurrentHP > 0)
        ((Selectable) this.mRefreshButton).set_interactable(true);
      else
        ((Selectable) this.mRefreshButton).set_interactable(false);
    }

    private void OnDestroy()
    {
      if (!Object.op_Inequality((Object) GuildRaidManager.Instance, (Object) null))
        return;
      GuildRaidManager.Instance.SetRefreshButton();
    }

    public void Activated(int pinID)
    {
      if (pinID != 1)
        return;
      this.Init();
    }

    private void Init()
    {
      MasterParam masterParam = MonoSingleton<GameManager>.Instance.MasterParam;
      GuildRaidBossParam guildRaidBossParam = MonoSingleton<GameManager>.Instance.GetGuildRaidBossParam(GuildRaidManager.Instance.CurrentBossInfo.BossId);
      if (guildRaidBossParam == null)
        return;
      UnitParam unitParam = masterParam.GetUnitParam(guildRaidBossParam.UnitIName);
      if (unitParam == null)
        return;
      DataSource.Bind<GuildRaidBossInfo>(((Component) this).get_gameObject(), GuildRaidManager.Instance.CurrentBossInfo, false);
      DataSource.Bind<GuildRaidBossParam>(((Component) this).get_gameObject(), guildRaidBossParam, false);
      DataSource.Bind<UnitParam>(((Component) this).get_gameObject(), unitParam, false);
      GameUtility.SetGameObjectActive(this.mApLoopBpRemain, GuildRaidManager.Instance.BpHealType != GuildRaidManager.GuildRaidBpHealType.Eternal);
      GameUtility.SetGameObjectActive(this.mApLoopBpEternal, GuildRaidManager.Instance.BpHealType == GuildRaidManager.GuildRaidBpHealType.Eternal);
      GlobalVars.SelectedQuestID = guildRaidBossParam.QuestIName;
      GuildRaidRewardParam guildRaidRewardRound1 = MonoSingleton<GameManager>.Instance.GetGuildRaidRewardRound(guildRaidBossParam.BeatRewardId, GuildRaidManager.Instance.CurrentRound);
      if (this.mListReward != null && this.mListReward.Count > 0)
      {
        for (int index = 0; index < this.mListReward.Count; ++index)
          Object.Destroy((Object) this.mListReward[index]);
      }
      this.mListReward.Clear();
      if (guildRaidRewardRound1 != null)
      {
        GameUtility.SetGameObjectActive(this.mBeatReward, true);
        if (Object.op_Inequality((Object) this.mBeatRewardItem, (Object) null) && guildRaidRewardRound1 != null && guildRaidRewardRound1.Rewards != null)
        {
          for (int index = 0; index < guildRaidRewardRound1.Rewards.Count; ++index)
          {
            GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) this.mBeatRewardItem, this.mBeatRewardItem.get_transform().get_parent());
            RewardListItem component = (RewardListItem) gameObject.GetComponent<RewardListItem>();
            if (!Object.op_Equality((Object) component, (Object) null))
            {
              component.AllNotActive();
              this.SetRewardIcon(guildRaidRewardRound1.Rewards[index], component);
              gameObject.SetActive(true);
              this.mListReward.Add(gameObject);
            }
          }
        }
      }
      else
        GameUtility.SetGameObjectActive(this.mBeatReward, false);
      GuildRaidRewardParam guildRaidRewardRound2 = MonoSingleton<GameManager>.Instance.GetGuildRaidRewardRound(guildRaidBossParam.LastAttackRewardId, GuildRaidManager.Instance.CurrentRound);
      if (guildRaidRewardRound2 != null)
      {
        GameUtility.SetGameObjectActive(this.mLastAtkReward, true);
        if (Object.op_Inequality((Object) this.mLastAtkRewardItem, (Object) null) && guildRaidRewardRound2 != null && guildRaidRewardRound2.Rewards != null)
        {
          for (int index = 0; index < guildRaidRewardRound2.Rewards.Count; ++index)
          {
            GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) this.mLastAtkRewardItem, this.mLastAtkRewardItem.get_transform().get_parent());
            RewardListItem component = (RewardListItem) gameObject.GetComponent<RewardListItem>();
            if (!Object.op_Equality((Object) component, (Object) null))
            {
              component.AllNotActive();
              this.SetRewardIcon(guildRaidRewardRound2.Rewards[index], component);
              gameObject.SetActive(true);
              this.mListReward.Add(gameObject);
            }
          }
        }
      }
      else
        GameUtility.SetGameObjectActive(this.mLastAtkReward, false);
      if (this.mListChallenge != null && this.mListChallenge.Count > 0)
      {
        for (int index = 0; index < this.mListChallenge.Count; ++index)
          Object.Destroy((Object) this.mListChallenge[index]);
      }
      this.mListChallenge.Clear();
      if (GuildRaidManager.Instance.CurrentBossChallengingPlayerList != null && Object.op_Inequality((Object) this.mChallengeList, (Object) null))
      {
        this.mChallengeList.SetActive(false);
        int count = GuildRaidManager.Instance.CurrentBossChallengingPlayerList.Count;
        for (int index = 0; index < count; ++index)
        {
          if (GuildRaidManager.Instance.CurrentBossChallengingPlayerList[index] != null)
          {
            GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) this.mChallengeList, this.mChallengeList.get_transform().get_parent());
            this.mListChallenge.Add(gameObject);
            DataSource.Bind<GuildRaidChallengingPlayer>(gameObject, GuildRaidManager.Instance.CurrentBossChallengingPlayerList[index], false);
            gameObject.SetActive(true);
          }
        }
      }
      if (GuildRaidManager.Instance.CurrentBossInfo.CurrentHP <= 0)
      {
        if (Object.op_Inequality((Object) this.mGuildRaidImage, (Object) null))
          ((Graphic) this.mGuildRaidImage).set_color(Color.get_cyan());
        if (Object.op_Inequality((Object) this.mClearIcon, (Object) null))
          this.mClearIcon.SetActive(true);
      }
      GameParameter.UpdateAll(((Component) this).get_gameObject());
      GuildRaidManager.Instance.ReSetRefreshWaitSec();
    }

    private void SetRewardIcon(GuildRaidReward reward, RewardListItem listItem)
    {
      if (reward == null || Object.op_Equality((Object) listItem, (Object) null))
        return;
      GameManager instance = MonoSingleton<GameManager>.Instance;
      GameObject gameObject = (GameObject) null;
      bool flag = false;
      switch (reward.Type)
      {
        case RaidRewardType.Item:
          ItemParam itemParam = instance.GetItemParam(reward.IName);
          if (itemParam != null)
          {
            gameObject = listItem.RewardItem;
            DataSource.Bind<ItemParam>(gameObject, itemParam, false);
            flag = true;
            break;
          }
          break;
        case RaidRewardType.Gold:
          gameObject = listItem.RewardGold;
          flag = true;
          break;
        case RaidRewardType.Coin:
          gameObject = listItem.RewardCoin;
          flag = true;
          break;
        case RaidRewardType.Award:
          AwardParam awardParam = instance.GetAwardParam(reward.IName);
          if (awardParam == null)
            return;
          gameObject = listItem.RewardAward;
          DataSource.Bind<AwardParam>(gameObject, awardParam, false);
          break;
        case RaidRewardType.Unit:
          UnitParam unitParam = instance.GetUnitParam(reward.IName);
          if (unitParam != null)
          {
            gameObject = listItem.RewardUnit;
            DataSource.Bind<UnitParam>(gameObject, unitParam, false);
            break;
          }
          break;
        case RaidRewardType.ConceptCard:
          ConceptCardData cardDataForDisplay = ConceptCardData.CreateConceptCardDataForDisplay(reward.IName);
          if (cardDataForDisplay != null)
          {
            gameObject = listItem.RewardCard;
            ConceptCardIcon component = (ConceptCardIcon) gameObject.GetComponent<ConceptCardIcon>();
            if (Object.op_Inequality((Object) component, (Object) null))
            {
              component.Setup(cardDataForDisplay);
              break;
            }
            break;
          }
          break;
        case RaidRewardType.Artifact:
          ArtifactParam artifactParam = instance.MasterParam.GetArtifactParam(reward.IName);
          if (artifactParam != null)
          {
            gameObject = listItem.RewardArtifact;
            DataSource.Bind<ArtifactParam>(gameObject, artifactParam, false);
            break;
          }
          break;
      }
      if (flag)
      {
        Transform transform = gameObject.get_transform().Find("amount/Text_amount");
        if (Object.op_Inequality((Object) transform, (Object) null))
        {
          Text component = (Text) ((Component) transform).GetComponent<Text>();
          if (Object.op_Inequality((Object) component, (Object) null))
            component.set_text(reward.Num.ToString());
        }
      }
      if (!Object.op_Inequality((Object) gameObject, (Object) null))
        return;
      gameObject.get_transform().SetParent(listItem.RewardList, false);
      gameObject.SetActive(true);
    }
  }
}
