﻿// Decompiled with JetBrains decompiler
// Type: SRPG.TownQuestPeriodLock
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class TownQuestPeriodLock : MonoBehaviour
  {
    [SerializeField]
    private TownQuestPeriodLock.PeriodlockTargets Target;
    [SerializeField]
    private bool isDraw;
    [SerializeField]
    private GameObject LockObject;

    public TownQuestPeriodLock()
    {
      base.\u002Ector();
    }

    private void Start()
    {
      Button component = (Button) ((Component) this).GetComponent<Button>();
      bool flag = false;
      switch (this.Target)
      {
        case TownQuestPeriodLock.PeriodlockTargets.Genesis:
          GenesisParam genesisParam = MonoSingleton<GameManager>.Instance.MasterParam.GetGenesisParam();
          if (genesisParam != null)
          {
            flag = !genesisParam.IsWithinPeriod();
            break;
          }
          break;
        case TownQuestPeriodLock.PeriodlockTargets.Advance:
          flag = !AdvanceEventParam.IsWithinPeriod((string) null);
          break;
      }
      bool active = !this.isDraw ? !flag : flag;
      GameUtility.SetGameObjectActive(this.LockObject, active);
      if (!active)
        return;
      GameUtility.SetButtonIntaractable(component, false);
    }

    [System.Flags]
    public enum PeriodlockTargets
    {
      None = 1,
      Genesis = 2,
      Advance = 4,
    }
  }
}
