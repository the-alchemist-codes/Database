﻿// Decompiled with JetBrains decompiler
// Type: SRPG.VersusRankMissionScheduleParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class VersusRankMissionScheduleParam
  {
    private int mScheduleId;
    private string mIName;

    public int ScheduleId
    {
      get
      {
        return this.mScheduleId;
      }
    }

    public string IName
    {
      get
      {
        return this.mIName;
      }
    }

    public bool Deserialize(JSON_VersusRankMissionScheduleParam json)
    {
      if (json == null)
        return false;
      this.mScheduleId = json.schedule_id;
      this.mIName = json.iname;
      return true;
    }
  }
}
