﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_VipParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_VipParam
  {
    public int exp;
    public int ticket;
    public int buy_coin_bonus;
    public int buy_coin_num;
    public int buy_stmn_num;
    public int reset_elite;
    public int reset_arena;
  }
}
