﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RuneDrawListIcon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class RuneDrawListIcon : MonoBehaviour
  {
    [SerializeField]
    private GameObject RuneItemGO;
    [SerializeField]
    private GameObject RuneFrameGO;
    [SerializeField]
    private GameObject EvoImageGO;
    [SerializeField]
    private ImageArray EnhImageArray;
    [SerializeField]
    private GameObject SetTypeGO;
    [SerializeField]
    private Image mOwnerIcon;
    private BindRuneData mRuneData;

    public RuneDrawListIcon()
    {
      base.\u002Ector();
    }

    public void SetDrawParam(BindRuneData rundata)
    {
      this.mRuneData = rundata;
      if (Object.op_Inequality((Object) this.RuneItemGO, (Object) null))
        DataSource.Bind<BindRuneData>(this.RuneItemGO.get_gameObject(), rundata, false);
      if (Object.op_Inequality((Object) this.RuneFrameGO, (Object) null))
        DataSource.Bind<BindRuneData>(this.RuneFrameGO.get_gameObject(), rundata, false);
      if (Object.op_Inequality((Object) this.EvoImageGO, (Object) null))
        DataSource.Bind<BindRuneData>(this.EvoImageGO.get_gameObject(), rundata, false);
      if (Object.op_Inequality((Object) this.EnhImageArray, (Object) null))
        DataSource.Bind<BindRuneData>(((Component) this.EnhImageArray).get_gameObject(), rundata, false);
      if (Object.op_Inequality((Object) this.SetTypeGO, (Object) null))
        DataSource.Bind<BindRuneData>(this.SetTypeGO.get_gameObject(), rundata, false);
      this.Refresh();
    }

    public void Refresh()
    {
      if (Object.op_Inequality((Object) this.RuneItemGO, (Object) null))
        GameParameter.UpdateAll(this.RuneItemGO.get_gameObject());
      if (Object.op_Inequality((Object) this.RuneFrameGO, (Object) null))
        GameParameter.UpdateAll(this.RuneFrameGO.get_gameObject());
      if (Object.op_Inequality((Object) this.EvoImageGO, (Object) null))
        GameParameter.UpdateAll(this.EvoImageGO.get_gameObject());
      if (Object.op_Inequality((Object) this.EnhImageArray, (Object) null))
        GameParameter.UpdateAll(((Component) this.EnhImageArray).get_gameObject());
      if (Object.op_Inequality((Object) this.SetTypeGO, (Object) null))
        GameParameter.UpdateAll(this.SetTypeGO.get_gameObject());
      this.RefreshOwnerIcon();
    }

    private void RefreshOwnerIcon()
    {
      if (Object.op_Equality((Object) this.mOwnerIcon, (Object) null) || this.mRuneData == null)
        return;
      RuneData rune = this.mRuneData.Rune;
      if (rune == null)
        return;
      UnitData owner = rune.GetOwner();
      if (owner != null)
        this.mOwnerIcon.set_sprite(AssetManager.Load<SpriteSheet>("ItemIcon/small").GetSprite(MonoSingleton<GameManager>.Instance.GetItemParam(owner.UnitParam.piece).icon));
      else
        this.mOwnerIcon.set_sprite((Sprite) null);
    }
  }
}
