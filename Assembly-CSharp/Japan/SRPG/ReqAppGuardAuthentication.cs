﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqAppGuardAuthentication
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Text;

namespace SRPG
{
  public class ReqAppGuardAuthentication : WebAPI
  {
    public ReqAppGuardAuthentication(string uniqueClientID, Network.ResponseCallback response)
    {
      this.name = "appguard/auth";
      StringBuilder stringBuilder = WebAPI.GetStringBuilder();
      stringBuilder.Append("\"unique_client_id\":");
      stringBuilder.Append("\"");
      stringBuilder.Append(uniqueClientID);
      stringBuilder.Append("\"");
      this.body = WebAPI.GetRequestString(stringBuilder.ToString());
      this.callback = response;
    }
  }
}
