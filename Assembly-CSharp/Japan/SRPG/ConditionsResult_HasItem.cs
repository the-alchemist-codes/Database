﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ConditionsResult_HasItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;

namespace SRPG
{
  public class ConditionsResult_HasItem : ConditionsResult
  {
    public ConditionsResult_HasItem(string iname, int condsItemNum)
    {
      ItemData itemDataByItemId = MonoSingleton<GameManager>.Instance.Player.FindItemDataByItemID(iname, false);
      if (itemDataByItemId != null)
        this.mCurrentValue = itemDataByItemId.Num;
      this.mTargetValue = condsItemNum;
      this.mIsClear = this.mCurrentValue >= this.mTargetValue;
    }

    public ConditionsResult_HasItem(string[] inames, int condsItemNum)
    {
      if (inames == null || inames.Length == 0)
        return;
      int num = 0;
      for (int index = 0; index < inames.Length; ++index)
      {
        ItemData itemDataByItemId = MonoSingleton<GameManager>.Instance.Player.FindItemDataByItemID(inames[index], false);
        if (itemDataByItemId != null)
          num += itemDataByItemId.Num;
      }
      this.mCurrentValue = num;
      this.mTargetValue = condsItemNum;
      this.mIsClear = this.mCurrentValue >= this.mTargetValue;
    }

    public override string text
    {
      get
      {
        return string.Empty;
      }
    }

    public override string errorText
    {
      get
      {
        return LocalizedText.Get("sys.ITEM_NOT_ENOUGH");
      }
    }
  }
}
