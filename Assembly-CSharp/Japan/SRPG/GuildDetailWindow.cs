﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildDetailWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  [FlowNode.Pin(10, "表示更新", FlowNode.PinTypes.Input, 10)]
  [FlowNode.Pin(1010, "ギルド情報取得リクエスト", FlowNode.PinTypes.Output, 1010)]
  public class GuildDetailWindow : MonoBehaviour, IFlowInterface
  {
    private const int PIN_INPUT_REFRESH = 10;
    private const int PIN_OUTPUT_REQUEST_GUILD_INFO = 1010;

    public GuildDetailWindow()
    {
      base.\u002Ector();
    }

    public void Activated(int pinID)
    {
      if (pinID != 10)
        return;
      this.Refresh();
    }

    private void Start()
    {
      SerializeValueBehaviour component1 = (SerializeValueBehaviour) ((Component) this).GetComponent<SerializeValueBehaviour>();
      if (Object.op_Equality((Object) component1, (Object) null))
        return;
      GuildData data = component1.list.GetObject<GuildData>(GuildSVB_Key.GUILD);
      if (data != null)
      {
        DataSource.Bind<GuildData>(((Component) this).get_gameObject(), data, false);
      }
      else
      {
        int num = component1.list.GetInt(GuildSVB_Key.GUILD_ID);
        if (num <= 0)
          return;
        FlowNode_ReqGuildInfo component2 = (FlowNode_ReqGuildInfo) ((Component) this).get_gameObject().GetComponent<FlowNode_ReqGuildInfo>();
        if (!Object.op_Inequality((Object) component2, (Object) null))
          return;
        component2.SetParam((long) num);
        FlowNode_GameObject.ActivateOutputLinks((Component) this, 1010);
      }
    }

    private void Refresh()
    {
      FlowNode_ReqGuildInfo component = (FlowNode_ReqGuildInfo) ((Component) this).get_gameObject().GetComponent<FlowNode_ReqGuildInfo>();
      if (Object.op_Inequality((Object) component, (Object) null))
        DataSource.Bind<GuildData>(((Component) this).get_gameObject(), component.GuildData, false);
      GameParameter.UpdateAll(((Component) this).get_gameObject());
    }
  }
}
