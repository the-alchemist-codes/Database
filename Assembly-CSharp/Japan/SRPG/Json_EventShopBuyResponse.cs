﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_EventShopBuyResponse
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class Json_EventShopBuyResponse
  {
    public Json_Currencies currencies;
    public Json_Item[] items;
    public JSON_EventShopItemListSet[] shopitems;
    public Json_MailInfo mail_info;
    public Json_ShopBuyConceptCard[] cards;
    public Json_Unit[] units;
    public int concept_count;
    public JSON_TrophyProgress[] trophyprogs;
    public JSON_TrophyProgress[] bingoprogs;
    public Json_Item[] runes;
    public int rune_storage_used;
  }
}
