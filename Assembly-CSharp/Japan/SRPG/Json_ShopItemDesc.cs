﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_ShopItemDesc
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class Json_ShopItemDesc
  {
    public string iname;
    public int num;
    public string itype;
    public int maxnum;
    public int boughtnum;
    public int step;
    public int has_count;

    public bool IsItem
    {
      get
      {
        return this.itype == "item";
      }
    }

    public bool IsArtifact
    {
      get
      {
        return this.itype == "artifact";
      }
    }

    public bool IsConceptCard
    {
      get
      {
        return this.itype == "concept_card";
      }
    }
  }
}
