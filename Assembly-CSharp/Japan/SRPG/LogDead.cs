﻿// Decompiled with JetBrains decompiler
// Type: SRPG.LogDead
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace SRPG
{
  public class LogDead : BattleLog
  {
    public List<LogDead.Param> list_normal = new List<LogDead.Param>();
    public List<LogDead.Param> list_sentence = new List<LogDead.Param>();

    public void Add(Unit unit, DeadTypes type)
    {
      LogDead.Param obj = new LogDead.Param();
      obj.self = unit;
      obj.type = type;
      if (type == DeadTypes.DeathSentence)
        this.list_sentence.Add(obj);
      else
        this.list_normal.Add(obj);
    }

    public struct Param
    {
      public Unit self;
      public DeadTypes type;
    }
  }
}
