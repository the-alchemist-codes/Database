﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ConceptCardEffectDecreaseInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class ConceptCardEffectDecreaseInfo
  {
    public const int DECREASE_RATE_DEFAULT = 0;
    public ConceptCardData m_ConceptCardData;
    public bool m_IsDecreaseEffect;

    public ConceptCardEffectDecreaseInfo(ConceptCardData conceptCard, bool isDecreaseEffect)
    {
      this.m_ConceptCardData = conceptCard;
      this.m_IsDecreaseEffect = isDecreaseEffect;
    }
  }
}
