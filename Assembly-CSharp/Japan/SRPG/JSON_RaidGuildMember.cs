﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_RaidGuildMember
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_RaidGuildMember : JSON_GuildMember
  {
    public int beat_score;
    public int rescue_score;
    public int round;
    public Json_Unit unit;
    public string selected_award;
  }
}
