﻿// Decompiled with JetBrains decompiler
// Type: SRPG.EventStandChara
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class EventStandChara : MonoBehaviour
  {
    public static List<EventStandChara> Instances = new List<EventStandChara>();
    public string CharaID;
    public bool mClose;
    private float[] PositionX;
    public const float FADEIN_TIME = 0.3f;
    public const float FADEOUT_TIME = 0.5f;
    private float FadeTime;
    private bool IsFading;
    private EventStandChara.StateTypes mState;

    public EventStandChara()
    {
      base.\u002Ector();
    }

    public bool Fading
    {
      get
      {
        return this.IsFading;
      }
    }

    public EventStandChara.StateTypes State
    {
      get
      {
        return this.mState;
      }
      set
      {
        this.mState = value;
      }
    }

    public static EventStandChara Find(string id)
    {
      for (int index = EventStandChara.Instances.Count - 1; index >= 0; --index)
      {
        if (EventStandChara.Instances[index].CharaID == id)
          return EventStandChara.Instances[index];
      }
      return (EventStandChara) null;
    }

    public static void DiscardAll()
    {
      for (int index = EventStandChara.Instances.Count - 1; index >= 0; --index)
      {
        if (!((Component) EventStandChara.Instances[index]).get_gameObject().get_activeInHierarchy())
          Object.Destroy((Object) ((Component) EventStandChara.Instances[index]).get_gameObject());
      }
      EventStandChara.Instances.Clear();
    }

    public static string[] GetCharaIDs()
    {
      List<string> stringList = new List<string>();
      for (int index = EventStandChara.Instances.Count - 1; index >= 0; --index)
        stringList.Add(EventStandChara.Instances[index].CharaID);
      return stringList.ToArray();
    }

    private void Awake()
    {
      EventStandChara.Instances.Add(this);
      this.FadeTime = 0.0f;
      this.IsFading = false;
    }

    private void OnDestroy()
    {
      EventStandChara.Instances.Remove(this);
      RawImage component = (RawImage) ((Component) this).get_gameObject().GetComponent<RawImage>();
      if (Object.op_Inequality((Object) component, (Object) null))
      {
        component.set_texture((Texture) null);
        Object.Destroy((Object) component);
      }
      this.mState = EventStandChara.StateTypes.Inactive;
    }

    public void Open(float fade = 0.3f)
    {
      this.mClose = false;
      this.StartFadeIn(fade);
    }

    public void Close(float fade = 0.5f)
    {
      this.mClose = true;
      this.StartFadeOut(fade);
    }

    public void StartFadeIn(float fade)
    {
      this.IsFading = true;
      this.FadeTime = fade;
      this.mState = EventStandChara.StateTypes.FadeIn;
      if ((double) this.FadeTime > 0.0)
        return;
      Color color = ((Graphic) ((Component) this).get_gameObject().GetComponent<RawImage>()).get_color();
      ((Graphic) ((Component) this).get_gameObject().GetComponent<RawImage>()).set_color(new Color((float) color.r, (float) color.g, (float) color.b, 1f));
    }

    public void StartFadeOut(float fade)
    {
      this.IsFading = true;
      this.FadeTime = fade;
      this.mState = EventStandChara.StateTypes.FadeOut;
      if ((double) this.FadeTime > 0.0)
        return;
      Color color = ((Graphic) ((Component) this).get_gameObject().GetComponent<RawImage>()).get_color();
      ((Graphic) ((Component) this).get_gameObject().GetComponent<RawImage>()).set_color(new Color((float) color.r, (float) color.g, (float) color.b, 0.0f));
    }

    private void Update()
    {
      if (!this.IsFading)
        return;
      this.FadeTime -= Time.get_deltaTime();
      if ((double) this.FadeTime <= 0.0)
      {
        this.FadeTime = 0.0f;
        this.IsFading = false;
      }
      else
      {
        if (this.mState == EventStandChara.StateTypes.FadeIn)
          this.FadeIn(this.FadeTime);
        if (this.mState != EventStandChara.StateTypes.FadeOut)
          return;
        this.FadeOut(this.FadeTime);
      }
    }

    private void FadeIn(float time)
    {
      Color color = ((Graphic) ((Component) this).get_gameObject().GetComponent<RawImage>()).get_color();
      ((Graphic) ((Component) this).get_gameObject().GetComponent<RawImage>()).set_color(new Color((float) color.r, (float) color.g, (float) color.b, Mathf.Lerp(1f, 0.0f, time)));
    }

    private void FadeOut(float time)
    {
      Color color = ((Graphic) ((Component) this).get_gameObject().GetComponent<RawImage>()).get_color();
      ((Graphic) ((Component) this).get_gameObject().GetComponent<RawImage>()).set_color(new Color((float) color.r, (float) color.g, (float) color.b, Mathf.Lerp(0.0f, 1f, time)));
    }

    public void InitPositionX(RectTransform canvasRect)
    {
      RectTransform component = (RectTransform) ((Component) this).GetComponent<RectTransform>();
      float[] numArray = new float[5];
      Rect rect1 = component.get_rect();
      numArray[0] = (float) ((double) ((Rect) ref rect1).get_width() / 2.0);
      Rect rect2 = canvasRect.get_rect();
      numArray[1] = (float) ((double) ((Rect) ref rect2).get_width() / 2.0);
      Rect rect3 = canvasRect.get_rect();
      double width1 = (double) ((Rect) ref rect3).get_width();
      Rect rect4 = component.get_rect();
      double num1 = (double) ((Rect) ref rect4).get_width() / 2.0;
      numArray[2] = (float) (width1 - num1);
      Rect rect5 = component.get_rect();
      numArray[3] = (float) (-(double) ((Rect) ref rect5).get_width() / 2.0);
      Rect rect6 = canvasRect.get_rect();
      double width2 = (double) ((Rect) ref rect6).get_width();
      Rect rect7 = component.get_rect();
      double num2 = (double) ((Rect) ref rect7).get_width() / 2.0;
      numArray[4] = (float) (width2 + num2);
      this.PositionX = numArray;
    }

    public float GetPositionX(int index)
    {
      return index >= 0 && index < this.PositionX.Length ? this.PositionX[index] : 0.0f;
    }

    public enum PositionTypes
    {
      Left,
      Center,
      Right,
      OverLeft,
      OverRight,
    }

    public enum StateTypes
    {
      FadeIn,
      Active,
      FadeOut,
      Inactive,
    }
  }
}
