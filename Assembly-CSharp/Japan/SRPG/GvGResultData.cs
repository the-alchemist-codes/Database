﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GvGResultData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace SRPG
{
  public class GvGResultData
  {
    public int Rank { get; private set; }

    public int Point { get; private set; }

    public List<int> CaptureNodes { get; private set; }

    public bool Deserialize(JSON_GvGResult json)
    {
      if (json == null)
        return false;
      this.Rank = json.rank;
      this.Point = json.point;
      this.CaptureNodes = new List<int>();
      if (json.capture_nodes != null)
        this.CaptureNodes.AddRange((IEnumerable<int>) json.capture_nodes);
      return true;
    }
  }
}
