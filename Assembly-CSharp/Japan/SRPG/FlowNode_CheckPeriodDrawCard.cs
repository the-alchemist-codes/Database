﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_CheckPeriodDrawCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  [FlowNode.NodeType("DrawCard/CheckPeriod", 32741)]
  [FlowNode.Pin(1, "Check", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(10, "True", FlowNode.PinTypes.Output, 10)]
  [FlowNode.Pin(11, "False", FlowNode.PinTypes.Output, 11)]
  public class FlowNode_CheckPeriodDrawCard : FlowNode
  {
    private const int PIN_IN_CHECK = 1;
    private const int PIN_OUT_TRUE = 10;
    private const int PIN_OUT_FALSE = 11;

    public override void OnActivate(int pinID)
    {
      if (pinID != 1)
        return;
      if (DrawCardParam.DrawCardEnabled)
        this.ActivateOutputLinks(10);
      else
        this.ActivateOutputLinks(11);
    }
  }
}
