﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqRuneEquip
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using Gsc.Network.Encoding;
using MessagePack;
using System;

namespace SRPG
{
  public class ReqRuneEquip : WebAPI
  {
    public ReqRuneEquip(
      ReqRuneEquip.RequestParam rp,
      SRPG.Network.ResponseCallback response,
      EncodingTypes.ESerializeCompressMethod serializeCompressMethod)
    {
      this.name = "unit/rune/set";
      this.body = WebAPI.GetRequestString<ReqRuneEquip.RequestParam>(rp);
      this.callback = response;
      this.serializeCompressMethod = serializeCompressMethod;
    }

    [MessagePackObject(true)]
    [Serializable]
    public class RequestParam
    {
      public long unit_id;
      public long[] rune_ids;

      public RequestParam()
      {
      }

      public RequestParam(long _unit_id, long[] _rune_ids)
      {
        this.unit_id = _unit_id;
        this.rune_ids = _rune_ids;
      }
    }

    [MessagePackObject(true)]
    [Serializable]
    public class Response
    {
      public Json_Unit[] units;
      public int rune_storage_used;
    }
  }
}
