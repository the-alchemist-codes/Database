﻿// Decompiled with JetBrains decompiler
// Type: SRPG.SortRuneConditionParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class SortRuneConditionParam
  {
    public SortRuneParam parent;
    public string cnds_iname;
    public string name;
    public eRuneSortType sort_type;

    public SortRuneConditionParam(SortRuneParam _parent)
    {
      this.parent = _parent;
    }

    public void Deserialize(JSON_SortRuneConditionParam json)
    {
      this.cnds_iname = json.cnds_iname;
      this.name = json.name;
      this.sort_type = (eRuneSortType) json.sort_type;
    }

    public string PrefsKey
    {
      get
      {
        return SortUtility.SortPrefs.MakeKey(this.parent.iname, this.cnds_iname);
      }
    }
  }
}
