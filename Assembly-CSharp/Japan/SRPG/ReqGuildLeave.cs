﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqGuildLeave
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Text;

namespace SRPG
{
  public class ReqGuildLeave : WebAPI
  {
    public ReqGuildLeave(string new_guild_master_uid, Network.ResponseCallback response)
    {
      this.name = "guild/leave";
      this.body = WebAPI.GetRequestString((string) null);
      if (!string.IsNullOrEmpty(new_guild_master_uid))
      {
        StringBuilder stringBuilder = WebAPI.GetStringBuilder();
        stringBuilder.Append("\"target_uid\":\"");
        stringBuilder.Append(new_guild_master_uid);
        stringBuilder.Append("\"");
        this.body = WebAPI.GetRequestString(stringBuilder.ToString());
      }
      this.callback = response;
    }
  }
}
