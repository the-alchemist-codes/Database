﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildRaidRewardRoundParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace SRPG
{
  public class GuildRaidRewardRoundParam : GuildRaidMasterParam<JSON_GuildRaidRewardRoundParam>
  {
    public string Id { get; private set; }

    public List<GuildRaidRewardRound> Reward { get; private set; }

    public override bool Deserialize(JSON_GuildRaidRewardRoundParam json)
    {
      if (json == null)
        return false;
      this.Id = json.id;
      if (json.rewards == null || json.rewards.Length == 0)
        return false;
      this.Reward = new List<GuildRaidRewardRound>();
      for (int index = 0; index < json.rewards.Length; ++index)
      {
        GuildRaidRewardRound guildRaidRewardRound = new GuildRaidRewardRound();
        if (guildRaidRewardRound.Deserialize(json.rewards[index]))
          this.Reward.Add(guildRaidRewardRound);
      }
      return true;
    }
  }
}
