﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_AIAction
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_AIAction
  {
    public string skill;
    public int type;
    public int turn;
    public int notBlock;
    public int noExecAct;
    public int nextActIdx;
    public int nextTurnAct;
    public int turnActIdx;
    public JSON_SkillLockCondition cond;
  }
}
