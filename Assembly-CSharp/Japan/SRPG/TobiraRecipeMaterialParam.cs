﻿// Decompiled with JetBrains decompiler
// Type: SRPG.TobiraRecipeMaterialParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class TobiraRecipeMaterialParam
  {
    private string mIname;
    private int mNum;

    public string Iname
    {
      get
      {
        return this.mIname;
      }
    }

    public int Num
    {
      get
      {
        return this.mNum;
      }
    }

    public void Deserialize(JSON_TobiraRecipeMaterialParam json)
    {
      if (json == null)
        return;
      this.mIname = json.iname;
      this.mNum = json.num;
    }
  }
}
