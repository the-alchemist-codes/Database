﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_IsSelectedQuestId
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  [FlowNode.NodeType("Story/IsSelectedQuestId", 32741)]
  [FlowNode.Pin(1, "Check", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(100, "Selected", FlowNode.PinTypes.Output, 100)]
  [FlowNode.Pin(110, "Not Selected", FlowNode.PinTypes.Output, 110)]
  public class FlowNode_IsSelectedQuestId : FlowNode
  {
    private const int PIN_IN_INPUT = 1;
    private const int PIN_OUT_SELECTED = 100;
    private const int PIN_OUT_NOT_SELECTED = 110;

    public override void OnActivate(int pinID)
    {
      if (pinID != 1)
        return;
      this.ActivateOutputLinks(!this.IsSelected() ? 110 : 100);
    }

    private bool IsSelected()
    {
      return GlobalVars.SelectedStoryPart.Get() != 0 || !string.IsNullOrEmpty(GlobalVars.SelectedSection.Get()) || !string.IsNullOrEmpty(GlobalVars.SelectedChapter.Get());
    }
  }
}
