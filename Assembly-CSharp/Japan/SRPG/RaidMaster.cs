﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RaidMaster
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace SRPG
{
  public class RaidMaster
  {
    public static void Deserialize<T, U>(ref List<T> obj, U[] json)
      where T : RaidMasterParam<U>, new()
      where U : JSON_RaidMasterParam
    {
      if (json == null)
        return;
      if (obj == null)
        obj = new List<T>();
      for (int index = 0; index < json.Length; ++index)
      {
        if ((object) json[index] != null)
        {
          T obj1 = new T();
          if (obj1.Deserialize(json[index]))
            obj.Add(obj1);
        }
      }
    }
  }
}
