﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RaidRankingList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace SRPG
{
  public class RaidRankingList
  {
    private RaidRankingData mMyInfo;
    private List<RaidRankingData> mRanking;

    public RaidRankingData MyInfo
    {
      get
      {
        return this.mMyInfo;
      }
    }

    public List<RaidRankingData> Ranking
    {
      get
      {
        return this.mRanking;
      }
    }

    public bool Deserialize(Json_RaidRankingList json)
    {
      this.mMyInfo = new RaidRankingData();
      this.mRanking = new List<RaidRankingData>();
      if (json.my_info != null && !this.mMyInfo.Deserialize(json.my_info))
        return false;
      if (json.ranking != null)
      {
        for (int index = 0; index < json.ranking.Length; ++index)
        {
          RaidRankingData raidRankingData = new RaidRankingData();
          if (raidRankingData.Deserialize(json.ranking[index]))
            this.mRanking.Add(raidRankingData);
        }
      }
      return true;
    }
  }
}
