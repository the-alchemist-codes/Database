﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RaidBeatRewardParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace SRPG
{
  public class RaidBeatRewardParam : RaidMasterParam<JSON_RaidBeatRewardParam>
  {
    private int mId;
    private List<RaidBeatRewardDataParam> mRewards;

    public int Id
    {
      get
      {
        return this.mId;
      }
    }

    public List<RaidBeatRewardDataParam> Rewards
    {
      get
      {
        return this.mRewards;
      }
    }

    public override bool Deserialize(JSON_RaidBeatRewardParam json)
    {
      if (json == null)
        return false;
      this.mId = json.id;
      this.mRewards = new List<RaidBeatRewardDataParam>();
      if (json.rewards != null)
      {
        for (int index = 0; index < json.rewards.Length; ++index)
        {
          RaidBeatRewardDataParam beatRewardDataParam = new RaidBeatRewardDataParam();
          if (beatRewardDataParam.Deserialize(json.rewards[index]))
            this.mRewards.Add(beatRewardDataParam);
        }
      }
      return true;
    }
  }
}
