﻿// Decompiled with JetBrains decompiler
// Type: SRPG.SkinConceptCardData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;

namespace SRPG
{
  public class SkinConceptCardData
  {
    public string iname;
    public ConceptCardParam param;

    public bool Deserialize(string _iname)
    {
      this.iname = _iname;
      this.param = MonoSingleton<GameManager>.Instance.MasterParam.GetConceptCardParam(_iname);
      return true;
    }
  }
}
