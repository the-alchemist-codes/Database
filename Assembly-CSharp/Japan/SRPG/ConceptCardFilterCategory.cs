﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ConceptCardFilterCategory
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace SRPG
{
  public class ConceptCardFilterCategory : MonoBehaviour
  {
    [SerializeField]
    private Text mHeaderText;
    [SerializeField]
    private GameObject mToggleTemplate;
    private List<Toggle> mToggles;
    private FilterConceptCardParam mFilterParam;
    private Dictionary<string, GameObject> mCreatedToggles;

    public ConceptCardFilterCategory()
    {
      base.\u002Ector();
    }

    public List<Toggle> Toggles
    {
      get
      {
        return this.mToggles;
      }
    }

    public FilterConceptCardParam FilterParam
    {
      get
      {
        return this.mFilterParam;
      }
    }

    public void Init(FilterConceptCardParam filter_param)
    {
      this.mFilterParam = filter_param;
      GameUtility.SetGameObjectActive(this.mToggleTemplate, false);
      this.mHeaderText.set_text(filter_param.name);
      for (int index = 0; index < filter_param.conditions.Length; ++index)
      {
        if (!this.mCreatedToggles.ContainsKey(filter_param.conditions[index].cnds_iname))
        {
          GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) this.mToggleTemplate, ((Component) this).get_transform(), false);
          Toggle componentInChildren1 = (Toggle) gameObject.GetComponentInChildren<Toggle>();
          // ISSUE: method pointer
          ((UnityEvent<bool>) componentInChildren1.onValueChanged).AddListener(new UnityAction<bool>((object) this, __methodptr(\u003CInit\u003Em__0)));
          this.mToggles.Add(componentInChildren1);
          gameObject.SetActive(true);
          this.mCreatedToggles.Add(filter_param.conditions[index].cnds_iname, gameObject);
          Text componentInChildren2 = (Text) gameObject.GetComponentInChildren<Text>();
          if (Object.op_Inequality((Object) componentInChildren2, (Object) null))
            componentInChildren2.set_text(filter_param.conditions[index].name);
          FilterUtility.FilterBindData data = new FilterUtility.FilterBindData(filter_param.conditions[index].rarity, filter_param.conditions[index].name, ArtifactTypes.None, (byte) 0);
          DataSource.Bind<FilterUtility.FilterBindData>(((Component) componentInChildren1).get_gameObject(), data, false);
          DataSource.Bind<FilterConceptCardConditionParam>(((Component) componentInChildren1).get_gameObject(), filter_param.conditions[index], false);
          if (Object.op_Inequality((Object) ConceptCardListFilterWindow.Instance, (Object) null))
          {
            bool flag = ConceptCardListFilterWindow.Instance.CurrentFilterPrefs.GetValue(filter_param.conditions[index].parent.iname, filter_param.conditions[index].cnds_iname);
            GameUtility.SetToggle(componentInChildren1, flag);
          }
        }
      }
    }

    private void OnToggleValueChanged()
    {
      if (!Object.op_Inequality((Object) ConceptCardListFilterWindow.Instance, (Object) null))
        return;
      ConceptCardListFilterWindow.Instance.UpdateTabState();
    }

    public bool IsExistActiveToggle()
    {
      for (int index = 0; index < this.mToggles.Count; ++index)
      {
        if (this.mToggles[index].get_isOn())
          return true;
      }
      return false;
    }
  }
}
