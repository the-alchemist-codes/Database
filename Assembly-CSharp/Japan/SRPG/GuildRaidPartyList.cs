﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildRaidPartyList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;
using UnityEngine;

namespace SRPG
{
  public class GuildRaidPartyList : MonoBehaviour
  {
    [SerializeField]
    private GenericSlot UnitSlotTemplate;
    [SerializeField]
    private Transform MainMemberHolder;
    [SerializeField]
    private Transform SubMemberHolder;
    [SerializeField]
    private GenericSlot CardSlotTemplate;
    [SerializeField]
    private Transform MainMemberCardHolder;
    [SerializeField]
    private Transform SubMemberCardHolder;

    public GuildRaidPartyList()
    {
      base.\u002Ector();
    }

    public void Setup(List<UnitData> party)
    {
      if (party == null || Object.op_Equality((Object) this.UnitSlotTemplate, (Object) null) || (Object.op_Equality((Object) this.MainMemberHolder, (Object) null) || Object.op_Equality((Object) this.SubMemberCardHolder, (Object) null)))
        return;
      ((Component) this.UnitSlotTemplate).get_gameObject().SetActive(false);
      if (Object.op_Equality((Object) this.CardSlotTemplate, (Object) null) || Object.op_Equality((Object) this.MainMemberCardHolder, (Object) null) || Object.op_Equality((Object) this.SubMemberCardHolder, (Object) null))
        return;
      ((Component) this.CardSlotTemplate).get_gameObject().SetActive(false);
      PartyData partyData = new PartyData(PlayerPartyTypes.GuildRaid);
      UnitData leader = party.Count <= 0 ? (UnitData) null : party[0];
      for (int index1 = 0; index1 < partyData.MAX_MAINMEMBER; ++index1)
      {
        int index2 = index1;
        PartySlotData data1 = new PartySlotData(PartySlotType.Free, (string) null, (PartySlotIndex) index2, false);
        GenericSlot genericSlot1 = (GenericSlot) Object.Instantiate<GenericSlot>((M0) this.UnitSlotTemplate, this.MainMemberHolder);
        ((Component) genericSlot1).get_gameObject().SetActive(true);
        genericSlot1.SetSlotData<PartySlotData>(data1);
        UnitData data2 = party.Count <= index2 ? (UnitData) null : party[index2];
        genericSlot1.SetSlotData<UnitData>(data2);
        GenericSlot genericSlot2 = (GenericSlot) Object.Instantiate<GenericSlot>((M0) this.CardSlotTemplate, this.MainMemberCardHolder);
        ((Component) genericSlot2).get_gameObject().SetActive(true);
        ((ConceptCardSlot) ((Component) genericSlot2).GetComponent<ConceptCardSlot>()).SetLeaderUnit(leader);
        genericSlot2.SetSlotData<PartySlotData>(data1);
        genericSlot2.SetSlotData<ConceptCardData>(data2?.MainConceptCard);
        ((ConceptCardIcon) ((Component) genericSlot2).GetComponent<ConceptCardIcon>()).Setup(data2?.MainConceptCard);
      }
      for (int index1 = 0; index1 < partyData.MAX_SUBMEMBER; ++index1)
      {
        int index2 = index1 + partyData.MAX_MAINMEMBER;
        PartySlotData data1 = new PartySlotData(PartySlotType.Free, (string) null, (PartySlotIndex) index2, false);
        GenericSlot genericSlot1 = (GenericSlot) Object.Instantiate<GenericSlot>((M0) this.UnitSlotTemplate, this.SubMemberHolder);
        ((Component) genericSlot1).get_gameObject().SetActive(true);
        genericSlot1.SetSlotData<PartySlotData>(data1);
        UnitData data2 = party.Count <= index2 ? (UnitData) null : party[index2];
        genericSlot1.SetSlotData<UnitData>(data2);
        GenericSlot genericSlot2 = (GenericSlot) Object.Instantiate<GenericSlot>((M0) this.CardSlotTemplate, this.SubMemberCardHolder);
        ((Component) genericSlot2).get_gameObject().SetActive(true);
        ((ConceptCardSlot) ((Component) genericSlot2).GetComponent<ConceptCardSlot>()).SetLeaderUnit(leader);
        genericSlot2.SetSlotData<PartySlotData>(data1);
        genericSlot2.SetSlotData<ConceptCardData>(data2?.MainConceptCard);
        ((ConceptCardIcon) ((Component) genericSlot2).GetComponent<ConceptCardIcon>()).Setup(data2?.MainConceptCard);
      }
    }
  }
}
