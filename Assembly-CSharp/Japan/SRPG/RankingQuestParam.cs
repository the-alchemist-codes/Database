﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RankingQuestParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System;

namespace SRPG
{
  public class RankingQuestParam
  {
    public int schedule_id;
    public RankingQuestType type;
    public string iname;
    public int reward_id;
    public RankingQuestRewardParam rewardParam;
    public RankingQuestScheduleParam scheduleParam;

    public bool Deserialize(JSON_RankingQuestParam json)
    {
      this.schedule_id = json.schedule_id;
      if (Enum.GetNames(typeof (RankingQuestType)).Length > json.type)
        this.type = (RankingQuestType) json.type;
      else
        DebugUtility.LogError("定義されていない列挙値が指定されようとしました");
      this.iname = json.iname;
      this.reward_id = json.reward_id;
      return true;
    }

    public static RankingQuestParam FindRankingQuestParam(
      string targetQuestID,
      int scheduleID,
      RankingQuestType type)
    {
      RankingQuestParam rankingQuestParam = (RankingQuestParam) null;
      GameManager instanceDirect = MonoSingleton<GameManager>.GetInstanceDirect();
      return UnityEngine.Object.op_Equality((UnityEngine.Object) instanceDirect, (UnityEngine.Object) null) || instanceDirect.RankingQuestParams == null ? rankingQuestParam : instanceDirect.RankingQuestParams.Find((Predicate<RankingQuestParam>) (param => param.schedule_id == scheduleID && param.type == type && param.iname == targetQuestID));
    }
  }
}
