﻿// Decompiled with JetBrains decompiler
// Type: SRPG.UnitPieceShopGroupParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace SRPG
{
  public class UnitPieceShopGroupParam
  {
    private string mIname;
    private List<UnitPieceShopGroupCost> mCosts;

    public string Iname
    {
      get
      {
        return this.mIname;
      }
    }

    public List<UnitPieceShopGroupCost> Costs
    {
      get
      {
        return this.mCosts;
      }
    }

    public bool Deserialize(JSON_UnitPieceShopGroupParam json)
    {
      if (json == null)
        return false;
      this.mIname = json.iname;
      this.mCosts = new List<UnitPieceShopGroupCost>();
      if (json.costs != null)
      {
        for (int index = 0; index < json.costs.Length; ++index)
        {
          if (json.costs[index] != null)
          {
            UnitPieceShopGroupCost pieceShopGroupCost = new UnitPieceShopGroupCost();
            if (pieceShopGroupCost.Deserialize(json.costs[index]))
              this.mCosts.Add(pieceShopGroupCost);
          }
        }
      }
      return true;
    }

    public static void Deserialize(
      ref List<UnitPieceShopGroupParam> param,
      JSON_UnitPieceShopGroupParam[] json)
    {
      if (json == null)
        return;
      param = new List<UnitPieceShopGroupParam>(json.Length);
      for (int index = 0; index < json.Length; ++index)
      {
        if (json[index] != null)
        {
          UnitPieceShopGroupParam pieceShopGroupParam = new UnitPieceShopGroupParam();
          if (pieceShopGroupParam.Deserialize(json[index]))
            param.Add(pieceShopGroupParam);
        }
      }
    }
  }
}
