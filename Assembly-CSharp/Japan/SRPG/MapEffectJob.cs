﻿// Decompiled with JetBrains decompiler
// Type: SRPG.MapEffectJob
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace SRPG
{
  public class MapEffectJob : MonoBehaviour
  {
    public GameObject GoMapEffectParent;
    public GameObject GoMapEffectBaseItem;

    public MapEffectJob()
    {
      base.\u002Ector();
    }

    public void Setup()
    {
      JobParam dataOfClass = DataSource.FindDataOfClass<JobParam>(((Component) this).get_gameObject(), (JobParam) null);
      if (dataOfClass == null)
        return;
      GameManager instanceDirect = MonoSingleton<GameManager>.GetInstanceDirect();
      if (!UnityEngine.Object.op_Implicit((UnityEngine.Object) instanceDirect))
        return;
      if (UnityEngine.Object.op_Implicit((UnityEngine.Object) this.GoMapEffectParent) && UnityEngine.Object.op_Implicit((UnityEngine.Object) this.GoMapEffectBaseItem))
      {
        this.GoMapEffectBaseItem.SetActive(false);
        BattleUnitDetail.DestroyChildGameObjects(this.GoMapEffectParent, new List<GameObject>((IEnumerable<GameObject>) new GameObject[1]
        {
          this.GoMapEffectBaseItem
        }));
      }
      DataSource component = (DataSource) ((Component) this).get_gameObject().GetComponent<DataSource>();
      if (UnityEngine.Object.op_Implicit((UnityEngine.Object) component))
        component.Clear();
      DataSource.Bind<JobParam>(((Component) this).get_gameObject(), dataOfClass, false);
      if (!UnityEngine.Object.op_Implicit((UnityEngine.Object) this.GoMapEffectParent) || !UnityEngine.Object.op_Implicit((UnityEngine.Object) this.GoMapEffectBaseItem))
        return;
      List<MapEffectJob.MeSkill> l = new List<MapEffectJob.MeSkill>();
      if (!string.IsNullOrEmpty(dataOfClass.MapEffectAbility))
      {
        AbilityParam abilityParam = instanceDirect.GetAbilityParam(dataOfClass.MapEffectAbility);
        if (abilityParam != null)
        {
          foreach (LearningSkill skill in abilityParam.skills)
          {
            SkillParam skillParam = instanceDirect.GetSkillParam(skill.iname);
            if (skillParam != null)
            {
              foreach (MapEffectParam haveMapEffectList in MapEffectParam.GetHaveMapEffectLists(skill.iname))
              {
                bool flag = false;
                foreach (MapEffectJob.MeSkill meSkill in l)
                {
                  if (meSkill.Equals(haveMapEffectList, skillParam))
                  {
                    flag = true;
                    break;
                  }
                }
                if (!flag)
                  l.Add(new MapEffectJob.MeSkill(haveMapEffectList, skillParam));
              }
            }
          }
        }
      }
      if (l.Count == 0)
        return;
      MySort<MapEffectJob.MeSkill>.Sort(l, (Comparison<MapEffectJob.MeSkill>) ((src, dsc) => src == dsc ? 0 : dsc.mMapEffectParam.Index - src.mMapEffectParam.Index));
      foreach (MapEffectJob.MeSkill meSkill in l)
      {
        GameObject gameObject = (GameObject) UnityEngine.Object.Instantiate<GameObject>((M0) this.GoMapEffectBaseItem);
        if (UnityEngine.Object.op_Implicit((UnityEngine.Object) gameObject))
        {
          gameObject.get_transform().SetParent(this.GoMapEffectParent.get_transform());
          gameObject.get_transform().set_localScale(Vector3.get_one());
          DataSource.Bind<MapEffectParam>(gameObject, meSkill.mMapEffectParam, false);
          DataSource.Bind<SkillParam>(gameObject, meSkill.mSkillParam, false);
          gameObject.SetActive(true);
        }
      }
    }

    private class MeSkill
    {
      public MapEffectParam mMapEffectParam;
      public SkillParam mSkillParam;

      public MeSkill(MapEffectParam me_param, SkillParam skill_param)
      {
        this.mMapEffectParam = me_param;
        this.mSkillParam = skill_param;
      }

      public bool Equals(MapEffectParam me_param, SkillParam skill_param)
      {
        return this.mMapEffectParam == me_param && this.mSkillParam == skill_param;
      }
    }
  }
}
