﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ResultMask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class ResultMask : MonoBehaviour
  {
    public RawImage ImgBg;

    public ResultMask()
    {
      base.\u002Ector();
    }

    public void SetBg(Texture2D tex)
    {
      if (!Object.op_Implicit((Object) this.ImgBg) || Object.op_Equality((Object) tex, (Object) null))
        return;
      this.ImgBg.set_texture((Texture) tex);
      ((Component) this.ImgBg).get_gameObject().SetActive(true);
    }
  }
}
