﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_AdvanceLapBossParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_AdvanceLapBossParam
  {
    public string iname;
    public int round_buff_max;
    public string boss_bid;
    public string other_bid;
    public JSON_AdvanceLapBossParam.LapInfo[] lap_info;

    [MessagePackObject(true)]
    [Serializable]
    public class LapInfo
    {
      public int round;
      public string reward_id;
    }
  }
}
