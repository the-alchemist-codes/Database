﻿// Decompiled with JetBrains decompiler
// Type: SRPG.MultiPlayVersusGradientFade
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class MultiPlayVersusGradientFade : MonoBehaviour
  {
    private static MultiPlayVersusGradientFade sInstance;
    private const string STATE_FADE_IN = "FadeIn";
    private const string STATE_FADE_OUT = "FadeOut";
    private const string STATE_FADE_IN_FINISH = "FadeInFinish";
    private const string STATE_FADE_OUT_FINISH = "FadeOutFinish";
    private bool mFading;
    private Animator mAnimator;
    private CanvasGroup mCanvasGroup;
    private string mStateName;

    public MultiPlayVersusGradientFade()
    {
      base.\u002Ector();
    }

    public static MultiPlayVersusGradientFade Instance
    {
      get
      {
        return MultiPlayVersusGradientFade.sInstance;
      }
    }

    public bool Fading
    {
      get
      {
        return this.mFading;
      }
    }

    private void Awake()
    {
      if (Object.op_Inequality((Object) MultiPlayVersusGradientFade.sInstance, (Object) null))
        Object.Destroy((Object) this);
      MultiPlayVersusGradientFade.sInstance = this;
      this.mAnimator = (Animator) ((Component) this).GetComponent<Animator>();
      this.mCanvasGroup = (CanvasGroup) ((Component) this).GetComponent<CanvasGroup>();
    }

    private void OnDestroy()
    {
      if (!Object.op_Equality((Object) MultiPlayVersusGradientFade.sInstance, (Object) this))
        return;
      MultiPlayVersusGradientFade.sInstance = (MultiPlayVersusGradientFade) null;
    }

    public void FadeIn()
    {
      if (this.mFading)
        return;
      this.mAnimator.Play(nameof (FadeIn));
      this.mStateName = "FadeInFinish";
      this.mFading = true;
    }

    public void FadeOut()
    {
      if (this.mFading)
        return;
      this.mCanvasGroup.set_blocksRaycasts(true);
      this.mAnimator.Play(nameof (FadeOut));
      this.mStateName = "FadeOutFinish";
      this.mFading = true;
    }

    private void Update()
    {
      if (!this.mFading)
        return;
      AnimatorStateInfo animatorStateInfo = this.mAnimator.GetCurrentAnimatorStateInfo(0);
      if (!((AnimatorStateInfo) ref animatorStateInfo).IsName(this.mStateName))
        return;
      if (this.mStateName == "FadeInFinish")
        this.mCanvasGroup.set_blocksRaycasts(false);
      this.mFading = false;
    }
  }
}
