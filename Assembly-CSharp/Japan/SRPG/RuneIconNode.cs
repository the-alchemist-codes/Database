﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RuneIconNode
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class RuneIconNode : ContentNode
  {
    [SerializeField]
    public RuneIcon Icon;

    public void Setup(BindRuneData rune_data)
    {
      if (Object.op_Equality((Object) this.Icon, (Object) null) || rune_data == null)
        return;
      this.Icon.Setup(rune_data, false);
      ((Component) this.Icon).get_gameObject().SetActive(true);
    }
  }
}
