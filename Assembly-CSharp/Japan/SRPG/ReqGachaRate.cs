﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqGachaRate
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class ReqGachaRate : WebAPI
  {
    public ReqGachaRate(string gachaid, Network.ResponseCallback response)
    {
      this.name = "gacha/slot_info";
      this.body = WebAPI.GetRequestString("\"gachaid\":\"" + gachaid + "\"");
      this.callback = response;
    }
  }
}
