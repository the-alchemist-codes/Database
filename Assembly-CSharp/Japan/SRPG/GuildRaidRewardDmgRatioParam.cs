﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildRaidRewardDmgRatioParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace SRPG
{
  public class GuildRaidRewardDmgRatioParam : GuildRaidMasterParam<JSON_GuildRaidRewardDmgRatioParam>
  {
    public string Id { get; private set; }

    public List<GuildRaidRewardDmgRatio> Reward { get; private set; }

    public override bool Deserialize(JSON_GuildRaidRewardDmgRatioParam json)
    {
      if (json == null)
        return false;
      this.Id = json.id;
      if (json.rewards == null || json.rewards.Length == 0)
        return false;
      this.Reward = new List<GuildRaidRewardDmgRatio>();
      for (int index = 0; index < json.rewards.Length; ++index)
      {
        GuildRaidRewardDmgRatio raidRewardDmgRatio = new GuildRaidRewardDmgRatio();
        if (raidRewardDmgRatio.Deserialize(json.rewards[index]))
          this.Reward.Add(raidRewardDmgRatio);
      }
      return true;
    }
  }
}
