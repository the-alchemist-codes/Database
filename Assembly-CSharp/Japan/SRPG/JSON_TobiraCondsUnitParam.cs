﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_TobiraCondsUnitParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_TobiraCondsUnitParam
  {
    public string id;
    public string unit_iname;
    public int lv;
    public int awake_lv;
    public JSON_TobiraCondsUnitParam.JobCond[] jobs;
    public int category;
    public int tobira_lv;

    [MessagePackObject(true)]
    [Serializable]
    public class JobCond
    {
      public string job_iname;
      public int job_lv;
    }
  }
}
