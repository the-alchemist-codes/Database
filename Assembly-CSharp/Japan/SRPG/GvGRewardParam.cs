﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GvGRewardParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System;
using System.Collections.Generic;

namespace SRPG
{
  public class GvGRewardParam : GvGMasterParam<JSON_GvGRewardParam>
  {
    public string Id { get; private set; }

    public List<GvGRewardDetailParam> Rewards { get; private set; }

    public override bool Deserialize(JSON_GvGRewardParam json)
    {
      if (json == null)
        return false;
      this.Id = json.id;
      this.Rewards = new List<GvGRewardDetailParam>();
      if (json.rewards != null)
      {
        for (int index = 0; index < json.rewards.Length; ++index)
        {
          GvGRewardDetailParam grewardDetailParam = new GvGRewardDetailParam();
          if (grewardDetailParam.Deserialize(json.rewards[index]))
            this.Rewards.Add(grewardDetailParam);
        }
      }
      return true;
    }

    public static GvGRewardParam GetReward(string id)
    {
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) MonoSingleton<GameManager>.Instance, (UnityEngine.Object) null))
        return (GvGRewardParam) null;
      List<GvGRewardParam> mGvGrewardParam = MonoSingleton<GameManager>.Instance.mGvGRewardParam;
      if (mGvGrewardParam != null)
        return mGvGrewardParam.Find((Predicate<GvGRewardParam>) (r => r != null && r.Id == id));
      DebugUtility.Log("<color=yellow>QuestParam/mGvGRewardParam no data!</color>");
      return (GvGRewardParam) null;
    }
  }
}
