﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqRuneFavorite
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using Gsc.Network.Encoding;
using MessagePack;
using System;

namespace SRPG
{
  public class ReqRuneFavorite : WebAPI
  {
    public ReqRuneFavorite(
      ReqRuneFavorite.RequestParam rp,
      SRPG.Network.ResponseCallback response,
      EncodingTypes.ESerializeCompressMethod serializeCompressMethod)
    {
      this.name = "unit/rune/favorite";
      this.body = WebAPI.GetRequestString<ReqRuneFavorite.RequestParam>(rp);
      this.callback = response;
      this.serializeCompressMethod = serializeCompressMethod;
    }

    [MessagePackObject(true)]
    [Serializable]
    public class RequestParam
    {
      public long rune_id;
      public int favorite;

      public RequestParam()
      {
      }

      public RequestParam(long runeId, bool isFavorite)
      {
        this.rune_id = runeId;
        this.favorite = !isFavorite ? 0 : 1;
      }
    }

    [MessagePackObject(true)]
    [Serializable]
    public class Response
    {
      public Json_RuneData rune;
    }
  }
}
