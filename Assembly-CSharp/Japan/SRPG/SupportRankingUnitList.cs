﻿// Decompiled with JetBrains decompiler
// Type: SRPG.SupportRankingUnitList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class SupportRankingUnitList : MonoBehaviour
  {
    [SerializeField]
    private ImageArray mRankImage;
    [SerializeField]
    private Text mRankText;
    [SerializeField]
    private Text mName;
    [SerializeField]
    private Text mScore;
    [SerializeField]
    private RawImage_Transparent mJobIcon;
    private const int DEFAULT_RANKING_NOTEXT = 3;

    public SupportRankingUnitList()
    {
      base.\u002Ector();
    }

    private void Awake()
    {
      this.Refresh();
    }

    private void Refresh()
    {
      SupportUnitRanking dataOfClass = DataSource.FindDataOfClass<SupportUnitRanking>(((Component) this).get_gameObject(), (SupportUnitRanking) null);
      if (dataOfClass == null || dataOfClass.rank <= 0)
        return;
      if (Object.op_Inequality((Object) this.mRankImage, (Object) null))
      {
        int num = dataOfClass.rank - 1;
        if (num >= this.mRankImage.Images.Length)
          ((Component) this.mRankImage).get_gameObject().SetActive(false);
        else
          this.mRankImage.ImageIndex = num;
      }
      if (Object.op_Inequality((Object) this.mRankText, (Object) null) && dataOfClass.rank > 3)
        this.mRankText.set_text(string.Format(LocalizedText.Get("sys.SUPPORT_SET_RANK"), (object) dataOfClass.rank.ToString()));
      JobParam jobParam = MonoSingleton<GameManager>.Instance.GetJobParam(dataOfClass.jobName);
      if (jobParam != null)
      {
        if (Object.op_Inequality((Object) this.mJobIcon, (Object) null))
          MonoSingleton<GameManager>.Instance.ApplyTextureAsync((RawImage) this.mJobIcon, jobParam == null ? (string) null : AssetPath.JobIconSmall(jobParam));
        if (Object.op_Inequality((Object) this.mName, (Object) null))
          this.mName.set_text(string.Format(LocalizedText.Get("sys.SUPPORT_SET_UNITNAME"), (object) dataOfClass.unit.UnitParam.name, (object) jobParam.name));
      }
      if (Object.op_Inequality((Object) this.mScore, (Object) null))
        this.mScore.set_text(string.Format(LocalizedText.Get("sys.SUPPORT_SET_SCORE"), (object) dataOfClass.score));
      if (dataOfClass.unit == null)
        return;
      DataSource.Bind<UnitData>(((Component) this).get_gameObject(), dataOfClass.unit, false);
    }
  }
}
