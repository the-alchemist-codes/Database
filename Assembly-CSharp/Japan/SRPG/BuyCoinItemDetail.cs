﻿// Decompiled with JetBrains decompiler
// Type: SRPG.BuyCoinItemDetail
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class BuyCoinItemDetail : MonoBehaviour, IFlowInterface
  {
    [SerializeField]
    private GameObject ItemDetailWindow;
    [SerializeField]
    private GameObject ArtifactDetailWindow;
    [SerializeField]
    private GameObject ConceptCardDetail;
    [SerializeField]
    private Text DetailTitle;
    [SerializeField]
    private GameObject ItemIcon;
    [SerializeField]
    private RewardListItem ItemIconList;

    public BuyCoinItemDetail()
    {
      base.\u002Ector();
    }

    private void Start()
    {
      this.Refresh();
    }

    public void Activated(int pinID)
    {
    }

    public void OnButtonItem(GameObject go)
    {
      ItemParam dataOfClass = DataSource.FindDataOfClass<ItemParam>(go, (ItemParam) null);
      if (dataOfClass == null || !Object.op_Inequality((Object) this.ItemDetailWindow, (Object) null))
        return;
      ItemData data = new ItemData();
      int itemAmount = MonoSingleton<GameManager>.Instance.Player.GetItemAmount(dataOfClass.iname);
      data.Setup(0L, dataOfClass, itemAmount);
      DataSource.Bind<ItemData>((GameObject) Object.Instantiate<GameObject>((M0) this.ItemDetailWindow), data, false);
    }

    public void OnButtonAtrifact(GameObject go)
    {
      if (Object.op_Equality((Object) go, (Object) null))
        return;
      ArtifactParam dataOfClass = DataSource.FindDataOfClass<ArtifactParam>(go, (ArtifactParam) null);
      if (dataOfClass == null || !Object.op_Inequality((Object) this.ArtifactDetailWindow, (Object) null))
        return;
      ArtifactData artifactDataForDisplay = ArtifactData.CreateArtifactDataForDisplay(dataOfClass, dataOfClass.raremax);
      if (artifactDataForDisplay == null)
        return;
      DataSource.Bind<ArtifactData>((GameObject) Object.Instantiate<GameObject>((M0) this.ArtifactDetailWindow), artifactDataForDisplay, false);
    }

    public void OnButtonConceptCard(GameObject go)
    {
      if (Object.op_Equality((Object) go, (Object) null))
        return;
      ConceptCardIcon dataOfClass = DataSource.FindDataOfClass<ConceptCardIcon>(go, (ConceptCardIcon) null);
      if (!Object.op_Inequality((Object) dataOfClass, (Object) null) || !Object.op_Inequality((Object) this.ConceptCardDetail, (Object) null))
        return;
      GlobalVars.SelectedConceptCardData.Set(dataOfClass.ConceptCard);
      Object.Instantiate<GameObject>((M0) this.ConceptCardDetail);
    }

    private void Refresh()
    {
      BuyCoinProductParam dataOfClass1 = DataSource.FindDataOfClass<BuyCoinProductParam>(((Component) this).get_gameObject(), (BuyCoinProductParam) null);
      PaymentManager.Product dataOfClass2 = DataSource.FindDataOfClass<PaymentManager.Product>(((Component) this).get_gameObject(), (PaymentManager.Product) null);
      if (dataOfClass2 == null || dataOfClass1 == null || (!Object.op_Inequality((Object) this.ItemIcon, (Object) null) || !Object.op_Inequality((Object) this.ItemIconList, (Object) null)))
        return;
      GameManager instance = MonoSingleton<GameManager>.Instance;
      BuyCoinRewardParam buyCoinRewardParam = instance.MasterParam.GetBuyCoinRewardParam(dataOfClass1.Reward);
      if (buyCoinRewardParam == null)
        return;
      if (Object.op_Inequality((Object) this.DetailTitle, (Object) null))
        this.DetailTitle.set_text(LocalizedText.Get("sys.BUYCOIN_DETAILTITLE", (object) dataOfClass2.name));
      for (int index = 0; index < buyCoinRewardParam.Reward.Count; ++index)
      {
        GameObject gameObject1 = (GameObject) Object.Instantiate<GameObject>((M0) this.ItemIcon);
        gameObject1.SetActive(true);
        gameObject1.get_transform().SetParent(this.ItemIcon.get_transform().get_parent(), false);
        RewardListItem componentInChildren1 = (RewardListItem) gameObject1.GetComponentInChildren<RewardListItem>();
        Text componentInChildren2 = (Text) gameObject1.GetComponentInChildren<Text>();
        GameObject gameObject2 = (GameObject) null;
        switch (buyCoinRewardParam.Reward[index].Type)
        {
          case BuyCoinManager.PremiumRewadType.Item:
            ItemParam itemParam = instance.GetItemParam(buyCoinRewardParam.Reward[index].Iname);
            if (itemParam != null)
            {
              gameObject2 = componentInChildren1.RewardItem;
              DataSource.Bind<ItemParam>(gameObject2, itemParam, false);
              componentInChildren2.set_text(LocalizedText.Get("sys.BUYCOIN_SET", (object) itemParam.name, (object) buyCoinRewardParam.Reward[index].Num.ToString()));
              break;
            }
            break;
          case BuyCoinManager.PremiumRewadType.Gold:
            gameObject2 = componentInChildren1.RewardGold;
            componentInChildren2.set_text(LocalizedText.Get("sys.BUYCOIN_GOLD", (object) buyCoinRewardParam.Reward[index].Num.ToString()));
            break;
          case BuyCoinManager.PremiumRewadType.Coin:
            gameObject2 = componentInChildren1.RewardCoin;
            componentInChildren2.set_text(LocalizedText.Get("sys.BUYCOIN_COIN", (object) buyCoinRewardParam.Reward[index].Num.ToString()));
            break;
          case BuyCoinManager.PremiumRewadType.Artifact:
            ArtifactParam artifactParam = instance.MasterParam.GetArtifactParam(buyCoinRewardParam.Reward[index].Iname);
            if (artifactParam != null)
            {
              gameObject2 = componentInChildren1.RewardArtifact;
              DataSource.Bind<ArtifactParam>(gameObject2, artifactParam, false);
              componentInChildren2.set_text(LocalizedText.Get("sys.BUYCOIN_SET", (object) artifactParam.name, (object) buyCoinRewardParam.Reward[index].Num.ToString()));
              break;
            }
            break;
          case BuyCoinManager.PremiumRewadType.ConceptCard:
            ConceptCardData cardDataForDisplay = ConceptCardData.CreateConceptCardDataForDisplay(buyCoinRewardParam.Reward[index].Iname);
            if (cardDataForDisplay != null)
            {
              gameObject2 = componentInChildren1.RewardCard;
              ConceptCardIcon component = (ConceptCardIcon) gameObject2.GetComponent<ConceptCardIcon>();
              if (Object.op_Inequality((Object) component, (Object) null))
              {
                DataSource.Bind<ConceptCardIcon>(gameObject2, component, false);
                component.Setup(cardDataForDisplay);
                componentInChildren2.set_text(LocalizedText.Get("sys.BUYCOIN_SET", (object) cardDataForDisplay.Param.name, (object) buyCoinRewardParam.Reward[index].Num.ToString()));
                break;
              }
              break;
            }
            break;
          case BuyCoinManager.PremiumRewadType.Unit:
            UnitParam unitParam = instance.GetUnitParam(buyCoinRewardParam.Reward[index].Iname);
            if (unitParam != null)
            {
              gameObject2 = componentInChildren1.RewardUnit;
              DataSource.Bind<UnitParam>(gameObject2, unitParam, false);
              componentInChildren2.set_text(LocalizedText.Get("sys.BUYCOIN_SET", (object) unitParam.name, (object) buyCoinRewardParam.Reward[index].Num.ToString()));
              break;
            }
            break;
        }
        if (Object.op_Inequality((Object) gameObject2, (Object) null))
        {
          gameObject2.get_transform().SetParent(componentInChildren1.RewardList, false);
          gameObject2.SetActive(true);
        }
      }
    }
  }
}
