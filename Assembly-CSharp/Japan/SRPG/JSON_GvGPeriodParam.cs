﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_GvGPeriodParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_GvGPeriodParam : JSON_GvGMasterParam
  {
    public int id;
    public string begin_at;
    public string end_at;
    public string exit_at;
    public string declaration_start_time;
    public string declaration_end_time;
    public string battle_start_time;
    public string battle_end_time;
    public int declared_cool_minutes;
    public int battle_cool_seconds;
    public int declare_num;
    public int map_idx;
    public int matching_count_min;
    public int matching_count_max;
    public int first_occupy_node_num;
    public int defense_unit_min;
    public string url;
    public string[] rule_cycle;
    public string url_title;
  }
}
