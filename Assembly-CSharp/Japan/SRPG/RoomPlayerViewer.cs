﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RoomPlayerViewer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class RoomPlayerViewer : MonoBehaviour
  {
    public GameObject[] PartyUnitSlots;

    public RoomPlayerViewer()
    {
      base.\u002Ector();
    }

    private void Start()
    {
      JSON_MyPhotonPlayerParam multiPlayerParam = GlobalVars.SelectedMultiPlayerParam;
      if (multiPlayerParam == null || multiPlayerParam.units == null || this.PartyUnitSlots == null)
        return;
      for (int index1 = 0; index1 < this.PartyUnitSlots.Length; ++index1)
      {
        for (int index2 = 0; index2 < multiPlayerParam.units.Length; ++index2)
        {
          if (multiPlayerParam.units[index2].slotID == index1)
          {
            DataSource.Bind<UnitData>(this.PartyUnitSlots[index1], multiPlayerParam.units[index2].unit, false);
            break;
          }
        }
      }
      DataSource.Bind<JSON_MyPhotonPlayerParam>(((Component) this).get_gameObject(), multiPlayerParam, false);
      DataSource.Bind<ViewGuildData>(((Component) this).get_gameObject(), new ViewGuildData()
      {
        id = multiPlayerParam.guild_id,
        name = multiPlayerParam.guild_name
      }, false);
      SerializeValueBehaviour component = (SerializeValueBehaviour) ((Component) this).GetComponent<SerializeValueBehaviour>();
      if (Object.op_Inequality((Object) component, (Object) null))
        component.list.SetField(GuildSVB_Key.GUILD_ID, multiPlayerParam.guild_id);
      GameParameter.UpdateAll(((Component) this).get_gameObject());
    }
  }
}
