﻿// Decompiled with JetBrains decompiler
// Type: SRPG.TowerRankInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace SRPG
{
  public class TowerRankInfo : MonoBehaviour
  {
    private readonly int SHOW_VIP_RANK;
    public Text Ranking;
    public Image OwnIcon;
    public GameObject OwnObj;
    public Image OwnTotalScore;
    public Text OwnSpeedScore;
    public Text OwnTechScore;
    public GameObject OwnSpeedObj;
    public GameObject OwnTechObj;
    [HeaderBar("▼「ランキング」表示用（最上階までクリア済み）")]
    public GameObject ClearPage;
    public GameObject ClearPageParent;
    public ListItemEvents ClearTemplate;
    [HeaderBar("▼「ランキング」表示用（最上階まで未クリア）")]
    public GameObject NotClearPage;
    public GameObject NotClearPageParent;
    public ListItemEvents NotClearTemplate;
    [HeaderBar("▼「ランキング」表示用（集計中）")]
    public GameObject NotDataObj;
    [HeaderBar("▼「自分の戦績」表示用オブジェクトの親")]
    [SerializeField]
    private GameObject m_TowerPlayerInfoRoot;
    [HeaderBar("▼タブ")]
    public Toggle Speed;
    public Toggle Tech;
    public Toggle OwnStatus;
    public GameObject Root;
    private SpriteSheet mSheet;
    private bool IsSpeed;

    public TowerRankInfo()
    {
      base.\u002Ector();
    }

    private void Start()
    {
      this.mSheet = AssetManager.Load<SpriteSheet>("UI/TowerRankIcon");
      this.UpdateOwnValue();
      this.UpdateRankValue(true);
      if (Object.op_Inequality((Object) this.Speed, (Object) null))
      {
        // ISSUE: method pointer
        ((UnityEvent<bool>) this.Speed.onValueChanged).AddListener(new UnityAction<bool>((object) this, __methodptr(OnChangeSpeed)));
      }
      if (Object.op_Inequality((Object) this.Tech, (Object) null))
      {
        // ISSUE: method pointer
        ((UnityEvent<bool>) this.Tech.onValueChanged).AddListener(new UnityAction<bool>((object) this, __methodptr(OnChangeTech)));
      }
      if (Object.op_Inequality((Object) this.OwnStatus, (Object) null))
      {
        // ISSUE: method pointer
        ((UnityEvent<bool>) this.OwnStatus.onValueChanged).AddListener(new UnityAction<bool>((object) this, __methodptr(OnChangeOwnStatus)));
      }
      if (!Object.op_Inequality((Object) this.OwnObj, (Object) null))
        return;
      DataSource.Bind<PlayerData>(this.OwnObj, MonoSingleton<GameManager>.Instance.Player, false);
    }

    private int GetSameRank(int score, int rank)
    {
      TowerResuponse towerResuponse = MonoSingleton<GameManager>.Instance.TowerResuponse;
      if (towerResuponse.SpdRank != null && towerResuponse.SpdRank.Length > 0 || towerResuponse.TecRank != null && towerResuponse.TecRank.Length > 0)
      {
        TowerResuponse.TowerRankParam[] towerRankParamArray = !this.IsSpeed ? towerResuponse.TecRank : towerResuponse.SpdRank;
        for (int index = 0; index < towerRankParamArray.Length; ++index)
        {
          if (towerRankParamArray[index].score == score)
            return index + 1;
        }
      }
      return rank;
    }

    private void UpdateOwnValue()
    {
      GameManager instance = MonoSingleton<GameManager>.Instance;
      TowerResuponse towerResuponse = instance.TowerResuponse;
      TowerParam tower = instance.FindTower(towerResuponse.TowerID);
      if (towerResuponse != null)
      {
        bool flag = towerResuponse.speedRank != 0 && towerResuponse.techRank != 0;
        if (Object.op_Inequality((Object) this.ClearPage, (Object) null))
          this.ClearPage.get_gameObject().SetActive(flag);
        if (Object.op_Inequality((Object) this.NotClearPage, (Object) null))
          this.NotClearPage.get_gameObject().SetActive(!flag);
      }
      int rank = !this.IsSpeed ? towerResuponse.techRank : towerResuponse.speedRank;
      int sameRank = this.GetSameRank(!this.IsSpeed ? towerResuponse.tec_score : towerResuponse.spd_score, rank);
      if (Object.op_Inequality((Object) this.OwnIcon, (Object) null))
      {
        if (sameRank <= this.SHOW_VIP_RANK)
          this.OwnIcon.set_sprite(this.mSheet.GetSprite((sameRank - 1).ToString()));
        else
          this.OwnIcon.set_sprite(this.mSheet.GetSprite("normal"));
      }
      if (Object.op_Inequality((Object) this.Ranking, (Object) null))
      {
        ((Component) this.Ranking).get_gameObject().SetActive(sameRank > this.SHOW_VIP_RANK);
        this.Ranking.set_text(sameRank.ToString() + LocalizedText.Get("sys.TOWER_RANK_LABEL"));
      }
      if (Object.op_Inequality((Object) this.OwnObj, (Object) null))
      {
        PlayerData player = instance.Player;
        long unitUniqueId = player.Partys[6].GetUnitUniqueID(0);
        DataSource.Bind<UnitData>(this.OwnObj, player.FindUnitDataByUniqueID(unitUniqueId), false);
      }
      if (Object.op_Inequality((Object) this.OwnSpeedObj, (Object) null))
        this.OwnSpeedObj.get_gameObject().SetActive(this.IsSpeed);
      if (Object.op_Inequality((Object) this.OwnTechObj, (Object) null))
        this.OwnTechObj.get_gameObject().SetActive(!this.IsSpeed);
      if (this.IsSpeed)
      {
        if (Object.op_Inequality((Object) this.OwnSpeedScore, (Object) null))
          this.OwnSpeedScore.set_text(towerResuponse.spd_score.ToString());
      }
      else if (Object.op_Inequality((Object) this.OwnSpeedScore, (Object) null))
        this.OwnTechScore.set_text(towerResuponse.tec_score.ToString());
      if (!Object.op_Inequality((Object) this.OwnTotalScore, (Object) null))
        return;
      string empty = string.Empty;
      this.OwnTotalScore.set_sprite(this.mSheet.GetSprite(!this.IsSpeed ? instance.ConvertTowerScoreToRank(tower, towerResuponse.tec_score, TOWER_SCORE_TYPE.DIED) : instance.ConvertTowerScoreToRank(tower, towerResuponse.spd_score, TOWER_SCORE_TYPE.TURN)));
    }

    private void UpdateRankValue(bool isSpeedRank = true)
    {
      TowerResuponse towerResuponse = MonoSingleton<GameManager>.Instance.TowerResuponse;
      GameObject gameObject = this.NotClearPageParent;
      ListItemEvents listItemEvents1 = this.NotClearTemplate;
      if (towerResuponse != null)
      {
        gameObject = towerResuponse.speedRank == 0 || towerResuponse.techRank == 0 ? this.NotClearPageParent : this.ClearPageParent;
        listItemEvents1 = towerResuponse.speedRank == 0 || towerResuponse.techRank == 0 ? this.NotClearTemplate : this.ClearTemplate;
        if (towerResuponse.SpdRank == null || towerResuponse.SpdRank.Length == 0 || (towerResuponse.TecRank == null || towerResuponse.TecRank.Length == 0))
        {
          if (!Object.op_Inequality((Object) this.NotDataObj, (Object) null))
            return;
          this.NotDataObj.get_gameObject().SetActive(true);
          return;
        }
      }
      if (Object.op_Inequality((Object) this.NotClearTemplate, (Object) null))
        ((Component) this.NotClearTemplate).get_gameObject().SetActive(false);
      if (Object.op_Inequality((Object) this.ClearTemplate, (Object) null))
        ((Component) this.ClearTemplate).get_gameObject().SetActive(false);
      Transform transform = gameObject.get_transform();
      for (int index = transform.get_childCount() - 1; index >= 0; --index)
      {
        Transform child = transform.GetChild(index);
        if (!Object.op_Equality((Object) child, (Object) null) && ((Component) child).get_gameObject().get_activeSelf())
          Object.DestroyImmediate((Object) ((Component) child).get_gameObject());
      }
      if (Object.op_Inequality((Object) gameObject, (Object) null))
        gameObject.get_gameObject().SetActive(true);
      TowerResuponse.TowerRankParam[] towerRankParamArray = !isSpeedRank ? towerResuponse.TecRank : towerResuponse.SpdRank;
      for (int index1 = 0; index1 < towerRankParamArray.Length; ++index1)
      {
        ListItemEvents listItemEvents2 = (ListItemEvents) Object.Instantiate<ListItemEvents>((M0) listItemEvents1);
        int num1 = index1;
        if (Object.op_Inequality((Object) listItemEvents2, (Object) null))
        {
          DataSource.Bind<UnitData>(((Component) listItemEvents2).get_gameObject(), towerRankParamArray[index1].unit, false);
          DataSource.Bind<ViewGuildData>(((Component) listItemEvents2).get_gameObject(), towerRankParamArray[index1].ViewGuild, false);
          SerializeValueBehaviour component = (SerializeValueBehaviour) ((Component) listItemEvents2).GetComponent<SerializeValueBehaviour>();
          if (Object.op_Inequality((Object) component, (Object) null))
          {
            long num2 = towerRankParamArray[index1].ViewGuild == null ? 0L : (long) towerRankParamArray[index1].ViewGuild.id;
            component.list.SetField(GuildSVB_Key.GUILD_ID, (int) num2);
          }
          listItemEvents2.OnSelect = new ListItemEvents.ListItemEvent(this.OnItemSelect);
          listItemEvents2.OnOpenDetail = new ListItemEvents.ListItemEvent(this.OnItemDetail);
          ((Component) listItemEvents2).get_transform().SetParent(gameObject.get_transform(), false);
          ((Component) listItemEvents2).get_gameObject().SetActive(true);
          for (int index2 = 0; index2 < index1; ++index2)
          {
            if (towerRankParamArray[index2].score == towerRankParamArray[index1].score)
            {
              num1 = index2;
              break;
            }
          }
          this.UpdateValue(listItemEvents2, num1, towerRankParamArray[index1], isSpeedRank);
        }
      }
      if (!Object.op_Inequality((Object) this.Root, (Object) null))
        return;
      GameParameter.UpdateAll(this.Root);
    }

    private void UpdateValue(
      ListItemEvents obj,
      int num,
      TowerResuponse.TowerRankParam param,
      bool isSpeed)
    {
      if (Object.op_Equality((Object) this.mSheet, (Object) null))
        return;
      GameManager instance = MonoSingleton<GameManager>.Instance;
      TowerResuponse towerResuponse = MonoSingleton<GameManager>.Instance.TowerResuponse;
      TowerParam tower = instance.FindTower(towerResuponse.TowerID);
      DataSource.Bind<TowerResuponse.TowerRankParam>(((Component) obj).get_gameObject(), param, false);
      Transform transform1 = ((Component) obj).get_transform().Find("body");
      if (!Object.op_Inequality((Object) transform1, (Object) null))
        return;
      Transform transform2 = ((Component) transform1).get_transform().Find("ranking");
      if (Object.op_Inequality((Object) transform2, (Object) null))
      {
        Image component = (Image) ((Component) transform2).GetComponent<Image>();
        if (Object.op_Inequality((Object) component, (Object) null))
        {
          if (num < this.SHOW_VIP_RANK)
            component.set_sprite(this.mSheet.GetSprite(num.ToString()));
          else
            component.set_sprite(this.mSheet.GetSprite("normal"));
        }
      }
      Transform transform3 = ((Component) transform1).get_transform().Find("rank");
      if (Object.op_Inequality((Object) transform3, (Object) null))
      {
        Text component = (Text) ((Component) transform3).GetComponent<Text>();
        if (Object.op_Inequality((Object) component, (Object) null))
        {
          if (num < this.SHOW_VIP_RANK)
            component.set_text(string.Empty);
          else
            component.set_text((num + 1).ToString() + LocalizedText.Get("sys.TOWER_RANK_LABEL"));
        }
      }
      Transform transform4 = ((Component) transform1).get_transform().Find("Text_player");
      if (Object.op_Inequality((Object) transform4, (Object) null))
      {
        Text component = (Text) ((Component) transform4).GetComponent<Text>();
        if (Object.op_Inequality((Object) component, (Object) null))
          component.set_text(param.name);
      }
      Transform transform5 = ((Component) transform1).get_transform().Find("player_level");
      if (Object.op_Inequality((Object) transform5, (Object) null))
      {
        Text component = (Text) ((Component) transform5).GetComponent<Text>();
        if (Object.op_Inequality((Object) component, (Object) null))
          component.set_text(LocalizedText.Get("sys.TOWER_RANK_LBL_LV") + param.lv.ToString());
      }
      if (isSpeed)
      {
        Transform transform6 = ((Component) transform1).get_transform().Find("speed");
        if (Object.op_Inequality((Object) transform6, (Object) null))
        {
          Transform transform7 = ((Component) transform6).get_transform().Find("speed_cnt");
          if (Object.op_Inequality((Object) transform7, (Object) null))
          {
            Text component = (Text) ((Component) transform7).GetComponent<Text>();
            if (Object.op_Inequality((Object) component, (Object) null))
              component.set_text(param.score.ToString());
          }
          ((Component) transform6).get_gameObject().SetActive(true);
        }
        string rank = instance.ConvertTowerScoreToRank(tower, param.score, TOWER_SCORE_TYPE.TURN);
        Transform transform8 = ((Component) transform1).get_transform().Find("score_img");
        if (!Object.op_Inequality((Object) transform8, (Object) null))
          return;
        Image component1 = (Image) ((Component) transform8).GetComponent<Image>();
        if (!Object.op_Inequality((Object) component1, (Object) null))
          return;
        component1.set_sprite(this.mSheet.GetSprite(rank));
      }
      else
      {
        Transform transform6 = ((Component) transform1).get_transform().Find("tech");
        if (Object.op_Inequality((Object) transform6, (Object) null))
        {
          Transform transform7 = ((Component) transform6).get_transform().Find("tech_cnt");
          if (Object.op_Inequality((Object) transform7, (Object) null))
          {
            Text component = (Text) ((Component) transform7).GetComponent<Text>();
            if (Object.op_Inequality((Object) component, (Object) null))
              component.set_text(param.score.ToString());
          }
          ((Component) transform6).get_gameObject().SetActive(true);
        }
        string rank = instance.ConvertTowerScoreToRank(tower, param.score, TOWER_SCORE_TYPE.DIED);
        Transform transform8 = ((Component) transform1).get_transform().Find("score_img");
        if (!Object.op_Inequality((Object) transform8, (Object) null))
          return;
        Image component1 = (Image) ((Component) transform8).GetComponent<Image>();
        if (!Object.op_Inequality((Object) component1, (Object) null))
          return;
        component1.set_sprite(this.mSheet.GetSprite(rank));
      }
    }

    private void OnItemSelect(GameObject go)
    {
    }

    private void OnItemDetail(GameObject go)
    {
    }

    public void OnChangeSpeed(bool val)
    {
      if (!val)
        return;
      GameUtility.SetGameObjectActive(this.m_TowerPlayerInfoRoot, false);
      this.IsSpeed = true;
      this.UpdateOwnValue();
      this.UpdateRankValue(this.IsSpeed);
    }

    public void OnChangeTech(bool val)
    {
      if (!val)
        return;
      GameUtility.SetGameObjectActive(this.m_TowerPlayerInfoRoot, false);
      this.IsSpeed = false;
      this.UpdateOwnValue();
      this.UpdateRankValue(this.IsSpeed);
    }

    public void OnChangeOwnStatus(bool val)
    {
      if (!val)
        return;
      GameUtility.SetGameObjectActive(this.m_TowerPlayerInfoRoot, true);
      GameUtility.SetGameObjectActive(this.ClearPage, false);
      GameUtility.SetGameObjectActive(this.NotClearPage, false);
      GameUtility.SetGameObjectActive(this.NotDataObj, false);
    }
  }
}
