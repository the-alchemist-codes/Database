﻿// Decompiled with JetBrains decompiler
// Type: SRPG.LimitedShopGiftItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class LimitedShopGiftItem : MonoBehaviour
  {
    public Text Amount;
    public Text Name;
    public GameObject ItemIcon;
    public GameObject ArtifactIcon;
    public ConceptCardIcon m_ConceptCardIcon;

    public LimitedShopGiftItem()
    {
      base.\u002Ector();
    }

    public void SetShopItemInfo(Json_ShopItemDesc shop_item_desc, string name, int amount)
    {
      this.ItemIcon.SetActive(false);
      this.ArtifactIcon.SetActive(false);
      ((Component) this.m_ConceptCardIcon).get_gameObject().SetActive(false);
      if (shop_item_desc.IsItem)
        this.ItemIcon.SetActive(true);
      else if (shop_item_desc.IsArtifact)
        this.ArtifactIcon.SetActive(true);
      else if (shop_item_desc.IsConceptCard)
        ((Component) this.m_ConceptCardIcon).get_gameObject().SetActive(true);
      this.Amount.set_text((shop_item_desc.num * amount).ToString());
      this.Name.set_text(name);
    }
  }
}
