﻿// Decompiled with JetBrains decompiler
// Type: SRPG.EquipTree
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class EquipTree : MonoBehaviour
  {
    public Image CursorImage;

    public EquipTree()
    {
      base.\u002Ector();
    }

    public void SetIsLast(bool is_last)
    {
      ((Behaviour) this.CursorImage).set_enabled(is_last);
    }
  }
}
