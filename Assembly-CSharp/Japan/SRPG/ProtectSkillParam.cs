﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ProtectSkillParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System.Collections.Generic;

namespace SRPG
{
  [MessagePackObject(true)]
  public class ProtectSkillParam
  {
    public string mIname;
    public ProtectTypes mType;
    public DamageTypes mDamageType;
    public SkillRankUpValue mValue;
    public int mRange;
    public int mHeight;
    public int mInit;
    public int mMax;
    public const int ETERNAL_PROTECT_TURN = 99;

    public string Iname
    {
      get
      {
        return this.mIname;
      }
    }

    public ProtectTypes Type
    {
      get
      {
        return this.mType;
      }
    }

    public DamageTypes DamageType
    {
      get
      {
        return this.mDamageType;
      }
    }

    public SkillRankUpValue Value
    {
      get
      {
        return this.mValue;
      }
    }

    public int Range
    {
      get
      {
        return this.mRange;
      }
    }

    public int Height
    {
      get
      {
        return this.mHeight;
      }
    }

    public static void Deserialize(
      ref List<ProtectSkillParam> ref_params,
      JSON_ProtectSkillParam[] json)
    {
      if (ref_params == null)
        ref_params = new List<ProtectSkillParam>();
      ref_params.Clear();
      if (json == null)
        return;
      foreach (JSON_ProtectSkillParam json1 in json)
      {
        ProtectSkillParam protectSkillParam = new ProtectSkillParam();
        protectSkillParam.Deserialize(json1);
        ref_params.Add(protectSkillParam);
      }
    }

    public void Deserialize(JSON_ProtectSkillParam json)
    {
      if (json == null)
        return;
      this.mIname = json.iname;
      this.mType = (ProtectTypes) json.type;
      this.mDamageType = (DamageTypes) json.dmg_type;
      this.mValue = (SkillRankUpValue) null;
      this.mRange = json.range;
      this.mHeight = json.height;
      if (this.mType == ProtectTypes.None || this.mType != ProtectTypes.Turn && (this.mType == ProtectTypes.Turn || this.mDamageType == DamageTypes.None))
        return;
      this.mValue = new SkillRankUpValue();
      this.Value.ini = json.ini;
      this.Value.max = json.max;
    }

    public bool IsProtectEternal(int value)
    {
      return this.Type == ProtectTypes.Turn && value >= 99;
    }
  }
}
