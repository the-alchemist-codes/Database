﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RuneEnforceGaugeData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class RuneEnforceGaugeData
  {
    public byte rare;
    public short val;

    public bool Deserialize(Json_RuneEnforceGaugeData json)
    {
      if (json == null)
        return false;
      this.rare = (byte) json.rare;
      this.val = (short) json.val;
      return true;
    }
  }
}
