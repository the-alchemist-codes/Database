﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqVersusLineMake
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class ReqVersusLineMake : WebAPI
  {
    public ReqVersusLineMake(string roomname, Network.ResponseCallback response)
    {
      this.name = "vs/friendmatch/line/make";
      this.body = string.Empty;
      ReqVersusLineMake reqVersusLineMake1 = this;
      reqVersusLineMake1.body = reqVersusLineMake1.body + "\"token\":\"" + JsonEscape.Escape(roomname) + "\"";
      ReqVersusLineMake reqVersusLineMake2 = this;
      reqVersusLineMake2.body = reqVersusLineMake2.body + ",\"is_draft\":" + (object) (!GlobalVars.IsVersusDraftMode ? 0 : 1);
      this.body = WebAPI.GetRequestString(this.body);
      this.callback = response;
    }
  }
}
