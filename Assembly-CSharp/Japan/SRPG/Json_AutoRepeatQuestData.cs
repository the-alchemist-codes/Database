﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_AutoRepeatQuestData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class Json_AutoRepeatQuestData
  {
    public string qid;
    public int[] units;
    public string start_time;
    public int time_per_lap;
    public int lap_num;
    public int lap_num_not_box;
    public int is_full_box;
    public int stop_reason;
    public int current_lap_num;
    public int gold;
    public BattleCore.Json_BtlDrop[][] drops;
    public int auto_repeat_check;
  }
}
