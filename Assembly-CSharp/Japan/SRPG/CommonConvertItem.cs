﻿// Decompiled with JetBrains decompiler
// Type: SRPG.CommonConvertItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class CommonConvertItem : MonoBehaviour
  {
    public GameObject Obj;
    public GameObject CommonObj;
    public LText Amount;
    public LText ItemName;
    public Text ItemUseNum;
    public Text CommonItemUseNum;

    public CommonConvertItem()
    {
      base.\u002Ector();
    }

    public void Bind(ItemData data, ItemData cmmon_data, int need_num)
    {
      DataSource.Bind<ItemData>(this.Obj, data, false);
      DataSource.Bind<ItemData>(this.CommonObj, cmmon_data, false);
      this.Amount.set_text(LocalizedText.Get("sys.COMMON_EQUIP_NUM", (object) cmmon_data.Num));
      this.ItemName.set_text(LocalizedText.Get("sys.COMMON_EQUIP_NAME", (object) cmmon_data.Param.name, (object) need_num));
      Text itemUseNum = this.ItemUseNum;
      string str1 = need_num.ToString();
      this.CommonItemUseNum.set_text(str1);
      string str2 = str1;
      itemUseNum.set_text(str2);
    }

    public void Refresh(ItemData data, ItemData cmmon_data)
    {
    }
  }
}
