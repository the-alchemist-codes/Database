﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_LimitedShopResponse
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class Json_LimitedShopResponse
  {
    public JSON_LimitedShopItemListSet[] shopitems;
    public int relcnt;
    public int concept_count;
  }
}
