﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FixedScrollablePulldown
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class FixedScrollablePulldown : ScrollablePulldownBase
  {
    public void ResetAllItems()
    {
      foreach (PulldownItem pulldownItem in this.Items)
      {
        GameUtility.RemoveComponent<SRPG_Button>(((Component) pulldownItem).get_gameObject());
        ((Component) pulldownItem).get_gameObject().SetActive(false);
      }
      this.ResetAllStatus();
    }

    public PulldownItem SetItem(string label, int index, int value)
    {
      if (index < 0 || index >= this.Items.Count)
        return (PulldownItem) null;
      PulldownItem pulldownItem = this.Items[index];
      if (Object.op_Inequality((Object) pulldownItem.Text, (Object) null))
        pulldownItem.Text.set_text(label);
      pulldownItem.Value = value;
      GameObject gameObject = ((Component) pulldownItem).get_gameObject();
      GameUtility.RequireComponent<SRPG_Button>(gameObject).AddListener((SRPG_Button.ButtonClickEvent) (g =>
      {
        this.Selection = value;
        this.ClosePulldown(false);
        this.TriggerItemChange();
      }));
      gameObject.get_transform().SetParent((Transform) this.ItemHolder, false);
      gameObject.SetActive(true);
      return pulldownItem;
    }
  }
}
