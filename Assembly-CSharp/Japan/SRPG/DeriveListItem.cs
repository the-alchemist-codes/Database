﻿// Decompiled with JetBrains decompiler
// Type: SRPG.DeriveListItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class DeriveListItem : MonoBehaviour
  {
    [HeaderBar("▼派生先のスキル/アビリティの罫線")]
    [SerializeField]
    private RectTransform m_DeriveLineV;
    [SerializeField]
    private RectTransform m_DeriveLineH;

    public DeriveListItem()
    {
      base.\u002Ector();
    }

    public void SetLineActive(bool lineActive, bool verticalActive)
    {
      GameUtility.SetGameObjectActive((Component) this.m_DeriveLineH, lineActive);
      if (lineActive)
        GameUtility.SetGameObjectActive((Component) this.m_DeriveLineV, verticalActive);
      else
        GameUtility.SetGameObjectActive((Component) this.m_DeriveLineV, false);
    }
  }
}
