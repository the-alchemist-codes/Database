﻿// Decompiled with JetBrains decompiler
// Type: SRPG.SkillDeriveParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;

namespace SRPG
{
  [MessagePackObject(true)]
  public class SkillDeriveParam : BaseDeriveParam<SkillParam>
  {
    public string BaseSkillIname
    {
      get
      {
        return this.m_BaseParam.iname;
      }
    }

    public string DeriveSkillIname
    {
      get
      {
        return this.m_DeriveParam.iname;
      }
    }

    public string BaseSkillName
    {
      get
      {
        return this.m_BaseParam.name;
      }
    }

    public string DeriveSkillName
    {
      get
      {
        return this.m_DeriveParam.name;
      }
    }
  }
}
