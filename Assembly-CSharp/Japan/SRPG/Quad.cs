﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Quad
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using UnityEngine;

namespace SRPG
{
  [Serializable]
  public struct Quad
  {
    public Vector2 v0;
    public Color32 c0;
    public Vector2 v1;
    public Color32 c1;
    public Vector2 v2;
    public Color32 c2;
    public Vector2 v3;
    public Color32 c3;
  }
}
