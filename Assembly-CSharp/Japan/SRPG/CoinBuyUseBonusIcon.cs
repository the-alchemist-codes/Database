﻿// Decompiled with JetBrains decompiler
// Type: SRPG.CoinBuyUseBonusIcon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class CoinBuyUseBonusIcon : MonoBehaviour
  {
    [SerializeField]
    private Button mIcon;
    [SerializeField]
    private GameObject mBadge;
    private CoinBuyUseBonusParam[] mBonusParams;
    private bool mNeedWaitHomeApi;
    private static CoinBuyUseBonusIcon mInstance;

    public CoinBuyUseBonusIcon()
    {
      base.\u002Ector();
    }

    public static CoinBuyUseBonusIcon Instance
    {
      get
      {
        return CoinBuyUseBonusIcon.mInstance;
      }
    }

    private void Awake()
    {
      CoinBuyUseBonusIcon.mInstance = this;
      this.mNeedWaitHomeApi = CoinBuyUseBonusWindow.IsDirtyBonusProgress();
      this.mBonusParams = MonoSingleton<GameManager>.Instance.MasterParam.GetEnableCoinBuyUseBonusParams();
      ((Component) this.mIcon).get_gameObject().SetActive(this.mBonusParams != null);
      this.RefreshBadge();
    }

    public void RefreshBadge()
    {
      bool flag = false;
      if (this.mBonusParams != null)
      {
        for (int index = 0; index < this.mBonusParams.Length; ++index)
        {
          if (MonoSingleton<GameManager>.Instance.Player.IsExistReceivableCoinBuyUseBonus(this.mBonusParams[index].Trigger, this.mBonusParams[index].Type))
          {
            flag = true;
            break;
          }
        }
      }
      this.mBadge.SetActive(flag);
    }

    private void Update()
    {
      ((Selectable) this.mIcon).set_interactable(!this.mNeedWaitHomeApi);
      if (!this.mNeedWaitHomeApi || CoinBuyUseBonusWindow.IsDirtyBonusProgress())
        return;
      this.RefreshBadge();
      this.mNeedWaitHomeApi = false;
    }
  }
}
