﻿// Decompiled with JetBrains decompiler
// Type: SRPG.BuyCoinRewardParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace SRPG
{
  public class BuyCoinRewardParam
  {
    private List<BuyCoinRewardItemParam> mRewards = new List<BuyCoinRewardItemParam>();
    private string mId;
    private BuyCoinManager.PremiumRewadType mDrawType;
    private string mDrawIname;
    private string mGiftMessage;

    public string Id
    {
      get
      {
        return this.mId;
      }
    }

    public BuyCoinManager.PremiumRewadType DrawType
    {
      get
      {
        return this.mDrawType;
      }
    }

    public string DrawIname
    {
      get
      {
        return this.mDrawIname;
      }
    }

    public string GiftMessage
    {
      get
      {
        return this.mGiftMessage;
      }
    }

    public List<BuyCoinRewardItemParam> Reward
    {
      get
      {
        return this.mRewards;
      }
    }

    public bool Deserialize(JSON_BuyCoinRewardParam json)
    {
      if (json == null)
        return false;
      this.mId = json.id;
      this.mDrawType = (BuyCoinManager.PremiumRewadType) json.draw_type;
      this.mDrawIname = json.draw_iname;
      this.mGiftMessage = json.gift_message;
      this.mRewards.Clear();
      if (json.rewards != null)
      {
        for (int index = 0; index < json.rewards.Length; ++index)
          this.mRewards.Add(new BuyCoinRewardItemParam()
          {
            Iname = json.rewards[index].iname,
            Type = (BuyCoinManager.PremiumRewadType) json.rewards[index].type,
            Num = json.rewards[index].num
          });
      }
      return true;
    }
  }
}
