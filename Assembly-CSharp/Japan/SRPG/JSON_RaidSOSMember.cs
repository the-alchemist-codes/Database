﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_RaidSOSMember
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  [Serializable]
  public class JSON_RaidSOSMember
  {
    public string fuid;
    public string name;
    public int lv;
    public Json_Unit unit;
    public int member_type;
    public long last_battle_time;
  }
}
