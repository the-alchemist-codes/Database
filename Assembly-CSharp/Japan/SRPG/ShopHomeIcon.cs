﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ShopHomeIcon
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;

namespace SRPG
{
  [FlowNode.Pin(0, "Refresh", FlowNode.PinTypes.Input, 0)]
  public class ShopHomeIcon : MonoBehaviour, IFlowInterface
  {
    public GameObject ShopIcon;
    public GameObject GuerrillaIcon;
    private static ShopHomeIcon mInstance;

    public ShopHomeIcon()
    {
      base.\u002Ector();
    }

    public static ShopHomeIcon Instance
    {
      get
      {
        return ShopHomeIcon.mInstance;
      }
    }

    private void Awake()
    {
      ShopHomeIcon.mInstance = this;
    }

    private void OnDestroy()
    {
      ShopHomeIcon.mInstance = (ShopHomeIcon) null;
    }

    private void Start()
    {
      this.Refresh();
    }

    public void Refresh()
    {
      bool flag = MonoSingleton<GameManager>.Instance.Player.IsGuerrillaShopOpen();
      if (Object.op_Inequality((Object) this.ShopIcon, (Object) null))
        this.ShopIcon.SetActive(!flag);
      if (!Object.op_Inequality((Object) this.GuerrillaIcon, (Object) null))
        return;
      this.GuerrillaIcon.SetActive(flag);
    }

    public void Activated(int pinID)
    {
      if (pinID != 0)
        return;
      this.Refresh();
    }
  }
}
