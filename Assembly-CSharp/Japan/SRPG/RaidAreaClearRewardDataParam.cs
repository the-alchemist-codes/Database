﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RaidAreaClearRewardDataParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class RaidAreaClearRewardDataParam
  {
    private int mRound;
    private string mRewardId;

    public int Round
    {
      get
      {
        return this.mRound;
      }
    }

    public string RewardId
    {
      get
      {
        return this.mRewardId;
      }
    }

    public bool Deserialize(JSON_RaidAreaClearRewardDataParam json)
    {
      if (json == null)
        return false;
      this.mRound = json.round;
      this.mRewardId = json.reward_id;
      return true;
    }
  }
}
