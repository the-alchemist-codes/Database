﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqTrustMasterConceptCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Text;

namespace SRPG
{
  public class ReqTrustMasterConceptCard : WebAPI
  {
    public ReqTrustMasterConceptCard(
      long card_iid,
      bool is_favorite,
      Network.ResponseCallback response)
    {
      this.name = "unit/concept/trust/reward";
      StringBuilder stringBuilder = WebAPI.GetStringBuilder();
      stringBuilder.Append("\"concept_iid\":");
      stringBuilder.Append(card_iid);
      this.body = WebAPI.GetRequestString(stringBuilder.ToString());
      this.callback = response;
    }
  }
}
