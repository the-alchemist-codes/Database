﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GvGNode
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class GvGNode : MonoBehaviour
  {
    [SerializeField]
    private GameObject mOffenseGuild;
    [SerializeField]
    private GameObject mDefenseGuild;
    [SerializeField]
    private ImageArray mOffenseGuildImageArray;
    [SerializeField]
    private ImageArray mDefenseGuildImageArray;
    [SerializeField]
    private Text mNodeName;
    [SerializeField]
    private GameObject mCoolTime;
    [SerializeField]
    private Text mCoolTimeText;
    [SerializeField]
    private ImageArray mDefensePartyIcon;
    [SerializeField]
    private GameObject mPrepare;
    [SerializeField]
    private GameObject mPrepareAnimON;
    [SerializeField]
    private GameObject mPrepareAnimOFF;
    [SerializeField]
    private GameObject mBattle;
    [SerializeField]
    private GameObject mBattleAnimON;
    [SerializeField]
    private GameObject mBattleAnimOFF;
    [SerializeField]
    private GameObject mDefense;
    [SerializeField]
    private GameObject mDefenseAnimON;
    [SerializeField]
    private GameObject mDefenseAnimOFF;
    private GvGNodeData mGvGNodeData;
    private float mCoolTimeCount;

    public GvGNode()
    {
      base.\u002Ector();
    }

    private void Update()
    {
      if (this.mGvGNodeData != null && this.mGvGNodeData.IsAttackWait && (double) this.mCoolTimeCount > 0.0)
      {
        if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.mCoolTime, (UnityEngine.Object) null) && !this.mCoolTime.get_activeSelf())
          GameUtility.SetGameObjectActive(this.mCoolTime, true);
        TimeSpan timeSpan = this.mGvGNodeData.AttackEnableTime - TimeManager.ServerTime;
        this.mCoolTimeCount = (float) timeSpan.TotalSeconds;
        this.mCoolTimeText.set_text(string.Format(LocalizedText.Get("sys.GVG_DECLARE_COOL_TIME"), (object) timeSpan.Minutes, (object) timeSpan.Seconds));
      }
      else
      {
        if (!UnityEngine.Object.op_Inequality((UnityEngine.Object) this.mCoolTime, (UnityEngine.Object) null) || !this.mCoolTime.get_activeSelf())
          return;
        GameUtility.SetGameObjectActive(this.mCoolTime, false);
      }
    }

    public void Refresh(GvGNodeData data)
    {
      if (data == null)
      {
        GameUtility.SetGameObjectActive(this.mOffenseGuild, false);
        GameUtility.SetGameObjectActive(this.mDefenseGuild, false);
        GameUtility.SetGameObjectActive(this.mPrepare, false);
        GameUtility.SetGameObjectActive(this.mBattle, false);
        GameUtility.SetGameObjectActive(this.mDefense, false);
      }
      else
      {
        this.mGvGNodeData = data;
        if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.mNodeName, (UnityEngine.Object) null))
          this.mNodeName.set_text(data.NodeParam.Name);
        bool active = data.IsAttackWait && GvGManager.Instance.GvGStatusPhase == GvGManager.GvGStatus.Offense;
        GameUtility.SetGameObjectActive(this.mCoolTime, active);
        this.mCoolTimeCount = !active ? 0.0f : (float) (data.AttackEnableTime - TimeManager.ServerTime).TotalSeconds;
        ViewGuildData viewGuild1 = GvGManager.Instance.FindViewGuild(data.DeclaredGuildId);
        if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.mOffenseGuild, (UnityEngine.Object) null))
        {
          DataSource.Bind<ViewGuildData>(this.mOffenseGuild, viewGuild1, false);
          GameUtility.SetGameObjectActive(this.mOffenseGuild, viewGuild1 != null);
          if (viewGuild1 != null && UnityEngine.Object.op_Inequality((UnityEngine.Object) this.mOffenseGuildImageArray, (UnityEngine.Object) null))
          {
            int battleGuildImageIndex = GvGManager.Instance.GetNodeBattleGuildImageIndex(data, true);
            this.mOffenseGuildImageArray.ImageIndex = this.mOffenseGuildImageArray.Images.Length <= battleGuildImageIndex ? this.mOffenseGuildImageArray.Images.Length - 1 : battleGuildImageIndex;
          }
        }
        if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.mDefenseGuild, (UnityEngine.Object) null))
        {
          ViewGuildData viewGuild2 = GvGManager.Instance.FindViewGuild(data.GuildId);
          DataSource.Bind<ViewGuildData>(this.mDefenseGuild, viewGuild2, false);
          GameUtility.SetGameObjectActive(this.mDefenseGuild, viewGuild2 != null);
          if (viewGuild2 != null && UnityEngine.Object.op_Inequality((UnityEngine.Object) this.mDefenseGuildImageArray, (UnityEngine.Object) null))
          {
            int battleGuildImageIndex = GvGManager.Instance.GetNodeBattleGuildImageIndex(data, false);
            this.mDefenseGuildImageArray.ImageIndex = this.mDefenseGuildImageArray.Images.Length <= battleGuildImageIndex ? this.mDefenseGuildImageArray.Images.Length - 1 : battleGuildImageIndex;
          }
        }
        if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.mDefensePartyIcon, (UnityEngine.Object) null))
        {
          if (data.DefensePartyNum == 0)
            GameUtility.SetGameObjectActive((Component) this.mDefensePartyIcon, false);
          else if (this.mGvGNodeData.IsAttackWait)
          {
            GameUtility.SetGameObjectActive((Component) this.mDefensePartyIcon, false);
          }
          else
          {
            GameUtility.SetGameObjectActive((Component) this.mDefensePartyIcon, true);
            this.mDefensePartyIcon.ImageIndex = GvGManager.Instance.GetDefensePartyIconIndex(data);
          }
        }
        this.SetNodeStatus(data.State);
        GameParameter.UpdateAll(((Component) this).get_gameObject());
      }
    }

    private void SetNodeStatus(GvGNodeState state)
    {
      GameUtility.SetGameObjectActive(this.mPrepare, false);
      GameUtility.SetGameObjectActive(this.mBattle, false);
      GameUtility.SetGameObjectActive(this.mDefense, false);
      GameUtility.SetGameObjectActive(this.mPrepareAnimON, false);
      GameUtility.SetGameObjectActive(this.mPrepareAnimOFF, false);
      GameUtility.SetGameObjectActive(this.mBattleAnimON, false);
      GameUtility.SetGameObjectActive(this.mBattleAnimOFF, false);
      GameUtility.SetGameObjectActive(this.mDefenseAnimON, false);
      GameUtility.SetGameObjectActive(this.mDefenseAnimOFF, false);
      GvGManager instance = GvGManager.Instance;
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) instance, (UnityEngine.Object) null))
        return;
      GvGManager.GvGStatus gvGstatusPhase = instance.GvGStatusPhase;
      if (this.mGvGNodeData.IsAttackWait || gvGstatusPhase != GvGManager.GvGStatus.Offense && gvGstatusPhase != GvGManager.GvGStatus.Declaration && gvGstatusPhase != GvGManager.GvGStatus.DeclarationCoolTime || state != GvGNodeState.DeclareSelf && state != GvGNodeState.DeclareOther && state != GvGNodeState.DeclaredEnemy)
        return;
      GameUtility.SetGameObjectActive(this.mPrepare, gvGstatusPhase != GvGManager.GvGStatus.Offense);
      if (state == GvGNodeState.DeclaredEnemy && UnityEngine.Object.op_Inequality((UnityEngine.Object) this.mDefense, (UnityEngine.Object) null))
        GameUtility.SetGameObjectActive(this.mDefense, gvGstatusPhase == GvGManager.GvGStatus.Offense);
      else
        GameUtility.SetGameObjectActive(this.mBattle, gvGstatusPhase == GvGManager.GvGStatus.Offense);
      bool active = instance.GetNodeBattleGuildImageIndex(this.mGvGNodeData, true) != 0 && instance.GetNodeBattleGuildImageIndex(this.mGvGNodeData, false) != 0;
      GameUtility.SetGameObjectActive(this.mPrepareAnimON, !active);
      GameUtility.SetGameObjectActive(this.mPrepareAnimOFF, active);
      GameUtility.SetGameObjectActive(this.mBattleAnimON, !active);
      GameUtility.SetGameObjectActive(this.mBattleAnimOFF, active);
      GameUtility.SetGameObjectActive(this.mDefenseAnimON, !active);
      GameUtility.SetGameObjectActive(this.mDefenseAnimOFF, active);
    }

    public void OnSelectNode()
    {
      if (GvGManager.Instance.GvGStatusPhase == GvGManager.GvGStatus.Finished)
        return;
      int nodeId = -1;
      GvGNodeData dataOfClass = DataSource.FindDataOfClass<GvGNodeData>(((Component) this).get_gameObject(), (GvGNodeData) null);
      if (dataOfClass != null)
        nodeId = dataOfClass.NodeId;
      if (nodeId < 0)
        return;
      GvGManager.Instance.OpenNodeInfo(nodeId);
    }
  }
}
