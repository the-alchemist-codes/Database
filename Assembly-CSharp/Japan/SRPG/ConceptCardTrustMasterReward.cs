﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ConceptCardTrustMasterReward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class ConceptCardTrustMasterReward : MonoBehaviour
  {
    [SerializeField]
    private Text mItemName;
    [SerializeField]
    private Text mItemAmount;
    [SerializeField]
    private GameObject mItemlist;
    [SerializeField]
    private ItemIcon mItemIcon;
    [SerializeField]
    private ArtifactIcon mArtifactIcon;
    [SerializeField]
    private ConceptCardIcon mCardIcon;
    [SerializeField]
    private Sprite CoinFrame;
    [SerializeField]
    private Sprite GoldFrame;

    public ConceptCardTrustMasterReward()
    {
      base.\u002Ector();
    }

    public void SetData(ConceptCardData data)
    {
      ConceptCardTrustRewardItemParam reward = data.GetReward();
      if (reward == null)
        return;
      switch (reward.reward_type)
      {
        case eRewardType.Item:
          this.SetItem(reward);
          break;
        case eRewardType.Artifact:
          this.SetArtifact(reward);
          break;
        case eRewardType.ConceptCard:
          this.SetConceptCard(reward);
          break;
      }
      this.mItemAmount.set_text(reward.reward_num.ToString());
      if (data.TrustBonusCount < 2)
        return;
      for (int index = 0; index < data.TrustBonusCount - 1; ++index)
      {
        GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) this.mItemlist, this.mItemlist.get_transform().get_parent());
        gameObject.get_gameObject().SetActive(true);
        switch (reward.reward_type)
        {
          case eRewardType.Item:
            ItemIcon componentInChildren1 = (ItemIcon) gameObject.GetComponentInChildren<ItemIcon>();
            if (Object.op_Inequality((Object) componentInChildren1, (Object) null))
            {
              componentInChildren1.UpdateValue();
              break;
            }
            break;
          case eRewardType.Artifact:
            ArtifactIcon componentInChildren2 = (ArtifactIcon) gameObject.GetComponentInChildren<ArtifactIcon>();
            if (Object.op_Inequality((Object) componentInChildren2, (Object) null))
            {
              componentInChildren2.UpdateValue();
              break;
            }
            break;
        }
      }
    }

    public void SetItem(ConceptCardTrustRewardItemParam reward_item)
    {
      if (string.IsNullOrEmpty(reward_item.iname))
        return;
      ItemParam itemParam = MonoSingleton<GameManager>.Instance.GetItemParam(reward_item.iname);
      this.mItemName.set_text(itemParam.name);
      DataSource.Bind<ItemParam>(((Component) this).get_gameObject(), itemParam, false);
      if (!Object.op_Inequality((Object) this.mItemIcon, (Object) null))
        return;
      ((Component) this.mItemIcon).get_gameObject().SetActive(true);
      this.mItemIcon.UpdateValue();
    }

    public void SetArtifact(ConceptCardTrustRewardItemParam reward_item)
    {
      if (string.IsNullOrEmpty(reward_item.iname))
        return;
      ArtifactParam artifactParam = MonoSingleton<GameManager>.Instance.MasterParam.GetArtifactParam(reward_item.iname);
      this.mItemName.set_text(artifactParam.name);
      DataSource.Bind<ArtifactParam>(((Component) this).get_gameObject(), artifactParam, false);
      if (!Object.op_Inequality((Object) this.mArtifactIcon, (Object) null))
        return;
      ((Component) this.mArtifactIcon).get_gameObject().SetActive(true);
      this.mArtifactIcon.UpdateValue();
    }

    public void SetConceptCard(ConceptCardTrustRewardItemParam reward_item)
    {
      if (string.IsNullOrEmpty(reward_item.iname))
        return;
      ConceptCardParam conceptCardParam = MonoSingleton<GameManager>.Instance.MasterParam.GetConceptCardParam(reward_item.iname);
      this.mItemName.set_text(conceptCardParam.name);
      DataSource.Bind<ConceptCardParam>(((Component) this).get_gameObject(), conceptCardParam, false);
      if (!Object.op_Inequality((Object) this.mCardIcon, (Object) null))
        return;
      ((Component) this.mCardIcon).get_gameObject().SetActive(true);
      this.mCardIcon.Setup(ConceptCardData.CreateConceptCardDataForDisplay(conceptCardParam.iname));
    }
  }
}
