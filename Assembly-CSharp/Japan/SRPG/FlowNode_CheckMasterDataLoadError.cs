﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_CheckMasterDataLoadError
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;

namespace SRPG
{
  [FlowNode.NodeType("System/Master/CheckMasterDataLoadError", 32741)]
  [FlowNode.Pin(0, "Check", FlowNode.PinTypes.Input, 0)]
  [FlowNode.Pin(1, "Success", FlowNode.PinTypes.Output, 1)]
  [FlowNode.Pin(2, "Failed", FlowNode.PinTypes.Output, 2)]
  public class FlowNode_CheckMasterDataLoadError : FlowNode
  {
    public override void OnActivate(int pinID)
    {
      if (pinID != 0)
        return;
      if (string.IsNullOrEmpty(MonoSingleton<GameManager>.Instance.MasterDataLoadErrorMessage))
        this.Success();
      else
        EmbedSystemMessage.Create(MonoSingleton<GameManager>.Instance.MasterDataLoadErrorMessage, (EmbedSystemMessage.SystemMessageEvent) (go =>
        {
          MonoSingleton<GameManager>.Instance.MasterDataLoadErrorMessage = string.Empty;
          this.Failure();
        }), true);
    }

    private void Success()
    {
      ((Behaviour) this).set_enabled(false);
      this.ActivateOutputLinks(1);
    }

    private void Failure()
    {
      ((Behaviour) this).set_enabled(false);
      this.ActivateOutputLinks(2);
    }
  }
}
