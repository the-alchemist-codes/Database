﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GvGRewardRankingDetailParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class GvGRewardRankingDetailParam
  {
    public int Ranking { get; private set; }

    public string RewardId { get; private set; }

    public bool Deserialize(JSON_GvGRewardRankingDetailParam json)
    {
      if (json == null)
        return false;
      this.Ranking = json.ranking;
      this.RewardId = json.reward_id;
      return true;
    }
  }
}
