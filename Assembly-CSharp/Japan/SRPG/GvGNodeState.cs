﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GvGNodeState
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public enum GvGNodeState
  {
    Npc = 0,
    OccupySelf = 10, // 0x0000000A
    DeclareSelf = 11, // 0x0000000B
    DeclaredEnemy = 12, // 0x0000000C
    OccupyOther = 20, // 0x00000014
    DeclareOther = 21, // 0x00000015
  }
}
