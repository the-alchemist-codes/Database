﻿// Decompiled with JetBrains decompiler
// Type: SRPG.UnitProfileWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System.Text;
using UnityEngine;

namespace SRPG
{
  public class UnitProfileWindow : MonoBehaviour
  {
    public string DebugUnitID;
    private MySound.Voice mUnitVoice;
    [FlexibleArray]
    public UnityEngine.UI.Text[] ProfileTexts;
    [SerializeField]
    public UnityEngine.UI.Text Illustrator;

    public UnitProfileWindow()
    {
      base.\u002Ector();
    }

    private void Start()
    {
      UnitData data = !string.IsNullOrEmpty(this.DebugUnitID) ? MonoSingleton<GameManager>.Instance.Player.FindUnitDataByUnitID(this.DebugUnitID) : MonoSingleton<GameManager>.Instance.Player.FindUnitDataByUniqueID((long) GlobalVars.SelectedUnitUniqueID);
      string skinVoiceSheetName = data.GetUnitSkinVoiceSheetName(-1);
      string sheetName = "VO_" + skinVoiceSheetName;
      string cueNamePrefix = data.GetUnitSkinVoiceCueName(-1) + "_";
      this.mUnitVoice = new MySound.Voice(sheetName, skinVoiceSheetName, cueNamePrefix, false);
      this.PlayProfileVoice();
      DataSource.Bind<UnitData>(((Component) this).get_gameObject(), data, false);
      GameParameter.UpdateAll(((Component) this).get_gameObject());
      if (data != null)
      {
        for (int index = 0; index < this.ProfileTexts.Length; ++index)
        {
          if (!Object.op_Equality((Object) this.ProfileTexts[index], (Object) null) && !string.IsNullOrEmpty(this.ProfileTexts[index].get_text()))
          {
            StringBuilder stringBuilder = GameUtility.GetStringBuilder();
            stringBuilder.Append("unit.");
            stringBuilder.Append(data.UnitParam.iname);
            stringBuilder.Append("_");
            stringBuilder.Append(this.ProfileTexts[index].get_text());
            this.ProfileTexts[index].set_text(LocalizedText.Get(stringBuilder.ToString()));
          }
        }
      }
      if (!Object.op_Inequality((Object) this.Illustrator, (Object) null))
        return;
      ArtifactParam selectedSkin = data.GetSelectedSkin(-1);
      string str = selectedSkin == null ? (string) null : selectedSkin.Illustrator;
      if (!string.IsNullOrEmpty(str))
        this.Illustrator.set_text(str);
      else
        this.Illustrator.set_text(LocalizedText.Get("unit." + data.UnitParam.iname + "_ILLUST"));
    }

    private void PlayProfileVoice()
    {
      if (this.mUnitVoice == null)
        return;
      this.mUnitVoice.Play("chara_0001", 0.0f, false);
    }

    private void OnDestroy()
    {
      if (this.mUnitVoice != null)
      {
        this.mUnitVoice.StopAll(0.0f);
        this.mUnitVoice.Cleanup();
      }
      this.mUnitVoice = (MySound.Voice) null;
    }
  }
}
