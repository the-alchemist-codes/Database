﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_VersusTowerParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_VersusTowerParam
  {
    public string vstower_id;
    public string iname;
    public int floor;
    public int rankup_num;
    public int win_num;
    public int lose_num;
    public int bonus_num;
    public int downfloor;
    public int resetfloor;
    public string[] winitem;
    public int[] win_itemnum;
    public string[] joinitem;
    public int[] join_itemnum;
    public string arrival_item;
    public string arrival_type;
    public int arrival_num;
    public string[] spbtl_item;
    public int[] spbtl_itemnum;
    public string[] season_item;
    public string[] season_itype;
    public int[] season_itemnum;
  }
}
