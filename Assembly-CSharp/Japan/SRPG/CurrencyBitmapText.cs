﻿// Decompiled with JetBrains decompiler
// Type: SRPG.CurrencyBitmapText
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class CurrencyBitmapText : BitmapText, ISerializationCallbackReceiver
  {
    private readonly UIVertex[] m_TempVerts = new UIVertex[4];
    private const string mDelimiter = ",";
    private const int mGroupingCount = 3;
    private string mModifiedText;

    public static string CreateFormatedText(string str)
    {
      string str1 = str;
      if (!string.IsNullOrEmpty(",") && !string.IsNullOrEmpty(str))
      {
        List<object> objectList = new List<object>();
        int num = 3;
        for (int index = str.Length - 1; index >= 0; --index)
        {
          char ch = str[index];
          if (num > 0)
          {
            objectList.Add((object) ch);
          }
          else
          {
            objectList.Add((object) ",");
            objectList.Add((object) ch);
            num = 3;
          }
          --num;
        }
        objectList.Reverse();
        StringBuilder stringBuilder = new StringBuilder();
        foreach (object obj in objectList)
          stringBuilder.Append(obj);
        str1 = stringBuilder.ToString();
      }
      return str1;
    }

    public void OnBeforeSerialize()
    {
    }

    public void OnAfterDeserialize()
    {
      this.mModifiedText = CurrencyBitmapText.CreateFormatedText((string) this.m_Text);
    }

    public override string text
    {
      get
      {
        return base.text;
      }
      set
      {
        this.mModifiedText = CurrencyBitmapText.CreateFormatedText(value);
        base.text = value;
      }
    }

    public string modifiedText
    {
      get
      {
        return this.mModifiedText;
      }
      set
      {
        this.mModifiedText = value;
        base.text = value;
      }
    }

    protected virtual void OnPopulateMesh(VertexHelper toFill)
    {
      if (Object.op_Equality((Object) this.get_font(), (Object) null))
        return;
      this.m_DisableFontTextureRebuiltCallback = (__Null) 1;
      Rect rect = ((Graphic) this).get_rectTransform().get_rect();
      this.get_cachedTextGenerator().PopulateWithErrors(this.mModifiedText, this.GetGenerationSettings(((Rect) ref rect).get_size()), ((Component) this).get_gameObject());
      IList<UIVertex> verts = this.get_cachedTextGenerator().get_verts();
      float num1 = 1f / this.get_pixelsPerUnit();
      int num2 = ((ICollection<UIVertex>) verts).Count - 4;
      if (num2 <= 0)
      {
        toFill.Clear();
      }
      else
      {
        // ISSUE: cast to a reference type
        // ISSUE: explicit reference operation
        // ISSUE: cast to a reference type
        // ISSUE: explicit reference operation
        Vector2 vector2_1 = Vector2.op_Multiply(new Vector2((float) (^(Vector3&) ref verts[0].position).x, (float) (^(Vector3&) ref verts[0].position).y), num1);
        Vector2 vector2_2 = Vector2.op_Subtraction(((Graphic) this).PixelAdjustPoint(vector2_1), vector2_1);
        toFill.Clear();
        if (Vector2.op_Inequality(vector2_2, Vector2.get_zero()))
        {
          for (int index1 = 0; index1 < num2; ++index1)
          {
            int index2 = index1 & 3;
            this.m_TempVerts[index2] = verts[index1];
            ref UIVertex local1 = ref this.m_TempVerts[index2];
            local1.position = (__Null) Vector3.op_Multiply((Vector3) local1.position, num1);
            ref __Null local2 = ref this.m_TempVerts[index2].position;
            // ISSUE: cast to a reference type
            // ISSUE: explicit reference operation
            // ISSUE: cast to a reference type
            // ISSUE: explicit reference operation
            (^(Vector3&) ref local2).x = (^(Vector3&) ref local2).x + vector2_2.x;
            ref __Null local3 = ref this.m_TempVerts[index2].position;
            // ISSUE: cast to a reference type
            // ISSUE: explicit reference operation
            // ISSUE: cast to a reference type
            // ISSUE: explicit reference operation
            (^(Vector3&) ref local3).y = (^(Vector3&) ref local3).y + vector2_2.y;
            if (index2 == 3)
              toFill.AddUIVertexQuad(this.m_TempVerts);
          }
        }
        else
        {
          for (int index1 = 0; index1 < num2; ++index1)
          {
            int index2 = index1 & 3;
            this.m_TempVerts[index2] = verts[index1];
            ref UIVertex local = ref this.m_TempVerts[index2];
            local.position = (__Null) Vector3.op_Multiply((Vector3) local.position, num1);
            if (index2 == 3)
              toFill.AddUIVertexQuad(this.m_TempVerts);
          }
        }
        this.m_DisableFontTextureRebuiltCallback = (__Null) 0;
      }
    }

    public virtual float preferredWidth
    {
      get
      {
        return this.get_cachedTextGeneratorForLayout().GetPreferredWidth(this.mModifiedText, this.GetGenerationSettings(Vector2.get_zero())) / this.get_pixelsPerUnit();
      }
    }

    public virtual float preferredHeight
    {
      get
      {
        Rect pixelAdjustedRect = ((Graphic) this).GetPixelAdjustedRect();
        return this.get_cachedTextGeneratorForLayout().GetPreferredHeight(this.mModifiedText, this.GetGenerationSettings(new Vector2((float) ((Rect) ref pixelAdjustedRect).get_size().x, 0.0f))) / this.get_pixelsPerUnit();
      }
    }
  }
}
