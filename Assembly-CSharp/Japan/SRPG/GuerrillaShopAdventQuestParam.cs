﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuerrillaShopAdventQuestParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class GuerrillaShopAdventQuestParam
  {
    public int id;
    public string qid;

    public bool Deserialize(JSON_GuerrillaShopAdventQuestParam json)
    {
      this.id = json.id;
      this.qid = json.qid;
      return true;
    }
  }
}
