﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_ReqGuildEdit
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System;
using UnityEngine;

namespace SRPG
{
  [FlowNode.NodeType("Guild/ReqGuildEdit", 32741)]
  [FlowNode.Pin(1, "ギルド編集", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(101, "ギルド編集完了", FlowNode.PinTypes.Output, 101)]
  public class FlowNode_ReqGuildEdit : FlowNode_Network
  {
    private const int PIN_INPUT_START_GUILD_EDIT = 1;
    private const int PIN_OUTPUT_END_GUILD_EDIT = 101;

    public override void OnActivate(int pinID)
    {
      GuildCommand instance = GuildCommand.Instance;
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) instance, (UnityEngine.Object) null))
        return;
      if (pinID == 1)
        this.ExecRequest((WebAPI) new ReqGuildEdit(instance.TempGuild, new Network.ResponseCallback(((FlowNode_Network) this).ResponseCallback)));
      ((Behaviour) this).set_enabled(true);
    }

    public override void OnSuccess(WWWResult www)
    {
      if (Network.IsError)
      {
        switch (Network.ErrCode)
        {
          case Network.EErrCode.Guild_InputNgWord:
            this.OnBack();
            break;
          case Network.EErrCode.Guild_PayerCoinShort:
            this.RollbackGuildCommand();
            this.OnBack();
            break;
          default:
            this.OnRetry();
            break;
        }
      }
      else
      {
        WebAPI.JSON_BodyResponse<FlowNode_ReqGuildEdit.Json_ResGuildEdit> jsonObject = JSONParser.parseJSONObject<WebAPI.JSON_BodyResponse<FlowNode_ReqGuildEdit.Json_ResGuildEdit>>(www.text);
        DebugUtility.Assert(jsonObject != null, "res == null");
        Network.RemoveAPI();
        try
        {
          MonoSingleton<GameManager>.Instance.Player.Deserialize(jsonObject.body.guild);
          MonoSingleton<GameManager>.Instance.Player.Deserialize(jsonObject.body.currencies);
        }
        catch (Exception ex)
        {
          DebugUtility.LogException(ex);
          return;
        }
        this.ActivateOutputLinks(101);
        ((Behaviour) this).set_enabled(false);
      }
    }

    private void RollbackGuildCommand()
    {
      if (UnityEngine.Object.op_Equality((UnityEngine.Object) GuildCommand.Instance, (UnityEngine.Object) null))
        return;
      GuildCommand.Instance.Back();
      GuildCommand.Instance.ResetTempGuild();
    }

    public class Json_ResGuildEdit
    {
      public JSON_Guild guild;
      public Json_Currencies currencies;
    }
  }
}
