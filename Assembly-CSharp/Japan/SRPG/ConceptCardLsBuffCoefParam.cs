﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ConceptCardLsBuffCoefParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using System.Collections.Generic;

namespace SRPG
{
  public class ConceptCardLsBuffCoefParam
  {
    public const int COEF_DEFAULT = 10000;
    private int mRare;
    private int[] mCoefs;
    private int[] mFriendCoefs;

    public int Rare
    {
      get
      {
        return this.mRare;
      }
    }

    public int[] Coefs
    {
      get
      {
        return this.mCoefs;
      }
    }

    public int[] FriendCoefs
    {
      get
      {
        return this.mFriendCoefs;
      }
    }

    public void Deserialize(JSON_ConceptCardLsBuffCoefParam json)
    {
      if (json == null)
        return;
      this.mRare = json.rare;
      this.mCoefs = (int[]) null;
      if (json.coefs != null && json.coefs.Length != 0)
      {
        this.mCoefs = new int[json.coefs.Length];
        for (int index = 0; index < json.coefs.Length; ++index)
          this.mCoefs[index] = json.coefs[index];
      }
      this.mFriendCoefs = (int[]) null;
      if (json.friend_coefs == null || json.friend_coefs.Length == 0)
        return;
      this.mFriendCoefs = new int[json.friend_coefs.Length];
      for (int index = 0; index < json.friend_coefs.Length; ++index)
        this.mFriendCoefs[index] = json.friend_coefs[index];
    }

    public static void Deserialize(
      ref List<ConceptCardLsBuffCoefParam> list,
      JSON_ConceptCardLsBuffCoefParam[] json)
    {
      if (json == null || json.Length == 0)
        return;
      if (list == null)
        list = new List<ConceptCardLsBuffCoefParam>(json.Length);
      list.Clear();
      for (int index = 0; index < json.Length; ++index)
      {
        ConceptCardLsBuffCoefParam cardLsBuffCoefParam = new ConceptCardLsBuffCoefParam();
        cardLsBuffCoefParam.Deserialize(json[index]);
        list.Add(cardLsBuffCoefParam);
      }
    }

    public static int GetCoef(List<ConceptCardLsBuffCoefParam> list, int rare, int bt_limit)
    {
      int num = 10000;
      if (list != null && bt_limit >= 0)
      {
        ConceptCardLsBuffCoefParam cardLsBuffCoefParam = list.Find((Predicate<ConceptCardLsBuffCoefParam>) (p => p.Rare == rare));
        if (cardLsBuffCoefParam != null && cardLsBuffCoefParam.Coefs != null && bt_limit < cardLsBuffCoefParam.Coefs.Length)
          num = cardLsBuffCoefParam.Coefs[bt_limit];
      }
      return num;
    }

    public static int GetFriendCoef(List<ConceptCardLsBuffCoefParam> list, int rare, int bt_limit)
    {
      int num = 10000;
      if (list != null && bt_limit >= 0)
      {
        ConceptCardLsBuffCoefParam cardLsBuffCoefParam = list.Find((Predicate<ConceptCardLsBuffCoefParam>) (p => p.Rare == rare));
        if (cardLsBuffCoefParam != null && cardLsBuffCoefParam.FriendCoefs != null && bt_limit < cardLsBuffCoefParam.FriendCoefs.Length)
          num = cardLsBuffCoefParam.FriendCoefs[bt_limit];
      }
      return num;
    }
  }
}
