﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RankMatchClassListItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class RankMatchClassListItem : ListItemEvents
  {
    public GameObject RewardUnit;
    public GameObject RewardItem;
    public GameObject RewardCard;
    public GameObject RewardArtifact;
    public GameObject RewardAward;
    public GameObject RewardGold;
    public GameObject RewardCoin;
    public Transform RewardList;
  }
}
