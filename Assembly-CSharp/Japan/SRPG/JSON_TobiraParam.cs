﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_TobiraParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_TobiraParam
  {
    public string unit_iname;
    public int enable;
    public int category;
    public string recipe_id;
    public string skill_iname;
    public JSON_TobiraLearnAbilityParam[] learn_abils;
    public string overwrite_ls_iname;
    public int priority;
  }
}
