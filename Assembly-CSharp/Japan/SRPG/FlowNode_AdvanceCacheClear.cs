﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FlowNode_AdvanceCacheClear
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  [FlowNode.NodeType("Advance/CacheClear", 32741)]
  [FlowNode.Pin(1, "Input", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(101, "Output", FlowNode.PinTypes.Output, 101)]
  public class FlowNode_AdvanceCacheClear : FlowNode
  {
    public const int PIN_IN = 1;
    public const int PIN_OUT = 101;
    [SerializeField]
    private bool GlobalVarsClear;

    public override void OnActivate(int pinID)
    {
      if (pinID != 1)
        return;
      AdvanceManager.CurrentChapterParam = (ChapterParam) null;
      AdvanceManager.CurrentEventParam = (AdvanceEventParam) null;
      if (this.GlobalVarsClear)
      {
        GlobalVars.SelectedChapter.Reset();
        GlobalVars.SelectedQuestID = string.Empty;
      }
      this.ActivateOutputLinks(101);
    }
  }
}
