﻿// Decompiled with JetBrains decompiler
// Type: SRPG.StateMachine`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  public class StateMachine<T>
  {
    private T mOwner;
    private SRPG.State<T> mState;

    public StateMachine(T owner)
    {
      this.mOwner = owner;
    }

    public SRPG.State<T> State
    {
      get
      {
        return this.mState;
      }
    }

    public string StateName
    {
      get
      {
        return this.mState != null ? this.mState.GetType().Name : "NULL";
      }
    }

    public System.Type CurrentState
    {
      get
      {
        return this.mState != null ? this.mState.GetType() : (System.Type) null;
      }
    }

    public void Update()
    {
      if (this.mState == null)
        return;
      this.mState.Update(this.mOwner);
    }

    public void GotoState(System.Type stateType)
    {
      if (this.mState != null)
        this.mState.End(this.mOwner);
      this.mState = (SRPG.State<T>) Activator.CreateInstance(stateType);
      this.mState.self = this.mOwner;
      this.mState.Begin(this.mOwner);
    }

    public void GotoState<StateType>() where StateType : SRPG.State<T>, new()
    {
      if (this.mState != null)
        this.mState.End(this.mOwner);
      this.mState = (SRPG.State<T>) new StateType();
      this.mState.self = this.mOwner;
      this.mState.Begin(this.mOwner);
    }

    public bool IsInState<StateType>() where StateType : SRPG.State<T>
    {
      return this.mState != null && this.mState.GetType() == typeof (StateType);
    }

    public bool IsInKindOfState<StateType>() where StateType : SRPG.State<T>
    {
      return this.mState != null && this.mState is StateType;
    }

    public void Command(string cmd)
    {
      if (this.mState == null)
        return;
      this.mState.Command(this.mOwner, cmd);
    }
  }
}
