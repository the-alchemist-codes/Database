﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GvGPartyContent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class GvGPartyContent : MonoBehaviour
  {
    [SerializeField]
    private bool IsSelf;
    [Space(10f)]
    [SerializeField]
    private string SVB_HpbGaugeName;
    [Space(10f)]
    [SerializeField]
    private GenericSlot UnitSlotTemplate;
    [SerializeField]
    private GenericSlot ConceptCardSlotTemplate;
    [Space(10f)]
    [SerializeField]
    private Text TeamText;
    [SerializeField]
    private ImageArray RoleImageArray;
    [SerializeField]
    private Text PlayerNameText;
    [SerializeField]
    private Slider StaminaSlider;
    [SerializeField]
    private ImageArray StaminaImageArray;
    [Space(10f)]
    [SerializeField]
    private GameObject mHpGauge;
    [SerializeField]
    private Button UnitSlotEditorButton;
    [SerializeField]
    private Button ConceptCardSlotEditorButton;
    [Space(10f)]
    [SerializeField]
    private Text BeatNumText;
    private List<GenericSlot> UnitList;
    private List<GenericSlot> CardList;

    public GvGPartyContent()
    {
      base.\u002Ector();
    }

    public GvGParty Party { get; private set; }

    private void Awake()
    {
      GameUtility.SetGameObjectActive((Component) this.UnitSlotTemplate, false);
      GameUtility.SetGameObjectActive((Component) this.ConceptCardSlotTemplate, false);
      GameUtility.SetGameObjectActive(this.mHpGauge, false);
    }

    public bool Setup(GvGNodeData node, int number, GvGParty party)
    {
      if (party == null || node == null)
        return false;
      this.Party = party;
      this.UnitList.ForEach((Action<GenericSlot>) (u => UnityEngine.Object.Destroy((UnityEngine.Object) ((Component) u).get_gameObject())));
      this.CardList.ForEach((Action<GenericSlot>) (c => UnityEngine.Object.Destroy((UnityEngine.Object) ((Component) c).get_gameObject())));
      this.UnitList.Clear();
      this.CardList.Clear();
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.TeamText, (UnityEngine.Object) null))
        this.TeamText.set_text(string.Format(LocalizedText.Get("sys.GVG_PARTY_TEAM_TITLE"), (object) number));
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.RoleImageArray, (UnityEngine.Object) null))
        this.RoleImageArray.ImageIndex = Mathf.Max(0, (int) (party.RoleId - 1));
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.PlayerNameText, (UnityEngine.Object) null))
        this.PlayerNameText.set_text(party.PlayerName);
      float num1 = (float) ((double) (node.NodeParam.ConsecutiveDebuffMax - this.Party.WinNum) / (double) node.NodeParam.ConsecutiveDebuffMax * 100.0);
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.StaminaSlider, (UnityEngine.Object) null))
        this.StaminaSlider.set_value(num1);
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.StaminaImageArray, (UnityEngine.Object) null))
        this.StaminaImageArray.ImageIndex = (double) num1 < 100.0 ? ((double) num1 < 50.0 ? ((double) num1 < 30.0 ? ((double) num1 < 10.0 ? 4 : 3) : 2) : 1) : 0;
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.UnitSlotEditorButton, (UnityEngine.Object) null))
        ((Selectable) this.UnitSlotEditorButton).set_interactable(party.WinNum == 0);
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.ConceptCardSlotEditorButton, (UnityEngine.Object) null))
        ((Selectable) this.ConceptCardSlotEditorButton).set_interactable(party.WinNum == 0);
      if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.BeatNumText, (UnityEngine.Object) null))
        this.BeatNumText.set_text(string.Format(LocalizedText.Get("sys.GVG_TEXT_BEATNUM", (object) party.BeatNum)));
      for (int index = 0; index < 3; ++index)
      {
        PartySlotData data = new PartySlotData(PartySlotType.Free, (string) null, (PartySlotIndex) index, false);
        data.Index = (PartySlotIndex) index;
        if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.UnitSlotTemplate, (UnityEngine.Object) null))
        {
          GenericSlot genericSlot = (GenericSlot) UnityEngine.Object.Instantiate<GenericSlot>((M0) this.UnitSlotTemplate, ((Component) this.UnitSlotTemplate).get_transform().get_parent());
          genericSlot.SetSlotData<PartySlotData>(data);
          ((Component) genericSlot).get_gameObject().SetActive(true);
          this.UnitList.Add(genericSlot);
        }
        if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.ConceptCardSlotTemplate, (UnityEngine.Object) null))
        {
          GenericSlot genericSlot = (GenericSlot) UnityEngine.Object.Instantiate<GenericSlot>((M0) this.ConceptCardSlotTemplate, ((Component) this.ConceptCardSlotTemplate).get_transform().get_parent());
          genericSlot.SetSlotData<PartySlotData>(data);
          ((Component) genericSlot).get_gameObject().SetActive(true);
          this.CardList.Add(genericSlot);
        }
      }
      UnitData leader = (UnitData) null;
      for (int index = 0; index < this.Party.Units.Count; ++index)
      {
        GvGPartyUnit unit1 = this.Party.Units[index];
        if (this.IsSelf && party.WinNum == 0)
          unit1.TempFlags |= UnitData.TemporaryFlags.TemporaryUnitData | UnitData.TemporaryFlags.AllowJobChange;
        if (index == 0)
          leader = (UnitData) unit1;
        if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.UnitSlotTemplate, (UnityEngine.Object) null))
        {
          GenericSlot unit2 = this.UnitList[index];
          unit2.SetSlotData<UnitData>((UnitData) unit1);
          this.UnitList.Add(unit2);
          if (this.IsSelf && party.WinNum == 0)
            unit2.SetSlotData<PlayerPartyTypes>(PlayerPartyTypes.GvG);
          SerializeValueBehaviour component1 = (SerializeValueBehaviour) ((Component) unit2).GetComponent<SerializeValueBehaviour>();
          if (UnityEngine.Object.op_Inequality((UnityEngine.Object) component1, (UnityEngine.Object) null))
          {
            GameObject gameObject = component1.list.GetGameObject(this.SVB_HpbGaugeName);
            if (UnityEngine.Object.op_Inequality((UnityEngine.Object) gameObject, (UnityEngine.Object) null))
            {
              Slider component2 = (Slider) gameObject.GetComponent<Slider>();
              if (UnityEngine.Object.op_Inequality((UnityEngine.Object) component2, (UnityEngine.Object) null))
              {
                float num2 = 0.0f;
                if (unit1 != null)
                {
                  gameObject.SetActive(true);
                  num2 = unit1.HP != -1 ? (float) unit1.HP / (float) (int) unit1.Status.param.hp : 1f;
                }
                component2.set_value(num2 * 100f);
              }
            }
          }
        }
        if (UnityEngine.Object.op_Inequality((UnityEngine.Object) this.ConceptCardSlotTemplate, (UnityEngine.Object) null))
        {
          GenericSlot card = this.CardList[index];
          ConceptCardSlot component = (ConceptCardSlot) ((Component) card).GetComponent<ConceptCardSlot>();
          if (UnityEngine.Object.op_Inequality((UnityEngine.Object) component, (UnityEngine.Object) null))
            component.SetLeaderUnit(leader);
          DataSource.Bind<UnitData>(((Component) card).get_gameObject(), (UnitData) unit1, false);
          card.SetSlotData<ConceptCardData>(unit1?.MainConceptCard);
          ((ConceptCardIcon) ((Component) card).GetComponent<ConceptCardIcon>()).Setup(unit1?.MainConceptCard);
        }
      }
      return true;
    }
  }
}
