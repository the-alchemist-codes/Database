﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GallerySortWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  [FlowNode.Pin(0, "ソート順の変更", FlowNode.PinTypes.Input, 0)]
  [FlowNode.Pin(1, "Save Setting", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(2, "ソート種別の変更", FlowNode.PinTypes.Input, 2)]
  [FlowNode.Pin(100, "Close", FlowNode.PinTypes.Output, 100)]
  public class GallerySortWindow : MonoBehaviour, IFlowInterface
  {
    private const int SORT_ORDER_CHANGE = 0;
    private const int SAVE_SETTING = 1;
    private const int SORT_TYPE_CHANGE = 2;
    private const int OUTPUT_CLOSE = 100;
    [SerializeField]
    private Toggle RarityButton;
    [SerializeField]
    private Toggle NameButton;
    [SerializeField]
    private Toggle AscButton;
    [SerializeField]
    private Toggle DescButton;
    private GallerySettings mSettings;
    private bool mIsRarityAscending;
    private bool mIsNameAscending;
    private GallerySettings.SortType mCurrentSortType;

    public GallerySortWindow()
    {
      base.\u002Ector();
    }

    public void Activated(int pinID)
    {
      switch (pinID)
      {
        case 0:
          if (!(FlowNode_ButtonEvent.currentValue is SerializeValueList currentValue))
            break;
          Toggle uiToggle = currentValue.GetUIToggle("toggle");
          if (Object.op_Equality((Object) uiToggle, (Object) this.AscButton))
          {
            if (this.mCurrentSortType == GallerySettings.SortType.Rarity)
            {
              this.mIsRarityAscending = uiToggle.get_isOn();
              break;
            }
            this.mIsNameAscending = uiToggle.get_isOn();
            break;
          }
          if (this.mCurrentSortType == GallerySettings.SortType.Rarity)
          {
            this.mIsRarityAscending = !uiToggle.get_isOn();
            break;
          }
          this.mIsNameAscending = !uiToggle.get_isOn();
          break;
        case 1:
          this.mSettings.sortType = this.mCurrentSortType;
          this.mSettings.isRarityAscending = this.mIsRarityAscending;
          this.mSettings.isNameAscending = this.mIsNameAscending;
          FlowNode_GameObject.ActivateOutputLinks((Component) this, 100);
          break;
        case 2:
          if (!(FlowNode_ButtonEvent.currentValue is SerializeValueList currentValue))
            break;
          if (Object.op_Equality((Object) currentValue.GetUIToggle("toggle"), (Object) this.RarityButton))
          {
            this.mCurrentSortType = GallerySettings.SortType.Rarity;
            this.AscButton.set_isOn(this.mIsRarityAscending);
            this.DescButton.set_isOn(!this.mIsRarityAscending);
            break;
          }
          this.mCurrentSortType = GallerySettings.SortType.Name;
          this.AscButton.set_isOn(this.mIsNameAscending);
          this.DescButton.set_isOn(!this.mIsNameAscending);
          break;
      }
    }

    private void Awake()
    {
      if (!(FlowNode_ButtonEvent.currentValue is SerializeValueList currentValue))
        return;
      FlowNode_ButtonEvent.currentValue = (object) null;
      this.mSettings = currentValue.GetObject("settings") as GallerySettings;
      if (this.mSettings == null)
        return;
      this.AscButton.set_isOn(this.mSettings.isRarityAscending);
      this.DescButton.set_isOn(!this.mSettings.isRarityAscending);
      this.mCurrentSortType = this.mSettings.sortType;
      this.mIsRarityAscending = this.mSettings.isRarityAscending;
      this.mIsNameAscending = this.mSettings.isNameAscending;
      if (this.mCurrentSortType == GallerySettings.SortType.Rarity)
      {
        this.RarityButton.set_isOn(true);
        this.NameButton.set_isOn(false);
        this.AscButton.set_isOn(this.mIsRarityAscending);
        this.DescButton.set_isOn(!this.mIsRarityAscending);
      }
      else
      {
        this.RarityButton.set_isOn(false);
        this.NameButton.set_isOn(true);
        this.AscButton.set_isOn(this.mIsNameAscending);
        this.DescButton.set_isOn(!this.mIsNameAscending);
      }
    }
  }
}
