﻿// Decompiled with JetBrains decompiler
// Type: SRPG.SameUnitWarningMessage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class SameUnitWarningMessage : MonoBehaviour
  {
    [SerializeField]
    private Button button;

    public SameUnitWarningMessage()
    {
      base.\u002Ector();
    }

    private void Start()
    {
      if (!Object.op_Inequality((Object) this.button, (Object) null))
        return;
      this.button.AddClickListener(new ButtonExt.ButtonClickEvent(this.OnClick));
    }

    private void OnClick(GameObject go)
    {
      GameManager instance = MonoSingleton<GameManager>.Instance;
      if (Object.op_Equality((Object) instance, (Object) null) || instance.MasterParam == null)
        return;
      UnitData dataOfClass = DataSource.FindDataOfClass<UnitData>(((Component) this).get_gameObject(), (UnitData) null);
      if (dataOfClass == null)
        return;
      SameUnitWarningMessage.SameUnitWarningMessageDialog(dataOfClass);
    }

    public static void SameUnitWarningMessageDialog(UnitData unit)
    {
      GameManager instance = MonoSingleton<GameManager>.Instance;
      if (Object.op_Equality((Object) instance, (Object) null) || instance.MasterParam == null || unit == null)
        return;
      UnitSameGroupParam unitSameGroup = instance.MasterParam.GetUnitSameGroup(unit.UnitID);
      if (unitSameGroup == null)
        return;
      string unitOtherNameText = unitSameGroup.GetGroupUnitOtherNameText(unit.UnitID);
      if (string.IsNullOrEmpty(unitOtherNameText))
        return;
      UIUtility.NegativeSystemMessage((string) null, string.Format(LocalizedText.Get("sys.PARTYEDITOR_SAMEUNIT_INPARTY"), (object) unitOtherNameText), (UIUtility.DialogResultEvent) (dialog => {}), (GameObject) null, false, -1);
    }
  }
}
