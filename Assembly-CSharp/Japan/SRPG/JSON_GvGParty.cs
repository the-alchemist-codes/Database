﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_GvGParty
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_GvGParty
  {
    public int id;
    public int win_num;
    public int beat_num;
    public int is_npc;
    public int role;
    public string name;
    public JSON_GvGPartyUnit[] units;
    public JSON_GvGPartyNPC[] npc_units;
  }
}
