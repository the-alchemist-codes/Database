﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqSellConceptCard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Text;

namespace SRPG
{
  public class ReqSellConceptCard : WebAPI
  {
    public ReqSellConceptCard(long[] sell_ids, Network.ResponseCallback response)
    {
      this.name = "unit/concept/sell";
      StringBuilder stringBuilder = WebAPI.GetStringBuilder();
      stringBuilder.Append("\"sell_ids\":[");
      for (int index = 0; index < sell_ids.Length; ++index)
      {
        stringBuilder.Append(sell_ids[index]);
        if (index != sell_ids.Length - 1)
          stringBuilder.Append(",");
      }
      stringBuilder.Append("]");
      this.body = WebAPI.GetRequestString(stringBuilder.ToString());
      this.callback = response;
    }
  }
}
