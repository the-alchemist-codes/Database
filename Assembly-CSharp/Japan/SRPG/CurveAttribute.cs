﻿// Decompiled with JetBrains decompiler
// Type: SRPG.CurveAttribute
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class CurveAttribute : PropertyAttribute
  {
    public float PosX;
    public float PosY;
    public float RangeX;
    public float RangeY;
    public bool b;
    public int x;

    public CurveAttribute(float PosX, float PosY, float RangeX, float RangeY, bool b)
    {
      this.\u002Ector();
      this.PosX = PosX;
      this.PosY = PosY;
      this.RangeX = RangeX;
      this.RangeY = RangeY;
      this.b = b;
    }
  }
}
