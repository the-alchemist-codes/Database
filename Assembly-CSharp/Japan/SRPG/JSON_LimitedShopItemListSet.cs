﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_LimitedShopItemListSet
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class JSON_LimitedShopItemListSet
  {
    public int id;
    public int sold;
    public Json_ShopItemDesc item;
    public JSON_LimitedShopItemListSet.LimitedCost cost;
    public Json_ShopItemDesc[] children;
    public int isreset;
    public long start;
    public long end;

    public class LimitedCost : Json_ShopItemCost
    {
      public string iname;
    }
  }
}
