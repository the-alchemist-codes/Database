﻿// Decompiled with JetBrains decompiler
// Type: SRPG.EnhanceEquipIconNode
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class EnhanceEquipIconNode : ContentNode
  {
    [SerializeField]
    public GameObject Icon;
    [SerializeField]
    public GameObject EmptyObject;

    public void Setup(EnhanceMaterial material)
    {
      if (Object.op_Equality((Object) this.Icon, (Object) null) || material == null)
        return;
      DataSource.Bind<EnhanceMaterial>(this.Icon, material, false);
      this.Icon.get_gameObject().SetActive(true);
      GameParameter.UpdateAll(this.Icon.get_gameObject());
    }

    public void Empty(bool is_enmpty)
    {
      if (Object.op_Equality((Object) this.EmptyObject, (Object) null) || Object.op_Equality((Object) this.Icon, (Object) null))
        return;
      this.Icon.get_gameObject().SetActive(!is_enmpty);
      this.EmptyObject.SetActive(is_enmpty);
    }
  }
}
