﻿// Decompiled with JetBrains decompiler
// Type: SRPG.BuyCoinProductParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class BuyCoinProductParam
  {
    private string mIname;
    private string mProductId;
    private string mShopId;
    private BuyCoinManager.PremiumRestrictionType mType;
    private int mVal;
    private bool mIsPlatformCommon;
    private string mReward;
    private string mTitle;
    private string mDescription;
    private int mBadge;

    public string Iname
    {
      get
      {
        return this.mIname;
      }
    }

    public string ProductId
    {
      get
      {
        return this.mProductId;
      }
    }

    public string ShopId
    {
      get
      {
        return this.mShopId;
      }
    }

    public BuyCoinManager.PremiumRestrictionType Type
    {
      get
      {
        return this.mType;
      }
    }

    public int Val
    {
      get
      {
        return this.mVal;
      }
    }

    public bool FlagPlatformCommon
    {
      get
      {
        return this.mIsPlatformCommon;
      }
    }

    public string Reward
    {
      get
      {
        return this.mReward;
      }
    }

    public string Title
    {
      get
      {
        return this.mTitle;
      }
    }

    public string Description
    {
      get
      {
        return this.mDescription;
      }
    }

    public int Badge
    {
      get
      {
        return this.mBadge;
      }
    }

    public bool Deserialize(JSON_BuyCoinProductParam json)
    {
      if (json == null)
        return false;
      this.mIname = json.iname;
      this.mProductId = json.product_id;
      this.mShopId = json.shop_id;
      this.mType = (BuyCoinManager.PremiumRestrictionType) json.type;
      this.mVal = json.val;
      this.mIsPlatformCommon = json.is_platform_common != 0;
      this.mReward = json.reward;
      this.mTitle = json.title;
      if (!string.IsNullOrEmpty(this.mTitle))
        this.mTitle = this.mTitle.Replace("<br>", "\n");
      this.mDescription = json.description;
      if (!string.IsNullOrEmpty(this.mDescription))
        this.mDescription = this.mDescription.Replace("<br>", "\n");
      this.mBadge = json.badge;
      return true;
    }
  }
}
