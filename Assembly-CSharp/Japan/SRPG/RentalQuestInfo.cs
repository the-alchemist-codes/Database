﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RentalQuestInfo
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;

namespace SRPG
{
  public class RentalQuestInfo
  {
    public string QuestId;
    public OInt Point;
    private QuestParam mQuestParam;

    public RentalQuestInfo(string quest_id, int point)
    {
      this.QuestId = quest_id;
      this.Point = (OInt) point;
      this.mQuestParam = (QuestParam) null;
    }

    public QuestParam Quest
    {
      get
      {
        if (this.mQuestParam == null && !string.IsNullOrEmpty(this.QuestId) && Object.op_Implicit((Object) MonoSingleton<GameManager>.Instance))
          this.mQuestParam = MonoSingleton<GameManager>.Instance.FindQuest(this.QuestId);
        return this.mQuestParam;
      }
    }
  }
}
