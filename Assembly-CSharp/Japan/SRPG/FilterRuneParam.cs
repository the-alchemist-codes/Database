﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FilterRuneParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class FilterRuneParam
  {
    public string iname;
    public string tab_name;
    public string name;
    public eRuneFilterTypes filter_type;
    public FilterRuneConditionParam[] conditions;

    public bool IsEnableFilterType(eRuneFilterTypes type)
    {
      return this.filter_type == type;
    }

    public void Deserialize(JSON_FilterRuneParam json)
    {
      this.iname = json.iname;
      this.tab_name = json.tab_name;
      this.name = json.name;
      this.filter_type = (eRuneFilterTypes) json.filter_type;
      if (json.cnds == null)
        return;
      this.conditions = new FilterRuneConditionParam[json.cnds.Length];
      for (int index = 0; index < json.cnds.Length; ++index)
      {
        FilterRuneConditionParam runeConditionParam = new FilterRuneConditionParam(this);
        runeConditionParam.Deserialize(json.cnds[index]);
        this.conditions[index] = runeConditionParam;
      }
    }

    public static void Deserialize(ref FilterRuneParam[] param, JSON_FilterRuneParam[] json)
    {
      if (json == null)
        return;
      param = new FilterRuneParam[json.Length];
      for (int index = 0; index < json.Length; ++index)
      {
        FilterRuneParam filterRuneParam = new FilterRuneParam();
        filterRuneParam.Deserialize(json[index]);
        param[index] = filterRuneParam;
      }
    }
  }
}
