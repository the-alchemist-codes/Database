﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildRaidChallengingPlayer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System;
using UnityEngine;

namespace SRPG
{
  public class GuildRaidChallengingPlayer
  {
    public string Name { get; private set; }

    public UnitParam Unit { get; private set; }

    public int Lv { get; private set; }

    public AwardParam SelectedAward { get; private set; }

    public GuildMemberData.eRole Role { get; private set; }

    public DateTime ChallengeTime { get; private set; }

    public bool Deserialize(JSON_GuildRaidChallengingPlayer json)
    {
      if (json == null)
        return false;
      this.Name = json.name;
      try
      {
        this.Unit = MonoSingleton<GameManager>.Instance.GetUnitParam(json.unit);
      }
      catch (Exception ex)
      {
        Debug.Log((object) ex);
        return false;
      }
      this.Lv = json.lv;
      if (!string.IsNullOrEmpty(json.selected_award))
        this.SelectedAward = MonoSingleton<GameManager>.Instance.GetAwardParam(json.selected_award);
      this.Role = (GuildMemberData.eRole) json.role_id;
      this.ChallengeTime = TimeManager.FromUnixTime((long) json.challenge_time);
      return true;
    }
  }
}
