﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ConceptCardTrustMaster
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class ConceptCardTrustMaster : MonoBehaviour
  {
    [SerializeField]
    private RawImage mCardImage;
    [SerializeField]
    private RawImage mCardImageAdd;

    public ConceptCardTrustMaster()
    {
      base.\u002Ector();
    }

    public void SetData(ConceptCardData data)
    {
      string path = AssetPath.ConceptCard(data.Param);
      if (Object.op_Inequality((Object) this.mCardImage, (Object) null))
        MonoSingleton<GameManager>.Instance.ApplyTextureAsync(this.mCardImage, path);
      if (!Object.op_Inequality((Object) this.mCardImageAdd, (Object) null))
        return;
      MonoSingleton<GameManager>.Instance.ApplyTextureAsync(this.mCardImageAdd, path);
    }
  }
}
