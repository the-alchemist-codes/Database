﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqUnitRentalExec
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using Gsc.Network.Encoding;
using MessagePack;
using System;

namespace SRPG
{
  public class ReqUnitRentalExec : WebAPI
  {
    public ReqUnitRentalExec(
      string rental_iname,
      string unit_iname,
      SRPG.Network.ResponseCallback response,
      EncodingTypes.ESerializeCompressMethod serializeCompressMethod)
    {
      this.name = "unit/rental/exec";
      this.body = WebAPI.GetRequestString<ReqUnitRentalExec.RequestParam>(new ReqUnitRentalExec.RequestParam()
      {
        rental_iname = rental_iname,
        unit_iname = unit_iname
      });
      this.callback = response;
      this.serializeCompressMethod = serializeCompressMethod;
    }

    [Serializable]
    public class RequestParam
    {
      public string rental_iname;
      public string unit_iname;
    }

    [MessagePackObject(true)]
    [Serializable]
    public class Response
    {
      public Json_Unit[] units;
    }
  }
}
