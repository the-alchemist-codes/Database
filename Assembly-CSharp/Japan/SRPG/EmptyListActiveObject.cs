﻿// Decompiled with JetBrains decompiler
// Type: SRPG.EmptyListActiveObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  [FlowNode.Pin(0, "Refresh", FlowNode.PinTypes.Input, 0)]
  [FlowNode.Pin(10, "Refreshed", FlowNode.PinTypes.Output, 10)]
  public class EmptyListActiveObject : MonoBehaviour, IFlowInterface, IGameParameter
  {
    private const int PIN_IN_REFRESH = 0;
    private const int PIN_OUT_EXIT = 10;
    [HeaderBar("中身が入る親オブジェクト(指定しなければ自分自身)")]
    [SerializeField]
    private Transform TargetListParent;
    [HeaderBar("リストが空のときに表示したいゲームオブジェクト")]
    [SerializeField]
    private GameObject EmptyTextObject;
    [HeaderBar("常に監視するか？")]
    [SerializeField]
    private bool AlwaysCheck;
    private bool mNeedCheck;

    public EmptyListActiveObject()
    {
      base.\u002Ector();
    }

    private void Awake()
    {
      if (Object.op_Equality((Object) this.TargetListParent, (Object) null))
        this.TargetListParent = ((Component) this).get_gameObject().get_transform();
      if (Object.op_Inequality((Object) this.EmptyTextObject, (Object) null))
        this.EmptyTextObject.SetActive(false);
      else
        DebugUtility.LogWarning("EmptyListActiveObject:EmptyTextObjectがnullはおかしい");
      this.mNeedCheck = false;
    }

    public void UpdateValue()
    {
      this.mNeedCheck = true;
    }

    public void Activated(int pinID)
    {
      if (pinID != 0)
        return;
      this.mNeedCheck = true;
      FlowNode_GameObject.ActivateOutputLinks((Component) this, 10);
    }

    private void LateUpdate()
    {
      if (!this.AlwaysCheck && !this.mNeedCheck)
        return;
      if (Object.op_Inequality((Object) this.TargetListParent, (Object) null))
      {
        bool active = true;
        if (this.TargetListParent.get_childCount() > 0)
        {
          for (int index = 0; index < this.TargetListParent.get_childCount(); ++index)
          {
            Transform child = this.TargetListParent.GetChild(index);
            if (Object.op_Inequality((Object) child, (Object) null) && ((Component) child).get_gameObject().get_activeSelf() && Vector3.op_Inequality(((Component) child).get_transform().get_localScale(), Vector3.get_zero()))
              active = false;
          }
        }
        GameUtility.SetGameObjectActive(this.EmptyTextObject, active);
      }
      this.mNeedCheck = false;
    }
  }
}
