﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ConditionsResult_QuestClear
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class ConditionsResult_QuestClear : ConditionsResult
  {
    private QuestParam mQuestParam;

    public ConditionsResult_QuestClear(QuestParam questParam)
    {
      this.mQuestParam = questParam;
      this.mIsClear = questParam.state == QuestStates.Cleared;
      this.mTargetValue = 2;
      this.mCurrentValue = (int) questParam.state;
    }

    public override string text
    {
      get
      {
        return LocalizedText.Get("sys.TOBIRA_CONDITIONS_QUEST_CLEAR", (object) this.mQuestParam.name);
      }
    }

    public override string errorText
    {
      get
      {
        return string.Format("クエスト「{0}」をクリアしていません", (object) this.mQuestParam.name);
      }
    }
  }
}
