﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GvGRankingData
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace SRPG
{
  public class GvGRankingData
  {
    private List<GvGRankingGuildData> mGuilds = new List<GvGRankingGuildData>();
    private List<GvGBeatRankingData> mBeats = new List<GvGBeatRankingData>();
    private GvGRankingGuildData mSelf;

    public GvGRankingGuildData Self
    {
      get
      {
        return this.mSelf;
      }
    }

    public List<GvGRankingGuildData> Guilds
    {
      get
      {
        return this.mGuilds;
      }
    }

    public List<GvGBeatRankingData> Beats
    {
      get
      {
        return this.mBeats;
      }
    }

    public void Deserialize(JSON_GvGRankingData[] guilds)
    {
      if (guilds == null)
        return;
      this.mGuilds.Clear();
      for (int index = 0; index < guilds.Length; ++index)
      {
        GvGRankingGuildData grankingGuildData = new GvGRankingGuildData();
        grankingGuildData.Deserialize(guilds[index]);
        this.mGuilds.Add(grankingGuildData);
      }
    }

    public void Deserialize(JSON_GvGBeatRanking[] beats)
    {
      if (beats == null)
        return;
      this.mBeats.Clear();
      for (int index = 0; index < beats.Length; ++index)
      {
        GvGBeatRankingData gbeatRankingData = new GvGBeatRankingData();
        gbeatRankingData.Deserialize(beats[index]);
        this.mBeats.Add(gbeatRankingData);
      }
    }
  }
}
