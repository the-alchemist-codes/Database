﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AlchemyShopManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace SRPG
{
  [FlowNode.Pin(1, "初期化", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(101, "初期化完了", FlowNode.PinTypes.Output, 101)]
  [FlowNode.Pin(102, "ショップリストがない", FlowNode.PinTypes.Output, 102)]
  [FlowNode.Pin(111, "カテゴリが選択された", FlowNode.PinTypes.Output, 111)]
  public class AlchemyShopManager : MonoBehaviour, IFlowInterface
  {
    [SerializeField]
    private GameObject WindowCategory;
    [SerializeField]
    private GameObject WindowShop;
    [Space(5f)]
    [SerializeField]
    private GameObject GoCategoryParent;
    [SerializeField]
    private AlchemyShopItemCategory TemplateCategory;
    [Space(5f)]
    [SerializeField]
    private ScrollRect SrControllerShop;
    private const int PIN_IN_INIT = 1;
    private const int PIN_OUT_INIT_FINISHED = 101;
    private const int PIN_OUT_NO_SHOP_LIST = 102;
    private const int PIN_OUT_CATEGORY_SELECTED = 111;
    private static AlchemyShopManager mInstance;
    private JSON_ShopListArray.Shops[] mShopArray;
    private LimitedShopListItem mShopItem;

    public AlchemyShopManager()
    {
      base.\u002Ector();
    }

    public static AlchemyShopManager Instance
    {
      get
      {
        return AlchemyShopManager.mInstance;
      }
    }

    private void Awake()
    {
      if (!Object.op_Implicit((Object) AlchemyShopManager.mInstance))
        AlchemyShopManager.mInstance = this;
      if (Object.op_Implicit((Object) this.WindowCategory))
        this.WindowCategory.SetActive(false);
      if (!Object.op_Implicit((Object) this.WindowShop))
        return;
      this.WindowShop.SetActive(false);
    }

    private void OnDestroy()
    {
      if (!Object.op_Equality((Object) AlchemyShopManager.mInstance, (Object) this))
        return;
      this.mShopArray = (JSON_ShopListArray.Shops[]) null;
      this.InternalClearGlobalLimitedShopItemInfo();
      AlchemyShopManager.mInstance = (AlchemyShopManager) null;
    }

    private void InternalClearGlobalLimitedShopItemInfo()
    {
      GlobalVars.LimitedShopItem = (LimitedShopListItem) null;
      GameManager instance = MonoSingleton<GameManager>.Instance;
      if (Object.op_Implicit((Object) instance))
        instance.Player.SetLimitedShopData(new LimitedShopData());
      this.mShopItem = (LimitedShopListItem) null;
    }

    private void Init()
    {
      if (Object.op_Implicit((Object) this.GoCategoryParent) && Object.op_Implicit((Object) this.TemplateCategory))
      {
        GameUtility.SetGameObjectActive(((Component) this.TemplateCategory).get_gameObject(), false);
        GameUtility.DestroyChildGameObjects(this.GoCategoryParent, new List<GameObject>((IEnumerable<GameObject>) new GameObject[1]
        {
          ((Component) this.TemplateCategory).get_gameObject()
        }));
        if (this.mShopArray != null && this.mShopArray.Length != 0)
        {
          for (int index = 0; index < this.mShopArray.Length; ++index)
          {
            // ISSUE: object of a compiler-generated type is created
            // ISSUE: variable of a compiler-generated type
            AlchemyShopManager.\u003CInit\u003Ec__AnonStorey0 initCAnonStorey0 = new AlchemyShopManager.\u003CInit\u003Ec__AnonStorey0();
            // ISSUE: reference to a compiler-generated field
            initCAnonStorey0.\u0024this = this;
            JSON_ShopListArray.Shops mShop = this.mShopArray[index];
            // ISSUE: reference to a compiler-generated field
            initCAnonStorey0.item_category = (AlchemyShopItemCategory) Object.Instantiate<AlchemyShopItemCategory>((M0) this.TemplateCategory, this.GoCategoryParent.get_transform(), false);
            // ISSUE: reference to a compiler-generated field
            if (Object.op_Implicit((Object) initCAnonStorey0.item_category))
            {
              // ISSUE: reference to a compiler-generated field
              // ISSUE: method pointer
              // ISSUE: method pointer
              initCAnonStorey0.item_category.SetItem(index, mShop, new UnityAction((object) initCAnonStorey0, __methodptr(\u003C\u003Em__0)), new UnityAction((object) initCAnonStorey0, __methodptr(\u003C\u003Em__1)));
              // ISSUE: reference to a compiler-generated field
              ((Component) initCAnonStorey0.item_category).get_gameObject().SetActive(true);
            }
          }
          this.SetGlobalVarsShopData(this.mShopArray[0]);
          if (Object.op_Implicit((Object) this.WindowCategory))
            this.WindowCategory.SetActive(true);
        }
      }
      if (!Object.op_Implicit((Object) this.WindowShop))
        return;
      this.WindowShop.SetActive(true);
    }

    private bool SetGlobalVarsShopData(JSON_ShopListArray.Shops shop)
    {
      if (shop == null)
        return false;
      LimitedShopListItem limitedShopListItem = ((Component) this).RequireComponent<LimitedShopListItem>();
      if (!Object.op_Implicit((Object) limitedShopListItem) || limitedShopListItem.shops == shop)
        return false;
      if (Object.op_Implicit((Object) this.GoCategoryParent))
      {
        AlchemyShopItemCategory[] componentsInChildren = (AlchemyShopItemCategory[]) this.GoCategoryParent.GetComponentsInChildren<AlchemyShopItemCategory>(true);
        if (componentsInChildren != null)
        {
          for (int index = 0; index < componentsInChildren.Length; ++index)
          {
            AlchemyShopItemCategory shopItemCategory = componentsInChildren[index];
            if (!Object.op_Equality((Object) shopItemCategory, (Object) null))
              shopItemCategory.SetCurrent(shopItemCategory.Shop == shop);
          }
        }
      }
      limitedShopListItem.SetShopList(shop);
      this.mShopItem = limitedShopListItem;
      GlobalVars.LimitedShopItem = this.mShopItem;
      return true;
    }

    private bool IsExistShopList()
    {
      return this.mShopArray != null && this.mShopArray.Length != 0;
    }

    private bool IsExistLimitedShopData()
    {
      GameManager instance = MonoSingleton<GameManager>.Instance;
      return Object.op_Implicit((Object) instance) && instance.Player.GetLimitedShopData().items.Count != 0;
    }

    private void OnTapCategoryItem(AlchemyShopItemCategory item)
    {
      if (!Object.op_Implicit((Object) item) || !this.SetGlobalVarsShopData(item.Shop))
        return;
      if (Object.op_Implicit((Object) this.SrControllerShop))
      {
        this.SrControllerShop.set_velocity(Vector2.get_zero());
        this.SrControllerShop.set_normalizedPosition(Vector2.get_up());
      }
      FlowNode_GameObject.ActivateOutputLinks((Component) this, 111);
    }

    private void OnTapCategoryLockItem(AlchemyShopItemCategory item)
    {
      if (!Object.op_Implicit((Object) item))
        return;
      GameManager instance = MonoSingleton<GameManager>.Instance;
      if (!Object.op_Implicit((Object) instance))
        return;
      GuildFacilityParam fromFacilityType = instance.MasterParam.GetGuildFacilityFromFacilityType(GuildFacilityParam.eFacilityType.GUILD_SHOP);
      if (fromFacilityType == null)
        return;
      int requiredUnlockShop = fromFacilityType.GetGuildShopLevelRequiredUnlockShop(item.Index + 1);
      if (requiredUnlockShop == 0)
        return;
      UIUtility.SystemMessage(string.Format(LocalizedText.Get("sys.GUILDFACILITY_SHOP_UNLOCK_NOTICE"), (object) requiredUnlockShop), (UIUtility.DialogResultEvent) null, (GameObject) null, false, -1);
    }

    public void Activated(int pinID)
    {
      if (pinID != 1)
        return;
      this.Init();
      if (this.IsExistShopList())
      {
        if (!this.IsExistLimitedShopData())
          return;
        FlowNode_GameObject.ActivateOutputLinks((Component) this, 101);
      }
      else
        FlowNode_GameObject.ActivateOutputLinks((Component) this, 102);
    }

    public static bool EntryShopList(JSON_ShopListArray.Shops[] shops)
    {
      if (!Object.op_Implicit((Object) AlchemyShopManager.mInstance) || shops == null)
        return false;
      AlchemyShopManager.mInstance.mShopArray = shops;
      return true;
    }

    public static void ClearGlobalLimitedShopItemInfo()
    {
      if (!Object.op_Implicit((Object) AlchemyShopManager.mInstance))
        return;
      AlchemyShopManager.mInstance.InternalClearGlobalLimitedShopItemInfo();
    }

    public static bool SetCurrentShopData(Json_LimitedShopResponse shop_data)
    {
      if (!Object.op_Implicit((Object) AlchemyShopManager.mInstance))
        return false;
      GameManager instance = MonoSingleton<GameManager>.Instance;
      if (!Object.op_Implicit((Object) instance) || instance.Player == null || shop_data == null)
        return false;
      LimitedShopData limitedShopData = instance.Player.GetLimitedShopData();
      if (!limitedShopData.Deserialize(shop_data))
        return false;
      instance.Player.SetLimitedShopData(limitedShopData);
      return true;
    }
  }
}
