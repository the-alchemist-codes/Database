﻿// Decompiled with JetBrains decompiler
// Type: SRPG.UnitRentalProgressLine
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class UnitRentalProgressLine : MonoBehaviour
  {
    private Scrollbar mScrollbar;

    public UnitRentalProgressLine()
    {
      base.\u002Ector();
    }

    private void SetValue(float value)
    {
      this.mScrollbar = (Scrollbar) ((Component) this).GetComponent<Scrollbar>();
      if (!Object.op_Inequality((Object) this.mScrollbar, (Object) null))
        return;
      this.mScrollbar.set_value(value);
    }

    public void UpdateValue()
    {
      RentalQuestInfo dataOfClass1 = DataSource.FindDataOfClass<RentalQuestInfo>(((Component) this).get_gameObject(), (RentalQuestInfo) null);
      UnitRentalParam dataOfClass2 = DataSource.FindDataOfClass<UnitRentalParam>(((Component) this).get_gameObject(), (UnitRentalParam) null);
      if (dataOfClass1 == null || dataOfClass2 == null)
        return;
      this.SetValue((float) (int) dataOfClass1.Point / (float) (int) dataOfClass2.PtMax);
    }
  }
}
