﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GenericBadge`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  internal class GenericBadge<T> where T : class
  {
    public bool mValue;
    public T mRawData;

    public GenericBadge(T data, bool value = false)
    {
      this.mRawData = data;
      this.mValue = value;
    }
  }
}
