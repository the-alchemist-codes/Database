﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RaidRescueRequestOption
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace SRPG
{
  [FlowNode.Pin(1, "Set", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(101, "Finish", FlowNode.PinTypes.Output, 2)]
  public class RaidRescueRequestOption : MonoBehaviour, IFlowInterface
  {
    [SerializeField]
    private Toggle mGuild;
    [SerializeField]
    private Toggle mFriend;
    [SerializeField]
    private Toggle mAll;

    public RaidRescueRequestOption()
    {
      base.\u002Ector();
    }

    private void Awake()
    {
      GameUtility.SetToggle(this.mGuild, false);
      GameUtility.SetToggle(this.mFriend, false);
      GameUtility.SetToggle(this.mAll, true);
      // ISSUE: method pointer
      ((UnityEvent<bool>) this.mGuild.onValueChanged).AddListener(new UnityAction<bool>((object) this, __methodptr(\u003CAwake\u003Em__0)));
      // ISSUE: method pointer
      ((UnityEvent<bool>) this.mFriend.onValueChanged).AddListener(new UnityAction<bool>((object) this, __methodptr(\u003CAwake\u003Em__1)));
      // ISSUE: method pointer
      ((UnityEvent<bool>) this.mAll.onValueChanged).AddListener(new UnityAction<bool>((object) this, __methodptr(\u003CAwake\u003Em__2)));
    }

    public void Activated(int pinID)
    {
      if (pinID != 1)
        return;
      RaidManager.Instance.RescueReqOptionGuild = this.mGuild.get_isOn();
      RaidManager.Instance.RescueReqOptionFriend = this.mFriend.get_isOn();
      FlowNode_GameObject.ActivateOutputLinks((Component) this, 101);
    }
  }
}
