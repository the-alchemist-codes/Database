﻿// Decompiled with JetBrains decompiler
// Type: SRPG.EmbedSystemMessageEx
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace SRPG
{
  public class EmbedSystemMessageEx : MonoBehaviour
  {
    public const string PrefabPath = "e/UI/EmbedSystemMessageEx";
    public Text Message;
    public GameObject ButtonTemplate;
    public GameObject CancelButtonTemplate;
    public GameObject ButtonBase;
    public string CloseEventName;

    public EmbedSystemMessageEx()
    {
      base.\u002Ector();
    }

    public static EmbedSystemMessageEx Create(string msg)
    {
      EmbedSystemMessageEx embedSystemMessageEx = (EmbedSystemMessageEx) Object.Instantiate<EmbedSystemMessageEx>(Resources.Load<EmbedSystemMessageEx>("e/UI/EmbedSystemMessageEx"));
      embedSystemMessageEx.Body = msg;
      return embedSystemMessageEx;
    }

    public void AddButton(
      string btn_text,
      bool is_close,
      EmbedSystemMessageEx.SystemMessageEvent callback,
      bool enableBackKey = false)
    {
      this.CreateButton(this.ButtonTemplate, this.ButtonBase, btn_text, is_close, enableBackKey, callback);
    }

    public void AddCancelButton(
      string btn_text,
      bool is_close,
      EmbedSystemMessageEx.SystemMessageEvent callback,
      bool enableBackKey = false)
    {
      this.CreateButton(this.CancelButtonTemplate, this.ButtonBase, btn_text, is_close, enableBackKey, callback);
    }

    private void CreateButton(
      GameObject template,
      GameObject parentObject,
      string btn_text,
      bool is_close,
      bool enableBackKey,
      EmbedSystemMessageEx.SystemMessageEvent callback)
    {
      // ISSUE: object of a compiler-generated type is created
      // ISSUE: variable of a compiler-generated type
      EmbedSystemMessageEx.\u003CCreateButton\u003Ec__AnonStorey0 buttonCAnonStorey0 = new EmbedSystemMessageEx.\u003CCreateButton\u003Ec__AnonStorey0();
      // ISSUE: reference to a compiler-generated field
      buttonCAnonStorey0.callback = callback;
      // ISSUE: reference to a compiler-generated field
      buttonCAnonStorey0.is_close = is_close;
      // ISSUE: reference to a compiler-generated field
      buttonCAnonStorey0.\u0024this = this;
      if (Object.op_Equality((Object) template, (Object) null) || Object.op_Equality((Object) parentObject, (Object) null))
        return;
      GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) template);
      gameObject.SetActive(true);
      LText componentInChildren1 = (LText) gameObject.GetComponentInChildren<LText>();
      if (Object.op_Inequality((Object) componentInChildren1, (Object) null))
        componentInChildren1.set_text(btn_text);
      Button componentInChildren2 = (Button) gameObject.GetComponentInChildren<Button>();
      if (Object.op_Inequality((Object) componentInChildren2, (Object) null))
      {
        // ISSUE: method pointer
        ((UnityEvent) componentInChildren2.get_onClick()).AddListener(new UnityAction((object) buttonCAnonStorey0, __methodptr(\u003C\u003Em__0)));
      }
      if (enableBackKey && Object.op_Equality((Object) gameObject.GetComponentInChildren<BackHandler>(), (Object) null) && Object.op_Inequality((Object) componentInChildren2, (Object) null))
        ((Component) componentInChildren2).get_gameObject().AddComponent<BackHandler>();
      gameObject.get_transform().SetParent(parentObject.get_transform(), false);
    }

    private void TriggerCloseEvent()
    {
      if (string.IsNullOrEmpty(this.CloseEventName))
        return;
      FlowNode_TriggerLocalEvent.TriggerLocalEvent((Component) this, this.CloseEventName);
    }

    private void Awake()
    {
      if (Object.op_Inequality((Object) this.ButtonTemplate, (Object) null))
        this.ButtonTemplate.SetActive(false);
      if (!Object.op_Inequality((Object) this.CancelButtonTemplate, (Object) null))
        return;
      this.CancelButtonTemplate.SetActive(false);
    }

    public string Body
    {
      set
      {
        this.Message.set_text(value);
      }
      get
      {
        return this.Message.get_text();
      }
    }

    public delegate void SystemMessageEvent(bool yes);
  }
}
