﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GenesisStarRewardWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class GenesisStarRewardWindow : MonoBehaviour
  {
    [SerializeField]
    private Text mBodyText;
    [SerializeField]
    private Transform mRewardIconParent;
    [SerializeField]
    private GenesisRewardIcon mRewardIconTemplate;
    [SerializeField]
    private Button mReceiveButton;
    [SerializeField]
    private Text mButtonText;

    public GenesisStarRewardWindow()
    {
      base.\u002Ector();
    }

    private void Start()
    {
      SerializeValueBehaviour component = (SerializeValueBehaviour) ((Component) this).GetComponent<SerializeValueBehaviour>();
      if (Object.op_Equality((Object) component, (Object) null))
        return;
      GenesisStarRewardParam genesisStarRewardParam = component.list.GetObject<GenesisStarRewardParam>("GENESIS_STAR_REWARD", (GenesisStarRewardParam) null);
      if (genesisStarRewardParam == null || Object.op_Equality((Object) this.mBodyText, (Object) null))
        return;
      if (!genesisStarRewardParam.IsReward)
        this.mBodyText.set_text(string.Format(LocalizedText.Get("sys.GENESIS_QUEST_STARREWARD_TEXT"), (object) genesisStarRewardParam.NeedStarNum));
      else
        this.mBodyText.set_text(string.Format(LocalizedText.Get("sys.GENESIS_QUEST_STARREWARD_RECEIVED")));
      if (Object.op_Equality((Object) this.mRewardIconTemplate, (Object) null))
        return;
      GenesisRewardParam genesisRewardParam = MonoSingleton<GameManager>.Instance.GetGenesisRewardParam(genesisStarRewardParam.RewardId);
      if (genesisRewardParam == null || genesisRewardParam.RewardList == null)
        return;
      for (int index = 0; index < genesisRewardParam.RewardList.Count; ++index)
      {
        GenesisRewardIcon genesisRewardIcon = (GenesisRewardIcon) Object.Instantiate<GenesisRewardIcon>((M0) this.mRewardIconTemplate, this.mRewardIconParent);
        genesisRewardIcon.Initialize(genesisRewardParam.RewardList[index]);
        ((Component) genesisRewardIcon).get_gameObject().SetActive(true);
      }
      if (Object.op_Equality((Object) this.mReceiveButton, (Object) null))
        return;
      int num = component.list.GetInt("GENESIS_STAR_POINT", 0);
      ((Selectable) this.mReceiveButton).set_interactable(genesisStarRewardParam.NeedStarNum <= num && !genesisStarRewardParam.IsReward);
      if (!Object.op_Inequality((Object) this.mButtonText, (Object) null))
        return;
      if (!genesisStarRewardParam.IsReward)
      {
        if (genesisStarRewardParam.NeedStarNum <= num)
          this.mButtonText.set_text(string.Format(LocalizedText.Get("sys.GENESIS_QUEST_STARREWARD_BTN_OK")));
        else
          this.mButtonText.set_text(string.Format(LocalizedText.Get("sys.GENESIS_QUEST_STARREWARD_BTN_UNACHIEVED")));
      }
      else
        this.mButtonText.set_text(string.Format(LocalizedText.Get("sys.GENESIS_QUEST_STARREWARD_BTN_RECEIVED")));
    }
  }
}
