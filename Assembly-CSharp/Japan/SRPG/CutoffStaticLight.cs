﻿// Decompiled with JetBrains decompiler
// Type: SRPG.CutoffStaticLight
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class CutoffStaticLight : AnimEvent
  {
    public override void OnStart(GameObject go)
    {
      GeneratedCharacter component = (GeneratedCharacter) go.GetComponent<GeneratedCharacter>();
      if (Object.op_Inequality((Object) component, (Object) null))
      {
        component.EnableStaticLight(false);
        CameraHook.SetDisableFog(true);
      }
      else
      {
        TacticsUnitController componentInParent = (TacticsUnitController) go.GetComponentInParent<TacticsUnitController>();
        if (!Object.op_Inequality((Object) componentInParent, (Object) null))
          return;
        componentInParent.EnableStaticLight(false);
        CameraHook.SetDisableFog(true);
      }
    }

    public override void OnEnd(GameObject go)
    {
      GeneratedCharacter component = (GeneratedCharacter) go.GetComponent<GeneratedCharacter>();
      if (Object.op_Inequality((Object) component, (Object) null))
      {
        component.EnableStaticLight(true);
        CameraHook.SetDisableFog(false);
      }
      else
      {
        TacticsUnitController componentInParent = (TacticsUnitController) go.GetComponentInParent<TacticsUnitController>();
        if (!Object.op_Inequality((Object) componentInParent, (Object) null))
          return;
        componentInParent.EnableStaticLight(true);
        CameraHook.SetDisableFog(false);
      }
    }
  }
}
