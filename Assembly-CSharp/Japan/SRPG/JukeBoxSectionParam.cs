﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JukeBoxSectionParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System.Collections.Generic;

namespace SRPG
{
  [MessagePackObject(true)]
  public class JukeBoxSectionParam
  {
    private string iname;
    private string title;

    public string Title
    {
      get
      {
        return this.title;
      }
    }

    public string Iname
    {
      get
      {
        return this.iname;
      }
    }

    public bool Deserialize(JSON_JukeBoxSectionParam json)
    {
      this.iname = json.iname;
      this.title = json.title;
      return true;
    }

    public static void Deserialize(
      ref List<JukeBoxSectionParam> ref_params,
      JSON_JukeBoxSectionParam[] json)
    {
      if (ref_params == null)
        ref_params = new List<JukeBoxSectionParam>();
      ref_params.Clear();
      if (json == null)
        return;
      foreach (JSON_JukeBoxSectionParam json1 in json)
      {
        JukeBoxSectionParam jukeBoxSectionParam = new JukeBoxSectionParam();
        jukeBoxSectionParam.Deserialize(json1);
        ref_params.Add(jukeBoxSectionParam);
      }
    }
  }
}
