﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RaidReward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class RaidReward
  {
    private RaidRewardType mType;
    private string mIName;
    private int mNum;

    public RaidRewardType Type
    {
      get
      {
        return this.mType;
      }
    }

    public string IName
    {
      get
      {
        return this.mIName;
      }
    }

    public int Num
    {
      get
      {
        return this.mNum;
      }
    }

    public bool Deserialize(JSON_RaidReward json)
    {
      this.mType = (RaidRewardType) json.item_type;
      this.mIName = json.item_iname;
      this.mNum = json.item_num;
      return true;
    }
  }
}
