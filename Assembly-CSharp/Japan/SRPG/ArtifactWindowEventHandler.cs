﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ArtifactWindowEventHandler
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class ArtifactWindowEventHandler : MonoBehaviour, IGameParameter
  {
    public ArtifactList mArtifactList;
    public ArtifactScrollList mArtifactScrollList;
    public Button mBackButton;
    public Button mForwardButton;

    public ArtifactWindowEventHandler()
    {
      base.\u002Ector();
    }

    public void OnBackButton(Button button)
    {
      ArtifactData artifactData = this.GetArtifactData();
      if (artifactData == null || Object.op_Equality((Object) this.mArtifactList, (Object) null) && Object.op_Equality((Object) this.mArtifactScrollList, (Object) null))
        return;
      if (Object.op_Inequality((Object) this.mArtifactScrollList, (Object) null))
        this.mArtifactScrollList.SelectBack(artifactData);
      else
        this.mArtifactList.SelectBack(artifactData);
    }

    public void OnForwardButton(Button button)
    {
      ArtifactData artifactData = this.GetArtifactData();
      if (artifactData == null || Object.op_Equality((Object) this.mArtifactList, (Object) null) && Object.op_Equality((Object) this.mArtifactScrollList, (Object) null))
        return;
      if (Object.op_Inequality((Object) this.mArtifactScrollList, (Object) null))
        this.mArtifactScrollList.SelectFoward(artifactData);
      else
        this.mArtifactList.SelectFoward(artifactData);
    }

    public ArtifactData GetArtifactData()
    {
      return DataSource.FindDataOfClass<ArtifactData>(((Component) this).get_gameObject(), (ArtifactData) null);
    }

    public void UpdateValue()
    {
      this.UpdateBackButtonIntaractable();
      this.UpdateForwardButtonIntaractable();
    }

    private void UpdateBackButtonIntaractable()
    {
      ArtifactData artifactData = this.GetArtifactData();
      if (artifactData == null)
        return;
      if (Object.op_Inequality((Object) this.mArtifactList, (Object) null))
      {
        if (!Object.op_Inequality((Object) this.mBackButton, (Object) null))
          return;
        ((Selectable) this.mBackButton).set_interactable(!this.mArtifactList.CheckStartOfIndex(artifactData));
      }
      else
      {
        if (!Object.op_Inequality((Object) this.mArtifactScrollList, (Object) null) || !Object.op_Inequality((Object) this.mBackButton, (Object) null))
          return;
        ((Selectable) this.mBackButton).set_interactable(!this.mArtifactScrollList.CheckStartOfIndex(artifactData));
      }
    }

    private void UpdateForwardButtonIntaractable()
    {
      ArtifactData artifactData = this.GetArtifactData();
      if (artifactData == null)
        return;
      if (Object.op_Inequality((Object) this.mArtifactList, (Object) null))
      {
        if (!Object.op_Inequality((Object) this.mForwardButton, (Object) null))
          return;
        ((Selectable) this.mForwardButton).set_interactable(!this.mArtifactList.CheckEndOfIndex(artifactData));
      }
      else
      {
        if (!Object.op_Inequality((Object) this.mArtifactScrollList, (Object) null) || !Object.op_Inequality((Object) this.mForwardButton, (Object) null))
          return;
        ((Selectable) this.mForwardButton).set_interactable(!this.mArtifactScrollList.CheckEndOfIndex(artifactData));
      }
    }
  }
}
