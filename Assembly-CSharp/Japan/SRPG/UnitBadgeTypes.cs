﻿// Decompiled with JetBrains decompiler
// Type: SRPG.UnitBadgeTypes
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  [System.Flags]
  public enum UnitBadgeTypes
  {
    EnableEquipment = 1,
    EnableAwaking = 2,
    EnableRarityUp = 4,
    EnableJobRankUp = 8,
    EnableClassChange = 16, // 0x00000010
  }
}
