﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ChallengeCountResetConfirmWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace SRPG
{
  [FlowNode.Pin(0, "close", FlowNode.PinTypes.Output, 0)]
  public class ChallengeCountResetConfirmWindow : MonoBehaviour, IFlowInterface
  {
    [SerializeField]
    private Text ResetMessageText;
    [SerializeField]
    private GameObject ResetCostItem;
    [SerializeField]
    private Text ResetCostNum;
    [SerializeField]
    private Text ResetCostConsume;
    [SerializeField]
    private Text ResetCountCurrent;
    [SerializeField]
    private Text ResetCountLimit;
    [SerializeField]
    private Button DecideButton;
    [SerializeField]
    private Button CancelButton;
    public ChallengeCountResetConfirmWindow.ChallengeCountResetEvent DecideEvent;
    public ChallengeCountResetConfirmWindow.ChallengeCountResetEvent CancelEvent;

    public ChallengeCountResetConfirmWindow()
    {
      base.\u002Ector();
    }

    private void Awake()
    {
      if (Object.op_Inequality((Object) this.DecideButton, (Object) null))
      {
        // ISSUE: method pointer
        ((UnityEvent) this.DecideButton.get_onClick()).AddListener(new UnityAction((object) this, __methodptr(OnClickDecide)));
      }
      if (!Object.op_Inequality((Object) this.CancelButton, (Object) null))
        return;
      // ISSUE: method pointer
      ((UnityEvent) this.CancelButton.get_onClick()).AddListener(new UnityAction((object) this, __methodptr(OnClickCancel)));
    }

    private void Start()
    {
    }

    private void OnClickDecide()
    {
      if (this.DecideEvent == null)
        return;
      this.DecideEvent(((Component) this).get_gameObject());
      FlowNode_GameObject.ActivateOutputLinks((Component) this, 0);
    }

    private void OnClickCancel()
    {
      if (this.CancelEvent == null)
        return;
      this.CancelEvent(((Component) this).get_gameObject());
    }

    public bool Setup(
      ItemData item,
      int use_num,
      int reset_num,
      int reset_max,
      ChallengeCountResetConfirmWindow.ChallengeCountResetEvent okEvent,
      ChallengeCountResetConfirmWindow.ChallengeCountResetEvent cancelEvent)
    {
      if (Object.op_Inequality((Object) this.ResetMessageText, (Object) null))
        this.ResetMessageText.set_text(LocalizedText.Get("sys.QUEST_CHALLENGE_RESET", (object) item.Param.name, (object) use_num));
      if (Object.op_Inequality((Object) this.ResetCostItem, (Object) null))
      {
        DataSource.Bind<ItemData>(this.ResetCostItem, item, false);
        GameParameter.UpdateAll(this.ResetCostItem);
      }
      if (Object.op_Inequality((Object) this.ResetCostNum, (Object) null))
        this.ResetCostNum.set_text(item.Num.ToString());
      if (Object.op_Inequality((Object) this.ResetCostConsume, (Object) null))
        this.ResetCostConsume.set_text((item.Num - use_num).ToString());
      if (Object.op_Inequality((Object) this.ResetCountCurrent, (Object) null))
        this.ResetCountCurrent.set_text(reset_num.ToString());
      if (Object.op_Inequality((Object) this.ResetCountLimit, (Object) null))
        this.ResetCountLimit.set_text(reset_max.ToString());
      this.DecideEvent = okEvent;
      this.CancelEvent = cancelEvent;
      return true;
    }

    public void Activated(int pinID)
    {
    }

    public delegate void ChallengeCountResetEvent(GameObject dialog);
  }
}
