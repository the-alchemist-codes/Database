﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RuneLotteryBaseState
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class RuneLotteryBaseState : RuneLotteryState
  {
    public short[] enh_param;

    public bool Deserialize(JSON_RuneLotteryBaseState json)
    {
      this.Deserialize((JSON_RuneLotteryState) json);
      this.enh_param = new short[json.enh_param.Length];
      for (int index = 0; index < json.enh_param.Length; ++index)
      {
        this.enh_param[index] = (short) 0;
        this.enh_param[index] = (short) json.enh_param[index];
      }
      return true;
    }
  }
}
