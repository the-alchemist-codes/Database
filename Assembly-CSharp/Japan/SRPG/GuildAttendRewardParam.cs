﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildAttendRewardParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace SRPG
{
  public class GuildAttendRewardParam
  {
    private string mID;
    private GuildAttendReward[] mRewards;

    public string id
    {
      get
      {
        return this.mID;
      }
    }

    public GuildAttendReward[] rewards
    {
      get
      {
        return this.mRewards;
      }
    }

    public static bool Deserialize(
      ref List<GuildAttendRewardParam> param,
      JSON_GuildAttendRewardParam[] json)
    {
      if (json == null)
        return false;
      param = new List<GuildAttendRewardParam>();
      for (int index = 0; index < json.Length; ++index)
      {
        GuildAttendRewardParam attendRewardParam = new GuildAttendRewardParam();
        attendRewardParam.Deserialize(json[index]);
        param.Add(attendRewardParam);
      }
      return true;
    }

    public void Deserialize(JSON_GuildAttendRewardParam json)
    {
      if (json == null || json.rewards == null)
        return;
      this.mID = json.id;
      this.mRewards = new GuildAttendReward[json.rewards.Length];
      for (int index = 0; index < json.rewards.Length; ++index)
      {
        GuildAttendReward guildAttendReward = new GuildAttendReward();
        guildAttendReward.Deserialize(json.rewards[index]);
        this.mRewards[index] = guildAttendReward;
      }
    }
  }
}
