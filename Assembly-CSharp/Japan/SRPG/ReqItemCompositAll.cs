﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqItemCompositAll
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class ReqItemCompositAll : WebAPI
  {
    public ReqItemCompositAll(string iname, bool is_cmn, Network.ResponseCallback response)
    {
      this.name = "item/gouseiall";
      int num = !is_cmn ? 0 : 1;
      this.body = WebAPI.GetRequestString("\"iname\":\"" + iname + "\",\"is_cmn\":" + (object) num);
      this.callback = response;
    }
  }
}
