﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqArtifact
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  public class ReqArtifact : WebAPI
  {
    public ReqArtifact(long last_artifact_iid, Network.ResponseCallback response)
    {
      this.name = "unit/job/artifact";
      this.body = WebAPI.GetRequestString<ReqArtifact.RequestParam>(new ReqArtifact.RequestParam()
      {
        last_iid = last_artifact_iid
      });
      this.callback = response;
    }

    [Serializable]
    public class RequestParam
    {
      public long last_iid;
    }

    [Serializable]
    public class Response
    {
      public Json_Artifact[] artifacts;
    }
  }
}
