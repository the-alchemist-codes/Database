﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AdvanceAssets
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class AdvanceAssets : ScriptableObject
  {
    [StringIsResourcePath(typeof (GameObject))]
    public string[] EventBG;
    [StringIsResourcePath(typeof (GameObject))]
    public string[] StageBG;
    [StringIsResourcePath(typeof (GameObject))]
    public string[] StagePreview;
    [StringIsResourcePath(typeof (GameObject))]
    public string[] BossBG;
    [StringIsResourcePath(typeof (GameObject))]
    public string[] GachaTopImage;

    public AdvanceAssets()
    {
      base.\u002Ector();
    }
  }
}
