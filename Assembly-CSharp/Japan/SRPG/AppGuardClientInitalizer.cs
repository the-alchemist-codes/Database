﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AppGuardClientInitalizer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System.Collections;
using System.Diagnostics;
using UnityEngine;

namespace SRPG
{
  public class AppGuardClientInitalizer : MonoBehaviour
  {
    public AppGuardClientInitalizer()
    {
      base.\u002Ector();
    }

    private void Awake()
    {
      DebugUtility.Log("GfW init");
      this.StartCoroutine(this.InitGameGuardWaitForWindowProc());
    }

    [DebuggerHidden]
    private IEnumerator InitGameGuardWaitForWindowProc()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new AppGuardClientInitalizer.\u003CInitGameGuardWaitForWindowProc\u003Ec__Iterator0()
      {
        \u0024this = this
      };
    }

    public void AppGuardClientInitalizer_OnDecideUniqueClientID(string uniqueClientID)
    {
      string uniqueClientID1;
      if (string.IsNullOrEmpty(uniqueClientID))
      {
        uniqueClientID1 = MonoSingleton<GameManager>.Instance.AppGuardUniqueClientID;
      }
      else
      {
        string str = uniqueClientID;
        MonoSingleton<GameManager>.Instance.AppGuardUniqueClientID = str;
        uniqueClientID1 = str;
      }
      Network.RequestAPI((WebAPI) new ReqAppGuardAuthentication(uniqueClientID1, new Network.ResponseCallback(this.AuthenticationResponseCallback)), false);
    }

    private void OnApplicationQuit()
    {
      this.CloseGameGuard();
    }

    public void CloseGameGuard()
    {
      AppGuardClient.OnApplicationQuit(false);
    }

    private void AuthenticationResponseCallback(WWWResult www)
    {
      if (FlowNode_Network.HasCommonError(www))
        return;
      if (Network.IsError)
      {
        int errCode = (int) Network.ErrCode;
        FlowNode_Network.Retry();
      }
      else
      {
        WebAPI.JSON_BodyResponse<AppGuardClientInitalizer.Json_Authentication> jsonObject = JSONParser.parseJSONObject<WebAPI.JSON_BodyResponse<AppGuardClientInitalizer.Json_Authentication>>(www.text);
        Network.RemoveAPI();
        if (jsonObject.body.status == 0)
        {
          Debug.Log((object) "Force Application Quit.");
          Application.Quit();
        }
        else
          Debug.Log((object) "Authentication Succeeded.");
      }
    }

    private class Json_Authentication
    {
      public int status;
    }
  }
}
