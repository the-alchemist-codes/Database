﻿// Decompiled with JetBrains decompiler
// Type: SRPG.EquipEnhanceConfirmWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class EquipEnhanceConfirmWindow : MonoBehaviour
  {
    private const int PIN_OUT_CLOSE = 0;
    [SerializeField]
    private GameObject mTemplateItem;
    [SerializeField]
    private GameObject mCostErrorObj;
    [SerializeField]
    private Text mCostText;
    [SerializeField]
    private Button mSelectButton;
    public EquipEnhanceConfirmWindow.OnEnhanceSelectedEvent OnSelectedEvent;
    public EquipEnhanceConfirmWindow.OnEnhanceSelectedEvent OnCanceledEvent;

    public EquipEnhanceConfirmWindow()
    {
      base.\u002Ector();
    }

    public void OnDecide()
    {
      if (this.OnSelectedEvent == null)
        return;
      this.OnSelectedEvent();
    }

    public void OnCancel()
    {
      if (this.OnCanceledEvent == null)
        return;
      this.OnCanceledEvent();
    }

    public void SetupItem(ItemData list, int num)
    {
      if (list == null)
        return;
      GameObject gameObject = (GameObject) Object.Instantiate<GameObject>((M0) this.mTemplateItem, this.mTemplateItem.get_transform().get_parent());
      if (Object.op_Equality((Object) gameObject, (Object) null))
        return;
      EquipEnhanceConfirmList componentInChildren = (EquipEnhanceConfirmList) gameObject.GetComponentInChildren<EquipEnhanceConfirmList>();
      if (Object.op_Inequality((Object) componentInChildren.mEnhanceNum, (Object) null))
        componentInChildren.mEnhanceNum.set_text(num.ToString());
      DataSource.Bind<ItemParam>(gameObject, list.Param, false);
      gameObject.SetActive(true);
    }

    public void SetupCost(int cost)
    {
      if (Object.op_Inequality((Object) this.mCostText, (Object) null))
        this.mCostText.set_text(cost.ToString());
      if (Object.op_Equality((Object) this.mCostErrorObj, (Object) null) || Object.op_Equality((Object) this.mSelectButton, (Object) null))
        return;
      if (cost > MonoSingleton<GameManager>.Instance.Player.Gold)
      {
        this.mCostErrorObj.SetActive(true);
        ((Selectable) this.mSelectButton).set_interactable(false);
      }
      else
      {
        this.mCostErrorObj.SetActive(false);
        ((Selectable) this.mSelectButton).set_interactable(true);
      }
    }

    public delegate void OnEnhanceSelectedEvent();
  }
}
