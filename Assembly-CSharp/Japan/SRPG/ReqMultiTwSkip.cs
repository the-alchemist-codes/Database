﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqMultiTwSkip
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace SRPG
{
  public class ReqMultiTwSkip : WebAPI
  {
    public ReqMultiTwSkip(string tower_id, int skip_floor, Network.ResponseCallback response)
    {
      this.name = "btl/multi/tower/skip";
      this.body = WebAPI.GetRequestString<ReqMultiTwSkip.RequestParam>(new ReqMultiTwSkip.RequestParam()
      {
        tower_id = tower_id,
        floor = skip_floor
      });
      this.callback = response;
    }

    [Serializable]
    public class Response
    {
      public ReqMultiTwStatus.FloorParam[] floors;
      public Json_PlayerDataAll player;
    }

    [Serializable]
    public class RequestParam
    {
      public string tower_id;
      public int floor;
    }
  }
}
