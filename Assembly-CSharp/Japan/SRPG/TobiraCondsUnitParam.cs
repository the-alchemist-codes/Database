﻿// Decompiled with JetBrains decompiler
// Type: SRPG.TobiraCondsUnitParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;

namespace SRPG
{
  public class TobiraCondsUnitParam
  {
    private string mId;
    private string mUnitIname;
    private int mLevel;
    private int mAwakeLevel;
    private List<TobiraCondsUnitParam.CondJob> mJobs;
    private TobiraParam.Category mCategory;
    private int mTobiraLv;
    private TobiraCondsUnitParam.ConditionsDetail mConditionsDetail;

    public string Id
    {
      get
      {
        return this.mId;
      }
    }

    public string UnitIname
    {
      get
      {
        return this.mUnitIname;
      }
    }

    public int Level
    {
      get
      {
        return this.mLevel;
      }
    }

    public int AwakeLevel
    {
      get
      {
        return this.mAwakeLevel;
      }
    }

    public TobiraParam.Category TobiraCategory
    {
      get
      {
        return this.mCategory;
      }
    }

    public int TobiraLv
    {
      get
      {
        return this.mTobiraLv;
      }
    }

    public List<TobiraCondsUnitParam.CondJob> Jobs
    {
      get
      {
        return this.mJobs;
      }
    }

    public bool IsSelfUnit
    {
      get
      {
        return string.IsNullOrEmpty(this.mUnitIname);
      }
    }

    public void Deserialize(JSON_TobiraCondsUnitParam json)
    {
      if (json == null)
        return;
      this.mId = json.id;
      this.mUnitIname = json.unit_iname;
      this.mLevel = json.lv;
      this.mAwakeLevel = json.awake_lv;
      this.mJobs = new List<TobiraCondsUnitParam.CondJob>();
      if (json.jobs != null && json.jobs.Length > 0)
      {
        foreach (JSON_TobiraCondsUnitParam.JobCond job in json.jobs)
        {
          if (job != null && !string.IsNullOrEmpty(job.job_iname) && job.job_lv > 0)
            this.mJobs.Add(new TobiraCondsUnitParam.CondJob(job.job_iname, job.job_lv));
        }
      }
      this.mCategory = (TobiraParam.Category) json.category;
      this.mTobiraLv = json.tobira_lv;
      this.UpdateConditionsFlag();
    }

    private void UpdateConditionsFlag()
    {
      if (this.IsSelfUnit)
        this.mConditionsDetail |= TobiraCondsUnitParam.ConditionsDetail.IsSelf;
      if (this.Level > 0)
        this.mConditionsDetail |= TobiraCondsUnitParam.ConditionsDetail.IsUnitLv;
      if (this.mJobs != null && this.mJobs.Count > 0)
        this.mConditionsDetail |= TobiraCondsUnitParam.ConditionsDetail.IsJobLv;
      if (this.mAwakeLevel > 0)
        this.mConditionsDetail |= TobiraCondsUnitParam.ConditionsDetail.IsAwake;
      if (this.mTobiraLv <= 0)
        return;
      this.mConditionsDetail |= TobiraCondsUnitParam.ConditionsDetail.IsTobiraLv;
    }

    public bool HasFlag(TobiraCondsUnitParam.ConditionsDetail flag)
    {
      return (this.mConditionsDetail & flag) != (TobiraCondsUnitParam.ConditionsDetail) 0;
    }

    public bool IsJobLvClear(UnitData unit)
    {
      if (this.Jobs != null && this.Jobs.Count > 0 && unit != null)
      {
        foreach (TobiraCondsUnitParam.CondJob job in this.Jobs)
        {
          if (unit.GetJobLevelByJobID(job.JobIname) >= job.JobLevel)
            return true;
        }
      }
      return false;
    }

    public class CondJob
    {
      private string mJobIname;
      private int mJobLevel;

      public CondJob(string job_iname, int job_level)
      {
        this.mJobIname = job_iname;
        this.mJobLevel = job_level;
      }

      public string JobIname
      {
        get
        {
          return this.mJobIname;
        }
      }

      public int JobLevel
      {
        get
        {
          return this.mJobLevel;
        }
      }
    }

    [System.Flags]
    public enum ConditionsDetail : long
    {
      IsSelf = 1,
      IsUnitLv = 2,
      IsAwake = 4,
      IsJobLv = 8,
      IsTobiraLv = 16, // 0x0000000000000010
    }
  }
}
