﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RankingQuestRankList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace SRPG
{
  public class RankingQuestRankList : MonoBehaviour, ScrollListSetUp
  {
    private float Space;
    private int m_Max;
    private RankingQuestUserData[] m_UserDatas;
    private RankingQuestRankWindow m_RankingWindow;

    public RankingQuestRankList()
    {
      base.\u002Ector();
    }

    private void Start()
    {
      this.m_RankingWindow = (RankingQuestRankWindow) ((Component) this).GetComponentInParent<RankingQuestRankWindow>();
    }

    public void SetData(RankingQuestUserData[] data)
    {
      this.m_UserDatas = data;
    }

    public void OnSetUpItems()
    {
      if (this.m_UserDatas == null)
        return;
      ScrollListController component1 = (ScrollListController) ((Component) this).GetComponent<ScrollListController>();
      // ISSUE: method pointer
      component1.OnItemUpdate.RemoveListener(new UnityAction<int, GameObject>((object) this, __methodptr(OnUpdateItems)));
      // ISSUE: method pointer
      component1.OnItemUpdate.AddListener(new UnityAction<int, GameObject>((object) this, __methodptr(OnUpdateItems)));
      ((ScrollRect) ((Component) this).GetComponentInParent<ScrollRect>()).set_movementType((ScrollRect.MovementType) 2);
      RectTransform component2 = (RectTransform) ((Component) this).GetComponent<RectTransform>();
      Vector2 sizeDelta = component2.get_sizeDelta();
      Vector2 anchoredPosition = component2.get_anchoredPosition();
      this.m_Max = this.m_UserDatas.Length;
      component1.Space = (component1.ItemScale + this.Space) / component1.ItemScale;
      anchoredPosition.y = (__Null) 0.0;
      sizeDelta.y = (__Null) ((double) component1.ItemScale * (double) component1.Space * (double) this.m_Max);
      component2.set_sizeDelta(sizeDelta);
      component2.set_anchoredPosition(anchoredPosition);
    }

    public void OnUpdateItems(int idx, GameObject obj)
    {
      if (this.m_UserDatas == null || idx < 0 || idx >= this.m_UserDatas.Length)
      {
        obj.SetActive(false);
      }
      else
      {
        obj.SetActive(true);
        ListItemEvents component1 = (ListItemEvents) obj.GetComponent<ListItemEvents>();
        if (Object.op_Inequality((Object) component1, (Object) null))
          component1.OnSelect = new ListItemEvents.ListItemEvent(this.OnItemSelect);
        DataSource.Bind<RankingQuestUserData>(obj, this.m_UserDatas[idx], false);
        DataSource.Bind<ViewGuildData>(obj, this.m_UserDatas[idx].ViewGuild, false);
        SerializeValueBehaviour component2 = (SerializeValueBehaviour) obj.GetComponent<SerializeValueBehaviour>();
        if (Object.op_Inequality((Object) component2, (Object) null))
        {
          long num = this.m_UserDatas[idx].ViewGuild == null ? 0L : (long) this.m_UserDatas[idx].ViewGuild.id;
          component2.list.SetField(GuildSVB_Key.GUILD_ID, (int) num);
        }
        DataSource.Bind<UnitData>(obj, this.m_UserDatas[idx].m_UnitData, false);
        RankingQuestInfo component3 = (RankingQuestInfo) obj.GetComponent<RankingQuestInfo>();
        if (!Object.op_Inequality((Object) component3, (Object) null))
          return;
        component3.UpdateValue();
      }
    }

    private void OnItemSelect(GameObject go)
    {
      this.m_RankingWindow.OnItemSelect(go);
    }
  }
}
