﻿// Decompiled with JetBrains decompiler
// Type: SRPG.FilterUnitTab
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class FilterUnitTab : MonoBehaviour
  {
    [SerializeField]
    private GameObject m_FilteredObject;
    [SerializeField]
    private GameObject m_DefaultObject;
    [SerializeField]
    private Toggle m_Toggle;
    private bool m_IsFiltered;

    public FilterUnitTab()
    {
      base.\u002Ector();
    }

    public bool isOn
    {
      get
      {
        return !Object.op_Equality((Object) this.m_Toggle, (Object) null) && this.m_Toggle.get_isOn();
      }
      set
      {
        if (Object.op_Equality((Object) this.m_Toggle, (Object) null))
          return;
        this.m_Toggle.set_isOn(value);
      }
    }

    public void SetIsFiltered(bool isFiltered)
    {
      this.m_IsFiltered = isFiltered;
      GameUtility.SetGameObjectActive(this.m_FilteredObject, this.m_IsFiltered);
      GameUtility.SetGameObjectActive(this.m_DefaultObject, !this.m_IsFiltered);
    }
  }
}
