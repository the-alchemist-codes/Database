﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildRaidReward
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class GuildRaidReward
  {
    public RaidRewardType Type { get; private set; }

    public string IName { get; private set; }

    public int Num { get; private set; }

    public bool Deserialize(JSON_GuildRaidReward json)
    {
      this.Type = (RaidRewardType) json.item_type;
      this.IName = json.item_iname;
      this.Num = json.item_num;
      return true;
    }
  }
}
