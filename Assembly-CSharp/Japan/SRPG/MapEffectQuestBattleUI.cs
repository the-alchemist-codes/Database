﻿// Decompiled with JetBrains decompiler
// Type: SRPG.MapEffectQuestBattleUI
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;

namespace SRPG
{
  public class MapEffectQuestBattleUI : MonoBehaviour
  {
    public SRPG_Button ButtonMapEffect;
    [StringIsResourcePath(typeof (GameObject))]
    public string PrefabMapEffectQuest;
    private LoadRequest mReqMapEffect;

    public MapEffectQuestBattleUI()
    {
      base.\u002Ector();
    }

    private void Start()
    {
      if (!Object.op_Implicit((Object) this.ButtonMapEffect))
        return;
      this.ButtonMapEffect.AddListener((SRPG_Button.ButtonClickEvent) (button => this.ReqOpenMapEffect()));
    }

    private void ReqOpenMapEffect()
    {
      this.StartCoroutine(this.OpenMapEffect());
    }

    [DebuggerHidden]
    private IEnumerator OpenMapEffect()
    {
      // ISSUE: object of a compiler-generated type is created
      return (IEnumerator) new MapEffectQuestBattleUI.\u003COpenMapEffect\u003Ec__Iterator0()
      {
        \u0024this = this
      };
    }
  }
}
