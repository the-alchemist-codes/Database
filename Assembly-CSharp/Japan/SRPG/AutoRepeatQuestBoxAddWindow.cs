﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AutoRepeatQuestBoxAddWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class AutoRepeatQuestBoxAddWindow : MonoBehaviour
  {
    [SerializeField]
    private Text mMessageText;
    [SerializeField]
    private Button mDecideButton;

    public AutoRepeatQuestBoxAddWindow()
    {
      base.\u002Ector();
    }

    private void Start()
    {
      this.InitText();
    }

    private void InitText()
    {
      if (Object.op_Equality((Object) this.mMessageText, (Object) null))
        return;
      int addCount = MonoSingleton<GameManager>.Instance.Player.AutoRepeatQuestBox.AddCount;
      AutoRepeatQuestBoxParam repeatQuestBoxParam1 = MonoSingleton<GameManager>.Instance.MasterParam.GetAutoRepeatQuestBoxParam(addCount);
      AutoRepeatQuestBoxParam repeatQuestBoxParam2 = MonoSingleton<GameManager>.Instance.MasterParam.GetAutoRepeatQuestBoxParam(addCount + 1);
      AutoRepeatQuestBoxParam repeatQuestBoxParam3 = MonoSingleton<GameManager>.Instance.MasterParam.GetLastAutoRepeatQuestBoxParam();
      if (repeatQuestBoxParam1 == null)
      {
        DebugUtility.LogError("MasterParam > AutoRepeatQuestBox に指定された拡張回数のデータが見つかりませんでした [拡張回数:" + (object) addCount + "]");
        this.mMessageText.set_text(string.Empty);
        if (!Object.op_Inequality((Object) this.mDecideButton, (Object) null))
          return;
        ((Selectable) this.mDecideButton).set_interactable(false);
      }
      else if (repeatQuestBoxParam2 == null)
      {
        DebugUtility.LogError("現在拡張できる最大回数に達しているのに、Box枠の拡張をしようとしています");
        this.mMessageText.set_text(string.Empty);
        if (!Object.op_Inequality((Object) this.mDecideButton, (Object) null))
          return;
        ((Selectable) this.mDecideButton).set_interactable(false);
      }
      else
        this.mMessageText.set_text(string.Format(LocalizedText.Get("sys.AUTO_REPEAT_QUEST_BOX_ADD_CONFIRM_TEXT"), (object) repeatQuestBoxParam2.Coin, (object) (repeatQuestBoxParam2.Size - repeatQuestBoxParam1.Size), (object) repeatQuestBoxParam3.Size, (object) repeatQuestBoxParam1.Size, (object) repeatQuestBoxParam2.Size));
    }
  }
}
