﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildRaidStagePoint
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;
using UnityEngine;

namespace SRPG
{
  public class GuildRaidStagePoint : MonoBehaviour
  {
    [SerializeField]
    private List<Transform> mStageList;
    [SerializeField]
    private List<GameObject> mStageEffect;

    public GuildRaidStagePoint()
    {
      base.\u002Ector();
    }

    public List<Transform> StageList
    {
      get
      {
        return this.mStageList;
      }
    }

    public void SetClearEffect(int target)
    {
      if (this.mStageEffect == null || this.mStageEffect.Count <= target)
        return;
      for (int index = 0; index < this.mStageEffect.Count; ++index)
        GameUtility.SetGameObjectActive(this.mStageEffect[index], false);
      for (int index = 0; index < this.mStageEffect.Count && index < target; ++index)
        GameUtility.SetGameObjectActive(this.mStageEffect[index], true);
    }
  }
}
