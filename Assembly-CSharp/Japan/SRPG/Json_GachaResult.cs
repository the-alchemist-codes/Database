﻿// Decompiled with JetBrains decompiler
// Type: SRPG.Json_GachaResult
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class Json_GachaResult
  {
    public int is_pending = -1;
    public int rest = -1;
    public Json_DropInfo[] add;
    public Json_DropInfo[] add_mail;
    public Json_GachaReceipt receipt;
    public Json_PlayerData player;
    public Json_Item[] items;
    public Json_Unit[] units;
    public Json_Mail[] mails;
    public Json_Artifact[] artifacts;
    public JSON_TrophyProgress[] trophyprogs;
    public JSON_TrophyProgress[] bingoprogs;
    public Json_Item[] runes;
    public int rune_storage_used;
  }
}
