﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ProductList
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  [AddComponentMenu("Payment/ProductList")]
  [FlowNode.Pin(0, "表示", FlowNode.PinTypes.Input, 0)]
  [FlowNode.Pin(100, "選択された", FlowNode.PinTypes.Output, 0)]
  public class ProductList : MonoBehaviour, IFlowInterface
  {
    [Description("リストアイテムとして使用するゲームオブジェクト")]
    public PurchaseListItem ItemTemplate;
    [Description("リスト限定アイテムとして使用するゲームオブジェクト")]
    public PurchaseListItem ItemLimitedTemplate;
    [Description("詳細画面として使用するゲームオブジェクト")]
    public GameObject DetailTemplate;
    [Description("リストアイテム(VIP)として使用するゲームオブジェクト")]
    public PurchaseListItem ItemVipTemplate;
    [Description("リストアイテム(PREMIUM)として使用するゲームオブジェクト")]
    public PurchaseListItem ItemPremiumTemplate;
    private GameObject mDetailInfo;
    public ScrollRect ScrollRect;
    private List<PurchaseListItem> mParchaseListItem;
    private List<int> coinNumList;

    public ProductList()
    {
      base.\u002Ector();
    }

    public void Activated(int pinID)
    {
      if (pinID != 0)
        return;
      this.Refresh();
    }

    private void Awake()
    {
      ((Behaviour) this).set_enabled(true);
      if (Object.op_Inequality((Object) this.ItemTemplate, (Object) null) && ((Component) this.ItemTemplate).get_gameObject().get_activeInHierarchy())
        ((Component) this.ItemTemplate).get_gameObject().SetActive(false);
      if (Object.op_Inequality((Object) this.ItemLimitedTemplate, (Object) null) && ((Component) this.ItemLimitedTemplate).get_gameObject().get_activeInHierarchy())
        ((Component) this.ItemLimitedTemplate).get_gameObject().SetActive(false);
      if (Object.op_Inequality((Object) this.DetailTemplate, (Object) null) && this.DetailTemplate.get_gameObject().get_activeInHierarchy())
        this.DetailTemplate.SetActive(false);
      if (Object.op_Inequality((Object) this.ItemVipTemplate, (Object) null) && ((Component) this.ItemVipTemplate).get_gameObject().get_activeInHierarchy())
        ((Component) this.ItemVipTemplate).get_gameObject().SetActive(false);
      if (Object.op_Inequality((Object) this.ItemPremiumTemplate, (Object) null) && ((Component) this.ItemPremiumTemplate).get_gameObject().get_activeInHierarchy())
        ((Component) this.ItemPremiumTemplate).get_gameObject().SetActive(false);
      this.coinNumList = (List<int>) null;
    }

    private void Start()
    {
      this.RefreshItems(true);
    }

    public void Refresh()
    {
      this.RefreshItems(false);
      if (!Object.op_Inequality((Object) this.ScrollRect, (Object) null))
        return;
      ListExtras component = (ListExtras) ((Component) this.ScrollRect).GetComponent<ListExtras>();
      if (Object.op_Inequality((Object) component, (Object) null))
        component.SetScrollPos(1f);
      else
        this.ScrollRect.set_normalizedPosition(Vector2.get_zero());
    }

    private void RefreshItems(bool is_start)
    {
      Transform transform = ((Component) this).get_transform();
      for (int index = transform.get_childCount() - 1; index >= 0; --index)
      {
        Transform child = transform.GetChild(index);
        if (!Object.op_Equality((Object) child, (Object) null) && ((Component) child).get_gameObject().get_activeSelf())
          Object.DestroyImmediate((Object) ((Component) child).get_gameObject());
      }
      PaymentManager.Product[] product = BuyCoinManager.Instance.GetProduct();
      if (Object.op_Equality((Object) this.ItemTemplate, (Object) null) || product == null)
        return;
      PurchaseListItem purchaseListItem1 = (PurchaseListItem) null;
      PurchaseListItem purchaseListItem2 = (PurchaseListItem) null;
      this.mParchaseListItem.Clear();
      List<PurchaseListItem> purchaseListItemList = new List<PurchaseListItem>();
      for (int index = 0; index < product.Length; ++index)
      {
        PurchaseListItem purchaseListItem3;
        if (product[index].productID == (string) MonoSingleton<GameManager>.Instance.MasterParam.FixParam.PremiumProduct)
        {
          purchaseListItem3 = (PurchaseListItem) Object.Instantiate<PurchaseListItem>((M0) this.ItemPremiumTemplate);
          purchaseListItem1 = purchaseListItem3;
        }
        else if (product[index].productID == (string) MonoSingleton<GameManager>.Instance.MasterParam.FixParam.VipCardProduct)
        {
          purchaseListItem3 = (PurchaseListItem) Object.Instantiate<PurchaseListItem>((M0) this.ItemVipTemplate);
          purchaseListItem2 = purchaseListItem3;
        }
        else
        {
          purchaseListItem3 = BuyCoinManager.Instance.mNowSelectTab != BuyCoinManager.BuyCoinShopType.COIN ? (PurchaseListItem) Object.Instantiate<PurchaseListItem>((M0) this.ItemLimitedTemplate) : (PurchaseListItem) Object.Instantiate<PurchaseListItem>((M0) this.ItemTemplate);
          purchaseListItemList.Add(purchaseListItem3);
        }
        if (Object.op_Equality((Object) purchaseListItem3, (Object) null))
          return;
        ((Object) purchaseListItem3).set_hideFlags((HideFlags) 52);
        purchaseListItem3.Init(product[index]);
        DataSource.Bind<PaymentManager.Product>(((Component) purchaseListItem3).get_gameObject(), product[index], false);
        if (!is_start)
        {
          ListItemEvents component = (ListItemEvents) ((Component) purchaseListItem3).GetComponent<ListItemEvents>();
          if (Object.op_Inequality((Object) component, (Object) null))
          {
            component.OnSelect = new ListItemEvents.ListItemEvent(this.OnSelectItem);
            component.OnOpenDetail = new ListItemEvents.ListItemEvent(this.OnOpenItemDetail);
            component.OnCloseDetail = new ListItemEvents.ListItemEvent(this.OnCloseItemDetail);
          }
        }
        ((Component) purchaseListItem3).get_transform().SetParent(transform, false);
        ((Component) purchaseListItem3).get_gameObject().SetActive(true);
        this.mParchaseListItem.Add(purchaseListItem3);
      }
      if (Object.op_Inequality((Object) purchaseListItem2, (Object) null))
        ((Component) purchaseListItem2).get_transform().SetAsFirstSibling();
      if (Object.op_Inequality((Object) purchaseListItem1, (Object) null))
        ((Component) purchaseListItem1).get_transform().SetAsFirstSibling();
      if (Object.op_Inequality((Object) purchaseListItem1, (Object) null) && purchaseListItem1.IsPurchased)
        ((Component) purchaseListItem1).get_transform().SetAsLastSibling();
      if (Object.op_Inequality((Object) purchaseListItem2, (Object) null) && purchaseListItem2.IsPurchased)
        ((Component) purchaseListItem2).get_transform().SetAsLastSibling();
      if (this.coinNumList == null)
      {
        PaymentManager.Product[] products = FlowNode_PaymentGetProducts.Products;
        this.coinNumList = new List<int>();
        for (int index = 0; index < products.Length; ++index)
        {
          if (!this.coinNumList.Contains(products[index].numPaid))
            this.coinNumList.Add(products[index].numPaid);
        }
        this.coinNumList.Sort();
      }
      for (int index1 = 0; index1 < purchaseListItemList.Count; ++index1)
      {
        int index2 = this.coinNumList.IndexOf(purchaseListItemList[index1].CoinNumPaid);
        if (index2 >= 0)
          purchaseListItemList[index1].SetSpriteId(index2);
        if (!BuyCoinManager.Instance.GetProductBuyConfirm(purchaseListItemList[index1].ProductID))
          ((Component) purchaseListItemList[index1]).get_transform().SetAsLastSibling();
      }
    }

    private void OnSelectItem(GameObject go)
    {
      PaymentManager.Product dataOfClass = DataSource.FindDataOfClass<PaymentManager.Product>(go, (PaymentManager.Product) null);
      if (!BuyCoinManager.Instance.GetProductBuyConfirm(dataOfClass))
        return;
      GlobalVars.SelectedProductID = dataOfClass.productID;
      GlobalVars.SelectedProductIname = dataOfClass.ID;
      FlowNode_GameObject.ActivateOutputLinks((Component) this, 100);
    }

    private void OnCloseItemDetail(GameObject go)
    {
      if (!Object.op_Inequality((Object) this.mDetailInfo, (Object) null))
        return;
      Object.DestroyImmediate((Object) this.mDetailInfo.get_gameObject());
      this.mDetailInfo = (GameObject) null;
    }

    private void OnOpenItemDetail(GameObject go)
    {
      PaymentManager.Product dataOfClass = DataSource.FindDataOfClass<PaymentManager.Product>(go, (PaymentManager.Product) null);
      if (!Object.op_Equality((Object) this.mDetailInfo, (Object) null) || dataOfClass == null)
        return;
      this.mDetailInfo = (GameObject) Object.Instantiate<GameObject>((M0) this.DetailTemplate);
      DataSource.Bind<PaymentManager.Product>(this.mDetailInfo, dataOfClass, false);
      this.mDetailInfo.SetActive(true);
    }

    private void Update()
    {
    }
  }
}
