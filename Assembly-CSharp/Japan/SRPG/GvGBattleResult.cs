﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GvGBattleResult
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections.Generic;
using UnityEngine;

namespace SRPG
{
  [FlowNode.Pin(1, "初期化", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(100, "GvGリプレイ", FlowNode.PinTypes.Output, 100)]
  public class GvGBattleResult : MonoBehaviour, IFlowInterface
  {
    public const int PIN_INPUT_INIT = 1;
    public const int PIN_OUTPUT_REPLAY = 100;
    [SerializeField]
    private GameObject mPlayerTemplate;
    [SerializeField]
    private GameObject mEnemyTemplate;
    [SerializeField]
    private GameObject mWin;
    [SerializeField]
    private GameObject mLose;

    public GvGBattleResult()
    {
      base.\u002Ector();
    }

    public void Activated(int pinID)
    {
      if (pinID != 1)
        return;
      this.Initialize();
    }

    private void Initialize()
    {
      SceneBattle instance = SceneBattle.Instance;
      if (Object.op_Equality((Object) instance, (Object) null))
        return;
      BattleCore.QuestResult questResult = instance.Battle.GetQuestResult();
      GameUtility.SetGameObjectActive(this.mWin, questResult == BattleCore.QuestResult.Win);
      GameUtility.SetGameObjectActive(this.mLose, questResult == BattleCore.QuestResult.Lose);
      if (!Object.op_Inequality((Object) this.mPlayerTemplate, (Object) null) || !Object.op_Inequality((Object) this.mEnemyTemplate, (Object) null))
        return;
      GameUtility.SetGameObjectActive(this.mPlayerTemplate, false);
      GameUtility.SetGameObjectActive(this.mEnemyTemplate, false);
      List<int> battleResultFinishHp1 = instance.Battle.GetPreBattleResultFinishHp(EUnitSide.Player);
      List<int> battleResultFinishHp2 = instance.Battle.GetPreBattleResultFinishHp(EUnitSide.Enemy);
      int index1 = 0;
      int index2 = 0;
      for (int index3 = 0; index3 < instance.Battle.Units.Count; ++index3)
      {
        Unit unit = instance.Battle.Units[index3];
        if (!unit.IsUnitFlag(EUnitFlag.CreatedBreakObj) && !unit.IsUnitFlag(EUnitFlag.IsDynamicTransform))
        {
          GameObject gameObject = (GameObject) null;
          int hp = 0;
          if (unit.Side == EUnitSide.Player)
          {
            gameObject = (GameObject) Object.Instantiate<GameObject>((M0) this.mPlayerTemplate, this.mPlayerTemplate.get_transform().get_parent());
            hp = battleResultFinishHp1[index1];
            ++index1;
          }
          else if (unit.Side == EUnitSide.Enemy)
          {
            gameObject = (GameObject) Object.Instantiate<GameObject>((M0) this.mEnemyTemplate, this.mEnemyTemplate.get_transform().get_parent());
            hp = battleResultFinishHp2[index2];
            ++index2;
          }
          if (!Object.op_Equality((Object) gameObject, (Object) null))
          {
            GvGBattleResultUnitContentData data = new GvGBattleResultUnitContentData();
            data.Deserilize(instance.Battle.Units[index3], hp);
            DataSource.Bind<GvGBattleResultUnitContentData>(gameObject, data, false);
            GameUtility.SetGameObjectActive(gameObject, true);
          }
        }
      }
    }

    public void OnReplaySet()
    {
      GlobalVars.GvGBattleReplay.Set(true);
      FlowNode_GameObject.ActivateOutputLinks((Component) this, 100);
    }
  }
}
