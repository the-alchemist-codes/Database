﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqGvGBattle
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using Gsc.Network.Encoding;
using MessagePack;
using System;

namespace SRPG
{
  public class ReqGvGBattle : WebAPI
  {
    public ReqGvGBattle(
      int id,
      int gid,
      int gvg_group_id,
      SRPG.Network.ResponseCallback response,
      EncodingTypes.ESerializeCompressMethod serializeCompressMethod)
    {
      this.name = "gvg/btl";
      this.body = WebAPI.GetRequestString<ReqGvGBattle.RequestParam>(new ReqGvGBattle.RequestParam()
      {
        id = id,
        gid = gid,
        gvg_group_id = gvg_group_id
      });
      this.callback = response;
      this.serializeCompressMethod = serializeCompressMethod;
    }

    [Serializable]
    public class RequestParam
    {
      public int id;
      public int gid;
      public int gvg_group_id;
    }

    [MessagePackObject(true)]
    [Serializable]
    public class Response
    {
      public JSON_GvGPartyUnit[] offense;
      public int win_num;
      public int beat_num;
      public int seed;
      public long[] used_units;
      public int cool_time;
    }
  }
}
