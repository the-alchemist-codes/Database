﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AdvanceLapBossParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System.Collections.Generic;
using UnityEngine;

namespace SRPG
{
  public class AdvanceLapBossParam
  {
    private string mIname;
    private int mRoundBuffMax;
    private string mBossBuffId;
    private string mOtherBuffId;
    private AdvanceLapBossParam.LapInfo[] mLapInfo;
    private BuffEffectParam mBossBuffParam;
    private BuffEffectParam mOtherBuffParam;

    public string Iname
    {
      get
      {
        return this.mIname;
      }
    }

    public int RoundBuffMax
    {
      get
      {
        return this.mRoundBuffMax;
      }
    }

    public string BossBuffId
    {
      get
      {
        return this.mBossBuffId;
      }
    }

    public BuffEffectParam BossBuffParam
    {
      get
      {
        if (this.mBossBuffParam == null)
        {
          GameManager instance = MonoSingleton<GameManager>.Instance;
          if (Object.op_Implicit((Object) instance) && instance.MasterParam != null)
            this.mBossBuffParam = instance.MasterParam.GetBuffEffectParam(this.mBossBuffId);
        }
        return this.mBossBuffParam;
      }
    }

    public string OtherBuffId
    {
      get
      {
        return this.mOtherBuffId;
      }
    }

    public BuffEffectParam OtherBuffParam
    {
      get
      {
        if (this.mOtherBuffParam == null)
        {
          GameManager instance = MonoSingleton<GameManager>.Instance;
          if (Object.op_Implicit((Object) instance) && instance.MasterParam != null)
            this.mOtherBuffParam = instance.MasterParam.GetBuffEffectParam(this.mOtherBuffId);
        }
        return this.mOtherBuffParam;
      }
    }

    public List<AdvanceLapBossParam.LapInfo> LapInfoList
    {
      get
      {
        return this.mLapInfo != null ? new List<AdvanceLapBossParam.LapInfo>((IEnumerable<AdvanceLapBossParam.LapInfo>) this.mLapInfo) : new List<AdvanceLapBossParam.LapInfo>();
      }
    }

    public void Deserialize(JSON_AdvanceLapBossParam json)
    {
      if (json == null)
        return;
      this.mIname = json.iname;
      this.mRoundBuffMax = json.round_buff_max;
      this.mBossBuffId = json.boss_bid;
      this.mOtherBuffId = json.other_bid;
      this.mLapInfo = (AdvanceLapBossParam.LapInfo[]) null;
      if (json.lap_info == null || json.lap_info.Length == 0)
        return;
      this.mLapInfo = new AdvanceLapBossParam.LapInfo[json.lap_info.Length];
      for (int index = 0; index < json.lap_info.Length; ++index)
      {
        this.mLapInfo[index] = new AdvanceLapBossParam.LapInfo();
        this.mLapInfo[index].Deserialize(json.lap_info[index]);
      }
    }

    public AdvanceRewardParam GetRoundReward(int round)
    {
      List<AdvanceLapBossParam.LapInfo> lapInfoList = this.LapInfoList;
      if (lapInfoList.Count == 0)
        return (AdvanceRewardParam) null;
      for (int index = lapInfoList.Count - 1; index >= 0; --index)
      {
        AdvanceLapBossParam.LapInfo lapInfo = lapInfoList[index];
        if (lapInfo != null && round >= lapInfo.Round)
          return lapInfo.Reward;
      }
      return lapInfoList[0].Reward;
    }

    public static void Deserialize(
      ref List<AdvanceLapBossParam> list,
      JSON_AdvanceLapBossParam[] json)
    {
      if (json == null)
        return;
      if (list == null)
        list = new List<AdvanceLapBossParam>(json.Length);
      list.Clear();
      for (int index = 0; index < json.Length; ++index)
      {
        AdvanceLapBossParam advanceLapBossParam = new AdvanceLapBossParam();
        advanceLapBossParam.Deserialize(json[index]);
        list.Add(advanceLapBossParam);
      }
    }

    public class LapInfo
    {
      private int mRound;
      private string mRewardId;
      private AdvanceRewardParam mReward;

      public int Round
      {
        get
        {
          return this.mRound;
        }
      }

      public string RewardId
      {
        get
        {
          return this.mRewardId;
        }
      }

      public AdvanceRewardParam Reward
      {
        get
        {
          if (this.mReward == null)
          {
            GameManager instance = MonoSingleton<GameManager>.Instance;
            if (Object.op_Implicit((Object) instance))
              this.mReward = instance.GetAdvanceRewardParam(this.mRewardId);
          }
          return this.mReward;
        }
      }

      public void Deserialize(JSON_AdvanceLapBossParam.LapInfo json)
      {
        if (json == null)
          return;
        this.mRound = json.round;
        this.mRewardId = json.reward_id;
      }
    }
  }
}
