﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildFacilitySVB_Key
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class GuildFacilitySVB_Key
  {
    public static readonly string EFFECT_NAME = "GUILD_FACILITY_EFFECT_NAME";
    public static readonly string EFFECT_VALUE = "GUILD_FACILITY_EFFECT_VALUE";
    public static readonly string EFFECT_PLUS = "GUILD_FACILITY_EFFECT_PLUS";
    public static readonly string EFFECT_PLUS_ICON = "GUILD_FACILITY_EFFECT_PLUS_ICON";
  }
}
