﻿// Decompiled with JetBrains decompiler
// Type: SRPG.AppPath
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public static class AppPath
  {
    public static string persistentDataPath
    {
      get
      {
        return Application.get_dataPath() + "/../data";
      }
    }

    public static string temporaryCachePath
    {
      get
      {
        return Application.get_dataPath() + "/../temp";
      }
    }

    public static string assetCachePath
    {
      get
      {
        return Application.get_dataPath() + "/..";
      }
    }

    public static string assetCachePathOld
    {
      get
      {
        return Application.get_dataPath() + "/..";
      }
    }

    public static string crashLogPath
    {
      get
      {
        return Application.get_dataPath() + "/../crash";
      }
    }
  }
}
