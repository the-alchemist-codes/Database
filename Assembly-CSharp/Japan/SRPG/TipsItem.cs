﻿// Decompiled with JetBrains decompiler
// Type: SRPG.TipsItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class TipsItem : MonoBehaviour
  {
    [SerializeField]
    private GameObject BadgeObj;
    [SerializeField]
    private GameObject CompleteObj;
    [SerializeField]
    private Text TitleObj;
    [SerializeField]
    private GameObject OverayImageObj;
    [SerializeField]
    private Button SelfButton;
    public string Title;
    public bool IsCompleted;
    public bool IsHidden;

    public TipsItem()
    {
      base.\u002Ector();
    }

    public void UpdateContent()
    {
      if (Object.op_Inequality((Object) this.BadgeObj, (Object) null))
        this.BadgeObj.SetActive(!this.IsHidden && !this.IsCompleted);
      if (Object.op_Inequality((Object) this.CompleteObj, (Object) null))
        this.CompleteObj.SetActive(!this.IsHidden && this.IsCompleted);
      if (Object.op_Inequality((Object) this.TitleObj, (Object) null))
        this.TitleObj.set_text(this.Title);
      if (Object.op_Inequality((Object) this.OverayImageObj, (Object) null))
        this.OverayImageObj.SetActive(this.IsHidden);
      if (!Object.op_Inequality((Object) this.SelfButton, (Object) null))
        return;
      ((Selectable) this.SelfButton).set_interactable(!this.IsHidden);
    }
  }
}
