﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReplayQuestListManager
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System.Collections.Generic;
using UnityEngine;

namespace SRPG
{
  public class ReplayQuestListManager : MonoBehaviour
  {
    private static ReplayQuestListManager mInstance;
    private List<string> mPlayableScenario;

    public ReplayQuestListManager()
    {
      base.\u002Ector();
    }

    public static ReplayQuestListManager Instance
    {
      get
      {
        return ReplayQuestListManager.mInstance;
      }
    }

    private void Awake()
    {
      ReplayQuestListManager.mInstance = this;
    }

    public List<string> PlayableScenario
    {
      get
      {
        if (this.mPlayableScenario == null)
          this.mPlayableScenario = ReplayQuestListManager.GetPlayableReplayScenario();
        return this.mPlayableScenario;
      }
    }

    public static List<string> GetPlayableReplayScenario()
    {
      QuestParam[] availableQuests = MonoSingleton<GameManager>.Instance.Player.AvailableQuests;
      if (availableQuests == null || availableQuests.Length <= 0)
        return new List<string>();
      List<string> stringList = new List<string>();
      long serverTime = Network.GetServerTime();
      foreach (QuestParam questParam in availableQuests)
      {
        if (!questParam.IsMulti && questParam.type != QuestTypes.Beginner && (questParam.state == QuestStates.Cleared || questParam.state == QuestStates.Challenged) && questParam.IsReplayDateUnlock(serverTime))
        {
          string eventStart = questParam.event_start;
          string eventClear = questParam.event_clear;
          if (!string.IsNullOrEmpty(eventStart) && (questParam.state == QuestStates.Cleared || questParam.state == QuestStates.Challenged) && !stringList.Contains(eventStart))
            stringList.Add(eventStart);
          if (!string.IsNullOrEmpty(eventClear) && questParam.state == QuestStates.Cleared && !stringList.Contains(eventClear))
            stringList.Add(eventClear);
        }
      }
      return stringList;
    }

    private void OnDestroy()
    {
      ReplayQuestListManager.mInstance = (ReplayQuestListManager) null;
    }
  }
}
