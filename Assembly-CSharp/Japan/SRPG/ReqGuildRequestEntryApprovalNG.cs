﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqGuildRequestEntryApprovalNG
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Text;

namespace SRPG
{
  public class ReqGuildRequestEntryApprovalNG : WebAPI
  {
    public ReqGuildRequestEntryApprovalNG(
      string[] request_user_uids,
      Network.ResponseCallback response)
    {
      this.name = "guild/apply/reject";
      StringBuilder stringBuilder = WebAPI.GetStringBuilder();
      stringBuilder.Append("\"target_uids\":[");
      for (int index = 0; index < request_user_uids.Length; ++index)
      {
        stringBuilder.Append("\"" + request_user_uids[index] + "\"");
        if (index != request_user_uids.Length - 1)
          stringBuilder.Append(",");
      }
      stringBuilder.Append("]");
      this.body = WebAPI.GetRequestString(stringBuilder.ToString());
      this.callback = response;
    }
  }
}
