﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildRaidRankingDamage
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using System;

namespace SRPG
{
  public class GuildRaidRankingDamage
  {
    public int Role { get; private set; }

    public int Rank { get; private set; }

    public long Score { get; private set; }

    public string Name { get; private set; }

    public int Lv { get; private set; }

    public UnitParam Unit { get; private set; }

    public int UnitStrengthTotal { get; private set; }

    public int BossId { get; private set; }

    public int Round { get; private set; }

    public DateTime KnockDownAt { get; private set; }

    public bool Deserialize(JSON_GuildRaidRankingDamage json)
    {
      this.Role = json.role;
      this.Rank = json.rank;
      this.Score = json.score;
      this.Name = json.name;
      this.Lv = json.lv;
      if (json.unit != null)
      {
        this.Unit = MonoSingleton<GameManager>.Instance.MasterParam.GetUnitParam(json.unit);
        if (this.Unit == null)
          return false;
      }
      this.UnitStrengthTotal = json.unit_strength_total;
      this.BossId = json.boss_id;
      this.Round = json.round;
      if (json.knock_down_at > 0)
        this.KnockDownAt = TimeManager.FromUnixTime((long) json.knock_down_at);
      return true;
    }
  }
}
