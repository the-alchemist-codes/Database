﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildRaidPeriodTime
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class GuildRaidPeriodTime
  {
    public string Begin { get; private set; }

    public string Open { get; private set; }

    public bool Deserialize(JSON_GuildRaidPeriodTime json)
    {
      this.Begin = json.begin_time;
      this.Open = json.open_time;
      return true;
    }
  }
}
