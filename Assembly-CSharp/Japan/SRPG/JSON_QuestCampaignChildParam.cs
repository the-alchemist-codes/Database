﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_QuestCampaignChildParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_QuestCampaignChildParam
  {
    public string iname;
    public int scope;
    public int quest_type;
    public int quest_mode;
    public string quest_id;
    public string unit;
    public int drop_rate;
    public int drop_num;
    public int exp_player;
    public int exp_unit;
    public int ap_rate;
    public int rental_fav_rate;
  }
}
