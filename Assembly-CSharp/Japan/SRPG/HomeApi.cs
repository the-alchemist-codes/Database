﻿// Decompiled with JetBrains decompiler
// Type: SRPG.HomeApi
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Text;

namespace SRPG
{
  public class HomeApi : WebAPI
  {
    public HomeApi(
      bool isMultiPush,
      bool isGuildInvite,
      bool isReqCoinBonus,
      bool isAutoRepeatQuest,
      bool isReqPartyOverWrite,
      bool isGuildRoleReward,
      Network.ResponseCallback response)
    {
      this.name = "home";
      if (!isMultiPush && !isGuildInvite && !isReqCoinBonus)
      {
        this.body = WebAPI.GetRequestString((string) null);
      }
      else
      {
        StringBuilder stringBuilder = WebAPI.GetStringBuilder();
        if (isMultiPush)
        {
          stringBuilder.Append("\"is_multi_push\":1");
          stringBuilder.Append(",");
        }
        if (isGuildInvite)
        {
          stringBuilder.Append("\"is_chk_guild_applied\":1");
          stringBuilder.Append(",");
        }
        if (isReqCoinBonus)
        {
          stringBuilder.Append("\"is_chk_coin_bonus\":1");
          stringBuilder.Append(",");
        }
        if (isAutoRepeatQuest)
        {
          stringBuilder.Append("\"is_auto_repeat\":1");
          stringBuilder.Append(",");
        }
        if (isReqPartyOverWrite)
        {
          stringBuilder.Append("\"is_party_decks\":1");
          stringBuilder.Append(",");
        }
        if (isGuildRoleReward)
        {
          stringBuilder.Append("\"is_guild_role_reward\":1");
          stringBuilder.Append(",");
        }
        if (stringBuilder[stringBuilder.Length - 1] == ',')
          --stringBuilder.Length;
        this.body = WebAPI.GetRequestString(stringBuilder.ToString());
      }
      this.callback = response;
    }
  }
}
