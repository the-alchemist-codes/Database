﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GuildLobby
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using GR;
using UnityEngine;

namespace SRPG
{
  [FlowNode.Pin(1, "初期化", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(2, "表示更新", FlowNode.PinTypes.Input, 2)]
  [FlowNode.Pin(3, "ギルド施設強化を開く", FlowNode.PinTypes.Input, 3)]
  [FlowNode.Pin(4, "GVG参加可能か？", FlowNode.PinTypes.Input, 4)]
  [FlowNode.Pin(110, "ギルマスです", FlowNode.PinTypes.Output, 110)]
  [FlowNode.Pin(120, "サブマスです", FlowNode.PinTypes.Output, 120)]
  [FlowNode.Pin(130, "メンバーです", FlowNode.PinTypes.Output, 130)]
  [FlowNode.Pin(140, "ギルド施設強化を開く", FlowNode.PinTypes.Output, 140)]
  [FlowNode.Pin(150, "ギルドに加入していない", FlowNode.PinTypes.Output, 150)]
  [FlowNode.Pin(200, "ギルドレイド開始", FlowNode.PinTypes.Output, 200)]
  [FlowNode.Pin(300, "GVG参加", FlowNode.PinTypes.Output, 300)]
  [FlowNode.Pin(310, "GVGマッチングされてない", FlowNode.PinTypes.Output, 310)]
  [FlowNode.Pin(320, "GVGマッチング中", FlowNode.PinTypes.Output, 320)]
  public class GuildLobby : MonoBehaviour, IFlowInterface
  {
    private const int PIN_INPUT_INIT = 1;
    private const int PIN_INPUT_REFRESH = 2;
    private const int PIN_INPUT_GUILD_FACILITY_ENHANCE = 3;
    private const int PIN_INPUT_GVG_STATUS = 4;
    private const int PIN_OUTPUT_GUILD_MASTER = 110;
    private const int PIN_OUTPUT_SUB_GUILD_MASTER = 120;
    private const int PIN_OUTPUT_GUILD_MEMBER = 130;
    private const int PIN_OUTPUT_GUILD_FACILITY_ENHANCE = 140;
    private const int PIN_OUTPUT_GUILD_ERROR_NOT_ENTRY = 150;
    private const int PIN_OUTPUT_GUILDRAID_START = 200;
    private const int PIN_OUTPUT_GVG_JOIN = 300;
    private const int PIN_OUTPUT_GVG_NOT_JOIN = 310;
    private const int PIN_OUTPUT_GVG_NOW_MATCHING = 320;
    private static GuildLobby mInstance;
    [SerializeField]
    private GameObject mWindow;
    [SerializeField]
    private GameObject mGuildInfo;
    [SerializeField]
    private GameObject mGuildInfoBadge;
    [SerializeField]
    private GameObject mGuildRaidOpen;
    [SerializeField]
    private GameObject mGuildRaidClose;
    [SerializeField]
    private GameObject mGvGOpen;
    [SerializeField]
    private GameObject mGvGClose;
    private SerializeValueBehaviour mSerializeValueBehaviour;

    public GuildLobby()
    {
      base.\u002Ector();
    }

    public static GuildLobby Instance
    {
      get
      {
        return GuildLobby.mInstance;
      }
    }

    public void Activated(int pinID)
    {
      switch (pinID)
      {
        case 1:
          this.Init();
          break;
        case 2:
          this.Refresh();
          break;
        case 3:
          this.OpenGuildFacilityEnhanceUI();
          break;
        case 4:
          if (GuildManager.Instance.EntryStatus == GuildManager.GvGEntryStatus.NOT_MATCHING)
          {
            FlowNode_GameObject.ActivateOutputLinks((Component) this, 310);
            break;
          }
          if (GuildManager.Instance.EntryStatus == GuildManager.GvGEntryStatus.MATCHINGPROCESS)
          {
            FlowNode_GameObject.ActivateOutputLinks((Component) this, 320);
            break;
          }
          FlowNode_GameObject.ActivateOutputLinks((Component) this, 300);
          break;
      }
    }

    private void Awake()
    {
      GuildLobby.mInstance = this;
      if (Object.op_Inequality((Object) this.mWindow, (Object) null))
        this.mWindow.SetActive(false);
      if (Object.op_Inequality((Object) this.mGuildInfoBadge, (Object) null))
        this.mGuildInfoBadge.SetActive(false);
      this.mSerializeValueBehaviour = (SerializeValueBehaviour) ((Component) this).GetComponent<SerializeValueBehaviour>();
    }

    private void Init()
    {
      if (Object.op_Inequality((Object) this.mWindow, (Object) null))
        this.mWindow.SetActive(true);
      this.Refresh();
    }

    public void Refresh()
    {
      if (MonoSingleton<GameManager>.Instance.Player.Guild == null || MonoSingleton<GameManager>.Instance.Player.PlayerGuild == null || !MonoSingleton<GameManager>.Instance.Player.PlayerGuild.IsJoined)
      {
        FlowNode_GameObject.ActivateOutputLinks((Component) this, 150);
      }
      else
      {
        if (Object.op_Inequality((Object) this.mGuildInfoBadge, (Object) null))
          this.mGuildInfoBadge.SetActive(GuildManager.Instance.EntryRequests.Length > 0);
        this.mSerializeValueBehaviour.list.SetObject(GuildSVB_Key.GUILD, (object) MonoSingleton<GameManager>.Instance.Player.Guild);
        DataSource.Bind<GuildData>(((Component) this).get_gameObject(), MonoSingleton<GameManager>.Instance.Player.Guild, false);
        if (Object.op_Inequality((Object) this.mGuildRaidOpen, (Object) null) && Object.op_Inequality((Object) this.mGuildRaidClose, (Object) null))
        {
          GuildRaidPeriodParam raidRewardPeriod = MonoSingleton<GameManager>.Instance.GetActiveGuildRaidRewardPeriod();
          GameUtility.SetGameObjectActive(this.mGuildRaidOpen, raidRewardPeriod != null);
          GameUtility.SetGameObjectActive(this.mGuildRaidClose, raidRewardPeriod == null);
        }
        if (Object.op_Inequality((Object) this.mGvGOpen, (Object) null) && Object.op_Inequality((Object) this.mGvGClose, (Object) null))
        {
          GvGPeriodParam gvGexitPeriod = GvGPeriodParam.GetGvGExitPeriod();
          GameUtility.SetGameObjectActive(this.mGvGOpen, gvGexitPeriod != null);
          GameUtility.SetGameObjectActive(this.mGvGClose, gvGexitPeriod == null);
        }
        DataSource.Bind<UnitData>(this.mGuildInfo, MonoSingleton<GameManager>.Instance.Player.Guild.GuildMaster.Unit, false);
        DataSource.Bind<GuildMemberData>(this.mGuildInfo, MonoSingleton<GameManager>.Instance.Player.Guild.GuildMaster, false);
        GameParameter.UpdateAll(((Component) this).get_gameObject());
        if (MonoSingleton<GameManager>.Instance.Player.PlayerGuild.IsGuildMaster)
          FlowNode_GameObject.ActivateOutputLinks((Component) this, 110);
        else if (MonoSingleton<GameManager>.Instance.Player.PlayerGuild.IsSubGuildMaster)
          FlowNode_GameObject.ActivateOutputLinks((Component) this, 120);
        else
          FlowNode_GameObject.ActivateOutputLinks((Component) this, 130);
      }
    }

    private void OpenGuildFacilityEnhanceUI()
    {
      if (MonoSingleton<GameManager>.Instance.Player.Guild.CreatedUid != MonoSingleton<GameManager>.Instance.DeviceId && Network.GetServerTime() < MonoSingleton<GameManager>.Instance.Player.PlayerGuild.JoinedAt + (long) MonoSingleton<GameManager>.Instance.MasterParam.FixParam.GuildInvestCoolTime)
        UIUtility.NegativeSystemMessage((string) null, LocalizedText.Get("sys.GUILD_TEXT_CANT_INVEST_FACILITY"), (UIUtility.DialogResultEvent) null, (GameObject) null, false, -1);
      else
        FlowNode_GameObject.ActivateOutputLinks((Component) this, 140);
    }

    public void OnGuildRaid()
    {
      PlayerData player = MonoSingleton<GameManager>.Instance.Player;
      if (player.CheckUnlock(UnlockTargets.GuildRaid))
        FlowNode_GameObject.ActivateOutputLinks((Component) this, 200);
      else
        LevelLock.ShowLockMessage(player.Lv, player.VipRank, UnlockTargets.GuildRaid);
    }
  }
}
