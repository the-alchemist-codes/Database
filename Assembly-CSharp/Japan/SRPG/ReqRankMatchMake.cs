﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqRankMatchMake
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class ReqRankMatchMake : WebAPI
  {
    public ReqRankMatchMake(Network.ResponseCallback response = null)
    {
      this.name = "vs/rankmatch/make";
      this.body = WebAPI.GetRequestString(string.Empty);
      this.callback = response;
    }

    public class Response
    {
      public string token;
      public string owner_name;
      public string btl_ver;
    }
  }
}
