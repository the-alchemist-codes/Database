﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GvGPartyUnit
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace SRPG
{
  public class GvGPartyUnit : UnitData
  {
    public int HP { get; private set; }

    public bool Deserialize(JSON_GvGPartyUnit json)
    {
      if (json == null || json.iid == 0L)
        return false;
      this.Deserialize((Json_Unit) json);
      this.HP = json.hp;
      return true;
    }

    public void SetHP(int hp)
    {
      this.HP = hp;
    }
  }
}
