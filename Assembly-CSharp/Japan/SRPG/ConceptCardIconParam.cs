﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ConceptCardIconParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

namespace SRPG
{
  public class ConceptCardIconParam : ContentSource.Param
  {
    public bool Enable = true;
    public ConceptCardData ConceptCard;
    public bool Select;
    public bool IsRecommend;
    public bool IsEmpty;
    public bool IsEnableDecreaseEffect;
    public int DecreaseEffectRate;
    private ConceptCardIconNode mNode;

    public override void OnEnable(ContentNode node)
    {
      base.OnEnable(node);
      this.mNode = node as ConceptCardIconNode;
      this.Refresh();
    }

    public override void OnDisable(ContentNode node)
    {
      this.mNode = (ConceptCardIconNode) null;
      base.OnDisable(node);
    }

    public void Refresh()
    {
      if (Object.op_Equality((Object) this.mNode, (Object) null))
        return;
      this.mNode.Setup(this.ConceptCard);
      this.mNode.Empty(this.IsEmpty);
      this.mNode.Select(this.Select);
      this.mNode.Enable(this.Enable);
      this.mNode.SetNotSellFlag(this.ConceptCard != null && this.ConceptCard.Param.not_sale);
      this.mNode.Recommend(this.IsRecommend);
      this.mNode.SetDecreaseEffectActive(this.IsEnableDecreaseEffect, this.DecreaseEffectRate);
    }
  }
}
