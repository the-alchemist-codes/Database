﻿// Decompiled with JetBrains decompiler
// Type: SRPG.RuneEnhanceEffectWindow
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  [FlowNode.Pin(1, "強化アニメーション開始", FlowNode.PinTypes.Input, 1)]
  [FlowNode.Pin(2, "強化成功アニメーション", FlowNode.PinTypes.Output, 2)]
  [FlowNode.Pin(3, "強化失敗アニメーション", FlowNode.PinTypes.Output, 3)]
  [FlowNode.Pin(10, "強化", FlowNode.PinTypes.Input, 10)]
  [FlowNode.Pin(11, "ゲージ使用", FlowNode.PinTypes.Input, 11)]
  [FlowNode.Pin(12, "ボタン表示更新", FlowNode.PinTypes.Input, 12)]
  [FlowNode.Pin(100, "強化通信開始", FlowNode.PinTypes.Output, 100)]
  [FlowNode.Pin(110, "強化通信完了", FlowNode.PinTypes.Input, 110)]
  [FlowNode.Pin(120, "強化再演出開始", FlowNode.PinTypes.Output, 120)]
  [FlowNode.Pin(200, "自身を閉じてルーン装備へ", FlowNode.PinTypes.Input, 200)]
  [FlowNode.Pin(1000, "自身を閉じる", FlowNode.PinTypes.Output, 1000)]
  public class RuneEnhanceEffectWindow : MonoBehaviour, IFlowInterface
  {
    private const int INPUT_RUNE_ENHANCE_ANIM = 1;
    private const int OUTPUT_RUNE_ENHANCE_SUCCESS_ANIM = 2;
    private const int OUTPUT_RUNE_ENHANCE_FAILURE_ANIM = 3;
    private const int INPUT_RUNE_ENHANCE = 10;
    private const int INPUT_RUNE_USE_GAUGE = 11;
    private const int INPUT_REFRESH_BUTTON = 12;
    private const int OUTPUT_START_ENHANCE = 100;
    private const int INPUT_FINISHED_ENHANCE = 110;
    private const int OUTPUT_START_RE_ENHANCE = 120;
    private const int INPUT_CLOSE_TO_EQUIP = 200;
    private const int OUTPUT_CLOSE_WINDOW = 1000;
    [SerializeField]
    private RuneIcon mRuneIcon;
    [SerializeField]
    private RuneDrawEnhanceLevel mRuneDrawEnhanceLevel;
    [SerializeField]
    private RuneDrawEnhancedBaseState mRuneDrawEnhancedBaseState;
    [SerializeField]
    private RuneDrawCost mRuneDrawCost;
    [SerializeField]
    private RuneDrawEnhanceEffect mRuneDrawEnhanceEffect;
    [SerializeField]
    private RuneDrawEnhancePercentage mRuneDrawEnhancePercentage;
    [SerializeField]
    private Button mEnhanceButton;
    [SerializeField]
    private GameObject mSwitchObj_NextEnhance;
    [SerializeField]
    private GameObject mSwitchObj_NextEvo;
    private RuneManager mRuneManager;
    private BindRuneData mRuneDataBefore;
    private BindRuneData mRuneDataCurr;
    private bool mIsEnhanceSuccess;
    private bool mIsUseEnforceGauge;

    public RuneEnhanceEffectWindow()
    {
      base.\u002Ector();
    }

    public void Awake()
    {
    }

    private void OnDestroy()
    {
    }

    public void Activated(int pinID)
    {
      switch (pinID)
      {
        case 1:
          this.CheckAnimation();
          break;
        case 10:
          this.ConfirmEnhance();
          break;
        case 11:
          if (Object.op_Implicit((Object) this.mRuneDrawEnhancePercentage) && this.mRuneDrawEnhancePercentage.IsCanUseGauge(this.mRuneDataCurr))
          {
            this.mIsUseEnforceGauge = !this.mIsUseEnforceGauge;
            this.mRuneDrawEnhancePercentage.SetDrawParam(this.mRuneDataCurr, this.mIsUseEnforceGauge);
            break;
          }
          UIUtility.SystemMessage(LocalizedText.Get("sys.RUNE_IS_NOT_USE_ENFORCEGAUGE"), (UIUtility.DialogResultEvent) (yes_button => {}), (GameObject) null, false, -1);
          break;
        case 12:
          this.EnhanceButtonRefresh();
          break;
        case 110:
          if (Object.op_Implicit((Object) this.mRuneManager))
          {
            this.mIsUseEnforceGauge = false;
            this.mRuneManager.RefreshRuneEnhanceFinished();
            FlowNode_ReqRuneEnhance.Clear();
          }
          FlowNode_GameObject.ActivateOutputLinks((Component) this, 120);
          break;
        case 200:
          if (Object.op_Implicit((Object) this.mRuneManager))
            this.mRuneManager.SelectedRuneAsEquip(this.mRuneDataCurr);
          FlowNode_GameObject.ActivateOutputLinks((Component) this, 1000);
          break;
      }
    }

    public void Setup(
      RuneManager manager,
      BindRuneData before_rune_data,
      BindRuneData after_rune_data,
      float _before_gauge)
    {
      this.mRuneManager = manager;
      this.mRuneDataBefore = before_rune_data;
      this.mRuneDataCurr = after_rune_data;
      this.mIsEnhanceSuccess = FlowNode_ReqRuneEnhance.IsResultSuccess();
      this.mIsUseEnforceGauge = FlowNode_ReqRuneEnhance.IsUseEnforceGauge();
      RuneData rune1 = this.mRuneDataBefore.Rune;
      RuneData rune2 = this.mRuneDataCurr.Rune;
      if (rune1 == null || rune2 == null)
        return;
      int add_level = rune2.EnhanceNum - rune1.EnhanceNum;
      bool isEvoNext = rune2.IsEvoNext;
      if (Object.op_Implicit((Object) this.mSwitchObj_NextEnhance))
        this.mSwitchObj_NextEnhance.SetActive(!isEvoNext);
      if (Object.op_Implicit((Object) this.mSwitchObj_NextEvo))
        this.mSwitchObj_NextEvo.SetActive(isEvoNext);
      if (Object.op_Implicit((Object) this.mRuneIcon))
        this.mRuneIcon.Setup(this.mRuneDataCurr, false);
      if (Object.op_Implicit((Object) this.mRuneDrawEnhanceLevel))
        this.mRuneDrawEnhanceLevel.SetDrawParam(this.mRuneDataBefore, add_level);
      if (Object.op_Implicit((Object) this.mRuneDrawEnhancedBaseState))
        this.mRuneDrawEnhancedBaseState.SetDrawParam(this.mRuneDataBefore, this.mRuneDataCurr);
      if (Object.op_Implicit((Object) this.mRuneDrawCost))
        this.mRuneDrawCost.SetDrawParam(this.mRuneDataCurr.EnhanceCost, 0);
      if (Object.op_Implicit((Object) this.mRuneDrawEnhanceEffect))
        this.mRuneDrawEnhanceEffect.SetDrawParam(this.mIsEnhanceSuccess);
      if (Object.op_Implicit((Object) this.mRuneDrawEnhancePercentage))
      {
        this.mRuneDrawEnhancePercentage.SetDrawParam(this.mRuneDataCurr, this.mIsUseEnforceGauge);
        this.mRuneDrawEnhancePercentage.SetupGaugeAnim(_before_gauge);
      }
      this.Refresh();
    }

    public void Refresh()
    {
      if (Object.op_Implicit((Object) this.mRuneIcon))
        this.mRuneIcon.Refresh();
      if (Object.op_Implicit((Object) this.mRuneDrawEnhanceLevel))
        this.mRuneDrawEnhanceLevel.Refresh();
      if (Object.op_Implicit((Object) this.mRuneDrawEnhancedBaseState))
        this.mRuneDrawEnhancedBaseState.Refresh();
      if (Object.op_Implicit((Object) this.mRuneDrawCost))
        this.mRuneDrawCost.Refresh();
      if (Object.op_Implicit((Object) this.mRuneDrawEnhanceEffect))
        this.mRuneDrawEnhanceEffect.Refresh();
      if (Object.op_Implicit((Object) this.mRuneDrawEnhancePercentage))
        this.mRuneDrawEnhancePercentage.Refresh();
      if (!Object.op_Implicit((Object) this.mEnhanceButton) || this.mRuneDataCurr == null)
        return;
      RuneCost enhanceCost = this.mRuneDataCurr.EnhanceCost;
      if (enhanceCost == null)
        return;
      ((Selectable) this.mEnhanceButton).set_interactable(enhanceCost.IsPlayerAmountEnough());
    }

    private void EnhanceButtonRefresh()
    {
      if (!Object.op_Implicit((Object) this.mEnhanceButton) || this.mRuneDataCurr == null)
        return;
      RuneCost enhanceCost = this.mRuneDataCurr.EnhanceCost;
      if (enhanceCost == null)
        return;
      ((Selectable) this.mEnhanceButton).set_interactable(enhanceCost.IsPlayerAmountEnough());
    }

    public void ConfirmEnhance()
    {
      if (Object.op_Equality((Object) this.mRuneManager, (Object) null))
        return;
      if (this.mIsUseEnforceGauge)
        UIUtility.ConfirmBox(LocalizedText.Get("sys.RUNE_ENHANCE_GAUGE_CONFIRM"), (UIUtility.DialogResultEvent) (yes_button =>
        {
          FlowNode_ReqRuneEnhance.SetTarget(this.mRuneDataCurr, this.mIsUseEnforceGauge);
          FlowNode_GameObject.ActivateOutputLinks((Component) this, 100);
        }), (UIUtility.DialogResultEvent) null, (GameObject) null, false, -1, (string) null, (string) null);
      else
        UIUtility.ConfirmBox(LocalizedText.Get("sys.RUNE_ENHANCE_NOGAUGE_CONFIRM"), (UIUtility.DialogResultEvent) (yes_button =>
        {
          FlowNode_ReqRuneEnhance.SetTarget(this.mRuneDataCurr, this.mIsUseEnforceGauge);
          FlowNode_GameObject.ActivateOutputLinks((Component) this, 100);
        }), (UIUtility.DialogResultEvent) null, (GameObject) null, false, -1, (string) null, (string) null);
    }

    private void CheckAnimation()
    {
      if (Object.op_Equality((Object) this.mRuneDrawEnhanceEffect, (Object) null))
        FlowNode_GameObject.ActivateOutputLinks((Component) this, 2);
      if (this.mRuneDrawEnhanceEffect.IsEnhanceSuccess)
        FlowNode_GameObject.ActivateOutputLinks((Component) this, 2);
      else
        FlowNode_GameObject.ActivateOutputLinks((Component) this, 3);
    }
  }
}
