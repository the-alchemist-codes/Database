﻿// Decompiled with JetBrains decompiler
// Type: SRPG.GalleryItem
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

namespace SRPG
{
  public class GalleryItem : MonoBehaviour
  {
    [SerializeField]
    private Button Button;
    [SerializeField]
    private GameObject UnknownImage;

    public GalleryItem()
    {
      base.\u002Ector();
    }

    public void SetAvailable(bool isAvailable)
    {
      if (Object.op_Inequality((Object) this.Button, (Object) null))
        ((Selectable) this.Button).set_interactable(isAvailable);
      if (!Object.op_Inequality((Object) this.UnknownImage, (Object) null))
        return;
      this.UnknownImage.SetActive(!isAvailable);
    }
  }
}
