﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqTrophyStarMission
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using Gsc.Network.Encoding;
using MessagePack;
using System;

namespace SRPG
{
  public class ReqTrophyStarMission : WebAPI
  {
    public ReqTrophyStarMission(
      SRPG.Network.ResponseCallback response,
      EncodingTypes.ESerializeCompressMethod serializeCompressMethod)
    {
      this.name = "trophy/star_mission";
      this.body = WebAPI.GetRequestString(string.Empty);
      this.callback = response;
      this.serializeCompressMethod = serializeCompressMethod;
    }

    [MessagePackObject(true)]
    [Serializable]
    public class StarMission
    {
      public ReqTrophyStarMission.StarMission.Info daily;
      public ReqTrophyStarMission.StarMission.Info weekly;

      [MessagePackObject(true)]
      [Serializable]
      public class Info
      {
        public string iname;
        public int ymd;
        public int star_num;
        public int[] rewards;
      }
    }

    [MessagePackObject(true)]
    [Serializable]
    public class Response
    {
      public ReqTrophyStarMission.StarMission star_mission;
    }
  }
}
