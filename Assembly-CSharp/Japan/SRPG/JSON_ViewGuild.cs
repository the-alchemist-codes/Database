﻿// Decompiled with JetBrains decompiler
// Type: SRPG.JSON_ViewGuild
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using MessagePack;
using System;

namespace SRPG
{
  [MessagePackObject(true)]
  [Serializable]
  public class JSON_ViewGuild
  {
    public int id;
    public string name;
    public string award_id;
    public int level;
    public int count;
    public int max_count;
    public string guild_master;
    public long created_at;
  }
}
