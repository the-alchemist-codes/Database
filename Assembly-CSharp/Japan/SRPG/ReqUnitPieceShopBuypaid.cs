﻿// Decompiled with JetBrains decompiler
// Type: SRPG.ReqUnitPieceShopBuypaid
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using Gsc.Network.Encoding;
using MessagePack;
using System;

namespace SRPG
{
  public class ReqUnitPieceShopBuypaid : WebAPI
  {
    public ReqUnitPieceShopBuypaid(
      string shopIname,
      string iname,
      int buynum,
      SRPG.Network.ResponseCallback response,
      EncodingTypes.ESerializeCompressMethod compress)
    {
      this.name = "shop/unitpiece/buy";
      this.body = WebAPI.GetRequestString<ReqUnitPieceShopBuypaid.RequestParam>(new ReqUnitPieceShopBuypaid.RequestParam()
      {
        shop_iname = shopIname,
        iname = iname,
        buynum = buynum
      });
      this.callback = response;
      this.serializeCompressMethod = compress;
    }

    [Serializable]
    public class RequestParam
    {
      public string shop_iname;
      public string iname;
      public int buynum;
    }

    [MessagePackObject(true)]
    [Serializable]
    public class Response
    {
      public Json_Item[] items;
      public Json_UnitPieceShopItem[] shopitems;
      public JSON_TrophyProgress[] trophyprogs;
      public JSON_TrophyProgress[] bingoprogs;
    }
  }
}
