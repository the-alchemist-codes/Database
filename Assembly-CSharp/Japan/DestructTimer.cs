﻿// Decompiled with JetBrains decompiler
// Type: DestructTimer
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

public class DestructTimer : MonoBehaviour
{
  public float Timer;

  public DestructTimer()
  {
    base.\u002Ector();
  }

  private void Update()
  {
    this.Timer -= Time.get_deltaTime();
    if ((double) this.Timer > 0.0)
      return;
    Object.Destroy((Object) ((Component) this).get_gameObject());
  }
}
