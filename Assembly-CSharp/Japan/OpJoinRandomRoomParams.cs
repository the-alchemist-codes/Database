﻿// Decompiled with JetBrains decompiler
// Type: OpJoinRandomRoomParams
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using ExitGames.Client.Photon;

internal class OpJoinRandomRoomParams
{
  public Hashtable ExpectedCustomRoomProperties;
  public byte ExpectedMaxPlayers;
  public MatchmakingMode MatchingType;
  public TypedLobby TypedLobby;
  public string SqlLobbyFilter;
  public string[] ExpectedUsers;
}
