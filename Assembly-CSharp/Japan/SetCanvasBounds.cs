﻿// Decompiled with JetBrains decompiler
// Type: SetCanvasBounds
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

public class SetCanvasBounds : MonoBehaviour
{
  [SerializeField]
  private bool IgnoreApplySafeAreaFlag;
  private static Vector2 add2DFlamePos;
  private static SetCanvasBounds.ScreenInfo m_CachedScreenInfo;
  public RectTransform panel;
  private Rect lastSafeArea;

  public SetCanvasBounds()
  {
    base.\u002Ector();
  }

  private static SetCanvasBounds.ScreenInfo CachedScreenInfo
  {
    get
    {
      if (SetCanvasBounds.m_CachedScreenInfo == null)
      {
        SetCanvasBounds.m_CachedScreenInfo = new SetCanvasBounds.ScreenInfo();
        SetCanvasBounds.m_CachedScreenInfo.Initialize();
      }
      return SetCanvasBounds.m_CachedScreenInfo;
    }
  }

  public static bool HasSafeArea
  {
    get
    {
      return SetCanvasBounds.CachedScreenInfo.HasSafeArea;
    }
  }

  public static bool IsWideScreen
  {
    get
    {
      return SetCanvasBounds.CachedScreenInfo.IsWide;
    }
  }

  public static void ClearScreenInfo()
  {
    SetCanvasBounds.m_CachedScreenInfo = (SetCanvasBounds.ScreenInfo) null;
  }

  public static Vector2 GetAddFrame()
  {
    Rect safeArea = SetCanvasBounds.GetSafeArea(false);
    int num = (Screen.get_width() - Screen.get_height() / 9 * 16) / 2;
    SetCanvasBounds.add2DFlamePos.x = (__Null) (double) (num - (int) ((Rect) ref safeArea).get_x());
    if (SetCanvasBounds.add2DFlamePos.x < 0.0)
      SetCanvasBounds.add2DFlamePos.x = (__Null) 0.0;
    return SetCanvasBounds.add2DFlamePos;
  }

  private static int PointToPixel(int pt, int retina)
  {
    return pt * retina;
  }

  private static Rect CalcSafeAreaPointToPixel(RectOffset edgeInsets, int retina)
  {
    Rect rect;
    ((Rect) ref rect).\u002Ector(0.0f, 0.0f, (float) Screen.get_width(), (float) Screen.get_height());
    edgeInsets.set_left(SetCanvasBounds.PointToPixel(edgeInsets.get_left(), retina));
    edgeInsets.set_right(SetCanvasBounds.PointToPixel(edgeInsets.get_right(), retina));
    edgeInsets.set_top(SetCanvasBounds.PointToPixel(edgeInsets.get_top(), retina));
    edgeInsets.set_bottom(SetCanvasBounds.PointToPixel(edgeInsets.get_bottom(), retina));
    return edgeInsets.Remove(rect);
  }

  public static Rect GetSafeArea(bool bgScale = false)
  {
    Rect rect;
    ((Rect) ref rect).\u002Ector(0.0f, 0.0f, (float) Screen.get_width(), (float) Screen.get_height());
    float num1 = 1.778667f;
    if (bgScale)
    {
      ((Rect) ref rect).\u002Ector(0.0f, 0.0f, (float) Screen.get_width(), (float) Screen.get_height());
      if ((double) num1 < (double) (Screen.get_width() / Screen.get_height()))
      {
        float num2 = (float) (((double) Screen.get_width() - (double) Screen.get_height() / 750.0 * 1334.0) / 2.0);
        ref Rect local1 = ref rect;
        ((Rect) ref local1).set_x(((Rect) ref local1).get_x() + num2 / 2f);
        ref Rect local2 = ref rect;
        ((Rect) ref local2).set_y(((Rect) ref local2).get_y() + num2 / 4f);
        ref Rect local3 = ref rect;
        ((Rect) ref local3).set_width(((Rect) ref local3).get_width() - num2 / 2f);
        ref Rect local4 = ref rect;
        ((Rect) ref local4).set_height(((Rect) ref local4).get_height() - num2 / 4f);
      }
    }
    return rect;
  }

  public static float CalcCanvasBoundsScale()
  {
    Rect safeArea = SetCanvasBounds.GetSafeArea(false);
    return ((Rect) ref safeArea).get_width() / (float) Screen.get_width();
  }

  private void Start()
  {
    if (this.IgnoreApplySafeAreaFlag)
      return;
    this.ApplySafeAreaScale(SetCanvasBounds.GetSafeArea(false));
  }

  private void ApplySafeAreaScale(Rect area)
  {
    if (Object.op_Inequality((Object) this.panel, (Object) null))
    {
      float num = ((Rect) ref area).get_width() / (float) Screen.get_width();
      Vector3 vector3;
      ((Vector3) ref vector3).\u002Ector(num, num, num);
      if (SetCanvasBounds.CachedScreenInfo.HasSafeArea)
      {
        if (SetCanvasBounds.CachedScreenInfo.IsWide)
        {
          this.panel.set_sizeDelta(new Vector2(0.0f, 750f));
          this.panel.set_anchorMin(new Vector2(0.0f, 1f));
          this.panel.set_anchorMax(new Vector2(1f, 1f));
          this.panel.set_pivot(new Vector2(0.5f, 1f));
        }
        else
        {
          this.panel.set_sizeDelta(new Vector2(0.0f, 750f));
          this.panel.set_anchorMin(new Vector2(0.0f, 0.5f));
          this.panel.set_anchorMax(new Vector2(1f, 0.5f));
          this.panel.set_pivot(new Vector2(0.5f, 0.5f));
        }
      }
      else if (SetCanvasBounds.CachedScreenInfo.IsWide)
      {
        this.panel.set_sizeDelta(new Vector2(0.0f, 750f));
        this.panel.set_anchorMin(new Vector2(0.0f, 0.5f));
        this.panel.set_anchorMax(new Vector2(1f, 0.5f));
        this.panel.set_pivot(new Vector2(0.5f, 0.5f));
        this.panel.set_offsetMin(new Vector2(0.0f, (float) this.panel.get_offsetMin().y));
        this.panel.set_offsetMax(new Vector2(0.0f, (float) this.panel.get_offsetMax().y));
      }
      else
      {
        this.panel.set_sizeDelta(new Vector2(0.0f, 750f));
        this.panel.set_anchorMin(new Vector2(0.0f, 0.5f));
        this.panel.set_anchorMax(new Vector2(1f, 0.5f));
        this.panel.set_pivot(new Vector2(0.5f, 0.5f));
      }
      ((Transform) this.panel).set_localScale(vector3);
    }
    this.lastSafeArea = area;
  }

  public static Vector2 GetScreenSize()
  {
    Rect viewportRect = SetCanvasBounds.CachedScreenInfo.ViewportRect;
    return ((Rect) ref viewportRect).get_size();
  }

  public static Rect GetCameraViewport()
  {
    return SetCanvasBounds.CachedScreenInfo.IsWide ? new Rect(0.0f, 0.0f, 1f, 1f) : SetCanvasBounds.CachedScreenInfo.NormalizedViewportRect;
  }

  private class ScreenInfo
  {
    private Rect m_SafeArea;
    private Rect m_NormalizedViewportRect;
    private Rect m_ViewportRect;
    private bool m_HasSafeArea;
    private bool m_IsWide;

    public Rect SafeArea
    {
      get
      {
        return this.m_SafeArea;
      }
      set
      {
        this.m_SafeArea = value;
        this.m_HasSafeArea = (double) ((Rect) ref this.m_SafeArea).get_width() != (double) Screen.get_width() || (double) ((Rect) ref this.m_SafeArea).get_height() != (double) Screen.get_height();
      }
    }

    public Rect NormalizedViewportRect
    {
      get
      {
        return this.m_NormalizedViewportRect;
      }
      set
      {
        this.m_NormalizedViewportRect = value;
        this.m_ViewportRect = (Rect) null;
        ((Rect) ref this.m_ViewportRect).set_position(new Vector2((float) Screen.get_width() * ((Rect) ref this.m_NormalizedViewportRect).get_x(), (float) Screen.get_height() * ((Rect) ref this.m_NormalizedViewportRect).get_y()));
        ((Rect) ref this.m_ViewportRect).set_size(new Vector2((float) Screen.get_width() * (float) ((Rect) ref this.m_NormalizedViewportRect).get_size().x, (float) Screen.get_height() * (float) ((Rect) ref this.m_NormalizedViewportRect).get_size().y));
      }
    }

    public Rect ViewportRect
    {
      get
      {
        return this.m_ViewportRect;
      }
    }

    public bool HasSafeArea
    {
      get
      {
        return this.m_HasSafeArea;
      }
    }

    public bool IsWide
    {
      get
      {
        return this.m_IsWide;
      }
      set
      {
        this.m_IsWide = value;
      }
    }

    public Vector2 ScreenSize
    {
      get
      {
        return ((Rect) ref this.m_SafeArea).get_size();
      }
    }

    public void Initialize()
    {
      Rect safeArea = SetCanvasBounds.GetSafeArea(false);
      ((Rect) ref safeArea).get_size();
      this.SafeArea = safeArea;
      float num1 = (float) Screen.get_width() / (float) Screen.get_height() / 1.778667f;
      Vector2 zero1 = Vector2.get_zero();
      Vector2 zero2 = Vector2.get_zero();
      if (1.0 > (double) num1)
      {
        ((Vector2) ref zero1).\u002Ector(0.0f, (float) ((1.0 - (double) num1) / 2.0));
        ((Vector2) ref zero2).\u002Ector(1f, num1);
        this.m_IsWide = false;
      }
      else
      {
        float num2 = 1f / num1;
        ((Vector2) ref zero1).\u002Ector((float) ((1.0 - (double) num2) / 2.0), 0.0f);
        ((Vector2) ref zero2).\u002Ector(num2, 1f);
        this.m_IsWide = true;
      }
      this.NormalizedViewportRect = new Rect(zero1, zero2);
    }
  }
}
