﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Tasks.ITask
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections;

namespace Gsc.Tasks
{
  public interface ITask
  {
    bool isDone { get; }

    void OnStart();

    IEnumerator Run();

    void OnFinish();
  }
}
