﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Network.YieldCallback`1
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections;

namespace Gsc.Network
{
  public delegate IEnumerator YieldCallback<TResponse>(TResponse response);
}
