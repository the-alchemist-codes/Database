﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Network.Data.IEntity
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace Gsc.Network.Data
{
  public interface IEntity : IObject
  {
    string pk { get; }

    void Update();

    void ResolveRefs();

    IEntity Clone();
  }
}
