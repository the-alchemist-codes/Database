﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Network.IWebTaskBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using Gsc.Tasks;
using System.Collections;

namespace Gsc.Network
{
  public interface IWebTaskBase : ITask, IEnumerator
  {
    bool isBreak { get; }

    void Break();
  }
}
