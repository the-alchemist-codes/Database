﻿// Decompiled with JetBrains decompiler
// Type: Gsc.DOM.Generic.Document
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace Gsc.DOM.Generic
{
  public class Document : IDocument, IDisposable
  {
    private readonly Value root;

    public Document(Document document, ref Value root)
    {
      this.root = root;
    }

    public Value Root
    {
      get
      {
        return this.root;
      }
    }

    IValue IDocument.Root
    {
      get
      {
        return (IValue) this.root;
      }
    }

    ~Document()
    {
      this.Dispose();
    }

    public void Dispose()
    {
    }
  }
}
