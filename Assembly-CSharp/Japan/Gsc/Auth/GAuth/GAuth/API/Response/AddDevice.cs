﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Auth.GAuth.GAuth.API.Response.AddDevice
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using Gsc.Auth.GAuth.GAuth.API.Generic;
using Gsc.DOM;
using Gsc.Network;

namespace Gsc.Auth.GAuth.GAuth.API.Response
{
  public class AddDevice : GAuthResponse<AddDevice>
  {
    public AddDevice(WebInternalResponse response)
    {
      using (IDocument document = this.Parse(response))
        this.IsSucceeded = document.Root["is_succeeded"].ToBool();
    }

    public bool IsSucceeded { get; private set; }
  }
}
