﻿// Decompiled with JetBrains decompiler
// Type: Gsc.Auth.GAuth.GAuth.API.Response.Migrate
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using Gsc.Auth.GAuth.GAuth.API.Generic;
using Gsc.DOM;
using Gsc.Network;

namespace Gsc.Auth.GAuth.GAuth.API.Response
{
  public class Migrate : GAuthResponse<Migrate>
  {
    public Migrate(WebInternalResponse response)
    {
      using (IDocument document = this.Parse(response))
        this.OldDeviceId = document.Root["old_device_id"].ToString();
    }

    public string OldDeviceId { get; private set; }
  }
}
