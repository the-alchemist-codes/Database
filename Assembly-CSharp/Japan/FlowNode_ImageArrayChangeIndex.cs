﻿// Decompiled with JetBrains decompiler
// Type: FlowNode_ImageArrayChangeIndex
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

[FlowNode.NodeType("UI/ImageArrayChangeIndex", 32741)]
[FlowNode.Pin(0, "Set", FlowNode.PinTypes.Input, 0)]
[FlowNode.Pin(10, "Out", FlowNode.PinTypes.Output, 10)]
public class FlowNode_ImageArrayChangeIndex : FlowNode
{
  [FlowNode.ShowInInfo]
  public int Index;
  [FlowNode.ShowInInfo]
  [FlowNode.DropTarget(typeof (GameObject), true)]
  public GameObject Target;

  public override void OnActivate(int pinID)
  {
    ImageArray component = (ImageArray) (!Object.op_Inequality((Object) this.Target, (Object) null) ? ((Component) this).get_gameObject() : this.Target).GetComponent<ImageArray>();
    if (Object.op_Inequality((Object) component, (Object) null))
      component.ImageIndex = this.Index;
    this.ActivateOutputLinks(10);
  }
}
