﻿// Decompiled with JetBrains decompiler
// Type: RegularLocalNotificationParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

public class RegularLocalNotificationParam
{
  public static readonly string CATEGORY_MORNING = "local_ordinary_morning";
  public static readonly string CATEGORY_NOON = "local_ordinary_noon";
  public static readonly string CATEGORY_AFTERNOON = "local_ordinary_afternoon";
  public string mMessage = string.Empty;
  public string mCategory = string.Empty;
  public int mHour;
  public int mMinute;
  public int mSecond;

  public RegularLocalNotificationParam(
    string message,
    string category,
    int hour,
    int min,
    int sec)
  {
    this.mMessage = message;
    this.mCategory = category;
    this.mHour = hour;
    this.mMinute = min;
    this.mSecond = sec;
  }
}
