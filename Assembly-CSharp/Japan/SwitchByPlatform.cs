﻿// Decompiled with JetBrains decompiler
// Type: SwitchByPlatform
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

public class SwitchByPlatform : MonoBehaviour
{
  [SerializeField]
  public RuntimePlatform[] hides;

  public SwitchByPlatform()
  {
    base.\u002Ector();
  }

  private void Start()
  {
    foreach (int hide in this.hides)
    {
      if (Application.get_platform() == (RuntimePlatform) hide)
        ((Component) this).get_gameObject().SetActive(false);
    }
  }
}
