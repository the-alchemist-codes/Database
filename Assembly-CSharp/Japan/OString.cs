﻿// Decompiled with JetBrains decompiler
// Type: OString
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using CodeStage.AntiCheat.ObscuredTypes;
using MessagePack;

[MessagePackObject(true)]
public struct OString
{
  private ObscuredString value;

  public OString(string value)
  {
    this.value = (ObscuredString) value;
  }

  public static implicit operator OString(string value)
  {
    return new OString(value);
  }

  public static implicit operator string(OString value)
  {
    return (string) value.value;
  }

  public override string ToString()
  {
    return this.value.ToString();
  }
}
