﻿// Decompiled with JetBrains decompiler
// Type: ScreenBlankSpaceFrame
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.Collections;
using System.Diagnostics;
using UnityEngine;
using UnityEngine.EventSystems;

public class ScreenBlankSpaceFrame : UIBehaviour
{
  [SerializeField]
  private RectTransform m_ImageLeft;
  [SerializeField]
  private RectTransform m_ImageRight;
  [SerializeField]
  private RectTransform m_ImageTop;
  [SerializeField]
  private RectTransform m_ImageBottom;
  [SerializeField]
  private RectTransform m_CanvasBoundsPanel;
  private Coroutine m_AdjustCoroutine;
  private DrivenRectTransformTracker m_Tracker;

  public ScreenBlankSpaceFrame()
  {
    base.\u002Ector();
  }

  protected virtual void Start()
  {
    base.Start();
    this.AsyncAdjustRecttransformSize();
  }

  protected virtual void OnRectTransformDimensionsChange()
  {
    this.AsyncAdjustRecttransformSize();
  }

  protected virtual void OnDisable()
  {
    ((DrivenRectTransformTracker) ref this.m_Tracker).Clear();
    base.OnDisable();
  }

  private void AsyncAdjustRecttransformSize()
  {
    if (!((Component) this).get_gameObject().get_activeInHierarchy())
      return;
    if (Application.get_isPlaying())
    {
      if (this.m_AdjustCoroutine != null)
        ((MonoBehaviour) this).StopCoroutine(this.m_AdjustCoroutine);
      this.m_AdjustCoroutine = ((MonoBehaviour) this).StartCoroutine(this.StartAdjustSize());
    }
    else
    {
      SetCanvasBounds.ClearScreenInfo();
      this.SetScreenOutFrameSize();
    }
  }

  [DebuggerHidden]
  private IEnumerator StartAdjustSize()
  {
    // ISSUE: object of a compiler-generated type is created
    return (IEnumerator) new ScreenBlankSpaceFrame.\u003CStartAdjustSize\u003Ec__Iterator0()
    {
      \u0024this = this
    };
  }

  private void SetScreenOutFrameSize()
  {
    Vector2 screenSize = SetCanvasBounds.GetScreenSize();
    Vector2 vector2;
    ((Vector2) ref vector2).\u002Ector((float) Screen.get_width(), (float) Screen.get_height());
    DrivenTransformProperties transformProperties = (DrivenTransformProperties) 65286;
    ((DrivenRectTransformTracker) ref this.m_Tracker).Clear();
    if (Object.op_Inequality((Object) this.m_ImageLeft, (Object) null))
      ((DrivenRectTransformTracker) ref this.m_Tracker).Add((Object) this, this.m_ImageLeft, transformProperties);
    if (Object.op_Inequality((Object) this.m_ImageRight, (Object) null))
      ((DrivenRectTransformTracker) ref this.m_Tracker).Add((Object) this, this.m_ImageRight, transformProperties);
    if (Object.op_Inequality((Object) this.m_ImageTop, (Object) null))
      ((DrivenRectTransformTracker) ref this.m_Tracker).Add((Object) this, this.m_ImageTop, transformProperties);
    if (Object.op_Inequality((Object) this.m_ImageBottom, (Object) null))
      ((DrivenRectTransformTracker) ref this.m_Tracker).Add((Object) this, this.m_ImageBottom, transformProperties);
    if (Vector2.op_Equality(vector2, screenSize))
    {
      GameUtility.SetGameObjectActive((Component) this.m_ImageLeft, false);
      GameUtility.SetGameObjectActive((Component) this.m_ImageRight, false);
      GameUtility.SetGameObjectActive((Component) this.m_ImageTop, false);
      GameUtility.SetGameObjectActive((Component) this.m_ImageBottom, false);
    }
    else if (SetCanvasBounds.IsWideScreen)
    {
      GameUtility.SetGameObjectActive((Component) this.m_ImageLeft, false);
      GameUtility.SetGameObjectActive((Component) this.m_ImageRight, false);
      GameUtility.SetGameObjectActive((Component) this.m_ImageTop, false);
      GameUtility.SetGameObjectActive((Component) this.m_ImageBottom, false);
    }
    else
    {
      GameUtility.SetGameObjectActive((Component) this.m_ImageLeft, false);
      GameUtility.SetGameObjectActive((Component) this.m_ImageRight, false);
      GameUtility.SetGameObjectActive((Component) this.m_ImageTop, true);
      GameUtility.SetGameObjectActive((Component) this.m_ImageBottom, true);
      this.SetAnchorAndPivot(this.m_ImageTop, (RectTransform.Edge) 2);
      this.SetRect(this.m_ImageTop, (RectTransform.Edge) 2);
      this.SetAnchorAndPivot(this.m_ImageBottom, (RectTransform.Edge) 3);
      this.SetRect(this.m_ImageBottom, (RectTransform.Edge) 3);
    }
  }

  private void SetRect(RectTransform rt, RectTransform.Edge edge)
  {
    if (Object.op_Equality((Object) rt, (Object) null))
      return;
    Rect cameraViewport = SetCanvasBounds.GetCameraViewport();
    Vector2 zero1 = Vector2.get_zero();
    Vector2 zero2 = Vector2.get_zero();
    Rect rect1 = (Rect) null;
    switch ((int) edge)
    {
      case 0:
        ref Vector2 local1 = ref zero1;
        double x1 = (double) ((Rect) ref cameraViewport).get_x();
        Rect rect2 = this.m_CanvasBoundsPanel.get_rect();
        // ISSUE: variable of the null type
        __Null x2 = ((Rect) ref rect2).get_size().x;
        double num1 = x1 * x2;
        local1.x = (__Null) num1;
        zero1.y = (__Null) 0.0;
        ref Vector2 local2 = ref zero2;
        double x3 = (double) ((Rect) ref cameraViewport).get_x();
        Rect rect3 = this.m_CanvasBoundsPanel.get_rect();
        // ISSUE: variable of the null type
        __Null x4 = ((Rect) ref rect3).get_size().x;
        double num2 = x3 * x4;
        local2.x = (__Null) num2;
        ref Vector2 local3 = ref zero2;
        Rect rect4 = this.m_CanvasBoundsPanel.get_rect();
        // ISSUE: variable of the null type
        __Null y1 = ((Rect) ref rect4).get_size().y;
        local3.y = y1;
        ((Rect) ref rect1).set_position(zero1);
        ((Rect) ref rect1).set_size(zero2);
        rect1 = new RectOffset(0, 0, 1, 1).Add(rect1);
        break;
      case 1:
        ref Vector2 local4 = ref zero1;
        double xMax = (double) ((Rect) ref cameraViewport).get_xMax();
        Rect rect5 = this.m_CanvasBoundsPanel.get_rect();
        // ISSUE: variable of the null type
        __Null x5 = ((Rect) ref rect5).get_size().x;
        double num3 = xMax * x5;
        local4.x = (__Null) num3;
        zero1.y = (__Null) 0.0;
        ref Vector2 local5 = ref zero2;
        double x6 = (double) ((Rect) ref cameraViewport).get_x();
        Rect rect6 = this.m_CanvasBoundsPanel.get_rect();
        // ISSUE: variable of the null type
        __Null x7 = ((Rect) ref rect6).get_size().x;
        double num4 = x6 * x7;
        local5.x = (__Null) num4;
        ref Vector2 local6 = ref zero2;
        Rect rect7 = this.m_CanvasBoundsPanel.get_rect();
        // ISSUE: variable of the null type
        __Null y2 = ((Rect) ref rect7).get_size().y;
        local6.y = y2;
        ((Rect) ref rect1).set_position(zero1);
        ((Rect) ref rect1).set_size(zero2);
        rect1 = new RectOffset(0, 0, 1, 1).Add(rect1);
        break;
      case 2:
        ref Vector2 local7 = ref zero1;
        double x8 = (double) ((Rect) ref cameraViewport).get_x();
        Rect rect8 = this.m_CanvasBoundsPanel.get_rect();
        // ISSUE: variable of the null type
        __Null x9 = ((Rect) ref rect8).get_size().x;
        double num5 = x8 * x9;
        local7.x = (__Null) num5;
        ref Vector2 local8 = ref zero1;
        double y3 = (double) ((Rect) ref cameraViewport).get_y();
        Rect rect9 = this.m_CanvasBoundsPanel.get_rect();
        // ISSUE: variable of the null type
        __Null y4 = ((Rect) ref rect9).get_size().y;
        double num6 = y3 * y4 * -1.0;
        local8.y = (__Null) num6;
        ref Vector2 local9 = ref zero2;
        double width1 = (double) ((Rect) ref cameraViewport).get_width();
        Rect rect10 = this.m_CanvasBoundsPanel.get_rect();
        // ISSUE: variable of the null type
        __Null x10 = ((Rect) ref rect10).get_size().x;
        double num7 = width1 * x10;
        local9.x = (__Null) num7;
        ref Vector2 local10 = ref zero2;
        double y5 = (double) ((Rect) ref cameraViewport).get_y();
        Rect rect11 = this.m_CanvasBoundsPanel.get_rect();
        // ISSUE: variable of the null type
        __Null y6 = ((Rect) ref rect11).get_size().y;
        double num8 = y5 * y6;
        local10.y = (__Null) num8;
        ((Rect) ref rect1).set_position(zero1);
        ((Rect) ref rect1).set_size(zero2);
        rect1 = new RectOffset(1, 1, 0, 0).Add(rect1);
        break;
      case 3:
        ref Vector2 local11 = ref zero1;
        double x11 = (double) ((Rect) ref cameraViewport).get_x();
        Rect rect12 = this.m_CanvasBoundsPanel.get_rect();
        // ISSUE: variable of the null type
        __Null x12 = ((Rect) ref rect12).get_size().x;
        double num9 = x11 * x12;
        local11.x = (__Null) num9;
        ref Vector2 local12 = ref zero1;
        double yMax = (double) ((Rect) ref cameraViewport).get_yMax();
        Rect rect13 = this.m_CanvasBoundsPanel.get_rect();
        // ISSUE: variable of the null type
        __Null y7 = ((Rect) ref rect13).get_size().y;
        double num10 = yMax * y7 * -1.0;
        local12.y = (__Null) num10;
        ref Vector2 local13 = ref zero2;
        double width2 = (double) ((Rect) ref cameraViewport).get_width();
        Rect rect14 = this.m_CanvasBoundsPanel.get_rect();
        // ISSUE: variable of the null type
        __Null x13 = ((Rect) ref rect14).get_size().x;
        double num11 = width2 * x13;
        local13.x = (__Null) num11;
        ref Vector2 local14 = ref zero2;
        double y8 = (double) ((Rect) ref cameraViewport).get_y();
        Rect rect15 = this.m_CanvasBoundsPanel.get_rect();
        // ISSUE: variable of the null type
        __Null y9 = ((Rect) ref rect15).get_size().y;
        double num12 = y8 * y9;
        local14.y = (__Null) num12;
        ((Rect) ref rect1).set_position(zero1);
        ((Rect) ref rect1).set_size(zero2);
        rect1 = new RectOffset(1, 1, 0, 0).Add(rect1);
        break;
    }
    rt.set_anchoredPosition(((Rect) ref rect1).get_position());
    rt.set_sizeDelta(((Rect) ref rect1).get_size());
  }

  private void SetAnchorAndPivot(RectTransform rt, RectTransform.Edge edge)
  {
    if (Object.op_Equality((Object) rt, (Object) null))
      return;
    switch ((int) edge)
    {
      case 0:
        rt.set_pivot(new Vector2(1f, 1f));
        rt.set_anchorMin(new Vector2(0.0f, 1f));
        rt.set_anchorMax(new Vector2(0.0f, 1f));
        break;
      case 1:
        rt.set_pivot(new Vector2(0.0f, 1f));
        rt.set_anchorMin(new Vector2(0.0f, 1f));
        rt.set_anchorMax(new Vector2(0.0f, 1f));
        break;
      case 2:
        rt.set_pivot(new Vector2(0.0f, 0.0f));
        rt.set_anchorMin(new Vector2(0.0f, 1f));
        rt.set_anchorMax(new Vector2(0.0f, 1f));
        break;
      case 3:
        rt.set_pivot(new Vector2(0.0f, 1f));
        rt.set_anchorMin(new Vector2(0.0f, 1f));
        rt.set_anchorMax(new Vector2(0.0f, 1f));
        break;
    }
  }
}
