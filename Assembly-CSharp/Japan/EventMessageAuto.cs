﻿// Decompiled with JetBrains decompiler
// Type: EventMessageAuto
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using SRPG;
using UnityEngine;

public class EventMessageAuto : MonoBehaviour
{
  private GameObject mEnableObject;

  public EventMessageAuto()
  {
    base.\u002Ector();
  }

  private void Awake()
  {
    RectTransform component = (RectTransform) ((Component) this).GetComponent<RectTransform>();
    Rect safeArea = SetCanvasBounds.GetSafeArea(false);
    if ((double) (((Rect) ref safeArea).get_width() / (float) Screen.get_width()) < 1.0)
    {
      float num = (float) Screen.get_height() - ((Rect) ref safeArea).get_height();
      component.set_anchoredPosition(new Vector2((float) component.get_anchoredPosition().x, (float) component.get_anchoredPosition().y + num));
    }
    EventScript.OnAutoFlagChanged += new EventScript.AutoFlagChange(this.OnFlagChanged);
    this.enableObject.SetActive(EventScript.IsMessageAuto);
  }

  private GameObject enableObject
  {
    get
    {
      if (Object.op_Equality((Object) this.mEnableObject, (Object) null))
      {
        Transform transform = ((Component) this).get_transform().Find("Enable");
        if (Object.op_Equality((Object) transform, (Object) null))
          return (GameObject) null;
        this.mEnableObject = ((Component) transform).get_gameObject();
      }
      return this.mEnableObject;
    }
  }

  public void Start()
  {
  }

  public void OnClick()
  {
    EventScript.IsMessageAuto = !EventScript.IsMessageAuto;
  }

  private void OnFlagChanged(bool value)
  {
    if (!Object.op_Inequality((Object) this.enableObject, (Object) null))
      return;
    this.enableObject.SetActive(value);
  }

  public void OnDestroy()
  {
    EventScript.OnAutoFlagChanged -= new EventScript.AutoFlagChange(this.OnFlagChanged);
    EventScript.IsMessageAuto = false;
  }
}
