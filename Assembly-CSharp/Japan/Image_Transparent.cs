﻿// Decompiled with JetBrains decompiler
// Type: Image_Transparent
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

[AddComponentMenu("UI/Image (透明)")]
public class Image_Transparent : Image
{
  public Image_Transparent()
  {
    base.\u002Ector();
  }

  protected virtual void OnPopulateMesh(VertexHelper toFill)
  {
    if (Object.op_Inequality((Object) this.get_sprite(), (Object) null))
      base.OnPopulateMesh(toFill);
    else
      toFill.Clear();
  }
}
