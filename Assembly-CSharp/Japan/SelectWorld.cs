﻿// Decompiled with JetBrains decompiler
// Type: SelectWorld
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

[AddComponentMenu("UI/Drafts/SelectWorld")]
public class SelectWorld : UIDraft
{
  [UIDraft.AutoGenerated]
  public Button World_Item;
  [UIDraft.AutoGenerated]
  public Text Text_WorldName;
  [UIDraft.AutoGenerated]
  public Text Text_MenuTitle;
  [UIDraft.AutoGenerated]
  public Button Btn_Decide;
}
