﻿// Decompiled with JetBrains decompiler
// Type: QuestLobbyNewsUI
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

public class QuestLobbyNewsUI : MonoBehaviour
{
  [SerializeField]
  private QuestLobbyNews.QuestLobbyCategory mCategory;
  [SerializeField]
  private GameObject mBadgeRoot;
  [SerializeField]
  private Text mText;

  public QuestLobbyNewsUI()
  {
    base.\u002Ector();
  }

  private void Start()
  {
    if (!Object.op_Implicit((Object) this.mText) || !Object.op_Implicit((Object) this.mBadgeRoot))
      return;
    this.mBadgeRoot.SetActive(false);
    QuestLobbyNews questLobbyNews = QuestLobbyNews.FindQuestLobbyNews(this.mCategory);
    if (questLobbyNews == null || !questLobbyNews.isShow())
      return;
    this.mBadgeRoot.SetActive(true);
    this.mText.set_text(questLobbyNews.GetShowText());
  }
}
