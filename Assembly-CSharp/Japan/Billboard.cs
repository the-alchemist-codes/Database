﻿// Decompiled with JetBrains decompiler
// Type: Billboard
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

[AddComponentMenu("Rendering/Billboard")]
public class Billboard : MonoBehaviour
{
  public Billboard()
  {
    base.\u002Ector();
  }

  private void OnEnable()
  {
    CameraHook.AddPreCullEventListener(new CameraHook.PreCullEvent(this.PreCull));
  }

  private void OnDisable()
  {
    CameraHook.RemovePreCullEventListener(new CameraHook.PreCullEvent(this.PreCull));
  }

  private void PreCull(Camera camera)
  {
    Transform transform1 = ((Component) this).get_transform();
    Transform transform2 = ((Component) camera).get_transform();
    transform1.set_rotation(Quaternion.LookRotation(transform2.get_forward(), transform2.get_up()));
  }
}
