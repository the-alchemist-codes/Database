﻿// Decompiled with JetBrains decompiler
// Type: DefaultSceneRequest
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

public class DefaultSceneRequest : SceneRequest
{
  private bool mSceneActivated;
  private AsyncOperation mRequest;
  private bool mAdditive;

  public DefaultSceneRequest(AsyncOperation request, bool additive)
  {
    request.set_allowSceneActivation(false);
    this.mAdditive = additive;
    this.mRequest = request;
    AssetBundleUnloader.AddAsyncOperation(request);
  }

  public override bool ActivateScene()
  {
    if (this.mSceneActivated)
      return this.mSceneActivated;
    this.mRequest.set_allowSceneActivation(true);
    this.mSceneActivated = true;
    AssetManager.OnSceneActivate((SceneRequest) this);
    return true;
  }

  public override bool IsActivated
  {
    get
    {
      return this.mSceneActivated;
    }
  }

  public override bool isAdditive
  {
    get
    {
      return this.mAdditive;
    }
  }

  public override bool isDone
  {
    get
    {
      return this.mSceneActivated && this.mRequest.get_isDone();
    }
  }

  public override bool canBeActivated
  {
    get
    {
      return (double) this.mRequest.get_progress() >= 0.899999976158142;
    }
  }

  public override bool MoveNext()
  {
    return this.mRequest != null && !this.isDone;
  }

  public override object Current
  {
    get
    {
      return (object) null;
    }
  }

  public override float progress
  {
    get
    {
      return this.mRequest != null ? this.mRequest.get_progress() : 0.0f;
    }
  }
}
