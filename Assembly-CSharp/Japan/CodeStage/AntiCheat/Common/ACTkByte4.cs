﻿// Decompiled with JetBrains decompiler
// Type: CodeStage.AntiCheat.Common.ACTkByte4
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

namespace CodeStage.AntiCheat.Common
{
  [Serializable]
  public struct ACTkByte4
  {
    public byte b1;
    public byte b2;
    public byte b3;
    public byte b4;

    public void Shuffle()
    {
      byte b2 = this.b2;
      this.b2 = this.b3;
      this.b3 = b2;
    }

    public void UnShuffle()
    {
      byte b3 = this.b3;
      this.b3 = this.b2;
      this.b2 = b3;
    }
  }
}
