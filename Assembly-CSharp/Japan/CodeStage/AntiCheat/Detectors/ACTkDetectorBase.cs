﻿// Decompiled with JetBrains decompiler
// Type: CodeStage.AntiCheat.Detectors.ACTkDetectorBase
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using UnityEngine;
using UnityEngine.Events;

namespace CodeStage.AntiCheat.Detectors
{
  [AddComponentMenu("")]
  public abstract class ACTkDetectorBase : MonoBehaviour
  {
    protected const string ContainerName = "Anti-Cheat Toolkit Detectors";
    protected const string MenuPath = "Code Stage/Anti-Cheat Toolkit/";
    protected const string GameObjectMenuPath = "GameObject/Create Other/Code Stage/Anti-Cheat Toolkit/";
    protected static GameObject detectorsContainer;
    [Tooltip("Automatically start detector. Detection Event will be called on detection.")]
    public bool autoStart;
    [Tooltip("Detector will survive new level (scene) load if checked.")]
    public bool keepAlive;
    [Tooltip("Automatically dispose Detector after firing callback.")]
    public bool autoDispose;
    [SerializeField]
    protected UnityEvent detectionEvent;
    [SerializeField]
    protected bool detectionEventHasListener;
    protected bool started;
    protected bool isRunning;

    protected ACTkDetectorBase()
    {
      base.\u002Ector();
    }

    public event Action CheatDetected;

    public bool IsRunning
    {
      get
      {
        return this.isRunning;
      }
    }

    private void Start()
    {
      if (Object.op_Equality((Object) ACTkDetectorBase.detectorsContainer, (Object) null) && ((Object) ((Component) this).get_gameObject()).get_name() == "Anti-Cheat Toolkit Detectors")
        ACTkDetectorBase.detectorsContainer = ((Component) this).get_gameObject();
      if (!this.autoStart || this.started)
        return;
      this.StartDetectionAutomatically();
    }

    private void OnEnable()
    {
      this.ResumeDetector();
    }

    private void OnDisable()
    {
      this.PauseDetector();
    }

    private void OnApplicationQuit()
    {
      this.DisposeInternal();
    }

    protected virtual void OnDestroy()
    {
      this.StopDetectionInternal();
      if (((Component) this).get_transform().get_childCount() == 0 && ((Component) this).GetComponentsInChildren<Component>().Length <= 2)
      {
        Object.Destroy((Object) ((Component) this).get_gameObject());
      }
      else
      {
        if (!(((Object) this).get_name() == "Anti-Cheat Toolkit Detectors") || ((Component) this).GetComponentsInChildren<ACTkDetectorBase>().Length > 1)
          return;
        Object.Destroy((Object) ((Component) this).get_gameObject());
      }
    }

    internal virtual void OnCheatingDetected()
    {
      if (this.CheatDetected != null)
        this.CheatDetected();
      if (this.detectionEventHasListener)
        this.detectionEvent.Invoke();
      if (this.autoDispose)
        this.DisposeInternal();
      else
        this.StopDetectionInternal();
    }

    protected virtual bool Init(ACTkDetectorBase instance, string detectorName)
    {
      if (Object.op_Inequality((Object) instance, (Object) null) && Object.op_Inequality((Object) instance, (Object) this) && instance.keepAlive)
      {
        Debug.LogWarning((object) ("[ACTk] " + ((Object) this).get_name() + ": self-destroying, other instance already exists & only one instance allowed!"), (Object) ((Component) this).get_gameObject());
        Object.Destroy((Object) this);
        return false;
      }
      Object.DontDestroyOnLoad(!Object.op_Inequality((Object) ((Component) this).get_transform().get_parent(), (Object) null) ? (Object) ((Component) this).get_gameObject() : (Object) ((Component) ((Component) this).get_transform().get_root()).get_gameObject());
      return true;
    }

    protected virtual void DisposeInternal()
    {
      Object.Destroy((Object) this);
    }

    protected virtual bool DetectorHasCallbacks()
    {
      return this.CheatDetected != null || this.detectionEventHasListener;
    }

    protected virtual void StopDetectionInternal()
    {
      this.CheatDetected = (Action) null;
      this.started = false;
      this.isRunning = false;
    }

    protected virtual void PauseDetector()
    {
      if (!this.started)
        return;
      this.isRunning = false;
    }

    protected virtual bool ResumeDetector()
    {
      if (!this.started || !this.DetectorHasCallbacks())
        return false;
      this.isRunning = true;
      return true;
    }

    protected abstract void StartDetectionAutomatically();
  }
}
