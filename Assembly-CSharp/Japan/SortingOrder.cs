﻿// Decompiled with JetBrains decompiler
// Type: SortingOrder
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

[ExecuteInEditMode]
[AddComponentMenu("Rendering/SortingOrder")]
[RequireComponent(typeof (Renderer))]
public class SortingOrder : MonoBehaviour
{
  [SerializeField]
  private int mSortingOrder;

  public SortingOrder()
  {
    base.\u002Ector();
  }

  private void Awake()
  {
    ((Behaviour) this).set_enabled(false);
  }

  private void OnValidate()
  {
    ((Renderer) ((Component) this).GetComponent<Renderer>()).set_sortingOrder(this.mSortingOrder);
  }
}
