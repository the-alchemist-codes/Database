﻿// Decompiled with JetBrains decompiler
// Type: EditorPlayerPrefs
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

public static class EditorPlayerPrefs
{
  public static void DeleteAll()
  {
  }

  public static int CountKeys()
  {
    return 0;
  }

  public static void DeleteKey(string key)
  {
  }

  public static void Flush()
  {
  }

  public static bool HasKey(string key)
  {
    return false;
  }

  public static void SetInt(string key, int value)
  {
  }

  public static int GetInt(string key)
  {
    return 0;
  }

  public static void SetString(string key, string value)
  {
  }

  public static string GetString(string key)
  {
    return (string) null;
  }
}
