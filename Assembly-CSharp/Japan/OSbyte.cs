﻿// Decompiled with JetBrains decompiler
// Type: OSbyte
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using CodeStage.AntiCheat.ObscuredTypes;
using MessagePack;

[MessagePackObject(true)]
public struct OSbyte
{
  private ObscuredSByte value;

  public OSbyte(sbyte value)
  {
    this.value = (ObscuredSByte) value;
  }

  public static implicit operator OSbyte(sbyte value)
  {
    return new OSbyte(value);
  }

  public static implicit operator sbyte(OSbyte value)
  {
    return (sbyte) value.value;
  }

  public static OSbyte operator ++(OSbyte value)
  {
    ref OSbyte local = ref value;
    local.value = (ObscuredSByte) (sbyte) ((int) (sbyte) local.value + 1);
    return value;
  }

  public static OSbyte operator --(OSbyte value)
  {
    ref OSbyte local = ref value;
    local.value = (ObscuredSByte) (sbyte) ((int) (sbyte) local.value - 1);
    return value;
  }

  public override string ToString()
  {
    return this.value.ToString();
  }
}
