﻿// Decompiled with JetBrains decompiler
// Type: Region
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

public class Region
{
  public CloudRegionCode Code;
  public string Cluster;
  public string HostAndPort;
  public int Ping;

  public Region(CloudRegionCode code)
  {
    this.Code = code;
    this.Cluster = code.ToString();
  }

  public Region(CloudRegionCode code, string regionCodeString, string address)
  {
    this.Code = code;
    this.Cluster = regionCodeString;
    this.HostAndPort = address;
  }

  public static CloudRegionCode Parse(string codeAsString)
  {
    if (codeAsString == null)
      return CloudRegionCode.none;
    int length = codeAsString.IndexOf('/');
    if (length > 0)
      codeAsString = codeAsString.Substring(0, length);
    codeAsString = codeAsString.ToLower();
    return Enum.IsDefined(typeof (CloudRegionCode), (object) codeAsString) ? (CloudRegionCode) Enum.Parse(typeof (CloudRegionCode), codeAsString) : CloudRegionCode.none;
  }

  internal static CloudRegionFlag ParseFlag(CloudRegionCode region)
  {
    return Enum.IsDefined(typeof (CloudRegionFlag), (object) region.ToString()) ? (CloudRegionFlag) Enum.Parse(typeof (CloudRegionFlag), region.ToString()) : (CloudRegionFlag) 0;
  }

  [Obsolete]
  internal static CloudRegionFlag ParseFlag(string codeAsString)
  {
    codeAsString = codeAsString.ToLower();
    CloudRegionFlag cloudRegionFlag = (CloudRegionFlag) 0;
    if (Enum.IsDefined(typeof (CloudRegionFlag), (object) codeAsString))
      cloudRegionFlag = (CloudRegionFlag) Enum.Parse(typeof (CloudRegionFlag), codeAsString);
    return cloudRegionFlag;
  }

  public override string ToString()
  {
    return string.Format("'{0}' \t{1}ms \t{2}", (object) this.Cluster, (object) this.Ping, (object) this.HostAndPort);
  }
}
