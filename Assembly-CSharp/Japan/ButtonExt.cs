﻿// Decompiled with JetBrains decompiler
// Type: ButtonExt
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof (Button))]
public class ButtonExt : MonoBehaviour
{
  private ButtonExt.ButtonClickEvent mOnClick;

  public ButtonExt()
  {
    base.\u002Ector();
  }

  private void Start()
  {
    // ISSUE: method pointer
    ((UnityEvent) ((Button) ((Component) this).GetComponent<Button>()).get_onClick()).AddListener(new UnityAction((object) this, __methodptr(OnClick)));
  }

  private void OnClick()
  {
    this.mOnClick(((Component) this).get_gameObject());
  }

  public void AddListener(ButtonExt.ButtonClickEvent listener)
  {
    this.mOnClick += listener;
  }

  public void RemoveListener(ButtonExt.ButtonClickEvent listener)
  {
    this.mOnClick -= listener;
  }

  public delegate void ButtonClickEvent(GameObject go);
}
