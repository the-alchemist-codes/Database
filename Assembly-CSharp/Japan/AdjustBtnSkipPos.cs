﻿// Decompiled with JetBrains decompiler
// Type: AdjustBtnSkipPos
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

public class AdjustBtnSkipPos : MonoBehaviour
{
  public AdjustBtnSkipPos()
  {
    base.\u002Ector();
  }

  private void Start()
  {
    RectTransform component = (RectTransform) ((Component) this).GetComponent<RectTransform>();
    Rect safeArea = SetCanvasBounds.GetSafeArea(false);
    float num1 = ((Rect) ref safeArea).get_width() / (float) Screen.get_width();
    Vector2 anchoredPosition = component.get_anchoredPosition();
    float num2 = 0.0f;
    if ((double) num1 < 1.0)
      num2 -= ((Rect) ref safeArea).get_x();
    float num3 = num2 - (float) SetCanvasBounds.GetAddFrame().x;
    if (Object.op_Inequality((Object) ((Component) this).get_transform().get_parent(), (Object) null))
      num3 /= (float) ((Component) this).get_transform().get_parent().get_localScale().x;
    ref Vector2 local = ref anchoredPosition;
    local.x = (__Null) (local.x + (double) num3);
    component.set_anchoredPosition(anchoredPosition);
  }
}
