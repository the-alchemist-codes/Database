﻿// Decompiled with JetBrains decompiler
// Type: EventCaching
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;

public enum EventCaching : byte
{
  DoNotCache = 0,
  [Obsolete] MergeCache = 1,
  [Obsolete] ReplaceCache = 2,
  [Obsolete] RemoveCache = 3,
  AddToRoomCache = 4,
  AddToRoomCacheGlobal = 5,
  RemoveFromRoomCache = 6,
  RemoveFromRoomCacheForActorsLeft = 7,
  SliceIncreaseIndex = 10, // 0x0A
  SliceSetIndex = 11, // 0x0B
  SlicePurgeIndex = 12, // 0x0C
  SlicePurgeUpToIndex = 13, // 0x0D
}
