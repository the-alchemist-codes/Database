﻿// Decompiled with JetBrains decompiler
// Type: FlowNode_LocalizedText
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

[FlowNode.NodeType("Common/LocalizedText", 32741)]
[FlowNode.Pin(0, "Load", FlowNode.PinTypes.Input, 0)]
[FlowNode.Pin(1, "Reload", FlowNode.PinTypes.Input, 1)]
[FlowNode.Pin(10, "Unload", FlowNode.PinTypes.Input, 2)]
[FlowNode.Pin(100, "Out", FlowNode.PinTypes.Output, 10)]
public class FlowNode_LocalizedText : FlowNode
{
  public string tableID;

  public override void OnActivate(int pinID)
  {
    if (string.IsNullOrEmpty(this.tableID))
      return;
    switch (pinID)
    {
      case 0:
        LocalizedText.LoadTable(this.tableID, false);
        break;
      case 1:
        LocalizedText.LoadTable(this.tableID, true);
        break;
      default:
        LocalizedText.UnloadTable(this.tableID);
        break;
    }
    this.ActivateOutputLinks(100);
  }
}
