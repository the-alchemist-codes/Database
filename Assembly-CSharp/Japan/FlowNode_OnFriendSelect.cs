﻿// Decompiled with JetBrains decompiler
// Type: FlowNode_OnFriendSelect
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

[AddComponentMenu("")]
[FlowNode.NodeType("Event/OnFriendSelect", 58751)]
[FlowNode.Pin(1, "Selected", FlowNode.PinTypes.Output, 0)]
public class FlowNode_OnFriendSelect : FlowNodePersistent
{
  public void Selected()
  {
    this.ActivateOutputLinks(1);
  }
}
