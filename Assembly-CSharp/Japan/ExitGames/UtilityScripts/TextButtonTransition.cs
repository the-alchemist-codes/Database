﻿// Decompiled with JetBrains decompiler
// Type: ExitGames.UtilityScripts.TextButtonTransition
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace ExitGames.UtilityScripts
{
  [RequireComponent(typeof (Text))]
  public class TextButtonTransition : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IEventSystemHandler
  {
    private Text _text;
    public Color NormalColor;
    public Color HoverColor;

    public TextButtonTransition()
    {
      base.\u002Ector();
    }

    public void Awake()
    {
      this._text = (Text) ((Component) this).GetComponent<Text>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
      ((Graphic) this._text).set_color(this.HoverColor);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
      ((Graphic) this._text).set_color(this.NormalColor);
    }
  }
}
