﻿// Decompiled with JetBrains decompiler
// Type: ExitGames.Client.Photon.Chat.ChannelCreationOptions
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace ExitGames.Client.Photon.Chat
{
  public class ChannelCreationOptions
  {
    public static ChannelCreationOptions Default = new ChannelCreationOptions();

    public bool PublishSubscribers { get; set; }

    public int MaxSubscribers { get; set; }
  }
}
