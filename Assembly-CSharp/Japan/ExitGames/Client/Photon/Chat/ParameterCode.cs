﻿// Decompiled with JetBrains decompiler
// Type: ExitGames.Client.Photon.Chat.ParameterCode
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace ExitGames.Client.Photon.Chat
{
  public class ParameterCode
  {
    public const byte ApplicationId = 224;
    public const byte Secret = 221;
    public const byte AppVersion = 220;
    public const byte ClientAuthenticationType = 217;
    public const byte ClientAuthenticationParams = 216;
    public const byte ClientAuthenticationData = 214;
    public const byte Region = 210;
    public const byte Address = 230;
    public const byte UserId = 225;
  }
}
