﻿// Decompiled with JetBrains decompiler
// Type: ExitGames.Client.Photon.Chat.ChatParameterCode
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace ExitGames.Client.Photon.Chat
{
  public class ChatParameterCode
  {
    public const byte Channels = 0;
    public const byte Channel = 1;
    public const byte Messages = 2;
    public const byte Message = 3;
    public const byte Senders = 4;
    public const byte Sender = 5;
    public const byte ChannelUserCount = 6;
    public const byte UserId = 225;
    public const byte MsgId = 8;
    public const byte MsgIds = 9;
    public const byte Secret = 221;
    public const byte SubscribeResults = 15;
    public const byte Status = 10;
    public const byte Friends = 11;
    public const byte SkipMessage = 12;
    public const byte HistoryLength = 14;
    public const byte WebFlags = 21;
    public const byte Properties = 22;
    public const byte ChannelSubscribers = 23;
  }
}
