﻿// Decompiled with JetBrains decompiler
// Type: ExitGames.Client.Photon.Chat.ChannelWellKnownProperties
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

namespace ExitGames.Client.Photon.Chat
{
  public class ChannelWellKnownProperties
  {
    public const byte MaxSubscribers = 255;
    public const byte PublishSubscribers = 254;
  }
}
