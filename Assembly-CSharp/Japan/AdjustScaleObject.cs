﻿// Decompiled with JetBrains decompiler
// Type: AdjustScaleObject
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

public class AdjustScaleObject : MonoBehaviour
{
  [SerializeField]
  private float SetPower;
  private Rect lastSafeArea;
  private Vector3 initScale;
  private float lastSetPower;

  public AdjustScaleObject()
  {
    base.\u002Ector();
  }

  private void Start()
  {
    this.initScale = ((Component) this).get_transform().get_localScale();
    this.ApplySafeAreaScale(SetCanvasBounds.GetSafeArea(false));
  }

  private void ApplySafeAreaScale(Rect area)
  {
    if ((double) (((Rect) ref area).get_width() / (float) Screen.get_width()) < 1.0)
      ((Component) this).get_transform().set_localScale(Vector3.op_Multiply(this.SetPower, this.initScale));
    else
      ((Component) this).get_transform().set_localScale(this.initScale);
    this.lastSafeArea = area;
    this.lastSetPower = this.SetPower;
  }
}
