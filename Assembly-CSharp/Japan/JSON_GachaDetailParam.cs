﻿// Decompiled with JetBrains decompiler
// Type: JSON_GachaDetailParam
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

public class JSON_GachaDetailParam
{
  public int pk;
  public JSON_GachaDetailParam.Fileds fields;

  public class Fileds
  {
    public int id;
    public string gname;
    public int type;
    public string text;
    public string image;
    public int width;
    public int height;
  }
}
