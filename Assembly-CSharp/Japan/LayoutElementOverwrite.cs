﻿// Decompiled with JetBrains decompiler
// Type: LayoutElementOverwrite
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.UI;

public class LayoutElementOverwrite : MonoBehaviour
{
  [SerializeField]
  private LayoutElement TargetLayoutElement;
  [SerializeField]
  private GameObject BaseGameObject;
  [SerializeField]
  private bool minWidth;
  [SerializeField]
  private bool minHeight;
  [SerializeField]
  private bool preferredWidth;
  [SerializeField]
  private bool preferredHeight;
  [SerializeField]
  private bool flexibleWidth;
  [SerializeField]
  private bool flexibleHeight;
  private Vector3 prevPos;

  public LayoutElementOverwrite()
  {
    base.\u002Ector();
  }

  public void Refresh()
  {
    if (Object.op_Equality((Object) this.BaseGameObject, (Object) null))
    {
      DebugUtility.LogWarning("LayoutElementOverwrite.cs => Apply():Base Game Object is Nothing.");
    }
    else
    {
      if (Object.op_Equality((Object) this.TargetLayoutElement, (Object) null))
      {
        this.TargetLayoutElement = (LayoutElement) ((Component) this).GetComponent<LayoutElement>();
        if (Object.op_Equality((Object) this.TargetLayoutElement, (Object) null))
        {
          DebugUtility.LogWarning("LayoutElementOverwrite.cs => Apply():Target Layout Element is Nothing.");
          return;
        }
      }
      Vector3 vector3 = ((Component) this.TargetLayoutElement).get_transform().InverseTransformPoint(this.BaseGameObject.get_transform().get_position());
      RectTransform component = (RectTransform) this.BaseGameObject.GetComponent<RectTransform>();
      ref Vector3 local1 = ref vector3;
      local1.y = local1.y - component.get_sizeDelta().y;
      ref Vector3 local2 = ref vector3;
      local2.x = local2.x + component.get_sizeDelta().x;
      if (this.minHeight)
        this.TargetLayoutElement.set_minHeight((float) -vector3.y);
      if (this.preferredHeight)
        this.TargetLayoutElement.set_preferredHeight((float) -vector3.y);
      if (this.flexibleHeight)
        this.TargetLayoutElement.set_flexibleHeight((float) -vector3.y);
      if (this.minWidth)
        this.TargetLayoutElement.set_minWidth((float) vector3.x);
      if (this.preferredWidth)
        this.TargetLayoutElement.set_preferredWidth((float) vector3.x);
      if (this.flexibleWidth)
        this.TargetLayoutElement.set_flexibleWidth((float) vector3.x);
      ((Behaviour) this.TargetLayoutElement).set_enabled(false);
      ((Behaviour) this.TargetLayoutElement).set_enabled(true);
    }
  }

  private void Update()
  {
    if (Object.op_Equality((Object) this.BaseGameObject, (Object) null))
      return;
    this.BaseGameObject.get_transform().SetAsLastSibling();
    Vector3 vector3 = ((Component) this.TargetLayoutElement).get_transform().InverseTransformPoint(this.BaseGameObject.get_transform().get_position());
    if (this.prevPos.x == vector3.x && this.prevPos.y == vector3.y)
      return;
    this.prevPos = vector3;
    this.Refresh();
  }
}
