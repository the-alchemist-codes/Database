﻿// Decompiled with JetBrains decompiler
// Type: SRPG_CanvasScaler
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[AddComponentMenu("Layout/Canvas Scaler (SRPG)")]
[ExecuteInEditMode]
public class SRPG_CanvasScaler : CanvasScaler
{
  public static bool UseKuroObi = true;
  public const float MinScreenWidth = 1334f;
  public const float MinScreenHeight = 750f;

  public SRPG_CanvasScaler()
  {
    base.\u002Ector();
  }

  protected virtual void Awake()
  {
    ((UIBehaviour) this).Awake();
    this.set_uiScaleMode((CanvasScaler.ScaleMode) 1);
    if (SRPG_CanvasScaler.UseKuroObi)
    {
      this.set_screenMatchMode((CanvasScaler.ScreenMatchMode) 1);
    }
    else
    {
      this.set_screenMatchMode((CanvasScaler.ScreenMatchMode) 0);
      this.set_matchWidthOrHeight(1f);
    }
    this.set_referenceResolution(new Vector2(1334f, 750f));
  }
}
