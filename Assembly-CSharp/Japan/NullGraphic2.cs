﻿// Decompiled with JetBrains decompiler
// Type: NullGraphic2
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[ExecuteInEditMode]
public class NullGraphic2 : Graphic
{
  public NullGraphic2()
  {
    base.\u002Ector();
  }

  protected virtual void Start()
  {
    ((UIBehaviour) this).Start();
    this.set_color(new Color(0.0f, 0.0f, 0.0f, 0.0f));
  }

  protected virtual void OnPopulateMesh(VertexHelper vh)
  {
    base.OnPopulateMesh(vh);
  }
}
