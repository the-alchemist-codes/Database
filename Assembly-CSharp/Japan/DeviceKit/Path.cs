﻿// Decompiled with JetBrains decompiler
// Type: DeviceKit.Path
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System.IO;
using UnityEngine;

namespace DeviceKit
{
  public static class Path
  {
    public static string documentPath
    {
      get
      {
        return Path.devicekit_documentPath();
      }
    }

    public static string applicationDataPath
    {
      get
      {
        return Path.devicekit_applicationDataPath();
      }
    }

    public static string cachePath
    {
      get
      {
        return Path.devicekit_cachePath();
      }
    }

    private static string CreateDirectory(string directory)
    {
      Directory.CreateDirectory(directory);
      return directory;
    }

    private static string devicekit_documentPath()
    {
      return Path.CreateDirectory(Application.get_persistentDataPath() + "/Documents");
    }

    private static string devicekit_applicationDataPath()
    {
      return Path.CreateDirectory(Application.get_persistentDataPath() + "/AppData");
    }

    private static string devicekit_cachePath()
    {
      return Application.get_temporaryCachePath();
    }
  }
}
