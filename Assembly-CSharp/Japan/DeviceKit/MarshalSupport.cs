﻿// Decompiled with JetBrains decompiler
// Type: DeviceKit.MarshalSupport
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using System;
using System.Runtime.InteropServices;
using System.Text;

namespace DeviceKit
{
  internal static class MarshalSupport
  {
    public static string ToString(IntPtr intptr)
    {
      if (!(intptr != IntPtr.Zero))
        return (string) null;
      int length = 0;
      while (Marshal.ReadByte(intptr, length) != (byte) 0)
        ++length;
      byte[] numArray = new byte[length];
      Marshal.Copy(intptr, numArray, 0, length);
      return Encoding.Default.GetString(numArray);
    }
  }
}
