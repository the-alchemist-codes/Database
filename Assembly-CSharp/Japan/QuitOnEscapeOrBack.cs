﻿// Decompiled with JetBrains decompiler
// Type: QuitOnEscapeOrBack
// Assembly: Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null
// MVID: 7A0D5D87-02C0-4B8D-8ABC-07B6861F0B1A
// Assembly location: D:\Projects\mining-tools\DLLFileExtraction\ex\Assembly-CSharp1.dll

using UnityEngine;

public class QuitOnEscapeOrBack : MonoBehaviour
{
  public QuitOnEscapeOrBack()
  {
    base.\u002Ector();
  }

  private void Update()
  {
    if (!Input.GetKeyDown((KeyCode) 27))
      return;
    Application.Quit();
  }
}
